//
//  KeypadConnector.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 23/04/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import Foundation


// ConnectorStatus Transition
class ConnectorStatus {
	
	enum ConnectorState {
		case blank
		case numberFromBlank
		case 条
		case numberFrom条
		case numberFromの
		case の
		case 第
		case numberFrom第
		case 項
		case numberFrom項
		case last号
		
		func keyTopString() -> String {
			switch self {
			case .numberFromBlank : return "条"
			case .numberFrom条 : return "項"
			case .numberFromの : return "第"
			case .numberFrom第 : return "項"
			case .numberFrom項 : return "号"
			case .項 : return "号"
			default: return ""
			}
		}
		
		mutating func transitionWithCharacter(_ moji:Character) {
			if moji.number {
				
				switch self {
				case .blank : self = .numberFromBlank
				case .条 : self = .numberFrom条
				case .項 : self = .numberFrom項
				case .の : self = .numberFromの
				case .第 : self = .numberFrom第
				default: break
				}
				return
			}
				
			else {
				
				switch String(moji) {
					
				case "条" : self = .条
				case "の" : self = .の
				case "第" : self = .第
				case "項" : self = .項
				case "号" : self = .last号
				default: break
				}
			}
			
		}
	}
	
	var state:ConnectorState = .blank
	var keyTopString:String {
		return state.keyTopString()
	}
	
	init(string:String) {
		
		for moji in string.characters {
			state.transitionWithCharacter(moji)
		}
	}
	
	func transitionWithString(_ string:String) {
		for moji in string.characters {
			state.transitionWithCharacter(moji)
		}
	}
}

extension Character {
	
	var number:Bool {
		switch String(self) {
		case "0":  return true
		case "1":  return true
		case "2":  return true
		case "3":  return true
		case "4":  return true
		case "5":  return true
		case "6":  return true
		case "7":  return true
		case "8":  return true
		case "9":  return true
		default: return false
		}
	}
	
	var 条:Bool { return String(self) == "条" }
	var の:Bool { return String(self) == "の" }
	var 第:Bool { return String(self) == "第" }
	var 項:Bool { return String(self) == "項" }
	var 号:Bool { return String(self) == "号" }
}

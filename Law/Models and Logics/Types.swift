//
//  Types.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 22/09/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import Foundation
#if os(iOS) || os(watchOS)
  import UIKit
#else
  typealias UIColor = NSColor
#endif

// DocItem represents an entry in the index.  Law doc, folder, bookmark, highlight, etc.
// Detail is defined in DocItem.swift
typealias DocItem = NSMutableDictionary

typealias ICloudConflictDecisionHandler = (_ fileVersion: NSFileVersion) -> Void
typealias ICloudConflictNotificationHandler = (_ allVersions: [NSFileVersion], _ decisionHandler: @escaping ICloudConflictDecisionHandler ) -> Void

// DocProperty represents plist file in law package.
typealias DocPropertyKey = String
typealias DocProperty = [DocPropertyKey: Any]
extension DocPropertyKey {
  
  static let title = "title"
  static let md5 = "md5"
  static let edition = "edition"
  static let sourceURL = "sourceURL"
  
}

// LinkDictionary represents all the links to Jobun
struct LinkDictionary {
	var link: String
	var content: String
	var numerical: String
	
	var description: String {
		return "\(link) : \(content) : \(numerical)"
	}
	
	var dictionaryRepresentation: [String: Any] {
    return ["l": link, "c": content, "n": numerical]
	}
	
  init?(from dict: [String: Any]) {
		
		if !(dict["l"] is String) { return nil }
		if !(dict["c"] is String) { return nil }
		if !(dict["n"] is String) { return nil }

		link = dict["l"] as! String!
		content = dict["c"] as! String!
		numerical = dict["n"] as! String!
	}
	
	init( link: String, content: String, numerical: String ) {
		self.link = link
		self.content = content
		self.numerical = numerical
	}
}

// This represents law package file entity
struct FileEntry {
	var packageURL: URL
	var anchor: String?
	var elementId: String?
	var plist: DocProperty
	
}

// Bookmark bundles property to pass to FilePackage
struct Bookmark {
	var title: String
	var encodedURL: String
	var clickedURL: URL?
}

// LinkDestination represents a link which user clicked
struct LinkDestination {
	var h_file: String
	var anchor_f: String
	var referenceName: String
	
	init?(queryDictionary dict: [String: String]) {
		guard dict["H_FILE"] != nil else { return nil }
		
		h_file = dict["H_FILE"]!
		anchor_f = dict["ANCHOR_F"] ?? ""
		referenceName =  dict["REF_NAME"] ?? ""
	}
}

enum LawUpdateStatus : Int {
	case upToDate = 0, hasUpdate, removed, unknown
}

enum DownloadState : String {
	case InCloud = "cloud"
	case Downloading = "downloading"
	case Installed = "installed"
	case None = ""
	
	static func fromString(_ string: String?) -> DownloadState {
		if string == "cloud" { return .InCloud }
		if string == "downloading" { return .Downloading }
		if string == "installed" { return .Installed }
		return .None
	}
}

enum HighlightColor : Int {
	
	case yellow = 0, green, blue, red, purple, underline, selection = 99, added = 100, changed = 101, deleted = 102
	
	init(rawValue: Int) {
		switch rawValue {
		case 0: self = .yellow
		case 1: self = .green
		case 2: self = .blue
		case 3: self = .red
		case 4: self = .purple
		case 5: self = .underline
    case 99: self = .selection
    case 100: self = .added
    case 101: self = .changed
    case 102: self = .deleted

		default: self = .yellow
		}
	}
	
	func className() -> String {
		switch self {
		case .yellow: return "hilt"
		case .green: return "hilt1"
		case .blue: return "hilt2"
		case .red: return "hilt3"
		case .purple: return "hilt4"
		case .underline: return "hilt5"
    case .selection: return ""
    case .added: return ""
    case .changed: return ""
    case .deleted: return ""
		}
	}
	
	func naturalName() -> String {
		switch self {
		case .yellow: return "Yellow"
		case .green: return "Green"
		case .blue: return "Blue"
		case .red: return "Red"
		case .purple: return "Purple"
		case .underline: return "Underline"
    case .selection: return "Selection"
    case .added: return "Added"
    case .changed: return "Changed"
    case .deleted: return "Deleted"
      
    }
  }
  
  #if os(iOS) || os(watchOS)
  
  func color() -> UIColor! {
  switch self {
  case .yellow: return RGBA( 255.0, 220.0, 0.0, 0.6 )
  case .green: return RGBA( 150.0, 237.0, 0.0, 0.6 )
  case .blue: return RGBA( 110.0, 186.0, 255.0, 0.6 )
  case .red: return RGBA( 255.0, 107.0, 155.0, 0.6 )
  case .purple: return RGBA( 187.0, 117.0, 255.0, 0.6 )
  case .underline: return RGBA( 240.0, 0.0, 0.0, 0.9 )
  case .selection: return #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
  case .added: return #colorLiteral(red: 0.2666666806, green: 0.7647058964, blue: 0.4132910344, alpha: 1)
  case .changed: return #colorLiteral(red: 0.9060058594, green: 0.6182157629, blue: 0, alpha: 1)
  case .deleted: return #colorLiteral(red: 0.7733289931, green: 0.1174971704, blue: 0.648392693, alpha: 1)
  }
  }
  #else
  func color() -> UIColor! {
    
    switch self {
    case .yellow: return RGBA( 255.0, 220.0, 0.0, 0.6 )
    case .green: return RGBA( 150.0, 237.0, 0.0, 0.6 )
    case .blue: return RGBA( 110.0, 186.0, 255.0, 0.6 )
    case .red: return RGBA( 255.0, 107.0, 155.0, 0.6 )
    case .purple: return RGBA( 187.0, 117.0, 255.0, 0.6 )
    case .underline: return RGBA( 240.0, 0.0, 0.0, 0.9 )
    case .selection: return #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
    case .added: return #colorLiteral(red: 0.2666666806, green: 0.7647058964, blue: 0.4132910344, alpha: 1)
    case .changed: return #colorLiteral(red: 0.9060058594, green: 0.6182157629, blue: 0, alpha: 1)
    case .deleted: return #colorLiteral(red: 0.7733289931, green: 0.1174971704, blue: 0.648392693, alpha: 1)
    default: return RGBA( 255.0, 220.0, 0.0, 0.6 )
    }
  }
  #endif
}


// Tint Color
let kTintColor = RGBA(0.0, 79.0, 120.0, 1.0)

enum BackgroundColorSchemeType : String {
	case White = "White"
	case Paper = "Paper"
	case Sepia = "Sepia"
	case Grey = "Grey"
	case Dark = "Dark"

	init() {
		self = .Paper
	}
	
	init(fromUserDefaults value: String?) {
		
		self.init()
		if value != nil {
			switch value! {
			case "Paper": self = .Paper
			case "White": self = .White
			case "Sepia": self = .Sepia
			case "Grey": self = .Grey
			case "Dark": self = .Dark
			default: self = .Paper
			}
		}
	}
	
	init(rawValue: String ) {
		
		switch rawValue {
		case "Paper": self = .Paper
		case "White": self = .White
		case "Sepia": self = .Sepia
		case "Grey": self = .Grey
		case "Dark": self = .Dark
		default: self = .Paper
		}
	}
  
  var inverted: Bool {
    switch self {
    case .Paper: return false
    case .White: return false
    case .Sepia: return false
    case .Grey: return true
    case .Dark: return true
    }
  }
	
  var backgroundColor: UIColor {
		switch self {
		case .Paper: return UIColor(red: 1, green: 1, blue: 240.0/255.0, alpha: 1)
		case .White: return UIColor(red: 1, green: 1, blue: 1, alpha: 1)
		case .Sepia: return UIColor(red: 247.0/255.0, green: 239.0/255.0, blue: 224.0/255.0, alpha: 1)
		case .Grey: return UIColor(red: 0.29, green: 0.29, blue: 0.29, alpha: 1)
		case .Dark: return UIColor(red: 0, green: 0, blue: 0, alpha: 1)
		}
	}
  
  var secondaryBackgroundColor: UIColor {
    switch self {
    case .Paper: return UIColor(red: 1, green: 1, blue: 1, alpha: 1)
    case .White: return UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
    case .Sepia: return UIColor(red: 1, green: 1, blue: 1, alpha: 1)
    case .Grey: return UIColor(red: 0.15, green: 0.15, blue: 0.15, alpha: 1)
    case .Dark: return UIColor(red: 0.25, green: 0.25, blue: 0.25, alpha: 1)
    }
  }
  
  var popoverBackgroundColor: UIColor {
    switch self {
    case .Paper: return secondaryBackgroundColor.withAlphaComponent(0.85)
    case .White: return UIColor(red: 1, green: 1, blue: 1, alpha: 0.85)
    case .Sepia: return secondaryBackgroundColor.withAlphaComponent(0.85)
    case .Grey: return secondaryBackgroundColor.withAlphaComponent(0.6)
    case .Dark: return secondaryBackgroundColor.withAlphaComponent(0.75)
    }
  }
  
  var tableSelectionColor: UIColor {
    switch self {
    case .Paper: return tintColor.withAlphaComponent(0.5)
    case .White: return tintColor.withAlphaComponent(0.5)
    case .Sepia: return tintColor.withAlphaComponent(0.5)
    case .Grey: return tintColor.withAlphaComponent(0.5)
    case .Dark: return tintColor.withAlphaComponent(0.5)
    }
  }
	
  var tableBackgroundColor: UIColor {
		switch self {
		case .Paper: return UIColor.white
		case .White: return UIColor.white
		case .Sepia: return UIColor(red: 247.0/255.0, green: 239.0/255.0, blue: 224.0/255.0, alpha: 1)
		case .Grey: return UIColor(red: 0.29, green: 0.29, blue: 0.29, alpha: 1)
		case .Dark: return UIColor(red: 0, green: 0, blue: 0, alpha: 1)
		}
	}
	
  var backgroundColorCSS: String {
		switch self {
		case .Paper: return ""
		case .White: return "body { background-color: white; color: black; }"
		case .Sepia: return "body { background-color: #F7EFE0; color: black; }"
		case .Grey: return "body { background-color: #4c4c4c; color: white; }"
		case .Dark: return "body { background-color: black; color: white; }"
		}
	}
	
  var linkColorCSS: String {
		switch self {
		case .Paper: return ""
		case .White: return ""
		case .Sepia: return ""
		case .Grey: return " a:link { color: #A4C6CE  } " + " a:visited { color: PaleTurquoise  } "
		case .Dark: return " a:link { color: #A4C6CE  } " + " a:visited { color: PaleTurquoise  } "
		}
	}
	
  var blockQuoteColorCSS: String {
		switch self {
		case .Paper: return "#eef2ff"
		case .White: return "#eef2ff"
		case .Sepia: return "#E0E8F7"
		case .Grey: return "#0B2D35"
		case .Dark: return "#253C4C"
		}
	}
	
  var blockQuoteBorderCSS: String {
		switch self {
		case .Paper: return "#cccce0"
		case .White: return "#cccce0"
		case .Sepia: return "#677FAA"
		case .Grey: return "#909090"
		case .Dark: return "#909090"
		}
	}
	
  var tintColor: UIColor {
		switch self {
		case .Paper: return kTintColor
		case .White: return kTintColor
		case .Sepia: return UIColor(red: 184.0/255.0, green: 126.0/255.0, blue: 55.0/255.0, alpha: 1)
		case .Grey: return #colorLiteral(red: 0, green: 0.6852248907, blue: 0.7926144004, alpha: 1)
		case .Dark: return #colorLiteral(red: 0, green: 0.6947829127, blue: 1, alpha: 1)
		}
	}
	
		#if os(iOS)
  var barStyle: UIBarStyle {
		switch self {
		case .Paper: return .default
		case .White: return .default
		case .Sepia: return .default
		case .Grey: return .default
		case .Dark: return .default
		}
	}
	
  var indicatorStyle: UIScrollViewIndicatorStyle {
		switch self {
		case .Paper: return .black
		case .White: return .black
		case .Sepia: return .black
		case .Grey: return .white
		case .Dark: return .white
		}
	}
	#endif
	
  var textColor: UIColor {
		switch self {
		case .Paper: return .black
		case .White: return .black
		case .Sepia: return .black
		case .Grey: return .white
		case .Dark: return .white
		}
	}
	
  var subTextColor: UIColor {
		switch self {
		case .Paper: return .gray
		case .White: return .gray
		case .Sepia: return .gray
		case .Grey: return .lightGray
		case .Dark: return .lightGray
		}
	}
}

// NOTIFICATION

enum LawNotification {
  static let settingsDidChange = Notification.Name("SettingsDidChange")
  static let splitViewOrientationDidChange = Notification.Name("SplitViewOrientationDidChange")
  static let highlightDidChangeExternally = Notification.Name("HighlightDidChangeExternally")
}

// Error

enum LawError: LocalizedError {
	
	case unknown
	case offline
	case accountMissing
	case userDisabledCloudKit
	case timeOut
	case notFound
	case iCloudOverCellularNotAllowed

	case fileError
	case unableToUpload

  
  var errorDescription: String? {
    
    switch self {
    case .unknown: return WLoc("ERR:Unknown Error")
    case .offline: return WLoc("ERR:OfflineError")
    case .accountMissing: return WLoc("ERR:Account Missing Error")
    case .userDisabledCloudKit: return WLoc("ERR:UserDisabledCloudKit")
    case .timeOut: return WLoc("ERR:TimeOut Error")
    case .notFound: return WLoc("ERR:NotFound Error")
    case .iCloudOverCellularNotAllowed: return WLoc("ERR:iCloudOverCellularNotAllowed Error")
      
    case .fileError: return WLoc("ERR:File Access Error")
    case .unableToUpload: return WLoc("ERR:Failed to upload")
      
    }
  }
}


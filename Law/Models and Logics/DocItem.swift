//
//  OutlineItemProtocol.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2014/09/03.
//  Copyright (c) 2014年 Catalystwo. All rights reserved.
//

import Foundation

extension DocItem {

  // -- CONSTANTS --
  enum Keys {
    static let anchor = "anchor" as NSString
    static let uuid = "uuid" as NSString
    static let title = "title" as NSString
    static let originalTitle = "originalTitle" as NSString
    static let md5 = "md5" as NSString
    static let edition = "edition" as NSString
    static let href = "href" as NSString
    static let h_file = "h_file" as NSString
    static let cloud = "cloud" as NSString
    static let sourceURL = "sourceURL" as NSString
    static let expanded = "expanded" as NSString
    static let isFolder = "folder" as NSString
    static let subitems = "items" as NSString
    
    static let packageName = "pkg" as NSString
    static let packageNameObsolete = "packageName" as NSString
    
    
    //HIGHLIHGT
    static let highlight = "hilt" as NSString
    static let highlightColor = "clr" as NSString
    
    //NOTES
    static let matchCount = "mtcc" as NSString
    static let nearestAnchor = "tag" as NSString
    
    static let etag = "etag" as NSString
    static let notes = "notes" as NSString
    
    static let selectedText = "text" as NSString
    
  }
}

extension DocItem /* aka NSMutableDictionary, HierarchicalDictionary */ {
	
  //MARK:- Convenience Initializers
	
	convenience init( folderWithTitle title:String ) {
		self.init()
		self.folder = true
		self.title = title
	}
	
	convenience init( highlightWithColor color:HighlightColor, nearestAnchor:String, matchCount:Int, text:String ) {
		self.init()
		
		self.highlight = true
		self.color = color
		self.uuid = shortUUIDString()
		self.nearestAnchor = nearestAnchor
		self.matchCount = matchCount //match count
		self.text = text
	}
	
	convenience init( bookmarkToAnchor anchor:String, inPackage packageName:String, title:String, originalTitle:String, withNotes notes:String?) {
		self.init()
		
		self.anchor = anchor
		self.packageName = packageName
		self.uuid = shortUUIDString()
		self.title = title
		self.originalTitle = originalTitle
    self.notes = notes
  }
  
  convenience init( lawDocumentWithTitle title:String?, packageName:String, sourceURLString:String?, md5:String? ) {
    self.init()
    
    if title != nil {
      self.title = title!
      self.originalTitle = title!
    }
    
    self.packageName = packageName
    self.sourceURLString = sourceURLString
    self.md5 = md5
  }
  
  convenience init( queryWithPackageName packageName:String, edition:String? ) {
    self.init()
    self.packageName = packageName
    self.edition = edition
  }
  
	
	//MARK:- Accessors
	
	var uuid:String? {
		get {
			return object(forKey: Keys.uuid) as? String
		}
		set {
			if newValue == nil {
				removeObject(forKey: Keys.uuid)
			}else {
				setObject(newValue! as String, forKey: Keys.uuid)
			}
		}
	}
  
  // RETURNS HUMAN READABLE TITLE
  var title: String {
    get {
      if folder == true {
        let string = object(forKey: Keys.title) as? String ?? "Untitled Folder"
        return  "📂 " + string
      }
      
      if highlight == true {
        let string = text ?? ""
        if let notes = notes {
          if notes.isEmpty == false  {
            return string + "💬"
          }
        }
        
        return string
      }
      
      if let notes = notes {
        let string = object(forKey: Keys.title) as? String ?? ""
        return "✏️" +  string + " " + notes
      }
      
      if object(forKey: Keys.anchor) != nil {
        let string = object(forKey: Keys.title) as? String ?? "Untitled"
        return "🔖" + string
      }
      
      let string = object(forKey: Keys.title) as? String ?? "Untitled"
      return string
    }
    set {
      setObject(newValue, forKey: Keys.title)
    }
  }
  
  var originalTitle: String {
    get {
      if folder == true {
        let string = object(forKey: Keys.title) as? String ?? "Untitled Folder"
        return string
      }
      
      let string = object(forKey: Keys.originalTitle) as? String ?? title
      if string.isEmpty == true { return title }
      return string
    }
    
    set {
      setObject(newValue, forKey: Keys.originalTitle)
    }
  }
  
  var md5: String? {
    get {
      let string = object(forKey: Keys.md5) as? String
      if string == "" { return nil }
      return string
    }
    
    set {
      if newValue == nil {
        removeObject(forKey: Keys.md5)
      }else {
        setObject(newValue!, forKey: Keys.md5)
      }
    }
  }
  
  var edition: String? {
    get {
      return object(forKey: Keys.edition) as? String
    }
    
    set {
      if newValue == nil {
        removeObject(forKey: Keys.edition)
      }else {
        setObject(newValue!, forKey: Keys.edition)
      }
    }
  }
  
  var href: String? {
    get {
      return object(forKey: Keys.href) as? String
    }
    set {
      if newValue == nil {
        removeObject(forKey: Keys.href)
      }else {
        setObject(newValue!, forKey: Keys.href)
      }
    }
  }
  
  var h_file: String? {
    get {
      return object(forKey: Keys.h_file) as? String
    }
    set {
      if newValue == nil {
        removeObject(forKey: Keys.h_file)
      }else {
        setObject(newValue!, forKey: Keys.h_file)
      }
    }
  }
  
  var cloud: DownloadState! {
    get {
      let string = object(forKey: Keys.cloud) as? String
      return DownloadState.fromString( string )
    }
    set { setObject(newValue.rawValue, forKey: Keys.cloud) }
  }
  
  var sourceURLString: String? {
    get {
      var string = object(forKey: Keys.sourceURL) as? String
      
      if string == "" { string = nil }
      return string
    }
    set {
      if newValue == nil {
        removeObject(forKey: Keys.sourceURL)
      }else {
        setObject(newValue!, forKey: Keys.sourceURL)
      }
    }
  }
  
  var expanded: Bool {
    get {
      if let expandedNumber:NSNumber = object(forKey: Keys.expanded) as? NSNumber  {
        return expandedNumber.boolValue
      }
      else { return false }
    }
    set {
      setObject( NSNumber(value: newValue), forKey: Keys.expanded)
    }
  }
  
  var folder: Bool {
    get {
      if object(forKey: Keys.isFolder) == nil { return false }
      return (object(forKey: Keys.isFolder)! as? NSNumber)?.boolValue ?? false }
    
    set {
      setObject( NSNumber(value: newValue), forKey: Keys.isFolder)
      
      if newValue == true {
        setObject( shortUUIDString(), forKey: Keys.uuid)
        let marray = NSMutableArray()
        setObject(marray, forKey: Keys.subitems)
        expanded = true
      }
    }
  }
  
  
  var packageURL: URL? {
    get {
      
      if let packageName = self.packageName as String? {
        return FilePackage.shared.url(with: packageName)
      }else {
        return nil
      }
    }
  }
  
  var packageURLExistsFile: Bool {
    get {
      if folder { return true }
      
      if let packageURL = self.packageURL {
        let fm = FileManager.default
        
        let path = packageURL.path
        return fm.fileExists( atPath: path )
      }
      
      return false
    }
  }
  
  var packageName: String? {
    // packageName -> pkg に名称変更した
    get {
      
      let suffix = ".html.comcatalystwopackage"
      var packageName = object(forKey: Keys.packageName) as? String ?? object(forKey: Keys.packageNameObsolete) as? String
      
      if packageName == nil { return nil }
      
      if packageName!.hasSuffix( suffix ) == false {
        packageName = packageName! + suffix
      }
      return packageName as String?
    }
    set {
      let suffix = ".html.comcatalystwopackage"
      
      if newValue == nil {
        removeObject(forKey: Keys.packageNameObsolete)
        removeObject(forKey: Keys.packageName)
        
      } else {
        
        var packageName = newValue! as String!
        
        if packageName!.hasSuffix( suffix ) == true {
          
          let length = (packageName! as NSString!).length - ( suffix as NSString!).length
          let index: String.Index = packageName!.index(packageName!.startIndex, offsetBy: length)
          packageName = packageName?.substring( to: index )
        }
        
        setObject( packageName!, forKey: Keys.packageName)
      }
    }
  }
  
  var anchor: String? {
    get { return object(forKey: Keys.anchor) as? String }
    set {
      if newValue == nil { removeObject(forKey: Keys.anchor) } else {
        setObject( newValue!, forKey:Keys.anchor) }
    }
  }
  
  var isLawEntity: Bool {
    
    get {
      if folder { return false } // folder
      if anchor != nil { return false } // Notes
      if highlight == true { return false } // Highlight
      if packageName == nil { return false }
      
      return true
    }
  }
  
  //MARK:- Highlight accessor
  
  var highlight: Bool {
    get {
      if let _ = object(forKey: Keys.highlight) { return true }
      return false
    }
    set {
      if newValue == true {
        setObject( true, forKey: Keys.highlight)
      }else {
        removeObject(forKey: Keys.highlight)
        
      }
    }
  }
  
  var color: HighlightColor? {
    get {
      if let rawColor = object(forKey: Keys.highlightColor) as? Int {
        return HighlightColor( rawValue: rawColor )
        
      }else {
        return nil
      }
    }
    
    set {
      if newValue == nil {
        removeObject(forKey: Keys.highlightColor)
      }else {
        setObject(newValue!.rawValue, forKey: Keys.highlightColor)
      }
    }
  }
  
  var matchCount: Int {
    get {
      
      if let count = object(forKey: Keys.matchCount) as? Int {
        return count
      }else {
        return 0
      }
    }
    set {
      setObject(newValue as Int, forKey: Keys.matchCount)
    }
  }
  
  var nearestAnchor: String? {
    get {
      return object(forKey: Keys.nearestAnchor) as? String
    }
    set {
      
      if newValue == nil {
        removeObject(forKey: Keys.nearestAnchor)
      }else {
        setObject(newValue! as String, forKey: Keys.nearestAnchor)
      }
    }
  }
  
  //
  
  var ETag: String? {
    get {
      return object(forKey: Keys.etag) as? String
    }
    
    set {
      
      if newValue == nil {
        removeObject(forKey: Keys.etag)
      }else {
        setObject(newValue! as String, forKey: Keys.etag)
      }
    }
  }
  
  var notes: String? {
    get {
      return object(forKey: Keys.notes) as? String
    }
    set {
      if newValue == nil {
        removeObject(forKey: Keys.notes)
      }else {
        setObject(newValue! as String, forKey: Keys.notes)
      }
    }
  }
  
  // Selected text in html
  var text: String? {
    get {
      var selectedText = object(forKey: Keys.selectedText) as? String
      if selectedText == nil { return nil }
      
      // BUG FIX WORK AROUND... selectedText may begins with a name=\"1t507r+\">
      if selectedText!.hasPrefix("a name") == true {
        if let range = selectedText?.range(of: "a name=.[^>]*>", options: .regularExpression, range: nil, locale: nil) {
          selectedText = selectedText!.substring(from: range.upperBound)
        }
      }
      
      return selectedText
    }
    set {
      if newValue == nil {
        removeObject(forKey: Keys.selectedText)
      }else {
        setObject(newValue! as String, forKey: Keys.selectedText)
      }
    }
  }
  
  // MARK:- Methods
  
  //	override public func isEqual(object: AnyObject?) -> Bool {
  //
  //		if object === self { return true }
  //
  //		if object is DocItem {
  //
  //			if self.uuid == (object as! DocItem).uuid {
  //				return true
  //			}
  //		}
  //
  //		return false
  //	}
  
  func removeFile() {
    if anchor == nil && folder == false && highlight == false {
      do {
        try FileManager.default.removeItem(at: packageURL!)
      } catch _ {
      }
    }
    if items != nil {
      for subitem in items! {
        (subitem as! DocItem).removeFile()
      }
    }
  }
}

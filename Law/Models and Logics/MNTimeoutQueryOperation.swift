//
//  CKDatabase+TimeOut.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 5/10/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import Foundation
import CloudKit


class MNTimeoutQueryOperation : CKQueryOperation{
	
	var operationTimer:Timer? = nil
	
	convenience init(query:CKQuery, timeOutInterval interval:TimeInterval?) {
		
		self.init( query:query )
		
		self.queuePriority = .veryHigh
		self.qualityOfService = .userInitiated
		self.allowsCellularAccess = true

		let timer = Timer.scheduledTimer( timeInterval: interval ?? 45.0, target: self, selector: #selector(MNTimeoutQueryOperation.mn_cancelOperation(_:)), userInfo: nil, repeats: false)
		operationTimer = timer
		
		self.addObserver(self, forKeyPath: "isFinished", options: NSKeyValueObservingOptions.new, context: nil)
	}
	
	deinit {
		operationTimer?.invalidate()
		operationTimer = nil
	}

  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    
		if keyPath == "isFinished" && isFinished == true  {
			operationTimer?.invalidate()
			operationTimer = nil
		}
	}
	
	func mn_cancelOperation(_ timer:Timer) {
		
		if isFinished == false {
			print("cancelled")
			cancel()
		}
		operationTimer = nil
	}
}


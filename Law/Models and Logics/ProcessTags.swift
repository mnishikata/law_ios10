//
//  ProcessTags.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2014/08/28.
//  Copyright (c) 2014年 Catalystwo. All rights reserved.
//

import Foundation
import UIKit

let processQueue_: DispatchQueue = DispatchQueue(label: "ProcessTagQueue" )

extension NSString {
  
  var titleOfHTML: String {
    
    let titleRange = range(of: "(?<=<TITLE>).*(?=</TITLE>)", options: [.regularExpression, .caseInsensitive])
    if titleRange.location == NSNotFound { return "Law" }
    return substring(with: titleRange)
  }
  
  var lawEditionDateString: String {
    
    //<DIV ALIGN="right">...<
    let editedDateRange = range(of: "(?<=<DIV ALIGN=\"right\">).[^<]*(?=<)", options: [.regularExpression, .caseInsensitive])
    if editedDateRange.location == NSNotFound { return "" }
    return substring(with: editedDateRange)
  }

  func getLinkDictionaries() -> [LinkDictionary] {
    
    func extractLinkAndContents( _ lineString: String! ) -> ( link: String, contents: String) {
      
      // lineString <A HREF="/html...">contents</a>
      
      // Extract contsnt (?<=>).*(?=\</A\>)
      // Link (?<=A HREF=").[^"]*(?=")
      
      let upperString: NSString = lineString.uppercased() as NSString
      
      let regex1 = "(?<=>).*(?=\\</A\\>)"
      let regex2 = "(?<=<A HREF=\").[^\"]*#.[^\"]*(?=\")"
      let regexB = "(?<=#).*"
      
      let range1 = upperString.range(of: regex1, options: [.regularExpression, .caseInsensitive])
      let range2 = upperString.range(of: regex2, options: [.regularExpression, .caseInsensitive])
      
      if range1.location == NSNotFound { return ("", "") }
      if range2.location == NSNotFound { return ("", "") }
      
      let linkString: NSString = (lineString as NSString).substring(with: range2) as NSString
      
      let partialRange = linkString.range(of: regexB, options: [.regularExpression, .caseInsensitive])
      
      if partialRange.location == NSNotFound { return ("", "") }
      
      let partialString = linkString.substring(with: partialRange)
      
      return ( link: partialString.compressedAnchor, contents: (lineString as NSString).substring(with: range1) )
    }
    
    // extract line  <A HREF=.*</A>
    let regex = "<A HREF=.*</A>"
    
    var linkDictionary: [LinkDictionary] = []
    var findRange = NSMakeRange(0, length)
    let upperHTML: NSString = uppercased as NSString
    
    while true {
      
      let options: NSString.CompareOptions = [.regularExpression, .caseInsensitive]
      let range: NSRange = upperHTML.range(of: regex, options: options,  range: findRange)
      
      if range.location == NSNotFound { break }
      
      let lineString = substring(with: range)
      
      let (link, contents) = extractLinkAndContents( lineString )
      
      // COntents - > number
      let numerical = contents.convertTitleToNumbers()
      let dict = LinkDictionary(link: link, content: contents, numerical: numerical)
      
      if link.isEmpty == false { linkDictionary.append(dict) }
      
      let loc = NSMaxRange(range as NSRange)
      findRange = NSMakeRange(loc, upperHTML.length - loc)
    }
    
    return linkDictionary
  }
  
  func tagType(at range: NSRange) -> (isBTag: Bool, isATag: Bool ) {
    
    // 同じ段落中に
    // < /a> </b>
    
    let returnUnichar = ("\n" as NSString).character(at: 0)
    let parenthesisUnichar = ("<" as NSString).character(at: 0)
    let slashUnichar = ("/" as NSString).character(at: 0)
    
    for loc in NSMaxRange(range) ..< length - 3 {
      let theUnichar = character(at: loc)
      
      if theUnichar == returnUnichar { continue }
      if theUnichar != parenthesisUnichar { continue }
      
      //		if moji == "\n" { continue }
      //		if moji != "<" { continue }
      
      let tagFirst = character(at: loc+1)
      
      if tagFirst != slashUnichar {
        // others such as <A etc
        return (false, false)
      }
      
      let tag:String = substring( with: NSMakeRange(loc+1, 2))
      
      if( tag.uppercased() == "/A" ) { return (false, true) }
      if( tag.uppercased() == "/B" ) { return (true, false) }
      
      // others such as /DIV
      return (false, false)
    }
    
    return (false, false)
  }


  var encodingType: String.Encoding {
    
    if range(of: "shift_jis").location == NSNotFound { return .shiftJIS }
    if range(of: "shift-jis").location == NSNotFound { return .shiftJIS }
    
    if range(of: "utf-8").location == NSNotFound { return .utf8 }
    if range(of: "utf_8").location == NSNotFound { return .utf8 }
    
    return .ascii
  }
  
  func extractMidashi() -> [LinkDictionary] {
    
    let extractRegexString = "^[\\p{Blank}\\t]*<A HREF=[^>]+\"data\">.*</A>"
    let trimRegex = "<[^<>]*>"
    let linkString = "(?<=HREF=\")[^\"]+"
    var midashis: [LinkDictionary] = []
    
    do {
      let extractRegex = try NSRegularExpression(pattern: extractRegexString, options: [.caseInsensitive, .anchorsMatchLines])
      let results: [NSTextCheckingResult] = extractRegex.matches(in: self as String, options: [], range: NSMakeRange(0, length))
      
      for result in results{
        let range = result.range
        
        let substring = self.substring(with: range ) as NSString
        let anchorRange: NSRange = substring.range(of: linkString, options: [.regularExpression, .caseInsensitive])
        if anchorRange.location == NSNotFound { continue }
        let anchor = substring.substring(with: anchorRange)
        
        let mstr = substring.mutableCopy() as! NSMutableString
        mstr.replaceOccurrences( of: trimRegex, with: "", options: [.regularExpression], range: NSMakeRange(0, mstr.length))
        
        let linkDict = LinkDictionary( link: anchor, content: mstr as String, numerical: "" )
        midashis.append( linkDict )
      }
      
    } catch {
    }
    
    return midashis
  }

  class func detectCommonPrefixNSString(_ string1: NSString, with string2: NSString) -> NSString {
    
    var shorter: NSString, longer: NSString
    
    if string1.length <  string2.length{ shorter = string1; longer = string2 }
    else { shorter = string2; longer = string1 }
    
    while true {
      
      if shorter.length == 0 { break }
      if longer.hasPrefix(shorter as String) { break }
      
      // 0. Delete last one letter or one tag from the bottom.
      
      // 1. If the last letter is a tag, remove the tag
      
      let regex = "<[^<>]*>\\Z"
      let theRange = NSMakeRange(0, shorter.length)
      let aRange: NSRange = shorter.range(of: regex, options: .regularExpression , range: theRange)
      
      var countToDelete = 1
      
      if aRange.length > 0 { countToDelete = aRange.length }
      
      let length: Int = shorter.length - countToDelete
      
      shorter = shorter.substring( to: length ) as NSString
    }
    
    return shorter
  }
  
  func getInyoTitleAndInyoData() -> (inyoTitle: URL?, inyoData: URL? ) {
    
    let upperHtmlString: NSString = uppercased as NSString
    
    let regex1 = "(?<=<FRAME SRC=\").*(?=\" NAME=\"inyotitle\">)"
    let regex2 = "(?<=<FRAME SRC=\").*(?=\" NAME=\"inyodata\">)"
    
    let range1 = upperHtmlString.range(of: regex1, options: [.regularExpression, .caseInsensitive])
    let range2 = upperHtmlString.range(of: regex2, options: [.regularExpression, .caseInsensitive])
    
    if ( range1.location == NSNotFound || range2.location == NSNotFound ) {
      return (nil, nil)
    }
    
    let string1 = substring( with: range1 )
    let string2 = substring( with: range2 )
    
    //http://law.e-gov.go.jp/
    let base = "http://law.e-gov.go.jp"
    
    return ( URL(string: base+string1), URL(string: base+string2))
  }

  func getSakuinAndData() -> ( indexURL: URL?, contentURL: URL? ) {
    
    let upperHtmlString: NSString = uppercased as NSString
    
    let regex1 = "(?<=<FRAME SRC=\").*(?=\" NAME=\"SAKUIN\">)"
    let regex2 = "(?<=<FRAME SRC=\").*(?=\" NAME=\"DATA\">)"
    
    let range1 = upperHtmlString.range(of: regex1, options: [.regularExpression, .caseInsensitive])
    let range2 = upperHtmlString.range(of: regex2, options: [.regularExpression, .caseInsensitive])
    
    if ( range1.location == NSNotFound || range2.location == NSNotFound ) {
      return (nil, nil)
    }
    
    let string1 = substring( with: range1 )
    let string2 = substring( with: range2 )
    
    //http://law.e-gov.go.jp/
    let base = "http://law.e-gov.go.jp"
    
    return ( URL(string: base+string1), URL(string: base+string2))
  }
  
  class func processContentData( _ data: Data, packageName: String, convert: Bool, linkDictionaries : inout [LinkDictionary] ) -> ( convertedHtmlString: NSMutableString, title: String, edited: String) {
    print("= processContentData \(data.count) =")
    
    var theString = NSMutableString(data: data, encoding: String.Encoding.ascii.rawValue)
    var title = ""
    var editedDate = ""
    
    if theString == nil { return ("", title: "", edited: "") }
    
    let theType = theString!.encodingType
    theString = NSMutableString(data: data, encoding: theType.rawValue)
    
    if ( convert == true &&  (theString != nil) ) {
      
      title = theString!.titleOfHTML
      editedDate = theString!.lawEditionDateString
      
      theString!.replaceCSS(stripCSSInstead: false, zoomScale: 1.0)
      
      print("Converting Kanji")
      theString!.convertKanjiNumberToArabicNumber(convertToZenkaku: true)
      
      // Compress anchors
      print("Compress anchors")
      theString!.compressAnchors()
      
      // Reconstruct link
      if theString == nil { return ("", title: "", edited: "") }
      
      theString!.appendLinks(linkDictionaries, packageName: packageName)
      theString!.appendParenthesisTags()
    }
    
    if theString == nil { return ("", title: "", edited: "") }
    
    return (theString!, title: title, edited: editedDate)
  }
  
  class func processIndexData( _ data: Data, convert: Bool, linkDictionaries : inout [LinkDictionary] ) -> ( title: String?, contentFileName: String?, htmlString: NSString? ) {
    
    var htmlString: NSMutableString? = NSMutableString(data: data, encoding: String.Encoding.ascii.rawValue)
    if htmlString == nil { return (nil,nil,nil) }
    
    let theType = htmlString!.encodingType
    htmlString = NSMutableString(data: data, encoding: theType.rawValue)
    
    if htmlString == nil { return (nil,nil,nil) }
    
    // Convert Kanji
    if convert == true {
      htmlString!.replaceCSS(stripCSSInstead: false, zoomScale: 1.0)
      
      if htmlString == nil { return (nil,nil,nil) }
      
      htmlString!.convertKanjiNumberToArabicNumber(convertToZenkaku: true)
    }
    
    // Compress anchors
    
    htmlString!.compressAnchors()
    
    // Get dictionary
    
    htmlString!.getLinkDictionaries().forEach {
      linkDictionaries.append( $0 )
    }
    
    // Get information if available
    
    let regex1 = "(?<=<!-- TITLE \\[).*(?=\\] -->)"
    let regex2 = "(?<=<!-- CONTENTFILE \\[).*(?=\\] -->)"
    
    let range1 = htmlString!.range(of: regex1, options: [.regularExpression, .caseInsensitive])
    let range2 = htmlString!.range(of: regex2, options: [.regularExpression, .caseInsensitive])
    
    if ( range1.location == NSNotFound || range2.location == NSNotFound ) {
      
      return (nil, nil, htmlString! as NSString)
    }
    
    let string1: NSString = htmlString!.substring(with: range1) as NSString
    return (string1.replacingPercentEscapes(using: String.Encoding.utf8.rawValue), htmlString!.substring(with: range2), htmlString! as NSString )
  }
}

//MARK:-
extension String {
  
  var encodedArticleTitle: String {
    //１８４の１２の二　のようになっているところがあるので注意。
    
    let kanjiNumber = "[一二三四五六七八九十]"
    let range = self.range(of: kanjiNumber, options: .regularExpression, range: startIndex ..< endIndex, locale: nil)
    var stringToUse: String!
    if range != nil {
      let substring = self.substring(with: range!)
      let arabic = substring.convertNumberToKanji(convertToZenkaku: false)
      stringToUse = replacingCharacters(in: range!, with: arabic )
    }else {
      stringToUse = self
    }
    
    var returnValue = ""
    for character in stringToUse.characters {
      var kanji: String = ""
      
      if character >= "０" && character <= "９" {
        switch character{
        case "０" : kanji = "0"
        case "１" : kanji = "1"
        case "２" : kanji = "2"
        case "３" : kanji = "3"
        case "４" : kanji = "4"
        case "５" : kanji = "5"
        case "６" : kanji = "6"
        case "７" : kanji = "7"
        case "８" : kanji = "8"
        case "９" : kanji = "9"
        default: break
        }
      }else {
        
        switch character{
        case "一" : kanji = "1"
        case "二" : kanji = "2"
        case "三" : kanji = "3"
        case "四" : kanji = "4"
        case "五" : kanji = "5"
        case "六" : kanji = "6"
        case "七" : kanji = "7"
        case "八" : kanji = "8"
        case "九" : kanji = "9"
        case "十" : kanji = "10"
        case "第" : kanji = "("
        case "条" : kanji = ")"
        case "の" : kanji = "-"
        case "ノ" : kanji = "-"
        default: kanji = String(character)
        }
      }
      returnValue += kanji
    }
    
    return returnValue
  }
  
  var decodedArticleTitle: String {
    // [660]-1
    
    var returnValue = ""
    for character in characters {
      var kanji: String = ""
      switch character{
      case "0" : kanji = "０"
      case "1" : kanji = "１"
      case "2" : kanji = "２"
      case "3" : kanji = "３"
      case "4" : kanji = "４"
      case "5" : kanji = "５"
      case "6" : kanji = "６"
      case "7" : kanji = "７"
      case "8" : kanji = "８"
      case "9" : kanji = "９"
      case "(" : kanji = "第"
      case ")" : kanji = "条"
      case "-" : kanji = "の"
      default: kanji = String(character)
      }
      
      returnValue += kanji
    }
    return returnValue
  }
  
  func convertPkgNameToHFileKanji() -> String {
    //  S63HO91 -> 昭六三法九一
    var total: String = ""
    var digit: String
    var suji: String = ""
    
    func appendSuji(_ digits: Int) -> String {
      
      if suji.characters.count == 0 { return "" }
      
      let theSuji = "000" + suji
      suji = ""
      return theSuji.substring(with: Range<String.Index>( theSuji.characters.index(theSuji.endIndex, offsetBy: 0 - digits) ..<  theSuji.endIndex))
    }
    
    let mpkg = NSMutableString(string: self)
    
    mpkg.replaceOccurrences(of: "HO", with: "法#", options: [], range: NSMakeRange(0,mpkg.length))
    mpkg.replaceOccurrences(of: "SE", with: "政#", options: [], range: NSMakeRange(0,mpkg.length))
    mpkg.replaceOccurrences(of: "KE", with: "憲#", options: [], range: NSMakeRange(0,mpkg.length))
    
    let mstr = mpkg as String
    
    for character in mstr.characters {
      
      if total.characters.count == 0 {
        switch character {
          
        case "S" : digit = "昭#"
        case "M" : digit = "明#"
        case "T" : digit = "大#"
        case "H" : digit = "平#"
        default: digit = String(character)
        }
        
      }else {
        
        switch character {
        case "1" : digit = "一"
        case "2" : digit = "二"
        case "3" : digit = "三"
        case "4" : digit = "四"
        case "5" : digit = "五"
        case "6" : digit = "六"
        case "7" : digit = "七"
        case "8" : digit = "八"
        case "9" : digit = "九"
        case "0" : digit = "〇"
        default: digit = String(character)
        }
      }
      
      total += digit
    }
    
    return total.replacingOccurrences(of: "#〇*(?=.)", with: "", options: .regularExpression, range: nil)
    
  }
  
  func convertHFileKanjiToPkg() -> String {
    
    // 昭六三法九一 -> S63HO91
    
    var total: String = ""
    var digit: String
    var suji: String = ""
    
    func appendSuji(_ digits: Int) -> String {
      
      if suji.characters.count == 0 { return "" }
      
      let theSuji = "000" + suji
      suji = ""
      return theSuji.substring(with: Range<String.Index>( theSuji.characters.index(theSuji.endIndex, offsetBy: 0 - digits) ..<  theSuji.endIndex))
    }
    
    for character in characters
    {
      
      if total.characters.count == 0 {
        switch character {
          
        case "昭" : digit = "S"
        case "明" : digit = "M"
        case "大" : digit = "T"
        case "平" : digit = "H"
        default: digit = String(character)
        }
        
      }else {
        
        switch character {
        case "一" : digit = ""; suji = suji + "1"
        case "二" : digit = ""; suji = suji + "2"
        case "三" : digit = ""; suji = suji + "3"
        case "四" : digit = ""; suji = suji + "4"
        case "五" : digit = ""; suji = suji + "5"
        case "六" : digit = ""; suji = suji + "6"
        case "七" : digit = ""; suji = suji + "7"
        case "八" : digit = ""; suji = suji + "8"
        case "九" : digit = ""; suji = suji + "9"
        case "〇" : digit = ""; suji = suji + "0"
        case "法" : digit = "HO"; total += appendSuji(2)
        case "政" : digit = "SE"; total += appendSuji(2)
        case "憲" : digit = "KE"; total += appendSuji(2)
        default: digit = String(character); total += appendSuji(2)
        }
      }
      
      total += digit
    }
    
    total += appendSuji(3)
    
    
    // Add necessary zeros
    
    // H8 -> H08
    // 9 -> 009
    
    // prefix regex [MTSH][]*
    
    return total
  }

  
  func convertNumberToKanji(convertToZenkaku: Bool) -> String {
    
    /*
     
     三百七十一
     
     get from left
     
     3
     x100
     7
     x10
     1
     */
    
    
    let myStr: String = self as String
    var total = 0
    var currentDigit: Int = 0
    
    for character in myStr.characters
    {
      
      if character == "千" { total += max( currentDigit, 1) * 1000; currentDigit = 0; }
      else if character == "百" { total += max( currentDigit, 1) * 100; currentDigit = 0; }
      else if character == "十"  { total += max( currentDigit, 1) * 10; currentDigit = 0;  }
      else  {
        if currentDigit != 0  {
          
          if currentDigit == 10 {
            total = total * 10
          }else {
            
            total += currentDigit
            total = total * 10
          }
          currentDigit = 0
        }
        
        switch character {
          
        case "一" : currentDigit = 1
        case "二" : currentDigit = 2
        case "三" : currentDigit = 3
        case "四" : currentDigit = 4
        case "五" : currentDigit = 5
        case "六" : currentDigit = 6
        case "七" : currentDigit = 7
        case "八" : currentDigit = 8
        case "九" : currentDigit = 9
        case "〇" : currentDigit = 10
        default: currentDigit = 0
        }
      }
    }
    
    
    if currentDigit == 10 {
      //		total = total * 10
      
    }else {
      total += currentDigit
    }
    
    // 数字を全角化
    
    if !convertToZenkaku { return String(total) }
    
    let thisString = String(total)
    var returnValue = ""
    for character in thisString.characters {
      var kanji: String = ""
      switch character{
      case "0" : kanji = "０"
      case "1" : kanji = "１"
      case "2" : kanji = "２"
      case "3" : kanji = "３"
      case "4" : kanji = "４"
      case "5" : kanji = "５"
      case "6" : kanji = "６"
      case "7" : kanji = "７"
      case "8" : kanji = "８"
      case "9" : kanji = "９"
      default: kanji = ""
      }
      
      returnValue += kanji
    }
    
    return returnValue
  }

  func transliterateToArabicNumber() -> String {
    var total: String = ""
    var digit: String
    
    for character in characters {
      switch character {
        
      case "一" : digit = "1"
      case "二" : digit = "2"
      case "三" : digit = "3"
      case "四" : digit = "4"
      case "五" : digit = "5"
      case "六" : digit = "6"
      case "七" : digit = "7"
      case "八" : digit = "8"
      case "九" : digit = "9"
      case "〇" : digit = "0"
      default: digit = String(character)
      }
      
      total += digit
    }
    
    return total
  }

  func convertTitleToNumbers() -> String {
    let mstring = NSMutableString(string: self)
    
    func replace(_ string: String, _ to: String) {
      mstring.replaceOccurrences(of: string, with: to, options: .literal, range: NSMakeRange(0, mstring.length));
    }
    
    replace("の", ".")
    replace("ノ", ".")
    replace("第", "")
    replace("条", "")
    replace("規則", "")
    
    replace("０", "0")
    replace("１", "1")
    replace("２", "2")
    replace("３", "3")
    replace("４", "4")
    replace("５", "5")
    replace("６", "6")
    replace("７", "7")
    replace("８", "8")
    replace("９", "9")
    
    return mstring as String
  }
  
  func getUrlQueryParameters() -> [String: String] {
    
    let comps = components(separatedBy: "&")
    var dict: [String: String] = [:]
    
    for comp in comps {
      
      let elements = comp.components(separatedBy: "=")
      
      if elements.count == 1 {
        dict[ elements[0] ] = ""
        continue
      }
      
      if elements.count < 2 { continue }
      
      let val = elements[1] as NSString
      if let decoded = val.replacingPercentEscapes(using: String.Encoding.shiftJIS.rawValue) {
        dict[ elements[0] ] = decoded
      }
    }
    /*
     "ANCHOR_F" = 1000000000000000000000000000000000000000000000003300000000002000000000000000000;
     "ANCHOR_T" = 1000000000000000000000000000000000000000000000003300000000002000000000000000000;
     "H_FILE" = "\U662d\U4e09\U56db\U6cd5\U4e00\U4e8c\U4e00";
     "REF_NAME" = "\U7b2c\U4e09\U5341\U4e09\U6761\U7b2c\U4e8c\U9805";
     */
    return dict
  }
  
  var compressedAnchor: String {
    
    //1000000000000000000000000000000000000000000000053100000000000000000000000000000
    //@#1o32x531o20x
    
    let mstr = NSMutableString(string: self)
    mstr.append("+")
    
    let regex = "0{4,}+"
    var findRange = NSMakeRange(0, mstr.length)
    
    while true {
      
      let range = mstr.range(of: regex, options: .regularExpression, range: findRange ) as NSRange
      
      if range.location == NSNotFound { break }
      
      var convertedString = NSString(format: "o%xx", range.length)
      
      switch convertedString {
      case "o7x": convertedString = "G"
      case "o8x": convertedString = "H"
      case "o9x": convertedString = "I"
      case "oax": convertedString = "J"
      case "obx": convertedString = "K"
      case "ocx": convertedString = "L"
      case "odx": convertedString = "M"
      case "oex": convertedString = "N"
      case "ofx": convertedString = "P"
      case "o12x": convertedString = "Q"
      case "o1dx": convertedString = "R"
      case "o2dx": convertedString = "S"
      case "o2ex": convertedString = "T"
      case "o2fx": convertedString = "U"
      case "o45x": convertedString = "Z"
      default: break
      }
      
      mstr.replaceCharacters(in: range, with: convertedString as String)
      
      let loc = NSMaxRange(range) - range.length + convertedString.length
      findRange = NSMakeRange(loc, mstr.length - loc)
    }
    
    return mstr as String
  }

  
  //  ABCDEF</div>
  //  ABC</div>
  //　detect ABC range
  
  static func detectCommonPrefix(_ string1: String!, with string2: String!) -> String {
    
    var shorter: String, longer: String
    
    if string1.characters.count <  string2.characters.count { shorter = string1; longer = string2 }
    else { shorter = string2; longer = string1 }
    
    while true {
      
      if shorter.characters.count == 0 { break }
      if longer.hasPrefix(shorter) { break }
      
      // 0. Delete last one letter or one tag from the bottom.
      
      // 1. If the last letter is a tag, remove the tag
      
      let regex = "<[^<>]*>\\Z"
      let theRange = NSMakeRange(0, shorter.characters.count)
      let aRange: NSRange = (shorter as NSString).range(of: regex, options: .regularExpression , range: theRange)
      
      var countToDelete = 1
      
      if aRange.length > 0 { countToDelete = aRange.length }
      
      let length: Int = shorter.characters.count - countToDelete
      
      let index: String.Index = shorter.index(shorter.startIndex, offsetBy: length)
      shorter = shorter.substring( to: index )
    }
    
    return shorter
  }
  
  func buildBlockQuote(href: String!, uuid: String!) -> String {
    
    let p1 = "<br><div class=\"blockquote\" id=\""
    let p2 = "\"> <a href=\"notes:"
    let p3 = "\" class=\"blockquote\">"
    let prefix = p1 + uuid + p2 + href + p3
    let suffix = "</a></div><br>"
    
    let mstr = NSMutableString(string: self)
    mstr.esscapeCharactersInHtml()
    mstr.replaceOccurrences(of: "\n", with: "<br>", options: [], range: NSMakeRange(0, mstr.length))
    return prefix + (mstr as String) + suffix
  }
}

//MARK:-
extension NSMutableString {

  func appendLinks(_ linkDictionary: [LinkDictionary], packageName: String ) {
    
    /*
     リンクを付加する条件
     A B タグ内ではない
     
     
     // 2. Append Bookmark link
     B タグ内にはBookmark linkを追加する
     */
    
    
    func moveoverCloseATag( _ htmlSentence: NSString ) -> NSString {
      
      let mstr = htmlSentence.mutableCopy() as! NSMutableString
      
      mstr.replaceOccurrences( of: "\n", with: "", options: .caseInsensitive, range: NSMakeRange(0, mstr.length))
      let extractRegex = "(?<=>).*(?=</A>)"
      
      let range: NSRange = mstr.range(of: extractRegex, options: [.regularExpression, .caseInsensitive])
      if range.location == NSNotFound { return mstr as NSString }
      
      let contentString = mstr.substring(with: range)
      let extendedRange = NSMakeRange( range.location, mstr.length - range.location )
      
      mstr.replaceCharacters(in: extendedRange, with: "</A>" + contentString)
      
      return mstr as NSString
    }
    
    func moveANameTag( _ htmlString: NSMutableString) -> Void {
      
      let findRegex = "<A NAME.[^<]*</A>"
      //let extractRegex = "(?<=>).*(?=</A>)"
      
      let mstr = htmlString
      var findRange = NSMakeRange(0, mstr.length)
      
      while true {
        
        let range: NSRange = mstr.range(of: findRegex, options: [.regularExpression, .caseInsensitive],  range: findRange)
        if range.location == NSNotFound { break }
        
        let original: NSString = mstr.substring(with: range) as NSString
        let convertedString = moveoverCloseATag(original)
        
        mstr.replaceCharacters(in: range, with: convertedString as String)
        
        let loc = NSMaxRange(range) - range.length + convertedString.length
        findRange = NSMakeRange(loc, mstr.length - loc)
      }
    }
    
    debugPrint("appendLinks")
    
    moveANameTag( self )
    
    
    debugPrint("Main")
    
    let t0 = Date().timeIntervalSinceReferenceDate
    
    //うしろから計算
    
    for dict in linkDictionary.reversed() {
      
      let convertedString = "<A HREF=\"intl:" + dict.link + "\" class=\"intl\">" + dict.content + "</A>"
      var findRange = NSMakeRange(0, length)
      
      while true {
        
        let linkRange: NSRange = range( of: dict.content, options: [.literal], range: findRange)
        if linkRange.location == NSNotFound { break }
        
        let (isBTag, isATag): (Bool, Bool) = tagType(at: linkRange)
        
        if isBTag == false && isATag == false {
          
          replaceCharacters(in: linkRange, with: convertedString) // MUTATEING
          
          let loc = NSMaxRange(linkRange) - linkRange.length + (convertedString as NSString).length
          findRange = NSMakeRange(loc, length - loc)
          
        }else if isBTag == true {
          
          // Bookmark tag
          
          let targetString = substring(with: linkRange)
          let theString = targetString.encodedArticleTitle + "@" + dict.link + "@" + packageName
          
          let bookmarkString = "<A HREF=\"comcatalystwolaws:" + theString + "\" class=\"bkmk\">" + dict.content + "</A>"
          replaceCharacters(in: linkRange, with: bookmarkString) // MUTATEING
          
          let loc = NSMaxRange(linkRange) - linkRange.length + (bookmarkString as NSString).length
          findRange = NSMakeRange(loc, length - loc)
          
        }else {
          findRange = NSMakeRange(NSMaxRange(linkRange), length - NSMaxRange(linkRange))
        }
      }
    }
    
    let t1 = Date().timeIntervalSinceReferenceDate
    
    print("appendLinks done \(t1-t0)") //
    
    /*
     old insideTag
     7.13538098335266
     
     new 
     6.51244401931763
     
     */
    
  }
  
  
  func appendParenthesisTags() {
    
    /*
     SPAN class="parenthesis"
     リンクを付加する条件
     B タグ内ではない
     
     最大広さの括弧を検出
     */
    
    
    /*
     substring version 26.344820022583
     unichar version 2.34652501344681
     */
    
    debugPrint("appendParenthesisTag")
    
    
    var findRange = NSMakeRange(0, length)
    
    let roundParenthesisOpen: unichar = ("（" as NSString).character(at: 0)
    let roundParenthesisClose = ("）" as NSString).character(at: 0)
    let roundParenthesSet = CharacterSet(charactersIn: "（）")
    
    let t0 = Date().timeIntervalSinceReferenceDate
    
    while true {
      
      // 1. get (
      let range: NSRange = self.range(of: "（", options: [.literal],  range: findRange)
      
      // 2. End
      if range.location == NSNotFound { break }
      if NSMaxRange(range) > length { break }
      
      // 3. Skip if in B tag
      
      let (isBTag, _ /*isATag*/ ): (Bool, Bool) = tagType(at: range)
      if true == isBTag {
        
        let loc = NSMaxRange(range)
        findRange = NSMakeRange(loc, length - loc)
        
        continue
      }
      
      // 4. Look for closing ）
      
      var pcount = 1
      var loc = NSMaxRange(range)
      
      let htmlLength = length
      while true {
        if loc >= htmlLength { loc = NSNotFound; break }
        
        let parenthesisRange = rangeOfCharacter(from: roundParenthesSet, options: [.literal], range: NSMakeRange(loc, htmlLength - loc ))
        if parenthesisRange.location == NSNotFound { loc = NSNotFound; break }
        
        loc = parenthesisRange.location
        let mojiUnichar = character(at: loc)
        
        if mojiUnichar == roundParenthesisOpen { pcount += 1 }
        else if mojiUnichar == roundParenthesisClose { pcount -= 1 }
        
        if pcount == 0 { break }
        
        loc += 1
      }
      
      // 5. Build String with tag
      let theRange = NSMakeRange( range.location, loc - range.location + 1)
      
      // 5-1. Only one letter?
      
      if loc == NSNotFound || theRange.length == 3 || theRange.location == NSNotFound || NSMaxRange(theRange) > length {
        
        let loc = NSMaxRange(range)
        findRange = NSMakeRange(loc, length - loc)
        
        continue
      }
      
      let pstring = substring(with: theRange)
      
      // 5-2. Only numbers inside the string?
      
      let content = substring(with: NSMakeRange(theRange.location+1, theRange.length-2))
      
      if Int(content) != nil {
        
        let loc = NSMaxRange(range)
        findRange = NSMakeRange(loc, length - loc)
        
        continue
      }
      
      let replacedString = "<SPAN class=\"brckt\">" + pstring + "</SPAN>"
      
      // 6. Replace
      replaceCharacters(in: theRange, with: replacedString)
      
      loc = NSMaxRange(theRange) - theRange.length + (replacedString as NSString).length
      findRange = NSMakeRange(loc, length - loc)
    }
    let t1 = Date().timeIntervalSinceReferenceDate
    debugPrint("appendParenthesisTag done \(t1-t0)")
  }
  
  func compressAnchors() {
    
    let regex1 = "(?<=NAME=\")[^\"]*(?=\")"
    let regex2 = "(?<=HREF=\"#)[^\"]*(?=\")"
    
    func convertMe(_ regex: String) {
      
      print("Converting with regex \(regex)")
      
      var findRange = NSMakeRange(0, length)
      
      while true {
        
        let range: NSRange = self.range(of: regex, options: [.regularExpression, .caseInsensitive],  range: findRange)
        
        if range.location == NSNotFound { print("break"); break }
        
        let anchor = substring(with: range)
        let convertedString = anchor.compressedAnchor
        
        replaceCharacters(in: range, with: convertedString)
        
        let loc = NSMaxRange(range) - range.length + (convertedString as NSString).length
        findRange = NSMakeRange(loc, length - loc)
      }
    }
    
    convertMe(regex1)
    convertMe(regex2)
  }
  
  func esscapeCharactersInHtml() {
    
    let lineSeparator = UnicodeScalar(0x2028)
    
    let moji = String(describing: lineSeparator)
    
    func replace(_ from: String, _ to: String) {
      replaceOccurrences(of: from, with: to, options: [], range: NSMakeRange(0, length));
    }
    
    replace( moji,	"\n")
    replace( "\r",	"\n")
    replace( "\"",	"”" )
    replace( "'",	"&apos;" )
    replace( "&",	"&amp;" )
    replace( "<",	"&lt;" )
    replace( ">",	"&gt;" )
  }
  
  func convertKanjiNumberToArabicNumber(convertToZenkaku: Bool) {
    
    let regex1 = "(?<=第)[一二三四五六七八九十百千〇]+(?=条|項|規則)"
    let regex2 = "(?<=(条|規則)の)[一二三四五六七八九十百千]+"
    let regex3 = "[一二三四五六七八九十百千]+(?=年|月|日)"
    
    func convertMe(_ regex: String, convertToZenkaku: Bool) {
      
      print("Converting with regex \(regex)")
      
      var findRange = NSMakeRange(0, length)
      
      while true {
        
        let range: NSRange = self.range(of: regex, options: [.regularExpression, .caseInsensitive],  range: findRange)
        
        if range.location == NSNotFound { break }
        
        let kanji = substring(with: range)
        let convertedString = kanji.convertNumberToKanji(convertToZenkaku: convertToZenkaku)
        replaceCharacters(in: range, with: convertedString )
        
        let loc = NSMaxRange(range) - range.length + (convertedString as NSString).length
        findRange = NSMakeRange(loc, length - loc)
      }
    }
    
    convertMe(regex1, convertToZenkaku: convertToZenkaku)
    convertMe(regex2, convertToZenkaku: convertToZenkaku)
    convertMe(regex3, convertToZenkaku: convertToZenkaku)
  }
  
  func rangeToHighlight(dictionary: DocItem, showTooltip: Bool) -> (rangeToReplace: NSRange, snippet: String )? {
    
    
    // Look for tag
    
    // Look for selectedHTML (after n match)
    
    // Insert tag
    
    
    let ATag = dictionary.nearestAnchor
    let uuid = dictionary.uuid
    let color = dictionary.color
    var selectedText = dictionary.text
    
    if uuid == nil { return nil } // Failed
    if ATag == nil { return nil }
    if color == nil { return nil }
    if selectedText == nil { return nil }
    
    
    // BUG FIX WORK AROUND... selectedText may begins with a name=\"1t507r+\">
    if selectedText!.hasPrefix("a name") == true {
      if let range = selectedText?.range(of: "a name=.[^>]*>", options: .regularExpression, range: nil, locale: nil) {
        selectedText = selectedText!.substring(from: range.upperBound)
      }
    }
    
    
    ///
    
    let notes = dictionary.notes
    
    let tagRange = self.range(of: ATag!, options: .literal )
    var range = NSMakeRange( NSMaxRange(tagRange), length - NSMaxRange(tagRange))
    var count: Int = dictionary.matchCount
    var success = false
    var rangeToReplace: NSRange = NSMakeRange(0,0)
    
    while true {
      
      // ここの検索は、htmlタグを無視した検索をする
      let thisRange = rangeOfStringInHtmlUntilANameTag(selectedText as NSString!,  inRange: range )
      
      if count == 0 {
        if thisRange.length != 0 {
          success = true
          rangeToReplace = thisRange
        }
        break
      }
      
      count = count - 1
      
      range = NSMakeRange( NSMaxRange(thisRange), length - NSMaxRange(thisRange))
    }
    
    if success == true {
      
      let className = color!.className()
      
      if notes != nil {
        
        let length = ( notes as NSString! ).length
        
        if length > 0 {
          selectedText! = selectedText! + "💬"
        }
      }
      
      let encodedUUID = uuid!.addingPercentEncoding(  withAllowedCharacters: .urlQueryAllowed)
      if encodedUUID == nil {
        // Unknonn error
        return nil
      }
      
      let encodedURL = "\"highlight:" + encodedUUID! + "\""
      let snippet1 = "<u class=\"" + className + "\" id=\"" + uuid! + "\">"
      var snippet2 = "<a href=" + encodedURL + " class='highlight' >" + selectedText! + "</a></u>"
      
      if showTooltip == false || notes == nil || notes?.isEmpty == true {
        
      }else {
        
        if let encodedNotes = notes?.addingPercentEncoding(  withAllowedCharacters: .urlQueryAllowed) {
          snippet2 = "<a href=" + encodedURL + " class='highlight' title='" + encodedNotes + "'>" + selectedText! + "</a></u>"
        }
      }
      
      let snippet = snippet1 + snippet2
      
      //htmlString.replaceCharactersInRange(rangeToReplace, withString: snippet)
      
      return ( rangeToReplace, snippet )
    }
    
    return nil
  }
  
  func replaceCSS(stripCSSInstead stripCSS: Bool, zoomScale: Double) {
    let ud = UserDefaults.standard
    ud.synchronize()
    
    var dimParenthesis = true
    
    if let obj = ud.object(forKey: "DimParenthesis") as? NSNumber {
      dimParenthesis = obj.boolValue
    }
    
    let obj2 = ud.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj2 )
    
    let fontType = ud.string(forKey: "FontName")
    
    var parenthesis: String
    var bodyFont: String
    var blockquoteColor: String, blockquoteBorder:String
    
    if dimParenthesis == true {
      
      if ( backgroundColorScheme == .Dark || backgroundColorScheme == .Grey ) {
        parenthesis = "SPAN.brckt { font-family: Hiragino Kaku Gothic ProN; font-weight:normal; font-size:90%; color: Silver  ; letter-spacing: -1px;} \n"
        
      }else {
        parenthesis = "SPAN.brckt { font-family: Hiragino Kaku Gothic ProN; font-weight:normal; font-size:90%; color: SlateGray ; letter-spacing: -1px;} \n"
        
      }
    }else {
      parenthesis = "SPAN.brckt { font-family: Hiragino Kaku Gothic ProN; font-weight:normal; letter-spacing: -1px;} \n"
    }
    
    if fontType == "Gothic" {
      bodyFont = "BODY { line-height: 180%; background-color: #FFFFF0; font-family: Hiragino Kaku Gothic ProN; zoom: \(zoomScale); }\n"
      
    }else {
      bodyFont = "BODY { line-height: 180%; background-color: #FFFFF0; font-family: Hiragino Mincho ProN; zoom: \(zoomScale);  }\n"
    }
    
    
    var bodyColor: String, linkColor: String
    
    bodyColor = backgroundColorScheme.backgroundColorCSS
    linkColor = backgroundColorScheme.linkColorCSS
    
    blockquoteColor = backgroundColorScheme.blockQuoteColorCSS
    blockquoteBorder = backgroundColorScheme.blockQuoteBorderCSS
    
    let openTag = "<STYLE type=\"text/css\">"
    let closeTag = "</STYLE>"
    
    // WKWEBVIEWのばあいは<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" />
    var replace = "<STYLE type=\"text/css\">\n" +
      "<!--\n" +
      bodyFont +
      bodyColor +
      "TD { line-height: 150%; text-indent: 0em; }\n" +
      linkColor +
      "B { font-family: Hiragino Kaku Gothic ProN; font-weight:bold; }\n" +
      "A.intl {  font-weight:normal; color:inherit; text-decoration: none; border-bottom:1px dotted; }\n" +
      "A.bkmk {  font-weight:inherit; color:inherit ;}\n" +
      "A:link { font-weight:inherit; text-decoration: none; border-bottom:1px dotted; }\n" +
      parenthesis +
      "DIV { line-height: 150%; }\n" +
      "DIV.arttitle {font-family: Hiragino Kaku Gothic ProN; margin-left: 1em; }\n" +
      "DIV.item { margin-left: 1em; text-indent: -1em; }\n" +
      "DIV.number { margin-left: 1em; text-indent: -1em; }\n" +
      "DIV.para1 { margin-left: 1em; text-indent: -1em; }\n" +
      "DIV.para2 { margin-left: 2em; text-indent: -1em; }\n" +
      "DIV.para3 { margin-left: 3em; text-indent: -1em; }\n" +
      "DIV.para4 { margin-left: 4em; text-indent: -1em; }\n" +
      "DIV.para5 { margin-left: 5em; text-indent: -1em; }\n" +
      "DIV.para6 { margin-left: 6em; text-indent: -1em; }\n" +
      "DIV.para7 { margin-left: 7em; text-indent: -1em; }\n" +
      "DIV.para8 { margin-left: 8em; text-indent: -1em; }\n" +
      "DIV.para9 { margin-left: 9em; text-indent: -1em; }\n" +
      "DIV.para10 { margin-left: 10em; text-indent: -1em; }\n\n" +
      "A.blockquote {  font-weight:normal; color:inherit; text-decoration: none; border-bottom:0px; }\n" +
      "A.highlight {  font-weight:normal; color:inherit; text-decoration: none; border-bottom:0px; }\n" +
      "u.hilt { background-color: rgba(255, 220, 0, 0.6); text-decoration: none; border-radius: 3px;}\n " +
      "u.hilt1 { background-color: rgba(150, 237, 0, 0.6); text-decoration: none; border-radius: 3px;}\n " +
      "u.hilt2 { background-color: rgba(110, 186, 255, 0.6); text-decoration: none; border-radius: 3px;}\n " +
      "u.hilt3 { background-color: rgba(255, 107, 155, 0.6); text-decoration: none; border-radius: 3px;}\n " +
      "u.hilt4 { background-color: rgba(187, 117, 255, 0.6); text-decoration: none; border-radius: 3px;}\n " +
      "u.hilt5 { border-bottom:3px red solid; text-decoration: none; }\n " +
      
      "DIV.blockquote { font-weight:normal;  font-size:94%; margin-bottom: -1.3em; margin-top: 0.2em; margin-left: -1em; padding-left: 5px; padding-right: 5px; padding-top: 2px; padding-bottom: 2px; text-indent: 0em;  background:" + blockquoteColor + "; border: 1px solid " + blockquoteBorder + "; }\n" +
      "-->\n" +
    "</STYLE>"
    
    if ud.bool(forKey: "UseWKWebView_debug") {
      replace = replace + "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" />"
    }
    //+ " <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0\">"
    
    
    if stripCSS {
      replace = "<STYLE type=\"text/css\"></STYLE>"
    }
    
    ///
    
    let openRange: NSRange = range(of: openTag, options: .caseInsensitive)
    let closeRange: NSRange = range(of: closeTag, options: .caseInsensitive)
    
    var targetRange: NSRange
    
    if openRange.location == NSNotFound {
      
      // Insert after <HEAD> tag
      let headRange: NSRange = range(of: "<HEAD>" , options: .caseInsensitive)
      if headRange.location == NSNotFound { return }
      targetRange = NSMakeRange(NSMaxRange(headRange),0)
      
    }
    else {
      targetRange = NSMakeRange(openRange.location, NSMaxRange(closeRange) - openRange.location)
    }
    
    replaceCharacters(in: targetRange, with: replace)
    
    return
  }
  
  func convertHTMLtoIncludeNotes(allHighlights: [DocItem]?, allNotes: [DocItem]?, showTooltip: Bool) {
    
    func lookForHREFandReturnCloseTagRange(_ href: NSString, inHTML: NSMutableString) -> NSRange {
      
      let range: NSRange = inHTML.range(of: href as String, options: .literal)
      
      if range.length == 0 { return range } // Not Found
      let searchRange = NSMakeRange( NSMaxRange(range), inHTML.length - NSMaxRange(range) )
      let closeTagRange = inHTML.range(of: "</a>", options: .caseInsensitive, range: searchRange)
      
      return closeTagRange
    }
    
    
    /*
     <B><A HREF=\"comcatalystwolaws:(1)@1o30x1R+@S35F03801000010.html\" class=\"bkmk\">第１条</A></B>\n
     
     */
    
    
    if allHighlights != nil && allHighlights?.isEmpty == false {
      
      typealias RangeAndSnippet = ( rangeToReplace: NSRange, snippet: String )
      
      var array: [RangeAndSnippet] = []
      
      DispatchQueue.concurrentPerform( iterations: allHighlights!.count) { (i: size_t) in
        
        let dict = allHighlights![i]
        
        if let result = rangeToHighlight(dictionary: dict, showTooltip: showTooltip) {
          
          // THREAD SAFE
          objc_sync_enter(self)
          array.append( result )
          objc_sync_exit(self)
        }
      }
      
      if array.count != allHighlights!.count {
        print("***** allHighlights err \(array.count) : \(allHighlights!.count)")
      }
      
      // Sort array
      
      array.sort { $0.rangeToReplace.location > $1.rangeToReplace.location }
      array.forEach { replaceCharacters(in: $0.rangeToReplace, with: $0.snippet) }
    }
    
    ///
    
    /* ORIGINAL CODES
     
     if allNotes != nil && allNotes?.isEmpty == false {
     
     for dict in allNotes! {
     
     let href = dict.href
     let text = dict.notes
     var uuid = dict.uuid
     
     if uuid == nil { uuid = "x" }
     
     if href == nil || href?.isEmpty == true || text == nil { continue }
     
     let range:NSRange = lookForHREFandReturnCloseTagRange(href!, inHTML: &mutableHTMLString )
     
     if range.length == 0 { continue }
     
     
     let builtBlockquote = buildBlockQuote(text! as String, href: href, uuid: uuid)
     
     mutableHTMLString.insertString(builtBlockquote, atIndex:NSMaxRange(range))
     }
     
     }
     */
    
    // DISPATCH_CODE
    
    if allNotes != nil && allNotes?.isEmpty == false {
      
      typealias Replacement = (range: NSRange, text: String, href: String?, uuid: String, blockquote: String)
      var array: [Replacement] = []
      
      DispatchQueue.concurrentPerform( iterations: allNotes!.count) { (i: size_t) in
        
        let dict = allNotes![i]
        
        let href = dict.href
        let text = dict.notes
        var uuid = dict.uuid
        
        if uuid == nil { uuid = "x" }
        if href == nil || href?.isEmpty == true || text == nil { return }
        
        let range: NSRange = lookForHREFandReturnCloseTagRange(href! as NSString, inHTML: self )
        if range.length == 0 { return }
        
        let builtBlockquote = text!.buildBlockQuote(href: href, uuid: uuid)
        let tuple: Replacement = (range, text! as String, href, uuid!, builtBlockquote)
        
        
        // THREAD SAFE
        objc_sync_enter(self)
        array.append( tuple )
        objc_sync_exit(self)
        
        //mutableHTMLString.insertString(builtBlockquote, atIndex: NSMaxRange(range))
      }
      
      if array.count != allNotes!.count {
        print("***** allNotes err \(array.count) : \(allNotes!.count)")
      }
      
      // Sort array
      array.sort { $0.range.location > $1.range.location }
      array.forEach { insert($0.blockquote, at: NSMaxRange($0.range)) }
    }
  }
}

//func detectDateIn( string:String ) -> NSDate? {
//	/*
//	法法法平成二五年一二月一一日法法法
//	 -> NSDate
//	*/
//	
//	let regexFormat = "(平成|昭和|大正|明治)[一二三四五六七八九〇十]+年[一二三四五六七八九〇十]+月[一二三四五六七八九〇十]+日"
//	
//	let range:NSRange = (string as NSString).rangeOfString(regexFormat, options: [.RegularExpressionSearch, .CaseInsensitiveSearch])
//
//	if range.location == Int(Foundation.NSNotFound) { return nil }
//
//	let japaneseDateString = (string as NSString).substringWithRange(range)
//
//	
//	let dateFormatter = NSDateFormatter()
//	let locale = NSLocale(localeIdentifier: "ja_JP")
//	dateFormatter.locale = locale
//	dateFormatter.calendar = NSCalendar(identifier: NSCalendarIdentifierJapanese)
//	dateFormatter.lenient = true
//	dateFormatter.dateStyle = NSDateFormatterStyle.LongStyle
//	dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
//
//	let date = dateFormatter.dateFromString( "平成二五年11月27日" )
//	
//	print("\(japaneseDateString) -> \(date)")
//	return nil
//}


//func isInsideTag( range:NSRange, inout htmlString:NSMutableString ) -> (isBTag:Bool, isATag: Bool ) {
//	
//	// 同じ段落中に
//	// < /a> </b>
//	
//	let ss = htmlString.substringWithRange(range)
//	
//	
//	var loc = NSMaxRange(range)
//	
//	for  ; loc < htmlString.length ; loc++
//	{
//		let mojiRange = NSMakeRange(loc, 1)
//		let moji:String = htmlString.substringWithRange(mojiRange)
//		
//		
//		if moji == "\n" { continue }
//		if moji != "<" { continue }
//		
//		let tag:String = htmlString.substringWithRange( NSMakeRange(loc+1, 2))
//		
//		if( tag.uppercaseString == "/A" ) { return (false, true) }
//		if( tag.uppercaseString == "/B" ) { return (true, false) }
//		
//		// others such as /DIV
//		return (false, false)
//	}
//	
//	
//	return (false, false)
//}


/*
func isInsideTagSwiftString( _ range:Range<String.Index>, htmlString:String ) -> (isBTag:Bool, isATag: Bool ) {
	
	// 同じ段落中に
	// < /a> </b>
	
	let returnUnichar = "\n"
	let parenthesisUnichar = "<"
	let slashUnichar = "/"
	
	for loc in range.endIndex ..< htmlString.characters.index(htmlString.endIndex, offsetBy: -3) {
		let theUnichar = String( htmlString.characters[ loc ] )
		
		if theUnichar == returnUnichar { continue }
		if theUnichar != parenthesisUnichar { continue }
		
		//		if moji == "\n" { continue }
		//		if moji != "<" { continue }
		
		let tagFirst = String( htmlString.characters[ htmlString.characters.index(loc, offsetBy: 1) ] )
		
		if tagFirst != slashUnichar {
			// others such as <A etc
			return (false, false)
		}
		
		let tag = htmlString.substring( with: htmlString.characters.index(loc, offsetBy: 1) ..< htmlString.characters.index(loc, offsetBy: 3))
		
		if( tag.uppercased() == "/A" ) { return (false, true) }
		if( tag.uppercased() == "/B" ) { return (true, false) }
		
		// others such as /DIV
		return (false, false)
	}
	
	return (false, false)
}
*/






/*
func appendParenthesisTagSwiftString( _ htmlString: NSMutableString ) -> Void {
	// Swift Version... THIS IS WAY SLOW
	
	debugPrint("appendParenthesisTag")
	
	var swiftString = htmlString as String
	
	var findRange:CountableRange<String.Index> = swiftString.characters.startIndex ..< swiftString.characters.endIndex
	
	let roundParenthesisOpen = "（"
	let roundParenthesisClose = "）"
	
	let t0 = Date().timeIntervalSinceReferenceDate
	
	while true {
		var finish = false
		autoreleasepool {
			// 1. get (
			guard let range = swiftString.range(of: "（", options: [.literal],  range:findRange)
				else {
					finish = true; return
			}
			// 2. End
			if range.upperBound >= swiftString.endIndex { finish = true; return }
			
			// 3. Skip if in B tag
			
			let (isBTag, _ /*isATag*/ ): (Bool, Bool) = isInsideTagSwiftString(range, htmlString: swiftString )
			if true == isBTag {
				
				findRange = range.upperBound ..< swiftString.endIndex
				
				return
			}
			
			// 4. Look for closing ）
			
			var pcount = 1
			var loc:String.Index? = range.upperBound
			
			let endIndex = swiftString.endIndex
			
			while true {
				if loc >= endIndex { loc = nil; break }
				
				let mojiUnichar = String(swiftString.characters[loc!])
				
				if mojiUnichar == roundParenthesisOpen { pcount += 1 }
				else if mojiUnichar == roundParenthesisClose { pcount -= 1 }
				
				if pcount == 0 { break }
				
				loc = swiftString.characters.index(loc!, offsetBy: 1)
			}
			
			
			// 5. Build String with tag
			if loc == nil {
				
				findRange = range.upperBound ..<  swiftString.endIndex
				return
			}
			
			
			let theRange = range.lowerBound ... loc!
			
			// 5-1. Only one letter?
			
			if theRange.count == 3 || theRange.endIndex > swiftString.endIndex {
				
				findRange = range.upperBound ..<  swiftString.endIndex
				return
			}
			
			let pstring = swiftString.substring(with: theRange)
			
			// 5-2. Only numbers inside the string?
			let contentRange = theRange.index(theRange.startIndex, offsetBy: 1) ..< theRange.index(theRange.endIndex, offsetBy: -1)
			let content = swiftString.substring(with: contentRange)
			
			if Int(content) != nil {
				
				findRange = range.upperBound ..<  swiftString.endIndex
				return
			}
			
			let replacedString = "<SPAN class=\"brckt\">" + pstring + "</SPAN>"
			
			// 6. Replace
			

			swiftString.replaceSubrange(theRange, with: replacedString)
			
			
			findRange = theRange.index(theRange.startIndex, offsetBy: replacedString.characters.count) ..< swiftString.endIndex
			print("\(findRange)")
		}
		
		if finish { break }
	}
	let t1 = Date().timeIntervalSinceReferenceDate
	debugPrint("appendParenthesisTag done \(t1-t0)")
}
*/



/// MARK:- Convert HTML to includes notes


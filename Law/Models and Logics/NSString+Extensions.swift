//
//  NSString+Extension.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 22/09/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import Foundation

extension NSString  {
	
	// Compare with aString ingoring self's html tags.
	// 'self' is SHORT
	
	func numberOfMatchesInHtml( _ aString:NSString! ) -> Int {
		
		if aString == nil { return 0 }
		
		let trimmedHTML = self.replacingOccurrences(of: "<[^<>]*>|\n", with: "", options: [.caseInsensitive, .regularExpression], range: NSMakeRange( 0, self.length)  )
		
		let regex: NSRegularExpression?
		do {
			regex = try NSRegularExpression(pattern: NSRegularExpression.escapedPattern(for: aString as String), options: .caseInsensitive)
		} catch _ {
			regex = nil
		}
		
		if regex == nil { return 0 }
		
		let theRange:NSRange = NSMakeRange(0, (trimmedHTML as NSString!).length )
		
		let numberOfMatches = regex!.numberOfMatches( in: trimmedHTML, options:.reportCompletion, range:theRange )
		
		return numberOfMatches
	}
	
	// Compare with aString ingoring self's html tags.
	// 'self' could be very LONG
	
	func rangeOfStringInHtmlUntilANameTag( _ aString:NSString!, inRange range: NSRange ) -> NSRange {
		
		var locInSelf:Int = range.location
		var locInSelfStartLocation:Int = range.location
		var locInString:Int = 0
		
		// Check range
		if locInSelf >= self.length { return NSMakeRange(NSNotFound,0)}
		
		// Look begins
		
		while true {
			
			// SKIP TAG loop
			while true {
				
				let theRange:NSRange = NSMakeRange(locInSelf, self.length - locInSelf )
				
				if self.length <= locInSelf {
					return NSMakeRange(NSNotFound, 0)
				}
				
				let aRange:NSRange = self.range(of: "<[^<>]*>|\n", options: [.regularExpression, .anchored], range: theRange)
				
				if aRange.length == 0 { break }
				
				// Skip tag
				
				if locInSelf == locInSelfStartLocation {
					locInSelf = NSMaxRange(aRange)
					locInSelfStartLocation = NSMaxRange(aRange)
					
				}else {
					locInSelf = NSMaxRange(aRange)
				}
				
				if locInSelf >= NSMaxRange(range) { return NSMakeRange(NSNotFound, 0) }
			}
			
			/// compare one letters in locInSelf and locInString
			
			let mojiInSelf:String = self.substring( with: NSMakeRange(locInSelf, 1) )
			let mojiInString:String = aString.substring( with: NSMakeRange(locInString, 1) )
			
			// If matches, increase locInSelf and locInString,
			// If locInString reaches to the end, return succuss
			// If locInSelf reaches to the boarder before locInString reaches to the end, return fail
			
			if mojiInSelf == mojiInString {
				
				locInString = locInString + 1
				
				// Successfully found
				if locInString == aString.length {
					return NSMakeRange( locInSelfStartLocation, locInSelf - locInSelfStartLocation + 1)
				}
				
				// Step
				locInSelf = locInSelf + 1
				
				// Not found
				if locInSelf >= NSMaxRange(range) {
					return NSMakeRange(NSNotFound, 0)
				}
				
			}else {
				
				// If failes increase locInSelf, reset locInString
				// If locInSelf reaches to the boarder before locInString reaches to the end, return fail
				
				locInString = 0
				locInSelfStartLocation = locInSelfStartLocation + 1
				locInSelf = locInSelfStartLocation
				
				// Not found
				if locInSelf >= NSMaxRange(range) {
					return NSMakeRange(NSNotFound, 0)
				}
			}
		}
	}
	
	func rangeOfHTMLSnipped(_ snippet:NSString, range r:NSRange? = nil) -> NSRange?{
		// ignoring case, ignoring /n
		
		var startSelfIndex = 0
		let searchRange = r ?? NSMakeRange(0, self.length)
		let returnChar = "\n"
		
		selfLoop: for selfIndex in searchRange.location ..< NSMaxRange(searchRange) {
			
			startSelfIndex = selfIndex
			if self.substring(with: NSMakeRange( selfIndex, 1 )) == returnChar { continue }
			
			var endSelfIndex = selfIndex
			var advance = 0
			
			snippetLoop: for snippetIndex in 0 ..< snippet.length {
				let char0 = snippet.substring(with: NSMakeRange( snippetIndex, 1 ))
				if char0 == returnChar { continue }
				
				while true {
					endSelfIndex = startSelfIndex + advance
					advance += 1
					
					if endSelfIndex >= self.length {
						// END
						return nil
					}
					
					let char1 = self.substring(with: NSMakeRange( endSelfIndex, 1 ))
					if char1 == returnChar { continue }
					
					// COMPARE
					
					if char1.lowercased() == char0.lowercased() {
						continue snippetLoop
					}else {
						continue selfLoop //DID NOT MATCH
					}
				}
			}
			
			// MATCH
			
			return NSMakeRange( startSelfIndex, endSelfIndex - startSelfIndex + 1 )
		}
		
		return nil
	}
}

extension String {
	
	func toZenkaku() -> String {
		
		let thisString = self
		var returnValue = ""
		for character in thisString.characters
		{
			var kanji:String = ""
			switch character{
			case "0" : kanji = "０"
			case "1" : kanji = "１"
			case "2" : kanji = "２"
			case "3" : kanji = "３"
			case "4" : kanji = "４"
			case "5" : kanji = "５"
			case "6" : kanji = "６"
			case "7" : kanji = "７"
			case "8" : kanji = "８"
			case "9" : kanji = "９"
			default: kanji = String( character )
			}
			returnValue += kanji
		}
		return returnValue
	}
	
	func removingParenthesises() -> String {
		var theString = self
		if theString.hasPrefix("（") {
			theString = theString.substring(from: theString.index(theString.startIndex, offsetBy: 1))
		}
		if theString.hasSuffix("）") {
			theString = theString.substring(to: theString.index(theString.endIndex, offsetBy: -1))
		}
		return theString
	}
	
	/*
	func rangeOfHTMLSnipped(_ snippet:String, range r:Range<String.Index>? = nil) -> Range<String.Index>? {
		// ignoring case, ignoring /n
		
		var startSelfIndex = self.startIndex
		let searchRange:Range<String.Index> = r ?? self.startIndex ..< self.endIndex
		let lowerSnippet = snippet.lowercased()
		let returnChar:Character = "\n"
		
		selfLoop: for selfIndex in searchRange {
			
			startSelfIndex = selfIndex
			if self[selfIndex] == returnChar { continue }
			
			var endSelfIndex = selfIndex
			var advance = 0
			
			snippetLoop: for snippetIndex in lowerSnippet.characters.indices {
				
				let char0 = lowerSnippet[snippetIndex]
				if char0 == returnChar { continue }
				
				while true {
					endSelfIndex = characters.index(startSelfIndex, offsetBy: advance)
					advance += 1

					if endSelfIndex >= self.endIndex {
						// END
						return nil
					}
					
					let char1 = self[endSelfIndex]
					if char1 == returnChar { continue }
					
					// COMPARE
					
					if String(char1).lowercased() == String(char0) {
						continue snippetLoop
					}else {
						continue selfLoop //DID NOT MATCH
					}
				}
			}
			
			// MATCH
			
			return Range<String.Index>( startSelfIndex ... endSelfIndex )
		}
	
		return nil
	}
	*/
}

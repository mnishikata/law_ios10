//
//  DownloadManager.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2014/09/13.
//  Copyright (c) 2014年 Catalystwo. All rights reserved.
//

#if os(OSX)
	import Cocoa
	#elseif os(iOS)
	import UIKit
#endif

/*
let kMZDownloadKeyURL = "URL"  as NSString
let kMZDownloadKeyStartTime = "startTime"  as NSString
let kMZDownloadKeyFileName = "fileName"  as NSString
let kMZDownloadKeyProgress = "progress" as NSString
let kMZDownloadKeyTask = "downloadTask" as NSString
let kMZDownloadKeyStatus = "requestStatus" as NSString
let kMZDownloadKeyDetails = "downloadDetails" as NSString
let kMZDownloadKeyResumeData = "resumedata" as NSString
let RequestStatusDownloading = "RequestStatusDownloading" as NSString
let RequestStatusPaused = "RequestStatusPaused" as NSString
let RequestStatusFailed = "RequestStatusFailed" as NSString
*/

class DownloadInfo {
  
  static let downloadKeyURL = "URL"
  static let downloadKeyFileName = "fileName"
  static let RequestStatusDownloading = "RequestStatusDownloading"
  static let RequestStatusPaused = "RequestStatusPaused"
  static let RequestStatusFailed = "RequestStatusFailed"

	var URL: URL
	var name: String
	var date: Date
	var status: String
	var task: URLSessionTask?
	var data: Data?
	var progressHandler: (_ progress: Double) -> Void
	var completionHander: (_ success: Bool, _ data: Data?, _ error: Error?) -> Void
	var progress: Double? {
		didSet {
      if progress != nil {
        self.progressHandler( progress! )
      }
    }
  }
  var taskDescription: String {
    let dict: [String: Any] = [DownloadInfo.downloadKeyURL: URL.path,
                                    DownloadInfo.downloadKeyFileName: name]
    
    let jsonData: Data?
    do {
      jsonData = try JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
    } catch _ {
      jsonData = nil
    }
    let jsonString = NSString(data: jsonData!, encoding: String.Encoding.utf8.rawValue)
    return jsonString! as String
  }
  
	init(URL: URL, name: String, task: URLSessionTask, progressHandler: @escaping (_ progress: Double)->Void, completionHander: @escaping (_ success: Bool, _ data: Data?, _ error: Error?)->Void) {
		self.URL = URL
		self.name = name
		self.date = Date()
		self.status = DownloadInfo.RequestStatusDownloading
		self.task = task
		self.completionHander = completionHander
		self.progressHandler = progressHandler
	}
}


class DownloadManager: NSObject, URLSessionDelegate, URLSessionDownloadDelegate {
	
	var downloadingArray: [DownloadInfo] = []

	// For NSURLConnection
	var receivedData: NSMutableData? = nil
	var receivedDataCompletion: ((_ data: NSMutableData) -> Void)? = nil

	static var sharedManager: DownloadManager = {
		
		let manager = DownloadManager()
		URLCache.shared.removeAllCachedResponses()
		return manager
	}()
	
	lazy var backgroundSession: Foundation.URLSession = {
		
		let configuration = URLSessionConfiguration.default//("com.catalystwo.BackgroundSession")
		configuration.allowsCellularAccess = true
		configuration.networkServiceType = .default
		configuration.httpShouldUsePipelining = true
		let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
		return session
	}()
	
	func addDownloadTask(_ name: String, URL: URL, progressHandler: @escaping (_ progress: Double)->Void, completionHandler: @escaping (_ success: Bool, _ data: Data?, _ error: Error?)->Void) -> DownloadInfo {
		
		print("Add Download Task \(URL)")
		let request = URLRequest(url: URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
		let downloadTask = backgroundSession.downloadTask(with: request)
		
		downloadTask.resume()
		
		let downloadInfo = DownloadInfo(URL: URL, name: name, task: downloadTask, progressHandler: progressHandler, completionHander: completionHandler)

		downloadTask.taskDescription = downloadInfo.taskDescription
				
		// Download started
		downloadingArray.append(downloadInfo)
		
		return downloadInfo
	}
		
	
	//MARK:-  NSURLSession Delegates
	
	func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
		
		for info: DownloadInfo in downloadingArray {
			
			if downloadTask !==  info.task { continue }
			
			let progress = Double(downloadTask.countOfBytesReceived)/Double(downloadTask.countOfBytesExpectedToReceive)
			info.progress = progress

			break
		}
	}
	
	func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
		
		for info in downloadingArray {
			if downloadTask !==  info.task { continue }
			
			// TELL DELEGATE
			// Finish
			
			print("Finish @ \(location)")
			let theData: Data?
			do {
				theData = try Data(contentsOf: location, options: .uncached)
			} catch _ {
				theData = nil
			}

			info.data = theData

			break
		}
	}
	
	func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
		
		var errorReasonNum: Int? = nil
    let nserror = error as? NSError
    
		if let userInfo = nserror?.userInfo  {
			if let reason = userInfo["NSURLErrorBackgroundTaskCancelledReasonKey"] as? String {
				errorReasonNum = Int(reason)
			}
		}
		//TODO: HANDLE ERROR
		if errorReasonNum != nil {
			
		}
		
		
		for info in downloadingArray {
			
			if info.task !== task { continue }
			
			var success = true
			
			if nserror != nil { success = false }
			info.completionHander( success, info.data, error )

			if error != nil {
				if nserror!.code != NSURLErrorCancelled {
										
					info.status = DownloadInfo.RequestStatusFailed

					
					// TELL DELEGATE
					// Error
					print("Error @ \(error)")

				}
				
			}else {
				
				// Complete
				// TELL DELEGATE
				
				print("Complete @ \(info)")
			}
			
			break
		}
	}
	
	
	func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
	}
	
	//MARK:- 2K loading using NSURLConnection
	
	func download2KAt(_ URL: URL, completion: @escaping (_ data: NSMutableData) -> Void) {
		
		let request = URLRequest(url: URL, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 60.0)
		receivedData = NSMutableData()
		let _ = NSURLConnection(request: request, delegate: self, startImmediately: true)
		
		receivedDataCompletion = completion
	}
	
	func connection( _ connection: NSURLConnection, didReceiveResponse response: URLResponse) {
	}
	
	func connection( _ connection: NSURLConnection,
	                 didReceiveData data: Data) {
		
		receivedData?.append(data)
		
		if receivedData != nil && receivedData!.length > 2000 {
			
			receivedDataCompletion?(receivedData!)
			receivedDataCompletion = nil
			
			connection.cancel()
		}
	}
	
	func connectionDidFinishLoading( _ connection: NSURLConnection) {
		
		receivedDataCompletion?(receivedData!)
		receivedDataCompletion = nil
	}
}

//
//  FilePackage+Saving.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 24/04/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import Foundation
import CloudKit

extension FilePackage {

	func savePackage(name: NSString!, plist: DocProperty?, indexHtml: NSString?, contentHtml: NSString? ) -> Bool {
		
		var myPlist = DocProperty()
		
		if plist != nil { myPlist = plist! }
		
		var indexData: Data? = nil
		
		// Build dict xml
		
		if indexHtml != nil {
			let linkDict = indexHtml!.getLinkDictionaries()
      let dictionaryReps = linkDict.map { $0.dictionaryRepresentation }
			
			do {
				indexData = try PropertyListSerialization.data(fromPropertyList: dictionaryReps, format: .binary, options: 0)
			} catch {
				// Unknown Error
				return false
			}
		}
		
		
		// Calc MD5 and set
		
		let md5 = contentHtml!.md5()
		
		myPlist[.md5] = md5!
		
		let packageName = name.appendingPathExtension(FilePackage.packageExtension)
		let packageURL = FileRepository.shared.documentFolderURL.appendingPathComponent(packageName!)
		
		let coordinator = NSFileCoordinator()
		var success: Bool! = false
		
		coordinator.coordinate(writingItemAt: packageURL, options: .forReplacing, error: nil) { (newURL: URL) -> Void in
			
			while true {
				
				let fm = FileManager.default
				
				if fm.fileExists(atPath: newURL.path) == false {
					let success: Bool
					do {
						try fm.createDirectory(at: newURL, withIntermediateDirectories: true, attributes: nil)
						success = true
					} catch _ {
						success = false
					}
					if success == false { break }
				}
				
				
				let data: Data?
				do {
					data = try PropertyListSerialization.data( fromPropertyList: myPlist, format: .xml, options: 0)
				} catch _ {
					data = nil
				}
				
				let theURL = newURL.appendingPathComponent(FilePackage.Filename.infoPlist)
				if data != nil {
					do {
						try data!.write(to: theURL, options: [.atomic])
					} catch {
						break
					}
				}
				
				if contentHtml != nil {
					
					let data = (contentHtml?.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false) as NSData?)?.deflate()
					let theURL = newURL.appendingPathComponent(FilePackage.Filename.content)
					
					do {
						try data?.write(to: theURL, options: [.atomic])
					}catch {
						break
					}
				}
				
				if indexData != nil {
					
					let theURL = newURL.appendingPathComponent(FilePackage.Filename.index)
					
					do {
						try indexData?.write(to: theURL, options: [.atomic])
					}catch {
						break
					}
				}
				
				// Successfully wrote!
				success = true
				break
			}
		}
		
		if success == false { return false }
		
		
		/*
		
		Update index
		
		*/
		
		// Delete update flag
		resetUpdates( packageURL )
		let _ = packageURL.setExcludedFromBackup()
		
		let fileEntry = FileEntry( packageURL: packageURL, anchor: nil, elementId: nil, plist: myPlist)
		addToIndex( fileEntry )
		
		
		// BUILD TOC2
		
		let _ = FilePackage.shared.buildHTMLElements(contentHtml!, for: packageURL, plist: myPlist, linkDictionaries: nil, overwriteMode: true )

		
		
		/* Cloud Kit
		
		check md5 and myPlist.title
		
		If it does not exists on CloudKit yet, upload it.
		
		*/
		
    CloudKitDomain.shared.cloudKit(md5!, title: plist![RecordType.LawDocPackageKey.title] as! String, edition: plist![RecordType.LawDocPackageKey.edition] as! String, packageURL: packageURL) { (success: Bool) -> Void in
      
      if success == false { return }
      
      let dict = self.documents.lookForWithCondition {
        $0.isLawEntity && $0.packageName == packageName
      }
      
      if dict != nil {
        if dict!.cloud == .none {
          dict!.cloud = .InCloud
          
          self.saveIndex( saveToICloud: true )
        }
        
      }else {
        print("unknown error")
      }
    }
    
		return true
  }
  
  func recover(from url: URL) {
    FileRepository.shared.recover(from: url)
  }
  
  func installSampleFiles() {
    FileRepository.shared.installSampleFiles()
  }
  
  var allBackupFiles: [URL] {
    return FileRepository.shared.allBackupFiles()
  }
  
  func keepBackupForFileAt() {
    FileRepository.shared.keepBackupForFileAt(theURLOrMyDocumentWhenNull: nil, forceBackup: true)

  }
  
	func addToIndex( _ file: FileEntry ) {
		if Thread.isMainThread == true {
			addToIndexMain( file )
		}else {
			DispatchQueue.main.async {
				self.addToIndexMain( file )
			}
		}
	}
	
	private func addToIndexMain( _ file: FileEntry ) {
		assert( Thread.isMainThread == true, "** CALL ME ON MAIN THREAD! ** ")
		
		let packageURL = file.packageURL
		let plist = file.plist
		let md5 = plist[DocPropertyKey.md5] as? NSString
		let title = plist[DocPropertyKey.title] as? String
		let sourceURL = plist[DocPropertyKey.sourceURL] as? String
		
		let lastPathComponent = file.packageURL.lastPathComponent
    
    // If file is already added to index, update md5 then return
    let existingDict = documents.lookForWithCondition {
      $0.isLawEntity == true && $0.packageName! == lastPathComponent
    }
    
    if existingDict != nil {
      let existingMD5 = existingDict!.md5
      
      if existingMD5 == nil || md5?.isEqual( to: existingMD5 as String! ) != true {
        
        existingDict!.md5 = md5 as? String
        saveIndex( saveToICloud: true )
      }
      sendIndexToWatch()
      return
    }
    
    //	If not added yet
		
		let theItem = DocItem( lawDocumentWithTitle: title, packageName: packageURL.lastPathComponent, sourceURLString: sourceURL, md5: md5 as String?)
		documents.addItem(theItem)
		
		saveIndex( saveToICloud: true )
		
		// Notify
		bookmarkingDidFinishHandler?()
	}
	
  
  //MARK:- Built TOC2
  
  func updateOverwriting(_ overwrite:Bool, progress:@escaping (_ count: Int, _ startCount: Int)->Void, completion: @escaping ()->Void ) {
    var contents:[URL] = []
    do {
      contents = try FileManager.default.contentsOfDirectory(at: FileRepository.shared.documentFolderURL,
                                                             includingPropertiesForKeys: nil, options: [.skipsHiddenFiles])
    } catch _ {
    }
    
    
    var count = contents.count
    let startCount = contents.count
    
    processQueue_.async {
      
      DispatchQueue.concurrentPerform( iterations: contents.count) { (i:size_t) in
        
        let url = contents[i]
        
        autoreleasepool {
          if url.pathExtension == FilePackage.packageExtension {
            print("Building toc for \(url.lastPathComponent)")
            
            let (plist,linkDictionaries,rawHTML) = self.loadPackage(at: url, onlyPlist:false, rawHTML:true)
            
            self.buildHTMLElements(rawHTML!, for:url, plist:plist, linkDictionaries:linkDictionaries!, overwriteMode: overwrite )
          }
          
          DispatchQueue.main.async {
            progress(count, startCount)
            //self.label.text = "\(startCount - count) / \(startCount)"
            count -= 1
          }
        }
      }
      
      DispatchQueue.main.async {
        completion()
        /*
         self.indicator.stopAnimating()
         self.label.text = "終わりました （" + String(
         Int(Date.timeIntervalSinceReferenceDate - BuildingTOC2ViewController.startTime ) ) + "秒）"
         
         BuildingTOC2ViewController.buildingTOC2 = false
         
         UserDefaults.standard.set(true, forKey:"UpdatedTOC2ForVersion3")
         UserDefaults.standard.synchronize()
         
         self.updateButton.isHidden = false
         self.updateButton.removeTarget(self, action: nil, for: .touchUpInside)
         self.updateButton.addTarget(self, action: #selector(self.close(_:)), for: .touchUpInside)
         self.updateButton.setTitle(WLoc("Start Using"), for: .normal)
         */
      }
    }
  }
  
	func buildHTMLElements(_ source: NSString, for packageURL: URL, plist: DocProperty?, linkDictionaries: [LinkDictionary]?, overwriteMode: Bool = false) {
		// CALL THIS METHOD IN BACKGROUND

    var theSource = source.replacingOccurrences(of: "\n", with: "") as NSString
		theSource = theSource.replacingOccurrences(of: "</br>", with: "<br>") as NSString
		
		let ud = UserDefaults.standard
		let failedKey = "TOC2_" + packageURL.lastPathComponent
		
		let urlToWrite = packageURL.appendingPathComponent(FilePackage.Filename.htmlElements)
		
		// DEBUG
		
		if overwriteMode == false {
			if FileManager.default.fileExists(atPath: urlToWrite.path) {
				return
			}
			
			if ud.bool(forKey: failedKey) == true {
				// THERE WAS A PROBLEM IN THE PAST
				let reportedKey = "TOC2_REPORTED_" + packageURL.lastPathComponent
				
				if ud.bool(forKey: reportedKey) == true {
					return
				}
				// REPORT HERE
				let errorReason = ud.object(forKey: failedKey + "_Error") as? String ?? " n/a"
				let string = (( plist?[.title] ?? packageURL.lastPathComponent ) as! String) + errorReason
				
				ud.set(true, forKey: reportedKey)
				ud.synchronize()
				
				CloudKitRepository.shared.report(string)
				
				//TODO: Flag needs to be reset at some stage
				return
			}
		}else {
			debugPrint("** overwriteMode **")
		}
		
		
		ud.set(true, forKey: failedKey)
		ud.removeObject(forKey: failedKey + "_Error")

		ud.synchronize()
		print("buildHTMLElements")
		
    if let array = ElementDescriptor.HTMLElements(in: theSource) {
//			let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
//			dispatch_apply( 2, queue)  { (n: size_t) in
//				
//				if n == 0 {
//					for i in 0 ..< array.count {
//						let elementObj = array[i]
//						
//						if !(elementObj is ElementDescriptor) { break } //END
//						let dict = (elementObj as! ElementDescriptor).dictionaryRepresentation()
//						objc_sync_enter(array)
//						array[i] = dict
//						objc_sync_exit(array)
//
//					}
//				}else {
//					
//					for i in (0 ..< array.count).reverse() {
//						let elementObj = array[i]
//						
//						if !(elementObj is ElementDescriptor) { break } //END
//						let dict = (elementObj as! ElementDescriptor).dictionaryRepresentation()
//						objc_sync_enter(array)
//						array[i] = dict
//						objc_sync_exit(array)
//
//					}
//				}
			//			}
			
      let dictionaryReps = array.map { $0.dictionaryRepresentation }
			print("buildHTMLElements compare")
			
			/*
			raw plist 507KB
			json 200KB
			json compressed 28KB
			XML compressed 34KB
			
			*/
			do {
				if let data = try (JSONSerialization.data(withJSONObject: dictionaryReps, options: []) as NSData).deflate() {
					
					try data.write(to: urlToWrite, options: .atomicWrite)
					
					ud.set(false, forKey: failedKey)
					ud.synchronize()
					print("buildHTMLElements done")
					
					
					// ***OPTIONAL: COMPARE WITH LINK DICTIONARY ( toc1 and toc2 )
					// ONLY FOR E-GOV LAWS
//
//					if linkDictionaries != nil {
//
//					var userDefined = false
//					if let sourcePath = plist!["sourceURL"] as? String {
//						userDefined = sourcePath.hasPrefix("user:") == true
//					}
//					
//					
//					var startIdex = 0
//					var toc2: [ElementDescriptor]? = nil
//					
//					toc2 = FilePackage.shared.loadHTMLElements(forPackage:packageURL)
//					if toc2 == nil { return }//UNKNOWN ERR
//					
//					toc1Loop: for link in linkDictionaries! {
//						var str:String
//						
//						if userDefined {
//							str = link.numerical
//						}else {
//							let num = link.numerical.stringByReplacingOccurrencesOfString(".", withString: "-")
//							
//							str = num.stringByReplacingOccurrencesOfString("^[0-9]*", withString: "($0)", options: .RegularExpressionSearch, range:(num.startIndex..<num.endIndex))
//							
//							if str.hasPrefix("()") { continue }
//						}
//						str = encodeArticleTitle(str)
//						
//						// look for anchor in toc2
//						toc2Loop: for hoge in startIdex..<toc2!.count {
//							//TODO:Speed
//							let element = toc2![hoge]
//							
//							if userDefined {
//								if let _ = element.anchorForPathOfUserDefinedLaw(str)  {
//									startIdex = hoge
//									continue toc1Loop
//								}
//							}else {
//								if let _ = element.anchorForPath(str)  {
//									startIdex = hoge
//									continue toc1Loop
//								}
//							}
//						}
//						
//						let title = plist?.title ?? packageURL.lastPathComponent
//						let reportString = "*  [ \(str) ] in \(title!) was not found"
//						//if gDebugMode { debugPrint(reportString) }
//						
//						if userDefined == false { // REPORT ONLY FOR E-GOV LAW
//							dispatch_async(dispatch_get_main_queue() ) {
//								CloudKitRepository.sharedManager().report(reportString)
//							}
//						}
//					}
//					}
				}
				
			}catch let error as NSError {
				// TODO: HANDLE WRITE ERROR ...
				//  POTENTIAL REASON
				//  No storage space
				print("buildHTMLElements exception \(error)")
				ud.set(error.localizedDescription, forKey: failedKey + "_Error")

			}catch _ {
			}
		}
		
	}
	
	//MARK:- Check Updates

	func checkUpdates( _ packageURL: URL, completion: @escaping ((_ success: Bool, _ updateStatus: LawUpdateStatus, _ edition: String?) -> Void) ) {
		
		if GCNetworkReachability.forInternetConnection().isReachable() == false {
			
			completion(false, .unknown, nil)
			return
		}
		
		// pkg "S23HO131" ... http://law.e-gov.go.jp/htmldata/S23/S23HO131.html
		
		// Check update info
		
		let infoURL = packageURL.appendingPathComponent(FilePackage.Filename.updateInfo)
		
		if let data = try? Data(contentsOf: URL(fileURLWithPath: infoURL.path)) {
			
			do {
        let plist = try PropertyListSerialization.propertyList(from: data, options: [], format: nil) as! [String:AnyObject]
				
				let date = plist[FilePackage.UpdateInfo.lastChecked]  as? Date
				let editionOnline = plist[FilePackage.UpdateInfo.edition] as? String
				
				let savedStatus = LawUpdateStatus( rawValue:  (plist[FilePackage.UpdateInfo.status] as? Int) ?? 0  )
				
				if savedStatus == .hasUpdate {
					// Already flagged
					completion(true, .hasUpdate, editionOnline)
					return
				}
				
				if savedStatus == .removed {
					// Already flagged
					completion(true, .removed, nil)
					return
				}
				
				// Check every 5 days
				if savedStatus == .upToDate && date != nil {
					if Date.timeIntervalSinceReferenceDate - date!.timeIntervalSinceReferenceDate < 5*24*60*60  {
					// Recently checked
					
					completion(true, .upToDate, nil)
					return
					}
				}
			} catch {
			}
		}
		
		// 1. User aded CKRecord?
		
		
		// 2.
		// Download html with max 2Kb
		// check edition
		//
		// -> Download from eGov
		
		
    let ( plist, _, _) = FilePackage.shared.loadPackage(at: packageURL, onlyPlist: true)
		
		if plist == nil {
			completion(false, .unknown, nil)
			return
		}
		
		let sourceURLString = plist![.sourceURL] as! String
		
		// 1.
		if sourceURLString.hasPrefix( "user:" ) {
			
			let fileUUID = (sourceURLString as NSString).substring(from: 5)
			let md5 = plist![.md5] as! String
			
			CloudKitRepository.shared.fetchRecord(withPackageBundle: fileUUID) {
				(results: [CKRecord], error: Error?) -> Void in
				
				var updatePlist: [String: Any] = [:]
				
				updatePlist[FilePackage.UpdateInfo.lastChecked] = Date()
				
				var requiresUpdate: LawUpdateStatus = .upToDate
				
				if results.count == 0 {
					
					// No related record found
					let _ = try? FileManager.default.removeItem(at: infoURL)
					requiresUpdate = .removed
				}
				
				if results.count > 0 {
					var myRecord: CKRecord? = nil
					for record in results {
						
						let thisMD5 = record.object(forKey: DocPropertyKey.md5) as? String
						if thisMD5 == md5 {
							myRecord = record
							break
						}
					}
					
					if myRecord == nil || results.index(of: myRecord!) != 0 {
						requiresUpdate = .hasUpdate
					}
				}
				
				updatePlist[FilePackage.UpdateInfo.status] = requiresUpdate.rawValue
				
				do {
					let plistData = try PropertyListSerialization.data(fromPropertyList: updatePlist, format: .binary, options: 0)
					
					let _ = try? plistData.write(to: infoURL, options: [])
					completion( true, requiresUpdate, nil )
					return
					
				} catch {
					print("Data received, but failed to create a plist data")
				}
				
				completion( true, requiresUpdate, nil )
				return
			}
			return
		}
		
		
		// 2.
		
		var thisEdited = plist![.edition] as? String ?? ""
		let dict = sourceURLString.getUrlQueryParameters()
		var updatePlist: [String: Any] = [:]
		
		updatePlist["LastUpdateCheck"] = Date()
		
		
		if let name = dict["H_FILE_NAME"] {
			
			/*
			Initial Roppo data didn't include edition string...
			*/
			
			if thisEdited.isEmpty {
				if name == "M29HO089" { thisEdited = "最終改正：平成二五年一二月一一日法律第九四号" }
				if name == "H08HO109" { thisEdited = "最終改正：平成二四年五月八日法律第三〇号" }
				if name == "M32HO048" { thisEdited = "最終改正：平成二六年六月二七日法律第九一号" }
				if name == "M40HO045" { thisEdited = "最終改正：平成二五年一一月二七日法律第八六号" }
				if name == "S23HO131" { thisEdited = "最終改正：平成二六年六月二五日法律第七九号" }
			}
			
			////////////////////
			
			
			let prefix = (name as NSString).substring(to: 3)
			
			let address = "http://law.e-gov.go.jp/htmldata/" + prefix + "/" + name + ".html"
			let URL = Foundation.URL(string:address)
			
			if URL == nil { return } // Unknown error
			
			DownloadManager.sharedManager.download2KAt( URL! ) { (data) -> Void in
				
				print("data \(data.length)")
				
				if data.length > 0  {
					
					var decodingString = NSMutableString(data: data as Data, encoding: String.Encoding.shiftJIS.rawValue)
					
					if decodingString == nil {
						// reduce one letter and try again
						let data2 = Data(bytes: data.bytes, count: data.length - 1)
						decodingString = NSMutableString(data: data2, encoding: String.Encoding.shiftJIS.rawValue)
					}
					
					if decodingString != nil  {
						
						let edited = decodingString!.lawEditionDateString
						var requiresUpdate: LawUpdateStatus = .upToDate
						
						if edited.isEmpty == false  {
							
							if edited != thisEdited {
								requiresUpdate = .hasUpdate
							}else {
								requiresUpdate = .upToDate
							}
						}
						updatePlist[FilePackage.UpdateInfo.status] = requiresUpdate.rawValue
						updatePlist[FilePackage.UpdateInfo.edition] = edited
						
						do {
							
							let plistData = try PropertyListSerialization.data(fromPropertyList: updatePlist, format: .binary, options: 0)
							
							let _ = try? plistData.write(to: infoURL, options: [])
							
							completion( true, requiresUpdate, edited as String )
							return
							
						} catch {
							print("Data received, but failed to create a plist data")
						}
					}else {
						print("Failed to create a string")
					}
				}
				
				do {
					let plistData = try PropertyListSerialization.data(fromPropertyList: updatePlist, format: .binary, options: 0)
					
					let _ = try? plistData.write(to: infoURL, options: [])
					print("Data was empty. Wrote it")
					
				} catch {
					print("Data was empty. and failed to create a plist data")
				}
				
				completion( false, .unknown, nil )
			}
			
			return
			
		}else {
			
			do {
				let plistData = try PropertyListSerialization.data(fromPropertyList: updatePlist, format: .binary, options: 0)
				let _ = try plistData.write(to: infoURL, options: [])
				
			} catch {
			}
			
			completion(false, .unknown, nil)
			return
		}
	}
	
	func resetUpdates( _ packageURL: URL ) {
		// Reset to "up to date" state
		
		let infoURL = packageURL.appendingPathComponent(FilePackage.Filename.updateInfo)
		do { try FileManager.default.removeItem(at: infoURL) } catch {}

	}
	
	func setUpToDate( _ packageURL: URL ) {
		let infoURL = packageURL.appendingPathComponent(FilePackage.Filename.updateInfo)
		
		var updatePlist: [String: Any] = [:]
		
		updatePlist[FilePackage.UpdateInfo.lastChecked] = Date()
		updatePlist[FilePackage.UpdateInfo.status] = LawUpdateStatus.upToDate.rawValue
		
		do {
			let plistData = try PropertyListSerialization.data(fromPropertyList: updatePlist, format: .binary, options: 0)
			try plistData.write(to: infoURL, options: [])
			
		} catch {
		}
	}

}

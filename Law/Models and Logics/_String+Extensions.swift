//
//  String+Extensions.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 22/09/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import Foundation
extension String {
	
	func toZenkaku() -> String {
		
		let thisString = self
		var returnValue = ""
		for character in thisString.characters
		{
			var kanji:String = ""
			switch character{
			case "0" : kanji = "０"
			case "1" : kanji = "１"
			case "2" : kanji = "２"
			case "3" : kanji = "３"
			case "4" : kanji = "４"
			case "5" : kanji = "５"
			case "6" : kanji = "６"
			case "7" : kanji = "７"
			case "8" : kanji = "８"
			case "9" : kanji = "９"
			default: kanji = String( character )
			}
			
			returnValue += kanji
		}
		
		return returnValue
	}
	
	
}

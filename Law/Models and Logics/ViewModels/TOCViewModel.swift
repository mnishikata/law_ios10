//
//  TOCViewModel.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2017/04/11.
//  Copyright © 2017年 Catalystwo. All rights reserved.
//

import UIKit

class TOCViewModel {
  var linkDictionaries:[LinkDictionary] = []
  var viewSize:CGSize = CGSize(width: 300,height: 300)
  let headerHeight: CGFloat = 30

  var preferredContentSize: CGSize {
      let height: CGFloat = max( viewSize.height - 180, 400 )
      return CGSize(width: min( 450, viewSize.width - 20 ), height: headerHeight + max( 44,  min( height,  CGFloat(linkDictionaries.count) * 44.0) ) )
  }
  
  var backgroundColorScheme: BackgroundColorSchemeType {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    return backgroundColorScheme
  }
  
  func bodyAttributedString(at index: Int) -> (NSAttributedString, String /*subtitle*/) {
    
    let dict = linkDictionaries[index]
    let content = dict.content as NSString
    let regexString = "（.*）"
    
    var range = content.range(of: regexString, options: [.regularExpression])
    
    if range.length == 0 {
      range = NSMakeRange( content.length, 0 )
    }
    
    var subtitle = content.substring(with: range)
    let body = content.substring( with: NSMakeRange(0, range.location) )
    
    // [　]*第[一二三四五六七八九〇十百].
    let section = "[　]*第.+　"
    let sectionRange = (body as NSString).range(of: section, options: [.regularExpression])
    
    var indentLevel = 0
    func countSpaceFromBeginning(_ string:String) -> String {
      
      let characters = string.characters
      var returnString = ""
      
      for character in characters {
        if character != "　" {
          break
        }
        
        returnString = returnString + "　" + " "
        indentLevel += 1
      }
      return returnString
    }
    
    let countedSpaces = countSpaceFromBeginning(body)
    
    
    var font =  UIFont.systemFont( ofSize: UIFont.smallSystemFontSize+2.0 )
    if indentLevel < 2 {
      font = UIFont.boldSystemFont(ofSize: font.pointSize)
    }
    let bodyColor = backgroundColorScheme.textColor
    var bodyFont = UIFont.systemFont(ofSize: UIFont.labelFontSize)
    if indentLevel < 2 {
      bodyFont = UIFont.boldSystemFont(ofSize: bodyFont.pointSize)
    }else {
      bodyFont = UIFont.systemFont(ofSize: bodyFont.pointSize - 1)
    }
    
    let attributedString = NSMutableAttributedString(string: body, attributes:[NSFontAttributeName:bodyFont, NSForegroundColorAttributeName:bodyColor])
    
    attributedString.setAttributes([NSFontAttributeName:font, NSForegroundColorAttributeName:backgroundColorScheme.tintColor], range: sectionRange)
    if subtitle.isEmpty == false { subtitle = countedSpaces + subtitle }
    return (attributedString, subtitle)
  }
  
  
}

//
//  HighlightListTableViewModel.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 18/12/16.
//
//

import Foundation

/*
 
 
 var anchor: String? = ""
 let fp = FilePackage.shared
 if let anchorText = highlight.nearestAnchor {
 (anchor,_) = fp.anchor(for: anchorText, in: packageUrl)
 
 let ( plist, linkDictionaries, _) = fp.loadPackage(at: packageUrl, onlyPlist: false, zoomScale: 1.0)
 let theLink = linkDictionaries?.filter { $0.link == anchorText }
 print(theLink?.first?.content)
 
 }
 
 
 */

class HighlightListTableViewModel {
 
  var allHighlights: [DocItem]
  var packageUrl: URL!
  var rowHeight: CGFloat = 86
  var viewSize:CGSize = CGSize(width: 300,height: 300)
  let headerHeight: CGFloat = 30

  subscript(idx: Int) -> DocItem? {
    guard idx < allHighlights.count else { return nil }
    return allHighlights[idx]
  }
  
  init(viewModel: DetailViewModel) {
    self.allHighlights = viewModel.allHighlights ?? []
    self.packageUrl = viewModel.packageURL
    
    let elementDescriptors: [ElementDescriptor]? = viewModel.toc2
    if elementDescriptors == nil { return }
    let contentHtml = viewModel.rawSource
    
    if contentHtml == nil { return }
    
    self.allHighlights.forEach { highlight in
      
      if let anchor = highlight.nearestAnchor {
        let range = contentHtml!.range(of: anchor, options: [])
        
        highlight["location"] = range.location
      }
    }
    
    self.allHighlights.sort { ($0["location"] as! Int) < ($1["location"] as! Int) }
    
    self.allHighlights.forEach {
      let location = $0["location"] as! Int
      
      element: for hoge in 0 ..< elementDescriptors!.count {
        let element = elementDescriptors![hoge]
        article: for bar in 0 ..< element.count {
          let article = element[bar]
          if NSLocationInRange(location, article.range) {
            $0["theTitle"] = article.humanReadablePath
            break element
          }
        }
      }
    }
  }
  
  var preferredContentSize: CGSize {
      let height: CGFloat = max( viewSize.height - 180, 400 )
      return CGSize(width: min( 450, viewSize.width - 20 ), height: headerHeight + max( 44,  min( height,  CGFloat(numberOfHighlights) * rowHeight) ) )
  }
  
  var backgroundColorScheme: BackgroundColorSchemeType {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    return backgroundColorScheme
  }
  
  func highlightAttributedString(at index: Int) -> NSAttributedString {
    guard index < allHighlights.count else { return NSAttributedString() }// UNKNOWN ERR
    guard packageUrl != nil else { return NSAttributedString() }
    let highlight = allHighlights[index]
    
    let title = highlight["theTitle"] as? String ?? ""
    
    let boldFont = UIFont.preferredFont(forTextStyle: .headline)
    let plainFont = UIFont.preferredFont(forTextStyle: .body)
    
    let mattr = NSMutableAttributedString(string: "")
    let titleAttr = NSAttributedString(string: title + " ", attributes: [NSFontAttributeName: boldFont, NSForegroundColorAttributeName: backgroundColorScheme.textColor])
    
    var attributes = [NSFontAttributeName: plainFont, NSBackgroundColorAttributeName: highlight.color?.color() ?? UIColor.clear, NSForegroundColorAttributeName: backgroundColorScheme.textColor] as [String : Any]
    
    if highlight.color == .underline {
      attributes = [NSFontAttributeName: plainFont, NSUnderlineStyleAttributeName: NSNumber(value: 2),  NSUnderlineColorAttributeName: UIColor.red, NSForegroundColorAttributeName: backgroundColorScheme.textColor]
    }
    
    let bodyAttr = NSAttributedString(string: (highlight.text ?? ""), attributes: attributes)
    mattr.append(titleAttr)
    mattr.append(bodyAttr)
    
    if highlight.notes?.isEmpty == false {
      let bodyAttr = NSAttributedString(string: "💬", attributes: attributes)
      mattr.append(bodyAttr)
    }
    
    return mattr
  }
  
  var numberOfHighlights: Int {
    return allHighlights.count
  }

  func delete(at index: Int) {
    guard index < allHighlights.count else { return }
    let itemToDelete = allHighlights[index]
    
    NotificationCenter.default.post(name: LawNotification.highlightDidChangeExternally, object: nil, userInfo: ["uuid": itemToDelete.uuid ?? "n/a", "snippet": itemToDelete.text ?? "n/a"])
    
    if FilePackage.shared.documents.removeAndDeleteForItem(itemToDelete) {
      itemToDelete.removeFile()
      
      FilePackage.shared.saveIndex( saveToICloud: true )
      FilePackage.shared.updateHighlight()
    }
    
    allHighlights.remove(at: index)
  }
}

//
//  BookmarkViewModel.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2017/04/11.
//  Copyright © 2017年 Catalystwo. All rights reserved.
//

import UIKit

class BookmarkViewModel {

  var allBookmars: [DocItem]
  var packageUrl: URL!
  var rowHeight: CGFloat = 44
  var viewSize:CGSize = CGSize(width: 300,height: 300)
  let headerHeight: CGFloat = 30
  
  subscript(idx: Int) -> DocItem? {
    guard idx < allBookmars.count else { return nil }
    return allBookmars[idx]
  }
  
  init(viewModel: DetailViewModel) {
    self.allBookmars = viewModel.allBookmars ?? []
    self.packageUrl = viewModel.packageURL
    
    let linkDictionaries = viewModel.linkDictionaries
    
    self.allBookmars.sort { b1, b2 in
      var idx1: Int? = nil, idx2: Int? = nil
      
      if let anchorText = b1.anchor {
        for hoge in 0 ..< linkDictionaries.count {
          if linkDictionaries[hoge].link == anchorText {
            idx1 = hoge
            break
          }
        }
      }
      
      if let anchorText = b2.anchor {
        for hoge in 0 ..< linkDictionaries.count {
          if linkDictionaries[hoge].link == anchorText {
            idx2 = hoge
            break
          }
        }
      }
      
      if idx1 == nil { return false }
      if idx2 == nil { return true }
      return idx1! < idx2!
    }
  }
  
  var numberOfBookmarks: Int {
    return allBookmars.count
  }
  
  var preferredContentSize: CGSize {
    let height: CGFloat = max( viewSize.height - 180, 400 )
    return CGSize(width: min( 450, viewSize.width - 20 ), height: headerHeight + max( 44,  min( height,  CGFloat(numberOfBookmarks) * rowHeight) ) )
  }
  
  var backgroundColorScheme: BackgroundColorSchemeType {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    return backgroundColorScheme
  }
  
  func delete(at index: Int) {
    guard index < allBookmars.count else { return }
    let itemToDelete = allBookmars[index]
    if FilePackage.shared.documents.removeAndDeleteForItem(itemToDelete) {
      itemToDelete.removeFile()
      
      FilePackage.shared.saveIndex( saveToICloud: true )
      FilePackage.shared.updateHighlight()
    }
    
    allBookmars.remove(at: index)
  }
  
  func bookmarkAttributedString(at index: Int) -> NSAttributedString {
    guard index < allBookmars.count else { return NSAttributedString() }// UNKNOWN ERR
    guard packageUrl != nil else { return NSAttributedString() }
    let bookmark = allBookmars[index]
    
    let title = bookmark.title
    let plainFont = UIFont.preferredFont(forTextStyle: .body)
    
    let mattr = NSMutableAttributedString(string: "")
    let titleAttr = NSAttributedString(string: title + " ", attributes: [NSFontAttributeName: plainFont, NSForegroundColorAttributeName: backgroundColorScheme.textColor])
    
    mattr.append(titleAttr)

    return mattr
  }
}

//
//  StackNavigationViewModel.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 22/12/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import Foundation

typealias RestorationKey = String
extension RestorationKey {
  
  static let className = "class"
  static let urlData = "FileEntryPackageURLData"
  static let packageURL = "FileEntryPackageURL"
  static let anchor = "FileEntryAnchor"
  static let plist = "FileEntryPlist"
  static let excerptType = "DetailViewControllerExcerptType"
  static let exerptElement = "DetailViewControllerExcerptElement"
  static let webOffsetY = "DetailViewControllerWebContentOffsetY"
  static let isInsideUtility = "DetailViewControllerInsideUtilityViewController"
  static let popNavigationWhenBack = "DetailViewControllerPopNavigationWhenBack"
  static let findString = "DetailViewControllerFindString"
  static let findPosition = "DetailViewControllerFindPosition"
  
  static let utilities = "UtilityViewController"
}

typealias StackNavigationStorage = [[RestorationKey : Any]]


protocol StackNavigation  {
  init(restorationInfo: [RestorationKey: Any]?)
  func restorationInfo() -> [RestorationKey: Any]
}

class StackNavigationViewModel {
  
  var tab: Tab? = nil
  var backgroundColorScheme: BackgroundColorSchemeType {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    return backgroundColorScheme
  }
  
  
  class func stackViewControllersFromPlist(_ plist: StackNavigationStorage) -> [UIViewController] {
    var viewControllers = [UIViewController]()
    
    for dict in plist {
      let className = dict[.className] as! String
      let swiftClassName = Bundle.main.infoDictionary!["CFBundleName"] as! String + "." + className
      
      if let ClassObj = NSClassFromString(swiftClassName) as? StackNavigation.Type {
        
        let controller = ClassObj.init(restorationInfo: dict)
        if controller is UIViewController {
          viewControllers.append(controller as! UIViewController)
        }
      }
    }
    return viewControllers
  }
  
  func stackPlistFromViewControlelrs(_ viewControllers: [AnyObject]) -> StackNavigationStorage {
    
    var infoStack: [[RestorationKey: Any]] = []
    
    for viewController in viewControllers {
      guard viewController is StackNavigation else { continue }
      var info = (viewController as! StackNavigation).restorationInfo()
      
      var className = NSStringFromClass(type(of: viewController))
      
      if info.isEmpty {
        className = NSStringFromClass(PlaceholderViewController.self)
      }
      
      // Laws.DetailViewController -> DetailViewController
      if className.hasPrefix("Laws.") {
        className = className.substring(from: className.index(className.startIndex, offsetBy: 5))
      }
      

      
      info[.className] = className
      infoStack.append(info)
    }
    return infoStack
    
  }
  
  var titles: [String]? {
    guard let tab = tab else { return nil }
    guard let plist = TabDataSourceStorage<StackNavigationStorage>()[tab] else { return nil }
    let titles = plist.map { (($0[.plist] as? DocProperty)?[.title] as? String ?? "")  }
    
    return titles
  }
  
  func pathString(omitRoot: Bool = true) -> String {
    guard let titleArray = self.titles else { return "" }
    if omitRoot && titleArray.count == 1 { return "" }
    
    var titleString = ""
    for title in titleArray {
      
      if titleString.characters.count != 0 {
        titleString = titleString + " ► "
      }
      titleString = titleString + title
    }
    
    return titleString
  }

}

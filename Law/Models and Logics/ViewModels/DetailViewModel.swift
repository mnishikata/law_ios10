
//
//  DetailViewModel
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 20/12/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import Foundation
import Firebase

protocol DetailViewDelegate: class {
  func detailItemDidChange()
  func didFinishLoading()
  func statJumpToExcerptAnchor()
  func contentOffsetYDidChange()
  func traitCollectionDidChange()
  func findDidChange()
  
}

enum JumpTimingType {
  case onViewDidAppear, onViewWillAppear
}

class DetailViewModel {
  weak var delegate: DetailViewDelegate?
  
  var traitCollection: UITraitCollection! {
    didSet {
      delegate?.traitCollectionDidChange()
    }
  }
  convenience init(delegate: DetailViewDelegate) {
    self.init()
    self.delegate = delegate
  }
  
  
  func nextPageViewModel( _ linkDestination: LinkDestination ) -> DetailViewModel? {
    let condition: ((_ dictionary: DocItem) -> Bool) = { (dictionary: DocItem) -> Bool in
      return dictionary.isLawEntity && dictionary.h_file == linkDestination.h_file
    }
    
    guard let theDestination = FilePackage.shared.documents.lookForWithCondition(condition) else { return nil }
    
    
    let viewModel = DetailViewModel()
    viewModel.popNavigationWhenBack = true
    viewModel.insideUtilityViewController = self.insideUtilityViewController
    viewModel.excerptType = self.excerptType
    viewModel.traitCollection = self.traitCollection
    
    let dictionary: DocProperty = [.title: theDestination.originalTitle]
    
    if linkDestination.anchor_f.characters.count > 0 {
      let object = FileEntry( packageURL: theDestination.packageURL!, anchor:  linkDestination.anchor_f.compressedAnchor, elementId: nil, plist: dictionary )
      viewModel.detailItem = object
      viewModel.startJump = true
    }else {
      let object = FileEntry( packageURL: theDestination.packageURL!, anchor: nil, elementId: nil,  plist: dictionary )
      viewModel.detailItem = object
      viewModel.startJump = false
    }
    
    return viewModel
  }
  
  var focusedElementId: String? {
    return detailItem?.elementId
  }
  
  var toolbarHeight: CGFloat {
    
    var toolbarHeight: CGFloat = traitCollection.verticalSizeClass == .compact ? 32 : 44
    if insideUtilityViewController == true {
      toolbarHeight = 0
    }
    return toolbarHeight
  }
  
  func javascript(toZoom scale: CGFloat) -> String {
    let script = "document.body.style.zoom = \"\(webViewScale * Double(scale)*100)%\"; "
    return script
  }
  
  var lawTabHeight: CGFloat {
    var height: CGFloat = traitCollection.verticalSizeClass == .compact ? 40 : 60
    
    if insideUtilityViewController == true || usesOldStyleTab == true {
      height = 0
    }
    
    return height
  }
  
  func lawTabViewFrame(withViewBounds viewBounds: CGRect) -> CGRect {
    return CGRect(x: 0, y: viewBounds.maxY - toolbarHeight - lawTabHeight, width: viewBounds.size.width, height: lawTabHeight)
  }
  
  
  func toolbarFrame(withViewBounds viewBounds: CGRect) -> CGRect {
    return CGRect(x: 0, y: viewBounds.maxY - toolbarHeight, width: viewBounds.size.width, height: toolbarHeight)
  }
  
  func webFrame(withViewBounds viewBounds: CGRect, inUtility: Bool = false) -> CGRect {
    var webFrame = viewBounds
    if inUtility { return  webFrame }
    webFrame.size.height -= toolbarHeight
    if usesOldStyleTab == false {
      webFrame.size.height -= lawTabHeight
    }
    return webFrame
  }
  
  var packageURL: URL? {
    return detailItem?.packageURL
  }
  
  var anchor: String? {
    get {
    return detailItem?.anchor
    }
    set {
      detailItem?.anchor = newValue
    }
  }
  
  var allHighlights: [DocItem]? {
    if detailItem == nil { return [] }
    return FilePackage.shared.allHighlights(detailItem!)
  }
  
  var allBookmars: [DocItem]? {
    if detailItem == nil { return [] }
    return FilePackage.shared.allBookmarks(detailItem!)
  }
  
  var allNotes: [DocItem]? {
    if detailItem == nil { return [] }
    return FilePackage.shared.allNotes(detailItem!)
  }
  
  private var detailItem: FileEntry? {
    didSet {
      if detailItem != nil && detailItem?.packageURL != oldValue?.packageURL {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
          AnalyticsParameterContentType: (detailItem?.plist[DocPropertyKey.title] as? NSString ?? "n/a" as NSString)
          ])
      }
      
      findString_ = nil

      // Update the view.
      //startContentOffset = nil
      
      if oldValue?.packageURL != detailItem?.packageURL {
        hasFinishedLoading = false
        rawSource_ = nil
        toc2_ = nil
      }else {
        if hasFinishedLoading == true && excerptType == .full {
          DispatchQueue.main.async { self.delegate?.didFinishLoading() }

        }
        
        if excerptType != .full && startJump == true {
          excerptElement = nil
          
          DispatchQueue.main.async { self.delegate?.statJumpToExcerptAnchor() }

        }
      }
      
      let ( plist, _, _ ) = FilePackage.shared.loadPackage(at: packageURL,  onlyPlist: true )
      
      if let sourcePath = plist?[DocPropertyKey.sourceURL] as? String {
        contentsIsUserDefined = sourcePath.hasPrefix("user:") == true
      }
      else {
        contentsIsUserDefined = false
      }
      
      DispatchQueue.main.async { self.delegate?.detailItemDidChange() }
    }
  }
  
  var backgroundColorScheme: BackgroundColorSchemeType {
    let ud = UserDefaults.standard
    let obj = ud.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    return backgroundColorScheme
  }
  
  var linkDictionaries: [LinkDictionary] = []
  var restoredState: [RestorationKey: Any]? = nil

  var hasFinishedLoading = false
  var requiresUpdates: LawUpdateStatus = .unknown
  var editionToUpdate: String? = nil
  
  // JUMP

  var startJump = false
  var startJumpOnViewDidAppear: JumpTimingType = .onViewWillAppear
  var viewAppeared = false

  private var canJump: Bool {
    return ( startJump == true && startJumpOnViewDidAppear == .onViewWillAppear ) &&
      ( detailItem?.anchor == nil && detailItem?.elementId == nil )
  }
  
  var doJump: Bool {
    if canJump == false { return false }
    if viewAppeared == false && startJumpOnViewDidAppear == .onViewDidAppear { return false }
    
    let ud = UserDefaults.standard
    ud.synchronize()
    if ud.bool(forKey: "StartupJump") == false {
      return false
    }
    
    startJump = false
    startJumpOnViewDidAppear = .onViewWillAppear
    return true
  }
  
  ///
  
  var javascriptForAnchor: String? {
    guard let anchorString = detailItem?.anchor else { return nil }
    let script = "document.location.href = \"#\(anchorString)\""
    return script
  }
  
  func javascript(forInternalLink url: NSURL) -> String? {
    
    var specifier = url.resourceSpecifier as NSString?
    
    if specifier == nil { return nil }
    
    if specifier!.hasPrefix("%23") {
      specifier = specifier?.substring(from: 3) as NSString?
    }
    
    if specifier!.hasPrefix("blank%23") {
      specifier = specifier?.substring(from: 8) as NSString?
    }
    
    let script = "document.location.href = \"#\(specifier!)\""
    return script
  }
  
  func bookmark(forExternalLink url: NSURL) -> Bookmark {
    
    let specifier = url.resourceSpecifier
    let comps = specifier?.components(separatedBy: "@")
    let decodedTitle =  ((comps?[0])! as NSString).replacingPercentEscapes(using: String.Encoding.utf8.rawValue)!.decodedArticleTitle
    
    let bookmark = Bookmark( title: decodedTitle, encodedURL: (comps?[1])!, clickedURL: url as URL)

    return bookmark
  }
  
  func aohonReaderAppLink(forLink url: NSURL) -> URL? {
    let specifier = url.resourceSpecifier
    let comps = specifier?.components(separatedBy: "@")

    var lawRaw: Int = NSNotFound
    switch comps![2] {
    case "S34HO121.html": lawRaw = 0
    case "S34HO123.html": lawRaw = 1
    case "S34HO125.html": lawRaw = 2
    case "S34HO127.html": lawRaw = 3
    default: return nil
    }
    
    let link = "comcatalystwoaohon:\(comps![0])@\(lawRaw)"
    let appLink = URL(string: link)!

    return appLink
  }
  
  
  var lawTitle: String? {
    return detailItem?.plist[.title] as? String
  }
  
  func attributedLawTitle(for url: NSURL) -> NSAttributedString {
    var attributes: [String: Any] = [:]
    attributes[NSLinkAttributeName] = url
    attributes[NSFontAttributeName] = UIFont.systemFont(ofSize: UIFont.systemFontSize)
    
    let bookmark = self.bookmark(forExternalLink: url)

    let attributedString = NSAttributedString(string: (lawTitle ?? "") + bookmark.title, attributes: attributes)

    return attributedString
  }
  
  var excerptType: ExcerptType = {
    let ud = UserDefaults.standard
    if false /*ud.object(forKey: "ExcerptMode") as? String == "Yes" */ {
      return .excerptArticle
    }
    return .full
  }()
  
  var findString_: String? = nil {
    didSet {
      if oldValue == nil && findString_ == nil {
        
      }else {
        DispatchQueue.main.async { self.delegate?.findDidChange() }
      }
    }
  }
  var findCount: Int = 0
  var findPosition: Int = 0 {
    didSet {
      delegate?.findDidChange()
    }
  }
  var findTotalString_: String? {
    if findString_ == nil { return nil }
    
    if findCount > 0 {
      
      let localizedString = NSString(format: WLoc("Found %d") as NSString, findCount)

      return "\(String(findPosition)) / " + (localizedString as String)
      
    }
    else {
      return WLoc("Not Found")
    }

  }

  var excerptElement: ElementDescriptor? = nil
  
  var toc2_: [ElementDescriptor]? = nil
  var toc2: [ElementDescriptor]? {
    if detailItem == nil { return nil }
    if toc2_ == nil {
      toc2_ = FilePackage.shared.loadHTMLElements(for: detailItem!.packageURL)
    }
    return toc2_
  }
  private(set) var contentsIsUserDefined: Bool = false
  
  var htmlSource: NSString? = nil
  
  var rawSource_: NSString? = nil
  var rawSource: NSString? {
    if rawSource_ != nil { return rawSource_ }
    if detailItem == nil { return nil }
    
    let ( _, _, rawSource) = FilePackage.shared.loadPackage(at: detailItem!.packageURL, onlyPlist: false, zoomScale: webViewScale, rawHTML: true)
    rawSource_ = rawSource
    return rawSource_
  }
  
  var webViewScale: Double {
    get {
      var doubleVal = UserDefaults.standard.double(forKey: "WebViewZoomScale") as Double
      
      if doubleVal < 0.2 { doubleVal = 1.0 }
      if doubleVal > 5.0 { doubleVal = 5.0 }
      
      return doubleVal
    }
    set {
      UserDefaults.standard.set(Double(newValue), forKey: "WebViewZoomScale")
    }
  }
  
  // USER INTERFACE
  var contentOffsetStack_: [CGFloat?] = []
  var contentOffsetForwardStack_: [CGFloat] = []
  
  var contentOffsetY_: CGFloat? = 0.0 {
    didSet {
      delegate?.contentOffsetYDidChange()
    }
  }
  var savedFindPosition_: Int? = nil

  //var startContentOffset: CGPoint? = nil
  var startSearch: String? = nil
  
  var insideUtilityViewController = false

  var usesOldStyleTab: Bool {
    if insideUtilityViewController { return true }
    return UserDefaults.standard.bool(forKey: "UseOldStyleTab")
  }
  
  var popNavigationWhenBack = false

  //var utilityViewContollerToAddWhenViewLoaded: UIViewController? = nil
  
  func loadFrom(url: URL, anchorText: String? = nil) {
    let fp = FilePackage.shared
    
    var anchor: String? = self.anchor
    if anchorText != nil {
      (anchor,_) = fp.anchor(for: anchorText!, in: url)
    }
    let ( plist, linkDictionaries, contentHtml) = fp.loadPackage(at: url, onlyPlist: false, zoomScale: webViewScale)
    
    if contentHtml != nil && linkDictionaries != nil {
      
      self.linkDictionaries = linkDictionaries!
      detailItem = FileEntry(packageURL: url, anchor: anchor, elementId: nil, plist: plist!)
      
      contentHtml!.convertHTMLtoIncludeNotes(allHighlights: allHighlights, allNotes: allNotes, showTooltip: false)
      htmlSource = contentHtml
      title = plist![DocPropertyKey.title] as? String ?? ""
      
    }
  }
  
  func loadFromRestorationInfo(_ restorationInfo: [RestorationKey: Any]) {
    // Load from user default
    var theURL: URL!
    if let data = restorationInfo[.urlData] as? Data {
      do {
        var flag = false
        theURL = try URL(resolvingBookmarkData: data, options: [.withoutUI], relativeTo: nil, bookmarkDataIsStale: &flag)
      } catch {
        return
      }
    }else {
      
      //FileEntryPackageURL
      if let lastPathComponent = restorationInfo[.packageURL] as? String {
        theURL = FilePackage.shared.url(with: lastPathComponent)
      }
      
      if theURL == nil { return }
    }
    
    let anchor = restorationInfo[.anchor] as? String
    guard let plist = restorationInfo[.plist] as? DocProperty else { return } // Unknown error
    
    excerptType = ExcerptType(rawValue: restorationInfo[.excerptType] as? Int ?? 0)!
    if let dict = restorationInfo[.exerptElement] as? [String: Any] {
      excerptElement = ElementDescriptor(from: dict)
    }
    
    detailItem = FileEntry( packageURL: theURL, anchor: anchor, elementId: nil, plist: plist )
    
    /// PATTERN A NORMAL
    
    if excerptElement != nil && excerptType != .full {
      // PATTERN B EXCERPT
      
      
    }else {
      let contentOffsetY = restorationInfo[.webOffsetY] as? CGFloat
      
      if contentOffsetY != nil {
        contentOffsetY_ = contentOffsetY as CGFloat!
        //startContentOffset = CGPoint(x: 0, y: contentOffsetY as CGFloat!)
      }
    }
    // Find state
    
    findString_ = restorationInfo[.findString] as? String
    savedFindPosition_ = restorationInfo[.findPosition] as? Int
    
    /*
    // Utility
    if let stackPlist = restorationInfo[.utilities] as? [[RestorationKey: Any]] {
      let viewControllers = StackNavigationController.stackViewControllersFromPlist(stackPlist)
      if viewControllers.count > 0 {
        let nav = StackNavigationController(rootViewController: viewControllers[0], restoreIdentifier: nil )
        nav.setViewControllers(viewControllers, animated: false)
        utilityViewContollerToAddWhenViewLoaded = nav
        //				setUtilityViewController(nav)
      }
    }*/
    
    insideUtilityViewController = restorationInfo[.isInsideUtility] as? Bool ?? false
    popNavigationWhenBack = restorationInfo[.popNavigationWhenBack] as? Bool ?? false
    
    if let title = plist[DocPropertyKey.title] {
       self.title = title as? String ?? ""
    }
    
  }
  
  var title: String = ""
  
  var reloadAlertTitle: String {
    var messageTitle = (requiresUpdates == .upToDate ? WLoc("Retrieve Up To Date") : WLoc("Retrieve Title") )
    
    if contentsIsUserDefined == true {
      messageTitle = (requiresUpdates == .upToDate ? WLoc("Retrieve Up To Date") : WLoc("Retrieve Title") )
    }
    
    if requiresUpdates == .unknown { messageTitle = "" }

    if requiresUpdates == .hasUpdate && editionToUpdate != nil {
      messageTitle += "\n(\(editionToUpdate!))"
    }

    return messageTitle
  }

  
  var reloadAlertMessage: String {
    var message = (requiresUpdates == .upToDate ? WLoc("RetrieveMessageUpToDate") : WLoc("RetrieveMessage") )
    
    if contentsIsUserDefined == true {
      message = (requiresUpdates == .upToDate ? WLoc("RetrieveMessageUpToDateUser") : WLoc("RetrieveMessageUser") )
    }
    return message
  }
  
  func restorationInfo() -> [RestorationKey: Any] {
    
    if detailItem == nil { return [:] }
    
    var dict: [RestorationKey: Any] = [:]
    
    // Package name
    if let lastPathComponent = detailItem?.packageURL.lastPathComponent {
      dict[.packageURL] = lastPathComponent
    }
    
    // Anchor
    if let anchor = detailItem?.anchor {
      dict[.anchor] = anchor
    }
    
    // Plist
    if let plist = detailItem?.plist {
      dict[.plist] = plist
    }
    
    // Scroll state
    if contentOffsetY_ != nil {
      dict[.webOffsetY] = contentOffsetY_!
    }
    
    // Excerpt mode
    dict[.excerptType] = excerptType.rawValue
    if excerptElement != nil {
      dict[.exerptElement] = excerptElement!.dictionaryRepresentation
    }
    
    // WebView
    dict[.popNavigationWhenBack] = popNavigationWhenBack
    
    // Find state
    if findString_ != nil {
      dict[.findString] = findString_!
      /*
      if self.isViewLoaded {
        dict[.findPosition] = theWebView.currentPosition()
      }
 */
    }
    
    dict[.isInsideUtility] = insideUtilityViewController
    
    
    // Save
    return dict
  }

  
  func hanreiUrl(_ title: String) -> URL? {
    guard let lawTitle = lawTitle else { return nil }
    
    
    //		let courtURLString:String = "http://www.courts.go.jp/app/hanrei_jp/list1?action_search=検索&filter%5BcourtName%5D=&filter%5BcourtType%5D=&filter%5BbranchName%5D=&filter%5BjikenGengo%5D=&filter%5BjikenYear%5D=&filter%5BjikenCode%5D=&filter%5BjikenNumber%5D=&filter%5BjudgeGengoFrom%5D=&filter%5BjudgeYearFrom%5D=&filter%5BjudgeMonthFrom%5D=&filter%5BjudgeDayFrom%5D=&filter%5BjudgeGengoTo%5D=&filter%5BjudgeYearTo%5D=&filter%5BjudgeMonthTo%5D=&filter%5BjudgeDayTo%5D=&filter%5Btext1%5D=\(lawTitle as! String!)\(title as String!)&filter%5Btext2%5D=&filter%5Btext3%5D=&filter%5Btext4%5D=&filter%5Btext5%5D=&filter%5Btext6%5D=&filter%5Btext7%5D=&filter%5Btext8%5D=&filter%5Btext9%5D="
    
    var queryString1 = ""
    var queryString2 = ""
    
    queryString1 = (lawTitle + (title as String!))
    queryString1 = queryString1.replacingOccurrences(of: "第", with: "")
    queryString2 = queryString1.replacingOccurrences(of: "民事訴訟法", with: "民訴法")
    queryString2 = queryString2.replacingOccurrences(of: "刑事訴訟法", with: "刑訴法")
    queryString2 = queryString2.replacingOccurrences(of: "日本国憲法", with: "憲法")
    queryString2 = queryString2.replacingOccurrences(of: "行政事件訴訟法", with: "行訴法")
    
    if queryString2 == queryString1 { queryString2 = (lawTitle + title) }
    
    queryString1 = queryString1.addingPercentEncoding(  withAllowedCharacters: CharacterSet.urlQueryAllowed)!
    queryString2 = queryString2.addingPercentEncoding(  withAllowedCharacters: CharacterSet.urlQueryAllowed)!
    
    let courtURLString: String = "http://www.courts.go.jp/app/hanrei_jp/list1?action_search=%E6%A4%9C%E7%B4%A2&filter%5BcourtName%5D=&filter%5BcourtType%5D=&filter%5BbranchName%5D=&filter%5BjikenGengo%5D=&filter%5BjikenYear%5D=&filter%5BjikenCode%5D=&filter%5BjikenNumber%5D=&filter%5BjudgeGengoFrom%5D=&filter%5BjudgeYearFrom%5D=&filter%5BjudgeMonthFrom%5D=&filter%5BjudgeDayFrom%5D=&filter%5BjudgeGengoTo%5D=&filter%5BjudgeYearTo%5D=&filter%5BjudgeMonthTo%5D=&filter%5BjudgeDayTo%5D=&filter%5Btext1%5D=" + queryString1 + "&filter%5Btext2%5D=" + queryString2 + "&filter%5Btext3%5D=&filter%5Btext4%5D=&filter%5Btext5%5D=&filter%5Btext6%5D=&filter%5Btext7%5D=&filter%5Btext8%5D=&filter%5Btext9%5D="
    
    let url = URL(string: courtURLString)
    return url
  }
  
  func hanrei2Url(_ title: String) -> URL? {
    guard let lawTitle = lawTitle else { return nil }
    
    //		let courtURLString:String = "http://www.courts.go.jp/app/hanrei_jp/list1?action_search=検索&filter%5BcourtName%5D=&filter%5BcourtType%5D=&filter%5BbranchName%5D=&filter%5BjikenGengo%5D=&filter%5BjikenYear%5D=&filter%5BjikenCode%5D=&filter%5BjikenNumber%5D=&filter%5BjudgeGengoFrom%5D=&filter%5BjudgeYearFrom%5D=&filter%5BjudgeMonthFrom%5D=&filter%5BjudgeDayFrom%5D=&filter%5BjudgeGengoTo%5D=&filter%5BjudgeYearTo%5D=&filter%5BjudgeMonthTo%5D=&filter%5BjudgeDayTo%5D=&filter%5Btext1%5D=\(lawTitle as! String!)\(title as String!)&filter%5Btext2%5D=&filter%5Btext3%5D=&filter%5Btext4%5D=&filter%5Btext5%5D=&filter%5Btext6%5D=&filter%5Btext7%5D=&filter%5Btext8%5D=&filter%5Btext9%5D="
    
    var queryString1 = ""
    var queryString2 = ""
    
    queryString1 = (lawTitle + (title as String!))
    queryString1 = queryString1.replacingOccurrences(of: "第", with: "")
    queryString1 = queryString1.replacingOccurrences(of: "日本国憲法", with: "憲法")
    
    if queryString2 == queryString1 { queryString2 = (lawTitle + (title as String!)) }
    
    queryString1 = queryString1.addingPercentEncoding(  withAllowedCharacters: CharacterSet.urlQueryAllowed)!
    queryString2 = queryString2.addingPercentEncoding(  withAllowedCharacters: CharacterSet.urlQueryAllowed)!
    
    let courtURLString: String = "http://thoz.org/hanrei/s/?q=" + queryString1
    
    let url = URL(string: courtURLString)
    return url
  }
  
  func bingUrl(_ title: String) -> URL? {
    guard let lawTitle = lawTitle else { return nil }
    
    let bingURLString: String = "http://www.bing.com/search?q=\"\(lawTitle)\(title)\""
    
    print("\(bingURLString.addingPercentEncoding(  withAllowedCharacters: CharacterSet.urlQueryAllowed))")
    let url = URL(string: bingURLString.addingPercentEncoding(  withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!

    return url
  }
  
  func wikiBooksUrl(_ title: String) -> URL? {
    guard let lawTitle = lawTitle else { return nil }
    
    let bingURLString: String = "https://ja.wikibooks.org/wiki/\(lawTitle)\(title as String!)"
    
    print("\(bingURLString.addingPercentEncoding(  withAllowedCharacters: CharacterSet.urlQueryAllowed))")
    let url = URL(string: bingURLString.addingPercentEncoding(  withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!
    
    return url
  }
  
  var shouldSuggestsAohonReaderApp: Bool {
    if detailItem?.packageURL.lastPathComponent.hasPrefix("S34HO121.html") == true ||
      detailItem?.packageURL.lastPathComponent.hasPrefix("S34HO123.html")  == true ||
      detailItem?.packageURL.lastPathComponent.hasPrefix("S34HO125.html")  == true ||
      detailItem?.packageURL.lastPathComponent.hasPrefix("S34HO127.html")  == true
    {

      return true
    }
    return false
  }
  
  var shouldShowBookmarkGuide: Bool {
    get {
      let ud = UserDefaults.standard
      if ud.bool(forKey: "BookmarkGuide") == true { return false }
      return true
    }
    
    set {
      let ud = UserDefaults.standard
      ud.set(!newValue, forKey: "BookmarkGuide")
    }
  }
  
  func goBack() {
    if contentOffsetStack_.count == 0 { return }
    let num: CGFloat? = contentOffsetStack_.last!
    
    if num == nil {
      contentOffsetY_ = nil
      contentOffsetStack_.removeLast()

      return
    }
    
    contentOffsetStack_.removeLast()
    
    var addingNumber: CGFloat? = contentOffsetY_
    
    if contentOffsetForwardStack_.count > 0 && contentOffsetForwardStack_.last == addingNumber {
      addingNumber = nil
    }
    
    if addingNumber != nil {
      contentOffsetForwardStack_.append(addingNumber!)
    }
    
    contentOffsetY_ = num!
  }
  
  func goFoward() {
    guard contentOffsetForwardStack_.count > 0 else { return }
    let num: CGFloat? = contentOffsetForwardStack_.last
    if num == nil { return }
    
    contentOffsetForwardStack_.removeLast()
    
    var addingNumber: CGFloat? = contentOffsetY_;
    
    if contentOffsetStack_.count > 0 && contentOffsetStack_.last! == addingNumber {
      addingNumber = nil
    }
    
    if addingNumber != nil {
      contentOffsetStack_.append(addingNumber!)
    }
    
    contentOffsetY_ = num!
  }
  
  func addHistory() {
    
    contentOffsetForwardStack_.removeAll(keepingCapacity: false)
    var num: CGFloat? = contentOffsetY_
    
    if contentOffsetStack_.count > 0 {
      if contentOffsetStack_.last! == num { num = nil }
    }
    
    if num != nil {
      contentOffsetStack_.append(num!)
    }
  }
  
  var isHttpURL: Bool? {
    if let absoluteString = detailItem?.packageURL.absoluteString {
      return absoluteString.hasPrefix("http")
    }
    return nil
  }
  
  func checkForUpdates(completion: @escaping (() -> Void) ) {
    if requiresUpdates == .unknown && detailItem != nil {
      FilePackage.shared.checkUpdates( detailItem!.packageURL) { (success, updateStatus, edition) in
        DispatchQueue.main.async {
          self.requiresUpdates = updateStatus
          if updateStatus == .hasUpdate {  self.editionToUpdate = edition }
          completion()
        }
      }
    }
  }
  
  func updateExcerptElement() {
    if excerptElement == nil {
      // look for anchor in toc2
      if toc2 != nil {
        for element in toc2! {
          if element.lookForRange(for: detailItem!.anchor!).location != NSNotFound {
            excerptElement = element
            break
          }
        }
      }
    }
  }
  
  let ReviewCount = 10
  
  func shouldReview() -> Bool {
    if #available(iOS 10.3, *) {
      
    }else {
      return false
    }
    
    let ud = UserDefaults.standard
    let version = ObjectiveCClass.versionString()
    let key = "ReviewAppVersion" + version! // "ReviewAppVersionVersion 3.0.8" // "ReviewAppVersion" + version!
    let count = ud.integer( forKey: key )
    
    if count != Int.max {
      ud.set(count+1, forKey: key )
      ud.synchronize()
      
      if count > ReviewCount  { // 30
        return true
      }
    }
    return false
  }
  
  func didReview() {
    // Review
    let ud = UserDefaults.standard
    let version = ObjectiveCClass.versionString()
    //let key = "ReviewAppVersion" + version!
    let key = "ReviewAppVersion" + version!
    
    ud.set(Int.max, forKey: key )
    ud.synchronize()
  }
  
  func anchor(for text: String) -> (anchor: String?, element: ElementDescriptor?, javascript: String?) {
    let (anchor, element) = FilePackage.shared.anchor(for: text, in: detailItem!.packageURL, toc2: toc2, toc1: linkDictionaries)
    
    let script = "window.location.href = '#\(anchor ?? "")'"

    return (anchor, element, script)
  }
  
  func addBookmark(_ bookmark: Bookmark) {
    FilePackage.shared.addBookmark(detailItem!, bookmark: bookmark )
  }

  func setNotes( _ text: String!, bookmark: Bookmark ) {
    FilePackage.shared.setNotes(text, entry: detailItem!, bookmark: bookmark )
  }
  
  func getNotes(bookmark: Bookmark ) -> String {
    return FilePackage.shared.notes( detailItem!, bookmark: bookmark) ?? ""
  }
  
  func deleteNotes(bookmark: Bookmark ) {
    FilePackage.shared.deleteNotes(detailItem!, bookmark: bookmark )
  }

  func setNotes( _ text: String!, sourceItem: DocItem ) {
    sourceItem.notes = text
    sourceItem.ETag = shortUUIDString()
    FilePackage.shared.saveIndex( saveToICloud: true )
  }
  
  func deleteNotes(sourceItem: DocItem ) {
    sourceItem.notes = nil
    sourceItem.ETag = shortUUIDString()
    FilePackage.shared.saveIndex( saveToICloud: true )
  }
  
  func addHighlight(sourceItem: DocItem) {
    FilePackage.shared.addHighlight( detailItem!, dictionary: sourceItem )
  }
  
  func setHighlight(sourceItem: DocItem, color: HighlightColor) {
    sourceItem.color = color
    sourceItem.ETag = shortUUIDString() // Change uuid as etag
    
    highlightDefaultColor = color
    FilePackage.shared.updateHighlight()
  }
  
  func deleteHighlight(withUuid uuid: String) {
    FilePackage.shared.removeHighlight( detailItem!, uuid: uuid)
  }
  
  
  var highlightDefaultColor: HighlightColor {
    get {
    var color: HighlightColor = .yellow
    if nil != UserDefaults.standard.object(forKey: "HighlightDefaultColor") {
      let rawValue = UserDefaults.standard.integer(forKey: "HighlightDefaultColor")
      color = HighlightColor( rawValue: rawValue )
    }
    return color
    }
    set {
      UserDefaults.standard.set(newValue.rawValue, forKey: "HighlightDefaultColor")
    }
  }
  


}



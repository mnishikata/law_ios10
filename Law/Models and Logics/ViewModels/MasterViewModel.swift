//
//  MasterViewModel.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 21/12/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import Foundation

protocol MasterViewDelegate: class {
  func traitCollectionDidChange()
  func tableDidUpdate()
  func conflictHandler() -> ICloudConflictNotificationHandler
}

protocol MasterViewCellModelProtocol {
  var backgroundColorScheme: BackgroundColorSchemeType { get }
  func baseFrame(baseWidth: CGFloat) -> CGRect
  func labelFrame(baseWidth: CGFloat, forItem item: DocItem) -> CGRect
  func fileExists(for item: DocItem) -> FileExistsType
  func isDownloading(for item: DocItem) -> Bool
  var downloadImage: UIImage { get }
  func font(for item: DocItem) -> UIFont
  func accessoryViewFrame(baseWidth: CGFloat) -> CGRect
  func highlightAttributes(for item: DocItem) -> [String: Any]
}

class MasterViewModel: MasterViewCellModelProtocol {

  weak var delegate: MasterViewDelegate?

  var adTypeIsVideo = false
  var dirty = false
  var downloadingPackageNames: [String] = []

  var traitCollection: UITraitCollection! {
    didSet {
      delegate?.traitCollectionDidChange()
    }
  }
  
  var eLawsBannerHeight: CGFloat {
    let date = Date() // 524664609.98954
    print(date)
    if date.timeIntervalSinceReferenceDate > (524664609.98954 + 10*24*60*60) { return 28 }
   return 0
  }
  var toolbarHeight: CGFloat { return traitCollection.verticalSizeClass == .compact ? 32 : 44 }
  func toolbarFrame(withViewBounds viewBounds: CGRect) -> CGRect {
    let frame = CGRect(x: 0, y: viewBounds.maxY - toolbarHeight, width: viewBounds.size.width, height: toolbarHeight)
    return frame
  }
  
  func tableViewFrame(withViewBounds viewBounds: CGRect) -> CGRect {
    let frame = CGRect(x: 0, y: bannerHeight, width: viewBounds.size.width, height: viewBounds.size.height - bannerHeight - toolbarHeight - eLawsBannerHeight)
    return frame
  }
  
  var bannerHeight: CGFloat {
    if UserDefaults.standard.object(forKey: "AdType") as? String == "Banner" {
      return 50
    }else {
      return 0
    }
  }
  func bannerViewFrame(withViewBounds viewBounds: CGRect) -> CGRect {
    let frame = CGRect(x: 0, y: 0, width: viewBounds.size.width, height: bannerHeight)
    return frame
  }
  
  var backgroundColorScheme: BackgroundColorSchemeType {
    let ud = UserDefaults.standard
    let obj = ud.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    return backgroundColorScheme
  }
  
  var appIconName: String? {
    get {
      let ud = UserDefaults.standard
      ud.synchronize()
      if let appIconName = ud.object(forKey: "AppIcon") as? String {
        if appIconName == "Primary" { return nil }
        return appIconName
      }
      return nil
    }
    set {
      let ud = UserDefaults.standard
      ud.set(newValue, forKey: "AppIcon")
    }
  }
  
  convenience init(delegate: MasterViewDelegate) {
    self.init()
    self.delegate = delegate
    adTypeIsVideo = (UserDefaults.standard.object(forKey: "AdType") as? String) == "video"
    
    let fp = FilePackage.shared
    fp.bookmarkingDidFinishHandler = { self.delegate?.tableDidUpdate() }
    let _ = fp.loadIndex( conflictHandler, updateHandler: { self.delegate?.tableDidUpdate() } )

  }
  
  var conflictHandler: ICloudConflictNotificationHandler? {
    return delegate?.conflictHandler()
  }
  
  func saveIfNecessary() {
    if dirty == false { return }
    FilePackage.shared.saveIndex( saveToICloud: true )
    dirty = false
  }
  
  func addFolder(name: String) {
    var string = name
    if string.isEmpty == true  { string = "Untitled Folder" }
    
    let dict = DocItem(folderWithTitle: string)
    FilePackage.shared.documents.addItem(dict)
    FilePackage.shared.saveIndex( saveToICloud: true )
  }
  
  var serializedData: Data? {
    let dict = FilePackage.shared.documents
    let serializedData: Data
    
    do {
      serializedData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
    } catch {
      return nil
    }
    
    return serializedData
  }
  
  
  
  func outlineView(child index: Int, ofItem item: DocItem!) -> DocItem! {
    
    if item == nil {
      let array = FilePackage.shared.documents.items!
      return array.object( at: index ) as! DocItem
    }
    return item.items?.object( at: index )  as! DocItem
  }
  
  func outlineView(isItemExpandable item: DocItem!) -> Bool {
    
    if item.items == nil || item.items!.count == 0 { return false }
    return true
  }
  
  func outlineView(isItemExpanded item: DocItem!) -> Bool {
    let dict = item as DocItem
    return dict.expanded
  }
  
  func outlineView(didExpandItem item: DocItem!)  {
    set(expanded: true, forItem: item)
  }
  
  func outlineView(didCollapseItem item: DocItem!)  {
    set(expanded: false, forItem: item)
  }
  
  var cellHeight: CGFloat = 44
  
  func set(expanded: Bool, forItem item: DocItem! )  {
    
    (item as DocItem).expanded = expanded
    dirty = true
  }
  
  func outlineView(numberOfChildrenOfItem item: DocItem!) -> Int {
    if item == nil {
      let items = FilePackage.shared.documents.items
      if items != nil { return items!.count }
      else { return 0 }
    }
    
    let subitems = (item as DocItem).items
    if subitems != nil { return subitems!.count }
    
    return 0
  }
  
  func outlineView(textForItem item: DocItem!) -> NSAttributedString! {
    let title = (item as DocItem).title
    
    var font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
    
    if (item as DocItem).anchor != nil {
      font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.subheadline)
    }
    
    let attributes = [ NSFontAttributeName: font ]
    let attributedText = NSAttributedString(string: title as String, attributes: attributes )
    return attributedText
  }
  
  func moveItem(_ item: DocItem!, toItem parentObj: Any!, at index: Int) {
    let originalItem = item as? DocItem
    
    let rootDocument = FilePackage.shared.documents
    let parentItem = parentObj as? DocItem ?? rootDocument
    
    var originalIndex = 0
    let newIndex = index
    
    let parentOfOriginalItem = originalItem?.findParentIn( rootDocument )
    
    if parentOfOriginalItem == nil { originalIndex = rootDocument.indexOfItem( originalItem! ) }
    else { originalIndex = parentOfOriginalItem!.indexOfItem( originalItem! ) }
    
    if parentOfOriginalItem == parentItem {
      if originalIndex > index { originalIndex += 1  }
    }
    
    // Insert
    parentItem.insertItem( originalItem!, atIndex: newIndex )
    
    // Remove
    if parentOfOriginalItem != nil {
      parentOfOriginalItem!.removeItemAtIndex(originalIndex)
    }
    else if rootDocument.items != nil {
      rootDocument.removeItemAtIndex(originalIndex)
    }
    
    FilePackage.shared.saveIndex( saveToICloud: true )

  }
  
  
  
  func deleteItem(_ itemToDelete: DocItem!) {
    
    if itemToDelete.highlight {
      NotificationCenter.default.post(name: LawNotification.highlightDidChangeExternally, object: nil, userInfo: ["uuid": itemToDelete.uuid ?? "n/a", "snippet": itemToDelete.text ?? "n/a"])
    }
    
    if FilePackage.shared.documents.removeAndDeleteForItem(itemToDelete) {
      itemToDelete.removeFile()
      
      FilePackage.shared.saveIndex( saveToICloud: true )
    }
  }
  
  func font(for item: DocItem) -> UIFont {
    if item.anchor != nil {
      return UIFont.preferredFont(forTextStyle: UIFontTextStyle.subheadline)
    }

    return UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
  }
  
  func highlightAttributes(for item: DocItem) -> [String: Any] {
    var attributes: [String: Any] = [:]
    
    let font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.subheadline)
    
    let color = item.color
    
    if color != nil {
      if color! == .underline {
        attributes[NSUnderlineColorAttributeName] = UIColor.red
        attributes[NSUnderlineStyleAttributeName] = NSUnderlineStyle.styleThick.rawValue
        
      }else {
        attributes[NSBackgroundColorAttributeName] = color!.color()
      }
    }
    
    attributes[NSFontAttributeName] =  font
    attributes[NSForegroundColorAttributeName] = backgroundColorScheme.textColor
    return attributes
  }
  
  func accessoryViewFrame(baseWidth: CGFloat) -> CGRect {
    return CGRect(x: baseWidth-30-7, y: (44-30)/2, width: 30, height: 30)
  }
  
  func baseFrame(baseWidth: CGFloat) -> CGRect {
    return CGRect(x: 0,y: 0,width: baseWidth,height: 44)
  }
  func labelFrame(baseWidth: CGFloat, forItem item: DocItem) -> CGRect {
    let status = fileExists(for: item)
    if status != .yes {
      return CGRect(x: 0,y: 0,width: baseWidth-44,height: 44)
    }
    return  CGRect(x: 0,y: 4,width: baseWidth,height: 44 - 8)
  }
  
  func fileExists(for item: DocItem) -> FileExistsType {
    if item.packageURLExistsFile { return .yes }
    if item.packageName != nil && downloadingPackageNames.contains( item.packageName! ) { return .downloading }
    return .no
  }
  
  func isDownloading(for item: DocItem) -> Bool {
    return item.packageName != nil && downloadingPackageNames.contains( item.packageName! )
  }
  
  var downloadImage: UIImage {
    let image = UIImage(named: "LawDownload")!.colorizedImage(withTint: backgroundColorScheme.tintColor, alpha: 1.0, glow: false)
    return image

  }
  
  func download(forItem item: DocItem, progress: @escaping (Double)->Void, completion: @escaping (_ success: Bool, _ error: Error?)->Void ) {
    guard item.packageName != nil else { return }

    item.cloud = .Downloading
    
    downloadingPackageNames.append( item.packageName! )

    CloudKitDomain.shared.downloadLaw( item, progress: progress ) { (success: Bool, error: Error?) -> Void in
      
      if let pacakgeIndex = self.downloadingPackageNames.index( of: item.packageName! ) {
        self.downloadingPackageNames.remove( at: pacakgeIndex )
      }
      
      
      if success == true {
        item.cloud = .Installed
        
      } else {
        item.cloud = .InCloud
      }
      
      completion(success, error)
    }
  }
}

enum FileExistsType {
    case yes, no, downloading
}

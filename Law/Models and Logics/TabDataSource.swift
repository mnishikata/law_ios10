//
//  TabDataSource.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 21/12/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import Foundation

typealias Tab = String

protocol TabDataSourceProtocol: class {
  func load(tab: Tab, jumpTo: String?)
  func tabWillSwitch()
}


protocol TabBarProtocol: class {
  func tabsDidUpdateExternally()
}


extension TabDataSource {
  
  // -- CONSTANTS --
  @nonobjc static let tabIdentifiersDidChange = Notification.Name("TabIdentifiersDidChange")
}

class TabDataSource {
  static var shared = TabDataSource()
  
  weak var delegate: TabDataSourceProtocol?
  weak var tabBarDelegate: TabBarProtocol?

  private(set) var current: Tab?
  var all: [Tab] {
    
    let ud = UserDefaults.standard
    ud.synchronize()
    
    var tabIdentifiers: [Tab] = []
    var array = ud.object(forKey: "TabIdentifiers") as? [Tab]
    
    if array != nil && array!.count > 0  {
      tabIdentifiers = array!
      current = ud.object(forKey: "CurrentTabIdentifier") as? Tab? ?? tabIdentifiers[0]
    }
    
    let sharedUD = NSUbiquitousKeyValueStore.default()
    sharedUD.synchronize()
    
    array = sharedUD.object(forKey: "TabIdentifiers") as? [Tab]
    if array != nil && array!.count > 0  {
      tabIdentifiers = array!
    }
    
    return tabIdentifiers
  }
  
  init() {
    let ud = UserDefaults.standard
    ud.synchronize()
    
    let sharedUD = NSUbiquitousKeyValueStore.default()
    sharedUD.synchronize()

    current = ud.object(forKey: "CurrentTabIdentifier") as? String
    
    if current == nil {
      
      let identifiers = all
      if identifiers.count > 0 {
        current = all[0]
      }
    }
    
    NotificationCenter.default.addObserver(forName: NSUbiquitousKeyValueStore.didChangeExternallyNotification, object: nil, queue: nil) { (notification) in
      
      let identifiers = self.all
      
      let ud = UserDefaults.standard
      ud.set(identifiers, forKey: "TabIdentifiers")
      ud.synchronize()
      
      self.tabBarDelegate?.tabsDidUpdateExternally()
    }
  }
  
  func currentTabIdentifier() -> Tab {
    if current != nil { return current! }
    return addNewTabIdentifier()
  }
  
  func selectTabIdentifier(_ identifier: Tab? = nil, jumpTo article: String? = nil) -> Bool {
    
    guard let identifier = identifier else {
      let _ = addNewTabIdentifier()
      return true
    }
    
    guard let _ = all.index( of: identifier ) else { return false }
    
    let ud = UserDefaults.standard
    
    delegate?.tabWillSwitch()
    
    current = identifier
    
    ud.set(current, forKey: "CurrentTabIdentifier")
    ud.synchronize()
  
    tabIdentifierDidSwitch(jumpTo: article)
    
    return true
  }
  
  func moveTabAt(index f: Int, to t: Int) {
    if f == t { return }
    
    var identifiers = all
    guard identifiers.count > f && identifiers.count > t else { return }
    let theIdentifier = identifiers[f]
    
    identifiers.remove(at: f)
    identifiers.insert(theIdentifier, at: t)
    
    let ud = UserDefaults.standard
    ud.set(identifiers, forKey: "TabIdentifiers")
    ud.synchronize()
    
    let sharedUD = NSUbiquitousKeyValueStore.default()
    sharedUD.set(identifiers, forKey: "TabIdentifiers")
    sharedUD.synchronize()
    
  }
  
  
  func swapTabIdentifiers( _ f: Int, t: Int) {
    
    var identifiers = all
    
    (identifiers[f], identifiers[t]) = (identifiers[t], identifiers[f])
    
    let ud = UserDefaults.standard
    ud.set(identifiers, forKey: "TabIdentifiers")
    ud.synchronize()
    
    let sharedUD = NSUbiquitousKeyValueStore.default()
    sharedUD.set(identifiers, forKey: "TabIdentifiers")
    sharedUD.synchronize()
    
  }
  
  private func addNewTabIdentifier() -> Tab {
    
    let center = NotificationCenter.default
    let uuid = shortUUIDString()
    
    var identifiers = all
    identifiers.append(uuid)
    
    delegate?.tabWillSwitch()
    
    current = uuid
    
    let ud = UserDefaults.standard
    
    ud.set(identifiers, forKey: "TabIdentifiers")
    ud.set(current, forKey: "CurrentTabIdentifier")
    ud.synchronize()
    
    let sharedUD = NSUbiquitousKeyValueStore.default()
    sharedUD.set(identifiers, forKey: "TabIdentifiers")
    sharedUD.synchronize()
    
    center.post(name: TabDataSource.tabIdentifiersDidChange, object: nil)
    tabIdentifierDidSwitch()
    
    return uuid
  }
  
  func removeTabIdentifier(_ identifier: Tab) {
    
    let center = NotificationCenter.default
    var identifiers = all
    let index = identifiers.index( of: identifier )
    
    if index != nil {
      identifiers.remove(at: index!)
      
      TabDataSourceStorage<Any>()[identifier] = nil
    }
    
    if identifier == current {
      
      if identifiers.count == 0 {
        current = nil
      }else {
        current = identifiers[0]
      }
      
      //center.post(name: StackNavigationController.tabIdentifierDidSwitch, object: nil)
      tabIdentifierDidSwitch()

    }
    
    let ud = UserDefaults.standard
    
    ud.set(identifiers, forKey: "TabIdentifiers")
    ud.set(current, forKey: "CurrentTabIdentifier")
    ud.synchronize()
    
    let sharedUD = NSUbiquitousKeyValueStore.default()
    sharedUD.set(identifiers, forKey: "TabIdentifiers")
    sharedUD.synchronize()
    
    center.post(name: TabDataSource.tabIdentifiersDidChange, object: nil)
    
  }
  
  private func tabIdentifierDidSwitch(jumpTo article: String? = nil) {
    if current != nil {
      delegate?.load(tab: current!, jumpTo: article)
    }
  }

  
}


// Tab key Storage
class TabDataSourceStorage<T> {
  
  let key = "StackNavigationController"
  subscript(identifier: Tab) -> T? {
    get {
      let ud = UserDefaults.standard
      let sharedUD = NSUbiquitousKeyValueStore.default()
      sharedUD.synchronize()
      var plist = sharedUD.object( forKey: key + identifier ) as! T?
      
      if plist == nil {
        plist = ud.object( forKey: key + identifier ) as! T?
      }
      return plist
    }
    set(infoStack) {
      if let infoStack = infoStack {
        let ud = UserDefaults.standard
        ud.set(infoStack, forKey: key + identifier)
        ud.synchronize()
        
        let sharedUD = NSUbiquitousKeyValueStore.default()
        sharedUD.set(infoStack, forKey: key + identifier)
        sharedUD.synchronize()
      }else {
        let ud = UserDefaults.standard
        ud.removeObject(forKey: key + identifier)
        ud.synchronize()
        
        let sharedUD = NSUbiquitousKeyValueStore.default()
        sharedUD.removeObject(forKey: key + identifier)
        sharedUD.synchronize()
      }
    }
  }
  
  func saveStackedData(_ identifier: Tab, _ infoStack: T!) {
    
    let ud = UserDefaults.standard
    ud.set(infoStack, forKey: key + identifier)
    ud.synchronize()
    
    let sharedUD = NSUbiquitousKeyValueStore.default()
    sharedUD.set(infoStack, forKey: key + identifier)
    sharedUD.synchronize()
  }
  
  func loadStackedData(_ identifier: Tab) -> Any! {
    let ud = UserDefaults.standard
    let sharedUD = NSUbiquitousKeyValueStore.default()
    sharedUD.synchronize()
    var plist = sharedUD.object( forKey: key + identifier ) as! T?
    
    if plist == nil {
      plist = ud.object( forKey: key + identifier ) as! T?
    }
    return plist
  }
}

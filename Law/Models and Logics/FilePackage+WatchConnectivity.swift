//
//  FilePackage+WatchConnectivity.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 16/12/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import Foundation
import WatchConnectivity

// MARK:- Watch

extension FilePackage: WatchConnectivityManagerDelegate {
  
  // -- WATCH MESSGE KEYS --
  enum WatchMessageKey {
    static let requestIndexFromWatch = "RequestIndex"
    static let indexEtag = "ETag"
    static let requestFilesFromWatch = "RequestFiles"
    
    // TODO: CHANGE TO FULL INDEX
    static let indexDataSource = "Source"
    static let indexDataSourceTitle = "title"
    static let indexDataSourcePackageName = "pkgName"
  }
  
  // -- ICLOUD PLIST KEYS --
  @nonobjc static let iCloudTimeStamp = "ICloudIndexPlistTimeStamp"
  @nonobjc static let iCloudBZ2 = "ICloudIndexPlistBZ2"
  @nonobjc static let iCloudPlist = "ICloudIndexPlist"
  
  
  func sendIndexToWatch() {
    print("** SEND INDEX TO WATCH")
    
    if #available(iOS 9.3, *) {
      DispatchQueue.main.async {
        
        var dict = self.updatesToSendToWatch() as [String: Any]
        let etag = shortUUIDString() // UPDATE ETAG
        let ud = UserDefaults.standard
        ud.set(etag, forKey: "IndexETagForWathApp")
        ud.synchronize()
        dict[FilePackage.WatchMessageKey.indexEtag] = etag
        WatchConnectivityManager.shared.sendApplicationContext(dict)
      }
    }
  }
  
  func updatesToSendToWatch() -> [String: Any] {
    
    let array = FilePackage.shared.documents.collectWithCondition {
      
      if $0.isLawEntity && $0.packageURLExistsFile, let pkg = $0.packageName {
        return [FilePackage.WatchMessageKey.indexDataSourceTitle: $0.title,
                FilePackage.WatchMessageKey.indexDataSourcePackageName: pkg]
      }
      
      return nil
    }
    
    return [FilePackage.WatchMessageKey.indexDataSource: array as! [[String: Any]]]
  }
  
  func watchConnectivityManager(_ watchConnectivityManager: WatchConnectivityManager, didReceiveMessage message: [String: Any], withReplyHandler reply: (([String: Any]) -> Void)? ) {
    
    // THIS METHOD COULD BE CALLED MANY TIMES
    if #available(iOS 9.3, *) {
      DispatchQueue.main.async {
        
        if let _ = message[FilePackage.WatchMessageKey.requestIndexFromWatch] {
          var dict = self.updatesToSendToWatch() as [String: Any]?
          
          if let etag = UserDefaults.standard.object(forKey: "IndexETagForWathApp") as? String {
            
            if (message[FilePackage.WatchMessageKey.indexEtag] as? String) == etag {
              // WATCH APP ALREADY HAS THE INDEX
              dict = nil
            }else {
              dict?["ETag"] = etag
            }
          }
          
          if dict != nil  {
            reply?(dict!)
          }
        }
        
        if let packageNames = message[FilePackage.WatchMessageKey.requestFilesFromWatch] as? [String] {
          
          //TODO:AVOID TOO MUCH TRANSFER REQUEST
          print("*** TODO TODO:AVOID TOO MUCH TRANSFER REQUEST")
          
          for pkgName in packageNames {
            let url = FileRepository.shared.documentFolderURL.appendingPathComponent(pkgName)
            let file: FileAndMetadata = ( url, ["filename": pkgName] )
            
            watchConnectivityManager.transferFiles(file)
            
          }
        }
      }
    }
  }
  
  
  // RECEIVE
  
  func watchConnectivityManager(_ watchConnectivityManager: WatchConnectivityManager, updatedWithContext context: [String: Any]) {
  }
  
  @available(iOS 9.0, *)
  func watchConnectivityManager(_ watchConnectivityManager: WatchConnectivityManager, didReceiveFile file: WCSessionFile) {
  }
  
  
  func watchConnectivityManager(_ watchConnectivityManager: WatchConnectivityManager, didReceiveMessageData data: Data) {
    
  }
  
}


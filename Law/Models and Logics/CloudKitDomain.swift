//
//  CloudKitRepository.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 16/12/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import Foundation
import CloudKit

// -- CONSTANTS --

enum RecordType {
  static let lawDocPackage = "LawDocPackage"
  static let myPackageBundle = "MyPackageBundle"
  static let report = "Report"
  
  enum LawDocPackageKey {
    static let md5 = "md5", title = "title", count = "count", edition = "edition"
    static let creatorUUID = "creatorUUID", creator = "creator", creationDate = "creationDate"
    static let modificationDate = "modificationDate"
    static let packageBundle = "packageBundle", package = "package"
  }
  
  enum MyPackageBundleKey {
    static let modificationDate = "modificationDate"
    static let title = "title"
    static let creatorUUID = "creatorUUID"
    static let packageBundle = "packageBundle"
    static let source = "source"
  }
  
  enum ReportKey {
    static let title = "title"
  }
}

enum LawCloudKitFetchStatus {
		case start
		case noResults
		case fetching
		case found
		
  /*
  var rowHeight: CGFloat {
    switch self{
    case .start:  return 60
    case .noResults:  return 240
    case .fetching: return 60
    case .found:  return 240
    }
  }
 */
}


final class CloudKitDomain {
  
  static var shared = CloudKitDomain()

  var creatorUUID: String? = nil
  
  init() {
    let _ = CloudKitRepository.shared
  }
  

  func updateIfHasPrivateAccount()  {
    CloudKitRepository.shared.hasPrivateAccount({(hasPrivateAccount: Bool)->Void in})
  }
  
  func hasPrivateAccount(_ callback: @escaping ((_ hasPrivateAccount: Bool)->Void))  {
    CloudKitRepository.shared.hasPrivateAccount(callback)
  }

  
  //MARK:- DOWNLOADING
  func downloadLaw( _ dictionary: DocItem , progress: @escaping ((_ progress: Double)->Void), completion: @escaping (_ success: Bool, _ error: Error?) -> Void )  {
    
    CloudKitRepository.shared.downloadRecord(dictionary , progress: progress, completion: completion)
  }
  
  func downloadSource(for record: CKRecord, progress progressHandler: @escaping (_ progress: Double) -> Void, completion: @escaping ((_ attributedString: NSAttributedString?, _ error: Error?) -> Void) ) {
    
    CloudKitRepository.shared.downloadSourceWithRecordID(record.recordID, progress: progressHandler) { (success, data, _, error ) in
      var success = success
      var error = error
      var attributedString: NSAttributedString? = nil

      if success {
        do {
          attributedString = try NSAttributedString(data: data!, options: [NSDocumentTypeDocumentAttribute: NSRTFTextDocumentType], documentAttributes: nil)
          
        }catch let error1 {
          error = error1
          success = false
        }
      }
      completion(attributedString, error )
    }
  }
  
  func fetchRecord(withPackageBundle packageBundle: String, desiredKeys: [String], completion: @escaping (_ results: [CKRecord], _ error: Error?)->Void ) {
    CloudKitRepository.shared.fetchRecord(withPackageBundle: packageBundle, desiredKeys: desiredKeys, completion: completion)
  }
  
  func downloadingProgress(_ record: CKRecord) -> Double? {
    return CloudKitRepository.shared.downloaders[record.recordID]
  }
  
  func downloadAsset(for record: CKRecord, progress progressHandler: @escaping ((_ progress: Double) -> Void), completion: @escaping (_ success: Bool, _ error: Error?) -> Void ) {
    CloudKitRepository.shared.downloadAssetWithRecordID(record.recordID, progress: progressHandler, completion: completion)
  }
  
  //MARK:- FETCHING
  
  var fetchedRecords: [CKRecord] = []
  var fetchStatus: LawCloudKitFetchStatus = .start
  var uniqueRecords: [CKRecord] {
    
    var theRecords: [CKRecord] = []
    for record: CKRecord in CloudKitDomain.shared.fetchedRecords {
      
      let thisTitle = record[RecordType.LawDocPackageKey.title] as? String ?? ""
      let thisCreator = record[RecordType.LawDocPackageKey.creatorUUID] as? String ?? ""
      
      // Look for thisTitle and thisPackageBundle in theRecords
      var hasAlready = false
      for thatRecord in theRecords {
        
        let thatTitle = thatRecord[RecordType.LawDocPackageKey.title] as? String ?? ""
        let thatCreator = thatRecord[RecordType.LawDocPackageKey.creatorUUID] as? String ?? ""
        
        if thatTitle == thisTitle && thatCreator == thisCreator {
          hasAlready = true
          break
        }
      }
      
      if hasAlready == false {
        theRecords.append(record)
      }
    }
    
    return theRecords
  }
  
  func fetch( _ string: String, tokenSearch: Bool, completion: @escaping (_ error: Error?) -> Void ) {
    
    self.fetchStatus = .fetching
    fetchedRecords = []
    CloudKitRepository.shared.fetch(string, tokenSearch: tokenSearch) { (results, cursor, error) in
      DispatchQueue.main.async {
        if results.count == 0 { self.self.fetchStatus = .noResults }
        else { self.self.fetchStatus = .found }
        self.fetchedRecords = results
        completion(error)
      }
    }
  }
  
  func clearFetch() {
    fetchedRecords = []
    fetchStatus = .start
  }
  
  //MARK:- FETCHING MY PACKAGE BUNDLES
  var myBundleRecords: [CKRecord] = []
  var myBundleFetchStatus: LawCloudKitFetchStatus = .start
  
  func fetchMyPackageBundles( _ completion: @escaping (_ error: Error?) -> Void ) {
    
    myBundleFetchStatus = .fetching
    
    CloudKitRepository.shared.fetchMyPackageBundles { (results, error) in
      
      if self.creatorUUID == nil {
        if results.count > 0 {
          self.creatorUUID = results[0].object(forKey: RecordType.MyPackageBundleKey.creatorUUID) as? String ?? ""
        }
        else {
          self.creatorUUID = ""
        }
      }
      
      DispatchQueue.main.async {
        if results.count == 0 { self.myBundleFetchStatus = .noResults }
        else { self.myBundleFetchStatus = .found }
        
        self.myBundleRecords = results
        completion(error)
      }
    }
  }
  
  //MARK:- DELETING
  
  func deleteMyPackageBundle(_ record: CKRecord, completion: @escaping (_ success: Bool, _ error: Error? ) -> Void) {
    CloudKitRepository.shared.deleteMyPackageBundle(record, completion: completion)
  }
  
  //MARK: - UPLOADING
  func addDocument(_ packageBundle: String, title: String, source: NSAttributedString, overwritingRecordID pkgRecordID: CKRecordID?, completion: ((_ success: Bool) -> Void)?) {
    
    // Check Creator UUID
    
    assert( creatorUUID != nil, "** creatorUUID should be set in 'fetchMyPackageBundles' before calling this method *** ")
    
    if creatorUUID!.isEmpty {
      creatorUUID = shortUUIDString()
    }
    
    
    CloudKitRepository.shared.addDocument(packageBundle, title: title, source: source, overwritingRecordID: pkgRecordID, creatorUUID: creatorUUID!, completion: completion)
  }
  
  func upload( _ md5: String, title: String, edition: String, packageURL: URL, isPrivate: Bool, creator: String?, overwritingRecordID docRecordID: CKRecordID!, completionHandler: ((_ success: Bool) -> Void)? ) {
    
    
    CloudKitRepository.shared.upload(md5, title: title, edition: edition, packageURL: packageURL, isPrivate: isPrivate, creator: creator, creatorUUID: creatorUUID, overwritingRecordID: docRecordID, completionHandler: completionHandler)
  }
  
  
    func cloudKit( _ md5: String, title: String, edition: String, packageURL: URL, completionHandler: ((_ success: Bool) -> Void)? ) {
      CloudKitRepository.shared.cloudKit( md5, title: title, edition: edition, creatorUUID: creatorUUID,  packageURL: packageURL, completionHandler: completionHandler)
  }
}


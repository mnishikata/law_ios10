//
//  HTMLElementObject.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 12/04/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//
/*
	ElementLocator is a struct that holds location of particular elemnt in the source
	ElementDescriptor describes 条文 article item.  ArticleDescriptor is a subitem thereof.
	ElementDescriptor is an intermediate form in the process of generating objects.
	The final output form is NSDictionary.
 */

/*
	Swift string is very slow when converting Range -> NSRange (position in String) via advanceBy()
	Re-written using NSString
 
	実用新案法４８の１３の２
	特許法１８４の１２の二 done
	特許協力条約 ... </br>　タグが問題。新規作成時は<br>にすることにする。包括的な解決策は？
 */

import Foundation
#if os(iOS)
import Kanna
#endif

typealias DereferenceSubitem = (title: String, detailedTitle: String, linkURL: String, arttitle: String)
typealias DereferenceItem = (title: String, subArray: [DereferenceSubitem])

#if os(iOS)
  
  final private class ElementLocator {
    var element: XMLElement!
    var range: NSRange? = nil
    var divRange: NSRange? = nil
    var gou = false
    var subitems: [ElementLocator]? = nil
    weak var parent: ElementLocator? = nil // USED TEMPORARILY WHEN ORGANISING SUBITEMS
    
    init(element: XMLElement, range: NSRange? = nil, divRange: NSRange? = nil, gou: Bool = false, subitems: [ElementLocator]? = nil) {
      self.element = element
      self.range = range
      self.divRange = divRange
      self.gou = gou
      self.subitems = subitems
    }
    
    func addSubitem( _ item: ElementLocator ) {
      if subitems == nil {
        subitems = [ElementLocator]()
      }
      subitems!.append(item)
    }

    func debugDescription() {
      
      var subitemsToPrint = subitems
      var title = ""
      

      if let name = element["name"] {
        title = "[🔱] \(name)"
      }else {
        title = element.text!
      }
      
      if gou {
        title = "   " + title
      }
      
      if subitemsToPrint != nil && subitemsToPrint!.count > 0  {
        let item = subitemsToPrint![0]
        if let name = item.element["name"] {
          title += " 🔱\(name) "
          
          subitemsToPrint?.remove(at: 0)
        }
      }
      
      if divRange != nil {
        print( "\(title) -  \(divRange!.length) - \(divRange!.location)" )
      }else {
        print( title  )
      }
      
      if subitemsToPrint != nil {
        for subitem in subitemsToPrint! {
          if let name = subitem.element["name"] {
            print(" - [🔱] \(name)"  )
            continue
          }
          
          if let _ = subitem.element["href"] {
            print(" - [HREF] \(subitem.range!.length) - \(subitem.range!.location) "  )
            continue
          }
        }
      }
    }
  }
#endif

// EQUATABLE
func ==(lhs: ArticleDescriptor, rhs: ArticleDescriptor) -> Bool {
  return NSEqualRanges(lhs.range, rhs.range) && lhs.title == rhs.title
}

typealias Referer = (path: String, humanReadablePath: String, arttitle: String, anchor: String?, doc: DocItem, internalLink: Bool)

enum DereferenceKeys {
  static let title = "title"
  static let range = "range"
  static let len = "len"
  static let loc = "loc"
  static let gou = "gou"
  static let anchor = "anchor"
  static let path = "path"
  static let humanReadablePath = "hrpath"
  static let links = "links"
  static let articles = "articles"
  static let arttitle = "arttitle"
  
  // TRANSIENT
  static let text = "txt"

}

final class ArticleDescriptor: Equatable  {

  var loadedDictionary: [String: Any]!
  
  var title: String! {
    get {
      return loadedDictionary[DereferenceKeys.title] as? String
    }
    set {
      loadedDictionary[DereferenceKeys.title] = newValue
    }
  }
  
  var range: NSRange! {
    get {
      
      if let range = loadedDictionary[DereferenceKeys.range] {
        return range as? NSRange
      }
      guard let length = loadedDictionary[DereferenceKeys.len] as? Int else { return nil }
      guard let location = loadedDictionary[DereferenceKeys.loc] as? Int else { return nil }

      let range = NSMakeRange(location, length)
      return range
    }
    set {
      loadedDictionary[DereferenceKeys.loc] = newValue.location
      loadedDictionary[DereferenceKeys.len] = newValue.length
    }
  }
  
  var gou: Bool! {
    get {
      return loadedDictionary[DereferenceKeys.gou] as? Bool
      
    }
    set {
      loadedDictionary[DereferenceKeys.gou] = newValue
    }
  }
  
  var links: [String]? = nil
  
  var anchor: String? {
    get {
      return loadedDictionary[DereferenceKeys.anchor] as? String
    }
    set {
      loadedDictionary[DereferenceKeys.anchor] = newValue
    }
  }
  
  weak var parent: ElementDescriptor?
  
  var path: String! {
    get {
      return loadedDictionary[DereferenceKeys.path] as? String
    }
    set {
      loadedDictionary[DereferenceKeys.path] = newValue
    }
  }
  
  lazy var referencedFrom: [Referer] = []
  
  init?(title: String?, range: NSRange?, gou: Bool = false, parent: ElementDescriptor, source: NSString? = nil) {
    if title == nil { return nil }
    if range == nil { return nil }
    
    self.loadedDictionary = [DereferenceKeys.title: title!, DereferenceKeys.gou: gou, DereferenceKeys.range: range!]
    
    if source != nil {
      self.loadedDictionary[DereferenceKeys.text] = source
    }
    self.parent = parent
  }
  
  // (3)-7 ３条の７
  // (3)-7.2 ３条の７第２項
  // (3)-7.2_5 ３条の７第２項第五号
  // (49)_5 49条第五号
  
  func title(coded: Bool) -> String {
    return coded ? encodeTitle(title!) : title!
  }

  
  var humanReadablePath: String {
    if let humanReadablePath = loadedDictionary[DereferenceKeys.humanReadablePath] as? String  {
      return humanReadablePath
    }
    
    let humanReadablePath = path(coded: false)
    loadedDictionary[DereferenceKeys.humanReadablePath] = humanReadablePath
    return humanReadablePath
  }
  
  func path(coded: Bool) -> String {
    var connector = ElementConnector() // DEFAULT CONNECTOR
    
    if coded {
      connector = ElementConnector(kou: (prefix: ".", suffix: ""), gou: (prefix: "_", suffix: ""), zenkaku: false)
    }
    
    let base = parent?.basePath(for: self, connector: connector) ?? "_"
    if parent?.title(coded: false) == title {
      
      if parent!.singleArticle == false {
        let hogeString = base + connector.kou.prefix + (coded ? "1" : "１") + connector.kou.suffix
        return hogeString
      }else {
        return base
      }
    }
    
    let prefix = ( gou! ? connector.gou.prefix : connector.kou.prefix )
    let suffix = ( gou! ? connector.gou.suffix : connector.kou.suffix )
    let hogeString = base + prefix + title(coded: coded) + suffix
    return hogeString
  }
  
  var description: String {
    var base = humanReadablePath
    if anchor != nil { base += " 🔱\(anchor!)" }
    
    if links == nil { return base }
    return links!.reduce(base) { $0 + " [LINK]" + $1 }
  }
  
  // METHODS
  
  func addLink(_ link: String) {
    if links == nil { links = [] }
    
    if link.hasPrefix("/cgi-bin/idxrefer.cgi?"),
      let linkQuery = URL(string: link)?.query {
      let dict = linkQuery.getUrlQueryParameters()
      if let linkDestination = LinkDestination(queryDictionary: dict) {
        if linkDestination.anchor_f.isEmpty == false {
          let anchor = linkDestination.anchor_f.compressedAnchor
          let h_file = linkDestination.h_file
          links!.append(":" + anchor + ":" + h_file)
        }else {
          let h_file = linkDestination.h_file
          links!.append("::" + h_file)
        }
        return
      }
    }
    links!.append(link)
  }
  
  // PERSISTENT STORE
  
  var dictionaryRepresentation: [String: Any] {
    
    if loadedDictionary[DereferenceKeys.path] != nil {
      return loadedDictionary // THIS INSTANCE WAS CREATED BY LOADED DICTIONRY
    }
    
    var dict: [String: Any] = [
      DereferenceKeys.title: title,
      DereferenceKeys.gou: gou,
      DereferenceKeys.path: path(coded: true),
      DereferenceKeys.humanReadablePath: path(coded: false),
      DereferenceKeys.loc: range.location,
      DereferenceKeys.len: range.length
    ]
    
    if anchor != nil {
      dict[DereferenceKeys.anchor] = anchor!
    }
    
    if links != nil {
      dict[DereferenceKeys.links] = links!
    }
    
    return dict
  }
  
  init?(from dict: [String: Any], parent: ElementDescriptor) {
    self.loadedDictionary = dict
    //		self.parent = parent
    
    if let links = dict[DereferenceKeys.links] as? [String] {
      self.links = links
    }
  }
}

extension ArticleDescriptor {
  func encodeTitle(_ string: String) -> String {
    
    var containsKanji = false
    var returnValue = ""
    
    for character in string.characters  {
      var kanji: String = ""
      
      if character >= "０" && character <= "９" {
        switch character{
          
        case "０" : kanji = "0"
        case "１" : kanji = "1"
        case "２" : kanji = "2"
        case "３" : kanji = "3"
        case "４" : kanji = "4"
        case "５" : kanji = "5"
        case "６" : kanji = "6"
        case "７" : kanji = "7"
        case "８" : kanji = "8"
        case "９" : kanji = "9"
        default: kanji = ""
        }
      }else {
        switch character{
        case "十" : kanji = "十"; containsKanji = true
        case "百" : kanji = "百"; containsKanji = true
        case "一" : kanji = "一"; containsKanji = true
        case "二" : kanji = "二"; containsKanji = true
        case "三" : kanji = "三"; containsKanji = true
        case "四" : kanji = "四"; containsKanji = true
        case "五" : kanji = "五"; containsKanji = true
        case "六" : kanji = "六"; containsKanji = true
        case "七" : kanji = "七"; containsKanji = true
        case "八" : kanji = "八"; containsKanji = true
        case "九" : kanji = "九"; containsKanji = true
        default: kanji = ""
        }
      }
      returnValue += kanji
    }
    
    if containsKanji {
      return returnValue.convertNumberToKanji(convertToZenkaku: false)
      
    }else {
      return returnValue
    }
  }
}

struct ElementConnector  {
  
  var kou = (prefix: "第", suffix: "項")
  var gou = (prefix: "第", suffix: "号")
  var zenkaku = true
}

//extension Referer: Equatable { }
//func ==(lhs: Referer, rhs: Referer) -> Bool {
//	return lhs.path == rhs.path && lhs.doc == rhs.doc
//
//}

extension Sequence where Self.Iterator.Element == Referer {
  func contains(_ other: Referer) -> Bool {
    return filter({$0.path == other.path && $0.doc == other.doc}).count > 0
  }
}

final class ElementDescriptor  {
  
  // 条 - META PROPERTY
  var anchor: String
  var range: NSRange
  var arttitle: String
  var referencedFrom: [Referer] = []
  
  // ARTICLES
  private var articles: [ArticleDescriptor]
  
  init?(anchor: String?, range: NSRange?, arttitle: String?) {
    if anchor == nil { return nil }
    if range == nil { return nil }
    if arttitle == nil { return nil }
    
    self.anchor = anchor!
    self.range = range!
    self.articles = []
    self.arttitle = arttitle!
  }
  
  // READONLY
  subscript(idx: Int) -> ArticleDescriptor {
    let article = articles[idx]
    article.parent = self
    return article
  }
  
  var count: Int {
    return articles.count
  }
  
  var singleArticle: Bool {
    // true when there's only one artcile
    let count = articles.reduce(0) { $0 + ( $1.gou! ? 0 : 1 ) }
    return count == 1
  }
  
  func anchorIsFirstArticle(_ anchor: String) -> Bool {
    if self.anchor == anchor { return true }
    if articles.count == 1 { return true }
    
    let hogeFlag = articles.count > 1 && articles[0].anchor == anchor
    return hogeFlag
  }
  
  // FUNCTIONS
  
  private var codedTitleCache: String? = nil
  func title(coded: Bool) -> String {
    if coded  {
      if codedTitleCache == nil {
        codedTitleCache = articles[0].title.encodedArticleTitle
      }
      return codedTitleCache!
      
    }
    return articles[0].title
  }
  
  func basePath(for article: ArticleDescriptor, connector: ElementConnector) -> String {
    
    let jouTitle = title(coded: !connector.zenkaku)
    
    if article.gou! {
      
      if singleArticle {
        return jouTitle
      }
      
      // FIND PARENT
      if let idx = articles.index(of: article)  {
        for hoge in (0 ..< idx).reversed() {
          let item = articles[hoge]
          if item.gou == true { continue }
          let itemTitle = item.title(coded: !connector.zenkaku)
          if item.title(coded: false) == title(coded: false) {
            let hogeString = jouTitle + connector.kou.prefix + (connector.zenkaku ? "１" : "1") + connector.kou.suffix
            return hogeString
          }else {
            let hogeString =  jouTitle + connector.kou.prefix + itemTitle + connector.kou.suffix
            return hogeString
          }
        }
      }
      
    }else {
      return jouTitle
    }
    
    return "***" // UNKNOWN ERROR
  }
  
  func addArticle(_ title: String?, range: NSRange?, gou: Bool = false, source: NSString? = nil ) -> ArticleDescriptor? {
    if let article = ArticleDescriptor(title: title, range: range, gou: gou, parent: self, source: source) {
      articles.append( article )
      return article
    }else {
      return nil
    }
  }
  
  var description: String {
    if articles.count == 0 { return "n/a" }
    let jouTitle = "[\(articles[0].title!)] 🔱\(anchor)"
    return articles.reduce(jouTitle) { $0 + "\n" + $1.description }
  }
  
  func buildRealmObjects(index: inout Int) -> [String] {
    
    // "--- OBJECTS ---"
    
    var generatedObjects: [String] = []
    
    let message = "\(index)\tjō\t\(self.title(coded: false))\t\(self.title(coded: true))\t\(self.anchor)\t\(self.arttitle)"
    generatedObjects.append(message)
    
    for article in self.articles {

      if article.anchor == nil {
        // ** anchor is nil... probably イロハ 
        continue
      }
      
      index += 1

      var path = article.path(coded: true)
      if article.gou != true {
        if path.hasSuffix(")") {
         path += ".1"
        }
      }
      
      var message = "\(article.title!)\t\(path)\t\(article.anchor!)"
      
      if article.gou == true {
        message = "\(index)\tgō\t" + message
      }else {
        message = "\(index)\tkō\t" + message
      }
      
      
      var txt = article.loadedDictionary[DereferenceKeys.text] as? String
      
      if article.gou == false, let range = txt?.range(of: "<DIV class=\"number\">") {
        txt = txt!.substring(to: range.lowerBound)
      }
      
      if let data = txt?.data(using: .utf16, allowLossyConversion:  false),
        
        let attrString = try? NSMutableAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil) {
        
        let titleLength = (article.title! as NSString).length + 1
        let range = NSMakeRange(titleLength, attrString.length - titleLength - 1)
        
        var txt = attrString.attributedSubstring(from: range).string
       txt = txt.replacingOccurrences(of: "\n", with: "\\n")
        message += "\t" + txt
        
      }else {
        message += "\t*** could not generate attributed string"
      }
      generatedObjects.append(message)

    }
    index += 1
    return generatedObjects
  }
  
  func debugPrint() {
    
    
  }
  
  // PERSISTENT STORE
  
  var dictionaryRepresentation: [String: Any] {
    var dict: [String: Any] = [:]
    
    dict[DereferenceKeys.anchor] = anchor
    dict[DereferenceKeys.loc] = range.location
    dict[DereferenceKeys.len] = range.length
    dict[DereferenceKeys.arttitle] = arttitle
    dict[DereferenceKeys.path] = title(coded: true)
    dict[DereferenceKeys.title] = title(coded: false)
    let dicts = articles.map{ $0.dictionaryRepresentation } as [[String: Any]]
    dict[DereferenceKeys.articles] = dicts
  
    return dict
  }
  
  convenience init?(from dict: [String: Any]) {
    let anchor = dict[DereferenceKeys.anchor] as? String
    if anchor == nil { return nil }
    let articlesDictionary = dict[DereferenceKeys.articles] as? [[String: Any]]
    var range: NSRange? = nil
    
    let length = dict[DereferenceKeys.len] as? Int
    let location = dict[DereferenceKeys.loc] as? Int
    var arttitle = dict[DereferenceKeys.arttitle] as? String
    
    if location == nil || length == nil { return nil }
    if arttitle == nil { arttitle = "" }
    
    range = NSMakeRange(location!, length!)
    
    self.init(anchor: anchor, range: range, arttitle: arttitle)
    
    //		articlesDictionary?.forEach COSTLY??
    
    for dictionary in articlesDictionary! {
      if let article = ArticleDescriptor(from: dictionary, parent: self) {
        articles.append( article )
      }
    }
  }
  
  func anchor(for path: String) -> String? {
    
    for article in articles {
      //print(" \(article.path) has prefix \(path) = \(article.path.hasPrefix( path )) anchor:\(article.anchor)")
      if article.path.hasPrefix(path) == true {
        if articles[0] == article {
          return anchor
        }
        return article.anchor
      }
    }
    return nil
  }
  
  func anchorForPathOfUserDefinedLaw(_ path: String) -> String? {
    
    let encodedPath = "@" + path
    for article in articles {
      //print(" \(article.anchor) has suffix \(encodedPath) = \(article.anchor?.hasSuffix( encodedPath )) ")
      
      if article.anchor?.hasSuffix( encodedPath ) == true {
        return article.anchor
      }
    }
    return nil
  }
  
  func lookForRange(for targetAnchor: String) -> NSRange {
    
    if anchor == targetAnchor { return range }
    
    for article in articles {
      if article.anchor == targetAnchor { return article.range }
    }
    
    return NSMakeRange(NSNotFound,0)
  }
  
  // path, humanReadablePath
  
  func addDereferences(toOther otherElement: ElementDescriptor, doc: DocItem, internalLink: Bool, owner_h_file: String) {
    
    let ownerDocAddedPercentage = owner_h_file.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    
    if ownerDocAddedPercentage == nil {
      return // COULD NOT ENCODE IN SHIFT JIS
    }
    
    for otherArticle in otherElement.articles {
      if otherArticle.links == nil { continue }
      
      let articleValue: Referer = ( otherArticle.path!, otherArticle.humanReadablePath, otherElement.arttitle, otherArticle.anchor, doc, internalLink: internalLink)
      
      for link in otherArticle.links! {
        
        if internalLink == true && link.hasPrefix("intl:") {
          // INTERNAL LINK
          
          //LOOK FOR SELF ANCHOR
          if "intl:" + anchor == link &&
            referencedFrom.contains(articleValue) == false {
            referencedFrom.append(articleValue)
          }
          
          //LOOK FOR ARTICLE ANCHOR
          articles.filter({$0.anchor != nil && "intl:" + $0.anchor! == link }).forEach {
            if $0.referencedFrom.contains(articleValue) == false {
              $0.referencedFrom.append(articleValue)
            }
          }
        }
        
        // EXTERNAL LINK
        
        // PATTERN 0 : RAW QUERY ...DEPRECATED
        
        if link.hasPrefix("/cgi-bin/idxrefer.cgi?"),
          let linkQuery = URL(string: link)?.query,
          linkQuery.range(of: ownerDocAddedPercentage!, options: .caseInsensitive, range: nil, locale: nil) != nil {
          
          let dict = linkQuery.getUrlQueryParameters()
          if let linkDestination = LinkDestination(queryDictionary: dict) {
            
            let anchor = linkDestination.anchor_f.compressedAnchor
            
            articles.filter({$0.anchor == anchor}).forEach {
              if $0.referencedFrom.contains(articleValue) == false {
                $0.referencedFrom.append(articleValue)
              }
            }
          }
          
        }else if link.hasPrefix(":") && link.hasSuffix( owner_h_file ) {
          // PATTERN 1 : PRE-COMPRESSED
          let comps = link.components(separatedBy: ":")
          if comps.count > 2 {
            let anchor = comps[1] as String!
            
            //LOOK FOR SELF ANCHOR
            if anchor == self.anchor && referencedFrom.contains(articleValue) == false {
              referencedFrom.append(articleValue)
            }
            
            articles.filter({$0.anchor == anchor}).forEach {
              
              if $0.referencedFrom.contains(articleValue) == false {
                $0.referencedFrom.append(articleValue)
              }
            }
          }
        }
        //
      }
    }
  }
  
  func dereferences() -> [DereferenceItem] {
    // Array [(title, SubArray)]
    // SubArray [ (title, linkURL) ]
    
    var array = [DereferenceItem]()
    
    
    var subArray: [DereferenceSubitem] = []
    let title = "[\(self.title(coded: false))を参照している条文]"
    for from in referencedFrom {
      var linkURL: String
      if from.anchor != nil {
        
        linkURL = "comcatalystwolaws:" + from.path + "@\(from.anchor!)@" + (from.doc.packageName! as NSString).deletingPathExtension
        
      }else {
        linkURL = "NO LINK"
      }
      let title = from.doc.title
      let detailedTitle = from.humanReadablePath
      subArray.append( (title, detailedTitle, linkURL, from.arttitle) )
    }
    
    array.append( (title, subArray) )
    
    for myArticle in articles {
      if myArticle.referencedFrom.count == 0 { continue }
      
      //LOOK FOR ARTICLE ANCHOR
      let title = "(\(myArticle.humanReadablePath)を参照している条文)"
      var subArray: [DereferenceSubitem] = []
      
      for from in myArticle.referencedFrom {
        
        var linkURL: String
        if from.anchor != nil {
          
          linkURL = "comcatalystwolaws:" + from.path + "@\(from.anchor!)@" + (from.doc.packageName! as NSString).deletingPathExtension
          
        }else {
          linkURL = "NO LINK"
        }
        let title = from.doc.title
        let detailedTitle = from.humanReadablePath
        subArray.append( (title, detailedTitle, linkURL, from.arttitle ) )
      }
      
      array.append( (title, subArray) )
      
    }
    return array
  }
  
  // MARK:- GENERATING
#if os(iOS)
  
  class func HTMLElements(in source: NSString, dump: Bool = false, buildRealmObjects: Bool = false) -> [ElementDescriptor]? {
  var dump = dump
  var buildRealmObjects = buildRealmObjects
  #if (arch(i386) || arch(x86_64)) && os(iOS)
  
  buildRealmObjects = true
  #endif
  
  var doc: HTMLDocument?
    do {
      let type = source.encodingType
      doc = try HTML(html: source as String, encoding: type)
    }catch let error as Error {
      print("** DOC WAS NIL **")
      return nil
    }
  
    func dumpMe(_ string: String) {
      if dump {
        print(string)
      }
    }
    
    // 刑事訴訟法２０条が特殊
    
    var elementLocators = NSMutableArray() //[ ElementLocator ]()
    var isNumber = false
    var divRange = NSMakeRange(0, source.length)
    var links: XPathObject? = nil
    autoreleasepool {
      links	= doc!.css("a, b, div, p") // SLICE WITH THESE TAGS
    }
    var insideDiv = false
    var debugCounter = 0
    
    for link in links! {
      autoreleasepool {
        var toHTML = NSMutableString(string: link.toHTML! )
        
        func replace(_ from: String,_ to: String) {
          toHTML.replaceOccurrences(of: from, with: to, options: [], range: NSMakeRange(0, toHTML.length))
        }
        
        replace("\n", "")
        
        if toHTML.range(of: "&a").location != NSNotFound {
          replace("&amp;", "&")
        }
        
        // TODO: 二　%E4%BA%8C
        /*
         NOTE: toHTML may contains kanji in a tag...
         <A HREF="comcatalystwolaws:(184)-12-二@1T184012002o17x+@S34HO121.html" ...
         
         THIS IS FIXED BY THIS ->
         　二　%E4%BA%8C
         　三　%E4%B8%89
         　四　%E5%9B%9B
         　五　%E4%BA%94
         　六　%E5%85%AD
         　七　%E4%B8%83
         　八　%E5%85%AB
         　九　%E4%B9%9D
         　十　%E5%8D%81
         　一　%E4%B8%80
         */
        if toHTML.range(of: "%E").location != NSNotFound {
          replace("%E4%BA%8C", "二")
          replace("%E4%B8%89", "三")
          replace("%E5%9B%9B", "四")
          replace("%E4%BA%94", "五")
          replace("%E5%85%AD", "六")
          replace("%E4%B8%83", "七")
          replace("%E5%85%AB", "八")
          replace("%E4%B9%9D", "九")
          replace("%E5%8D%81", "十")
          replace("%E4%B8%80", "一")
          replace("%E3%83%8E", "ノ")
        }
        
        if link.tagName == "div"  {
          let classTag = link["class"]
          if classTag == "arttitle" || classTag == "item" || classTag == "number" {
            
            insideDiv = true
            
            let searchRange = NSMakeRange(divRange.location, source.length - divRange.location)
            let range = source.range(of: toHTML as String, options: [.caseInsensitive], range: searchRange)
            
            debugCounter += 1
            
            if range.location != NSNotFound {
              divRange = range
            }else {
              dumpMe("\(toHTML) was not found")
            }
            
            // APPEND
            isNumber = (classTag == "number")
            var locator = ElementLocator(element: link, range: divRange, gou: isNumber)
            elementLocators.add( locator ) // DIV TAG
            
            return
          }
        }
        
        if link.tagName == "p"  {
          insideDiv = false
          var locator = ElementLocator(element: link)
          elementLocators.add( locator ) // DIV TAG
          
          return
        }
        
        //			if let _ = link["name"]  {
        //
        //				let searchRange = Range<String.Index>(divRange.startIndex ..< source.endIndex )
        //
        //				let range = source.rangeOfString(toHTML, options: [.CaseInsensitiveSearch], range: searchRange)
        //				var locator = ElementLocator()
        //				locator.element = link
        //				locator.range = range
        //				elementLocators.append( locator ) // DIV TAG
        //				continue
        //			}
        
        
        let searchRange = NSMakeRange(divRange.location, source.length - divRange.location )
        let range = source.range(of: toHTML as String, options: [.caseInsensitive], range: searchRange)
        
        var locator = ElementLocator(element: link, range: insideDiv ? range : nil, divRange: divRange, gou: isNumber)
        elementLocators.add( locator ) // DIV TAG
        
        // APPEND
        //			elementLocators.append( ElementLocator( element:link, range: insideDiv ? divRange : nil, gou:isNumber, subitems:nil) ) // A B TAG
      }
    }
    
    
    dumpMe("** rangeOfHTMLSnipped counter: \(debugCounter)")
    
    var articles: [ElementLocator] = []
    var arttitle = ""
    var subitems: [ElementLocator] = []
    var totalRange: NSRange? = nil
    var lastArticle: ElementLocator? = nil
    var elementDictionary: [ElementDescriptor] = []
  
    var indexNumber = 0
    var realmObjects: [String] = []
    func organise() {
      // ASSIGN subitems TO ONE OF articles
      if articles.count == 0 { return }
      
      var joubunAnchor: ElementLocator? = nil
      
      if subitems.count > 0 {
        joubunAnchor = subitems[0]
        subitems.remove(at: 0)
      }
      
      for item in subitems {
        guard let range: NSRange = item.range else { continue }
        
        articles = articles.map { (article: ElementLocator) -> ElementLocator in
          
          let articleRange = article.range
          
          // MATCH
          if articleRange != nil && NSEqualRanges(range, articleRange!) {
            article.addSubitem( item )
          }
            // ITEM INSIDE ARTICLE
          else if range.location >= articleRange!.location && NSMaxRange(range) <= NSMaxRange(articleRange!) {
            article.addSubitem( item )
          }
            // ITEM ADJACENT
          else if NSMaxRange(articleRange!) == range.location {
            article.addSubitem( item )
          }
          
          //ADDING [HREF] to articles
          
          if item.parent != nil && NSEqualRanges(item.parent!.range!, article.range!) {
            article.addSubitem( item )
          }
          
          return article
        }
      }
      
      dumpMe("--- ORGANISED - \(totalRange!.location)-\(totalRange!.length)")
      //DUMP
      if dump {
        
        joubunAnchor?.debugDescription()
        
        for article in articles {
          article.debugDescription()
        }
      }

  
      // MAKE OBJECTS
      
      if joubunAnchor != nil,
      let elementName = joubunAnchor!.element["name"],
      let elementObj = ElementDescriptor(anchor: elementName, range: totalRange, arttitle: arttitle) {

        for article in articles {
  
  
  #if DEBUG
  let articleObj = elementObj.addArticle(article.element.text, range: article.divRange, gou: article.gou, source: source.substring(with: article.divRange!) as NSString)

  #else
  let articleObj = elementObj.addArticle(article.element.text, range: article.divRange, gou: article.gou, source: nil)

  #endif
  
          if articleObj == nil { continue } // FAILED TO MAKE ARTICLE
          if article.subitems == nil { continue } // 'EMPTY' ARTICLE
          
          for subitem in article.subitems! {
            if let href = subitem.element["href"] {
              articleObj!.addLink(href)
            }
            
            if let name = subitem.element["name"] {
              articleObj!.anchor = name
            }
          }
        }
        
        elementDictionary.append(elementObj)
  
        if buildRealmObjects {
          realmObjects +=  elementObj.buildRealmObjects(index: &indexNumber)
        }
  
        if dump {
          // TEST ARCHIVING
          
          let archive = elementObj.dictionaryRepresentation

  
          let loadedObj = ElementDescriptor(from: archive)
          
          if loadedObj != nil {
            assert( loadedObj!.description == elementObj.description )
          }
          
          elementObj.debugPrint()
        }
      }
    }
    
    for locator in elementLocators {
      autoreleasepool {
        
        let range = (locator as! ElementLocator).range
        let elm = (locator as! ElementLocator).element
        let tagName = elm?.tagName
        if tagName == "p" {
          
          //ORGANIZE
          if articles.count > 0 {
            organise()
          }
          dumpMe("=====================")
          
          articles = []
          subitems = []
          lastArticle = nil
          totalRange = nil
          arttitle = ""
          return
        }
        
        if range == nil { return }
        
        if tagName == "div" {
          if totalRange == nil {
            totalRange = range
          }
          else {
            if NSMaxRange(totalRange!) < NSMaxRange(range!) {
              totalRange!.length += NSMaxRange(range!) - NSMaxRange(totalRange!)
            }
            
            if totalRange!.location > range!.location {
              totalRange!.location = range!.location
            }
          }
          
          if elm?["class"] == "arttitle",
          let text = elm?.text {
            arttitle = text.removingParenthesises()
            if dump {
              print("arttitle " + arttitle)
            }
          }
          return
        }
        
        if let _ = elm?["name"] {
          subitems.append( locator as! ElementLocator )
          //				dumpMe("[🔱] \(name) \(range)"  ) <---- THIS CAUSED A CRASH BUT WHY???
          
          return
        }
        
        if let href = elm?["href"] {
          if href.hasPrefix("highlight:") { return }
          
          if href.hasPrefix("comcatalystwolaws") {
            //dumpMe("[🔵] \(href) " )
            
          }else {
            dumpMe("[HREF] \(range!)" )
            if lastArticle != nil {
              let elementToAdd = locator as! ElementLocator
              elementToAdd.parent = lastArticle!
              subitems.append( elementToAdd )
              
            }
            else {
              subitems.append( locator as! ElementLocator )
            }
          }
          return
        }
        
        dumpMe( "\(elm!.text!)-\(NSStringFromRange(range!)) " )
        
        articles.append( locator as! ElementLocator )
        lastArticle = locator as? ElementLocator
      }
    }
    //ORGANIZE
    if articles.count > 0 {
      organise()
    }
  
  
  if buildRealmObjects == true {
  let docTitle = doc!.title ?? ""
  
  let string = (realmObjects as NSArray).componentsJoined(by: "\n")
  try! string.write(toFile:"/Users/masa/Desktop/Built Realm Objects \(docTitle).txt", atomically: false, encoding: .utf16)
  
  
  }
  
  links = nil
  doc = nil
  
    return elementDictionary
  }
  
#endif
}

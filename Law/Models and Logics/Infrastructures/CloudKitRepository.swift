//
//  CloudKitRepository
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 30/12/14.
//  Copyright (c) 2014 Catalystwo. All rights reserved.
//

import Foundation
import CloudKit

//
// REPOSITORY
//

// MARK: Cloud Kit

class CloudKitRepository : NSObject {
	
	var container_: CKContainer? = nil
	var database_: CKDatabase? = nil
	var privateDatabase_: CKDatabase? = nil
	var hasPrivateAccount_: Bool? = nil
	var downloaders: [CKRecordID: Double/*progress*/] = [:]
	
	// MARK: -
	
  static var shared = CloudKitRepository()
	
	class func updateIfHasPrivateAccount()  {
		shared.hasPrivateAccount({(hasPrivateAccount: Bool)->Void in})
	}
	
	func hasPrivateAccount(_ callback: @escaping ((_ hasPrivateAccount: Bool)->Void))  {
		
		hasPrivateAccount_ = nil
		
		container_?.accountStatus { (status, error) -> Void in
			
			DispatchQueue.main.async {
				
				if .available == status || .couldNotDetermine == status {
					self.hasPrivateAccount_ = true
					callback(true)
					
				}else if .restricted == status {
					self.hasPrivateAccount_ = false
					callback(false)
					
				}else {
					callback(false)
				}
			}
		}
	}
	
	override init() {
		super.init()
		
		container_  = CKContainer(identifier: "iCloud.com.catalystwo.Laws")
		
		if container_ != nil {
			
			container_?.accountStatus { (status, error) -> Void in
				
				print("CloudKit Status \(status)")
				
				if .available == status || .couldNotDetermine == status {
					self.hasPrivateAccount_ = true
				}
				
				if .restricted == status {
					self.hasPrivateAccount_ = false
				}
				
				if CKAccountStatus.noAccount == status {
					self.hasPrivateAccount_ = false
				}
			}
			
			database_ = container_!.publicCloudDatabase
			privateDatabase_ = container_!.privateCloudDatabase
		}
	}
	
	// Increment count, and upload to CloudKit if necessary
  func cloudKit( _ md5: String, title: String, edition: String, creatorUUID: String?,  packageURL: URL, completionHandler: ((_ success: Bool) -> Void)? ) {
		
		if hasPrivateAccount_ != true {
			completionHandler?(false)
			return
		}
		
		let predicate = NSPredicate( format: "(md5 == %@) && (title == %@)",  md5 , title)
		let query = CKQuery(recordType: RecordType.lawDocPackage, predicate: predicate)
		let queryOperation = MNTimeoutQueryOperation(query: query)
		queryOperation.resultsLimit = 1
		queryOperation.desiredKeys = [RecordType.LawDocPackageKey.md5, RecordType.LawDocPackageKey.title, RecordType.LawDocPackageKey.count]
		queryOperation.qualityOfService = .userInitiated
		queryOperation.allowsCellularAccess = true

		let results = NSMutableArray()
		queryOperation.recordFetchedBlock = { (record: CKRecord) -> Void in
			results.add(record)
		}
		
		queryOperation.queryCompletionBlock = { (cursor: CKQueryCursor?, error: Error?) -> Void in
			
			print("Found \(results.count)")
			
			if results.count == 0 {
				
				let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)
				queue.async {
          self.upload( md5, title: title, edition: edition, packageURL: packageURL, isPrivate: false, creator: nil, creatorUUID: creatorUUID, overwritingRecordID: nil, completionHandler: completionHandler )
				}
				
			}else {
				
				if self.hasPrivateAccount_ == true {
					
					let record = results[0] as! CKRecord
					let count = record[RecordType.LawDocPackageKey.count] as? NSNumber
					var countInt: Int = 0
					
					if count != nil { countInt = count!.intValue }
					countInt += 1
					
					record[RecordType.LawDocPackageKey.count] = NSNumber(value: countInt)
					
					let saveOperation = CKModifyRecordsOperation(recordsToSave: [record], recordIDsToDelete: nil)
					saveOperation.savePolicy = .changedKeys
					saveOperation.qualityOfService = .userInitiated
					saveOperation.queuePriority = .veryHigh
					saveOperation.modifyRecordsCompletionBlock = { (records: [CKRecord]?, recordID: [CKRecordID]?, error: Error?) -> Void in
						if error != nil { print("saving error \(error!.localizedDescription)") }
					}
					
					self.database_?.add(saveOperation)
				}
				
				completionHandler?( (error == nil) )
			}
			
			if error != nil { print("\(error)") }
		}
		
		database_?.add(queryOperation)
	}
	
	
  func upload( _ md5: String, title: String, edition: String, packageURL: URL, isPrivate: Bool, creator: String?, creatorUUID: String?, overwritingRecordID docRecordID: CKRecordID!, completionHandler: ((_ success: Bool) -> Void)? ) {
		
		if hasPrivateAccount_ != true {
			completionHandler?(false)
			return
		}
		
		/*
		var compressWithBZ2 = false
		
		let november:NSTimeInterval = 467_982_000.0 // November 1... bz2 compression starts
		
		if NSDate().timeIntervalSinceReferenceDate > november {
			compressWithBZ2 = true
		}
		*/
		
		var compressWithBZ2 = true
		
		
		// Create as new
		
		func writeForRecord( _ record: CKRecord ) {
			
			record[RecordType.LawDocPackageKey.md5] = md5 as NSString
			record[RecordType.LawDocPackageKey.title] = title as NSString
			record[RecordType.LawDocPackageKey.edition] = edition as NSString
			
			if isPrivate == true && creatorUUID?.isEmpty == false {
				record[RecordType.LawDocPackageKey.creatorUUID] = creatorUUID! as NSString
				
				if creator != nil {
					record[RecordType.LawDocPackageKey.creator] = creator!as NSString
				}
			}
			
			let name = ((packageURL.lastPathComponent as NSString?)?.deletingPathExtension as NSString?)?.deletingPathExtension
			
			// Check fatal error
			assert( name?.isEmpty == false, "** name shouldn't be empty ** " )
			
			record[RecordType.LawDocPackageKey.packageBundle] = name as NSString?
			
			// Prepare asset
			
			// Pattern 0, BZIP
			var assetURL: URL? = nil
			
			if compressWithBZ2 == true {
				
				let data = FileRepository.shared.compressedFromPackage( packageURL )
				
				if data != nil {
					
					let component = packageURL.lastPathComponent + ".bz2"
					
					let bz2Path = (NSTemporaryDirectory() as NSString).appendingPathComponent(component)
					assetURL = URL(fileURLWithPath: bz2Path)
					
					do {
						try data?.write(to: assetURL!, options: [])
					}catch {
						print("failed to write at \(assetURL)")
						assetURL = nil // Failed to write... give up
					}
				}
			}
			
			// Pattern 1, ZIP
			// compressWithBZ2 is false
			if assetURL == nil {
				if let zipArchivePath = ObjectiveCClass.zipFile(at: packageURL, completion: nil) {
					assetURL = URL(fileURLWithPath: zipArchivePath)
				}
			}
			
			if assetURL == nil {
				completionHandler?(false) // FILE ERROR
				return
			}
			
			let asset = CKAsset(fileURL: assetURL! )
			
			record.setObject(asset, forKey: "package")
			
			let saveOperation = CKModifyRecordsOperation(recordsToSave: [record], recordIDsToDelete: nil)

			saveOperation.savePolicy = .changedKeys
			saveOperation.qualityOfService = .userInitiated
			saveOperation.queuePriority = .veryHigh

			saveOperation.modifyRecordsCompletionBlock = { (records: [CKRecord]?, recordID: [CKRecordID]?, error: Error?) -> Void in

				if error != nil {
					print("CKRecord \(error!.localizedDescription)")
					completionHandler?(false)
					
				} else{
					print("Done")
					
					completionHandler?(true)
				}
				
				do {
					// Clean up
					try FileManager.default.removeItem(at: assetURL!)
				} catch _ {
				}
			}
	
			database_?.add(saveOperation)
		}
		
		if docRecordID != nil {
			
			let operation = CKFetchRecordsOperation(recordIDs: [docRecordID!])
			operation.qualityOfService = .userInitiated
			operation.queuePriority = .veryHigh
			operation.allowsCellularAccess = true
			operation.perRecordProgressBlock = {(theRecordID: CKRecordID, progress: Double) -> Void in }
			operation.fetchRecordsCompletionBlock = { (recordsByRecordID: [CKRecordID : CKRecord]?,  error: Error?) -> Void in
				
				if let theRecord = recordsByRecordID?[docRecordID!] {
					writeForRecord(theRecord)
					
				}else {
					completionHandler?(false)
				}
			}
			
			database_?.add(operation)
			
		}else {
			let record = CKRecord(recordType: RecordType.lawDocPackage)
			writeForRecord(record)
		}
	}
	
	
	// MARK:- Public method... called from outside
	
	func downloadSourceWithRecordID( _ recordID: CKRecordID, progress progressHandler: @escaping (_ progress: Double) -> Void, completion: @escaping ((_ success: Bool, _ data: Data?, _ docRecordID: CKRecordID?, _ error: Error?) -> Void) ) {
    
    /*
     Download package based on record's record-ID
     */
    
    assert( Thread.isMainThread == true, "** Call on main thread! **")
    
    downloaders[recordID] = 0
		
		let operation = CKFetchRecordsOperation(recordIDs: [recordID])
		
		operation.qualityOfService = .userInitiated
		operation.queuePriority = .veryHigh
		operation.allowsCellularAccess = true
		operation.perRecordProgressBlock = { [unowned self] (theRecordID: CKRecordID, progress: Double) -> Void in
			self.downloaders[recordID] = progress
      DispatchQueue.main.async(execute: { progressHandler(progress) })
		}
		
		operation.fetchRecordsCompletionBlock = { [unowned self] (recordsByRecordID: [CKRecordID : CKRecord]?,  error: Error?) -> Void in
			
			let theRecord = recordsByRecordID?[recordID]
			let docRecordID = theRecord?.recordID
			
			DispatchQueue.main.async {
				var error = error
        var decompressedData: Data! = nil
        var success = false
        
        defer { completion(success, decompressedData, docRecordID, error ) }

				// Finish downlaoding
				self.downloaders[recordID] = nil
				
				if theRecord == nil || error != nil { return }
				
        guard let URL = (theRecord!.object(forKey: "source") as? CKAsset)?.fileURL else { return }

				// decompress data
				let data = try? Data(contentsOf: URL)
				do {
					decompressedData = try BZipCompression.decompressedData(with: data)
				} catch let error1 {
					
					// Error
          error = error1
					return
				}
				success = true
				return
			}
		}
		
		privateDatabase_?.add(operation)
		return
	}
	
	func downloadAssetWithRecordID( _ recordID: CKRecordID, progress progressHandler: @escaping ((_ progress: Double) -> Void), completion: @escaping (_ success: Bool, _ error: Error?) -> Void ) {
		
		/*
     Download package based on record's record-ID
     */
    
    assert( Thread.isMainThread == true, "** Call on main thread! **")
    
    self.downloaders[recordID] = 0 // dummy
    
    let operation = CKFetchRecordsOperation(recordIDs: [recordID])
    
    operation.queuePriority = .veryHigh
    operation.qualityOfService = .userInitiated
    operation.allowsCellularAccess = true
    operation.perRecordProgressBlock = { (theRecordID: CKRecordID, progress: Double) -> Void in
      DispatchQueue.main.async { progressHandler(progress) }
    }
    operation.fetchRecordsCompletionBlock = { (recordsByRecordID: [CKRecordID : CKRecord]?,  error: Error?) -> Void in
      
      DispatchQueue.main.async { progressHandler(1.0) }
      
      print("** record loaded ** \(Date().timeIntervalSinceReferenceDate)")
      
      self.downloaders[recordID] = nil
      
      guard let theRecord = recordsByRecordID?[recordID], error == nil else {
        DispatchQueue.main.async { completion(false, error) }
        return
      }
      
      guard let URL = (theRecord[RecordType.LawDocPackageKey.package] as? CKAsset)?.fileURL else {
        DispatchQueue.main.async { completion(false, error) }
        return
      }
      
      // Increase download count
      
      if self.hasPrivateAccount_ == true {
        
				let count = theRecord[RecordType.LawDocPackageKey.count] as? NSNumber
				var countInt: Int = 0
				
				if count != nil { countInt = count!.intValue }
				countInt += 1
		
				theRecord[RecordType.LawDocPackageKey.count] = NSNumber(value: countInt)
				
				let saveOperation = CKModifyRecordsOperation(recordsToSave: [theRecord], recordIDsToDelete: nil)
				saveOperation.savePolicy = .changedKeys
				saveOperation.qualityOfService = .userInitiated
				saveOperation.queuePriority = .veryHigh

				saveOperation.modifyRecordsCompletionBlock = { (records: [CKRecord]?, recordID: [CKRecordID]?, error: Error?) -> Void in
					if error != nil { print("saving error \(error!.localizedDescription)") }
				}
				
				self.database_?.add(saveOperation)
			}
			
			print("** download finished ** \(Date().timeIntervalSinceReferenceDate)")
			
			///
			// Decompress Main
			///
			
			
			var archiveWasBZip2 = false
			
			// Pattern 1, unzip
			
			var packageURL: URL? = nil
			
			let t0 = Date().timeIntervalSinceNow
			packageURL =  FileRepository.shared.unzipAt( URL.path )
			let t1 = Date().timeIntervalSinceNow
			
			if packageURL == nil {
				
				print("==BZ2== after delay \(t1-t0)")
				// Pattern 0, un bzip
        guard let data = try? Data(contentsOf: URL) else {
					completion(false, LawError.fileError)
					return
        }
				archiveWasBZip2 = true
				packageURL = FileRepository.shared.decompressBZip(data, in: FileRepository.shared.documentFolderURL)
			}
			
			/*
			files must be like this
			/var/mobile/Containers/Data/Application/F419A06A-3DB5-41FC-92E0-E25994EE68E0/Documents/H11HO128.html.comcatalystwopackage/Contents.htmlcompressed
			
			/var/mobile/Containers/Data/Application/F419A06A-3DB5-41FC-92E0-E25994EE68E0/Documents/H11HO128.html.comcatalystwopackage/Info.plist
			
			/var/mobile/Containers/Data/Application/F419A06A-3DB5-41FC-92E0-E25994EE68E0/Documents/H11HO128.html.comcatalystwopackage/Toc.plist
			*/

			if packageURL == nil {
				
				DispatchQueue.main.async { completion(false, LawError.fileError ) }
				return
			}
			
			print("** unzipping finished ** \(Date().timeIntervalSinceReferenceDate)")
			
			/* DEBUG */
			/*
			
			
			Zipped Asset	301_341
			BZip				158_193 (compress XML)
			175_816 Binary
			152_970 JSON
			BZip decomp		2_472_176
			
			
			COMPRESSION_LZ4	493_424.00
			COMPRESSION_LZMA	154_208.00
			*/
			
			
			// LZMA
			
			//							do {
			//								let contentURL = packageURL.URLByAppendingPathComponent( "Contents.htmlcompressed" )
			//
			//								let contentData = NSData(contentsOfURL: contentURL)?.inflate()
			//								let contentHtml = NSMutableString(data: contentData!, encoding: NSUTF8StringEncoding)
			//
			//								let infoPlistURL = packageURL.URLByAppendingPathComponent( "Info.plist" )
			//								let plistData = NSData(contentsOfURL: infoPlistURL)
			//								let infoPlist = try NSPropertyListSerialization.propertyListWithData(plistData!, options: [.Immutable], format: nil) as? NSDictionary
			//
			//
			//								let tocURL = packageURL.URLByAppendingPathComponent( "Toc.plist" )
			//								let tocData = NSData(contentsOfURL: tocURL)
			//
			//								let tocPlist:NSArray
			//
			//								tocPlist = try NSPropertyListSerialization.propertyListWithData(tocData!, options: [.Immutable], format: nil) as! NSArray
			//
			//
			//								let dict = [ "Contents" : contentHtml as NSString!,  "Info.plist" : infoPlist as NSDictionary!, "Toc.plist" : tocPlist as NSArray! ]
			//
			//
			//								let data = try NSJSONSerialization.dataWithJSONObject(dict, options: [])
			//let temp = (NSTemporaryDirectory() as NSString).stringByAppendingPathComponent("lzma")
			//
			//								data.writeToFile(temp, atomically: false)
			//
			//								let outTemp = (NSTemporaryDirectory() as NSString).stringByAppendingPathComponent("lzmaOut")
			//
			//								let outURL = NSURL(fileURLWithPath: outTemp)
			//								ObjectiveCClass.compress( NSURL(fileURLWithPath: temp), to:outURL)
			//
			//								let outData = NSData(contentsOfURL: outURL)
			//								print("outData \(outData?.length)")
			//
			//							} catch {
			//
			//							}
			
			
			//							// BZip
			//							do {
			//								let contentURL = packageURL.URLByAppendingPathComponent( "Contents.htmlcompressed" )
			//
			//								let contentData = NSData(contentsOfURL: contentURL)?.inflate()
			//								let contentHtml = NSMutableString(data: contentData!, encoding: NSUTF8StringEncoding)
			//
			//								let infoPlistURL = packageURL.URLByAppendingPathComponent( "Info.plist" )
			//								let plistData = NSData(contentsOfURL: infoPlistURL)
			//								let infoPlist = try NSPropertyListSerialization.propertyListWithData(plistData!, options: [.Immutable], format: nil) as? NSDictionary
			//
			//
			//								let tocURL = packageURL.URLByAppendingPathComponent( "Toc.plist" )
			//								let tocData = NSData(contentsOfURL: tocURL)
			//
			//								let tocPlist:NSArray
			//
			//								tocPlist = try NSPropertyListSerialization.propertyListWithData(tocData!, options: [.Immutable], format: nil) as! NSArray
			//
			//
			//								let dict = [ "Contents" : contentHtml as NSString!,  "Info.plist" : infoPlist as NSDictionary!, "Toc.plist" : tocPlist as NSArray! ]
			//
			//
			//								let data = try NSJSONSerialization.dataWithJSONObject(dict, options: [])
			//
			//								print("Compress")
			//								print(NSDate())
			//									let compressedData = try! BZipCompression.compressedDataWithData(data, blockSize: BZipDefaultBlockSize, workFactor: BZipDefaultWorkFactor)
			//
			//								print("BZIP \(compressedData.length)")
			//								let t0 = NSDate().timeIntervalSinceReferenceDate
			//
			//								let decompressedData = try! BZipCompression.decompressedDataWithData(compressedData)
			//								let t1 = NSDate().timeIntervalSinceReferenceDate
			//
			//								print("Decompressed \(decompressedData.length) \(t1-t0)")
			//
			//							}catch {
			//
			//							}
			//
			
			// Delete update flag
			FilePackage.shared.resetUpdates( packageURL! )
			let _ = packageURL!.setExcludedFromBackup()
			
			
			
			//
			// Update CloudKit ... update package bundle
			//
			
			let packageFilename = theRecord[RecordType.LawDocPackageKey.packageBundle] as? String
			
			if (packageFilename == nil) || (packageFilename?.isEmpty == true) {
				
				let name = ((packageURL!.lastPathComponent as NSString?)?.deletingPathExtension as NSString?)?.deletingPathExtension
				
				if name?.isEmpty == false {
					
					theRecord[RecordType.LawDocPackageKey.packageBundle] = name as NSString?
					
					let saveOperation = CKModifyRecordsOperation(recordsToSave: [theRecord], recordIDsToDelete: nil)
					saveOperation.savePolicy = .changedKeys
					saveOperation.qualityOfService = .userInitiated
					saveOperation.queuePriority = .veryHigh

					saveOperation.modifyRecordsCompletionBlock = nil
					self.database_?.add(saveOperation)
					
				}
			}
			
			//
			// Update archive
			//
			
			if archiveWasBZip2 == false {
				//let november:NSTimeInterval = 467_982_000.0 // November 1... bz2 compression starts
				
				if true /*NSDate().timeIntervalSinceReferenceDate > november*/ {
					
					
					// Convert archive to BZip2
					
					if let data = FileRepository.shared.compressedFromPackage( packageURL! ) {
						let component = (packageURL!.lastPathComponent) + ".bz2"
						let bz2Path = (NSTemporaryDirectory() as NSString).appendingPathComponent(component)
						let assetURL = Foundation.URL(fileURLWithPath: bz2Path)
						
						do {
							try data.write(to: assetURL, options: [])
							
							let asset = CKAsset(fileURL: assetURL )
							theRecord[RecordType.LawDocPackageKey.package] = asset
							
							let saveOperation = CKModifyRecordsOperation(recordsToSave: [theRecord], recordIDsToDelete: nil)
							
							saveOperation.savePolicy = .changedKeys
							saveOperation.qualityOfService = .userInitiated
							saveOperation.queuePriority = .veryHigh
							saveOperation.modifyRecordsCompletionBlock = { (records: [CKRecord]?, recordID: [CKRecordID]?, error:Error?) -> Void in
								
								print(error)
								
								do {
									// Clean up
									try FileManager.default.removeItem(at: assetURL)
								} catch _ {
								}
							}
							
							self.database_?.add(saveOperation)
							
							print("updating archive")
							
						}catch {
							
						}
					}
				}
			}
			
			print("** loading package ** \(Date().timeIntervalSinceReferenceDate)")
			
			// Add Package
			
      let ( myPlist, _, _) = FilePackage.shared.loadPackage(at: packageURL, onlyPlist: false, buildTOC2: true, overwriteTOC2: true /* THIS BUILD TOC2 */ )
			
			//					let sourceURLString = myPlist?["sourceURL"] as! String?
			//
			//					if sourceURLString != nil {
			//					let dict = processQuery(sourceURLString!)
			//						let h_name = dict["H_NAME"] as! String?
			//
			//						println("\(sourceURLString)")
			//						println("\(h_name)")
			//						println("\(dict)")
			//
			//					}
			
			let fileEntry = FileEntry( packageURL: packageURL!, anchor: nil, elementId: nil, plist: myPlist!)

			
			FilePackage.shared.addToIndex( fileEntry )

			print("** finishing ** \(Date().timeIntervalSinceReferenceDate)")

      let _ = FilePackage.shared.loadPackage(at: packageURL, onlyPlist: false)
			
			DispatchQueue.main.async {
				completion(true, nil )
			}
			return
		}
		
		database_?.add(operation)
		return
	}
	
	func downloadRecord( _ dictionary: DocItem , progress: @escaping ((_ progress: Double)->Void), completion: @escaping (_ success: Bool, _ error: Error?) -> Void )  {
		
		if GCNetworkReachability.forInternetConnection().isReachable() == false {
			completion(false, LawError.offline)
			return
		}
		
		let title = dictionary.originalTitle
		let md5 = dictionary.md5

		// 1. md5 is not nil ... search with title and md5
		// 2. search with packageName and use the latest version
		
		var predicate: NSPredicate? = nil
		
		if md5?.isEmpty == false {
			predicate = NSPredicate( format: " (%K == %@) && (%K == %@) ", RecordType.LawDocPackageKey.md5, md5!, RecordType.LawDocPackageKey.title, title )

		}else if let packageName = dictionary.packageName {
			let suffix = ".html.comcatalystwopackage"
			
			if packageName.characters.count <= suffix.characters.count {
				
				// Unknown error
				completion(false, LawError.unknown)
				return
			}
			
			let packageNameToUse = (packageName as NSString?)!.substring(with: NSMakeRange(0, packageName.characters.count - suffix.characters.count ))
			predicate = NSPredicate( format: " %K == %@ ", RecordType.LawDocPackageKey.packageBundle, packageNameToUse)
			
			if let edition = dictionary.edition {
				let editionPredicate = NSPredicate( format: " %K == %@ ", RecordType.LawDocPackageKey.edition, edition )
				predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate!, editionPredicate])
			}
			
		}else {
			
			// Unknown error
			completion(false, LawError.unknown)
			return
		}
		
		let query = CKQuery(recordType: RecordType.lawDocPackage, predicate: predicate!)
		let queryOperation = MNTimeoutQueryOperation(query: query, timeOutInterval: nil)
		var results: [CKRecord] = []
		
		queryOperation.desiredKeys = []
		queryOperation.recordFetchedBlock = { (record: CKRecord) -> Void in
			results.append(record)
		}
		queryOperation.queryCompletionBlock = { (cursor: CKQueryCursor?, error: Error?) -> Void in
			
			/*
			print("Found \(results.count)")
			print("error \(error?.code)")
			
			let desc = error?.userInfo["CKErrorDescription"] as! String?
			print("error?.userInfo \(error?.userInfo)")
			print("error?.domain \(error?.domain)")
			*/

			DispatchQueue.main.async {
				
				if error == nil {
					
					if results.count == 0 {
						completion(false, LawError.notFound)
						return
					}
					
					// Choose latest record then download
					
					var mostRecentRecord: CKRecord? = nil
					
					for record in results {
						if mostRecentRecord == nil {
							mostRecentRecord = record
							continue
						}
						
						let date = record.modificationDate
						let date2 = mostRecentRecord!.modificationDate
						
						if date != nil && date2 != nil {
							let compareResult = date!.compare( date2! )
							
							if compareResult == ComparisonResult.orderedDescending {
								mostRecentRecord = record
							}
						}
					}
					
					if mostRecentRecord == nil {
						completion(false, LawError.unknown)
						return
					}
					print("download")
					
					self.downloadAssetWithRecordID( mostRecentRecord!.recordID,  progress: progress, completion: completion )
					
					return
				}
				
				switch error! {
					
				case CKError.Code.networkUnavailable:
					completion(false, LawError.iCloudOverCellularNotAllowed)
					return
				case CKError.Code.permissionFailure, CKError.Code.internalError:
					completion(false, LawError.accountMissing)
					return
				case CKError.Code.operationCancelled:
          completion(false, LawError.timeOut)
          return
        default:
          completion(false, error)
					return
				}
			}
		}
		
		database_?.add(queryOperation)
	}
	
	
	func fetchRecord(withPackageBundle packageBundle: String, completion: @escaping (_ results: [CKRecord], _ error: Error?) -> Void ) {
		
		fetchRecord( withPackageBundle: packageBundle, desiredKeys: [RecordType.LawDocPackageKey.md5, RecordType.LawDocPackageKey.title], completion: completion )
	}
	
	
	func fetchRecord(withPackageBundle packageBundle: String, desiredKeys: [String], completion: @escaping (_ results: [CKRecord], _ error: Error?)->Void ) {
		
		//["md5", "title", "packageBundle", "count" ]
		
		let predicate = NSPredicate( format: "%K == %@", RecordType.LawDocPackageKey.packageBundle, packageBundle)
		
		let query = CKQuery(recordType: RecordType.lawDocPackage, predicate: predicate)
		let queryOperation = MNTimeoutQueryOperation(query: query, timeOutInterval: nil)
		let sort = NSSortDescriptor(key: RecordType.LawDocPackageKey.creationDate, ascending: false)
		var results: [CKRecord] = []
		
		query.sortDescriptors = [sort]
		
		queryOperation.desiredKeys = desiredKeys
		queryOperation.recordFetchedBlock = { (record: CKRecord) -> Void in
			results.append(record)
		}
		
		queryOperation.queryCompletionBlock = { (cursor: CKQueryCursor?, error: Error?) -> Void in
			
			results.sort {
				
				let date1 = $0[RecordType.LawDocPackageKey.creationDate] as? Date
				let date2 = $1[RecordType.LawDocPackageKey.creationDate] as? Date
				
				if date1 == nil  { return false }
				if date2 == nil  { return true }

				return  date1!.compare( date2! ) == .orderedDescending
			}
			
			completion(results, error)
		}
		
		database_?.add(queryOperation)
	}
	
	
	func fetchMyPackageBundles( _ completion: @escaping (_ results: [CKRecord], _ error: Error?) -> Void ) {
		
		let predicate = NSPredicate( format: "TRUEPREDICATE" )
		let query = CKQuery(recordType: RecordType.myPackageBundle, predicate: predicate)
		let queryOperation = MNTimeoutQueryOperation(query: query, timeOutInterval: nil)
		let sort = NSSortDescriptor(key: RecordType.MyPackageBundleKey.modificationDate, ascending: false)
		var results: [CKRecord] = []
		query.sortDescriptors = [sort]
		queryOperation.desiredKeys = [ RecordType.MyPackageBundleKey.title, RecordType.MyPackageBundleKey.packageBundle, RecordType.MyPackageBundleKey.creatorUUID ]
		queryOperation.recordFetchedBlock = { (record: CKRecord) -> Void in
			
			results.append(record)
		}
		
		queryOperation.queryCompletionBlock = { (cursor: CKQueryCursor?, error: Error?) -> Void in
			

			
			results.sort {
				
				let date1 = $0[RecordType.MyPackageBundleKey.modificationDate] as? Date
				let date2 = $1[RecordType.MyPackageBundleKey.modificationDate] as? Date
				if date1 == nil { return true }
				if date2 == nil { return false }
				
				return  date1!.compare( date2! ) == .orderedDescending
      }
      
      if  error == nil {
        completion(results, nil)
        return
      }
      
      switch error! {
      case CKError.Code.notAuthenticated : completion(results, LawError.userDisabledCloudKit)
      default: completion(results, error)
      }
      
			return
		}
		
		privateDatabase_?.add(queryOperation)
	}
	
	func fetch( _ string: String, tokenSearch: Bool, completion: @escaping (_ results: [CKRecord], _ cursor: CKQueryCursor?, _ error: Error?) -> Void ) {
		
		let predicate1 = NSPredicate( format: "allTokens TOKENMATCHES[cdl] %@", string)
		let predicate2 = NSPredicate( format: "%K beginswith %@", RecordType.LawDocPackageKey.title, string)
		
		var predicate = predicate1
		
		if tokenSearch == false { predicate = predicate2 }
		
		// SPECIAL QUERY FOR DEBBING
		if string == "西向く侍" {
			predicate = NSPredicate( format: " !(%K beginswith 'S') && !(%K beginswith 'H') && !(%K beginswith 'M') && !(%K beginswith 'T')", RecordType.LawDocPackageKey.packageBundle, RecordType.LawDocPackageKey.packageBundle, RecordType.LawDocPackageKey.packageBundle, RecordType.LawDocPackageKey.packageBundle)
		}
		
		if string == "未圧縮検索" {
			predicate = NSPredicate( format: " %K < %@", RecordType.LawDocPackageKey.modificationDate, Date(timeIntervalSinceReferenceDate: 467_982_000.0) as CVarArg)
		}
		
		let query = CKQuery(recordType: RecordType.lawDocPackage, predicate: predicate)
		let queryOperation = MNTimeoutQueryOperation(query: query, timeOutInterval: nil)
		let sort = NSSortDescriptor(key: RecordType.LawDocPackageKey.creationDate, ascending: false)
		var results: [CKRecord] = []
		
		query.sortDescriptors = [sort]
		
		queryOperation.resultsLimit = 200
		queryOperation.desiredKeys = [RecordType.LawDocPackageKey.md5, RecordType.LawDocPackageKey.title, RecordType.LawDocPackageKey.edition, RecordType.LawDocPackageKey.creator, RecordType.LawDocPackageKey.creatorUUID]
		
		queryOperation.recordFetchedBlock = { (record: CKRecord) -> Void in
			results.append(record)
		}
		
		queryOperation.queryCompletionBlock = { (cursor: CKQueryCursor?, error: Error?) -> Void in
			
			print("Found \(results.count) cursor\(cursor)")
			
			results.sort { (r1: CKRecord, r2: CKRecord) -> Bool in
				
				let creationDate1: Date? = r1.creationDate
				let creationDate2: Date? = r2.creationDate

				if creationDate1 == nil { return false }
				if creationDate2 == nil { return true }
				
				return  creationDate1!.compare( creationDate2! ) == .orderedDescending
				
				//TODO: Set Swift Optimization 'None'... Xcode 7.1 bug?
			}
			
			
			if error != nil {
				print("\(error)")
				
				var thisError: Error? = nil
				
				switch error! {
				case CKError.Code.networkUnavailable: thisError = LawError.iCloudOverCellularNotAllowed
					
					/*"Couldn't send a valid signature" == desc || "Received a didCompleteWithError without receiving a response from the server" == desc*/
				case CKError.Code.internalError, CKError.Code.permissionFailure: thisError = LawError.accountMissing
				case CKError.Code.operationCancelled: thisError = LawError.timeOut
				default: thisError = error
				}
				
				completion([], cursor, thisError)
			}
			
			// Special
			if string == "西向く侍" {
				completion(results, cursor, nil)
				return
			}
			
			if string == "未圧縮検索" {
				completion(results, cursor, nil)
				return
			}
			
			// Sort by title
			
			results.sort { (record1: CKRecord, record2: CKRecord) -> Bool in
				
				let title1 = record1[RecordType.LawDocPackageKey.title] as? NSString ?? ""
				let title2 = record2[RecordType.LawDocPackageKey.title] as? NSString ?? ""
				
				if title1 == title2 {
					if record1.creationDate!.timeIntervalSinceReferenceDate > record2.creationDate!.timeIntervalSinceReferenceDate { return true }
					return false
				}
				
				if title1.compare( title2 as String! ) == .orderedAscending { return true }
				return false
			}
			
			// md5, titleが同じものを抽出し、最新以外の全てを削除フラグ
			
			var uniqueNameAndMD5s: [(title: String, md5: String)] = []
			var nonUniqueNameAndMD5s: [(title: String, md5: String)] = []
			
			var nonUniqueRecords: [CKRecord] = []
			
			let _ /*uniqueResults*/ = results.filter {
				
				let title = $0[RecordType.LawDocPackageKey.title] as? String ?? ""
				let md5 = $0[RecordType.LawDocPackageKey.md5] as? String ?? ""
				
				for hoge in uniqueNameAndMD5s {
					
					if( hoge.title == title && hoge.md5 == md5 ) {
						
						if nonUniqueRecords.contains($0) == false {
							nonUniqueRecords.append($0)
						}
						
						var alreadyInTheArray = false
						for ( title: hogeTitle, md5: hogeMD5 ) in nonUniqueNameAndMD5s {
							
							if hogeTitle == title && hogeMD5 == md5 {
								alreadyInTheArray = true
							}
						}
						
						if alreadyInTheArray == false {
							nonUniqueNameAndMD5s += [( title: title, md5: md5 )]
						}
						
						return false
					}
				}
				uniqueNameAndMD5s += [( title: title, md5: md5 )]
				
				return true
			}
			
			// Delete process
			
			let toBeDeleted = self.deleteNonUniqueRecords(nonUniqueNameAndMD5s, inResults: results)
			
			// FILTER results
			
			let filtered = results.filter {
				
				if toBeDeleted.contains( $0 ) { return false }
				
				let title = $0[RecordType.LawDocPackageKey.title] as? NSString ?? ""
				let range = title.range(of: string, options: [.caseInsensitive, .widthInsensitive])
				if range.length != 0 { return true }
				
				let edition = $0[RecordType.LawDocPackageKey.edition] as? NSString ?? ""
				let range2 = edition.range(of: string, options: [.caseInsensitive, .widthInsensitive])
				if range2.length == 0 { return false }
				return true
			}
			
			// 最後におまけ処理
			//			self.updatePackageBundleAttribute()
			
			
			completion(filtered, cursor, nil)
		}
		
		database_?.add(queryOperation)
	}
	
	func deleteMyPackageBundle(_ record: CKRecord, completion: @escaping (_ success: Bool, _ error: Error? ) -> Void) {
		
		let packageBundle = record[RecordType.MyPackageBundleKey.packageBundle] as? String
		
		if packageBundle == nil {
			completion(false, LawError.unknown )
			return
		}
		
		// Fetch
		fetchRecord(withPackageBundle: packageBundle!) { (results, error) -> Void in
			
			if error != nil {
				completion(false, error )
				return
			}
			
			func deletePackageBundleWithRecord() {
				// Delete list
				DispatchQueue.main.async {
					
					let deleteOperation = CKModifyRecordsOperation(recordsToSave: nil, recordIDsToDelete: [record.recordID])
					
					deleteOperation.qualityOfService = .userInitiated
					deleteOperation.queuePriority = .veryHigh
					deleteOperation.modifyRecordsCompletionBlock = { (records: [CKRecord]?, recordID: [CKRecordID]?, error: Error?) -> Void in
						
						DispatchQueue.main.async {
							completion( (error == nil), error )
						}
					}
					
					self.privateDatabase_?.add(deleteOperation)
				}
			}
			
			if results.count == 0 {
				
				// already deleted
				deletePackageBundleWithRecord()
				return
			}
			
			self.deleteRecords(results, fromDatabase: self.database_!) {
				(error: Error?) -> Void in
				
				if error != nil {
					DispatchQueue.main.async {
						completion(false, error )
					}
					return
				}
				
				print("No err")
				deletePackageBundleWithRecord()
			}
		}
	}
	
  func addDocument(_ packageBundle: String, title: String, source: NSAttributedString, overwritingRecordID pkgRecordID: CKRecordID?, creatorUUID: String, completion: ((_ success: Bool) -> Void)?) {
		
		if hasPrivateAccount_ != true {
			completion?(false)
			return
		}
		
		var data: Data? = nil
		
		do {
			data = try source.data(from: NSMakeRange(0, source.length), documentAttributes: [NSDocumentTypeDocumentAttribute: NSRTFTextDocumentType])
		} catch {
		}
		
		var bz2: Data? = nil
		
		if data != nil {
			do {
				bz2 = try BZipCompression.compressedData(with: data, blockSize: BZipDefaultBlockSize, workFactor: BZipDefaultWorkFactor)
			} catch _ {
				bz2 = data
			}
		}
		
		// Create a new record
		
		func writeForRecord( _ record: CKRecord ) {
			
			record[RecordType.MyPackageBundleKey.packageBundle] = packageBundle as NSString
			record[RecordType.MyPackageBundleKey.title] = title as NSString
			record[RecordType.MyPackageBundleKey.creatorUUID] = creatorUUID as NSString?
			
			let component = packageBundle + ".bz2"

			let bz2Path = (NSTemporaryDirectory() as NSString).appendingPathComponent(component)
			
			if bz2 != nil {
				let bz2url = URL(fileURLWithPath: bz2Path)
				let _ = try? bz2!.write(to: bz2url, options: [])
				
				let asset = CKAsset(fileURL: bz2url)
				record[RecordType.MyPackageBundleKey.source] = asset
				
			}
			
			let saveOperation = CKModifyRecordsOperation(recordsToSave: [record], recordIDsToDelete: nil)
			saveOperation.savePolicy = .allKeys
			saveOperation.qualityOfService = .userInitiated
			saveOperation.queuePriority = .veryHigh

			saveOperation.modifyRecordsCompletionBlock = { (records: [CKRecord]?, recordID: [CKRecordID]?, error: Error?) -> Void in

				if error != nil {
					print("CKRecord \(error!.localizedDescription)")
					completion?(false)
					
				} else{
					print("Done")
					completion?(true)
				}
				
				do {
					try FileManager.default.removeItem(atPath: bz2Path)
				} catch {
				}
			}
			
			privateDatabase_?.add(saveOperation)
		}
		
		if pkgRecordID != nil {
			
			let operation = CKFetchRecordsOperation(recordIDs: [pkgRecordID!])
			
			operation.qualityOfService = .userInitiated
			operation.queuePriority = .veryHigh

			operation.allowsCellularAccess = true
			operation.perRecordProgressBlock = {(theRecordID: CKRecordID, progress: Double) -> Void in }
			operation.fetchRecordsCompletionBlock = { (recordsByRecordID: [CKRecordID : CKRecord]?,  error: Error?) -> Void in
				
				if let theRecord = recordsByRecordID?[pkgRecordID!] {
					writeForRecord(theRecord)
					
				}else {
					completion?(false)
				}
			}
			privateDatabase_?.add(operation)
			
		}else {
			
			let record = CKRecord(recordType: RecordType.myPackageBundle)
			writeForRecord(record)
		}
	}
	
	// MARK:- Internal methods
	
	
	func report(_ string: String) {
		if hasPrivateAccount_ != true {
			return
		}
		let record = CKRecord(recordType: RecordType.report)
		record[RecordType.ReportKey.title] = string as NSString

		let saveOperation = CKModifyRecordsOperation(recordsToSave: [record], recordIDsToDelete: nil)
		
		saveOperation.savePolicy = .changedKeys
		saveOperation.qualityOfService = .userInitiated
		saveOperation.queuePriority = .veryHigh
		
		saveOperation.modifyRecordsCompletionBlock = { (records: [CKRecord]?, recordID: [CKRecordID]?, error: Error?) -> Void in }
		
		database_?.add(saveOperation)
	}
	
	// MARK:- Private methods
	
	private func deleteNonUniqueRecords( _ nonUniqueNameAndMD5s: [(title: String, md5: String)], inResults results: [CKRecord]) -> [CKRecord] {
		
		if hasPrivateAccount_ != true {
			return []
		}
		
		/*
		
		Delete identical records except for one
		
		Returns records to be deleted
		
		
		*/
		
		
		// 1. Get relevant records
		// 2. Find the latest record
		// 3. Delete the rest
		
		var thisArray: [CKRecord] = []
		
		for (title, md5) in nonUniqueNameAndMD5s {
			
			var latestCKRecord: CKRecord? = nil
			
			let filtered = results.filter { ( record: CKRecord) -> Bool in
				
				let hogeTitle = record[RecordType.LawDocPackageKey.title] as? String ?? ""
				let hogeMD5 = record[RecordType.LawDocPackageKey.md5] as? String ?? ""
				
				if title == hogeTitle && md5 == hogeMD5 {
					
					if latestCKRecord?.creationDate!.compare( record.creationDate! ) != ComparisonResult.orderedAscending {
						latestCKRecord = record
					}
					return true
				}
				return false
			}
			
			thisArray += filtered
			
			// Now delete latestCKRecord from the array
			
			if latestCKRecord != nil {
				let index = thisArray.index(of: latestCKRecord!)
				if index != nil { thisArray.remove(at: index!) }
			}
		}
		
		print("** to be deleted **")
		deleteRecords( thisArray, fromDatabase: database_!, completion: nil)
		
		return thisArray
	}
	
	private func deleteRecords(_ records: [CKRecord], fromDatabase: CKDatabase, completion: ((_ error: Error? ) -> Void)?) {
		
		if records.count == 0 {
			completion?(nil)
			return
		}
		
		var recordIDs: [CKRecordID] = []
		for record in records {
			recordIDs.append(record.recordID)
		}
		let deleteOperation = CKModifyRecordsOperation(recordsToSave: nil, recordIDsToDelete: recordIDs)
		
		deleteOperation.qualityOfService = .userInitiated
		deleteOperation.queuePriority = .veryHigh

		deleteOperation.modifyRecordsCompletionBlock = { (records: [CKRecord]?, recordID: [CKRecordID]?, error: Error?) -> Void in

			completion?(error)
		}
		
		self.database_?.add(deleteOperation)
	}
}

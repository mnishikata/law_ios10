//
//  FileRepository.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 16/12/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import Foundation

final class FileRepository {
  static var shared = FileRepository()
  
  private var errorHandler: ((_ error:Error) -> Void)?
  private var indexUpdateHandler: ((_ root: DocItem?) -> Void)?

  var documentFolderURL: URL = {
    return FileManager.default.urls(for: .documentDirectory,  in: .userDomainMask)[0]
  }()
  
  // Backup
  var lastBackupDateString = ""
  lazy var backupFilenameFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyyMMdd"
    return formatter
  }()

  
  func loadIndex( _ conflictHandler: ICloudConflictNotificationHandler?, completion completionHandler: @escaping ((_ root: DocItem?) -> Void) ) -> Bool {
    
    indexUpdateHandler = completionHandler
    
    var root = DocItem()// = ["folder":true, "items" : NSMutableArray() ]
    root.items = NSMutableArray()
    root.folder = true
    
    // Read index.plist
    
    let indexFileURL = documentFolderURL.appendingPathComponent("index.plist")
    
    if FileManager.default.fileExists(atPath: indexFileURL.path) {
      let coordinator = NSFileCoordinator()
      
      coordinator.coordinate(readingItemAt: indexFileURL, options: .withoutChanges, error: nil) { (theURL: URL) -> Void in
        
        do {
          let plistData = try Data(contentsOf: theURL, options: .dataReadingMapped)
          
          if let plist = try PropertyListSerialization.propertyList(from: plistData,
                                                                    options: [.mutableContainersAndLeaves], format: nil) as? DocItem {
            root = plist
          }
        } catch let error1 as NSError {
          self.errorHandler?(error1)
        }
      }
    }
    
    // Upgrade so that index contains md5
    #if os(iOS)
      root.makeObjectsPerformBlock { (dictionary: DocItem) -> Void in
        
        if dictionary.isLawEntity && dictionary.packageURL != nil && dictionary.sourceURLString == nil  {
          
          let _ = dictionary.packageURL!.setExcludedFromBackup()
          
          let ( plist, _, _) = FilePackage.shared.loadPackage(at: dictionary.packageURL! as URL!, onlyPlist: true )
          
          if let md5String = plist?[.md5]  {
            dictionary.md5 = md5String as? String
          }
          
          let originalTitle = plist?[.title] as? String ?? ""
          dictionary.originalTitle = originalTitle
          
          let sourceURLString = plist?[.sourceURL] as? String
          dictionary.sourceURLString = sourceURLString
          
        }else {
          dictionary.sourceURLString = nil
          
        }
      }
    #endif
    
    
    // Finilize
    indexUpdateHandler?(root)
    
    // Load from iCloud. Overwrite MyDocumentItems_ when completed
    ICloudRepository.shared.errorHandler = errorHandler
    ICloudRepository.shared.conflictHandler = conflictHandler
    ICloudRepository.shared.cloudDidUpdateHandler = { url in self.loadFromICloudFile(at: url) }
    ICloudRepository.shared.loadFromICloud()
    
    return true
  }
  
  func saveIndex(saveToICloud: Bool ) -> Void {
    
    // Load items' sourceURL -> Index
    
    var data: Data
    
    do {
      data = try PropertyListSerialization.data( fromPropertyList: FilePackage.shared.documents, format: .binary, options: 0)
    } catch let error1 as NSError {
      // Unknown Error
      errorHandler?(error1)
      return
    }
    
    #if arch(i386) || arch(x86_64)
      
      // Dump on Simulator
      do {
        let data2 = try PropertyListSerialization.data( fromPropertyList: FilePackage.shared.documents, format: .xml, options: 0) as Data?
        
        let string = NSString(data: data2!, encoding: String.Encoding.utf8.rawValue)
        print("\(string)")
      } catch let error1 as NSError {
        errorHandler?(error1)
        
      }
      
    #endif
    
    
    let theURL = documentFolderURL.appendingPathComponent("index.plist")
    
    let coordinator = NSFileCoordinator()
    
    #if os(iOS)
      // Daily Backup
      coordinator.coordinate(writingItemAt: theURL, options: .forReplacing, error: nil) { (newURL: URL) -> Void in
        self.keepBackupForFileAt( theURLOrMyDocumentWhenNull: newURL, forceBackup: false)
        let _ = try? data.write(to: newURL, options: [NSData.WritingOptions.atomic])
      }
    #endif
    
    // iCloud
    if saveToICloud == true && ICloudRepository.shared.ready {
      saveIndexToICloud(FilePackage.shared.documents)
    }
  }
  
  private func saveIndexToICloud(_ propertyList: NSObject) {
    let ud = UserDefaults.standard
    if false == ud.bool( forKey: "iCloudSync" ) { return }
    
    let dataToCompress: Data
    do {
      // COPY PROPERTY LIST BEFORE PASSING IT TO NSJSONSerialization
      let plistCopy = propertyList.copy()
      dataToCompress = try JSONSerialization.data(withJSONObject: plistCopy, options: [])
    } catch let error1 as NSError {
      errorHandler?(error1)
      
      // Unknown Error
      return
    }
    
    // Pattern A: Binary Property list -> deflate
    // Pattern B: JSON -> bz2
    
    processQueue_.async {
      
      print("+++ save to iCloud +++")
      
      guard let bz2 = try? BZipCompression.compressedData(with: dataToCompress, blockSize: BZipDefaultBlockSize, workFactor: BZipDefaultWorkFactor) else {
        return
      }
      
      let timeStamp = Date.timeIntervalSinceReferenceDate
      
      let dictionary: [String: Any] = [FilePackage.iCloudTimeStamp: timeStamp,
                                       FilePackage.iCloudBZ2: bz2]
      
      guard let dataToSave = try? PropertyListSerialization.data( fromPropertyList: dictionary, format: .binary, options: 0) else { return }
      
      ///
      /// Upload
      ///
      guard let actualUrl = ICloudRepository.shared.saveDataOnBackground(dataToSave, filename: FilePackage.ICloudFileName) else { return }
      
      // Get modification date
      
      if let attributes = try? FileManager.default.attributesOfItem(atPath: actualUrl.path) {
        if let modificationDate = attributes[FileAttributeKey.modificationDate] as? Date {
          
          ud.set( modificationDate.timeIntervalSinceReferenceDate,
                  forKey: "iCloudDidChangeLastUpdateTimeStamp" )
          
        }
      }
      ud.set( timeStamp, forKey: FilePackage.iCloudTimeStamp )
    }
  }

  
  private func loadFromICloudFile(at url: URL) {
    
    /// Execute on background thread
    
    processQueue_.async {
      
      let coordinator = NSFileCoordinator()
      
      coordinator.coordinate(readingItemAt: url, options: .withoutChanges, error: nil) { (newURL: URL) -> Void in
        
        var rootItem: DocItem? = nil
        var timeStamp: TimeInterval = 0
        
        defer {
          DispatchQueue.main.async {
            self.iCloudDidLoad(dictionary: rootItem, timeStamp: timeStamp)
          }
        }
        
        /////////////////////////
        
        
        let data = try? Data( contentsOf: newURL )
        if data == nil { return }
        
        let dictionary: [String: Any]
        
        do {
          dictionary = try PropertyListSerialization.propertyList(from: data!, format: nil) as! [String: Any]
        } catch {
          
          // Unkonw error
          return
        }
        
        timeStamp = dictionary[FilePackage.iCloudTimeStamp] as! TimeInterval
        let myTimeStamp = UserDefaults.standard.double( forKey: FilePackage.iCloudTimeStamp )
        
        if timeStamp <= myTimeStamp {
          // iCloud is older... do not read
          timeStamp = 0
          return
        }
        
        let deflatedData = dictionary[FilePackage.iCloudPlist] as? Data
        if let bz2Data = dictionary[FilePackage.iCloudBZ2] as? Data {
          
          print("Loading \(deflatedData?.count) and \(bz2Data.count)")
          
          /*
           bz2 decompression takes 10 times slower than gzip
           bz2 compress about 60%-70% more
           */
          
          do {
            let decompressedData = try BZipCompression.decompressedData(with: bz2Data)
            
            rootItem = try JSONSerialization.jsonObject(with: decompressedData, options: .mutableContainers) as? DocItem
            
            print("*** Loaded from bz2. GZip was not used. ***")
            return
          } catch let error1 as NSError {
            self.errorHandler?(error1)
          }
        }
        
        if let inflatedData = (deflatedData as NSData?)?.inflate() {
          
          do {
            rootItem = try PropertyListSerialization.propertyList(from: inflatedData,
                                                                  options: [.mutableContainersAndLeaves], format: nil) as? DocItem
            
            return
            
          } catch let error1 as NSError {
            self.errorHandler?(error1)
          }
        }
        
        /// Failed to read
        
        rootItem = nil
        timeStamp = 0
        
        return
        
      }
    }
  }
  
  func iCloudDidLoad(dictionary: DocItem?, timeStamp: Double) {
    
    let ud = UserDefaults.standard
    let myTimeStamp = ud.double( forKey: FilePackage.iCloudTimeStamp )
    
    // First run and dictionary is not nil, ask user
    // ?
    
    
    if dictionary == nil || ( timeStamp < myTimeStamp ) { return }
    
    
    // Load from iCloud
    
    
    let modifiedPackageNames = compareIndexFile(FilePackage.shared.documents, with: dictionary!)
    
    print("modifiedPackageName \(modifiedPackageNames)")
    // modifiedPackageName ["M29HO089.html.comcatalystwopackage"]
    
    
    indexUpdateHandler?(dictionary!)

    saveIndex( saveToICloud: false )
    
    ud.set( timeStamp, forKey: FilePackage.iCloudTimeStamp )
    
    let center = NotificationCenter.default
    center.post(name: FilePackage.FilePackageLoadedPackages, object: self, userInfo: ["ModifiedPackageNames": modifiedPackageNames])
    
  }
  
  func compareIndexFile( _ dictionary1: DocItem, with dictionary2: DocItem) -> [String] {
    print("== Compare Dictionaries ==")
    
    let longUUIDForPackageName1 = longUUIDForIndexFile(dictionary1)
    let longUUIDForPackageName2 = longUUIDForIndexFile(dictionary2)
    
    let allKeys1 = longUUIDForPackageName1.allKeys as! [String]
    let allKeys2 = longUUIDForPackageName2.allKeys as! [String]
    
    var differentPackageName: [String] = []
    
    for key in allKeys1 {
      let uuid1 = longUUIDForPackageName1[key] as? String
      let uuid2 = longUUIDForPackageName2[key] as? String
      
      if uuid2 == nil || uuid2! != uuid1 {
        differentPackageName.append(key)
        continue
      }
    }
    
    for key in allKeys2 {
      let uuid1 = longUUIDForPackageName1[key] as? String
      let uuid2 = longUUIDForPackageName2[key] as? String
      
      if uuid1 == nil || uuid1! != uuid2 {
        
        if differentPackageName.index(of: key) == nil {
          differentPackageName.append(key)
        }
        continue
      }
    }
    
    return differentPackageName
  }
  
  private func longUUIDForIndexFile(_ theDictionary: DocItem) -> NSMutableDictionary {
    // 1. List all packageNames
    
    var allPackageNames: [String: [DocItem]] = [:]
    
    let condition = { (dictionary: DocItem) -> Any? in
      
      if let packageName = dictionary.packageName  {
        
        var array = allPackageNames[packageName]
        
        // Already in the array
        if array != nil {
          array!.append( dictionary )
          allPackageNames[packageName] = array
          
        }else {
          allPackageNames[packageName] = [dictionary]
        }
      }
      
      return nil
    }
    
    let _ = theDictionary.collectWithCondition(condition) as! [String]
    
    // 2. List dictionaries for every packageName
    
    let sortedItemsLongUUID = NSMutableDictionary()
    
    for (packageName, docs) in allPackageNames {
      
      // 3. Sort by uuid
      let sortedDocs = docs.sorted{ ($0.uuid ?? "") < ($1.uuid ?? "") }
      
      // 3. Pick up uuid and combine as a single string
      var uuidLongString = ""
      
      for item in sortedDocs {
        // etagがある場合はそちらを優先
        let thisUUID = item.ETag ?? item.uuid ?? "•"
        uuidLongString += thisUUID
      }
      
      sortedItemsLongUUID[packageName] = uuidLongString
    }
    
    return sortedItemsLongUUID
  }

  
  //MARK:- Backup
  
  
  
  func keepBackupForFileAt(theURLOrMyDocumentWhenNull theURL: URL?, forceBackup: Bool) {
    
    if theURL == nil && FilePackage.shared.documents.items?.count == 0 { return } // Not ready
    
    let fm = FileManager.default
    let ud = UserDefaults.standard
    
    let todaysString = backupFilenameFormatter.string(from: Date())
    
    if lastBackupDateString == "" {
      lastBackupDateString = ud.string(forKey: "LastBackupDate") ?? ""
    }
    
    if todaysString != lastBackupDateString || forceBackup == true {
      
      lastBackupDateString = todaysString
      
      var uuid = todaysString
      
      if forceBackup == true {
        uuid = shortUUIDString()
      }
      let toURL = documentFolderURL.appendingPathComponent("backup_" + uuid + ".plist" )
      
      do {
        
        // Convert to JSON, then compress in BZip2
        
        if fm.fileExists(atPath: toURL.path) {
          try fm.removeItem(at: toURL)
        }
        
        var data: Data? = nil
        
        if theURL != nil {
          data = try? Data(contentsOf: theURL!)
          
          if data != nil {
            
            let plist = try PropertyListSerialization.propertyList(from: data!, format: nil)
            let jsonData = try JSONSerialization.data(withJSONObject: plist, options: [])
            
            data = jsonData
          }
          
        }else {
          let plist = FilePackage.shared.documents.copy()
          data = try JSONSerialization.data( withJSONObject: plist, options: [])
        }
        
        if data != nil {
          let compressed = try BZipCompression.compressedData(with: data!, blockSize: BZipDefaultBlockSize, workFactor: BZipDefaultWorkFactor)
          try compressed.write(to: toURL, options: [])
          try (toURL as NSURL).setResourceValue( Date(), forKey: URLResourceKey.contentModificationDateKey)
          ud.set(lastBackupDateString, forKey: "LastBackupDate")
        }
        
      } catch {
      }
    }
  }
  
  func allBackupFiles() -> [URL] {
    
    let fm = FileManager.default
    var backups: [URL] = []
    
    do {
      let contents = try fm.contentsOfDirectory(at: documentFolderURL, includingPropertiesForKeys: [.contentModificationDateKey, .fileSizeKey], options: [.skipsHiddenFiles, .skipsPackageDescendants, .skipsSubdirectoryDescendants])
      
      for content: URL in contents {
        
        if content.lastPathComponent.hasPrefix("backup_") == true {
          backups.append(content)
        }
      }
    } catch {
    }
    return backups
  }
  
  func removeAllBackups() {
    
    let daysToKeepBackups: Double = 10
    
    // remove backup file 5 days or older
    
    let today = Date()
    let fm = FileManager.default
    
    do {
      let contents = allBackupFiles()
      
      var backupsToDelete: [URL] = []
      for content: URL in contents {
        
        
        var obj: AnyObject? = nil
        try (content as NSURL).getResourceValue( &obj, forKey: URLResourceKey.contentModificationDateKey )
        
        let date = obj as! Date
        
        if today.timeIntervalSinceReferenceDate -
          date.timeIntervalSinceReferenceDate > daysToKeepBackups * 24 * 60 * 60 {
          backupsToDelete.append(content)
        }
      }
      
      for url in backupsToDelete {
        
        try fm.removeItem(at: url)
      }
      
    } catch {
    }
  }
  
  func recover(from url: URL) {
    
    let indexFileURL = documentFolderURL.appendingPathComponent("index.plist")
    do {
      let data = try? Data(contentsOf: url)
      let decompressed = try BZipCompression.decompressedData(with: data!)
      
      // json to plist
      let plist = try JSONSerialization.jsonObject(with: decompressed, options: .mutableContainers)
      let plistData = try PropertyListSerialization.data(fromPropertyList: plist, format: .binary, options: 0)
      
      let _ = try? plistData.write( to: indexFileURL, options: [.atomic])
      
      // Notify
      let _ = FilePackage.shared.loadIndex(nil, updateHandler: nil)
      
    } catch {
    }
    
  }

  //MARK:- bzip2 Compression
  
  
  func compressedFromPackage(_ packageURL: URL) -> Data? {
    
    do {
      let contentURL = packageURL.appendingPathComponent( FilePackage.Filename.content )
      
      let contentData = ((try? Data(contentsOf: contentURL)) as NSData?)?.inflate()
      if contentData == nil { return nil }
      
      let contentHtml = NSMutableString(data: contentData!, encoding: String.Encoding.utf8.rawValue)
      if contentHtml == nil { return nil }
      
      let infoPlistURL = packageURL.appendingPathComponent( FilePackage.Filename.infoPlist )
      let plistData = try? Data(contentsOf: infoPlistURL)
      let infoPlist = try PropertyListSerialization.propertyList(from: plistData!, format: nil)
      
      let tocURL = packageURL.appendingPathComponent( FilePackage.Filename.index )
      let tocData = try? Data(contentsOf: tocURL)
      let tocPlist = try PropertyListSerialization.propertyList(from: tocData!, format: nil)
      
      // Strip CSS
      contentHtml!.replaceCSS(stripCSSInstead: true, zoomScale: 1.0)
      
      var dict = [ FilePackage.contentHTMLKey : contentHtml!,  FilePackage.Filename.infoPlist : infoPlist,
                   FilePackage.Filename.index : tocPlist, FilePackage.packagenameKey : packageURL.lastPathComponent ]
      
      // Additional toc2
      
      let toc2URL = packageURL.appendingPathComponent( FilePackage.Filename.htmlElements )
      if FileManager.default.fileExists(atPath: toc2URL.path),
        let toc2Data = ((try? Data(contentsOf: toc2URL)) as NSData?)?.inflate() {
        let toc2Plist = try JSONSerialization.jsonObject(with: toc2Data, options: [])
        dict[FilePackage.Filename.htmlElements] = toc2Plist
      }
      
      
      let data = try PropertyListSerialization.data(fromPropertyList: dict, format: .xml, options: 0)
      let compressedData = try BZipCompression.compressedData(with: data, blockSize: BZipDefaultBlockSize, workFactor: BZipDefaultWorkFactor)
      
      return compressedData
      
    } catch let error1 as NSError {
      errorHandler?(error1)
      return nil
    }
  }
  
  func decompressBZip(_ data: Data, in folderURL: URL) -> URL? {
    do {
      let decompressedData = try BZipCompression.decompressedData(with: data)
      
      guard let dict = try PropertyListSerialization.propertyList(from: decompressedData, format: nil) as? [String: Any] else { return nil }
      guard let contentHtml = dict[FilePackage.contentHTMLKey] as? NSString else { return nil }
      guard let infoPlist = dict[FilePackage.Filename.infoPlist] else { return nil }
      guard let tocPlist = dict[FilePackage.Filename.index] else { return nil }
      guard let filename = dict[FilePackage.packagenameKey] else { return nil }
      let toc2Plist = dict[FilePackage.Filename.htmlElements]
      
      let contentHtmlData = (contentHtml.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: true) as NSData?)?.deflate()
      
      if contentHtmlData == nil { return nil }
      
      let plistData = try PropertyListSerialization.data( fromPropertyList: infoPlist, format: .binary, options: 0)
      let tocData = try PropertyListSerialization.data( fromPropertyList: tocPlist, format: .binary, options: 0)
      
      
      // Saving
      
      let URL = folderURL.appendingPathComponent(filename as! String)
      
      let contentURL = URL.appendingPathComponent(FilePackage.Filename.content)
      let plistURL = URL.appendingPathComponent(FilePackage.Filename.infoPlist)
      let tocURL = URL.appendingPathComponent(FilePackage.Filename.index)
      
      let fm = FileManager.default
      
      if fm.fileExists(atPath: URL.path) == false {
        try fm.createDirectory(atPath: URL.path, withIntermediateDirectories: true, attributes: nil)
      }
      
      let _ = try? contentHtmlData!.write(to: contentURL, options: [])
      let _ = try? plistData.write(to: plistURL, options: [])
      let _ = try? tocData.write(to: tocURL, options: [])
      
      
      let toc2URL = URL.appendingPathComponent(FilePackage.Filename.htmlElements)
      if toc2Plist != nil {
        let toc2Data = try JSONSerialization.data( withJSONObject: toc2Plist!, options: [])
        let _ = try? (toc2Data as NSData).deflate()!.write(to: toc2URL, options: [])
        
      }else {
        if FileManager.default.fileExists(atPath: toc2URL.path) {
          try FileManager.default.removeItem(at: toc2URL)
        }
      }
      
      return URL
    } catch let error1 as NSError {
      errorHandler?(error1)
      return nil
    }
  }
  #if os(iOS)
  
  func unzipAt(_ filePath: String) -> URL? {
				
				let zip = ZipArchive()
				//zip.delegate = self
				var flag = zip.unzipOpenFile( filePath )
				if flag == false { return nil }
				
				flag = zip.unzipFile( to: documentFolderURL.path, overWrite: true )
				if flag == false { return nil }
				
				flag = zip.closeZipFile2()
				//if flag == false { return nil }
				
				let files = zip.unzippedFiles
				
				/// Failed
				if files?.count == 0 {
          return nil
				}
				
				let path = files?[0] as! String
				let folderPath = (path as NSString).deletingLastPathComponent
				
				/// Failed
				if (folderPath as NSString).pathExtension != FilePackage.packageExtension {
          return nil
				}
				
				let packageURL = Foundation.URL( fileURLWithPath: folderPath )
				// Remove toc2 as well
				let toc2 = packageURL.appendingPathComponent(FilePackage.Filename.htmlElements)
				do { try FileManager.default.removeItem(at: toc2) } catch {}
    
				
				return packageURL
  }
  
  // MARK: -

  func installSampleFiles() {
    guard let folderURL = Bundle.main.url(forResource: "SampleDocuments", withExtension: "") else { return }
    
    let fm = FileManager.default
    // Files
    
    do {
      let contents = try fm.contentsOfDirectory(at: folderURL, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
      for url in contents {
        if url.lastPathComponent == "index.plist" {
          
          let dest = documentFolderURL.appendingPathComponent( "index.plist" )
          do {
            try fm.copyItem(at: url , to: dest)
          } catch _ {
          }
          
        } else if let data = try? Data(contentsOf: url ) {
          if let destURL = decompressBZip(data, in: documentFolderURL) {
            let _ = destURL.setExcludedFromBackup()
            
            /*
             // BUILD HTML ELEMENTS
             
             let (plist,linkDictionaries,rawHTML) = loadPackage(at: destURL, onlyPlist:false, rawHTML:true)
             
             buildHTMLElements(rawHTML!, for:destURL, plist:plist, linkDictionaries:linkDictionaries!, overwriteMode: true )
             
             */
          }
        }
      }
    } catch _ {
    }
  }
  
  
  #endif

}

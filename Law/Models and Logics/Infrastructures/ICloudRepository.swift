//
//  ICloudRepository.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 16/12/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import Foundation

class ICloudRepository: NSObject {
  static var shared = ICloudRepository()

  private var _query: NSMetadataQuery? = nil
  var conflictHandler: ICloudConflictNotificationHandler?
  var errorHandler: ((_ error:Error) -> Void)?
  var cloudDidUpdateHandler: ((_ url: URL)->Void)?

  var ready = false
  
  func loadFromICloud() {
    
    //		iCloudDidLoadHandler = handler
    
    if false == UserDefaults.standard.bool( forKey: "iCloudSync" ) {
      return
    }
    
    let fm = FileManager.default
    let token = fm.ubiquityIdentityToken
    
    if token == nil {
      return
    }
    
    // Document folder observer
    if _query == nil {
      
      _query = NSMetadataQuery()
      _query!.searchScopes = [NSMetadataQueryUbiquitousDocumentsScope]
      _query!.enableUpdates()
      
      let predicate = NSPredicate(format: "%K LIKE %@", NSMetadataItemFSNameKey , FilePackage.ICloudFileName)
      _query!.predicate = predicate
      
      let center = NotificationCenter.default
      center.addObserver(self, selector: #selector(cloudDidChange), name: .NSMetadataQueryDidUpdate, object: nil)
      center.addObserver(self, selector: #selector(cloudDidChange), name: .NSMetadataQueryDidFinishGathering, object: nil)
      
      _query!.start()
      
    }
  }
  
  func cloudDidChange() {
    
    assert( conflictHandler != nil, "*** conflictHandler is nil! ***" )
    
    let ud = UserDefaults.standard
    let results = _query!.results
    
    if results.count == 0 {
      
      print("*** New for iCloud ***")
      
      ready = true
      return /* First time to use*/
    }
    
    print("- iCloud Loading")
    
    let metadata = results[0] as! NSMetadataItem
    
    guard let url = metadata.value(forAttribute: NSMetadataItemURLKey) as? URL else { return }
    
    var modificationDate = Date.distantPast
    
    let status = metadata.value(forAttribute: NSMetadataUbiquitousItemDownloadingStatusKey) as? String
    if status == NSMetadataUbiquitousItemDownloadingStatusNotDownloaded {
      
      do {
        try FileManager.default.startDownloadingUbiquitousItem(at: url)
      } catch let error1 as NSError {
        errorHandler?(error1)
      }
      return
    }
    
    if status != NSMetadataUbiquitousItemDownloadingStatusCurrent { return } // Not Up to date
    
    if metadata.value(forAttribute: NSMetadataItemFSContentChangeDateKey) != nil {
      modificationDate = metadata.value(forAttribute: NSMetadataItemFSContentChangeDateKey) as! Date
    }
    
    print("*** iCloud Changed ***")
    print("modificationDate \(modificationDate)")
    
    let allVersions = NSFileVersion.unresolvedConflictVersionsOfItem(at: url)
    
    if allVersions == nil ||  allVersions!.count == 0  {
      let lastTimeStamp = ud.double(forKey: "iCloudDidChangeLastUpdateTimeStamp")
      
      print("lastTimeStamp \(lastTimeStamp) - \(modificationDate.timeIntervalSinceReferenceDate)")
      
      if lastTimeStamp >= modificationDate.timeIntervalSinceReferenceDate {
        ready = true
        return
      }
      else {
        ud.set( modificationDate.timeIntervalSinceReferenceDate,
                forKey: "iCloudDidChangeLastUpdateTimeStamp" )
      }
    }
    
    
    // Check CONFLICT at url
    
    
    if allVersions != nil && allVersions!.count > 0 {
      
      let currentVersion = NSFileVersion.currentVersionOfItem(at: url)!
      
      /*
       
       Call conflict handler with allVersions and decision handler that sends fileVersion to use
       
       */
      
      print("call conflict handler \(conflictHandler)")
      
      let decisionHandler: ICloudConflictDecisionHandler = { (fileVersion: NSFileVersion) -> Void in
        
        if fileVersion === currentVersion {
          
          var success: Bool
          do {
            try NSFileVersion.removeOtherVersionsOfItem(at: url)
            success = true
            
            for aVersion in allVersions! {
              try aVersion.remove()
            }
            
          } catch let error1 as NSError {
            self.errorHandler?(error1)
            success = false
          }
          
          print("removeOtherVersionsOfItemAtURL \(success)")
          
        }else {
          
          var success: URL?
          do {
            
            // Keep current backup
            #if os(iOS)
              FileRepository.shared.keepBackupForFileAt(theURLOrMyDocumentWhenNull: nil, forceBackup: true)
            #endif
            
            success = try fileVersion.replaceItem(at: url, options: .byMoving)
            
            for aVersion in allVersions! {
              if aVersion !== fileVersion {
                try aVersion.remove()
              }
            }
            
            ud.set( 0, forKey: FilePackage.iCloudTimeStamp )
            
          } catch let error1 as NSError {
            self.errorHandler?(error1)
            success = nil
          }
          print("replaceItemAtURL \(success)")
        }
        self.cloudDidUpdateHandler?(url)
        self.ready = true
      }
      
      conflictHandler!( ( [currentVersion] + allVersions!), decisionHandler)
    }else {
      
      cloudDidUpdateHandler?(url)
      ready = true
    }
  }
  
  func saveDataOnBackground(_ data: Data, filename: String) -> URL? {
    assert( Thread.isMainThread != true, "** CALL ON BACKGROUND **")
    
    let fm = FileManager.default
    guard let _ = fm.ubiquityIdentityToken else { return nil }
    guard let url = fm.url(forUbiquityContainerIdentifier: nil) else { return nil }
    
    let iCloudDocumentFolder = url.appendingPathComponent("Documents")

    if fm.fileExists(atPath: iCloudDocumentFolder.path) == false {
      do {
        try fm.createDirectory(at: iCloudDocumentFolder, withIntermediateDirectories: true, attributes: nil)
      } catch _ {
      }
    }
    
    let urlToWrite = iCloudDocumentFolder.appendingPathComponent(filename)
    var actualUrl: URL? = urlToWrite
    let coordinator = NSFileCoordinator()
    coordinator.coordinate(writingItemAt: urlToWrite, options: .forReplacing, error: nil) { (newURL: URL) -> Void in
      actualUrl = newURL
      
      do {
        try data.write( to: newURL, options: [.atomic] )

      } catch {
        actualUrl = nil
      }
      
    }
    
    return actualUrl
  }
}

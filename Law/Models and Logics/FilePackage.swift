//
//  FilePackage.swift


import Foundation
import WatchConnectivity

extension FilePackage {
  
  // Notification Name
  @nonobjc static let FilePackageLoadedPackages = Notification.Name("FilePackageLoadedPackages")
  
  // FILE/PACKAGE CONSTANTS
  @nonobjc static let packageExtension = "comcatalystwopackage"
  enum Filename {
    static let infoPlist = "Info.plist"
    static let index = "Toc.plist"
    static let content = "Contents.htmlcompressed"
    static let rawContent = "Raw.htmlcompressed" // optional
    static let updateInfo = "Update.plist"
    static let htmlElements = "Toc2.plistcompressed"
  }
  
  @nonobjc static let contentHTMLKey = "Contents"
  @nonobjc static let packagenameKey = "packageName"
  
  @nonobjc static let ICloudFileName = "CompressedIndexPlist"

  //PLIST KEY ... SEE DocPropertyKey
  /*
  enum PlistKeys {
    static let title = "title"
    static let edition = "edition"
    static let sourceURL = "sourceURL"
    static let md5 = "md5"
  }*/
  
  //UpdateInfoKey
  enum UpdateInfo {
    static let status = "UpdateStatus"
    static let edition = "Edition"
    static let lastChecked = "LastUpdateCheck"
  }
}

//MARK:-
final class FilePackage: NSObject {
	
  // DATA SOURCE
	private static var MyDocumentItems_: DocItem = [:]
	
  private var errorHandler: ((_ error:Error) -> Void)?
	var bookmarkingDidFinishHandler: (() -> Void)?
  
  static var shared = FilePackage()
  var documents: DocItem {
    return FilePackage.MyDocumentItems_
  }
  
	// MARK:- Body
	
	override init() {
		super.init()
		
		// Watch Kit
		
		if #available(iOS 9.3, *) {
			if WCSession.isSupported() {
				let defaultSession = WCSession.default()
				defaultSession.delegate = WatchConnectivityManager.shared
				WatchConnectivityManager.shared.delegate = self
//				WatchConnectivityManager.sharedConnectivityManager.cancelOutstandingFileTransfers = true
				defaultSession.activate()
				
			}
		} else {
			// Fallback on earlier versions
		}
		#if os(iOS)
		FileRepository.shared.removeAllBackups()
		#endif
	}
	
	func packageURL(from packageNameWithoutExtension: String ) -> URL {
		let packageURL = FileRepository.shared.documentFolderURL.appendingPathComponent(packageNameWithoutExtension).appendingPathExtension( FilePackage.packageExtension )
		return packageURL
	}
	
	
	//MARK:- Utils
	
	func anchor(for string: String, in packageURL: URL, toc2: [ElementDescriptor]? = nil, toc1: [LinkDictionary]? = nil) -> (anchor: String?, element: ElementDescriptor?) {
		
		let mstr = NSMutableString(string: string)
		var anchor: String? = nil
		var destinationToc2: [ElementDescriptor]? = toc2
		var userDefined = false
		var linkDictionaries: [LinkDictionary]? = toc1
		
		
		if toc2 != nil && toc1 != nil {
      let ( plist, _, _) = loadPackage(at: packageURL, onlyPlist: true)
			if let sourcePath = plist?[DocPropertyKey.sourceURL] as? String {
				userDefined = sourcePath.hasPrefix("user:") == true
			}
			destinationToc2 = toc2
			linkDictionaries = toc1
		}else if toc2 == nil && toc1 != nil {
			
		}else {
			
      let ( plist, ld, _) = loadPackage(at: packageURL, onlyPlist: false)
			linkDictionaries = ld
			
			if linkDictionaries == nil { return (nil,nil) }
			destinationToc2 = loadHTMLElements(for: packageURL)
			
			if let sourcePath = plist![DocPropertyKey.sourceURL] as? String {
				userDefined = sourcePath.hasPrefix("user:") == true
			}
		}
		
		if destinationToc2 != nil {
			let encodedString = userDefined ? encodeForTOC2ForUserDoc(mstr) : encodeForTOC2(mstr)
			// look for anchor in toc2
			for element in destinationToc2! {
        anchor = element.anchor(for: encodedString)
				if anchor != nil {
					
					debugPrint("Found anchor \(anchor)")
					return (anchor!, element)
				}
			}
		}
		
		if destinationToc2 == nil || anchor == nil {
			encodeForTOC1(mstr)
			
			// Look for anchor in link dictionary
			for dict in linkDictionaries! {
				
				// string 48.2
				if dict.numerical == (mstr as String) {
					anchor = dict.link
					break
				}
			}
		}
		
		return (anchor as String?, nil )
	}
	
	func encodeForTOC2(_ string: NSString) -> String {
		
		let mstrg = NSMutableString(string: string)
		mstrg.replaceOccurrences(of: "の", with: "-", options: .literal, range: NSMakeRange(0, mstrg.length))
		//条+num -> "."
		//項 -> "_"
		//号 -> ""
		
		// 自作の場合は、第4条　優先権第D.項第 (1)号 🔱1@8@4
		
		mstrg.replaceOccurrences(of: "[0-9]+(?=号)", with: "_$0", options: .regularExpression, range: NSMakeRange(0, mstrg.length))
		mstrg.replaceOccurrences(of: "[条第]+(?=[0-9])", with: ".", options: .regularExpression, range: NSMakeRange(0, mstrg.length))
		mstrg.replaceOccurrences(of: "項(?=[0-9])", with: "_", options: .regularExpression, range: NSMakeRange(0, mstrg.length))
		mstrg.replaceOccurrences(of: "[条項号]", with: "", options: .regularExpression, range: NSMakeRange(0, mstrg.length))
		mstrg.replaceOccurrences(of: "^[0-9]*", with: "($0)", options: .regularExpression, range: NSMakeRange(0, mstrg.length))
		
		debugPrint("encodeForTOC2 \(mstrg)")
		return mstrg as String
	}
	
	func encodeForTOC2ForUserDoc(_ string: NSString) -> String {
		
		let mstrg = NSMutableString(string: string)
		mstrg.replaceOccurrences(of: "の", with: "-", options: .literal, range: NSMakeRange(0, string.length))
		//条+num -> "."
		//項 -> "_"
		//号 -> ""
		
		// 自作の場合は、第4条　優先権第D.項第 (1)号 🔱1@8@4
		
		let gouRange = mstrg.range(of: "[0-9]+(?=号)", options: .regularExpression, range: NSMakeRange(0, mstrg.length))
		let kouRange = mstrg.range(of: "(?<=[条第])[0-9](?!号)", options: .regularExpression, range: NSMakeRange(0, mstrg.length))
		let jouRange = mstrg.range(of: "^[0-9\\-]+", options: .regularExpression, range: NSMakeRange(0, mstrg.length))
		
		var returnString = ""
		
		if jouRange.location != NSNotFound { returnString = mstrg.substring(with: jouRange) }
		if kouRange.location != NSNotFound { returnString = mstrg.substring(with: kouRange) + "@" + returnString }
		if gouRange.location != NSNotFound { returnString = mstrg.substring(with: gouRange) + "@" + returnString }
		
		debugPrint("encodeForTOC2ForUserDoc \(returnString)")
		return returnString
	}
	
	func encodeForTOC1(_ string: NSMutableString) {
		string.replaceOccurrences(of: "の", with: ".", options: .literal, range: NSMakeRange(0, string.length))
	}
	
	func loadHTMLElements(for packageURL: URL) -> [ElementDescriptor]? {
		let urlToWrite = packageURL.appendingPathComponent(FilePackage.Filename.htmlElements)
		let path = urlToWrite.path
		if FileManager.default.fileExists(atPath: path) == false {
			return nil
    }
    
    guard let inflated = ((try? Data(contentsOf: urlToWrite)) as NSData?)?.inflate() else { return nil }
    do {
      let plist = try JSONSerialization.jsonObject(with: inflated, options: [.mutableContainers]) as? [[String: Any]]
      let array: [ElementDescriptor]? = plist?.map { ElementDescriptor(from: $0) }.flatMap { $0 }
      return array
      
    } catch let error1 as NSError {
      errorHandler?(error1)
    }
    
    return nil
  }
  
	func loadPackage(at packageURL: URL!, onlyPlist: Bool, zoomScale: Double = 1.0, rawHTML: Bool = false,
	                 buildTOC2: Bool = false, overwriteTOC2: Bool = false ) ->
    ( plist: DocProperty?, linkDictionaries: [LinkDictionary]?, contentHtml: NSMutableString?) {
			
		if packageURL?.path == nil { return (nil, nil, nil) }
		
		if false == FileManager.default.fileExists(atPath: packageURL.path) {
			return (nil, nil, nil)
		}
		
		var error: NSError? = nil
		let coordinator = NSFileCoordinator()
      var plist: DocProperty? = nil
      var linkDictionaries: [LinkDictionary]? = nil
      var contentHtml: NSMutableString? = nil
      
      coordinator.coordinate(readingItemAt: packageURL, options: .withoutChanges, error: nil) { (theURL: URL)  -> Void in
        
        // Load plist
        
        let plistURL = theURL.appendingPathComponent( FilePackage.Filename.infoPlist )
        let plistData: Data
        do {
          plistData = try Data(contentsOf: plistURL, options: .dataReadingMapped)
          
        } catch let error1 as NSError {
          error = error1
          return
          
        } catch { return }
        
        do {
          plist = try PropertyListSerialization.propertyList(from: plistData, options: [.mutableContainers], format: nil) as? DocProperty
        } catch let error1 as NSError {
          
          // Unknown error
          error = error1
          return
        } catch { return }
        
        // Stop here
        if onlyPlist == true { return }
        
        // Load index html
        
        let indexURL = theURL.appendingPathComponent( FilePackage.Filename.index )
        let indexdData: Data
        do {
          indexdData = try Data(contentsOf: indexURL, options: .dataReadingMapped)
        } catch let error1 as NSError {
          error = error1
          plist = nil; linkDictionaries = nil; contentHtml = nil;
          return
          
        } catch {
          plist = nil; linkDictionaries = nil; contentHtml = nil;
          return
        }
        
        
        do {
          if let array = try PropertyListSerialization.propertyList(from: indexdData, format: nil) as? [[String : Any]] {
            linkDictionaries = array.map { LinkDictionary(from: $0 ) }.flatMap { $0 }
          }
        } catch let error1 as NSError {
          error = error1
          plist = nil; linkDictionaries = nil; contentHtml = nil;
          return
        } catch {
          plist = nil; linkDictionaries = nil; contentHtml = nil;
          return
        }
        
        // Load content html
        var contentData: Data?
        var loadedFromRawHTML = false
        if rawHTML {
          let filename = FilePackage.Filename.rawContent
          let contentURL = theURL.appendingPathComponent( filename )
          
          if FileManager.default.fileExists(atPath: contentURL.path) {
            do {
              contentData = try Data(contentsOf: contentURL, options: .dataReadingMapped)
              loadedFromRawHTML = true
            } catch let error1 as NSError {
              error = error1
              plist = nil; linkDictionaries = nil; contentHtml = nil;
              return
            } catch {
              plist = nil; linkDictionaries = nil; contentHtml = nil;
              return
            }
          }
        }
        
        if contentData == nil {
          let filename = FilePackage.Filename.content
          let contentURL = theURL.appendingPathComponent( filename )
          
          do {
            contentData = try Data(contentsOf: contentURL, options: .dataReadingMapped)
          } catch let error1 as NSError {
            error = error1
            plist = nil; linkDictionaries = nil; contentHtml = nil;
            return
          } catch {
            plist = nil; linkDictionaries = nil; contentHtml = nil;
            return
          }
        }
        
        // Unzip
        var inflatedData: Data? = nil
        autoreleasepool {
          inflatedData = (contentData! as NSData).inflate()
          contentData = nil
        }
        
        // Failed inflating
        if inflatedData == nil {
          plist = nil; linkDictionaries = nil; contentHtml = nil;
          return
        }
        
        contentHtml = NSMutableString(data: inflatedData!, encoding: String.Encoding.utf8.rawValue)
        inflatedData = nil
        
        if let contentHtml = contentHtml {
          #if os(iOS)
            //						let delayInSeconds = 10.0
            //						let popTime = dispatch_time(
            //							DISPATCH_TIME_NOW,
            //							Int64(delayInSeconds * Double(NSEC_PER_SEC))
            //						)
            //
            //						dispatch_after(popTime, processQueue_){
            //						self.buildHTMLElements(contentHtml!, forPackage: packageURL, plist:plist, linkDictionaries:linkDictionaries!, overwriteMode: true )
            //						}
            
            if buildTOC2 == true {
              self.buildHTMLElements(contentHtml, for: packageURL, plist: plist, linkDictionaries: linkDictionaries!, overwriteMode: overwriteTOC2 )
            }
          #endif
          
          if rawHTML && loadedFromRawHTML == false {
            contentHtml.replaceOccurrences(of: "\n", with: "", options: [.caseInsensitive], range: NSMakeRange(0,contentHtml.length))
            contentHtml.replaceOccurrences(of: "</br>", with: "<br>", options: [.caseInsensitive], range: NSMakeRange(0,contentHtml.length))
          }
          
          if rawHTML == false {
            contentHtml.replaceCSS(stripCSSInstead: false, zoomScale: zoomScale)
          }
        }
        
        /* Debug */
        
        //			let rawHTMLPath = packageURL!.URLByAppendingPathComponent("raw.html").path
        //			contentData.writeToFile(rawHTMLPath!, atomically: false)
        //
        //           let rawIndexPath = packageURL!.URLByAppendingPathComponent("rawIndex.html").path
        //           indexdData.writeToFile(rawIndexPath!, atomically: false)
        
        
        return
      }
      
      
      if error != nil {
        print("** iCloud Reading Error \(error!.localizedDescription) **")
        errorHandler?(error!)
      }
      
      return ( plist: plist, linkDictionaries: linkDictionaries, contentHtml: contentHtml )
  }
  
  
  
  //MARK:- Built TOC2
  
  func updateOverwriting(_ overwrite:Bool) {
  }
  
  //MARK:- File I/O
  
  func loadIndex( _ handler: ICloudConflictNotificationHandler?, updateHandler: (() -> Void)? ) -> Bool {
    return FileRepository.shared.loadIndex(handler) { root in
      if root != nil {
        FilePackage.MyDocumentItems_ = root!
      }
      updateHandler?()
    }
  }
  
  func saveIndex(saveToICloud: Bool ) {

    FileRepository.shared.saveIndex(saveToICloud: saveToICloud)
    
		// SEND TO WATCH
		sendIndexToWatch()
	}

  func url(with name: String) -> URL {
    return FileRepository.shared.documentFolderURL.appendingPathComponent(name)
  }
 	
	// MARK:- Bookmarking

	func addBookmark(_ entry: FileEntry, bookmark: Bookmark) {
		
		//self.reload() necessary?
    
    let theParentItem = documents.lookForWithCondition {
      $0.anchor == nil && $0.packageName == entry.packageURL.lastPathComponent
    }
    
    var originalTitle: NSString = (theParentItem?.title as String? ?? "Untitled") as NSString
		if originalTitle.length > 7 { originalTitle = originalTitle.substring(to: 7) + "…" as NSString }
		
		let dict = DocItem( bookmarkToAnchor: bookmark.encodedURL,
			inPackage: entry.packageURL.lastPathComponent,
			title: (originalTitle as String) + bookmark.title,
			originalTitle: entry.plist[DocPropertyKey.title] as! String,
			withNotes: nil)

		
		if theParentItem != nil { theParentItem!.addItem(dict) }
		else { documents.addItem(dict) }
		
		saveIndex( saveToICloud: true )
		
		bookmarkingDidFinishHandler?()
	}
  
  func setNotes( _ text: String!, entry: FileEntry, bookmark: Bookmark ) {
    
    // 1. Already Exists
    
    let theDictionary = documents.lookForWithCondition {
      $0.anchor == bookmark.encodedURL && $0.packageName == entry.packageURL.lastPathComponent
    }
    
    if theDictionary != nil {
      
      theDictionary!.notes = text
			
			if let clickedURL = bookmark.clickedURL {
				theDictionary!.href = clickedURL.absoluteString
			}
			theDictionary!.ETag = shortUUIDString()  // update uuid as etag
			saveIndex( saveToICloud: true )
			
			print("Update Notes")
			
			return
    }
    
    // 2. Add as new note
    
    
    let theParentItem = documents.lookForWithCondition {
      $0.anchor == nil && $0.packageName == entry.packageURL.lastPathComponent
    }
    
		var originalTitle = (theParentItem?.title as String?) ?? "Untitled"
		
		if originalTitle.characters.count > 7 { originalTitle = (originalTitle as NSString).substring(to: 7) + "…" }
		
		let dict = DocItem( bookmarkToAnchor: bookmark.encodedURL,
			inPackage: entry.packageURL.lastPathComponent,
			title: bookmark.title,
			originalTitle: originalTitle,
			withNotes: text)
		
		if let clickedURL = bookmark.clickedURL {
			dict.href = clickedURL.absoluteString
		}
		
		// Look for the parent to add
		
    let parent: DocItem? = documents.lookForWithCondition {
      $0.isLawEntity == true && $0.packageURL?.lastPathComponent == entry.packageURL.lastPathComponent
    }
		
		if parent != nil { parent!.addItem(dict) }
		else { documents.addItem(dict) }
		
		print("Added Notes")
		
		saveIndex( saveToICloud: true )
		
		bookmarkingDidFinishHandler?()
		
		//tableView?.reloadData()
	}
	
	func removeHighlight( _ entry: FileEntry, uuid: String) {
		//self.reload() necessary?
		
		// 1. Already Exists
		
    if let theDictionary = documents.lookForWithCondition({ $0.uuid == uuid }) {
			
			let _ = documents.removeAndDeleteForItem( theDictionary )
			saveIndex( saveToICloud: true )

			bookmarkingDidFinishHandler?()
			//tableView?.reloadData()
		}
	}
	
	func updateHighlight( ) {
		bookmarkingDidFinishHandler?()
	}
	
	func addHighlight( _ entry: FileEntry, dictionary: DocItem) {
		// Look for the parent to add
    let parent: DocItem? = documents.lookForWithCondition {
      $0.isLawEntity == true && $0.packageURL?.lastPathComponent == entry.packageURL.lastPathComponent
    }
		var originalTitle = (parent?.title as String?) ?? "Untitled"
		
		if originalTitle.characters.count > 7 { originalTitle = (originalTitle as NSString).substring(to: 7) + "…" }
		
		if parent != nil { parent!.addItem(dictionary) }
		else { documents.addItem(dictionary) }
		
		dictionary.originalTitle = originalTitle
		dictionary.highlight = true
		dictionary.packageName = entry.packageURL.lastPathComponent
		
		print("Added Notes")
		
		saveIndex( saveToICloud: true )
		
		bookmarkingDidFinishHandler?()
	}
	
	func deleteNotes( _ entry: FileEntry, bookmark: Bookmark) {
		// 1. Already Exists
		
    if let theDictionary = documents.lookForWithCondition({
      $0.anchor == bookmark.encodedURL && $0.packageName == entry.packageURL.lastPathComponent
    }) {
			
			let _ = documents.removeAndDeleteForItem( theDictionary )
			saveIndex( saveToICloud: true )
			
			bookmarkingDidFinishHandler?()
		}
	}
	
	func notes(_ entry: FileEntry, bookmark: Bookmark) -> String? {
    let theDictionary = documents.lookForWithCondition {
      $0.anchor == bookmark.encodedURL && $0.packageName == entry.packageURL.lastPathComponent
    }
		return theDictionary?.notes
	}
  
  func allBookmarks(_ entry: FileEntry) -> [DocItem]? {
    let dictionaries = documents.collectWithCondition {
      $0.highlight == false && $0.anchor != nil && $0.packageName == entry.packageURL.lastPathComponent ? $0 : nil
    }
    return dictionaries as? [DocItem]
  }
	
	func allNotes(_ entry: FileEntry) -> [DocItem]? {
    let dictionaries = documents.collectWithCondition {
      $0.anchor != nil && $0.notes != nil &&  $0.packageName == entry.packageURL.lastPathComponent ? $0 : nil
    }
		return dictionaries as? [DocItem]
	}
	
	func allHighlights(_ entry: FileEntry) -> [DocItem]? {
    let dictionaries = documents.collectWithCondition {
      $0.highlight == true && $0.packageName == entry.packageURL.lastPathComponent ? $0 : nil
    }
		return dictionaries as? [DocItem]
	}
}


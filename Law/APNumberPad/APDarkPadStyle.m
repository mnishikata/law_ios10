//
//  APDarkPadStyle.m
//  APNumberPad
//
//  Created by VANGELI ONTIVEROS on 14/07/14.
//  Copyright (c) 2014 Andrew Podkovyrin. All rights reserved.
//

#import "APDarkPadStyle.h"

@implementation APDarkPadStyle

#pragma mark - Number button

+ (UIFont *)numberButtonFont {
    return [UIFont fontWithName:@"STHeitiTC-Light" size:28.f];
}

+ (UIColor *)numberButtonTextColor {
    return [[UIColor whiteColor]colorWithAlphaComponent:0.8f];
}

+ (UIColor *)numberButtonBackgroundColor {
    return [UIColor darkGrayColor];
}

+ (UIColor *)numberButtonHighlightedColor{
    return [UIColor colorWithRed:66/255.0f green:66/255.0f blue:66/255.0f alpha:1.0];
}

+ (UIColor *)keyboardDividerColor {
  return [UIColor colorWithRed:38/255.0f green:38/255.0f blue:38/255.0f alpha:1.0];
}

#pragma mark - Function button

+ (UIFont *)functionButtonFont {
    return [UIFont fontWithName:@"STHeitiTC-Light" size:28.f];
}

+ (UIColor *)functionButtonTextColor {
    return [[UIColor whiteColor]colorWithAlphaComponent:0.8f];
}

+ (UIColor *)functionButtonBackgroundColor {
    return [UIColor colorWithRed:66/255.0f green:66/255.0f blue:66/255.0f alpha:1.0];
}

+ (UIColor *)functionButtonHighlightedColor {
    return [UIColor darkGrayColor];
}

+ (UIImage *)clearFunctionButtonImage {
    return [UIImage imageNamed:@"APNumberPad.bundle/images/apnumberpad_backspace_icon.png"];
}


@end

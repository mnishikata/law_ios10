//
//  APNumberPad.m
//
//  Created by Andrew Podkovyrin on 16/05/14.
//  Copyright (c) 2014 Podkovyrin. All rights reserved.
//

#import "APNumberPad.h"

#import "APNumberButton.h"
#import "APNumberPadDefaultStyle.h"

#import "UIImage+Rotation.h"

@interface APNumberPad () {
	BOOL _clearButtonLongPressGesture;
	
	BOOL isIPadStyle_;
	
	UIButton __weak * highlightedButton_;
	
	struct {
		unsigned int textInputSupportsShouldChangeTextInRange:1;
		unsigned int delegateSupportsTextFieldShouldChangeCharactersInRange:1;
		unsigned int delegateSupportsTextViewShouldChangeTextInRange:1;
	} _delegateFlags;
}

/**
 *  Array of APNumberButton
 */
@property (copy, readwrite, nonatomic) NSArray *numberButtons;

/**
 *  Left function button
 */
@property (strong, readwrite, nonatomic) APNumberButton *leftButton; // の
@property (strong, readwrite, nonatomic) APNumberButton *rightButton; // 条

/**
 *  Right function button
 */
@property (strong, readwrite, nonatomic) APNumberButton *clearButton;


/**
 *  Right function button
 */
@property (strong, readwrite, nonatomic) APNumberButton *deleteButton;

/**
 *  Right function button
 */
@property (strong, readwrite, nonatomic) APNumberButton *closeKeyboardButton;

/**
 *  Right function button
 */
@property (strong, readwrite, nonatomic) APNumberButton *findButton;


/**
 *  APNumberPad delegate
 */
@property (weak, readwrite, nonatomic) id<APNumberPadDelegate> delegate;

/**
 *  Auto-detected text input
 */
@property (weak, readwrite, nonatomic) UIResponder<UITextInput> *textInput;

/**
 *  Last touch on view. For support tap by tap entering text
 */
@property (weak, readwrite, nonatomic) UITouch *lastTouch;

/**
 *  The class to use for styling the number pad
 */
@property (strong, readwrite, nonatomic) Class<APNumberPadStyle> styleClass;

@end


@implementation APNumberPad

+ (instancetype)numberPadWithDelegate:(id<APNumberPadDelegate>)delegate {
	return [self numberPadWithDelegate:delegate numberPadStyleClass:nil iPadStyle:NO];
}

+ (instancetype)numberPadWithDelegate:(id<APNumberPadDelegate>)delegate numberPadStyleClass:(NSString*)styleClass iPadStyle:(BOOL)style{
	return [[self alloc] initWithDelegate:delegate numberPadStyleClass:styleClass iPadStyle:style];
}

- (instancetype)initWithDelegate:(id<APNumberPadDelegate>)delegate numberPadStyleClass:(NSString*)styleClass iPadStyle:(BOOL)style{
	self = [super initWithFrame:CGRectZero];
	if (self) {
		
		isIPadStyle_ = style;
		
		self.styleClass = NSClassFromString(styleClass);
		self.frame = [self.styleClass numberPadFrame];
		self.autoresizingMask = UIViewAutoresizingFlexibleHeight; // for support rotation
		self.backgroundColor = [self.styleClass keyboardDividerColor];
		
		[self addNotificationsObservers];
		
		self.delegate = delegate;
		
		// Number buttons (0-9)
		//
		NSMutableArray *numberButtons = [NSMutableArray array];
		for (int i = 0; i < 11; i++) {
			APNumberButton *numberButton = [self numberButton:i];
			[self addSubview:numberButton];
			[numberButtons addObject:numberButton];
		}
		self.numberButtons = numberButtons;
		
		// Function button
		//
		self.leftButton = [self functionButton];
		[self.leftButton setTitle:@"の" forState:UIControlStateNormal];
		self.leftButton.titleLabel.font = [self.styleClass smallFunctionButtonFont];
		[self.leftButton setTitleColor:[self.styleClass functionButtonTextColor] forState:UIControlStateNormal];
		[self.leftButton addTarget:self action:@selector(functionButtonAction:) forControlEvents:UIControlEventTouchUpInside];
		
		[self.leftButton setBackgroundImage:[UIImage ap_imageWithColor:[self.styleClass numberButtonBackgroundColor]] forState:UIControlStateNormal];
		[self.leftButton setBackgroundImage:[UIImage ap_imageWithColor:[self.styleClass numberButtonHighlightedColor]] forState:UIControlStateHighlighted];

		[self addSubview:self.leftButton];
		
		// Right button
		self.rightButton = [self functionButton];
		[self.rightButton setTitle:@"条" forState:UIControlStateNormal];
		self.rightButton.titleLabel.font = [self.styleClass smallFunctionButtonFont];
		[self.rightButton setTitleColor:[self.styleClass functionButtonTextColor] forState:UIControlStateNormal];
		[self.rightButton addTarget:self action:@selector(functionButtonAction:) forControlEvents:UIControlEventTouchUpInside];
		
		[self.rightButton setBackgroundImage:[UIImage ap_imageWithColor:[self.styleClass numberButtonBackgroundColor]] forState:UIControlStateNormal];
		[self.rightButton setBackgroundImage:[UIImage ap_imageWithColor:[self.styleClass numberButtonHighlightedColor]] forState:UIControlStateHighlighted];
		
		[self addSubview:self.rightButton];

		
		// Clear button
		//
		self.clearButton = [self functionButton];
		[self.clearButton setTitle:NSLocalizedString(@"Jump",@"") forState:UIControlStateNormal];
		[self.clearButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		self.clearButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.f];
		[self.clearButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];

		
		[self.clearButton setBackgroundImage:[UIImage ap_imageWithColor:[self.styleClass functionButtonDefaultButtonColor]] forState:UIControlStateNormal];
		
		[self.clearButton setBackgroundImage:[UIImage ap_imageWithColor:[self.styleClass functionButtonBackgroundColor]] forState:UIControlStateHighlighted];
		
		[self.clearButton setBackgroundImage:[UIImage ap_imageWithColor:[self.styleClass functionButtonBackgroundColor]] forState:UIControlStateDisabled];
		
		[self.clearButton addTarget:self action:@selector(clearButtonAction) forControlEvents:UIControlEventTouchUpInside];
		
		[self addSubview:self.clearButton];
		
		
		
		
		// deleteButton
		self.deleteButton = [self functionButton];
		[self.deleteButton setImage:[UIImage colorizedImage:[UIImage imageNamed:@"Delete"] withTintColor:[self.styleClass functionButtonTextColor] alpha:1 glow:NO] forState:UIControlStateNormal];
		
		[self.deleteButton setTitleColor:[self.styleClass functionButtonTextColor] forState:UIControlStateDisabled];
		
		[self.deleteButton addTarget:self action:@selector(deleteButtonAction) forControlEvents:UIControlEventTouchUpInside];
		
		UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]
																						initWithTarget:self action:@selector(longPressGestureRecognizerAction:)];
		longPressGestureRecognizer.cancelsTouchesInView = NO;
		[self.deleteButton addGestureRecognizer:longPressGestureRecognizer];
		[self addSubview:self.deleteButton];
		
		
		//closeKeyboardButton
		self.closeKeyboardButton = [self functionButton];
		[self.closeKeyboardButton setImage:[UIImage colorizedImage:[UIImage imageNamed:@"Dismiss"] withTintColor:[self.styleClass functionButtonTextColor] alpha:1 glow:NO] forState:UIControlStateNormal];

		[self.closeKeyboardButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		
		[self.closeKeyboardButton addTarget:self action:@selector(closeButtonAction) forControlEvents:UIControlEventTouchUpInside];
		
		[self addSubview:self.closeKeyboardButton];
		
		
		
		//Find button
		self.findButton = [self functionButton];
		[self.findButton setImage:[UIImage colorizedImage:[UIImage imageNamed:@"DockSearchButton-flat"] withTintColor:[self.styleClass functionButtonTextColor] alpha:1 glow:NO] forState:UIControlStateNormal];
		
		[self.findButton addTarget:self action:@selector(findButtonAction) forControlEvents:UIControlEventTouchUpInside];
		
		[self addSubview:self.findButton];
		
		
		
	}
	return self;
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)layoutSubviews {
	[super layoutSubviews];
	
	BOOL isIPad = isIPadStyle_;

	
	int rows = 4;
	int sections = 5;
	
	if( isIPad == YES ) { sections = 4; }
	
	
	CGFloat sep = [self.styleClass separator];
	CGFloat left = 0.f;
	CGFloat top = 0.f;
	
#if defined(__LP64__) && __LP64__
	CGFloat buttonHeight = ceilf( trunc((CGRectGetHeight(self.bounds) - sep * (rows - 1)) / rows) + sep );
#else
	CGFloat buttonHeight = ceilf( trunc((CGRectGetHeight(self.bounds) - sep * (rows - 1)) / rows) + sep );
#endif
	CGFloat fragmentHeight = ceilf( CGRectGetHeight(self.bounds) - buttonHeight * rows );

	
	CGFloat bodyWidth = CGRectGetWidth(self.bounds);

	
	
	if( isIPad == YES ) {
		
		bodyWidth = self.bounds.size.width;
	}
	
	CGFloat margin = (CGRectGetWidth(self.bounds) - bodyWidth ) /2;
	
	if( isIPad == YES ) {
		
		margin = 0;
	}
	
	
	CGSize buttonSize = CGSizeMake( ceilf(( bodyWidth - sep * (sections - 1)) / sections), buttonHeight);
	
	if( sep < 1.0 ) {
		buttonSize = CGSizeMake( ceilf( ( bodyWidth - sep * (sections - 1)) / sections ), buttonHeight);
	}
	
	CGFloat fragmentWidth = ceilf(  ( CGRectGetWidth(self.bounds) - buttonSize.width * sections ) / 2 );
	

	
	// Number buttons (1-9)
	//

	if( isIPad == YES ) {
		
		left = margin ;

		for (int i = 1; i < self.numberButtons.count - 1; i++) {
			
			APNumberButton *numberButton = self.numberButtons[i];
			numberButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);

			if (i == 3 || i == 6 || i == 9 ) {
				left = margin ;
				top += buttonSize.height + sep;
			} else {
				left += buttonSize.width + sep;
			}
		}
	}else {
		
		left = margin + buttonSize.width;

		for (int i = 1; i < self.numberButtons.count - 1; i++) {
			APNumberButton *numberButton = self.numberButtons[i];
			numberButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
			
			if (i == 3 || i == 6 || i == 9 ) {
				left = margin + buttonSize.width;
				top += buttonSize.height + sep;
			} else {
				left += buttonSize.width + sep;
			}
			
		}
	}

	
	// Function button
	//
	
	if( isIPad == YES ) {
		left = margin ;

	}
	else {
		left = margin + buttonSize.width;

	}
	
	self.leftButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height + fragmentHeight);
	
	// Number buttons (0)
	//
	left += buttonSize.width + sep;
	UIButton *zeroButton = self.numberButtons.firstObject;
	zeroButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height + fragmentHeight);
	
	
	left += buttonSize.width + sep;

	self.rightButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height  + fragmentHeight);
	

	if( self.delegate.enableConnectorButton == NO ) {
		[self.rightButton setTitle:@"" forState:UIControlStateNormal];
	}

	
	// Clear button
	//
	left += buttonSize.width + sep;
	top -= buttonSize.height;
	
	self.clearButton.frame = CGRectMake(left, top - sep, buttonSize.width + fragmentWidth, buttonSize.height + buttonSize.height  + fragmentHeight);
	
	
	// deleteButton
	top -= buttonSize.height * 2;
	
	self.deleteButton.frame = CGRectMake(left, top - sep * 4, buttonSize.width + fragmentWidth, buttonSize.height + buttonSize.height + sep*2 );

	
	if( isIPad == NO ) {
		// closeKeyboardButton
		left = 0;
		
		self.findButton.frame = CGRectMake(left, top - sep * 4, buttonSize.width + fragmentWidth - sep, buttonSize.height + buttonSize.height + sep*2 );
		
		
		// findButton
		
		top += buttonSize.height * 2 ;
		
		self.closeKeyboardButton.frame = CGRectMake(left, top - sep, buttonSize.width + fragmentWidth - sep, buttonSize.height + buttonSize.height  + fragmentHeight );
		
	}
	
	[self updateButtons];

}

#pragma mark - Notifications

- (void)addNotificationsObservers {
	[[NSNotificationCenter defaultCenter] addObserver:self
														  selector:@selector(textDidBeginEditing:)
																name:UITextFieldTextDidBeginEditingNotification
															 object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
														  selector:@selector(textDidBeginEditing:)
																name:UITextViewTextDidBeginEditingNotification
															 object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
														  selector:@selector(textDidEndEditing:)
																name:UITextFieldTextDidEndEditingNotification
															 object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
														  selector:@selector(textDidEndEditing:)
																name:UITextViewTextDidEndEditingNotification
															 object:nil];
}

- (void)textDidBeginEditing:(NSNotification *)notification {
	if (![notification.object conformsToProtocol:@protocol(UITextInput)]) {
		return;
	}
	
	//	UIResponder<UITextInput> *textInput = notification.object;
	
//	if (textInput.inputView && self == textInput.inputView) {
//		self.textInput = textInput;
//		
//		_delegateFlags.textInputSupportsShouldChangeTextInRange = NO;
//		_delegateFlags.delegateSupportsTextFieldShouldChangeCharactersInRange = NO;
//		_delegateFlags.delegateSupportsTextViewShouldChangeTextInRange = NO;
//		
//		if ([self.textInput respondsToSelector:@selector(shouldChangeTextInRange:replacementText:)]) {
//			_delegateFlags.textInputSupportsShouldChangeTextInRange = YES;
//		} else if ([self.textInput isKindOfClass:[UITextField class]]) {
//			id<UITextFieldDelegate> delegate = [(UITextField *)self.textInput delegate];
//			if ([delegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)]) {
//				_delegateFlags.delegateSupportsTextFieldShouldChangeCharactersInRange = YES;
//			}
//		} else if ([self.textInput isKindOfClass:[UITextView class]]) {
//			id<UITextViewDelegate> delegate = [(UITextView *)self.textInput delegate];
//			if ([delegate respondsToSelector:@selector(textView:shouldChangeTextInRange:replacementText:)]) {
//				_delegateFlags.delegateSupportsTextViewShouldChangeTextInRange = YES;
//			}
//		}
//	}
}

- (void)textDidEndEditing:(NSNotification *)notification {
	self.textInput = nil;
}

#pragma mark - UIResponder

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	[[UIDevice currentDevice] playInputClick];
	
	// Perform number button action for previous `self.lastTouch`
	//
	if (self.lastTouch) {
		[self performLastTouchAction];
	}
	
	// `touches` contains only one UITouch (self.multipleTouchEnabled == NO)
	//
	self.lastTouch = [touches anyObject];
	
	highlightedButton_ = nil;
	
	// Update highlighted state for number buttons, cancel `touches` for everything but the catched
	//
	CGPoint location = [self.lastTouch locationInView:self];
	for (APNumberButton *b in self.numberButtons) {
		if (CGRectContainsPoint(b.frame, location)) {
			b.highlighted = YES;
			highlightedButton_ = b;
			[self performLastTouchAction];

			break;
			
		} else {
			b.highlighted = NO;
			[b np_touchesCancelled:touches withEvent:event];
		}
	}

	if( [self.delegate respondsToSelector:@selector(numberPadTouchesBegan) ] )
	{
		[self.delegate numberPadTouchesBegan];
		
	}
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	if (!self.lastTouch || ![touches containsObject:self.lastTouch]) {
		return; // ignore old touches movings
	}
	
	CGPoint location = [self.lastTouch locationInView:self];
	
	// Forget highlighted state for functional buttons after move
	//
	if (!CGRectContainsPoint(self.clearButton.frame, location)) {
		[self.clearButton np_touchesCancelled:touches withEvent:event];
		
		// Disable long gesture action for clear button
		//
		_clearButtonLongPressGesture = NO;
	}
	
	if (!CGRectContainsPoint(self.leftButton.frame, location)) {
		[self.leftButton np_touchesCancelled:touches withEvent:event];
	}
	
	if (!CGRectContainsPoint(self.rightButton.frame, location)) {
		[self.rightButton np_touchesCancelled:touches withEvent:event];
	}

	
	
	// Update highlighted state for number buttons
	//
	for (APNumberButton *b in self.numberButtons) {
		
		if( highlightedButton_ == b )
			b.highlighted = CGRectContainsPoint(b.frame, location);
	}
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	if (!self.lastTouch || ![touches containsObject:self.lastTouch]) {
		return; // ignore old touches
	}
	
//	[self performLastTouchAction];
	[self updateButtons];
	
	self.lastTouch = nil;
	highlightedButton_ = nil;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	// Reset hightlighted state for all buttons, forget `self.lastTouch`
	//
	self.leftButton.highlighted = NO;
	self.rightButton.highlighted = NO;
	self.clearButton.highlighted = NO;
	
	for (APNumberButton *b in self.numberButtons) {
		b.highlighted = NO;
	}
	
	[self updateButtons];

	self.lastTouch = nil;
}


-(void)updateButtons {
	
	if( [self.delegate respondsToSelector:@selector(hasText) ] )
	{
		self.clearButton.enabled = [self.delegate hasText];
		
	}

	[self.clearButton setNeedsDisplay];
}

- (void)performLastTouchAction {
	// Reset highlighted state for all buttons, perform action for catched button
	//
//	CGPoint location = [self.lastTouch locationInView:self];
//	for (APNumberButton *b in self.numberButtons) {
//		b.highlighted = NO;
//		if ( b == highlightedButton_ && CGRectContainsPoint(b.frame, location)) {
			[self numberButtonAction:highlightedButton_];
//		}
//	}
}

#pragma mark - Custom accessors

- (void)setStyleClass:(Class)styleClass {
	if (styleClass) {
		_styleClass = styleClass;
	} else {
		_styleClass = [APNumberPadDefaultStyle class];
	}
}

#pragma mark - Left function button

- (UIButton *)leftFunctionButton {
	return nil;
}

#pragma mark - Actions

- (void)numberButtonAction:(UIButton *)sender {
	if (!self.textInput) {
		
		NSString *text = sender.currentTitle;
		
		[self.delegate performSelector:@selector(insertText:) withObject:text];

		return;
	}
	
	NSString *text = sender.currentTitle;
	
	if (_delegateFlags.textInputSupportsShouldChangeTextInRange) {
		if ([self.textInput shouldChangeTextInRange:self.textInput.selectedTextRange replacementText:text]) {
			[self.textInput insertText:text];
		}
	} else if (_delegateFlags.delegateSupportsTextFieldShouldChangeCharactersInRange) {
		NSRange selectedRange = [[self class] selectedRange:self.textInput];
		UITextField *textField = (UITextField *)self.textInput;
		if ([textField.delegate textField:textField shouldChangeCharactersInRange:selectedRange replacementString:text]) {
			[self.textInput insertText:text];
		}
	} else if (_delegateFlags.delegateSupportsTextViewShouldChangeTextInRange) {
		NSRange selectedRange = [[self class] selectedRange:self.textInput];
		UITextView *textView = (UITextView *)self.textInput;
		if ([textView.delegate textView:textView shouldChangeTextInRange:selectedRange replacementText:text]) {
			[self.textInput insertText:text];
		}
	} else {
		[self.textInput insertText:text];
	}
}

-(void)deleteButtonAction {

	if (!self.textInput) {
		
		[self.delegate performSelector:@selector(deleteBackward) withObject:nil];
		
		return;
	}
}

- (void)closeButtonAction {
	if (!self.textInput) {
		
		[self.delegate performSelector:@selector(dismissKeyboard) withObject:nil];
		
		return;
	}
}


- (void)findButtonAction {
	if (!self.textInput) {
		
		[self.delegate performSelector:@selector(findTapped) withObject:nil];
		
		return;
	}
}

- (void)clearButtonAction {
	if (!self.textInput) {
		
		[self.delegate performSelector:@selector(jumpTapped) withObject:nil];

		return;
	}
	
//	if (_delegateFlags.textInputSupportsShouldChangeTextInRange) {
//		UITextRange *textRange = self.textInput.selectedTextRange;
//		if ([textRange.start isEqual:textRange.end]) {
//			UITextPosition *newStart = [self.textInput positionFromPosition:textRange.start inDirection:UITextLayoutDirectionLeft offset:1];
//			textRange = [self.textInput textRangeFromPosition:newStart toPosition:textRange.end];
//		}
//		if ([self.textInput shouldChangeTextInRange:textRange replacementText:@""]) {
//			[self.textInput deleteBackward];
//		}
//	} else if (_delegateFlags.delegateSupportsTextFieldShouldChangeCharactersInRange) {
//		NSRange selectedRange = [[self class] selectedRange:self.textInput];
//		if (selectedRange.length == 0 && selectedRange.location > 0) {
//			selectedRange.location--;
//			selectedRange.length = 1;
//		}
//		UITextField *textField = (UITextField *)self.textInput;
//		if ([textField.delegate textField:textField shouldChangeCharactersInRange:selectedRange replacementString:@""]) {
//			[self.textInput deleteBackward];
//		}
//	} else if (_delegateFlags.delegateSupportsTextViewShouldChangeTextInRange) {
//		NSRange selectedRange = [[self class] selectedRange:self.textInput];
//		if (selectedRange.length == 0 && selectedRange.location > 0) {
//			selectedRange.location--;
//			selectedRange.length = 1;
//		}
//		UITextView *textView = (UITextView *)self.textInput;
//		if ([textView.delegate textView:textView shouldChangeTextInRange:selectedRange replacementText:@""]) {
//			[self.textInput deleteBackward];
//		}
//	} else {
//		[self.textInput deleteBackward];
//	}
}

-(void)setRightButtonTitle:(NSString*)title
{
	[self.rightButton setTitle:title forState:UIControlStateNormal];
}

- (void)functionButtonAction:(id)sender {
	if (!self.textInput) {
		if( sender == self.leftButton ) {
			NSString *text = @"の";
			[self.delegate performSelector:@selector(insertText:) withObject:text];
		}
		
		if( sender == self.rightButton ) {
			[self.delegate performSelector:@selector(inputConnector) withObject:nil];
		}
		
		
		return;
	}
	
	NSString *text = @"の";
	
	if (_delegateFlags.textInputSupportsShouldChangeTextInRange) {
		if ([self.textInput shouldChangeTextInRange:self.textInput.selectedTextRange replacementText:text]) {
			[self.textInput insertText:text];
		}
	} else if (_delegateFlags.delegateSupportsTextFieldShouldChangeCharactersInRange) {
		NSRange selectedRange = [[self class] selectedRange:self.textInput];
		UITextField *textField = (UITextField *)self.textInput;
		if ([textField.delegate textField:textField shouldChangeCharactersInRange:selectedRange replacementString:text]) {
			[self.textInput insertText:text];
		}
	} else if (_delegateFlags.delegateSupportsTextViewShouldChangeTextInRange) {
		NSRange selectedRange = [[self class] selectedRange:self.textInput];
		UITextView *textView = (UITextView *)self.textInput;
		if ([textView.delegate textView:textView shouldChangeTextInRange:selectedRange replacementText:text]) {
			[self.textInput insertText:text];
		}
	} else {
		[self.textInput insertText:text];
	}
}

#pragma mark - Clear button long press

- (void)longPressGestureRecognizerAction:(UILongPressGestureRecognizer *)gestureRecognizer {
	if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
		_clearButtonLongPressGesture = YES;
		[self clearButtonActionLongPress];
	} else if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
		_clearButtonLongPressGesture = NO;
	}
}

- (void)clearButtonActionLongPress {
	if (_clearButtonLongPressGesture) {
		if ([self.textInput hasText]) {
			[[UIDevice currentDevice] playInputClick];
			
			[self clearButtonAction];
			[self performSelector:@selector(clearButtonActionLongPress) withObject:nil afterDelay:0.1f]; // delay like in iOS keyboard
		} else {
			_clearButtonLongPressGesture = NO;
		}
	}
}

#pragma mark - UIInputViewAudioFeedback

- (BOOL)enableInputClicksWhenVisible {
	return YES;
}


#pragma mark - Additions

+ (NSRange)selectedRange:(id<UITextInput>)textInput {
	UITextRange *textRange = [textInput selectedTextRange];
	
	NSInteger startOffset = [textInput offsetFromPosition:textInput.beginningOfDocument toPosition:textRange.start];
	NSInteger endOffset = [textInput offsetFromPosition:textInput.beginningOfDocument toPosition:textRange.end];
	
	return NSMakeRange(startOffset, endOffset - startOffset);
}

#pragma mark - Button fabric
- (APNumberButton *)numberButton:(int)number {
	APNumberButton *b = [APNumberButton buttonWithBackgroundColor:[self.styleClass numberButtonBackgroundColor]
																highlightedColor:[self.styleClass numberButtonHighlightedColor]];
	[b setTitleColor:[self.styleClass numberButtonTextColor] forState:UIControlStateNormal];
	b.titleLabel.font = [self.styleClass numberButtonFont];
	[b setTitle:[NSString stringWithFormat:@"%d", number] forState:UIControlStateNormal];
	return b;
}

- (APNumberButton *)functionButton {
	APNumberButton *b = [APNumberButton buttonWithBackgroundColor:[self.styleClass functionButtonBackgroundColor]
																highlightedColor:[self.styleClass functionButtonHighlightedColor]];
	b.exclusiveTouch = YES;
	return b;
}

@end

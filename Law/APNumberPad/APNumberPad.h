//
//  APNumberPad.h
//
//  Created by Andrew Podkovyrin on 16/05/14.
//  Copyright (c) 2014 Podkovyrin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APNumberPadStyle.h"

@protocol APNumberPadDelegate;

///

@interface APNumberPad : UIView <UIInputViewAudioFeedback>

+ (instancetype)numberPadWithDelegate:(id<APNumberPadDelegate>)delegate numberPadStyleClass:(NSString*)styleClass iPadStyle:(BOOL)style;

-(void)setRightButtonTitle:(NSString*)title;
//+ (instancetype)numberPadWithDelegate:(id<APNumberPadDelegate>)delegate;

/**
 *  Left function button for custom configuration
 */
@property (strong, readonly, nonatomic) UIButton *leftFunctionButton;

/**
 *  The class to use for styling the number pad
 */
@property (strong, readonly, nonatomic) Class<APNumberPadStyle> styleClass;

@end

///

@protocol APNumberPadDelegate <NSObject>

@optional
- (void)insertText:(NSString*)text;
- (void)inputConnector;
- (void)deleteBackward;
- (void)jumpTapped;
- (void)findTapped;
- (void)dismissKeyboard;
-(BOOL)enableConnectorButton;
- (BOOL)hasText;

- (void)numberPadTouchesBegan;

@end

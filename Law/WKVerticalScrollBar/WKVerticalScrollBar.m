//
// WKVerticalScrollBar
// http://github.com/litl/WKVerticalScrollBar
//
// Copyright (C) 2012 litl, LLC
// Copyright (C) 2012 WKVerticalScrollBar authors
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//
//

#import "WKVerticalScrollBar.h"

#define CLAMP(x, low, high)  (((x) > (high)) ? (high) : (((x) < (low)) ? (low) : (x)))

#define HIDE_TIMER_SEC 1.5

@interface WKVerticalScrollBar ()
- (void)commonInit;
@end

@implementation WKVerticalScrollBar

@synthesize handleWidth = _handleWidth;
@synthesize handleHitWidth = _handleHitWidth;
@synthesize handleSelectedWidth = _handleSelectedWidth;
@synthesize handleMinimumHeight = _handleMinimumHeight;

- (id)initWithFrame:(CGRect)frame
{
  if ((self = [super initWithFrame:frame])) {
    [self commonInit];
  }
  return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
  if ((self = [super initWithCoder:aDecoder])) {
    [self commonInit];
  }
  return self;
}

- (void)commonInit
{
  contentOffsetY_ = CGFLOAT_MAX;
  initialFlashHandle_ = YES;
  _handleWidth = 3.0f;
  _handleSelectedWidth = 10.0f;
  _handleHitWidth = 44.0f;
  _handleMinimumHeight = 60.0f;
  
  _handleCornerRadius = _handleWidth / 2;
  _handleSelectedCornerRadius = _handleSelectedWidth / 2;
  
  handleHitArea = CGRectZero;
  
  normalColor = [[UIColor colorWithWhite:0.4f alpha:0.8f] retain];
  selectedColor = [[UIColor colorWithWhite:0.2f alpha:0.8f] retain];
  
  handle = [[CALayer alloc] init];
  [handle setCornerRadius:_handleCornerRadius];
  [handle setAnchorPoint:CGPointMake(1.0f, 0.0f)];
  [handle setFrame:CGRectMake(0, 0, _handleWidth, 0)];
  [handle setBackgroundColor:[normalColor CGColor]];
  [handle setOpacity:0.0f];
  
  [[self layer] addSublayer:handle];
}

- (void)dealloc
{
  [handle release];
  
  [_scrollView removeObserver:self forKeyPath:@"contentOffset"];
  [_scrollView removeObserver:self forKeyPath:@"contentSize"];
  [_scrollView release];
  
  [_handleAccessoryView release];
  
  [normalColor release];
  [selectedColor release];
  
  
  [hideTimer_ invalidate];
  [hideTimer_ release];
  hideTimer_ = nil;
  
  [super dealloc];
  
}

- (UIScrollView *)scrollView
{
  return _scrollView;
}

- (void)setScrollView:(UIScrollView *)scrollView;
{
  [_scrollView removeObserver:self forKeyPath:@"contentOffset"];
  [_scrollView removeObserver:self forKeyPath:@"contentSize"];
  
  [_scrollView release];
  _scrollView = [scrollView retain];
  
  [_scrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
  [_scrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
  [_scrollView setShowsVerticalScrollIndicator:NO];
  
  [self setNeedsLayout];
}

- (UIView *)handleAccessoryView
{
  return _handleAccessoryView;
}

- (void)setHandleAccessoryView:(UIView *)handleAccessoryView
{
  [handleAccessoryView retain];
  [_handleAccessoryView removeFromSuperview];
  [_handleAccessoryView release];
  _handleAccessoryView = handleAccessoryView;
  
  [_handleAccessoryView setAlpha:0.0f];
  [self addSubview:_handleAccessoryView];
  [self setNeedsLayout];
}

- (void)setHandleColor:(UIColor *)color forState:(UIControlState)state
{
  if (state == UIControlStateNormal) {
    [color retain];
    [normalColor release];
    normalColor = color;
  } else if (state == UIControlStateSelected) {
    [color retain];
    [selectedColor release];
    selectedColor = color;
  }
}

- (CGFloat)handleCornerRadius
{
  return _handleCornerRadius;
}

- (void)setHandleCornerRadius:(CGFloat)handleCornerRadius
{
  _handleCornerRadius = handleCornerRadius;
  
  if (!handleDragged) {
    [handle setCornerRadius:_handleCornerRadius];
  }
}

- (CGFloat)handleSelectedCornerRadius
{
  return _handleSelectedCornerRadius;
}

- (void)setHandleSelectedCornerRadius:(CGFloat)handleSelectedCornerRadius
{
  _handleSelectedCornerRadius = handleSelectedCornerRadius;
  
  if (handleDragged) {
    [handle setCornerRadius:_handleSelectedCornerRadius];
  }
}

- (void)layoutSubviews
{
  [super layoutSubviews];
  [self updateUI:NO];
}

-(void)setFrame:(CGRect)frame
{
  [super setFrame:frame];
  
}

-(void)updateUI:(BOOL)animated {
  
  CGFloat contentHeight = [_scrollView contentSize].height;
  CGFloat frameHeight = [_scrollView frame].size.height;
  
  if (contentHeight <= frameHeight) {
    // Prevent divide by 0 below when we arrive here before _scrollView has geometry.
    // Also explicity hide handle when not needed, occasionally it's left visible due to
    // layoutSubviews being called with transient & invalid geometery
    [handle setOpacity:0.0f];
    return;
  }
  
  [CATransaction begin];
  
  if( animated == NO )
    [CATransaction setDisableActions:YES];
  
  CGRect bounds = [self bounds];
  
  // Calculate the current scroll value (0, 1) inclusive.
  // Note that contentOffset.y only goes from (0, contentHeight - frameHeight)
  // prevent divide by 0 below when we arrive here before _scrollView has geometry
  CGFloat scrollValue = (contentHeight - frameHeight == 0) ? 0
  : [_scrollView contentOffset].y / (contentHeight - frameHeight);
  
  // Set handleHeight proportionally to how much content is being currently viewed
  CGFloat handleHeight = CLAMP((frameHeight / contentHeight) * bounds.size.height,
                               _handleMinimumHeight, bounds.size.height);
  
  if( initialFlashHandle_ != YES )
    [handle setOpacity:(handleHeight == bounds.size.height) ? 0.0f : 1.0f];
  
  // Not only move the handle, but also shift where the position maps on to the handle,
  // so that the handle doesn't go off screen when the scrollValue approaches 1.
  CGFloat handleY = CLAMP((scrollValue * bounds.size.height) - (scrollValue * handleHeight),
                          0, bounds.size.height - handleHeight);
  
  CGFloat previousWidth = [handle bounds].size.width ?: _handleWidth;
  
  
  [handle setPosition:CGPointMake(bounds.size.width - 3, handleY + 3)];
  [handle setBounds:CGRectMake(0, 0, previousWidth, handleHeight - 6)];
  
  // Center the accessory view to the left of the handle
  CGRect accessoryFrame = [_handleAccessoryView frame];
  [_handleAccessoryView setCenter:CGPointMake(bounds.size.width - _handleHitWidth - (accessoryFrame.size.width / 2),
                                              handleY + (handleHeight / 2))];
  
  handleHitArea = CGRectMake(bounds.size.width - _handleHitWidth, handleY,
                             _handleHitWidth, handleHeight);
  
  
  [CATransaction commit];
  
  
  
  if( initialFlashHandle_ == YES ) {
    initialFlashHandle_ = NO;
    // Execute in next run loop to start with refreshed aniation loop
    [self performSelector:@selector(flashHandle) withObject:nil afterDelay:0.0];
  }
  else {
    [hideTimer_ invalidate];
    [hideTimer_ release];
    hideTimer_ = nil;
    hideTimer_ = [NSTimer scheduledTimerWithTimeInterval:HIDE_TIMER_SEC target:self selector: @selector(hideHandle) userInfo:nil repeats:NO];
    [hideTimer_ retain];
    
  }
}


- (BOOL)handleVisible
{
  return [handle opacity] == 1.0f;
}

- (void)growHandle
{
  if (![self handleVisible]) {
    return;
  }
  
  [CATransaction begin];
  [CATransaction setAnimationDuration:0.3f];
  
  [handle setCornerRadius:_handleSelectedCornerRadius];
  [handle setBounds:CGRectMake(0, 0, _handleSelectedWidth, [handle bounds].size.height)];
  [handle setBackgroundColor:[selectedColor CGColor]];
  
  [CATransaction commit];
  
  [UIView animateWithDuration:0.3f animations:^{
    [_handleAccessoryView setAlpha:1.0f];
  }];
  
  [hideTimer_ invalidate];
  [hideTimer_ release];
  hideTimer_ = nil;
  hideTimer_ = [NSTimer scheduledTimerWithTimeInterval:HIDE_TIMER_SEC target:self selector: @selector(hideHandle) userInfo:nil repeats:NO];
  [hideTimer_ retain];
  
}

-(void)hideHandle
{
  [self shrinkHandle];
  [UIView animateWithDuration:0.3f animations:^{
    handle.opacity = 0.0;
    
  }];
  
  [hideTimer_ invalidate];
  [hideTimer_ release];
  hideTimer_ = nil;
}

- (void)shrinkHandle
{
  if (![self handleVisible]) {
    return;
  }
  
  [CATransaction begin];
  [CATransaction setAnimationDuration:0.3f];
  
  [handle setCornerRadius:_handleCornerRadius];
  [handle setBounds:CGRectMake(0, 0, _handleWidth, [handle bounds].size.height)];
  [handle setBackgroundColor:[normalColor CGColor]];
  
  [CATransaction commit];
  
  [UIView animateWithDuration:0.3f animations:^{
    [_handleAccessoryView setAlpha:0.0f];
  }];
}

-(void)flashHandle
{
  
  [CATransaction begin];
  [CATransaction setDisableActions:YES];
  
  [handle setCornerRadius:_handleCornerRadius];
  [handle setBounds:CGRectMake(0, 0, _handleWidth-5, [handle bounds].size.height)];
  [handle setBackgroundColor:[[normalColor colorWithAlphaComponent:0.0] CGColor]];
  [CATransaction commit];
  
  
  
  [CATransaction begin];
  [CATransaction setAnimationDuration:0.20f];
  
  [handle setCornerRadius:_handleSelectedCornerRadius];
  [handle setBounds:CGRectMake(0, 0, _handleSelectedWidth, [handle bounds].size.height)];
  [handle setBackgroundColor:[selectedColor CGColor]];
  
  
  [CATransaction commit];
  
  
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:0.20f];
    
    [handle setCornerRadius:_handleCornerRadius];
    [handle setBounds:CGRectMake(0, 0, _handleWidth, [handle bounds].size.height)];
    [handle setBackgroundColor:[normalColor CGColor]];
    [CATransaction setCompletionBlock:^{ }];
    [CATransaction commit];
    
  });
  
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
  return CGRectContainsPoint(handleHitArea, point);
}

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
  if (![self handleVisible]) {
    return NO;
  }
  
  lastTouchPoint = [touch locationInView:self];
  
  // When the user initiates a drag, make the handle grow so it's easier to see
  handleDragged = YES;
  [self growHandle];
  
  [self setNeedsLayout];
  
  return YES;
}

- (BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
  CGPoint point = [touch locationInView:self];
  
  CGSize contentSize = [_scrollView contentSize];
  CGPoint contentOffset = [_scrollView contentOffset];
  CGFloat frameHeight = [_scrollView frame].size.height;
  CGFloat deltaY = ((point.y - lastTouchPoint.y) / [self bounds].size.height)
  * [_scrollView contentSize].height;
  
  [_scrollView setContentOffset:CGPointMake(contentOffset.x,  CLAMP(contentOffset.y + deltaY,
                                                                    0, contentSize.height - frameHeight))
                       animated:NO];
  lastTouchPoint = point;
  
  
  [hideTimer_ invalidate];
  [hideTimer_ release];
  hideTimer_ = nil;
  hideTimer_ = [NSTimer scheduledTimerWithTimeInterval:HIDE_TIMER_SEC target:self selector: @selector(hideHandle) userInfo:nil repeats:NO];
  [hideTimer_ retain];
  
  return YES;
}

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
  lastTouchPoint = CGPointZero;
  
  // When user drag is finished, return handle to previous size
  handleDragged = NO;
  [self shrinkHandle];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
  if (object != _scrollView) {
    return;
  }
  
  if( contentOffsetY_ != _scrollView.contentOffset.y || contentSizeHeight_ != _scrollView.contentSize.height) {
    
    
    if( contentOffsetY_ == CGFLOAT_MAX ) {
      // First time drawing
      
      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        contentOffsetY_ = _scrollView.contentOffset.y;
        contentSizeHeight_ = _scrollView.contentSize.height;
        
        
        [self updateUI:NO];
        
      });
      
    }else {
      
      contentOffsetY_ = _scrollView.contentOffset.y;
      contentSizeHeight_ = _scrollView.contentSize.height;
      
      
      [self updateUI:NO];
      
    }
    
  }
}

@end

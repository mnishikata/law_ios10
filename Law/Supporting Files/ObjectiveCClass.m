//
//  ObjectiveCClass.m
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2014/09/05.
//  Copyright (c) 2014年 Catalystwo. All rights reserved.
//

#import "ObjectiveCClass.h"

#import "ZipFile.h"
#import "ZipException.h"
#import "FileInZipInfo.h"
#import "ZipWriteStream.h"
#import "ZipReadStream.h"
#import "ZipArchive.h"
//#import "BlockCompression.h"

#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <mach/mach.h>
//#import "GAI.h"
//#import "GAIDictionaryBuilder.h"
#endif


#if TARGET_OS_IPHONE



@implementation UIPasteboard (AttributedString)

- (void) setAttributedString:(NSAttributedString *)attributedString alternativeText:(NSString*)text{

	
	NSDictionary *documentAttributes = [NSDictionary dictionaryWithObjectsAndKeys:NSHTMLTextDocumentType, NSDocumentTypeDocumentAttribute, nil];
	NSData *htmlData = [attributedString dataFromRange:NSMakeRange(0, attributedString.length) documentAttributes:documentAttributes error:NULL];
	NSString *htmlString = [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
	
	
	NSDictionary *resourceDictionary = @{ @"WebResourceData" : [htmlString dataUsingEncoding:NSUTF8StringEncoding],
													  @"WebResourceFrameName":  @"",
													  @"WebResourceMIMEType" : @"text/html",
													  @"WebResourceTextEncodingName" : @"UTF-8",
													  @"WebResourceURL" : @"about:blank" };
	
	
	
	NSDictionary *htmlItem = @{ (NSString *)kUTTypeText : text,
										 @"Apple Web Archive pasteboard type" : @{ @"WebMainResource" : resourceDictionary } };
	
	[self setItems:@[ htmlItem ]];
}


@end

#endif


@implementation ObjectiveCClass

#if TARGET_OS_IPHONE



+(void)updateAppearance:(BOOL)darkMode
{
	if( darkMode )
		[[UIView appearanceWhenContainedIn:[UIAlertController class], nil] setBackgroundColor:[UIColor grayColor]];
	else
		[[UIView appearanceWhenContainedIn:[UIAlertController class], nil] setBackgroundColor:[UIColor clearColor]];
	
}

+(void)track:(NSString*)title
{
/*
	id tracker = [[GAI sharedInstance] defaultTracker];
	
	[tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Law"     // Event category (required)
																			action:@"Did Appear"  // Event action (required)
																			 label:title          // Event label
																			 value:nil] build]];    // Event value
*/
}
#endif

+ (void)registerDefaultsFromSettingsBundle {
	NSString *settingsBundle = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
	if(!settingsBundle) {
		NSLog(@"Could not find Settings.bundle");
		return;
	}
	
    NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[settingsBundle stringByAppendingPathComponent:@"Root.plist"]];
    NSArray *preferences = [settings objectForKey:@"PreferenceSpecifiers"];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    for(NSDictionary *prefSpecification in preferences) {
        NSString *key = [prefSpecification objectForKey:@"Key"];
        if(key) {
            
            if( [ud objectForKey:key] == nil ) {
                
                [ud setObject:[prefSpecification objectForKey:@"DefaultValue"] forKey:key];
                
            }
        }
    }
    
	[[NSUserDefaults standardUserDefaults] synchronize];
	
	
}

+(NSPredicate*)thePredicate:(NSString*)md5 title:(NSString*)title
{
    return [NSPredicate predicateWithFormat:@"(md5 == %@) && (title == %@)", md5, title];
    
}

+(BOOL)unzipFileAt:(NSString*)path to:(NSString*)unzipPath
{
    ZipArchive* zip = [[ZipArchive alloc] init];
    BOOL flag = [zip UnzipOpenFile: path];
    
    
    flag = [zip UnzipFileTo: unzipPath overWrite:YES];
    
    flag = [zip CloseZipFile2];

    return flag;
}

+(NSString*)zipFileAt:(NSURL*)url completion:(void (^)(NSString* path, NSError* error))block
{
    
    NSFileManager* fm = [NSFileManager defaultManager];
    NSError* error = nil;
    BOOL success;
    
    
    
    NSString* baseFolderName = url.lastPathComponent;
    
    NSString* name = [baseFolderName stringByAppendingString:@".zip"];
    
    
    NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:name];
    
    if( [fm fileExistsAtPath: filePath] )
    {
        success = [fm removeItemAtPath:filePath error:&error];
        if( !success )
        {
            if( block )
                block( nil, error );
            
            return nil;
        }
    }
    
    NSFileCoordinator* coordinator = [[NSFileCoordinator alloc] initWithFilePresenter:nil];
    
    [coordinator coordinateReadingItemAtURL:url options:NSFileCoordinatorReadingWithoutChanges error:nil byAccessor:^(NSURL *newURL) {
        
        
        NSArray* subitems = [[NSFileManager defaultManager] contentsOfDirectoryAtURL:url includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];
        
        
        // Create Zip
        
        ZipFile *zipFile= [[ZipFile alloc] initWithFileName:filePath mode:ZipFileModeCreate];
        
        
        for( NSURL* subItemURL in subitems )
        {
            @autoreleasepool {
                // Create  stream for sub item
                
                NSString* zipName = [NSString stringWithFormat:@"%@/%@", baseFolderName, subItemURL.lastPathComponent];
                
                ZipWriteStream *stream1 = [zipFile writeFileInZipWithName:zipName
                                                                 fileDate:[NSDate date]
                                                         compressionLevel:ZipCompressionLevelBest];
                
                NSData* data = [[NSData alloc] initWithContentsOfURL: subItemURL];
                [stream1 writeData:data];
                [stream1 finishedWriting];
                
            }
        }
        
        
        [zipFile close];
        
    }];
    
    
    
    if( block )
    {
        block( filePath, nil );
    }
    
    return filePath;
    
}


+(NSString*)versionString
{
	CFBundleRef bundle = CFBundleGetMainBundle();
	CFStringRef versionString = CFBundleGetValueForInfoDictionaryKey(bundle, CFSTR("CFBundleShortVersionString"));
	CFStringRef buildVersionString = CFBundleGetValueForInfoDictionaryKey(bundle, CFSTR("CFBundleVersion"));
	
	NSString* string = [NSString stringWithFormat:@"Version %@ (%@)",  (__bridge NSString*)versionString,  (__bridge NSString*)buildVersionString];
	
	return string;
}

+(NSString*)ramUsage
{
	
	struct mach_task_basic_info info;
	mach_msg_type_number_t size = MACH_TASK_BASIC_INFO_COUNT;
	kern_return_t kerr = task_info(mach_task_self(),
											 MACH_TASK_BASIC_INFO,
											 (task_info_t)&info,
											 &size);
	if( kerr == KERN_SUCCESS ) {
		NSLog(@"Memory in use (in bytes): %llu", info.resident_size);
		return [NSString stringWithFormat:@"%.1lf MB", ((double)info.resident_size/1024.0/1024.0)];

	} else {
		NSLog(@"Error with task_info(): %s", mach_error_string(kerr));
	}
	
	
	return @"";
}

//
//+(void)compress:(NSURL*)inURL to:(NSURL*)outURL
//{
//	
//	FILE* dataIn;
//	FILE* dataOut;
//	compression_algorithm algorithm = COMPRESSION_LZMA;
//
//	
//	char* path = (char*)malloc(512);
//	if ([inURL.path getCString:path maxLength:512 encoding:NSUTF8StringEncoding]) {
//		dataIn = fopen(path, "rb");
//	}
//	
//	if( dataIn != NULL )
//		if ([outURL.path getCString:path maxLength:512 encoding:NSUTF8StringEncoding]) {
//			dataOut = fopen(path, "wb");
//			if (dataOut != NULL) {
//				
//				float compression_ratio;
//				
//				compression_ratio = doBlockCompression(dataIn, dataOut, algorithm);
//				NSLog(@"compression_ratio = %.2f", compression_ratio);
//			}
//		}
//}

@end

//@interface UIMenuItem (ext)
//- (instancetype)initWithTitle:(NSString *)title
//							  action:(SEL)action;
//@end
//
//@implementation UIMenuItem (ext)
//
//- (instancetype)initWithTitle:(NSString *)title
//							  action:(SEL)action {
//	self = [super init];
//	if (self) {
//
//		NSLog(@"action %@", NSStringFromSelector(action));
//		
//	}
//	return self;
//}
//
//@end

//@interface UICalloutBarButton : UIButton
//@end
//
//@implementation UICalloutBarButton (ext)
//
//
//@end

//@interface UICalloutBar (ext)
//@end
//
//@implementation UICalloutBar (ext)
//
//- (void)updateAvailableButtons {
//
//
//}
//@end

//@interface UICalloutBar (ext)
//
//@end
//
//
//@implementation UICalloutBar (ext)

//-(id)targetForAction:(SEL)action {
//	
//	id obj = [self targetForAction:action withSender:nil];
//
//	NSLog(@"action %@ %@",NSStringFromSelector(action), obj);
//	
//	
//	if( [@"highlight" isEqualToString:NSStringFromSelector(action) ] ) {
//		return [UIApplication sharedApplication];
//	}
//	return nil;
//	
//}


//
//- (void)updateAvailableButtons
//{
//	id target = [self targetForAction:@selector(copy:) ];
//	
//	NSLog(@"target %@", NSStringFromClass([target class]));
//
//
//}



//@end





//@interface UIMenuController (ext)
//@end
//
//@implementation UIMenuController (ext)
//
///*
// calloutBar:willStartAnimation:
// calloutBar:didFinishAnimation:
// */
//
//static BOOL isDisplayingMenu = NO;
//
//- (void)calloutBar:(id)arg1 willStartAnimation:(id)arg2
//{
//	[arg1 performSelector:@selector(removeFromSuperview)];
//	
//	id point = [arg1 valueForKey:@"m_targetPoint"];
//	
//	
//	
//	NSLog(@"arg1 %@",arg1);
//	NSLog(@"arg2 %@",arg2);
//	NSLog(@"point %@",point);
//	
//	
//	NSLog(@"%@", self.menuItems);
//	
//	
//	if( isDisplayingMenu == NO ) {
//		
//		isDisplayingMenu = YES;
//		
//		[self setMenuVisible:YES animated:YES];
//		
//		isDisplayingMenu = NO;
//	}
//	
//
//	return ;
//}
//
//@end




//
//  ObjectiveCClass.h
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2014/09/05.
//  Copyright (c) 2014年 Catalystwo. All rights reserved.
//

#import <Foundation/Foundation.h>

#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>


@interface UIPasteboard (AttributedString)
- (void) setAttributedString:(NSAttributedString *)attributedString alternativeText:(NSString*)text;
@end


#endif



@interface ObjectiveCClass : NSObject
+(void)track:(NSString*)title;
+ (void)registerDefaultsFromSettingsBundle;
+(NSPredicate*)thePredicate:(NSString*)md5 title:(NSString*)title;
+(BOOL)unzipFileAt:(NSString*)path to:(NSString*)unzipPath;
+(NSString*)zipFileAt:(NSURL*)url completion:(void (^)(NSString* path, NSError* error))block;
+(void)updateAppearance:(BOOL)darkMode;
+(NSString*)versionString;
+(NSString*)ramUsage;

//+(void)compress:(NSURL*)inURL to:(NSURL*)outURL;

@end


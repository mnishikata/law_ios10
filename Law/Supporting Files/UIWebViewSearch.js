var uiWebview_SearchResultCount = 0;
var a = new Array();

/*!
 @method     uiWebview_HighlightAllOccurencesOfStringForElement
 @abstract   // helper function, recursively searches in elements and their child nodes
 @discussion // helper function, recursively searches in elements and their child nodes
 
 element    - HTML elements
 keyword    - string to search
 */



//
////we create a SPAN element for every parts of matched keywords
//var span = document.createElement("span");
//var text = document.createTextNode(value.substr(idx,keyword.length));
//span.appendChild(text);
//span.setAttribute("class","uiWebviewHighlight");
//span.style.backgroundColor="black";
//span.style.color="white";
//uiWebview_SearchResultCount++;
//// update the counter
//text = document.createTextNode(value.substr(idx+keyword.length));
//element.deleteData(idx, value.length - idx);
//var next = element.nextSibling;
//element.parentNode.insertBefore(span, next);
//element.parentNode.insertBefore(text, next);
//element = text;
//
//TO THIS:
//
//
//var span = document.createElement("span");
//var text = document.createTextNode(value.substr(idx,keyword.length));
//span.appendChild(text);
//span.setAttribute("class","MyAppHighlight");
//text = document.createTextNode(value.substr(idx+keyword.length));
//element.deleteData(idx, value.length - idx);
//var next = element.nextSibling;
//element.parentNode.insertBefore(span, next);
//element.parentNode.insertBefore(text, next);
//element = text;
//span.scrollIntoView();
//span.style.backgroundColor="yellow";
//// must set the CSS style after scrolling to first search instance span.style.color="black";
//a.push(span);
//// SET THIS CODE HERE
//SearchResultCount++;
//// update the counterIN OBJECTIVE-C CHANGE THIS:
//- (NSInteger)highlightAllOccurencesOfString:(NSString*)str{
// // The JS File
//	NSString *filePath = [[NSBundle mainBundle] pathForResource:@"UIWebViewSearch" ofType:@"js" inDirectory:@""];
//	NSData *fileData = [NSData dataWithContentsOfFile:filePath];
//	NSString *jsString = [[NSMutableString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
//	[_webView stringByEvaluatingJavaScriptFromString:jsString];
//	[jsString release];
//	// The JS Function
//	NSString *startSearch = [NSString stringWithFormat:@"uiWebview_HighlightAllOccurencesOfString('%@')",str];
//	[_webView stringByEvaluatingJavaScriptFromString:startSearch];
//	// Search Occurence Count
//	// uiWebview_SearchResultCount - is a javascript
//	var NSString *result = [_webView stringByEvaluatingJavaScriptFromString:@"uiWebview_SearchResultCount"];
//	return [result integerValue];
//}
//TO THIS:
//- (NSInteger)highlightAllOccurencesOfString:(NSString*)str {
// NSString *filePath = [[NSBundle mainBundle] pathForResource:@"UIWebViewSearch" ofType:@"js"];
//	NSData *fileData = [NSData dataWithContentsOfFile:filePath];
//	NSString *jsString = [[NSMutableString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
//	[webView stringByEvaluatingJavaScriptFromString:jsString];
//	NSString *startSearch = [NSString stringWithFormat:@"HighlightAllOccurencesOfString('%@')",str];
//	[webView stringByEvaluatingJavaScriptFromString:startSearch];
//	NSString *result = [webView stringByEvaluatingJavaScriptFromString:@"a.length"];
//	currentPosition = [result2 intValue] - 1;
//	return [result integerValue];
//}
//
//ADD THIS METHOD:
//- (IBAction)next:(id)sender { // I use a segmented control declared in the header file. Connect its outlet and method in the XIB file if (sgmtPreviousNext.selectedSegmentIndex == 0) { currentPosition = currentPosition + 1;
//										// declare currentPosition in the header file: int currentPosition;
//	NSString *previousScrollPosition = [NSString stringWithFormat:@"a[%d].scrollIntoView()", currentPosition];
//	[webView stringByEvaluatingJavaScriptFromString:previousScrollPosition];
//}
//if (sgmtPreviousNext.selectedSegmentIndex == 1) {
// currentPosition = currentPosition - 1;
//	NSString *nextScrollPosition = [NSString stringWithFormat:@"a[%d].scrollIntoView()", currentPosition];
//	[webView stringByEvaluatingJavaScriptFromString:nextScrollPosition];
//}
//}


function uiWebview_HighlightAllOccurencesOfStringForElement(element,keyword) {
    
    if (element) {
        if (element.nodeType == 3) {        // Text node
			  
			  var thisArray = new Array();

            while (true) {
					

                //if (counter < 1) {
                var value = element.nodeValue;  // Search for keyword in text node
                var idx = value.toLowerCase().indexOf(keyword);
                
                if (idx < 0) break;             // not found, abort
                
                //(value.split);
                
                //we create a SPAN element for every parts of matched keywords
					var span = document.createElement("span");
					var text = document.createTextNode(value.substr(idx,keyword.length));
					span.appendChild(text);
					span.setAttribute("class","uiWebviewHighlight");
					span.style.backgroundColor="yellow";
					span.style.color="black";

					text = document.createTextNode(value.substr(idx+keyword.length));
					element.deleteData(idx, value.length - idx);
					var next = element.nextSibling;
					element.parentNode.insertBefore(span, next);
					element.parentNode.insertBefore(text, next);
					element = text;
					
//					span.style.backgroundColor="blue";
//					// must set the CSS style after scrolling to first search instance span.style.color="black";
//					a.push(span);
					
					thisArray.splice(0, 0, span)

					
					// SET THIS CODE HERE
					uiWebview_SearchResultCount++;
                
            }
			  
			  a = a.concat( thisArray )
			  
        } else if (element.nodeType == 1) { // Element node
            if (element.style.display != "none" && element.nodeName.toLowerCase() != 'select') {
                for (var i=element.childNodes.length-1; i>=0; i--) {
                    uiWebview_HighlightAllOccurencesOfStringForElement(element.childNodes[i],keyword);
                }
            }
        }
    }

	return uiWebview_SearchResultCount
}

String.prototype.toOneByteAlphaNumeric = function(){
	return this.replace(/[Ａ-Ｚａ-ｚ０-９]/g, function(s) {
							  return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
							  });
}

// 半角英数字文字列を全角文字列に変換する
String.prototype.toTwoByteAlphaNumeric = function(){
	return this.replace(/[A-Za-z0-9]/g, function(s) {
							  return String.fromCharCode(s.charCodeAt(0) + 0xFEE0);
							  });
}


// the main entry point to start the search
function uiWebview_HighlightAllOccurencesOfString(keyword) {
    uiWebview_RemoveAllHighlights();
    return uiWebview_HighlightAllOccurencesOfStringForElement(document.body, keyword.toTwoByteAlphaNumeric());
}

// helper function, recursively removes the highlights in elements and their childs
function uiWebview_RemoveAllHighlightsForElement(element) {
    if (element) {
        if (element.nodeType == 1) {
            if (element.getAttribute("class") == "uiWebviewHighlight") {
                var text = element.removeChild(element.firstChild);
                element.parentNode.insertBefore(text,element);
                element.parentNode.removeChild(element);
                return true;
            } else {
                var normalize = false;
                for (var i=element.childNodes.length-1; i>=0; i--) {
                    if (uiWebview_RemoveAllHighlightsForElement(element.childNodes[i])) {
                        normalize = true;
                    }
                }
                if (normalize) {
                    element.normalize();
                }
            }
        }
    }
    return false;
}

// the main entry point to remove the highlights
function uiWebview_RemoveAllHighlights() {
    uiWebview_SearchResultCount = 0;
    uiWebview_RemoveAllHighlightsForElement(document.body);
}


function getBodyTextOffset(node, offset) {
	var sel = window.getSelection();
	var range = document.createRange();
	range.selectNode(document.body);
	range.setEnd(node, offset);
	sel.removeAllRanges();
	sel.addRange(range);
	

	var content = sel.getRangeAt(0).cloneContents();
	
	span = document.createElement('div');

	span.appendChild(content);
	var htmlContent = span.innerHTML;
	
	
	return htmlContent;
}

var suffix_
var theString_
var thisContents_

/*
 
1  node = startContainer
 
2. node contents Contains A NAME ?
 
3. YES -> return string and index

4. NO ->  node = node.parentNode ... go to 2
 
 */

var selectedIndex_
var selectionNodeContents_

function getNodeTextOffset(targetNode, currentNode, offsetInCurrentNode) {
	var sel = window.getSelection();
	var range = document.createRange();
	range.selectNode(targetNode);
	
	if( range == null ) { return null }
	
	range.setEnd(currentNode, offsetInCurrentNode);
	sel.removeAllRanges();
	sel.addRange(range);
	
	
	var content = sel.getRangeAt(0).cloneContents();
	
	span = document.createElement('div');
	
	span.appendChild(content);
	var htmlContent = span.innerHTML;
	
	
	return htmlContent;
}

function getSelectionIndexAndNodeAndTheString_AndSuffix_AndThisContents_() {
	
	return getSelectionIndexAndNode() + "<$>" + theString_ + "<$>" + suffix_ +  "<$>" + thisContents_
	
}

function getSelectionIndexAndNode() {
	
	var source = 0
	
	if (window.getSelection) {
		sel = window.getSelection();
		if (sel.rangeCount) {
			range = sel.getRangeAt(sel.rangeCount - 1);
			
			originalSelection_ = range.cloneRange()
			
			theString_ = range.toString()

			var node = range.startContainer
			
			
			while(1) {
				source = getNodeTextOffset(node, range.startContainer, range.startOffset);
				
				

				if( source == null ) { break }
				
				if( source.toLowerCase().indexOf("a name") > -1 ) {
					break
				}
				
				node = node.parentNode.previousElementSibling
				
			}
			suffix_ = getNodeTextOffset(node, range.endContainer, range.endOffset);
			range.setEnd( range.endContainer, range.endContainer.length)
			thisContents_ = getNodeTextOffset(node, range.endContainer, range.endOffset);

			return source

		}
	}

	return null
}

var originalSelection_

function replaceSelectionWith( htmlSnippet, uuid, className ) {

	originalSelection_.deleteContents()
	
	
	var span = buildHighlightElement( htmlSnippet, uuid, className )
	originalSelection_.insertNode( span )
	
}

// highlight class = 6
function buildHighlightElement( htmlSnippet, uuid, className ) {
	
	var span = document.createElement("u")
	span.className = className; //"highlight";
	span.id = uuid
	
	var link = document.createElement("a")
	link.href = "highlight:" + uuid
	link.className = "highlight"
	
	var tt = document.createTextNode( htmlSnippet )
	
	link.appendChild(tt)
	span.appendChild(link)

	
	return span
}

function replaceElementWith( elementId, htmlSnippet  ) {

	// select element
	var element = document.getElementById(elementId)
	
	var tt = document.createTextNode( htmlSnippet )
	
	element.parentNode.replaceChild( tt, element )

}


function replaceElementWithElement( elementId, htmlSnippet, className  ) {
	
	// select element
	var element = document.getElementById(elementId)
	
	var span = buildHighlightElement( htmlSnippet, elementId, className )

	element.parentNode.replaceChild( span, element )
	
}

function getSelectionPrefix() {
	var sel, range;
	var start = 0, end = 0;
	if (window.getSelection) {
		sel = window.getSelection();
		if (sel.rangeCount) {
			range = sel.getRangeAt(sel.rangeCount - 1);
			start = getBodyTextOffset(range.startContainer, range.startOffset);
			suffix_ = getBodyTextOffset(range.endContainer, range.endOffset);

			theString_ = range.toString()
			
			return start;
			
		}
	} else if (document.selection) {
		// IE stuff here
	}

	return nil;
}

function getSelectionSuffix() {
	var sel, range;
	var start = 0, end = 0;
	if (window.getSelection) {
		sel = window.getSelection();
		if (sel.rangeCount) {
			range = sel.getRangeAt(sel.rangeCount - 1);

			suffix_ = getBodyTextOffset(range.endContainer, range.endOffset);
			
			return end;
			
		}
	} else if (document.selection) {
		// IE stuff here
	}
	return nil;
}

Element.prototype.documentOffsetTop = function () {
	return this.offsetTop + ( this.offsetParent ? this.offsetParent.documentOffsetTop() : 0 );
};

function selectText(id) {
	
	var ele = document.getElementById(id);
	ele.scrollIntoView(true)
	ele.select()

}

function jumpToAnchor(anchor) {
  document.location = anchor
}


function blinkText(id) {
	
	// Scroll
	
	var ele = document.getElementById(id);
	ele.scrollIntoView(true)
	
	window.scrollBy( 0, -window.innerHeight/2 );
	
	
	// Blink interval
	setInterval(blinker, 150);
	
	
	// Flag to see what state text is in (true or false)
	var flag = true;
	
	// Number of times to blink text
	var blinkNum = 4;
	var i = 1;
	var divID = document.getElementById(id);
	var theColor = divID.style.backgroundColor
	
	function blinker() {
		if (i < blinkNum) {
			if (flag) {
				divID.style.backgroundColor = "#999999";
				flag = false;
			} else {
				divID.style.backgroundColor = theColor;
				flag = true;
			}
			
			i++;
		} else {
			// Delete if it's still showing
			divID.style.backgroundColor = theColor;
			
			// Stop blinking
			clearInterval(blinker);
		}
	}
}

function blinkElement(element) {
	
	
	element.scrollIntoView();
	window.scrollBy( 0, -window.innerHeight/2 );

	
	// Blink interval
	setInterval(blinker, 150);
	
	// Flag to see what state text is in (true or false)
	var flag = true;
	
	// Number of times to blink text
	var blinkNum = 4;
	var i = 1;
	var divID = element;
	var theColor = divID.style.backgroundColor
	
	function blinker() {
		if (i < blinkNum) {
			if (flag) {
				divID.style.backgroundColor = "#999999";
				flag = false;
			} else {
				divID.style.backgroundColor = theColor;
				flag = true;
			}
			
			i++;
		} else {
			// Delete if it's still showing
			divID.style.backgroundColor = theColor;
			
			// Stop blinking
			clearInterval(blinker);
		}
	}
}

//
//function getElementOffsetY(  ) {
//	
//	
//	var vpH = viewPortHeight(), // Viewport Height
//	st = scrollY(), // Scroll Top
//	y = posY( document.elementFromPoint(2, 500) );
//	
//	
//	var elem = document.elementFromPoint(2, 500);
//	
//	if( elem == null ) { return "null"; }
//	elem.style.color = 'red';
//	
//	return y;
//}
//
//function viewPortHeight() {
//	var de = document.documentElement;
//	
//	if(!!window.innerWidth)
//	{ return window.innerHeight; }
//	else if( de && !isNaN(de.clientHeight) )
//	{ return de.clientHeight; }
//	
//	return 0;
//}
//
//function scrollY() {
//	if( window.pageYOffset ) { return window.pageYOffset; }
//	return Math.max(document.documentElement.scrollTop, document.body.scrollTop);
//}
//
//
//function posY(elm) {
//	var test = elm, top = 0;
//	
//	while(!!test && test.tagName.toLowerCase() !== "body") {
//		top += test.offsetTop;
//		test = test.offsetParent;
//	}
//	
//	return top;
//}



//var top = document.getElementById( 'middle' ).documentOffsetTop() - ( window.innerHeight / 2 );
//window.scrollTo( 0, top );

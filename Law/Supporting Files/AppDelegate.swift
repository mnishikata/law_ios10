//
//  AppDelegate.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2014/08/25.
//  Copyright (c) 2014年 Catalystwo. All rights reserved.
//


import UIKit
import Firebase

#if DEBUG
let gDebugMode = true
#else
let gDebugMode = false
#endif

@UIApplicationMain


class AppDelegate: UIResponder, UIApplicationDelegate, UIAlertViewDelegate, UISplitViewControllerDelegate, GADInterstitialDelegate {
 
  var window: UIWindow? = nil
	var masterViewController: MasterViewController? = nil
	var detailNavigationController: StackNavigationController? = nil
	var backgroundColorScheme = BackgroundColorSchemeType()
	var iCloudWelcomeViewController: ICloudWelcomeViewController? = nil
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		
    if #available(iOS 11.0, *) {
      assert(false, "*** Please run this app on iOS 10")
    }
    
		print(Date().timeIntervalSinceReferenceDate)
		// 463555660.704646
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyyMMdd"
		let date = formatter.date(from: "20151101")
		print( date?.timeIntervalSinceReferenceDate )
		
    // Use Firebase library to configure APIs
    FirebaseApp.configure()
    
    
		ObjectiveCClass.registerDefaultsFromSettingsBundle()
		
		let ud = UserDefaults.standard
		
		if ud.object(forKey: "Disclaimer") == nil {
			
			let alert = UIAlertView(title: "", message: WLoc("DisclaimerMessage"), delegate: nil, cancelButtonTitle: nil, otherButtonTitles: WLoc("Agree"))
			UserDefaults.standard.set("Agree", forKey: "Disclaimer")
			alert.show()
			
			//install
			FilePackage.shared.installSampleFiles()
			
			ud.set(true, forKey:"iCloudSync")
			ud.set(true, forKey:"iCloudSyncFirstRun")
      ud.set(true, forKey:"UpdatedTOC2ForVersion3")

			ud.synchronize()
			
		}
		else
		{
			if ud.bool(forKey: "iCloudSyncFirstRun") == false {
				
				// First time
				ud.set(true, forKey:"iCloudSyncFirstRun")
				ud.synchronize()
				
				//Execute later
				DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
					
					let controller = ICloudWelcomeViewController(nibName:"ICloudWelcomeViewController" , bundle:nil)
					self.iCloudWelcomeViewController = controller
					controller.modalPresentationStyle = .formSheet
					
					controller.completionHandler = {  (accept:Bool) ->Void in
						
						self.iCloudWelcomeViewController = nil
						
						if accept == true {
							ud.set(true, forKey:"iCloudSync")
							
							self.masterViewController?.reload()
						}
					}
					
					self.window!.rootViewController?.present( controller, animated:true, completion:nil)
				}
			}
      
      // What's New
      
      // MTZWhatsNew.perform(#selector(MTZWhatsNew.setLastAppVersion(_:)), with: "0.0")
      
      if UserDefaults.standard.bool(forKey: "UpdatedTOC2ForVersion3") != true {
        //Execute later
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
          
          
          let controller = BuildingTOC2ViewController()
          controller.modalPresentationStyle = .formSheet
          self.window!.rootViewController?.present( controller, animated:true, completion:nil)
          
          
          /*
           MTZWhatsNew.handle { (whatsNew) -> Void in
           
           if let vc = MTZWhatsNewGridViewController(features: whatsNew) {
           vc.backgroundGradientBottomColor = UIColor(red: 0, green: 67/255, blue: 97/255, alpha: 1)
           vc.backgroundGradientTopColor = UIColor(red: 0, green: 47/255, blue: 67/255, alpha: 1)
           vc.view.tintColor = UIColor(red: 0, green: 67/255, blue: 97/255, alpha: 1)
           vc.dismissHandler = {
           }
           
           self.window!.rootViewController?.present( vc, animated:true, completion:nil)
           }
           }*/
        }
      }
    }
    
    
    window = UIWindow(frame: UIScreen.main.bounds)
		
		// Setup master
		let controller = MasterViewController()
		masterViewController = controller
		let nav = UINavigationController(rootViewController: controller)
		
		// Setup detail
		let dvc = PlaceholderViewController(backgroundColorScheme: backgroundColorScheme)
		
		// Detail Navigation Controller
		if ud.bool(forKey: "AppDelegateWillResignActive") != true {
			// crashed
			// Increase crash count
			// If crash count becomes higher than n, remove identifier info
			
			var count = ud.integer(forKey: "AppDelegateWillResignActiveCrashCount")
			
			count = count + 1
			ud.set(count, forKey: "AppDelegateWillResignActiveCrashCount")
			ud.synchronize()
			
			if count > 3, let currentTab = TabDataSource.shared.current {
				TabDataSourceStorage<StackNavigationStorage>()[currentTab] = nil
				print("removed restored state")
			}
		}
		
		let detailNav = StackNavigationController(rootViewController: dvc, tab: TabDataSource.shared.currentTabIdentifier())
		detailNavigationController = detailNav
		controller.detailNavigationController = detailNav
		
		
		// Setup split
		let svc = MySplitViewController()
		svc.delegate = self
		svc.preferredDisplayMode = .automatic
		
		//
		
		if UIDevice.current.userInterfaceIdiom == .pad {
			svc.viewControllers = [nav, detailNav]
			
		} else {
			svc.viewControllers = [nav]
			
			if detailNav.hasLoadedContents == true {
				svc.showDetailViewController(detailNav, sender: self)
			}
		}
		controller.navigationItem.leftBarButtonItem = svc.displayModeButtonItem
		
		window!.rootViewController = svc
		window!.makeKeyAndVisible()
		
		updateTintColor()
    loadInterstitialAd()

		//
		
		googleAnalystic()
		
		
		// Check iCloud
		
		let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)
		
		queue.async {
			let _ = FileManager.default.url(forUbiquityContainerIdentifier:nil)
		}
		
    
    UIApplication.shared.isIdleTimerDisabled = ud.bool(forKey: "AvoidScreenLock")

		return true
	}
	
	func updateTintColor() {
		
		let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as! String?
		let updatedBackgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
		let changed = ( updatedBackgroundColorScheme != backgroundColorScheme )
				
		backgroundColorScheme = updatedBackgroundColorScheme
		
		if window!.tintColor != backgroundColorScheme.tintColor { window!.tintColor = backgroundColorScheme.tintColor }

		let svc = window!.rootViewController as! UISplitViewController
		svc.setNeedsStatusBarAppearanceUpdate()
		svc.view.backgroundColor = UIColor.lightGray
		let viewControllers = svc.viewControllers
		
		for vc in viewControllers {
			if vc.isKind(of: UINavigationController.self) {
				(vc as! UINavigationController).navigationBar.barStyle = backgroundColorScheme.barStyle
			}
		}
		
		if changed == true {
			NotificationCenter.default.post(name: LawNotification.settingsDidChange, object: self)
		}
	}
	
	
	func googleAnalystic() {
		
		// REMOVE ANALYTICS !!!!
		
		let fm = FileManager.default
		let libURL = fm.urls(for:.libraryDirectory, in: .userDomainMask)[0]
		
		do {
			let contents = try fm.contentsOfDirectory(at: libURL, includingPropertiesForKeys: nil, options: [.skipsHiddenFiles, .skipsPackageDescendants, .skipsSubdirectoryDescendants] )
			
			for url in contents {
				if url.lastPathComponent.hasPrefix("googleanalytics") == true {
					// DELETE IT
					try fm.removeItem(at: url)
				}
			}
			
		}catch {
		}
		
		/*
		// Optional: automatically send uncaught exceptions to Google Analytics.
		GAI.sharedInstance().trackUncaughtExceptions = true;

		// Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
		GAI.sharedInstance().dispatchInterval = 20;

		// Optional: set Logger to VERBOSE for debug information.
		//	GAI.sharedInstance().logger().logLevel = .kGAILogLevelVerbose

		// Initialize tracker. Replace with your tracking ID.

		GAI.sharedInstance().trackerWithTrackingId("UA-49877766-12")
		GAI.sharedInstance().defaultTracker.allowIDFACollection = true
		ACTConversionReporter.reportWithConversionID("979559990", label: "KNlzCLPIxlwQtsyL0wM", value: "0.00", isRepeatable: false)
*/
	}
	

	func handleFile(_ url:URL) {
		let fm = FileManager.default
		let data = try? Data(contentsOf: url)
		
		if data == nil {
			do {
				try  fm.removeItem(at: url)
			} catch {
			}
			return
		}
		
		let plist:NSDictionary

		do {
			plist = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSDictionary
		} catch {
			do {
				try fm.removeItem(at: url)
			} catch {
			}
			return
		}
		
		do {
			try fm.removeItem(at: url)
		} catch {
		}
		
		let fp = FilePackage.shared
		let alertController = UIAlertController(title: WLoc("Importing"), message: WLoc("Importing Message"), preferredStyle: .alert)
		let action1 = UIAlertAction(title: WLoc("Replace"), style: .destructive) { (action) -> Void in
			
			// Backup first
			fp.keepBackupForFileAt()
			fp.documents.setDictionary( plist as Dictionary )
			fp.saveIndex( saveToICloud: true )
			self.masterViewController?.reload()
		}
		
		let action2 = UIAlertAction(title: WLoc("Append"), style: .default) { (action) -> Void in
			
			let array = plist.object(forKey: "items") as! [Any]
      fp.documents.items?.addObjects(from: array as [Any])
			fp.saveIndex( saveToICloud: true )
			self.masterViewController?.reload()
		}
		
		let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in }
		
		alertController.addAction(action1)
		alertController.addAction(action2)
		alertController.addAction(cancel)
		
		let svc = window!.rootViewController as! UISplitViewController
		svc.present(alertController, animated: true, completion: nil)
	}
	
  func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
		
		if url.pathExtension == "comcatalystwolawsindex" {
			
			self.handleFile(url)
			return true
		}
		
		//comcatalystwolaws
		
		let scheme = url.scheme
		let specifier = (url as NSURL).resourceSpecifier as NSString?
		
		if scheme != "comcatalystwolaws" || specifier == nil { return false }
		
		if specifier == nil  { return false }
		if (specifier! as NSString).length == 0 { return false }
		
		let comps = specifier!.components(separatedBy: "@")
		let title = comps[0] as NSString
		let packageName = comps.last as NSString!
		let bodyRange = NSMakeRange(title.length + 1, specifier!.length - title.length - (packageName?.length)! - 2)
		let anchorString = specifier!.substring( with: bodyRange )
		
		print("opening \(title) = \(anchorString) in file \(packageName)")
		
		// Opening Documents
		
		func openDocumentAt(_ packageURL:URL) {
			
      masterViewController?.openWithDetailItem(url: packageURL, anchor: anchorString, startJump: true)
			hideMaster()
		}
		
		
		
    let packageURL = FilePackage.shared.packageURL(from: packageName as! String)
		
		let fileExists = FileManager.default.fileExists(atPath: packageURL.path)
		
		if fileExists == false {
			
			// Download
			let svc = window!.rootViewController as! UISplitViewController
			
			let controller = WaitingViewController( nibName: "WaitingViewController", bundle:nil)
			controller.modalPresentationStyle = UIModalPresentationStyle.popover
			let popover = controller.popoverPresentationController
			popover?.delegate = controller
			popover?.permittedArrowDirections = [.any]
			popover?.sourceView = svc.view
			popover?.sourceRect = CGRect(x: svc.view.bounds.midX,y: 20,width: 1,height: 1)
			
			svc.present(controller, animated: true, completion: nil)
			
			let string = packageName?.deletingPathExtension // "S38HO133"
			let mdict = DocItem( queryWithPackageName:string!, edition:nil)
			
			CloudKitDomain.shared.downloadLaw(mdict, progress: {  (progress) -> Void in
				controller.progress = progress }) { (success: Bool, error: Error?) -> Void in
					
					controller.dismiss(animated: true) {
						if success == true { openDocumentAt(packageURL) }
						displayErrorAlertIfNecessary(error, in: svc)
					}
			}
			
			return false
		}
		
		openDocumentAt(packageURL)
		return true
	}
	
	func hideMaster() {
		
		let svc = window!.rootViewController as! UISplitViewController
		let array = svc.viewControllers
		
		if array.count == 1 {
			if let nav = array[0] as? UINavigationController {
				if nav.viewControllers.count == 1 {
					svc.showDetailViewController(detailNavigationController!, sender: self)
				}
			}
		}
	}
	
	//MARK:- Others
	
	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
		
		let ud = UserDefaults.standard
		
		ud.set(true, forKey: "AppDelegateWillResignActive")
		ud.set(0, forKey: "AppDelegateWillResignActiveCrashCount")
		ud.synchronize()
	}
	
	
	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
		
		let shortcutsToDocuments = false /// FALSE!!!
		
		if #available(iOS 9.0, *) {
			
			var shortcutItems:[UIApplicationShortcutItem] = []
      
      if shortcutsToDocuments == false {
        
        TabDataSource.shared.all.forEach {
          let stackViewModel = StackNavigationViewModel()
          stackViewModel.tab = $0
          let titleString = stackViewModel.pathString(omitRoot: false)
          let item = UIApplicationShortcutItem(type: "Tab", localizedTitle: titleString, localizedSubtitle:nil, icon:nil, userInfo:["identifier":$0] )
          shortcutItems.append(item)
          
        }
      }
			
			UIApplication.shared.shortcutItems = shortcutItems
			
		} else {
			// Fallback on earlier versions
		}
	}
	
  var interstitial: GADInterstitial!

	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
		
		CloudKitDomain.shared.updateIfHasPrivateAccount()
		updateTintColor()
    
    loadInterstitialAd()
	}
  
  func loadInterstitialAd() {
    if UserDefaults.standard.object(forKey: "AdType") as? String == "Interstitial" {
      interstitial = GADInterstitial(adUnitID: "ca-app-pub-5527180066718750/8654125823")
      interstitial.delegate = self
      let request = GADRequest()
      // Request test ads on devices you specify. Your test device ID is printed to the console when
      // an ad request is made.
      request.testDevices = [ kGADSimulatorID ]
      interstitial.load(request)
    }
  }
  
  func interstitialDidReceiveAd(_ ad: GADInterstitial) {
    if let root = window?.rootViewController {
    interstitial.present(fromRootViewController: root)
    }
  }
	
	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
		
		UserDefaults.standard.removeObject(forKey: "AppDelegateWillResignActive")
		UserDefaults.standard.synchronize()
	}
	
	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	}
	
	@available(iOS 9.0, *)
  func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
			
		if let identifier = shortcutItem.userInfo?["identifier"] as? String {
			
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
			let _ = TabDataSource.shared.selectTabIdentifier(identifier, jumpTo: nil)
			self.hideMaster()
      }
			completionHandler(true)
      
			return
		}
		
		completionHandler(false)
	}
	
	// MARK:-
	
	
	//	func splitViewController(svc: UISplitViewController,
	//		willChangeToDisplayMode displayMode: UISplitViewControllerDisplayMode) {
	//
	//			if displayMode == .PrimaryHidden {
	//
	//
	//				let item = UIBarButtonItem(title: "Laws", style: .Plain, target: self, action: Selector("showMaster") )
	//				let controller: AnyObject? = self.detailNavigationController?.viewControllers[0]
	//				controller?.navigationItem.leftBarButtonItem = item
	//
	//			}else {
	//
	//				let controller: AnyObject? = self.detailNavigationController?.viewControllers[0]
	//				controller?.navigationItem.leftBarButtonItem = nil
	//
	//			}
	//
	//	}
	//
	//	func showMaster(){
	//		let svc = window.rootViewController as UISplitViewController
	//
	//
	//		UIView.animateWithDuration(0.3, animations: {
	//
	//			svc.preferredDisplayMode = .PrimaryOverlay
	//
	//		})
	//	}
	
	
}

class MySplitViewController: UISplitViewController {
	
	static func viewControllerWithRestorationIdentifierPath( _ identifierComponents: [Any],
		coder: NSCoder) -> UIViewController? {
			
		let obj = MySplitViewController()
		return obj
	}
	
	override var preferredStatusBarStyle:UIStatusBarStyle {
		
		let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
		let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
		
		if backgroundColorScheme == .Dark { return .lightContent }
		if backgroundColorScheme == .Grey { return .lightContent }

		return .default
	}
	
	override var supportedInterfaceOrientations:UIInterfaceOrientationMask {
		return  [UIInterfaceOrientationMask.all]
	}
	
	override func viewWillTransition(to size: CGSize,
		with coordinator: UIViewControllerTransitionCoordinator) {
			
		super.viewWillTransition(to: size, with:coordinator)
		NotificationCenter.default.post(name: LawNotification.splitViewOrientationDidChange, object: self)
	}
	
	override func viewDidLoad() {
		self.view.backgroundColor = UIColor.white
	}

}



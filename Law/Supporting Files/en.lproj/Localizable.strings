/* 
  Localizable.strings
  LawInSwift

  Created by Masatoshi Nishikata on 2014/08/28.
  Copyright (c) 2014年 Catalystwo. All rights reserved.
*/

"Cancel" = "Cancel";
"Find" = "Find";
"Jump" = "Jump";
"JumpMessage" = "e.g.）第１００条 → 100\n第１００条の２ → 100の2";

"Add" = "Add";
"AddMessage" = "Do you want to download the page from the URL you copied?";
"OK" = "OK";

"Not Found" = "Not Found";
"Found %d" = "%d matches";
"Found %@" = "Found “%@”";
"Prev" = "⬆︎Prev ";
"Next" = " Next⬇︎";
"Reference" = "Reference";
"Laws" = "Laws";


"DisclaimerMessage" = "This application uses the information supplied by the e-Gov website by the Ministry of Internal Affairs and Communications of Japan. Please read the notice information (law.e-gov.go.jp) supplied by the Ministry of Internal Affairs and Communications of Japan before the usage of the application.  The application changes the layout of the original data in order to be able to browse conveniently.  It may differ from the actual laws.  Please check actual laws for actual practising of laws.  We disclaim any reliabilities caused by this application. The documents installed automatically for the first launch are sample only.  Please read the notice information in e-Gov(law.e-gov.go.jp) website and before downloading actual data from e-Gov website.";
"Agree" = "Agree";

"Delete Item..." = "Delete Item…";
"TOC" = "TOC";
"TabBookmark" = "Bookmarks";
"Bookmark" = "🔖Bookmark";
"Copy to Clipboard" = "📎Copy URL";

"Create Folder" = "Create";
"New Folder" = "New Folder";
"New Folder Message" = "You can drag & drop an item to a folder in Edit mode.";
"Rename" = "Rename";

"Thank You" = "Thank You for using this app.  Did you like it?";
"Like" = "Like";
"Dislike" = "Dislike";

"HelpURL" = "http://www.catalystwo.com/LawBrowser_en.html";

"RetrieveMessage" = "To update to the latest information, tap Reload button. Please read the information supplied in e-Gov website by the Ministry of Internal Affairs and Communications of Japan. ";

"RetrieveMessageUpToDate" = "This law is up-to-date. To reload the information, tap Reload button. Please read the information supplied in e-Gov website by the Ministry of Internal Affairs and Communications of Japan. ";


"RetrieveMessageUser" = "To update, tap Update button.";

"RetrieveMessageUpToDateUser" = "This law, updated by a user, is up-to-date. When the user modified it, the app will notify you.  To reload immediately, tap Update button.";

"Retrieve Title" = "There is an Update";
"Retrieve Up To Date" = "Up To Date";

"Retrieve" = "Agree & Reload";
"Is this law in local? %@ %@ %@" = "Is this law %@ %@ stored locally? If so you can link to the file. Or, you can read articles online.";
"Link" = "Link";
"Read Online" = "Download";

"Choose document to link" = "Choose document to link";
"Linked to " = "Linked ";
"Tap to Unlink" = "Tap to Unlink";
"LinkToFileTableViewControllerFooter" = "When tapping an external link, one of these internal documents will be displayed instead of a remote web site.";

"More..." = "More…";
"Update Law" = "Update";
"Link..." = "Link…";
"Delete" = "Delete";

"Download From Cloud" = "Download From Cloud";
"First Letter" = "Begins with";
"Keyword Search" = "Keyword";
"Search" = "Search";

"Jump" = "Jump";


"LawCloudKitFetchStateNoResults" = "Not found on Cloud.  Please download from e-Gov website.";

"LawCloudKitFetchStateFound" = "The laws listed here are cached on Cloud. If not found on Cloud, you may find it on e-Gov website.";

"LawCloudKitFetchStateStart" = "Look for laws you want to download from Cloud";

"Settings" = "Settings";

"Start iCloud Sync" = "Start Using";
"iCloud First Message" = "Thank you for updating! This version supports iCloud sync.  If you do not want to enable iCloud sync, open Settings app > iCloud > iCloud Drive , then turn off the switch for this app before tapping “Start Using”.";

"See Ad" = "[Tap here to view ad]";
"Read Review" = "Read Reviews";


"Review App" = "If you liked this application, please give it stars! That helps a lot!";
"Review App Button" = "Review";


"Not Found" = "Not Found";
"FoundStr" = "Found";
"FoundStrSuffix" = "";

"FoundStrOverflow" = "Found more than";
"FoundStrSuffixOverflow" = "";


"ConflictMessage" = "The changes made from the multiple deviecs of yours seem to be conflicted on iCloud.  Please choose one version to use." ;

"KushizashiSearch" = "Cross-Search";
"KushizashiPlaceholder" = "Search from added laws";

"Notes" = "✏️Notes";
"Delete Notes" = "Deleting Notes";
"Are you sure you want to delete this notes?" = "Are you sure you want to delete this notes?";

"Hanrei" = "courts.go.jp";
"Hanrei2" = "thoz.org";

"Highlight" = "Highlight";
"Highlights" = "Highlights";

"Unhighlight" = "Unhighlight";

"Sorry" = "Sorry";
"Highlight is not allowed for this selection." = "Highlight is not allowed for this selection.  Selection may not span multiple lines or overlap another selection.";
"Unhighlight Message" = "Do you want to unhighlight this? The notes will also be removed.";

"Clear History" = "Clear History";

"History" = "History";


"Jump to article" = "Jump";

"Send Data" = "Send Data";

"Importing" = "Importing";
"Importing Message" = "You are about to open the index file.  If you choose Replace, it will replace all the data in this app (The existing data will be removed, then replaced by the importing index).  If you choose Append, the importing index will be added to the existing index.";


"Replace" = "Replace";
"Append" = "Append";

"Delete History" = "Delete History";
"Delete History Message" = "Are you sure you want to delete the history?";

"Downloading" = "Downloading";

"ShowUnique" = "All/Unique";
"ShowUniqueDialogue" = "";
"ShowUniqueDialogueMessage" = "Show all or show only the latest ones for each law.";

"Show All" = "Show All";
"Show Unique" = "Show Latest Only";

"Close" = "Close";

"MyPackageBundleHistory" = "Already Published: ";

"My Package Bundles" = "Upload Law";

"EditorPreviewControllerMessage" = "Tap “Upload” button to finish.";

"Upload" = "Upload";
"Edit" = "Edit";
"Done" = "Done";
"Preview" = "Preview";
"Upload New Law" = "Tap here to start";

"CloudKitMyPackageBundleTableViewControllerMessage" = "You can upload a law document that are not availabe on eGov website.  The law you upload will be shared with other users to use.";

"UploadFinalMessage" = "Tap Upload button to submit to Cloud server.  Other people will be able to download it.  Please do not upload any material that violates other people's copyright.  You can remove the uploaded document anytime later but you can not remove documents downloaded on other user's device.";

"Modify" = "Modify";

"CloudKitPackageBundleViewControllerMessage" = "To update this law, tap ”Modify” button.  To add a new version of this law as a new document, go back and tap ”Upload New Law…” button ";

"Updating Download Count" = "Loading Download Count…";
"Download:" = "Download:";

"Modification Date:" = "Modification Date:";


"Delete Record?" = "Delete Record?";
"Delete Record Message" = "Are you sure you want to delete this law from the cloud server?";

"Delete this from server" = "Delete from Cloud";
"Paste" = "Paste";


"EditorBodyViewControllerSwitchMessage" = "『第１条　見出し』のようになっている場合はこのスイッチをオンにします。「第１条」と同じ行にすぐ本文が開始している場合はオフにします。";

"EditorBodyMenuPopoverToggleSwitch" = "条文番号の行は見出し行";

"Menu" = "Menu";

"Clear" = "Claar";


"Download from Website" = "Download";

"Set Law contents" = "Set Law contents";

"Download Website" = "Download Website";
"Download Website Message" = "Download website contents and paste it as text. Set URL and click Download button.";


"Has users item" = "\n👤…represents document that have been uploaded by a user.";
"This is users item" = "👤…this has been uploaded by a user. It's not a law from eGov website.";
"Agree" = "Agree";

"Uploaded" = "Uploaded";

"Failed to upload" = "Failed to upload";
"Could not continue" = "Could not continue";




"Download" = "Download";
"Undo" = "↩︎";

"Upload Center" = "Upload Center";
"Download from eGov" = "Download from eGov";

"iCloudAuthorizationMessage" = "To upload to Cloud Server, you need to have an iCloud account.";

"Download?" = "Download?";
"Download? message" = "Do you want to download this law from eGov website? This will be added to your list.";

"Create Law" = "Create Law";

"Clear Message" = "Are you sure you want to clear the text?";

"No Toc" = "No TOC";
"No Bookmark" = "No Bookmark";
"No Highlight" = "No Highlight";

"eGovWebsite" = "e-Gov Website";

"Backup Files" = "Backup Files";
"Recover?" = "Recover?";
"RecoverMessage" = "Do you want to recover this file? This will replace existing data.";
"Recover" = "Recover";
"Backup" = "Backup";



"Plus Button Tutorial" = "Tap “+” button to add from more than 8,000 laws.";

"BackupFooter" =  "Daily backups of the last 10 days are automatically stored on your device.  The backups will automatically be removed in 10 days.";

"LawTitle" = "Title";
"LawDescription" = "Description";
"LawEditor" = "Editor";
"LawContents" = "Contents";
"Not Set" = "Not Set";
"LawTitleFooter" = "Set title etc";
"LawBodyFooter" = "Set body of the law.\nTap Preview to proceed.";
"Back" = "Back";

"Copy" = "Copy";
"Define" = "Define";
"Share" = "Share…";
"Add Shortcut" = "Add…";

"Custom Dictionary Message" = "[New Feature]";

"Custom Dictionary Message Body" = "Now you can add your favourite websites or apps in the menu.  Set it up in Settings View of this app.";

"All Articles" = "ALL";
"Dereferences" = "Referers";
"Dereferences Footer" = "総務省の方がつけたリンクを遡ることにより取得された逆参照条文を表示しています。※すべての条文にリンクがはられているわけではありません。例えば、『法２５０条から２６０条の規定は、本条にも適用される。』という条文では、２５１〜２５９条のリンクは貼られません。また、『５条１項』という条文の『５条』の部分にのみリンクが貼られている場合があります。総務省の方のリンクの張り忘れ、リンク先の間違い等もあります。";
"Looking Up" = "Looking Up";
"No dereferences" = "No articles";
"Aohon" = "工業所有権法逐条解説";
"AohonMessage" = "『工業所有権法逐条解説』専用アプリをストアにアップロードしました。";
"AohonGo" = "見てみる";

// Error
"ERR:OfflineError" = "Requires Internet Connection";
"ERR:Account Missing Error" = "Sorry, you need an Apple ID";
"ERR:Unknown Error" = "Sorry, an error occurred";
"ERR:File Access Error" = "Sorry, unable to access the file";
"ERR:Failed to upload" = "Sorry, failed to upload";
"ERR:UserDisabledCloudKit" = "To upload to Cloud Server, you need to have an iCloud account or enable iCloud Drive.";
"ERR:TimeOut Error" = "Time Out (connection is not stable or Apple Server is not responding.";
"ERR:NotFound Error" = "Not Found.  It seems to have been deleted from the cloud server.";
"ERR:iCloudOverCellularNotAllowed Error" = "Accessing iCloud over cellular network is not allowed. To change this, go to Settings app ▸ iCloud ▸ iCloud Drive ▸ Turn “Use Celluar Data” ON.";

"Start Using" = "Start Using";
"<BLANK>" = "BLANK";
"Tab Dragging Message" = "drag out to close tab";
"rebuild index" = "Rebuild Index";

"BookmarkGuideTitle" = "Added";
"BookmarkGuideMessage" = "The bookmark has been added to your Law list";
"Not Show Again" = "OK. Do Not Show This Message Again";


//
//  OrigamiTransformLayer.m
//  TableTest
//
//  Created by Nishikata Masatoshi on 2/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OrigamiTransformLayer.h"
#import <UIKit/UIKit.h>

@implementation  OrigamiTransformLayer
@synthesize index,  startAngle, endAngle, shadowLayer, imageLayer, subImageLayer;


#define ACTIONS [[NSDictionary alloc] initWithObjectsAndKeys:\
[NSNull null], @"onOrderIn",\
[NSNull null], @"onOrderOut",\
[NSNull null], @"hidden",\
[NSNull null], @"sublayers",\
[NSNull null], @"contents",\
[NSNull null], @"bounds",\
[NSNull null], @"frame",\
[NSNull null], @"opacity",\
[NSNull null], @"position",\
[NSNull null], @"transform",\
nil]


+ (OrigamiTransformLayer *)transformLayerFromImage:(UIImage *)image subImage:(UIImage*)subImage frame:(CGRect)frame anchorPiont:(CGPoint)anchorPoint startAngle:(CGFloat)start endAngle:(CGFloat)end darker:(BOOL)darker
{
	
	OrigamiTransformLayer *jointLayer = [OrigamiTransformLayer layer];
	jointLayer.anchorPoint = anchorPoint;
	jointLayer.startAngle = start;
	jointLayer.endAngle = end;
	jointLayer.actions = ACTIONS;

	CGFloat layerHeight;
	
	layerHeight = image.size.height - frame.origin.y;
	jointLayer.frame = CGRectMake(0, 0,  frame.size.width, layerHeight );
	if (frame.origin.y) {
		jointLayer.position = CGPointMake(frame.size.width/2, frame.size.height);
	}
	else {
		jointLayer.position = CGPointMake(frame.size.width/2, 0);
	}
	
	
	CGFloat scale = [[UIScreen mainScreen] scale];
	
	//map image onto transform layer
	CALayer *imageLayer = [CALayer layer];
	imageLayer.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
	imageLayer.anchorPoint = anchorPoint;
	imageLayer.edgeAntialiasingMask = 0;
	imageLayer.opaque = YES;
	imageLayer.position = CGPointMake( frame.size.width/2, layerHeight*anchorPoint.y);
	imageLayer.actions = ACTIONS;
	imageLayer.contentsScale = [[UIScreen mainScreen] scale];
	jointLayer.imageLayer = imageLayer;
	[jointLayer addSublayer:imageLayer];
	
	
	if( CGRectGetMaxY(frame) > image.size.height || !image )
	{
		imageLayer.backgroundColor = [UIColor whiteColor].CGColor;
		
	}else {
		
		CGRect imageRect = frame;
		
		imageRect.origin.y *= scale;
		imageRect.size.width *= scale;
		imageRect.size.height *= scale;
		
		
		CGImageRef imageCrop = CGImageCreateWithImageInRect(image.CGImage, imageRect);
		imageLayer.contents = (__bridge id)imageCrop;
		imageLayer.backgroundColor = [UIColor clearColor].CGColor;
		CFRelease(imageCrop);
		
	}
	

	//map image onto transform layer
	if( subImage )
	{
		CALayer *subImageLayer = [CALayer layer];
		subImageLayer.hidden = YES;
		subImageLayer.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
		subImageLayer.anchorPoint = anchorPoint;
		subImageLayer.edgeAntialiasingMask = 0;
		subImageLayer.opaque = YES;
		subImageLayer.position = CGPointMake( frame.size.width/2, layerHeight*anchorPoint.y);
		subImageLayer.actions = ACTIONS;
		subImageLayer.contentsScale = [[UIScreen mainScreen] scale];
		jointLayer.subImageLayer = subImageLayer;
		[jointLayer addSublayer:subImageLayer];
		
		
		if( CGRectGetMaxY(frame) > subImage.size.height )
		{
			subImageLayer.backgroundColor = [UIColor whiteColor].CGColor;
			
		}else {
			
			CGRect imageRect = frame;
			
			imageRect.origin.y *= scale;
			imageRect.size.width *= scale;
			imageRect.size.height *= scale;

			CGImageRef imageCrop = CGImageCreateWithImageInRect(subImage.CGImage, imageRect);
			subImageLayer.contents = (__bridge id)imageCrop;
			subImageLayer.backgroundColor = [UIColor clearColor].CGColor;
			CFRelease(imageCrop);
			
		}
	}
	
	
	//add shadow
	NSInteger index = frame.origin.y/frame.size.height;
	CALayer *shadowLayer = [CALayer layer];
	shadowLayer.frame = imageLayer.bounds;
	shadowLayer.opacity = 0.0;
	shadowLayer.edgeAntialiasingMask = 0;
	shadowLayer.backgroundColor = [UIColor clearColor].CGColor;
	shadowLayer.actions = ACTIONS;
	
	if (index%2) {
		shadowLayer.contents = (__bridge id)[UIImage imageNamed:@"OrigamiShadowBottom.png"].CGImage;
	}
	else {
		
		if( darker )
			shadowLayer.contents = (__bridge id)[UIImage imageNamed:@"OrigamiShadowTopDarker.png"].CGImage;

			else
		shadowLayer.contents = (__bridge id)[UIImage imageNamed:@"OrigamiShadowTop.png"].CGImage;
	}
	
	jointLayer.shadowLayer = shadowLayer;
	jointLayer.index = index;
	

	[jointLayer addSublayer:shadowLayer];
	

	return jointLayer;
}

-(void)setProgress:(CGFloat)progress
{
	CATransform3D t = CATransform3DIdentity;
	t = CATransform3DMakeRotation( endAngle * progress + startAngle * (1-progress), 1, 0, 0);
	
	self.transform = t;
	
	
	CGFloat shadowAniOpacity;

	if (self.index%2) {
		shadowAniOpacity = 0.8;
	}
	else {
		shadowAniOpacity = 1.0;
	}
	self.shadowLayer.opacity = shadowAniOpacity*progress ;
	
	
	[self removeAllAnimations];

}

-(void)dealloc
{

	[shadowLayer removeFromSuperlayer];
	shadowLayer = nil;
	
	[imageLayer removeFromSuperlayer];
	imageLayer = nil;
	
	[subImageLayer removeFromSuperlayer];
	subImageLayer = nil;
}

@end

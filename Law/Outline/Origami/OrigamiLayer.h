//
//  OrigamiLayer.h
//  TableTest
//
//  Created by Nishikata Masatoshi on 2/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface OrigamiLayer : CALayer
{
	NSMutableArray* transformLayers_;
	
	CGFloat originalHeight_;
	CALayer* headerLayer_;
	CALayer* footerLayer_;
	CGFloat progress_;
	UIImage* image_;
	UIImage* subImage_;

}

- (id)init;
-(void)dealloc;
-(void)setSubImageHidden:(BOOL)flag;
- (void)setNumberOfFolds:(NSUInteger)folds;
-(void)setHeight:(CGFloat)height;
-(void)setHeight:(CGFloat)height andOriginY:(CGFloat)originY;
-(CGFloat)height;
-(void)setProgress:(CGFloat)newProgress;

@property (nonatomic, readwrite) NSUInteger numberOfFolds;

@property (nonatomic, retain) CALayer* headerLayer;
@property (nonatomic, retain) CALayer* footerLayer;

@property (nonatomic, readwrite) CGFloat progress;
@property (nonatomic, readonly) CGFloat originalHeight;
@property (nonatomic, readwrite) CGFloat height;

@property (nonatomic, retain) UIImage* image;
@property (nonatomic, retain) UIImage* subImage;

@end
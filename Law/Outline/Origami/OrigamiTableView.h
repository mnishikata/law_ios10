//
//  OrigamiTableView.h
//  TableTest
//
//  Created by Nishikata Masatoshi on 3/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrigamiLayer.h"
#import "OrigamiTransformLayer.h"
#import "OrigamiAnimation.h"
#import "OrigamiPinchGestureRecognizer.h"


@class OrigamiTableView, OrigamiPinchGestureRecognizer;

@protocol OrigamiTableViewDelegate <NSObject>

// Static
- (NSInteger)origamiTableView:(OrigamiTableView *)tableView numberOfRowsInSection:(NSInteger)section;
- (UITableViewCell *)origamiTableView:(OrigamiTableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)origamiTableView:(OrigamiTableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;


// Transition
- (UITableViewCell*)origamiTableView:(OrigamiTableView *)tableView canInsertingRowAtIndexPath:(NSIndexPath *)indexPath;

- (BOOL)origamiTableView:(OrigamiTableView *)tableView canCollapseCellsInRange:(NSRange)range;

- (UITableViewCell*)origamiTableView:(OrigamiTableView *)tableView collapsingCellForRowAtIndexPath:(NSIndexPath *)indexPath inRange:(NSRange)range;


@optional

- (void)origamiTableView:(OrigamiTableView *)tableView didCollapseCellsInRange:(NSRange)range;
- (void)origamiTableView:(OrigamiTableView *)tableView willInsertRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)origamiTableView:(OrigamiTableView *)tableView didInsertRowAtIndexPath:(NSIndexPath *)indexPath;


- (BOOL) origamiTableView: (OrigamiTableView*) tableView canMoveRowAtIndexPath: (NSIndexPath*) indexPath;

- (UITableViewCellEditingStyle)origamiTableView:(OrigamiTableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)origamiTableView:(OrigamiTableView*) tableView commitDeletingRowAtIndexPath:(NSIndexPath *)indexPath;

- (void)origamiTableView:(OrigamiTableView *)tableView indentAtIndexPath:(NSIndexPath *)indexPath withAllSiblings:(BOOL)withSiblings;
- (void)origamiTableView:(OrigamiTableView *)tableView unindentAtIndexPath:(NSIndexPath *)indexPath withAllSiblings:(BOOL)withSiblings;


-(void)origamiTableViewWillBeginDragging:(OrigamiTableView *)tableView;

- (CGFloat)origamiTableView:(OrigamiTableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;

- (NSString *)origamiTableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath;
@end



@interface UITableViewCell (LayerDrawing)
-(void)mn_drawContentsInContext:(CGContextRef)context;
+(UIImage*)mn_cellSnapshot:(NSArray*)cells maxHeight: (CGFloat)maxHeight alignBottom:(BOOL)alignBottom backgroundColor:(UIColor*)backgroundColor;

@end




@interface OrigamiTableView : UITableView <UITableViewDelegate, UITableViewDataSource, OrigamiPanGestureRecognizerDelegate, UIGestureRecognizerDelegate>
{
	OrigamiLayer *origamiLayer_;
	
	__block OrigamiAnimation  * animation_;
	
	NSIndexPath* pinchingPadIndexPath_; // ダミーセルの行
	
	
	OrigamiPinchGestureRecognizer* pinchGesture_;
	OrigamiPanGestureRecognizer* panGesture_;
	UISwipeGestureRecognizer* indentGesture_;
	UISwipeGestureRecognizer* unindentGesture_;

	
	BOOL transitioning_;
	
	BOOL insertMode_;
	CGFloat pinchingPadHeight_;
	NSRange rowsBetweenFingers_;
	
	
	BOOL isPanning_;
	BOOL panningOnTop_;
	BOOL panningOnBottom_;
	BOOL panningOnBottomOutside_;

	id <OrigamiTableViewDelegate> __weak origamiDelegate_;
}
- (id)initWithFrame:(CGRect)frame ;
- (id)initWithCoder:(NSCoder *)coder ;
-(void)setup;
-(void)dealloc;
-(id)origamiDelegate;
-(void)setOrigamiDelegate:(id<OrigamiTableViewDelegate>)origamiDelegate;
-(BOOL)setupAndAddOrigamiLayerWithCells:(NSArray*)cellArray subCells:(NSArray*)subCells expand:(BOOL)expand toY: (CGFloat)pointY ;
//-(void)origamiTransitionWithCells:(NSArray*)cellArray expand:(BOOL)expand toY: (CGFloat)pointY completion:(void (^)(BOOL finished))completion;
-(void)setSubImageHidden:(BOOL)flag;
-(void)adjustContentOffset:(BOOL)animated;
-(void)closeOrigamiLayer;
-(CALayer*)layerForCells:(NSArray*)cells alignBottom:(BOOL)alignBottom;
-(void)reloadAndCloseVisibleCells:(CGFloat)animationDuration alignBottom:(BOOL)alignBottom withCompletion:(void (^)(BOOL finished))completion;
-(void)reloadAndExpandFrom:(NSIndexPath*)topIndexPath length:(NSUInteger)length visibleCells:(NSArray*)visibleCells scrollToVisible:(BOOL)scrollToVisible withCompletion:(void (^)(BOOL finished))completion ; 
-(CGFloat)origamiExpandedHeight;
-(CGFloat)origamiCurrentHeight;
-(void)setOrigamiHeight:(CGFloat)height andOriginY:(CGFloat)originY;
-(void)pinchGesture:(OrigamiPinchGestureRecognizer*)gesture;
- (BOOL)origamiCanPanForTouch:(UITouch *)touch;
-(void)setEnableSystemGestureRecognizers:(BOOL)flag;
-(void)origamiCancelsPanning;
- (void)panGesture:(OrigamiPanGestureRecognizer *)gesture ;
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
- (BOOL)tableView: (UITableView*) tableView canMoveRowAtIndexPath: (NSIndexPath*) indexPath;
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath;

-(void)collapseCells:(NSArray*)cellArray toY: (CGFloat)pointY completion:(void (^)(BOOL finished))completion;
-(void)expandCells:(NSArray*)cellArray from: (NSIndexPath*)fromIndexPath completion:(void (^)(BOOL finished))completion;
-(void)collapseCells:(NSArray*)cellArray toView: (UIView*)floatingView completion:(void (^)(BOOL finished))completion;


@property (nonatomic, retain) OrigamiLayer *origamiLayer;
@property (nonatomic, readonly) CGFloat origamiExpandedHeight;
@property (nonatomic, readonly) CGFloat origamiCurrentHeight;
@property (nonatomic, weak) id <OrigamiTableViewDelegate>  origamiDelegate;

@property (nonatomic, readonly) OrigamiPinchGestureRecognizer* origamiPinchGesture;
@property (nonatomic, readonly) OrigamiPanGestureRecognizer* origamiPanGesture;

@property (nonatomic, readwrite) BOOL transitioning;
@property (nonatomic, readwrite) BOOL disablePan;

@end



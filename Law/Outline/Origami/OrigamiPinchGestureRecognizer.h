//
//  MNPinchGestureRecognizer.h
//  TableTest
//
//  Created by Nishikata Masatoshi on 1/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIGestureRecognizerSubclass.h>
#import <UIKit/UIGestureRecognizer.h>

@interface OrigamiPinchGestureRecognizer : UIGestureRecognizer
{
	
	CGPoint initialContentOffset_;
	CGPoint initialLocationInViewA_;
	CGPoint initialLocationInViewB_;
	CGPoint locationInViewA_;
	CGPoint locationInViewB_;

	CGFloat initialDistance_;
	
	CGFloat currentDistance_;
	CGFloat lastDistance_;

	CGPoint lastLocationInViewA_;
	CGPoint lastLocationInViewB_;

}

-(CGFloat)distance;

@property (nonatomic, readonly) CGPoint initialContentOffset;

@property (nonatomic, readonly) CGFloat distance;
@property (nonatomic, readonly) CGFloat initialDistance;

@property (nonatomic, readonly) CGPoint initialTopLocation;
@property (nonatomic, readonly) CGPoint initialBottomLocation;

@property (nonatomic, readonly) CGPoint topLocation;
@property (nonatomic, readonly) CGPoint bottomLocation;

@property (nonatomic, readonly) CGFloat pinchVelocity;

@end


@interface OrigamiPanGestureRecognizer : UIGestureRecognizer
{
	
	CGPoint initialContentOffset_;
	CGPoint initialLocationInView_;
	CGPoint locationInView_;
	
//	CGFloat initialDistance_;
	
	CGFloat currentDistance_;
	CGFloat lastDistance_;
	
	CGPoint lastLocationInView_;
	
}

-(CGFloat)distance;

@property (nonatomic, readonly) CGPoint initialContentOffset;

@property (nonatomic, readonly) CGFloat distance;
//@property (nonatomic, readonly) CGFloat initialDistance;

@property (nonatomic, readonly) CGPoint initialLocation;
//@property (nonatomic, readonly) CGPoint initialBottomLocation;

@property (nonatomic, readonly) CGPoint location;
//@property (nonatomic, readonly) CGPoint bottomLocation;

@property (nonatomic, readonly) CGFloat velocity;

@end


@protocol OrigamiPanGestureRecognizerDelegate <NSObject>

-(BOOL)origamiCanPanForTouch:(UITouch*)touch;
-(void)origamiCancelsPanning;

@end
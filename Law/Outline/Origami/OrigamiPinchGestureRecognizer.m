//
//  MNPinchGestureRecognizer.m
//  TableTest
//
//  Created by Nishikata Masatoshi on 1/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OrigamiPinchGestureRecognizer.h"

@implementation OrigamiPinchGestureRecognizer
@synthesize distance = currentDistance_;
@synthesize initialDistance = initialDistance_;
@synthesize initialContentOffset = initialContentOffset_;

- (id)initWithTarget:(UITableView*)target action:(SEL)action; {
	self = [super initWithTarget:(id)target action:(SEL)action];
	if (self) {

		
	}
	return self;
}

-(void)setState: (UIGestureRecognizerState)aState
{

	if( aState >= UIGestureRecognizerStateEnded )
	{
		[(UITableView*)self.view setScrollEnabled:YES];

	}
	
	
	[super setState: aState];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	
	// Ignore 3 or more touchs
	if( event.allTouches.count > 2 || self.state >= UIGestureRecognizerStateBegan )
	{
		for( UITouch* touch in touches )
		{
			[self ignoreTouch: touch forEvent:event];
		}
		
		[super touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event];

		
		return;
	}
	
	
	
	
	
	if( event.allTouches.count == 1 )
		self.state = UIGestureRecognizerStatePossible;
	
	if( event.allTouches.count == 2 )
	{
		// Ignore touches outside of this view
//		for( UITouch *touch in event.allTouches )
//		{
//			CGPoint point = [touch locationInView:self.view];
//			if( !CGRectContainsPoint(self.view.bounds, point) )
//			{
//				for( UITouch* touch in touches )
//				{
//					[self ignoreTouch: touch forEvent:event];
//				}
//				
//				return;
//			}
//		}
		
		self.state = UIGestureRecognizerStateBegan;

				
		[(UITableView*)self.view setScrollEnabled:NO];
		
		NSArray* allTouches = event.allTouches.allObjects;
		

		initialLocationInViewA_ = [[allTouches objectAtIndex:0] locationInView:self.view];
		initialLocationInViewA_.y -= [(UITableView*)self.view contentOffset].y;
		
		initialLocationInViewB_ = [[allTouches objectAtIndex:1] locationInView:self.view];
		initialLocationInViewB_.y -= [(UITableView*)self.view contentOffset].y;

		lastLocationInViewA_ = initialLocationInViewA_;
		locationInViewA_ = initialLocationInViewA_;

		lastLocationInViewB_ = initialLocationInViewB_;
		locationInViewB_ = initialLocationInViewB_;
		
		initialDistance_ = sqrtf( powf(initialLocationInViewA_.x - initialLocationInViewB_.x, 2)  + powf(initialLocationInViewA_.y - initialLocationInViewB_.y, 2));

		currentDistance_ = initialDistance_;
		lastDistance_ = currentDistance_;

		initialContentOffset_ = [(UITableView*)self.view contentOffset];
		
		
	}
	
	[super touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event];

	
	
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{

	if( event.allTouches.count < 2 )
	{
		
		[super touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event];
		return;
		
	}
	
	

	
	
//	if( self.state <= UIGestureRecognizerStateChanged )
//	{
//
//		if( fabsf( initialLocationInViewA_.x - locationInViewA_.x ) > fabsf( initialLocationInViewA_.y - locationInViewA_.y ) )
//					if( fabsf( initialLocationInViewB_.x - locationInViewB_.x ) > fabsf( initialLocationInViewB_.y - locationInViewB_.y ) )
//		{
//			self.state = UIGestureRecognizerStateCancelled;
//			[super touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event];
//			
//			return;
//		}
//		
//		
//	}
//	
	
	

	
	
	if( event.allTouches.count == 2 )
	{
		NSArray* allTouches = event.allTouches.allObjects;
		
		lastLocationInViewA_ = locationInViewA_;
		lastLocationInViewB_ = locationInViewB_;
		
		locationInViewA_ = [[allTouches objectAtIndex:0] locationInView:self.view];
		locationInViewA_.y -= [(UITableView*)self.view contentOffset].y;
		
		locationInViewB_ = [[allTouches objectAtIndex:1] locationInView:self.view];
		locationInViewB_.y -= [(UITableView*)self.view contentOffset].y;


		
		lastDistance_ = currentDistance_;
		
		currentDistance_ = sqrtf( powf(locationInViewA_.x - locationInViewB_.x, 2)  + powf(locationInViewA_.y - locationInViewB_.y, 2));

	}
	
	
	self.state = UIGestureRecognizerStateChanged;	

	[super touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event];
	
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	
	if( self.state == UIGestureRecognizerStatePossible )
	{
		self.state = UIGestureRecognizerStateFailed;
	}
	
	if( self.state ==  UIGestureRecognizerStateBegan || 
			  self.state == UIGestureRecognizerStateChanged  )
	{
		self.state = UIGestureRecognizerStateEnded;
	}
	
	
	
	[super touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event];
	
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event;
{


	if( self.state != UIGestureRecognizerStateFailed &&
	   self.state != UIGestureRecognizerStateEnded ) 
	{
		
		self.state = UIGestureRecognizerStateCancelled;
	}
	
	
	[super touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event];
	
}


- (void)reset
{
	[super reset];

}

- (BOOL)canBePreventedByGestureRecognizer:(UIGestureRecognizer *)preventingGestureRecognizer
{
	if( self.state >= UIGestureRecognizerStateBegan ) return NO;
	

	if( [preventingGestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]] )
		return NO;

	
	return YES;
}

- (BOOL)canPreventGestureRecognizer:(UIGestureRecognizer *)preventingGestureRecognizer
{

	
	if( self.state == UIGestureRecognizerStateBegan || self.state == UIGestureRecognizerStateChanged  )
	{

//		//ここではpanをpreventしない。自らがキャンセルするにまかせる。そうすることで、パン側のキャンセルアクションが呼ばれる
//		if( [preventingGestureRecognizer isKindOfClass:[OrigamiPanGestureRecognizer class]] )
//			return NO;
		
		return YES;
	}
	
	return NO;
}

#pragma mark - Accessors

-(CGPoint)initialTopLocation
{
	if( initialLocationInViewA_.y > initialLocationInViewB_.y ) return initialLocationInViewB_;
	else return initialLocationInViewA_;
}

-(CGPoint)initialBottomLocation
{
	if( initialLocationInViewA_.y < initialLocationInViewB_.y ) return initialLocationInViewB_;
	else return initialLocationInViewA_;

}

-(CGPoint)topLocation
{
	if( locationInViewA_.y > locationInViewB_.y ) return locationInViewB_;
	else return locationInViewA_;
}

-(CGPoint)bottomLocation
{
	if( locationInViewA_.y < locationInViewB_.y ) return locationInViewB_;
	else return locationInViewA_;
	
}

-(CGFloat)pinchVelocity
{

	return  currentDistance_ - lastDistance_;
}

@end




@implementation OrigamiPanGestureRecognizer
@synthesize distance = currentDistance_;
@synthesize initialContentOffset = initialContentOffset_;
@synthesize initialLocation = initialLocationInView_;
@synthesize location = locationInView_;

- (id)initWithTarget:(UITableView*)target action:(SEL)action; {
	self = [super initWithTarget:(id)target action:(SEL)action];
	if (self) {
		
		
	}
	return self;
}

-(void)setState: (UIGestureRecognizerState)aState
{
	
	if( aState >= UIGestureRecognizerStateEnded )
	{
		[(UITableView*)self.view setScrollEnabled:YES];
		
	}
	
	
	if( aState == UIGestureRecognizerStateCancelled )
	{
		
	}
	
	[super setState: aState];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{

	// Ignore 2 or more touchs
	if( event.allTouches.count > 1 || self.state >= UIGestureRecognizerStateBegan )
	{
		
		
		self.state = UIGestureRecognizerStateCancelled;
		
		if( [self.delegate respondsToSelector:@selector(origamiCancelsPanning)] )
		{
			[(id <OrigamiPanGestureRecognizerDelegate>) self.delegate origamiCancelsPanning];
		}
		
		[super touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event];

		return;
	}
	
	
	
	// Ignore touches outside of this view
	//		for( UITouch *touch in event.allTouches )
	//		{
	//			CGPoint point = [touch locationInView:self.view];
	//			if( !CGRectContainsPoint(self.view.bounds, point) )
	//			{
	//				for( UITouch* touch in touches )
	//				{
	//					[self ignoreTouch: touch forEvent:event];
	//				}
	//				
	//				return;
	//			}
	//		}
	
	UITouch* touch = [touches anyObject];

	BOOL canStart = YES;
	
	if( [self.delegate respondsToSelector:@selector(origamiCanPanForTouch:)] )
	{
		canStart = [(id <OrigamiPanGestureRecognizerDelegate>) self.delegate origamiCanPanForTouch: touch];
	}
	
	if( !canStart )
	{
		[self ignoreTouch:touch forEvent:event];
		
		[super touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event];

		return;
	}
		
		
	self.state = UIGestureRecognizerStatePossible;
	
	
	[(UITableView*)self.view setScrollEnabled:NO];
	
	
	
	initialLocationInView_ = [touch locationInView:self.view];
	initialLocationInView_.y -= [(UITableView*)self.view contentOffset].y;
	
	
	lastLocationInView_ = initialLocationInView_;
	locationInView_ = initialLocationInView_;
	
	
	
	currentDistance_ = 0;
	lastDistance_ = 0;
	
	initialContentOffset_ = [(UITableView*)self.view contentOffset];
	
	
	
	[super touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event];
	
	
	
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	

	UITouch* touch = [touches anyObject];
	
	
	locationInView_ = [touch locationInView:self.view];
	locationInView_.y -= [(UITableView*)self.view contentOffset].y;
	
	
	lastDistance_ = currentDistance_;
	
	currentDistance_ = sqrtf( powf(locationInView_.y - initialLocationInView_.y, 2));
	
	
	if( self.state == UIGestureRecognizerStatePossible )
	{
		
		if( ABS( initialLocationInView_.x - locationInView_.x ) > ABS( initialLocationInView_.y - locationInView_.y ) )
		{
			[self ignoreTouch:touch forEvent:event];
			[super touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event];
			
			return;
		}
		
		
		self.state = UIGestureRecognizerStateBegan;
	}
	
	if( self.state == UIGestureRecognizerStateBegan )
	{
		self.state = UIGestureRecognizerStateChanged;
	}
	
	[super touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event];
	
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{

	if( self.state == UIGestureRecognizerStatePossible )
	{
		self.state = UIGestureRecognizerStateFailed;
	}
	
	if( self.state ==  UIGestureRecognizerStateBegan || 
		self.state == UIGestureRecognizerStateChanged  )
	{
		self.state = UIGestureRecognizerStateEnded;
	}
	
	
	[super touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event];
	
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event;
{

	if( self.state != UIGestureRecognizerStateFailed &&
	   self.state != UIGestureRecognizerStateEnded ) 
	{
		
		self.state = UIGestureRecognizerStateCancelled;
	}
	
	
	[super touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event];
	
}


- (void)reset
{
	[super reset];
	
}

- (BOOL)canBePreventedByGestureRecognizer:(UIGestureRecognizer *)preventingGestureRecognizer
{

	if( self.state >= UIGestureRecognizerStateBegan )
	{
		if( [preventingGestureRecognizer isKindOfClass:[OrigamiPinchGestureRecognizer class]] )
			return YES;
		
		if( [preventingGestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]] )
		{
			return YES;// Disable long rpess
		}

		if( [preventingGestureRecognizer isKindOfClass:[UISwipeGestureRecognizer class]] )
		{
			return YES;
		}
		
		return NO;
	}
	
	return YES;
}

- (BOOL)canPreventGestureRecognizer:(UIGestureRecognizer *)preventingGestureRecognizer
{

	if( [preventingGestureRecognizer isKindOfClass:[OrigamiPinchGestureRecognizer class]] )
		return NO;
	
	if( [preventingGestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]] )
	{
	//	if( self.state == UIGestureRecognizerStateBegan || self.state == UIGestureRecognizerStateChanged  )
		{

			return YES;
		}	
	}
	
	if( [preventingGestureRecognizer isKindOfClass:[UISwipeGestureRecognizer class]] )
	{
		return NO;
	}
	
	if( self.state == UIGestureRecognizerStateBegan || self.state == UIGestureRecognizerStateChanged  )
	{
		return YES;
	}
	
	return NO;
}

#pragma mark - Accessors



-(CGFloat)velocity
{
	
	return  currentDistance_ - lastDistance_;
}

@end


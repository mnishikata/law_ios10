//
//  C3DTAnimation.h
//  mySTEP
//
//  Created by Dr. H. Nikolaus Schaller on Sat Jan 07 2006.
//  Copyright (c) 2005 DSITRI.
//
//  This file is part of the mySTEP Library and is provided
//  under the terms of the GNU Library General Public License.
//
#import <QuartzCore/QuartzCore.h>



@interface OrigamiAnimation : NSObject 
{
	NSDate *startDate_;
	CADisplayLink *mDisplayLink;
	NSTimeInterval duration_;
	CGFloat progress_;
	CGFloat frameRate_;
	BOOL isAnimating_;      // ?or the NSThread *
	
	void(^progressBlock)(CGFloat);
	
	void(^processHandler)(void);
	void(^completionHandler)(void);

	CGFloat startProgress_;
	
	dispatch_queue_t queue_;
}

- (id) initWithDuration:(NSTimeInterval) duration;
- (void) dealloc;
- (BOOL) isAnimating_;
- (NSArray *) runLoopModesForAnimating;;
- (void) _animate:(NSTimer *) timer;;
- (void) startAnimation;;
- (void) stopAnimation;;
- (void) startAnimationFromProgress:(CGFloat)startProgress;


@property (nonatomic, copy) void(^progressBlock)(CGFloat);
@property (nonatomic, copy) void(^processHandler)(void); // Performed once in animation process
@property (nonatomic, copy) void(^completionHandler)(void);



@end



//
//  C3DTAnimation.m
//  mySTEP
//
//  Created by Dr. H. Nikolaus Schaller on Sat Mar 06 2006.
//  Copyright (c) 2006 DSITRI.
//
//  This file is part of the mySTEP Library and is provided
//  under the terms of the GNU Library General Public License.
//

#import "OrigamiAnimation.h"


@implementation OrigamiAnimation
@synthesize completionHandler, progressBlock, processHandler;


- (id) initWithDuration:(NSTimeInterval) duration
{
	if((self=[super init]))
	{
		duration_ = duration;
		frameRate_ = 30;
		
		queue_ = dispatch_queue_create("Animation_Queue", DISPATCH_QUEUE_SERIAL);
	}
	return self;
}


- (void) dealloc;
{
	mDisplayLink = nil;
	startDate_ = nil;
	completionHandler = nil;
	processHandler = nil;
	progressBlock = nil;
	
//	dispatch_release(queue_);
	
#if OS_OBJECT_USE_OBJC_RETAIN_RELEASE
	
	queue_ = nil;
	
#else
	dispatch_release(queue_);
	
#endif

}

- (BOOL) isAnimating_
{ 
	return mDisplayLink != nil; 
}


- (NSArray *) runLoopModesForAnimating;
{
	return nil;     // default means any mode
}


- (void) _animate:(NSTimer *) timer;
{ // called every 1/fps seconds from NSTimer
	

	
	progress_ = [[NSDate date] timeIntervalSinceDate:startDate_]/duration_;
	if( progress_ + startProgress_ > 1.0)
	{ 
		if( progressBlock )
			progressBlock( 1.0 );

		
		// done
		[mDisplayLink invalidate];
		mDisplayLink = nil;
		startDate_ = nil;
		
		if( processHandler )
		{
			dispatch_sync(queue_, processHandler);
			processHandler = nil;
		}

		
		if( completionHandler )
		{
			dispatch_sync(queue_, completionHandler);
			completionHandler = nil;
		}
		
		progressBlock = nil;
		
		return;
	}
	
	if( progressBlock )
		progressBlock( progress_ + startProgress_);
}

- (void) startAnimation
{
	
	[self startAnimationFromProgress:0];
}

- (void) startAnimationFromProgress:(CGFloat)startProgress
{
	startProgress_= startProgress;
	
	if(!mDisplayLink )
	{
		startDate_=[NSDate new];
		[self runLoopModesForAnimating];        // schedule only in specified modes
		
		mDisplayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(_animate:)];
		[mDisplayLink setFrameInterval:1];
		[mDisplayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
		
	}
}


- (void) stopAnimation;
{
	[mDisplayLink invalidate];
	mDisplayLink=nil;
	startDate_ = nil;
	
	if( processHandler )
	{
		dispatch_sync(queue_, processHandler);
		processHandler = nil;
	}

	
	if( completionHandler )
	{
		dispatch_sync(queue_, completionHandler);
		
		completionHandler = nil;

	}
	
	progressBlock = nil;

}



@end

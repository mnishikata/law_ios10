//
//  OrigamiTransformLayer.h
//  TableTest
//
//  Created by Nishikata Masatoshi on 2/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface OrigamiTransformLayer : CATransformLayer
+ (OrigamiTransformLayer *)transformLayerFromImage:(UIImage *)image subImage:(UIImage*)subImage frame:(CGRect)frame anchorPiont:(CGPoint)anchorPoint startAngle:(CGFloat)start endAngle:(CGFloat)end darker:(BOOL)darker;
-(void)setProgress:(CGFloat)progress;

@property (nonatomic, readwrite) CGFloat startAngle;
@property (nonatomic, readwrite) CGFloat endAngle;
@property (nonatomic, retain) CALayer* shadowLayer;
@property (nonatomic, retain) CALayer* imageLayer;
@property (nonatomic, retain) CALayer* subImageLayer;

@property (nonatomic, readwrite) NSUInteger index;

@end


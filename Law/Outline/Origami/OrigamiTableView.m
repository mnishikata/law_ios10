//
//  OrigamiTableView.m
//  TableTest
//
//  Created by Nishikata Masatoshi on 3/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OrigamiTableView.h"

#define  ANIMATION_DURATION 0.4
#define CLOSE_ANIMATION 0.25



@implementation UITableViewCell (LayerDrawing)

+(UIImage*)mn_cellSnapshot:(NSArray*)cells maxHeight: (CGFloat)maxHeight alignBottom:(BOOL)alignBottom backgroundColor:(UIColor*)backgroundColor
{
	if( !cells ) return nil;
	
	CGSize totalSize = CGSizeZero;
	CGFloat minY = MAXFLOAT;
	
	for (UITableViewCell* cell in cells )
	{
		[cell layoutIfNeeded];
		
		
		totalSize.height += cell.frame.size.height;
		
		minY = MIN( minY, cell.frame.origin.y ); 
		
		totalSize.width = cell.frame.size.width;
	}
	
	CGFloat imageHeight = MIN(maxHeight, totalSize.height);
	
	CGFloat screenScale = [[UIScreen mainScreen] scale];
		
	UIGraphicsBeginImageContextWithOptions(CGSizeMake(totalSize.width, imageHeight), YES,  screenScale);
	
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	
	CGContextSetFillColorWithColor(context, backgroundColor.CGColor);
	CGContextFillRect(context, CGRectMake(0, 0, totalSize.width, imageHeight));
	
	
	CGFloat originY = 0;
	
	if( alignBottom && totalSize.height > maxHeight )
	{
		originY = maxHeight - totalSize.height;
	}
	
	for( NSUInteger hoge = 0; hoge < cells.count; hoge++ )
	{
		@autoreleasepool {
				
			UITableViewCell* cell = [cells objectAtIndex: hoge];
			if( ![cell isKindOfClass:[UITableViewCell class]] ) continue;
			if( ![cell respondsToSelector:@selector(mn_drawContentsInContext:)] ) continue;
			CGFloat height = cell.frame.size.height;
						
			CGContextSaveGState(context);
			
			CGAffineTransform t = CGAffineTransformMakeTranslation(floorf( cell.frame.origin.x), originY);
			CGContextConcatCTM(context, t);
			
			[cell mn_drawContentsInContext:context];
			
//			CGPoint points[2];
//			points[0] = CGPointMake(0,  height - 0.5);
//			points[1] = CGPointMake(totalSize.width, height - 0.5);
//			CGContextSetLineWidth(context, 1.0);
//			CGContextSetStrokeColorWithColor(context, [UIColor colorWithWhite:224.0/255.0 alpha:1].CGColor);
//			CGContextStrokeLineSegments(context, points, 2);
			
			CGContextRestoreGState(context);
				
				
			originY += height;
			
		}
	}
	
	UIImage *viewSnapShot = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return viewSnapShot;
}


-(void)mn_drawContentsInContext:(CGContextRef)context
{
	
	UIGraphicsPushContext(context);
	[[UIColor blackColor] set];
	
	NSDictionary* attributes = @{ NSFontAttributeName : [UIFont boldSystemFontOfSize:20] };
	[self.textLabel.text drawAtPoint:CGPointMake(10, 10)
							withAttributes:attributes];
	UIGraphicsPopContext();
}


@end



@implementation OrigamiTableView

@synthesize origamiLayer = origamiLayer_;
@synthesize origamiPinchGesture = pinchGesture_;
@synthesize origamiPanGesture = panGesture_;
@synthesize transitioning = transitioning_;
@synthesize origamiDelegate = origamiDelegate_;

- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
    if (self) {
        
		 [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
	self = [super initWithCoder:coder];
	if (self) {
		[self setup];
	}
	return self;
}

-(void)dealloc
{
	origamiDelegate_ = nil;
	animation_ = nil;
}


-(void)setup
{
	if( !pinchGesture_ )
	{
		pinchGesture_ = [[OrigamiPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchGesture:)];
		[self addGestureRecognizer: pinchGesture_];
	}
	
	if( !panGesture_ )
	{
		panGesture_ = [[OrigamiPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGesture:)];
		panGesture_.delegate = self;
		[self addGestureRecognizer: panGesture_];
	}
	
	if( !indentGesture_ )
	{
		indentGesture_ = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(indent:)];
		indentGesture_.direction = UISwipeGestureRecognizerDirectionRight;
		[self addGestureRecognizer:indentGesture_];
		
	}
	
	
	if( !unindentGesture_ )
	{
		unindentGesture_ = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(unindent:)];
		unindentGesture_.direction = UISwipeGestureRecognizerDirectionLeft;
		[self addGestureRecognizer:unindentGesture_];
		
	}
	
	
	// Accurate control for panning
	self.panGestureRecognizer.delegate = self;
	
//	if( !doubleIndentGesture_ )
//	{
//		doubleIndentGesture_ = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(doubleIndent:)];
//		doubleIndentGesture_.direction = UISwipeGestureRecognizerDirectionRight;
//		doubleIndentGesture_.numberOfTouchesRequired = 2;
//		[self addGestureRecognizer:doubleIndentGesture_];
//		
//	}
//	
	
//	if( !doubleUnindentGesture_ )
//	{
//		doubleUnindentGesture_ = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(doubleUnindent:)];
//		doubleUnindentGesture_.direction = UISwipeGestureRecognizerDirectionLeft;
//		doubleUnindentGesture_.numberOfTouchesRequired = 2;
//		[self addGestureRecognizer:doubleUnindentGesture_];
//		
//	}
	
	self.dataSource = self;
	self.delegate = self;
}


-(void)reloadData
{
	[[NSNotificationCenter defaultCenter] postNotificationName:@"OrigamiTableViewWillReloadNotificationName" object:self];
	
	[super reloadData];
}

-(CALayer*)layerForCells:(NSArray*)cells alignBottom:(BOOL)alignBottom
{
	
	CALayer* layer = [CALayer layer];
	
	layer.edgeAntialiasingMask = 0;
	layer.opaque = YES;
	layer.contentsScale = [[UIScreen mainScreen] scale];
	layer.backgroundColor = self.backgroundColor.CGColor;
	layer.frame = CGRectMake(0, 0, origamiLayer_.bounds.size.width, self.bounds.size.height);
	
	if( cells && cells.count > 0 )
	{
		
		UIImage *pushingViewSnapShot = [UITableViewCell mn_cellSnapshot: cells 
																				maxHeight: self.frame.size.height
																			  alignBottom: alignBottom
																		  backgroundColor: self.backgroundColor];
		
		layer.contents = (__bridge id)pushingViewSnapShot.CGImage;
		
	}
	
	if( alignBottom )
	{
		layer.contentsGravity = kCAGravityTop;
		layer.anchorPoint = CGPointMake(0, 1);

	}
	else
	{
		layer.contentsGravity = kCAGravityBottom;
		layer.anchorPoint = CGPointMake(0, 0);
	}
	
	return layer;
}


#pragma mark - Opening



-(BOOL)setupAndAddOrigamiLayerWithCells:(NSArray*)cellArray subCells:(NSArray*)subCells expand:(BOOL)expand toY: (CGFloat)pointY
{

	
	if( cellArray.count == 0 )
	{
//		NSException* exception = [NSException exceptionWithName:@"Error" reason:@"No cells to expand" userInfo:nil];
//		[exception raise];
		return NO;
	}
	
	
	UIImage *viewSnapShot = [UITableViewCell mn_cellSnapshot:cellArray
																  maxHeight: self.frame.size.height
																alignBottom: NO
														  backgroundColor: self.backgroundColor];
	
	UIImage *subViewSnapShot = [UITableViewCell mn_cellSnapshot:subCells
																	  maxHeight: self.frame.size.height
																	alignBottom: NO
															  backgroundColor: self.backgroundColor];
	
	//set layers
	
	CGSize totalSize = viewSnapShot.size;
	
	if( origamiLayer_ )
	{
		[self closeOrigamiLayer];
	}
	
	origamiLayer_ = [OrigamiLayer layer];
		
	NSUInteger numberOfFolds = MAX( 1, (int)floorf( cellArray.count /2.0 ) );
	
	if( totalSize.height == self.frame.size.height )
		numberOfFolds = 2;
	
	origamiLayer_.frame = CGRectMake(0, pointY, totalSize.width, totalSize.height);
	origamiLayer_.image = viewSnapShot;
	origamiLayer_.subImage = subViewSnapShot;
	origamiLayer_.numberOfFolds = numberOfFolds;
	
	[self.layer addSublayer:origamiLayer_];
	
	//レイヤーを追加した後でこれを行わないと、表示が正しく行なわれない 誤
	//[self layoutIfNeeded];

	
	if( expand )
	{
		[origamiLayer_ setProgress: 0];
	}else
	{
		[origamiLayer_ setProgress: 1];

	}
	
	return YES;
}


//-(void)origamiTransitionWithCells:(NSArray*)cellArray expand:(BOOL)expand toY: (CGFloat)pointY completion:(void (^)(BOOL finished))completion
//{
//	if( transitioning_ )
//	{
//		if( completion ) completion(NO);
//		return;
//	}
//	
//	transitioning_ = YES;
//	
//	// Reload before call this method
//	
//	pinchGesture_.enabled = NO;
//	panGesture_.enabled = NO;
//	
//	
//	[self layoutIfNeeded];
//	
//	
//	[self setupAndAddOrigamiLayerWithCells:cellArray subCells:nil expand:expand toY:pointY];
//	
//	
//	// Header view
//	
//	
//	NSMutableArray* headerViews = [NSMutableArray array];
//	for( UIView *subview in self.visibleCells )
//	{
//		if( subview.frame.origin.y < pointY  )
//		{
//			[headerViews addObject: subview];
//		}
//	}
//	
//	
//	CGFloat headerHeight = pointY - self.contentOffset.y;
//	CALayer* headerLayer = [self layerForCells: headerHeight > 0? headerViews:nil];
//	headerLayer.contentsGravity = kCAGravityTop;
//	headerLayer.anchorPoint = CGPointMake(0, 1);
//	
//	//origamiLayer_.headerLayer = headerLayer;
//	
//	
//	
//	
//	
//	// Footer view
//	
//	
//	NSMutableArray* footerViews = [NSMutableArray array];
//	for( UIView *subview in self.visibleCells )
//	{
//		if( subview.frame.origin.y >= pointY  )
//		{
//			[footerViews addObject: subview];
//		}
//	}
//	
//	
//	CGFloat footerHeight = self.bounds.size.height - (pointY - self.contentOffset.y);
//	CALayer* footerLayer = [self layerForCells: footerHeight > 0? footerViews:nil];
//	footerLayer.contentsGravity = kCAGravityBottom;
//	footerLayer.anchorPoint = CGPointMake(0, 0);
//	
//	origamiLayer_.footerLayer = footerLayer;
//	
//	
//	[origamiLayer_ setProgress: expand? 1:0];
//	
//	// Animation
//	
//	animation_ = [[OritamiAnimation alloc] initWithDuration:CLOSE_ANIMATION];
//	
//	__block OrigamiTableView __weak *tableView = self;
//	
//	animation_.progressBlock = ^(CGFloat progress){
//		
//		[tableView.origamiLayer setProgress: expand? 1-progress:progress];
//	};
//	
//	animation_.completionHandler = ^{
//		
//		[tableView.origamiLayer removeFromSuperlayer];
//		tableView.origamiLayer = nil;
//		
//		[tableView reloadData];
//		
//		if (completion)
//			completion(YES);
//		
//		tableView.origamiPinchGesture.enabled = YES;
//		tableView.origamiPanGesture.enabled = YES;
//		
//		tableView.transitioning = NO;
//	};
//	
//	
//	[animation_ startAnimation];
//	
//	
//	
//}


-(void)expandCells:(NSArray*)cellArray from: (NSIndexPath*)fromIndexPath completion:(void (^)(BOOL finished))completion
{
	
	if( transitioning_ )
	{
		if( completion ) completion(NO);
		return;
	}
	
	// Reload before call this method
	
	pinchGesture_.enabled = NO;
	panGesture_.enabled = NO;

	
	if( cellArray.count == 0 )
	{
		completion(YES);
		transitioning_ = NO;
		return;
	}
	
	transitioning_ = YES;
	
	
	
	[self layoutIfNeeded];
	
	
	CGRect rect = [self rectForRowAtIndexPath:fromIndexPath];
	
	
	BOOL flag = [self setupAndAddOrigamiLayerWithCells: cellArray
											subCells: nil 
											  expand: NO 
												  toY: CGRectGetMaxY(rect)];
	
	if( !flag )
	{
		if (completion)
			completion(YES);
		return;
	}
	
	__block OrigamiTableView __weak *tableView = self;

	[self reloadAndExpandFrom: fromIndexPath
							 length: cellArray.count
					 visibleCells: self.visibleCells
				 scrollToVisible: YES
				  withCompletion: ^(BOOL finished){
		

					  [tableView.origamiLayer removeFromSuperlayer];
					  tableView.origamiLayer = nil;
					  
					  if (completion)
						  completion(YES);
					  
					  tableView.origamiPinchGesture.enabled = YES;
					  tableView.origamiPanGesture.enabled = YES;
					  
					  tableView.transitioning = NO;
					  
					  [tableView flashScrollIndicators];

	}];
	
}


#pragma mark - Change State

-(void)setSubImageHidden:(BOOL)flag
{
	[origamiLayer_ setSubImageHidden:flag];
}



-(void)adjustContentOffset:(BOOL)animated
{
	
	CGPoint contentOffset = self.contentOffset;
	
	if( contentOffset.y < 0 ) contentOffset.y = 0;
	if( contentOffset.y > self.contentSize.height - self.bounds.size.height )
		contentOffset.y  = self.contentSize.height - self.bounds.size.height;
	
	if( self.contentSize.height < self.frame.size.height )
		contentOffset.y = 0;
	
	[self setContentOffset:contentOffset animated:animated];
}

#pragma mark - Closing

-(void)closeOrigamiLayer
{
	origamiLayer_.contents = nil;
	[origamiLayer_ removeFromSuperlayer];
	origamiLayer_ = nil;
	
}

-(void)collapseCells:(NSArray*)cellArray toY: (CGFloat)pointY completion:(void (^)(BOOL finished))completion
{
	if( transitioning_ )
	{
		if( completion ) completion(NO);
		return;
	}
	
	transitioning_ = YES;
	
	// Reload before call this method
	
	pinchGesture_.enabled = NO;
	panGesture_.enabled = NO;
	
	
	[self layoutIfNeeded];
	
	BOOL flag = [self setupAndAddOrigamiLayerWithCells: cellArray
											subCells: nil
											  expand: YES 
												  toY: pointY];
	
	if( !flag )
	{
		if (completion)
			completion(YES);
	
		return;
	}
	
	
	__block OrigamiTableView __weak *tableView = self;
	
	[self reloadAndCloseVisibleCells: CLOSE_ANIMATION 
								alignBottom: NO
							withCompletion: ^(BOOL finished){
								
								[tableView.origamiLayer removeFromSuperlayer];
								tableView.origamiLayer = nil;
								
								if (completion)
									completion(YES);
								
								tableView.origamiPinchGesture.enabled = YES;
								tableView.origamiPanGesture.enabled = YES;
								
								tableView.transitioning = NO;
								
								[tableView flashScrollIndicators];
							}];
	
}

-(void)collapseCells:(NSArray*)cellArray toView: (UIView*)floatingView completion:(void (^)(BOOL finished))completion
{
	
	// Reload before call this method
	
	pinchGesture_.enabled = NO;
	panGesture_.enabled = NO;
	
	
	BOOL flag = [self setupAndAddOrigamiLayerWithCells:cellArray subCells:nil expand:YES toY:0];
	
	if( !flag )
	{
		if (completion)
			completion(YES);
		return;
	}
	
	
	CGRect frame = origamiLayer_.frame;
	frame.origin.y = floatingView.bounds.size.height;
	origamiLayer_.frame = frame;
	
	[floatingView.layer addSublayer: origamiLayer_];
	
	
	// Animation
	
	animation_ = [[OrigamiAnimation alloc] initWithDuration:CLOSE_ANIMATION];
	
	__block OrigamiTableView __weak *tableView = self;
	
	animation_.progressBlock = ^(CGFloat progress){
		
		[tableView.origamiLayer setProgress: progress];
	};
	
	animation_.completionHandler = ^{
		
		[tableView.origamiLayer removeFromSuperlayer];
		tableView.origamiLayer = nil;
		
		if (completion)
			completion(YES);
		
		tableView.origamiPinchGesture.enabled = YES;
		tableView.origamiPanGesture.enabled = YES;
		
		tableView.transitioning = NO;
		
		[tableView flashScrollIndicators];

	};
	
	
	[animation_ startAnimation];
	
	
}



-(void)reloadAndCloseVisibleCells:(CGFloat)duration alignBottom:(BOOL)alignBottom withCompletion:(void (^)(BOOL finished))completion
{
	
	self.userInteractionEnabled = NO;
	[self layoutIfNeeded];

	
	//リロードで全体が動く場合があるため、元座標を取得しておく
	CGRect rect = [origamiLayer_.superlayer convertRect:origamiLayer_.frame toLayer:self.superview.layer];
	
	
	// これが消えて行く座標
	//CGFloat vanishingY = CGRectGetMaxY( [self rectForSection:0] );
	CGFloat vanishingY = origamiLayer_.frame.origin.y;


	[self reloadData];
	[self layoutIfNeeded];
	
	if( alignBottom )
	[self adjustContentOffset:NO];

	CGRect rect2 = [origamiLayer_.superlayer convertRect:origamiLayer_.frame 
																toLayer:self.superview.layer];


	CGFloat posDifY = rect2.origin.y - rect.origin.y;
	
	
	NSArray* visibleCells = self.visibleCells;

	
	// Header view
	
	NSMutableArray* headerViews = [NSMutableArray array];
	for( UIView *subview in visibleCells )
	{		
		if( subview.frame.origin.y < vanishingY  )
		{
			[headerViews addObject: subview];
		}
	}

	
	CGFloat headerHeight = vanishingY - self.contentOffset.y;
	CALayer* headerLayer = [self layerForCells: ( headerHeight > 0? headerViews:nil )
											 alignBottom: YES];

	origamiLayer_.headerLayer = headerLayer;
	
	
	// Footer view
	
	
	NSMutableArray* footerViews = [NSMutableArray array];
	for( UIView *subview in visibleCells )
	{
		if( subview.frame.origin.y >= vanishingY  )
		{
			[footerViews addObject: subview];
		}
	}
	
	
	CGFloat footerHeight = self.bounds.size.height - (vanishingY - self.contentOffset.y);
	CALayer* footerLayer = [self layerForCells: ( footerHeight > 0? footerViews:nil )
											 alignBottom: NO];
	
	origamiLayer_.footerLayer = footerLayer;
	
	
	
	// アニメーション開始設定
	
	CGFloat startOriginY = origamiLayer_.frame.origin.y;
	startOriginY -= posDifY;
	
	CGRect frame = origamiLayer_.frame;
	frame.origin.y = startOriginY;
	origamiLayer_.frame = frame;
	
	CGFloat endOriginY = vanishingY;
	
	
	// Animation
	
	//広がった状態から閉じる
	if( origamiLayer_.frame.size.height > origamiLayer_.originalHeight )
	{
		CGFloat startHeight = origamiLayer_.frame.size.height;

		
		animation_ = [[OrigamiAnimation alloc] initWithDuration:duration];
		
		__block OrigamiTableView __weak *tableView = self;
		
		animation_.progressBlock = ^(CGFloat progress){
			
			CGFloat height = (1.0 - progress) * startHeight;
						
			[tableView.origamiLayer setHeight: height ];
			
			CGFloat currentY = startOriginY*(1-progress) + endOriginY*progress;
			
			CGRect frame = tableView.origamiLayer.frame;
			frame.origin.y = currentY;
			tableView.origamiLayer.frame = frame;

			
		};
		
		animation_.completionHandler = ^{
			
			[tableView.origamiLayer removeFromSuperlayer];
			tableView.origamiLayer = nil;
			
			if (completion)
				completion(YES);
			
			tableView.userInteractionEnabled = YES;
			

		};
				
		[animation_ startAnimation];
		
		
	}else
	{
		//半分閉じた状態から閉じる
		CGFloat startProgress;
		
		CGFloat rad = acosf( origamiLayer_.frame.size.height / origamiLayer_.originalHeight);
		startProgress = rad / M_PI_2;
		
		
		animation_ = [[OrigamiAnimation alloc] initWithDuration: duration ];
		
		__block OrigamiTableView __weak *tableView = self;
		
		animation_.progressBlock = ^(CGFloat progress){
			
			CGFloat currentHeight = tableView.origamiLayer.frame.size.height;
			
			[tableView.origamiLayer setProgress: progress];
			
			if( alignBottom )
			{
				CGFloat difY = currentHeight - tableView.origamiLayer.frame.size.height;
				
				CGRect frame = tableView.origamiLayer.frame;
				frame.origin.y += difY;
				tableView.origamiLayer.frame = frame;
				
			}
			else
			{
				CGFloat duration = 1.0 - startProgress;
				CGFloat currentY = 0;
				
				if( duration !=0 )
				{
				currentY = startOriginY*(1-(progress-startProgress)/duration) + endOriginY*((progress-startProgress)/duration);
				
				
				CGRect frame = tableView.origamiLayer.frame;
				frame.origin.y = currentY;
				tableView.origamiLayer.frame = frame;
					
				}
				
			}
		};
		
		animation_.completionHandler = ^{
			
			[tableView.origamiLayer removeFromSuperlayer];
			tableView.origamiLayer = nil;
			
			if (completion)
				completion(YES);
			
			tableView.userInteractionEnabled = YES;
			

		};
		

		[origamiLayer_ setProgress: startProgress];
		
		[animation_ startAnimationFromProgress: startProgress];
		
	}
}


-(void)reloadAndExpandFrom:(NSIndexPath*)topIndexPath length:(NSUInteger)length visibleCells:(NSArray*)visibleCells scrollToVisible:(BOOL)scrollToVisible withCompletion:(void (^)(BOOL finished))completion
{

	self.userInteractionEnabled = NO;
	[self layoutIfNeeded];
	
	// Pattern A: Expand (spring back)
	if( origamiLayer_.frame.size.height <= origamiLayer_.originalHeight )
	{
		//リロードで全体が動く場合があるため、元座標を取得しておく
		CGRect rect = [origamiLayer_.superlayer convertRect:origamiLayer_.frame toLayer:self.superview.layer];

		// これが追加されるセルの座標
		

		//これだと、セルが再度読み込まれてしまう
		//CGFloat pointY = CGRectGetMinY( [self rectForSection:1] );
		CGFloat pointY = origamiLayer_.frame.origin.y;

		CGFloat footerY = pointY + self.origamiCurrentHeight;

		
		// Header view
		
		if( !origamiLayer_.headerLayer )
		{
			NSMutableArray* headerViews = [NSMutableArray array];
			for( UIView *subview in visibleCells ) // <<self.visibleCellsを呼ぶと、再読み込みされてしまう。subviewsでも同じ事。
			{		
				if( subview.frame.origin.y < pointY  )
				{
					[headerViews addObject: subview];
				}
			}
			
			
			CGFloat headerHeight = pointY - self.contentOffset.y;
			CALayer* headerLayer = [self layerForCells: headerHeight > 0? headerViews:nil
													 alignBottom: YES];
			
			origamiLayer_.headerLayer = headerLayer;
		}
	
		
		// Footer view
		
		if( !origamiLayer_.footerLayer )
		{
			NSMutableArray* footerViews = [NSMutableArray array];
			for( UIView *subview in visibleCells )
			{
				if( subview.frame.origin.y >= pointY  )
				{
					[footerViews addObject: subview];
				}
			}
			
			
			CGFloat footerHeight = self.bounds.size.height - (footerY - self.contentOffset.y);
			
			CALayer* footerLayer = [self layerForCells: footerHeight > 0? footerViews:nil
													 alignBottom: NO];
			
			origamiLayer_.footerLayer = footerLayer;
		}

		// Reload
		
		
		[self reloadData];
		
		
		if( scrollToVisible )
		{
			CGRect rect = [self rectForRowAtIndexPath: topIndexPath];
			
			if( !CGRectIsEmpty(rect) )
			{
				CGRect rectToMakeVisible = rect;
				

				rectToMakeVisible.size.height += [self origamiExpandedHeight];
				
								
				if( CGRectGetMaxY(rectToMakeVisible) > self.contentOffset.y + self.bounds.size.height )
				{
					CGFloat newOffseet = MIN( rect.origin.y , CGRectGetMaxY(rectToMakeVisible) - self.bounds.size.height );
										
					self.contentOffset = CGPointMake( 0, newOffseet);
				}
			}
		}
		
		[self layoutIfNeeded];

		
		CGRect rect2 = [origamiLayer_.superlayer convertRect:origamiLayer_.frame toLayer:self.superview.layer];
		
		CGFloat posDifY = rect2.origin.y - rect.origin.y;
		
		

		
		
		// アニメーション開始設定
		
		CGFloat startOriginY = origamiLayer_.frame.origin.y;
		startOriginY -= posDifY;
		
		CGRect frame = origamiLayer_.frame;
		frame.origin.y = startOriginY;
		origamiLayer_.frame = frame;
		
		CGFloat endOriginY = pointY;
	
		//半分広がった状態から開く
		
		CGFloat startProgress;
		
		CGFloat rad = acosf( origamiLayer_.frame.size.height / origamiLayer_.originalHeight);
		startProgress = 1.0 - (rad / M_PI_2);
		
		animation_ = [[OrigamiAnimation alloc] initWithDuration: CLOSE_ANIMATION];
		
		__block OrigamiTableView __weak *tableView = self;
		
		animation_.progressBlock = ^(CGFloat progress){
			
			
			[tableView.origamiLayer setProgress: 1.0 - progress ];
			
			CGFloat duration = 1.0 - startProgress;

			CGFloat currentY;
			
			if( duration != 0 )
			currentY = startOriginY*(1-(progress-startProgress)/duration) + endOriginY*((progress-startProgress)/duration);
			else
				currentY = endOriginY;
			
			CGRect frame = tableView.origamiLayer.frame;
			frame.origin.y = currentY;
			tableView.origamiLayer.frame = frame;
		};
		
		animation_.completionHandler = ^{
			
			[tableView.origamiLayer removeFromSuperlayer];
			tableView.origamiLayer = nil;
			
			if (completion)
				completion(YES);
			
			tableView.userInteractionEnabled = YES;
			
			
		};
		

		
		[origamiLayer_ setProgress: startProgress];
//		
		[animation_ startAnimationFromProgress: startProgress];
		
	}
	
	
	// Pattern B: Shrunk to set
	else
	{
		//リロードで全体が動く場合があるため、元座標を取得しておく
		CGRect rect = [origamiLayer_.superlayer convertRect:origamiLayer_.frame toLayer:self.superview.layer];
		
		[self reloadData];
		[self layoutIfNeeded];
		
		CGRect rect2 = [origamiLayer_.superlayer convertRect:origamiLayer_.frame toLayer:self.superview.layer];
		
		CGFloat posDifY = rect2.origin.y - rect.origin.y;
		
		
		
		// これが消えて行く座標
		CGFloat pointY = CGRectGetMinY( [self rectForRowAtIndexPath: topIndexPath] );
				
		NSUInteger footerRow = topIndexPath.row + length + (insertMode_?0:-1);
		
		NSIndexPath *tempToIndexPath = [NSIndexPath indexPathForRow:footerRow inSection:topIndexPath.section];
		
		
		
		CGFloat footerY = MAXFLOAT;
		
		@try {
			
			//rectForRowAtIndexPathはCGRectZeroを返すはずだけど、exceptionを出す
			CGRect fotterRect = [self rectForRowAtIndexPath: tempToIndexPath];
			footerY = CGRectGetMaxY(fotterRect);
			
		}
		@catch (NSException *exception) {
			
			footerY = MAXFLOAT;
			
		}
		
		
		// Header view
		
		NSMutableArray* headerViews = [NSMutableArray array];
		for( UIView *subview in visibleCells )
		{		
			if( subview.frame.origin.y < pointY  )
			{
				[headerViews addObject: subview];
			}
		}
		
		
		CGFloat headerHeight = pointY - self.contentOffset.y;
		CALayer* headerLayer = [self layerForCells: headerHeight > 0? headerViews:nil
												 alignBottom: YES];
		
		origamiLayer_.headerLayer = headerLayer;
		
		
		// Footer view
		
		
		NSMutableArray* footerViews = [NSMutableArray array];
		for( UIView *subview in visibleCells )
		{
			if( subview.frame.origin.y >= footerY  )
			{
				[footerViews addObject: subview];
			}
		}
		
		
		
		CGFloat footerHeight = self.bounds.size.height - (footerY - self.contentOffset.y);
		CALayer* footerLayer = [self layerForCells: footerHeight > 0? footerViews:nil
												 alignBottom: NO];
		
		origamiLayer_.footerLayer = footerLayer;
		

		// アニメーション開始設定
		
		CGFloat startOriginY = origamiLayer_.frame.origin.y;
		startOriginY -= posDifY;
		
		CGRect frame = origamiLayer_.frame;
		frame.origin.y = startOriginY;
		origamiLayer_.frame = frame;
		
		CGFloat endOriginY = pointY;

		
		
		CGFloat startHeight = origamiLayer_.frame.size.height;
		
		CGFloat endHeight = [self origamiExpandedHeight];
		
		animation_ = [[OrigamiAnimation alloc] initWithDuration:CLOSE_ANIMATION];
		
		__block OrigamiTableView __weak *tableView = self;
		
		animation_.progressBlock = ^(CGFloat progress){
			
			CGFloat height = (1.0 - progress) * startHeight + endHeight * progress;
			
			[tableView.origamiLayer setHeight: height];
			
			
			CGFloat currentY = startOriginY*(1-progress) + endOriginY*progress;
			
			CGRect frame = tableView.origamiLayer.frame;
			frame.origin.y = currentY;
			tableView.origamiLayer.frame = frame;
			
			
		};
		
		animation_.completionHandler = ^{
			
			[tableView.origamiLayer removeFromSuperlayer];
			tableView.origamiLayer = nil;
			
			if (completion)
				completion(YES);
			
			tableView.userInteractionEnabled = YES;
			
		};
		
		[animation_ startAnimation];

		
	}
}

#pragma mark - Accessor Setter

-(CGFloat)origamiExpandedHeight
{
	return origamiLayer_.originalHeight;
}

-(CGFloat)origamiCurrentHeight
{
	return origamiLayer_.frame.size.height;
}

-(void)setOrigamiHeight:(CGFloat)height andOriginY:(CGFloat)originY
{
	[origamiLayer_ setHeight: height  andOriginY: originY];

}


#pragma mark - Gesture action

-(void)pinchGesture:(OrigamiPinchGestureRecognizer*)gesture
{
	

	if( gesture.state >= UIGestureRecognizerStateEnded )
	{
		if( gesture.state == UIGestureRecognizerStateEnded || gesture.state == UIGestureRecognizerStateCancelled )
		{
		}

		//		pinchingPadIndexPath_ = nil;
		//		origamiCell_ = nil;
		//		pinchingPadHeight_ = 0;
		//		
		//		[self.tableView closeOrigamiLayer];
		//		[self.tableView reloadData];
		//		return;
		
		if( pinchingPadIndexPath_ )
		{
			NSIndexPath *indexPath = pinchingPadIndexPath_;
			__block OrigamiTableView __weak *tableView = self;

			NSArray* visibleCells = self.visibleCells;
			
			pinchingPadIndexPath_ = nil;
			
			
			BOOL closing = YES;
			
			if( gesture.initialDistance - 20 > gesture.distance )
			{
				if( gesture.pinchVelocity <= 2 )
					closing = YES;		
				else
					closing = NO;
			}
			else
			{
				closing = NO;
			}
			
			
			if( insertMode_ )
			{
				
				if( self.origamiExpandedHeight < self.origamiCurrentHeight )
				{
					closing = NO;
				}
				else if( gesture.pinchVelocity < 0 || self.origamiCurrentHeight < 10)
				{
					closing = YES;
				}
				else
				{
					closing = NO;
				}
			}
			
			
			if( gesture.state == UIGestureRecognizerStateCancelled ||  
				gesture.state == UIGestureRecognizerStateFailed )
			{
				if( insertMode_ )
					closing = YES;
				else
					closing = NO;
			}
			
			
			
			if( closing )
			{
				CGPoint contentOffset = self.contentOffset;
				
				if( !insertMode_ )
				{
					// Tell delegate

					if( [self.origamiDelegate respondsToSelector:@selector(origamiTableView:didCollapseCellsInRange:)] )
					{
					
						[self.origamiDelegate origamiTableView: self
											didCollapseCellsInRange: rowsBetweenFingers_];
					}

				}
				

				self.contentOffset = contentOffset;
				
				
				[self reloadAndCloseVisibleCells:ANIMATION_DURATION 
										alignBottom:NO
										withCompletion: ^(BOOL finished){
					
					pinchingPadHeight_ = 0;
					[tableView adjustContentOffset:YES];

				}];
				
			}
			else
			{
				if( insertMode_ )
				{
					// Tell delegate
					
					if( [self.origamiDelegate respondsToSelector:@selector(origamiTableView:willInsertRowAtIndexPath:)] )
					{
						[self.origamiDelegate origamiTableView: self
										willInsertRowAtIndexPath: indexPath];
					}
				}
				
				
				
				[self reloadAndExpandFrom: indexPath 
										 length: rowsBetweenFingers_.length
								 visibleCells: visibleCells
							 scrollToVisible: NO
							  withCompletion: ^(BOOL finished){
								  
								  pinchingPadHeight_ = 0;
								  pinchingPadIndexPath_ = nil;
								  
								  [tableView adjustContentOffset:YES];
								  
								  
								  if( insertMode_ )
								  {
									  // Tell delegate
									  
									  if( [self.origamiDelegate respondsToSelector:@selector(origamiTableView:didInsertRowAtIndexPath:)] )
									  {
										  [self.origamiDelegate origamiTableView: self
															 didInsertRowAtIndexPath: indexPath];
									  }
								  }
								  
							  }];
				
			}
			
		}
		
		return;
	}	
	
	
	
	if(  !pinchingPadIndexPath_ && 
		( gesture.state == UIGestureRecognizerStateBegan || 
		 gesture.state == UIGestureRecognizerStateChanged ))
	{
		
				
		//CGFloat rowHeight = self.rowHeight;

		CGPoint topLocation = gesture.topLocation;
		CGPoint bottomLocation = gesture.bottomLocation;
		
		
		topLocation.y += self.contentOffset.y;
		bottomLocation.y += self.contentOffset.y;
		
		
		NSIndexPath *pinchingTopIndexPath = [self indexPathForRowAtPoint: topLocation];
		NSIndexPath *pinchingBottomIndexPath = [self indexPathForRowAtPoint: bottomLocation];
		
		if( !pinchingTopIndexPath && !pinchingBottomIndexPath )
			return;
		
		if( [pinchingTopIndexPath isEqual: pinchingBottomIndexPath] )
			return;	
		
		if( pinchingTopIndexPath.section != pinchingBottomIndexPath.section )
			return;	
		
		
		
		if( pinchingTopIndexPath && pinchingBottomIndexPath )
		{
			rowsBetweenFingers_ = NSMakeRange(pinchingTopIndexPath.row + 1, pinchingBottomIndexPath.row - pinchingTopIndexPath.row -1);
			
		}else if( pinchingBottomIndexPath )
		{
			rowsBetweenFingers_ = NSMakeRange(0, pinchingBottomIndexPath.row);
		}else 
		{
			NSUInteger numberOfCells = [self.dataSource tableView:self
										numberOfRowsInSection:pinchingBottomIndexPath.section];
			
			rowsBetweenFingers_ = NSMakeRange(pinchingTopIndexPath.row + 1,
														 numberOfCells - pinchingTopIndexPath.row - 1);
			
		}
		
		
		
		insertMode_ =  ( rowsBetweenFingers_.length == 0) ? YES:NO;
		
		
		

		
		//挿入セルの作成
		
		NSMutableArray* cells = [[NSMutableArray alloc] init];
		NSMutableArray* subCells = nil;

		if( insertMode_ )
		{
			NSIndexPath* indexPath = [NSIndexPath indexPathForRow:rowsBetweenFingers_.location
																	  inSection:0];
			UITableViewCell* aCell = [self.origamiDelegate origamiTableView: self
															 canInsertingRowAtIndexPath: indexPath];
			
			if( !aCell )
			{
				pinchingPadIndexPath_ = nil;
				[self reloadData];

				return;	
			}
			
			CGRect frame = aCell.frame;
			frame.origin = CGPointZero;
			frame.size.width = self.bounds.size.width;
			aCell.frame = frame;
			[aCell setNeedsLayout];
			[cells addObject:aCell];
			
		}else
		{
			BOOL canCollapse = [self.origamiDelegate origamiTableView: self 
														 canCollapseCellsInRange: rowsBetweenFingers_];
			
			if( !canCollapse )
			{
				pinchingPadIndexPath_ = nil;
				[self reloadData];
				return;
			}
			
			
			NSUInteger numberOfCells = rowsBetweenFingers_.length;
			subCells = [[NSMutableArray alloc] init];
				
			CGFloat originY = 0;
			
			for( NSUInteger hoge = 0 ; hoge < numberOfCells; hoge++ )
			{
				NSIndexPath* indexPath = [NSIndexPath indexPathForRow:hoge + rowsBetweenFingers_.location inSection:0];
				
				// Look for visible cells
				
				UITableViewCell* aCell = nil;
				
				aCell = [self cellForRowAtIndexPath:indexPath];
				
				if( !aCell )
				{
					aCell = [self.origamiDelegate origamiTableView:self cellForRowAtIndexPath:indexPath];
				}
				
				UITableViewCell* collapsingCell = [self.origamiDelegate origamiTableView:self collapsingCellForRowAtIndexPath:indexPath inRange:rowsBetweenFingers_];

				[cells addObject: aCell];
				[subCells addObject: collapsingCell];

				CGFloat rowHeight = self.rowHeight;
				
				if( [self.origamiDelegate respondsToSelector:@selector( origamiTableView: heightForRowAtIndexPath:) ] )
				{
					rowHeight = [self.origamiDelegate origamiTableView:self heightForRowAtIndexPath:indexPath];
				}
				
				aCell.frame = CGRectMake(0, originY, self.bounds.size.width, rowHeight);
				collapsingCell.frame = CGRectMake(0, originY, self.bounds.size.width, rowHeight);

				originY += rowHeight;
			}
			
		
				
		}
		
		// Footer view
		
		
		NSMutableArray* footerViews = [NSMutableArray array];
		CGFloat maxHeight = self.bounds.size.height ;
		NSUInteger numberOfRows = [self numberOfRowsInSection:0];
		CGFloat originY  = 0;
		
		for( NSUInteger hoge = NSMaxRange(rowsBetweenFingers_) ; originY < maxHeight ; hoge++ )
		{
			NSIndexPath* indexPath = [NSIndexPath indexPathForRow:hoge inSection:0];
			
			if( indexPath.row >= numberOfRows )
				break;
			
			
			UITableViewCell* aCell = nil;
			aCell = [self cellForRowAtIndexPath:indexPath];
			
			
			if( !aCell )
			{
				aCell = [self.origamiDelegate origamiTableView:self cellForRowAtIndexPath:indexPath];

			}
			
			CGFloat rowHeight = self.rowHeight;
			
			if( [self.origamiDelegate respondsToSelector:@selector( origamiTableView: heightForRowAtIndexPath:) ] )
			{
				rowHeight = [self.origamiDelegate origamiTableView: self 
													heightForRowAtIndexPath: indexPath];
			}

			
			aCell.frame = CGRectMake(0, originY, self.bounds.size.width, rowHeight);

			originY += aCell.frame.size.height;
			
			[footerViews addObject: aCell];
			
			
			if( originY > maxHeight ) break;
			
		}
		
		
		CALayer* footerLayer = [self layerForCells: footerViews alignBottom: NO];
		
		
		//ピンチインデックスを設定
		
		
		pinchingPadIndexPath_ = [NSIndexPath indexPathForRow:rowsBetweenFingers_.location
																 inSection:0];
		
		
		//セクションに分割される。
		[self reloadData];
		
		CGFloat origamiY = 0;
		
		CGRect frame = [self rectForSection:1];
		origamiY = CGRectGetMaxY(frame);
		
		BOOL flag = [self setupAndAddOrigamiLayerWithCells: cells
																subCells: subCells
																  expand: !insertMode_
																	  toY: origamiY];
		
		if( !flag )
		{
			
			pinchingPadIndexPath_ = nil;
			[self reloadData];
			return;
		
		}
		

		footerLayer.frame = CGRectMake(0, 0, origamiLayer_.bounds.size.width, self.bounds.size.height);

		origamiLayer_.footerLayer = footerLayer;
		

	}
	
	
	if( insertMode_ )
		pinchingPadHeight_ = gesture.distance - gesture.initialDistance;
	
	else
		pinchingPadHeight_ = gesture.distance - gesture.initialDistance + self.origamiExpandedHeight;
	
	
	CGFloat topMovement = gesture.topLocation.y - gesture.initialTopLocation.y;
	
	
	CGPoint initialContentOffset = gesture.initialContentOffset;
	CGPoint currentContentOffset = self.contentOffset;
	CGPoint newContentOffset = CGPointMake( currentContentOffset.x, 
														initialContentOffset.y - topMovement);
	
	
	
	//[self reloadData];
	[self setContentOffset: newContentOffset];
	

	// Adjust location and height
	
	CGRect frame = [self rectForSection: 1];
	
	[self setOrigamiHeight: MAX( 0, pinchingPadHeight_) 
								 andOriginY: frame.origin.y];
	
	
	
	if( gesture.initialDistance - 20 > gesture.distance )
	{
		
		[self setSubImageHidden:NO];
		
		
	}else
	{
		[self setSubImageHidden:YES];
		
	}
	
}

- (BOOL)origamiCanPanForTouch:(UITouch *)touch
{
	if( self.disablePan ) return NO;
	
	CGPoint location = [touch locationInView:self];
	NSIndexPath *indexPath = [self indexPathForRowAtPoint: location];
	
	BOOL onTop = ( indexPath && indexPath.row == 0 );
	BOOL onBottom = (indexPath.row == [self numberOfRowsInSection:0]-1 );
	BOOL onBottomOutside = ( location.y > self.contentSize.height );
	
	BOOL flag =  (onTop || onBottom || onBottomOutside );
	
	
	return flag;
	
	
	return YES;
}

-(void)setEnableSystemGestureRecognizers:(BOOL)flag
{

	self.panGestureRecognizer.enabled = flag;

//	NSArray* gestures = [self gestureRecognizers];
//
//	for( UIGestureRecognizer *gesture in gestures )
//	{
//		if( [gesture isKindOfClass:NSClassFromString(@"UIScrollViewDelayedTouchesBeganGestureRecognizer")] )
//		{
//			gesture.enabled = flag;
//		}
//	}
	
	/*
	UIScrollViewDelayedTouchesBeganGestureRecognizer
	UIScrollViewPanGestureRecognizer
	UISwipeGestureRecognizer
	UIGobblerGestureRecognizer
	OrigamiPinchGestureRecognizer
	OrigamiPanGestureRecognizer
	 */
}



-(void)origamiCancelsPanning
{
	[self setEnableSystemGestureRecognizers:YES];
	pinchingPadIndexPath_ = nil;
	pinchingPadHeight_ = 0;

	[self closeOrigamiLayer];
	
	//Reloadをすると、UILongPressエラーになる
	//[self reloadData];
	
}

-(void)indent:(UISwipeGestureRecognizer*)gesture
{
	CGPoint location = [gesture locationInView:self];
	NSIndexPath *indexPath = [self indexPathForRowAtPoint: location];
	
	if( [self.origamiDelegate respondsToSelector:@selector(origamiTableView:indentAtIndexPath:withAllSiblings:)] )
	{
		[self.origamiDelegate origamiTableView: self
									indentAtIndexPath: indexPath
									  withAllSiblings: NO];
	}
}


-(void)unindent:(UISwipeGestureRecognizer*)gesture
{
	CGPoint location = [gesture locationInView:self];
	NSIndexPath *indexPath = [self indexPathForRowAtPoint: location];
	
	
	if( [self.origamiDelegate respondsToSelector:@selector(origamiTableView:unindentAtIndexPath:withAllSiblings:)] )
	{
		[self.origamiDelegate origamiTableView: self
								 unindentAtIndexPath: indexPath
									  withAllSiblings: NO];
	}
}



-(void)doubleUnindent:(UISwipeGestureRecognizer*)gesture
{
	CGPoint location = [gesture locationInView:self];
	NSIndexPath *indexPath = [self indexPathForRowAtPoint: location];
	
	
	if( [self.origamiDelegate respondsToSelector:@selector(origamiTableView:unindentAtIndexPath:withAllSiblings:)] )
	{
		[self.origamiDelegate origamiTableView: self
								 unindentAtIndexPath: indexPath
									  withAllSiblings: YES];
	}
}


- (void)panGesture:(OrigamiPanGestureRecognizer *)gesture 
{
	
	// ピンチされた場合は、UIGestureRecognizerStateCancelledになる
	
	CGPoint location = [gesture locationInView:self];
	NSIndexPath *indexPath = [self indexPathForRowAtPoint: location];
	
	BOOL onTop = ( indexPath && indexPath.row == 0 );
	BOOL onBottom = (indexPath.row == [self numberOfRowsInSection:0]-1 );
	BOOL onBottomOutside =NO;// ( location.y > self.contentSize.height );
	
	
	
	
	if( gesture.state >= UIGestureRecognizerStateEnded )
	{
		if( gesture.state == UIGestureRecognizerStateEnded || gesture.state == UIGestureRecognizerStateCancelled || gesture.state == UIGestureRecognizerStateFailed )
		{
		}
		
		[self setEnableSystemGestureRecognizers:YES];
		
		//		pinchingPadIndexPath_ = nil;
		//		origamiCell_ = nil;
		//		pinchingPadHeight_ = 0;
		//		
		//		[self.tableView closeOrigamiLayer];
		//		[self.tableView reloadData];
		//		return;
		
		if( pinchingPadIndexPath_ )
		{
			NSIndexPath *indexPath = pinchingPadIndexPath_;
			__block OrigamiTableView __weak *tableView = self;
			
			NSArray* visibleCells = self.visibleCells;
			
			pinchingPadIndexPath_ = nil;
			
			
			BOOL closing = YES;
			
			if( ABS( [gesture velocity] ) < 5 )
			{
				closing = NO;
			}

			if( self.origamiCurrentHeight < self.origamiExpandedHeight )
				closing = YES;
			
				
			if( closing )
			{
				
				[self reloadAndCloseVisibleCells: ANIMATION_DURATION 
											alignBottom: onBottom
										withCompletion: ^(BOOL finished){
					
					
					if( onTop )
						self.contentOffset = CGPointZero;

					if( onBottom )
					{
						[tableView adjustContentOffset:NO];
					}
					//[tableView adjustContentOffset:YES];
					
					
					pinchingPadHeight_ = 0;

				}];
				
			}
			else
			{
				
				// Tell delegate
				[self.origamiDelegate origamiTableView: self
									willInsertRowAtIndexPath: indexPath];
				
				
				[self reloadAndExpandFrom: indexPath 
										 length: rowsBetweenFingers_.length
								 visibleCells: visibleCells
							 scrollToVisible: NO
							  withCompletion: ^(BOOL finished){
								  
								  pinchingPadHeight_ = 0;
								  pinchingPadIndexPath_ = nil;
								  
								  [tableView adjustContentOffset:YES];
								  
								  
								  [self.origamiDelegate origamiTableView: self
													 didInsertRowAtIndexPath: indexPath];
							  }];
				
			}
			
		}
		
		return;
	}	
	
	
	if(  !pinchingPadIndexPath_ && 
		( gesture.state == UIGestureRecognizerStateBegan || 
		 gesture.state == UIGestureRecognizerStateChanged ))
	{
		if( !onTop && !onBottom && !onBottomOutside ) return;
			
		
		panningOnTop_ = onTop;
		panningOnBottom_ = onBottom;
		panningOnBottomOutside_ = onBottomOutside;
		
		
		[self setEnableSystemGestureRecognizers:NO];
		
		if( onTop )
		{
			rowsBetweenFingers_ = NSMakeRange(0, 0);
			
		}else 		
		{
			rowsBetweenFingers_ = NSMakeRange([self numberOfRowsInSection:0], 0);
		}
		
		
		
		insertMode_ = YES;
		
		
		
		//ピンチインデックスを設定
		
		
		pinchingPadIndexPath_ = [NSIndexPath indexPathForRow: rowsBetweenFingers_.location
																 inSection: 0];
		
		//セクションに分割される。
		[self reloadData];
		
		
		//挿入セルの作成
		
		NSMutableArray* cells = [[NSMutableArray alloc] init];
		NSMutableArray* subCells = nil;
		
		UITableViewCell* aCell = [self.origamiDelegate origamiTableView: self
														 canInsertingRowAtIndexPath: pinchingPadIndexPath_];
		
		if( !aCell )
		{
			pinchingPadIndexPath_ = nil;
			[self reloadData];
			
			return;	
		}
		
		[cells addObject:aCell];
		
		
		CGFloat origamiY = 0;
		
		CGRect frame = [self rectForSection:1];
		origamiY = CGRectGetMaxY(frame);
		
		BOOL flag = [self setupAndAddOrigamiLayerWithCells: cells
												subCells: subCells
												  expand: !insertMode_ 
													  toY: origamiY];
		
		if( !flag )
		{
			pinchingPadIndexPath_ = nil;
			[self reloadData];

			return;
		}
		
	}
	
	
	CGFloat movement = gesture.location.y - gesture.initialLocation.y ;
	
	if( panningOnBottomOutside_ )
		pinchingPadHeight_ = MAX(0, movement);
	

	if( panningOnTop_ )
	{
		
		if( movement > 0 )
		{
			CGFloat origamiOrigin = (panningOnTop_? -movement : origamiLayer_.frame.origin.y );

			pinchingPadHeight_ = MAX(0, ABS(movement) );
			
			CGPoint initialContentOffset = gesture.initialContentOffset;
			CGPoint currentContentOffset = self.contentOffset;
			CGPoint newContentOffset;
			
			newContentOffset = CGPointMake( currentContentOffset.x, 
													 initialContentOffset.y - movement);
			
			[self setContentOffset: newContentOffset];
			
			[self setOrigamiHeight: MAX( 0, pinchingPadHeight_) 
							andOriginY: origamiOrigin];
		}
		else
		{
			pinchingPadHeight_ = 0;
		}
		
	}

	
	if( panningOnBottom_ )
	{
		if( movement < 0 )
		{
			CGFloat origamiOrigin = (panningOnTop_? -movement : origamiLayer_.frame.origin.y );

			pinchingPadHeight_ = MAX(0, ABS(movement) );
			
			CGPoint initialContentOffset = gesture.initialContentOffset;
			CGPoint currentContentOffset = self.contentOffset;
			CGPoint newContentOffset;
			
			newContentOffset = CGPointMake( currentContentOffset.x, 
													 initialContentOffset.y - movement);
			
			[self setContentOffset: newContentOffset];
			
			[self setOrigamiHeight: MAX( 0, pinchingPadHeight_) 
							andOriginY: origamiOrigin];
			
		}else
		{
			pinchingPadHeight_ = 0;
		}
	}
	
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
	if( [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] )
	{
		UIPanGestureRecognizer* panGesture = (UIPanGestureRecognizer*)gestureRecognizer;
		
		CGPoint velocity = [panGesture velocityInView:self];
		
		if( ABS( velocity.x ) > ABS( velocity.y ) ) return NO;
	}
	
	
	return YES;
}
#pragma mark - Delegate & datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Return the number of sections.
	// ピンチ中はセクションに分割される
	if( pinchingPadIndexPath_ ) return 2;
	return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	//セクションヘッダの背景として、グレーを返す
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, pinchingPadHeight_)];
	
	view.backgroundColor = [self separatorColor];
	
	return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{

	if( ! pinchingPadIndexPath_ || section == 0 ) return 0;
	
	//セクション１は、ピンチパッドの高さを返す
		
	return pinchingPadHeight_; //dummy cell
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	// Return the number of rows in the section.
	
	NSUInteger rows = [self.origamiDelegate origamiTableView:self numberOfRowsInSection:section];
	
	if( !pinchingPadIndexPath_ ) return rows;
	
	
	// ピンチ中は、セクションが分割される
	
	
	if( section == 0 )
	{
		return pinchingPadIndexPath_.row;
	}
	// if section  == 1
	
	
	return rows - pinchingPadIndexPath_.row - rowsBetweenFingers_.length;
	
}


- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	NSIndexPath* modifiedIndexPath =nil;
	
	
	
	NSUInteger index = indexPath.row;
	
	if( indexPath.section == 1 )
	{
		index += pinchingPadIndexPath_.row + rowsBetweenFingers_.length;
	}
		

	modifiedIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
	
	
	UITableViewCell* cell = [self.origamiDelegate origamiTableView:self cellForRowAtIndexPath:modifiedIndexPath];
	
	
	return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self.origamiDelegate origamiTableView:self didSelectRowAtIndexPath:indexPath];
}

- (BOOL)tableView: (UITableView*) tableView canMoveRowAtIndexPath: (NSIndexPath*) indexPath
{
//	if( [self.origamiDelegate respondsToSelector:@selector(origamiTableView: canMoveRowAtIndexPath:)] )
//	{
//		return [self.origamiDelegate origamiTableView: self 
//										canMoveRowAtIndexPath: indexPath];
//	}
	
	return NO;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if( [self.origamiDelegate respondsToSelector:@selector(origamiTableView: editingStyleForRowAtIndexPath:)] )
	{
		return [self.origamiDelegate origamiTableView: self 
							 editingStyleForRowAtIndexPath: indexPath];
	}
	
	return UITableViewCellEditingStyleNone;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if( [self.origamiDelegate respondsToSelector:@selector(origamiTableView: commitDeletingRowAtIndexPath:)] )
	{
		[self.origamiDelegate origamiTableView: self 
									commitDeletingRowAtIndexPath: indexPath];
		
		//[self reloadData];
	}
	
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	if( [self.origamiDelegate respondsToSelector:@selector(origamiTableViewWillBeginDragging:)] )
	{
		[self.origamiDelegate origamiTableViewWillBeginDragging:self];
	}
		
}



-(void)setContentOffset:(CGPoint)offset
{
	[super setContentOffset: offset];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return [self.origamiDelegate origamiTableView: self heightForRowAtIndexPath:indexPath];
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if( [self.origamiDelegate respondsToSelector:@selector(origamiTableView: titleForDeleteConfirmationButtonForRowAtIndexPath:)] )
	{
		return [self.origamiDelegate origamiTableView: self 
					 titleForDeleteConfirmationButtonForRowAtIndexPath: indexPath];
		
	}
	
	return @"Delete";
}



@end

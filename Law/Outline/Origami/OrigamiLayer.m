//
//  OrigamiLayer.m
//  TableTest
//
//  Created by Nishikata Masatoshi on 2/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OrigamiLayer.h"
#import "OrigamiTransformLayer.h"

#define ACTIONS [[NSDictionary alloc] initWithObjectsAndKeys:\
[NSNull null], @"onOrderIn",\
[NSNull null], @"onOrderOut",\
[NSNull null], @"hidden",\
[NSNull null], @"sublayers",\
[NSNull null], @"contents",\
[NSNull null], @"bounds",\
[NSNull null], @"frame",\
[NSNull null], @"opacity",\
[NSNull null], @"position",\
[NSNull null], @"transform",\
nil]

@implementation OrigamiLayer

@synthesize numberOfFolds, footerLayer = footerLayer_;
@synthesize headerLayer = headerLayer_, originalHeight = originalHeight_;
@synthesize progress = progress_;
@synthesize subImage = subImage_, image = image_;

- (id)init
{
	self = [super init];
	if (self) {
		CATransform3D transform = CATransform3DIdentity;
		transform.m34 = -1.0/800.0;
		self.sublayerTransform = transform;
		self.backgroundColor = [UIColor colorWithWhite:0.2 alpha:1].CGColor;
		
		transformLayers_ = [[NSMutableArray alloc] init];
		self.actions = ACTIONS;
		
		self.contentsScale = [[UIScreen mainScreen] scale];

		self.opacity = 1.0;

	}
	return self;
}

-(void)dealloc
{

	headerLayer_ = nil;
	footerLayer_ = nil;
	image_ = nil;
	subImage_ = nil;
	[transformLayers_ removeAllObjects];
	transformLayers_ = nil;	
}


-(void)setSubImageHidden:(BOOL)flag
{
	
	for( OrigamiTransformLayer* layer in transformLayers_ )
	{
		if( !layer.subImageLayer ) return;
		
		layer.imageLayer.hidden = !flag;
		layer.subImageLayer.hidden = flag;
	}
}

- (void)setNumberOfFolds:(NSUInteger)folds
{

	for( CALayer* layer in transformLayers_ )
	{
		[layer removeFromSuperlayer];
	}
	[transformLayers_ removeAllObjects];
	
	
	numberOfFolds = folds;
	//setup rotation angle
	CGFloat endAngle;
	
	CGFloat frameWidth = self.bounds.size.width;
	CGFloat frameHeight = self.bounds.size.height;
	CGFloat foldHeight = frameHeight/(numberOfFolds*2);
	CALayer *prevLayer = self;
	
	CGPoint anchorPoint = CGPointMake(0.5, 0);
	
	originalHeight_ = frameHeight;
	

	for (int b=0; b < numberOfFolds*2; b++) 
	{
		CGRect imageFrame;
		
		if(b == 0)
			endAngle = -M_PI_2;
		else {
			if (b%2)
				endAngle = M_PI;
			else
				endAngle = -M_PI;
		}
		imageFrame = CGRectMake(0, b*foldHeight, frameWidth, foldHeight);
		
		OrigamiTransformLayer *transLayer = nil;
		transLayer = [OrigamiTransformLayer transformLayerFromImage: self.image
																			subImage: self.subImage 
																				frame: imageFrame 
																		anchorPiont: anchorPoint 
																		 startAngle: 0 
																			endAngle: endAngle 
																			  darker: numberOfFolds==1?YES:NO];

		[prevLayer addSublayer:transLayer]; //次々にビューを追加している。親、子、孫、、
		prevLayer = transLayer;
		
		[transformLayers_ addObject: transLayer];
		
		
	}
	
}

-(void)setFooterLayer:(CALayer *)footerLayer
{
	if( footerLayer_ )
	{
		[footerLayer_ removeFromSuperlayer];
	}
	
	footerLayer_ = footerLayer;
	
	if( footerLayer )
	{
		footerLayer_.actions = ACTIONS;
		footerLayer_.position = CGPointMake(0, self.bounds.size.height);
		
		[self addSublayer:footerLayer_];
	}
}

-(void)setHeaderLayer:(CALayer *)headerLayer
{
	if( headerLayer_ )
	{
		[headerLayer_ removeFromSuperlayer];
	}
	
	headerLayer_ = headerLayer;
	
	if( headerLayer )
	{
		headerLayer_.actions = ACTIONS;
		headerLayer_.position = CGPointMake(0, 0);

		[self addSublayer:headerLayer_];
	}
}

-(void)setHeight:(CGFloat)height
{
	// Convert height -> progress
	
	CGRect frame = self.frame;
	frame.size.height = height;
	
	[self setHeight:height andOriginY: frame.origin.y];

}

-(void)setHeight:(CGFloat)height andOriginY:(CGFloat)originY
{
	// Convert height -> progress
	
	CGRect frame = self.frame;
	frame.size.height = height;
	frame.origin.y = originY;
	self.frame = frame;
	
	if( height <= originalHeight_ )
	{
		CGFloat rad = acosf( height / originalHeight_);
		CGFloat progress = rad / M_PI_2;
		
		progress_ = progress;
		
		for( NSUInteger hoge = 0; hoge < transformLayers_.count; hoge++ )
		{
			OrigamiTransformLayer *layer = [transformLayers_ objectAtIndex:hoge];
			if( hoge == 0 )
			{
				CGPoint pos = layer.position;
				pos.y = 0;
				layer.position = pos;
				
			}
			
			[layer setProgress: progress_];
		}
		
		
		if( footerLayer_ )
		{
			CGFloat yy = self.bounds.size.height; 
			footerLayer_.position = CGPointMake(0, yy);
			
		}
		
		
		if( headerLayer_ )
		{
			headerLayer_.position = CGPointMake(0, 0);
		}
		
	}
	
	else
	{
		// Expand pad * 2
		CGFloat pad = (height -  originalHeight_) /2;
		
		
		CGRect frame = self.frame;
		frame.size.height = originalHeight_ + pad * 2;
		self.frame = frame;
		
		
		for( NSUInteger hoge = 0; hoge < transformLayers_.count; hoge++ )
		{
			OrigamiTransformLayer *layer = [transformLayers_ objectAtIndex:hoge];
			
			[layer setProgress: 0];
			
			if( hoge == 0 )
			{
				CGPoint pos = layer.position;
				pos.y = pad;
				layer.position = pos;
				
			}
		}
		
		
		
		
		if( footerLayer_ )
		{
			CGPoint pos = footerLayer_.position;
			pos.x = 0;
			pos.y = self.bounds.size.height;
			footerLayer_.position = pos;
		}
	}
	
	
	
}



-(CGFloat)height
{
	return self.frame.size.height;
}




-(void)setProgress:(CGFloat)newProgress
{
	
	progress_ = newProgress;
	
	
	CGRect frame = self.frame;
	frame.size.height = originalHeight_ * cos(progress_*M_PI_2);
	
	self.frame = frame;
	
	
	if( newProgress >= 0 )
	{
		
		for( NSUInteger hoge = 0; hoge < transformLayers_.count; hoge++ )
		{
			OrigamiTransformLayer *layer = [transformLayers_ objectAtIndex:hoge];
			if( hoge == 0 )
			{
				CGPoint pos = layer.position;
				pos.y = 0;
				layer.position = pos;
					
			}
			
			[layer setProgress: newProgress];
		}		
		
		
		if( footerLayer_ )
		{
			CGFloat yy = self.bounds.size.height;
			footerLayer_.position = CGPointMake(0, yy);
		}
		
		if( headerLayer_ )
		{
			headerLayer_.position = CGPointMake(0, 0);
		}
	}

	
	
}


@end

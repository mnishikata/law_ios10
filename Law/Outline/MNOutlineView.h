//
//  UIOutlineView.h
//  OutlineView
//
//  Created by Anton Sydorov on 5/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OutlineRowObject.h"

@protocol MNOutlineViewDataSource;
@protocol MNOutlineViewDelegate;
@protocol MNOutlineViewCellDelegate;
@class MNOutlineView;
@class MNOutlineViewCell;


@protocol MNOutlineViewCellDelegate <NSObject>

- (void) outlineViewCellExpand: (MNOutlineViewCell*) cell;
- (void) outlineViewCellCollapse: (MNOutlineViewCell*) cell;

@end

@interface MNOutlineViewCellContentView: UIView<UIGestureRecognizerDelegate>
{
@private
	BOOL                                expandable;
	BOOL                                expanded;
	BOOL                                moveable;
	
	
	UIButton*                           expandCollapseButton;
	
	CGRect										customizableFrame_;
	UIView*										/*__weak*/ customView_;
	
	BOOL	gripperIsOutside_;
	UIImageView*      						gripperView;
	
	
	id<MNOutlineViewCellDelegate>       /*__weak*/ cellDelegate;
	
}
+(void)setFlatTintColor:(UIColor*)color;
- (BOOL) canMoveFromPoint: (CGPoint) touchPoint;

@property (nonatomic, assign)   BOOL                                expandable;
@property (nonatomic, assign)   BOOL                                expanded;
@property (nonatomic, assign)   BOOL                                moveable;
//@property (nonatomic, readonly) UILabel*                            textLabel;
@property (nonatomic, weak) id<MNOutlineViewCellDelegate>         cellDelegate;
@property (nonatomic, weak) UIView*	customView;

@property (nonatomic, assign)   BOOL                                gripperIsOutside;

@end


@interface MNOutlineViewCell: UITableViewCell<MNOutlineViewCellDelegate>
{
@private
	MNOutlineViewCellContentView /*__weak*/ * cellContentView;
	id<MNOutlineViewCellDelegate>	 /*__weak*/ cellDelegate;
	UIView /*__weak*/ * backgroundView_;
	OutlineRowObject*  outlineRowObject_;
}

@property (nonatomic, assign) BOOL                                  expandable;
@property (nonatomic, assign) BOOL                                  expanded;
@property (nonatomic, assign) BOOL                                  moveable;
@property (nonatomic, weak) id<MNOutlineViewCellDelegate>         cellDelegate;

@property (nonatomic, retain) OutlineRowObject*  outlineRowObject;
@property (nonatomic, assign) UIView*	customView;
@property (nonatomic, weak) MNOutlineViewCellContentView*       cellContentView;

@property (nonatomic, assign) BOOL                                  showGripperOnlyWhenEditing;

- (void) prepareForMove;

- (BOOL) canMoveFromPoint: (CGPoint) touchPoint;
//-(void)setAdditionalIndent:(CGFloat)indent;

@end


#import "OrigamiTableView.h"


@interface MyCache : NSObject
{
	NSMutableArray* keyArray_;
	NSMutableArray* objectArray_;
}
-(id)objectForKey:(id)key;
-(void)setObject:(id)obj forKey:(id)key;
-(void)removeAllObjects;
@end

@interface MNOutlineView: UIView <OrigamiTableViewDelegate, UIGestureRecognizerDelegate, MNOutlineViewCellDelegate>
{
	IBOutlet id <MNOutlineViewDataSource>                dataSource;
	IBOutlet id <MNOutlineViewDelegate>                  delegate;
	
@private
	OrigamiTableView*                                    internalTableView;
	NSMutableDictionary*                                 itemsExpandStatus;
	
	NSIndexPath*                                         initialIndexPath;
	NSIndexPath*                                         movingIndexPath;
	NSInteger                                            initialItemLevel;
	CGPoint                                              touchOffset;
	UIView*                                              snapShotView;
	MNOutlineViewCellContentView*                        snapShotContentView;
	NSInteger                                            movedRowItemLevel;
	BOOL                                                 movedRowWasExpanded;
	id                                                   originalItemToMove;
	
	UILongPressGestureRecognizer*                        longPressGestureRecognizer;
	
	NSTimer*                                             autoscrollTimer;
	NSInteger                                            autoscrollDistance;
	NSInteger                                            autoscrollThreshold;
	
	BOOL canCollapse_;
	
	MyCache* numberOfRowsCache_;
	MyCache* rowHeightCache_;

	MyCache* temporaryRowHeightCache_;
	
	NSMutableArray* rowObjects_;
  
  UIColor* selectionBackgroundColor_;
	
	// Additional Implementation
	OutlineRowObject* previousOutlineRowObject_; // When nil, that means adding on top row
	
	BOOL editing_;
}
+(void)setFlatTintColor:(UIColor*)color;
-(void)reloadData;
-(void)setContentInset:(UIEdgeInsets)inset;
-(UIEdgeInsets)contentInset;
-(void)setScrollIndicatorInsets:(UIEdgeInsets)inset;
-(UIEdgeInsets)scrollIndicatorInsets;
-(void)setSelectionBackgroundColor:(UIColor *)backgroundColor;
-(void)setContentOffset:(CGPoint)offset;
-(void)setContentOffset:(CGPoint)offset animated:(BOOL)animated;
-(CGPoint)contentOffset;
-(void)flashScrollIndicators;
- (void)scrollRectToVisible:(CGRect)rect animated:(BOOL)animated;
-(CGSize)contentSize;
-(OutlineRowObject*)getOutlineRowObjectFromIndexPath:(NSIndexPath*)indexPath;
-(void)setupIndentOrigamiLayerWithCells:(NSArray*)cellArray toY: (CGFloat)pointY ;
-(void)setEditing:(BOOL)editing;
-(void)setEditing:(BOOL)editing animated:(BOOL)flag;
//-(OrigamiAnimation*)indentAnimationFirstHalf:(BOOL)unindent forCells:(NSArray*)cells ;
-(NSArray*)visibleCells;
- (OutlineRowObject*) rowObjectForPlainRow: (NSInteger) row;
- (NSInteger) calcTotalNumberOfRows;
-(void)tableViewWillReload:(NSNotification*)notification;
-(void)moveToLast:(BOOL)animated;

@property (nonatomic,assign)   id <MNOutlineViewDataSource> dataSource;
@property (nonatomic,assign)   id <MNOutlineViewDelegate>   delegate;
@property (nonatomic,readwrite)  BOOL editing;
@property (nonatomic,readwrite)  BOOL canCollapse;
@property (nonatomic,readonly)   CGSize contentSize;
@property (nonatomic,readwrite)  CGPoint contentOffset;
@property (nonatomic,readonly)  NSArray* visibleCells;
@property (nonatomic,readonly)  UITableView* tableView;
@property (nonatomic, assign) BOOL                                  showGripperOnlyWhenEditing;

@end




@protocol MNOutlineViewDataSource <NSObject>

- (id) outlineView: (MNOutlineView*) outlineView child: (NSInteger) index ofItem: (id) item;
- (BOOL) outlineView: (MNOutlineView*) outlineView isItemExpandable: (id) item;
- (BOOL) outlineView: (MNOutlineView*) outlineView isItemExpanded: (id) item;
- (NSInteger) outlineView: (MNOutlineView*) outlineView numberOfChildrenOfItem: (id) item;

@optional

- (id) outlineView: (MNOutlineView*) outlineView itemForPersistentObject: (id) object;
- (id) outlineView: (MNOutlineView*) outlineView persistentObjectForItem: (id) item;
- (BOOL) outlineView: (MNOutlineView*) outlineView isItemMoveable: (id) item;

@end



@protocol MNOutlineViewDelegate <NSObject>

- (UIView*) outlineView: (MNOutlineView*) outlineView viewForItem: (id) item forRowAtIndexPath:(NSIndexPath*)indexPath;

- (void) outlineView: (MNOutlineView*) outlineView moveItem: (id) item toItem: (id) parentItem atIndex: (NSInteger) index;

- (void) outlineView: (MNOutlineView*) outlineView setItem: (id) item expanded:(BOOL)flag;

@optional

- (BOOL) outlineView: (MNOutlineView*) outlineView shouldExpandItem: (id) item;
- (BOOL) outlineView: (MNOutlineView*) outlineView shouldCollapseItem: (id) item;
- (void) outlineView: (MNOutlineView*) outlineView didSelectItem: (id) item inCell:(MNOutlineViewCell*)cell atIndexPath:(NSIndexPath*)indexPath;


// Origami Delegate

- (void)outlineView:(MNOutlineView *)outlineView willInsertRowAtIndex:(NSUInteger)index ofItem:(id)parentItem;

- (void)outlineView:(MNOutlineView *)outlineView didInsertRowAtIndex:(NSUInteger)index ofItem:(id)parentItem;

- (NSInteger)outlineView:(MNOutlineView *)outlineView commitDeletionItem:(id)itemToDelete forIndexPath:(NSIndexPath*)indexPath;

-(id)outlineView:(MNOutlineView*)outlineView canInsertingRowAtIndex:(NSUInteger)index ofItem:(id)parentItem preferredRowHeight:(CGFloat*)rowHeight;
-(void)outlineViewWillBeginDragging:(MNOutlineView *)outlineView;
- (CGFloat)outlineView:(MNOutlineView *)outlineView heightForRowAtIndex:(NSUInteger)index ofItem:(id)parentItem;

- (NSString*)outlineView:(MNOutlineView *)outlineView titleForDeleteConfirmationButtonForItem:(id)item atIndexPath:(NSIndexPath*)indexPath;

-(BOOL)outlineView:(MNOutlineView*)outlineView canIndentAtIndex:(NSUInteger)index ofItem:(id)parentItem;
-(BOOL)outlineView:(MNOutlineView*)outlineView canUnindentAtIndex:(NSUInteger)index ofItem:(id)parentItem;

- (BOOL) outlineView: (MNOutlineView*) outlineView canItemAcceptSubitem: (id) item;


@end



//
//  OutlineRowObject.m
//  OutlineViewCoreDataTest
//
//  Created by Nishikata Masatoshi on 9/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OutlineRowObject.h"

@implementation OutlineRowObject
@synthesize item, itemLevel, index, parentRowObject, expanded;
-(void)dealloc
{

	self.item = nil;
	self.parentRowObject = nil;
	
}

-(BOOL)isChildOf:(OutlineRowObject*)object
{
	if( !self.parentRowObject ) return NO;
	if( self.itemLevel < object.itemLevel ) return NO;
	
	if( [self.parentRowObject.item isEqual: object.item] ) return YES;
	
	BOOL recursive = [self.parentRowObject isChildOf: object.item];
	return recursive;
}


-(BOOL)isEqual:(OutlineRowObject*)object
{
	if( ![object isKindOfClass:[self class]] ) return NO;
	
	if( self.index != object.index ) return NO;
	if( self.itemLevel != object.itemLevel ) return NO;
	if( self.expanded != object.expanded ) return NO;
	if( ![self.parentRowObject isEqual: object.parentRowObject] ) return NO;
	if( ![self.item isEqual: object.item] ) return NO;

	return YES;
}


@end

//
//  UIOutlineView.m
//  OutlineView
//
//  Created by Anton Sydorov on 5/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MNOutlineView.h"
#import <QuartzCore/QuartzCore.h>
#import "OrigamiAnimation.h"
#import "OrigamiTableView.h"

#define LEFT_INDENT 30

static const CGFloat MNOutlineViewCellContentViewExpandLeft     = 0.0;
static const CGFloat MNOutlineViewCellContentViewExpandTitleGap = 0.0;
static const CGFloat MNOutlineViewCellContentViewTouchArea      = 7.0;
static const CGFloat MNOutlineViewCellLevelStep                 = 30.0;
static const CGFloat MNOutlineViewCellGripperLeftGap            = 10.0;
static const CGFloat MNOutlineViewCellGripperRightGap           = 5.0;
static const CGFloat MNOutlineViewCellMoveAnimationDuration     = 0.2;
static const CGFloat MNOutlineViewCellLongPressDelay            = 0.01;
static const CGFloat MNOutlineViewReorderAnimationDuration      = 0.1;
static const CGFloat MNOutlineViewCellGripperWidth            = 30.0;

#define kLeftOffset 38
#define kDraggingWindowLeftMargin (internalTableView.editing?kLeftOffset:0)

@implementation MyCache

-(id)objectForKey:(id)key
{
	
	NSUInteger idx = [keyArray_ indexOfObjectIdenticalTo: key];
	
	if( idx  != NSNotFound )
		return [objectArray_ objectAtIndex: idx];
	
	
	return nil;
}

-(void)setObject:(id)obj forKey:(id)key
{
	if( !keyArray_ ) keyArray_ = [[NSMutableArray alloc] init];
	if( !objectArray_ ) objectArray_ = [[NSMutableArray alloc] init];
	
	NSUInteger idx = [keyArray_ indexOfObjectIdenticalTo: key];
	
	if( idx  != NSNotFound )
	{
		[objectArray_ replaceObjectAtIndex:idx withObject:obj];
		return;
	}
	
	[keyArray_ addObject: key];
	[objectArray_ addObject: obj];
}

-(void)removeAllObjects
{
	[keyArray_ removeAllObjects];
	[objectArray_ removeAllObjects];
}

@end


#pragma mark -
#pragma mark MNOutlineViewCellContentView

@interface MNOutlineViewCellContentView()
@end


@implementation MNOutlineViewCellContentView

@synthesize expandable;
@synthesize expanded;
@synthesize moveable;
@synthesize cellDelegate;
@synthesize customView = customView_;

@synthesize gripperIsOutside = gripperIsOutside_;

static UIImage* expandButtonImage = nil;
static UIImage* collapseButtonImage = nil;
//static UIImage* expandButtonImageH = nil;
//static UIImage* collapseButtonImageH = nil;

static UIImage* gripperImage = nil;

+(void)setFlatTintColor:(UIColor*)color
{
  if( color != nil ) {
    expandButtonImage = [[self colorizedImage: [UIImage imageNamed: @"MNOutlineUndisclosedFlat"] withTintColor: color] retain];
    collapseButtonImage = [[self colorizedImage: [UIImage imageNamed: @"MNOutlineDisclosedFlat"] withTintColor: color] retain];
    gripperImage = [[self colorizedImage: [UIImage imageNamed: @"MNOutlineGripperFlat"] withTintColor: color] retain];

  }else {
    expandButtonImage = nil;
    collapseButtonImage = nil;
    gripperImage = nil;
  }
}

+ (UIImage *)colorizedImage:(UIImage *)image withTintColor:(UIColor*)tintColor
{
  UIGraphicsBeginImageContextWithOptions(image.size, NO, [[UIScreen mainScreen] scale]);
  
  CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
  
  CGContextRef ctx = UIGraphicsGetCurrentContext();
  
  CGAffineTransform t = CGAffineTransformIdentity;
  t = CGAffineTransformScale(t, 1, -1);
  t = CGAffineTransformTranslate(t, 0, -rect.size.height);
  CGContextConcatCTM(ctx, t);
  
  CGContextSetFillColorWithColor(ctx, tintColor.CGColor);
  
  UIRectFill( rect );
  
  CGContextSetAlpha(ctx, 1.0);
  CGContextSetBlendMode(ctx, kCGBlendModeDestinationIn);
  CGContextDrawImage(ctx, rect, image.CGImage);
  
  UIImage* theImage = UIGraphicsGetImageFromCurrentImageContext();
  
  UIGraphicsEndImageContext();
  
  return theImage;
}

- (id) initWithFrame: (CGRect) frame
{
	if ((self = [super initWithFrame: frame]))
	{
		expandable = NO;
		expanded = YES;
		moveable = YES;
		cellDelegate = nil;
		self.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
		//self.userInteractionEnabled = YES;
		
		if (expandButtonImage == nil)
		{
			expandButtonImage = [[UIImage imageNamed: @"MNOutlineUndisclosed"] retain];
		}
		
		if (collapseButtonImage == nil)
		{
			collapseButtonImage = [[UIImage imageNamed: @"MNOutlineDisclosed"] retain];
		}
		
		//		 if (expandButtonImageH == nil)
		//		 {
		//			 expandButtonImageH = [[UIImage imageNamed: @"MNOutlineUndisclosedH.png"] retain];
		//		 }
		//
		//		 if (collapseButtonImageH == nil)
		//		 {
		//			 collapseButtonImageH = [[UIImage imageNamed: @"MNOutlineDisclosedH.png"] retain];
		//		 }
		
		if (gripperImage == nil)
		{
			gripperImage = [[UIImage imageNamed: @"MNOutlineGripper"] retain];
		}
		
		expandCollapseButton = [UIButton buttonWithType: UIButtonTypeCustom];
		expandCollapseButton.hidden = YES;
		[expandCollapseButton addTarget: self
										 action: @selector(onExpandOrCollapse:)
							forControlEvents: UIControlEventTouchUpInside];
		[self addSubview: expandCollapseButton];
		
		gripperView = [[[UIImageView alloc] initWithImage: gripperImage] autorelease];
		gripperView.userInteractionEnabled = NO;
		gripperView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
		[self addSubview: gripperView];
		
		customizableFrame_ = CGRectMake(0, 0, frame.size.width, frame.size.height);
		
		self.opaque = NO;
	}
	
	return self;
}

-(void)dealloc
{
	[super dealloc];
}

//- (void) touchesBegan: (NSSet*) touches withEvent: (UIEvent*) event
//{
//    UITouch* touch = [touches anyObject];
//    CGRect touchArea = CGRectInset(gripperView.frame, -MNOutlineViewCellContentViewTouchArea,
//                                   -MNOutlineViewCellContentViewTouchArea);
//    if (!CGRectContainsPoint(touchArea, [touch locationInView: self]))
//    {
//        [super touchesBegan: touches withEvent: event];
//    }
//}


- (void) layoutSubviews
{
	[super layoutSubviews];
	
	CGFloat currentX = MNOutlineViewCellContentViewExpandLeft;
	
	expandCollapseButton.hidden = !expandable;
	[expandCollapseButton setImage: (expanded ? collapseButtonImage : expandButtonImage)
								 forState: UIControlStateNormal];
	
	//	[expandCollapseButton setImage: (expanded ? collapseButtonImageH : expandButtonImageH)
	//								 forState: UIControlStateHighlighted];
	
	
	//[expandCollapseButton sizeToFit];
	
	CGRect expBtnFrame = expandCollapseButton.frame;
	expBtnFrame.size.height = self.bounds.size.height;
	expBtnFrame.size.width = MNOutlineViewCellLevelStep;
	
	expBtnFrame.origin.x = currentX;
	expBtnFrame.origin.y = (int)((CGRectGetHeight(self.bounds) - CGRectGetHeight(expBtnFrame)) / 2.0);
	expandCollapseButton.frame = expBtnFrame;
	currentX += CGRectGetWidth(expBtnFrame) + MNOutlineViewCellContentViewExpandTitleGap;
	
	CGRect gripperRect = gripperView.frame;
	CGFloat gripperLeft = CGRectGetWidth(self.bounds) - MNOutlineViewCellGripperRightGap;
	gripperLeft -= CGRectGetWidth(gripperRect);
	gripperRect.origin.x = gripperLeft;
	
	if( gripperIsOutside_ )
	{
		gripperRect.origin.x += CGRectGetWidth(gripperRect) + MNOutlineViewCellGripperRightGap + 20;
	}
	
	gripperRect.origin.y = (int)((CGRectGetHeight(self.bounds) - CGRectGetHeight(gripperRect)) / 2.0) + 1;
	gripperView.frame = gripperRect;
	gripperView.hidden = (!moveable || gripperIsOutside_) ;
	
	CGFloat cutFromTotalWidth = CGRectGetWidth(gripperRect) + MNOutlineViewCellGripperRightGap;
	cutFromTotalWidth += MNOutlineViewCellGripperLeftGap;
	if (!moveable || gripperIsOutside_ )
	{
		cutFromTotalWidth = 10.0;
	}
	
	//    CGRect textFrame = textLabel.frame;
	//    textFrame.origin.x = currentX;
	//    textFrame.origin.y = 0;
	//    textFrame.size.width = CGRectGetWidth(self.bounds) - cutFromTotalWidth - currentX;
	//    textFrame.size.height = CGRectGetHeight(self.bounds);
	//    textLabel.frame = textFrame;
	
	customizableFrame_ = CGRectMake( currentX,
											  0,
											  CGRectGetWidth(self.bounds) - cutFromTotalWidth - currentX,
											  CGRectGetHeight(self.bounds));
	
	customView_.frame = customizableFrame_;
}

- (void) onExpandOrCollapse: (id) sender
{
	if (cellDelegate != nil)
	{
		if (expanded)
		{
			[cellDelegate outlineViewCellCollapse: nil];
		} else
		{
			[cellDelegate outlineViewCellExpand: nil];
		}
	}
}

- (void) setExpandable: (BOOL) anExpandable
{
	expandable = anExpandable;
	[self setNeedsLayout];
}

- (void) setExpanded: (BOOL) anExpanded
{
	expanded = anExpanded;
	[self setNeedsLayout];
}

- (void) setMoveable: (BOOL) aMoveable
{
	moveable = aMoveable;
	[self setNeedsLayout];
}

- (BOOL) canMoveFromPoint: (CGPoint) touchPoint
{
	if (moveable)
	{
		CGRect touchArea = CGRectInset(gripperView.frame, -MNOutlineViewCellContentViewTouchArea,
												 -MNOutlineViewCellContentViewTouchArea);
		return CGRectContainsPoint(touchArea, touchPoint);
	}
	
	return NO;
}

-(void)setCustomView:(UIView*)view
{
	if( customView_ == view ) return;
	
	[customView_ removeFromSuperview];
	customView_ = view;
	
	if( !view ) return;
	
	customView_.frame = customizableFrame_;
	customView_.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	
	[self addSubview: customView_];
}

@end

#pragma mark -
#pragma mark MNOutlineViewCell

@interface MNOutlineViewCell()
{
	UIImageView /*__weak*/ * separatorView_;
}
@end

@implementation MNOutlineViewCell

@dynamic expandable;
@dynamic expanded;
@dynamic moveable;
@synthesize outlineRowObject = outlineRowObject_;
@synthesize cellDelegate;
@synthesize cellContentView;

- (id) initWithStyle: (UITableViewCellStyle) style
     reuseIdentifier: (NSString*) reuseIdentifier
{
	if ((self = [super initWithStyle: style reuseIdentifier: reuseIdentifier]))
	{
		cellDelegate = nil;
		
		self.cellContentView = [[[MNOutlineViewCellContentView alloc] initWithFrame: CGRectMake(0, 0, self.contentView.bounds.size.width, 44)] autorelease];
		cellContentView.cellDelegate = self;
		cellContentView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
		[self.contentView addSubview: cellContentView];
		
		self.contentView.backgroundColor = [UIColor clearColor];
		self.contentScaleFactor = [[UIScreen mainScreen] scale];
		
		// Separator
		
		{
			CGFloat height = self.bounds.size.height;
			CGFloat width = self.bounds.size.width;
			
			UIImageView* line = [[[UIImageView alloc] initWithFrame:CGRectMake(LEFT_INDENT, height-1, width - LEFT_INDENT, 1.0 )] autorelease];
			line.backgroundColor = [UIColor clearColor];
			
			line.image = [UIImage imageNamed:@"OrigamiSeparator"];
      line.alpha = 0.7;
//			line.contentMode = UIViewContentModeCenter;
			
//			UIView* line2 = [[[UIView alloc] initWithFrame:CGRectMake(0, height-0, width, 1)] autorelease];
//			line2.backgroundColor = [UIColor colorWithRed: 1
//																 green: 1
//																  blue: 1
//																 alpha: 0.7];
			
			line.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
			
//			line2.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
			[self addSubview: line];
//			[self addSubview: line2];
			
			separatorView_ = line;
		}
		
		
	}
	
	return self;
}

- (void) dealloc
{
	[cellContentView removeFromSuperview];
	self.cellContentView = nil;
	
	[backgroundView_ removeFromSuperview];
	backgroundView_ = nil;
	
	[outlineRowObject_ release];
	
	
	[super dealloc];
}


- (void) layoutSubviews
{
	[super layoutSubviews];
	[self resizeSubviews];
	
}

-(void)resizeSubviews {
	
	
	CGRect contentFrame = self.contentView.bounds;
	contentFrame.origin.x = outlineRowObject_.itemLevel * MNOutlineViewCellLevelStep ;
	contentFrame.size.width -= contentFrame.origin.x;
	cellContentView.frame = contentFrame;
	
	
	CGFloat height = self.bounds.size.height;
	CGFloat width = self.bounds.size.width;
	CGRect frame = CGRectMake( LEFT_INDENT + outlineRowObject_.itemLevel * MNOutlineViewCellLevelStep, height - 1, width - LEFT_INDENT, 1.0 );
	separatorView_.frame = frame;
	
	if( self.showGripperOnlyWhenEditing )
	{
		self.moveable = self.editing;
	}
}

//-(void)setAdditionalIndent:(CGFloat)indent
//{
//	CGRect contentFrame = self.contentView.bounds;
//	contentFrame.origin.x = outlineRowObject_.itemLevel * MNOutlineViewCellLevelStep + indent;
//	
//	contentFrame.size.width -= contentFrame.origin.x;
//	
//	cellContentView.frame = contentFrame;
//
//}


-(void)prepareForReuse
{
	[super prepareForReuse];
	cellContentView.gripperIsOutside = NO;
}

- (BOOL) expandable
{
	return cellContentView.expandable;
}

- (void) setExpandable: (BOOL) expandable
{
	cellContentView.expandable = expandable;
}

- (BOOL) expanded
{
	return cellContentView.expanded;
}

- (void) setExpanded: (BOOL) expanded
{
	cellContentView.expanded = expanded;
}

- (BOOL) moveable
{
	return cellContentView.moveable;
}

- (void) setMoveable: (BOOL) moveable
{
	cellContentView.moveable = moveable;
}

//- (UILabel*) textLabel
//{
//    return cellContentView.textLabel;
//}

-(void)setCustomView:(UIView*)view
{
	cellContentView.customView = view;
}

-(UIView*)customView
{
	return cellContentView.customView;
}

- (void) prepareForMove
{
	self.customView = nil;
	self.expandable = NO;
	self.moveable = NO;
	self.highlighted = NO;
}

- (BOOL) canMoveFromPoint: (CGPoint) touchPoint
{
	if (self.moveable)
	{
		return [cellContentView canMoveFromPoint: [self convertPoint: touchPoint toView: cellContentView]];
	}
	
	return NO;
}


- (void)willTransitionToState:(UITableViewCellStateMask)state
{
	[super willTransitionToState:state];
	
	if( self.showGripperOnlyWhenEditing )
	{
		cellContentView.gripperIsOutside = !(state & UITableViewCellStateShowingEditControlMask);

	}else
	{
	cellContentView.gripperIsOutside = state & UITableViewCellStateShowingEditControlMask;
	}
}

- (void)didTransitionToState:(UITableViewCellStateMask)state
{
	[super didTransitionToState:state];
	
	if( self.showGripperOnlyWhenEditing )
	{
		cellContentView.gripperIsOutside = !(state & UITableViewCellStateShowingEditControlMask);
		
	}else
	{
		cellContentView.gripperIsOutside = state & UITableViewCellStateShowingEditControlMask;
	}	
}

//- (void)setEditing:(BOOL)editing animated:(BOOL)animated
//{
//	if( editing )
//	{
//		[cellContentView setMoveable: NO];
//		[cellContentView setExpanded: NO];
//	}else {
//		[cellContentView setMoveable: self.moveable];
//		[cellContentView setExpanded: self.expandable];
//
//	}
//
//	[super setEditing:editing animated:animated];
//}

#pragma mark -
#pragma mark MNOutlineViewCellDelegate

- (void) outlineViewCellExpand: (MNOutlineViewCell*) cell
{
	if (cellDelegate != nil)
	{
		[cellDelegate outlineViewCellExpand: self];
	}
}

- (void) outlineViewCellCollapse: (MNOutlineViewCell*) cell
{
	if (cellDelegate != nil)
	{
		[cellDelegate outlineViewCellCollapse: self];
	}
}

#pragma mark - Drawing

-(void)mn_drawContentsInContext:(CGContextRef)context
{
	
	CGContextSaveGState(context);
	
	CGContextSetShadowWithColor(context, CGSizeZero, 0, [UIColor clearColor].CGColor);
	
	// Draw Separator
	CGFloat height = self.bounds.size.height;
	CGFloat width = self.bounds.size.width;
	
	UIColor* color = [UIColor colorWithRed: 120/255.0f
												green: 120/255.0f
												 blue: 120/255.0f
												alpha: 0.4];
	
	CGContextSetStrokeColorWithColor(context, color.CGColor);
	
	CGFloat lineWidth = 1.0 / self.contentScaleFactor;
	
	CGContextSetLineWidth(context, lineWidth );
	
	CGPoint points[2];
	
	points[0] = CGPointMake(LEFT_INDENT + outlineRowObject_.itemLevel * MNOutlineViewCellLevelStep, height - lineWidth/2.0);
	points[1] = CGPointMake(width, height - lineWidth/2.0);
	
	CGContextStrokeLineSegments(context, points, 2);
	
	color = [UIColor colorWithRed: 1
									green: 1
									 blue: 1
									alpha: 0.7];
	
	CGContextSetStrokeColorWithColor(context, color.CGColor);
	
	points[0] = CGPointMake(0, height + 0.5);
	points[1] = CGPointMake(width, height + 0.5);
	
//	CGContextStrokeLineSegments(context, points, 2);
	
	CGContextConcatCTM(context, CGAffineTransformMakeTranslation(outlineRowObject_.itemLevel * MNOutlineViewCellLevelStep + self.contentView.frame.origin.x, 0));
	[cellContentView.layer renderInContext: context];

	CGContextRestoreGState(context);
}

@end


#pragma mark -
#pragma mark UIOutlineView

@interface MNOutlineView()

- (void) doInit:(CGRect)frame;
- (NSInteger) calcNumberOfRowsForItem: (id) item;
- (NSInteger) calcTotalNumberOfRows;

- (OutlineRowObject*) rowObjectForPlainRow: (NSInteger) row;

- (id) itemForPersistentObject: (id) object;
- (id) persistentObjectForItem: (id) item;

- (BOOL) itemIsCollapsed: (id) item;
- (void) item: (id) item setCollapsed: (BOOL) collapsed;

- (void) moveRowToLocation: (CGPoint) location;

- (void) onLongPress: (UIGestureRecognizer*) sender;

- (CGFloat) autoscrollDistanceForProximityToEdge: (CGFloat) proximity;
- (void) normalizeAutoscrollDistance;
- (void) autoscrollTimerFired: (NSTimer*) timer;
- (void) stopAutoscrolling;
- (void) checkAutoscroll;

//- (void)origamiTableView:(OrigamiTableView *)tableView indentAtIndexPath:(NSIndexPath *)indexPath withAllSiblings:(BOOL)flag;
//- (void)origamiTableView:(OrigamiTableView *)tableView unindentAtIndexPath:(NSIndexPath *)indexPath withAllSiblings:(BOOL)flag;

-(OrigamiAnimation*)indentAnimation:(BOOL)unindent forLayer:(CALayer*)layerBeforeReload;


@end


@implementation MNOutlineView

@synthesize  dataSource;
@synthesize  delegate;
@synthesize editing = editing_;
@synthesize canCollapse = canCollapse_;
@synthesize tableView = internalTableView;

- (id) initWithFrame: (CGRect) frame
{
	if ((self = [super initWithFrame: frame]))
	{
		// Initialization code
		[self doInit:CGRectMake(0,0,frame.size.width,frame.size.height)];
	}
	return self;
}

- (id) initWithCoder: (NSCoder*) aDecoder
{
	if ((self = [super initWithCoder: aDecoder]))
	{
		// Initialization code
		[self doInit:self.bounds];
	}
	return self;
}

+(void)setFlatTintColor:(UIColor*)color {
  [MNOutlineViewCellContentView setFlatTintColor: color];
}

- (void) doInit:(CGRect)frame
{
	initialIndexPath = nil;
	movingIndexPath = nil;
	snapShotView = nil;
	snapShotContentView = nil;
	autoscrollTimer = nil;
	autoscrollDistance = 0;
	autoscrollThreshold = 0;
	canCollapse_ = YES;
	
	rowObjects_ = [[NSMutableArray alloc] init];
	rowHeightCache_ = [[MyCache alloc] init];
	numberOfRowsCache_ = [[MyCache alloc] init];

	temporaryRowHeightCache_ = [[MyCache alloc] init];
	
	internalTableView = [[OrigamiTableView alloc] initWithFrame: frame];
	internalTableView.origamiDelegate = self;
	internalTableView.rowHeight = 44;
	internalTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	internalTableView.allowsSelectionDuringEditing = YES;
	internalTableView.backgroundColor = self.backgroundColor;
	internalTableView.disablePan = YES;
	
	//internalTableView.delaysContentTouches = NO;
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tableViewWillReload:) name:@"OrigamiTableViewWillReloadNotificationName" object:internalTableView];
	
	[self addSubview: internalTableView];
	[internalTableView release];
	
	itemsExpandStatus = [[NSMutableDictionary alloc] init];
	
	longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget: self
																										action: @selector(onLongPress:)];
	longPressGestureRecognizer.minimumPressDuration = MNOutlineViewCellLongPressDelay;
	longPressGestureRecognizer.delegate = self;
	[internalTableView addGestureRecognizer: longPressGestureRecognizer];
	
	[longPressGestureRecognizer release];
}

- (void) dealloc
{
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[itemsExpandStatus release];
	[initialIndexPath release];
	[movingIndexPath release];
	[numberOfRowsCache_ removeAllObjects];
	[numberOfRowsCache_ release];
	[rowObjects_ removeAllObjects];
	[rowObjects_ release];
	[rowHeightCache_ removeAllObjects];
	[rowHeightCache_ release];
	
	[temporaryRowHeightCache_ removeAllObjects];
	[temporaryRowHeightCache_ release];
	[super dealloc];
}

-(void)tableViewWillReload:(NSNotification*)notification
{
	[numberOfRowsCache_ removeAllObjects];
	[rowObjects_ release];
	rowObjects_ = nil;
	[rowHeightCache_ removeAllObjects];
	[temporaryRowHeightCache_ removeAllObjects];

	
	[self buildRowObjects];
}

- (void) layoutSubviews
{
	[super layoutSubviews];
	
	internalTableView.frame = self.bounds;
}

-(void)setBackgroundColor:(UIColor *)backgroundColor
{
	internalTableView.backgroundColor = backgroundColor;
	[self reloadData];
}

-(void)setSelectionBackgroundColor:(UIColor *)backgroundColor
{
  [selectionBackgroundColor_ release];
  selectionBackgroundColor_ = [backgroundColor retain];
  [self reloadData];
}

-(void)setFrame:(CGRect)frame
{
	[super setFrame:frame];
	internalTableView.frame = self.bounds;
	
}

- (NSInteger) calcNumberOfRowsForItem: (id) parentItem
{
	
	NSInteger retVal = 1;
	
	if ([dataSource outlineView: self isItemExpandable: parentItem])
	{
		NSNumber* number = [numberOfRowsCache_ objectForKey: parentItem];
		if( number )
		{
			return [number integerValue];
		}
		
		NSInteger numberOfItems = [dataSource outlineView: self numberOfChildrenOfItem: parentItem];
		
		for (NSInteger itemIndex = 0; itemIndex < numberOfItems; itemIndex++)
		{
			id item = [dataSource outlineView: self child: itemIndex ofItem: parentItem];
			if (![self itemIsCollapsed: item])
			{
				retVal += [self calcNumberOfRowsForItem: item];
			} else
			{
				retVal += 1;
			}
		}
		
		[numberOfRowsCache_ setObject: [NSNumber numberWithInteger:retVal]
									  forKey: parentItem];
	}
	
	return retVal;
}


- (NSInteger) calcNumberOfRowsForExpandableItem: (id) parentItem
{
	
	NSInteger retVal = 1;
	
//	if ([dataSource outlineView: self isItemExpandable: parentItem])
	{
		NSNumber* number = [numberOfRowsCache_ objectForKey: parentItem];
		if( number )
		{
			return [number integerValue];
		}
		
		NSInteger numberOfItems = [dataSource outlineView: self numberOfChildrenOfItem: parentItem];
		
		for (NSInteger itemIndex = 0; itemIndex < numberOfItems; itemIndex++)
		{
			id item = [dataSource outlineView: self child: itemIndex ofItem: parentItem];
			if (![self itemIsCollapsed: item])
			{
				retVal += [self calcNumberOfRowsForItem: item];
			} else
			{
				retVal += 1;
			}
		}
		
		[numberOfRowsCache_ setObject: [NSNumber numberWithInteger:retVal]
									  forKey: parentItem];
	}
	
	return retVal;
}


- (NSInteger) calcTotalNumberOfRows
{
	NSInteger retVal = 0;
	
	if (dataSource != nil)
	{
		NSInteger numberOrRootItems = [dataSource outlineView: self numberOfChildrenOfItem: nil];
		
		for (NSInteger rootItemIndex = 0; rootItemIndex < numberOrRootItems; rootItemIndex++)
		{
			id rootItem = [dataSource outlineView: self child: rootItemIndex ofItem: nil];
			if (rootItem == nil)
			{
				// We should have item available
				@throw NSObjectNotAvailableException;
			} else
			{
				if (![self itemIsCollapsed: rootItem])
				{
					retVal += [self calcNumberOfRowsForItem: rootItem];
				} else
				{
					retVal += 1;
				}
			}
		}
	}
	
	return retVal;
}

-(void)moveToLast:(BOOL)animated
{
	[internalTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self calcTotalNumberOfRows]-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:animated];
}

#pragma mark - Additional methods

NSInteger gCount = 0;


-(void)buildRowObjects
{
	[rowObjects_ release];
	rowObjects_ = nil;
	
	rowObjects_ = [[NSMutableArray alloc] init];
	
	if( !dataSource ) return;

	[self buidRowObjectsForParentItem:nil rowObject:nil];
}

-(void)buidRowObjectsForParentItem:(id)item rowObject:(OutlineRowObject*)parentRowObject
{
	NSInteger numberOfItems = [dataSource outlineView: self numberOfChildrenOfItem: item];
	
	for (NSInteger itemIndex = 0; itemIndex < numberOfItems; itemIndex++)
	{
		OutlineRowObject* rowObj = [[[OutlineRowObject alloc] init] autorelease];
		
		id thisItem = [dataSource outlineView: self child: itemIndex ofItem: item];
		
		rowObj.item = thisItem;
		rowObj.itemLevel = (parentRowObject?parentRowObject.itemLevel+1:0);
		rowObj.parentRowObject = parentRowObject;
		rowObj.index = itemIndex;
		rowObj.expanded = ![self itemIsCollapsed: thisItem];
		
		[rowObjects_ addObject: rowObj];
		
		if( rowObj.expanded == YES ) {
			
			[self buidRowObjectsForParentItem: thisItem rowObject:rowObj];
		}
		
	}

}

- (OutlineRowObject*) rowObjectForPlainRow: (NSInteger) row
{
	if( !dataSource ) return nil;
	
	return rowObjects_[row];

}



#pragma mark -



- (id) itemForPersistentObject: (id) object
{
	id retVal = object;
	
	if (dataSource != nil)
	{
		if ([dataSource respondsToSelector: @selector(outlineView:itemForPersistentObject:)])
		{
			retVal = [dataSource outlineView: self itemForPersistentObject: object];
		}
	}
	
	return retVal;
}

- (id) persistentObjectForItem: (id) item
{
	id retVal = item;
	
	if (dataSource != nil)
	{
		if ([dataSource respondsToSelector: @selector(outlineView:persistentObjectForItem:)])
		{
			retVal = [dataSource outlineView: self persistentObjectForItem: item];
		}
	}
	
	return retVal;
}

- (BOOL) itemIsCollapsed: (id) item
{
	BOOL retVal = NO;
	

	
	if( [dataSource respondsToSelector:@selector(outlineView:isItemExpanded:)] )
	{
//		if( [itemCollapsedCache_ objectForKey: item] != nil ) {
//			return [(NSNumber*)[itemCollapsedCache_ objectForKey: item] boolValue];
//		}
		
		BOOL boolean = ![dataSource outlineView:self isItemExpanded:item];
//		[itemCollapsedCache_ setObject:[NSNumber numberWithBool:boolean] forKey:item];

		return boolean;
	}
	
	
	id persistentObject = [self persistentObjectForItem: item];
	NSNumber* status = (NSNumber*)[itemsExpandStatus objectForKey: persistentObject];
	if (status != nil)
	{
		retVal = ![status boolValue];
	}
	
	NSUInteger numberOfChildren = [dataSource outlineView: self numberOfChildrenOfItem: item];
	if( numberOfChildren == 0 ) return YES;
	
	return retVal;
}

- (void) item: (id) item setCollapsed: (BOOL) collapsed
{
	[temporaryRowHeightCache_ removeAllObjects];
	
	if( [delegate respondsToSelector:@selector(outlineView: setItem: expanded:)] )
	{
		[delegate outlineView: self setItem: item expanded: !collapsed];
		[numberOfRowsCache_ removeAllObjects];
		[self buildRowObjects];

		return;
	}
	
	id persistentObject = [self persistentObjectForItem: item];
	[itemsExpandStatus setObject: [NSNumber numberWithBool: !collapsed]
								 forKey: persistentObject];
	
	[numberOfRowsCache_ removeAllObjects];
	[self buildRowObjects];

}


- (void) moveRowToLocation: (CGPoint) location
{
	
	NSIndexPath *newIndexPath = [internalTableView indexPathForRowAtPoint:location];
	CGRect rectForTheIndexPath = [internalTableView rectForRowAtIndexPath:newIndexPath];
	
	// If touched location haven't passed the center of the new index path, ignore.
	if( [newIndexPath compare: movingIndexPath] != NSOrderedSame )
	{
		if( newIndexPath.row < movingIndexPath.row )
		{
			if( location.y < CGRectGetMidY(rectForTheIndexPath) )
				newIndexPath = movingIndexPath;
		}
		else
		{
			if( location.y < CGRectGetMidY(rectForTheIndexPath) )
				newIndexPath = movingIndexPath;
		}
	}
	
	
	// Analyze the new moving index path
	// 1. It's a valid index path
	// 2. It's not the current index path of the cell
	if ([newIndexPath section] != NSNotFound && [newIndexPath row] != NSNotFound
		 && [newIndexPath compare: movingIndexPath] != NSOrderedSame
		 )
	{
		if ([[internalTableView delegate] respondsToSelector:@selector(tableView:targetIndexPathForMoveFromRowAtIndexPath:toProposedIndexPath:)])
		{
			NSIndexPath *proposedDestinationIndexPath = [[internalTableView delegate] tableView: internalTableView
																	 targetIndexPathForMoveFromRowAtIndexPath: movingIndexPath
																								 toProposedIndexPath: newIndexPath];
			
			// If the delegate does not allow moving to the new index path cancel moving row
			if ([newIndexPath compare:proposedDestinationIndexPath] != NSOrderedSame) {
				return;
			}
		}
		
		[internalTableView beginUpdates];
		
		// Move the row
		if  (movingIndexPath != nil)
		{
			[internalTableView deleteRowsAtIndexPaths: [NSArray arrayWithObject: movingIndexPath]
											 withRowAnimation: UITableViewRowAnimationAutomatic];
		}
		
		if  (newIndexPath != nil)
		{
			[internalTableView insertRowsAtIndexPaths: [NSArray arrayWithObject: newIndexPath]
											 withRowAnimation: UITableViewRowAnimationAutomatic];
		}
		
		// Update the moving index path
		[movingIndexPath autorelease];
		movingIndexPath = [newIndexPath retain];
		[internalTableView endUpdates];
	}
}

-(void)reloadData
{
	[internalTableView reloadData];
}

-(void)setEditing:(BOOL)editing
{
	editing_ = editing;
	[internalTableView setEditing:editing animated:NO];
}

-(void)setEditing:(BOOL)editing animated:(BOOL)flag
{
	editing_ = editing;
	[internalTableView setEditing:editing animated:flag];

}

- (void) onLongPress: (UIGestureRecognizer*) sender
{
	
	
	switch (sender.state)
	{
		case UIGestureRecognizerStateBegan:
		{
			CGPoint touchPoint = [sender locationInView: internalTableView];
			NSIndexPath* touchedIndexPath = [internalTableView indexPathForRowAtPoint: touchPoint];
			
			MNOutlineViewCell* touchedCell = (MNOutlineViewCell*)[internalTableView cellForRowAtIndexPath: touchedIndexPath];
			originalItemToMove = [self itemForPersistentObject: touchedCell.outlineRowObject.item];
			[originalItemToMove retain];
			
			movedRowWasExpanded = NO;
			
			
			snapShotView = [[[UIView alloc] initWithFrame: touchedCell.frame] autorelease];
			snapShotView.backgroundColor = internalTableView.backgroundColor;
			snapShotView.layer.shadowOpacity = 0.7;
			snapShotView.layer.shadowOffset = CGSizeMake(0.0, 3.0);
			snapShotView.layer.shadowRadius = 6;
			if (touchedCell.expandable && touchedCell.expanded)
			{
				movedRowWasExpanded = YES;
				[self outlineViewCellCollapseOnSnapShotView: touchedCell];
				
			}
			
			[initialIndexPath autorelease];
			initialIndexPath = [touchedIndexPath retain];
			[movingIndexPath autorelease];
			movingIndexPath = [touchedIndexPath retain];
			
			touchOffset = CGPointMake(touchedCell.center.x - touchPoint.x, touchedCell.center.y - touchPoint.y);
			
			initialItemLevel = touchedCell.outlineRowObject.itemLevel;
			movedRowItemLevel = touchedCell.outlineRowObject.itemLevel;
			
			
			[internalTableView addSubview: snapShotView];
			
			movedRowItemLevel = touchedCell.outlineRowObject.itemLevel;
			CGRect contentRect = touchedCell.contentView.frame;
			contentRect.origin.x = movedRowItemLevel * MNOutlineViewCellLevelStep + kDraggingWindowLeftMargin;
			contentRect.size.width = CGRectGetWidth(snapShotView.bounds) - contentRect.origin.x - MNOutlineViewCellGripperWidth;
			
			snapShotContentView = [[MNOutlineViewCellContentView alloc] initWithFrame: contentRect];
			snapShotContentView.customView = [delegate outlineView: self viewForItem: touchedCell.outlineRowObject.item forRowAtIndexPath:nil];
			snapShotContentView.expandable = touchedCell.expandable;
			snapShotContentView.expanded = NO;
			snapShotContentView.moveable = NO;
			[snapShotContentView setNeedsLayout];
			
			[snapShotView addSubview: snapShotContentView];
			[snapShotContentView release];
			
			
			UIImageView* dummyGripper = [[[UIImageView alloc] initWithImage: gripperImage] autorelease];
			
			CGRect gripperRect = dummyGripper.frame;
			CGFloat gripperLeft = CGRectGetWidth(snapShotView.bounds) - MNOutlineViewCellGripperRightGap;
			gripperLeft -= CGRectGetWidth(gripperRect);
			gripperRect.origin.x = gripperLeft;
			gripperRect.origin.y = (int)((CGRectGetHeight(snapShotView.bounds) - CGRectGetHeight(gripperRect)) / 2.0) + 1;
			dummyGripper.frame = gripperRect;
			[snapShotView addSubview:dummyGripper];
			
			//			MNOutlineViewCellContentView* gripperView = [[MNOutlineViewCellContentView alloc] initWithFrame: touchedCell.contentView.frame];
			//			gripperView.expandable = NO;
			//			gripperView.moveable = YES;
			//			[snapShotView addSubview: gripperView];
			//			[gripperView release];
			
			
			[touchedCell prepareForMove];
			
			autoscrollThreshold = (NSInteger)(CGRectGetHeight(snapShotView.bounds) * 0.6);
			autoscrollDistance = 0;
			
			internalTableView.allowsSelection = NO;
			
			
			if( [delegate respondsToSelector:@selector(outlineViewWillBeginDragging:)] )
			{
				[delegate outlineViewWillBeginDragging: self];
			}
			
			break;
		}
			
		case UIGestureRecognizerStateChanged:
		{
			CGPoint touchPoint = [sender locationInView: internalTableView];
			
			
			
			[self checkAutoscroll];
			
			// If the table view does not scroll, compute a new index path for the moving cell
			if (autoscrollDistance == 0)
			{
				[self moveRowToLocation: touchPoint];
			}
			
			
			
			CGPoint snapCenter = snapShotView.center;
			snapCenter.y = touchPoint.y + touchOffset.y;
			snapShotView.center = snapCenter;
			
			NSInteger maxItemLevel = 0;
			NSInteger minItemLevel = 0;
			
			if (movingIndexPath.row > 0)
			{
				NSIndexPath* prevCellPath = [NSIndexPath indexPathForRow: movingIndexPath.row - 1
																			  inSection: 0];
				MNOutlineViewCell* prevCell = (MNOutlineViewCell*)[internalTableView cellForRowAtIndexPath: prevCellPath];
				
				BOOL canAccept = YES;
				
				if( [self.delegate respondsToSelector:@selector(outlineView:canItemAcceptSubitem:)] )
				{
					canAccept = [self.delegate outlineView:self canItemAcceptSubitem:prevCell.outlineRowObject.item];
				}
				
				
				if( canAccept )
				{
					maxItemLevel = prevCell.outlineRowObject.itemLevel + 1;
				}
				else
				{
					maxItemLevel = prevCell.outlineRowObject.itemLevel;
				}
				
				[previousOutlineRowObject_ autorelease];
				previousOutlineRowObject_ = [prevCell.outlineRowObject retain];
				
			}else
			{
				[previousOutlineRowObject_ autorelease];
				previousOutlineRowObject_ = nil;
			}
			
			
			if (movingIndexPath.row < [self calcTotalNumberOfRows] - 1)
			{
				NSIndexPath* nextCellPath = [NSIndexPath indexPathForRow: movingIndexPath.row + 1
																			  inSection: 0];
				MNOutlineViewCell* nextCell = (MNOutlineViewCell*)[internalTableView cellForRowAtIndexPath: nextCellPath];
				
				minItemLevel = nextCell.outlineRowObject.itemLevel;
			}
			
			NSInteger possibleLevelsCount = ABS(maxItemLevel - minItemLevel);
			CGRect cellRect = [internalTableView rectForRowAtIndexPath: movingIndexPath];
			CGFloat positionInRow = (CGRectGetMaxY(cellRect) - snapCenter.y) / CGRectGetHeight(cellRect);
			
			NSInteger newItemLevel = (int)(((positionInRow * ((CGFloat)possibleLevelsCount)) + minItemLevel) + 0.5);
			
			newItemLevel = MAX( 0, newItemLevel );
			
			if (newItemLevel != movedRowItemLevel)
			{
				movedRowItemLevel = newItemLevel;
				
				[UIView animateWithDuration: MNOutlineViewReorderAnimationDuration
									  animations: ^
				 {
					 CGRect contentRect = snapShotContentView.frame;
					 contentRect.origin.x = newItemLevel * MNOutlineViewCellLevelStep + kDraggingWindowLeftMargin;
					 contentRect.size.width = CGRectGetWidth(snapShotView.bounds) - contentRect.origin.x - MNOutlineViewCellGripperWidth;
					 snapShotContentView.frame = contentRect;
				 }];
			}
			
			
			//[self moveRowToLocation: touchPoint];
			
			
			break;
		}
			
		case UIGestureRecognizerStateEnded:
		{
			[self stopAutoscrolling];
			
			CGRect finalRect = [internalTableView rectForRowAtIndexPath: movingIndexPath];
			[UIView animateWithDuration: MNOutlineViewCellMoveAnimationDuration
								  animations: ^
			 {
				 snapShotView.frame = finalRect;
			 }
								  completion: ^(BOOL finished)
			 {
				 if (finished)
				 {
					 //					 NSLog(@"previousOutlineRowObject_ %@", previousOutlineRowObject_.);
					 
					 [snapShotView removeFromSuperview];
					 snapShotView = nil;
					 
					 if ((initialIndexPath.row != movingIndexPath.row) || (initialItemLevel != movedRowItemLevel))
					 {
						 
						 if ([delegate respondsToSelector: @selector(outlineView:moveItem:toItem:atIndex:)])
						 {
							 
							 NSUInteger itemLevel = previousOutlineRowObject_.itemLevel;
							 
							 if( !previousOutlineRowObject_ )
							 {
								 [delegate outlineView: self
												  moveItem: originalItemToMove
													 toItem: nil
													atIndex: 0];
							 }
							 
							 // Drop in
							 else if( itemLevel < movedRowItemLevel )
							 {
								 
								 // "Dropping animation" when parent is closed
								 NSUInteger numberOfChildern = [dataSource outlineView: self
																		  numberOfChildrenOfItem: previousOutlineRowObject_.item];
								 
								 CGPoint contentOffsetBeforeReload = internalTableView.contentOffset;
								 
								 [delegate outlineView: self
												  moveItem: originalItemToMove
													 toItem: previousOutlineRowObject_.item
													atIndex: 0];
								 
								 
								 if( !previousOutlineRowObject_.expanded && numberOfChildern > 0 )
								 {
									 CGPoint contentOffsetAfterReload = internalTableView.contentOffset;
									 internalTableView.contentOffset = contentOffsetBeforeReload;
									 
									 // Setup "Closing" cell
									 MNOutlineViewCell *cell = [[[MNOutlineViewCell alloc] initWithFrame: finalRect] autorelease];
									 OutlineRowObject* rowObj = [[[OutlineRowObject alloc] init] autorelease];
									 
									 rowObj.itemLevel = previousOutlineRowObject_.itemLevel + 1;
									 rowObj.item = originalItemToMove;
									 
									 cell.outlineRowObject = rowObj;
									 cell.customView = [delegate outlineView: self viewForItem: originalItemToMove forRowAtIndexPath:nil];
									 cell.expandable = [dataSource outlineView: self isItemExpandable: originalItemToMove];
									 cell.expanded = NO;
									 
									 // Animate and drop
									 
									 NSIndexPath* buffer = movingIndexPath;
									 movingIndexPath = nil;
									 
									 [internalTableView collapseCells: [NSArray arrayWithObject:cell]
																			toY: CGRectGetMinY(finalRect)
																  completion:^(BOOL finished){ }];
									 
									 movingIndexPath = buffer;
									 
									 
									 // This makes smooth dropping
									 [internalTableView setContentOffset:contentOffsetAfterReload animated:YES];
									 
								 }
							 }
							 
							 // Add as a sibling
							 else if( itemLevel == movedRowItemLevel )
							 {
								 
								 [delegate outlineView: self
												  moveItem: originalItemToMove
													 toItem: previousOutlineRowObject_.parentRowObject.item
													atIndex: previousOutlineRowObject_.index +1];
							 }
							 
							 // Upper hierarchy
							 else
							 {
								 
								 OutlineRowObject* parentRow = previousOutlineRowObject_;
								 OutlineRowObject* siblingRow = nil;
								 
								 NSInteger level = itemLevel - movedRowItemLevel;
								 
								 while( level >= 0  )
								 {
									 siblingRow = parentRow;
									 parentRow = parentRow.parentRowObject;
									 
									 level--;
								 }
								 
								 
								 [delegate outlineView: self
												  moveItem: originalItemToMove
													 toItem: parentRow.item
													atIndex: siblingRow.index +1];
							 }
							 
							 
						 }
						 
						 
					 }
					 
					 
					 NSIndexPath* finalIndexPath = movingIndexPath;
					 movingIndexPath = nil;
					 
					 [self reloadData];
					 
					 
					 //                     [internalTableView reloadRowsAtIndexPaths: [NSArray arrayWithObject: finalIndexPath]
					 //                                              withRowAnimation: UITableViewRowAnimationNone];
					 
					 
					 if (movedRowWasExpanded)
					 {
						 
						 MNOutlineViewCell* movedCell = (MNOutlineViewCell*)[internalTableView cellForRowAtIndexPath: finalIndexPath];
						 [self outlineViewCellExpand: movedCell];
					 }
					 
					 [initialIndexPath autorelease];
					 initialIndexPath = nil;
					 
					 [finalIndexPath autorelease];
					 
					 internalTableView.allowsSelection = YES;
				 }
				 
				 [originalItemToMove release];
				 originalItemToMove = nil;
			 }];
			break;
		}
			
		default:
		{
			
			if (snapShotView != nil)
			{
				[snapShotView removeFromSuperview];
				snapShotView = nil;
			}
			
			if (movingIndexPath != nil)
			{
				NSIndexPath* finalIndexPath = movingIndexPath;
				movingIndexPath = nil;
				
				[internalTableView reloadRowsAtIndexPaths: [NSArray arrayWithObject: finalIndexPath]
												 withRowAnimation: UITableViewRowAnimationNone];
				
				if (movedRowWasExpanded)
				{
					MNOutlineViewCell* movedCell = (MNOutlineViewCell*)[internalTableView cellForRowAtIndexPath: finalIndexPath];
					[self outlineViewCellExpand: movedCell];
				}
				
				[finalIndexPath autorelease];
			}
			
			if (initialIndexPath != nil)
			{
				[initialIndexPath autorelease];
				initialIndexPath = nil;
			}
			
			internalTableView.allowsSelection = YES;
			
			if( originalItemToMove )
			{
				[originalItemToMove release];
				originalItemToMove = nil;
			}
			
			break;
		}
	}
}

#pragma mark -
#pragma mark Autoscrolling

- (void) checkAutoscroll
{
	autoscrollDistance = 0;
	
	
	UIEdgeInsets inset = internalTableView.contentInset;
	
	
	if (CGRectIntersectsRect(snapShotView.frame, internalTableView.bounds))
	{
		CGPoint touchLocation = [longPressGestureRecognizer locationInView: internalTableView];
		touchLocation.y += touchOffset.y;
		
		CGFloat distanceToTopEdge  = touchLocation.y - CGRectGetMinY(internalTableView.bounds) - inset.top;
		CGFloat distanceToBottomEdge = CGRectGetMaxY(internalTableView.bounds) - inset.bottom - touchLocation.y;
		
		if (distanceToTopEdge < autoscrollThreshold)
		{
			autoscrollDistance = [self autoscrollDistanceForProximityToEdge: distanceToTopEdge] * -1;
		}
		else if (distanceToBottomEdge < autoscrollThreshold)
		{
			autoscrollDistance = [self autoscrollDistanceForProximityToEdge: distanceToBottomEdge];
		}
	}
	
	if (autoscrollDistance == 0)
	{
		[autoscrollTimer invalidate];
		autoscrollTimer = nil;
	}
	else if (!autoscrollTimer)
	{
		autoscrollTimer = [NSTimer scheduledTimerWithTimeInterval: (1.0 / 60.0)
																			target: self
																		 selector: @selector(autoscrollTimerFired:)
																		 userInfo: nil
																		  repeats: YES];
	}
}


- (CGFloat) autoscrollDistanceForProximityToEdge: (CGFloat) proximity
{
	return ceilf((autoscrollThreshold - proximity) / 5.0);
}


- (void) normalizeAutoscrollDistance
{
	UIEdgeInsets inset = internalTableView.contentInset;

	if (internalTableView.contentSize.height <= CGRectGetHeight(internalTableView.bounds) - inset.top - inset.bottom)
	{
		autoscrollDistance = 0;
		return;
	}
	
	CGFloat minDistance = internalTableView.contentOffset.y * -1.0  - inset.top;
	CGFloat maxDistance = internalTableView.contentSize.height;
	maxDistance -= (CGRectGetHeight(internalTableView.bounds) - inset.bottom + internalTableView.contentOffset.y);
	
	autoscrollDistance = MAX(autoscrollDistance, minDistance);
	autoscrollDistance = MIN(autoscrollDistance, maxDistance);
}


- (void) autoscrollTimerFired: (NSTimer*) timer
{
	[self normalizeAutoscrollDistance];
	
	CGPoint contentOffset = [internalTableView contentOffset];
	contentOffset.y += autoscrollDistance;
	[internalTableView setContentOffset: contentOffset];
	
	// Move the snap shot appropriately
	CGPoint snapShotCenter = snapShotView.center;
	snapShotCenter.y += autoscrollDistance;
	snapShotView.center = snapShotCenter;
	
	// Even if we autoscroll we need to update the moved cell's index path
	CGPoint touchLocation = [longPressGestureRecognizer locationInView: internalTableView];
	[self moveRowToLocation: touchLocation];
}


- (void) stopAutoscrolling
{
	autoscrollDistance = 0;
	
	if (autoscrollTimer != nil)
	{
		[autoscrollTimer invalidate];
		autoscrollTimer = nil;
	}
}

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger) origamiTableView: (OrigamiTableView*) tableView numberOfRowsInSection: (NSInteger) section
{
	return [self calcTotalNumberOfRows];
}

- (UITableViewCell*) origamiTableView: (OrigamiTableView*) tableView cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
	static NSString* CellIdentifier = @"Cell";
	MNOutlineViewCell* cell = (MNOutlineViewCell*)[tableView dequeueReusableCellWithIdentifier: CellIdentifier];
	if (cell == nil)
	{
		cell = [[[MNOutlineViewCell alloc] initWithStyle: UITableViewCellStyleDefault
                                     reuseIdentifier: CellIdentifier] autorelease];
    
  }
  if( selectionBackgroundColor_ != nil ) {
      UIView* view = [[UIView alloc] init];
      view.backgroundColor = selectionBackgroundColor_;
      cell.selectedBackgroundView = view;

  }else {
    cell.selectedBackgroundView = nil;
  }
	cell.backgroundColor = tableView.backgroundColor;
  cell.showsReorderControl = NO;
	cell.showGripperOnlyWhenEditing = self.showGripperOnlyWhenEditing;

	
	// Convert table row to data index (plain row)
	NSInteger currentRowIndex = indexPath.row;
	if (movingIndexPath != nil)
	{
		if ( movingIndexPath.row < initialIndexPath.row )
		{
			if( indexPath.row > movingIndexPath.row && indexPath.row <= initialIndexPath.row )
			{
				currentRowIndex--;
			}
		}
		
		
		if ( movingIndexPath.row > initialIndexPath.row )
		{
			if( indexPath.row >= initialIndexPath.row && indexPath.row < movingIndexPath.row )
			{
				currentRowIndex++;
			}
		}
		
		if( movingIndexPath.row == indexPath.row )
		{
			currentRowIndex = initialIndexPath.row;
		}
	}
	
	
	OutlineRowObject* rowObject = [self rowObjectForPlainRow: currentRowIndex];
	
	id item = rowObject.item;
	
	if (item != nil)
	{
		if ((movingIndexPath == nil) || ([indexPath compare: movingIndexPath] != NSOrderedSame))
		{
			cell.customView = [delegate outlineView: self viewForItem: item forRowAtIndexPath:indexPath];
			cell.expandable = [dataSource outlineView: self isItemExpandable: item];
			cell.cellDelegate = self;
			cell.outlineRowObject = rowObject;
			cell.expanded = ![self itemIsCollapsed: item];
			cell.moveable = YES;
			
			if( [dataSource respondsToSelector:@selector(outlineView:isItemMoveable:)] )
			{
				cell.moveable = [dataSource outlineView:self isItemMoveable: item];
			}
			
			if( self.showGripperOnlyWhenEditing )
			{
				cell.moveable = NO;
			}
			
		} else
		{
			[cell prepareForMove];
		}
	}
	
	
	cell.editing = internalTableView.editing;
	[cell layoutIfNeeded];

	return cell;
}

//- (BOOL) origamiTableView: (OrigamiTableView*) tableView canMoveRowAtIndexPath: (NSIndexPath*) indexPath
//{
//	return YES;
//}

#pragma mark -
#pragma mark UITableViewDelegate

- (void) origamiTableView: (OrigamiTableView*) tableView didSelectRowAtIndexPath: (NSIndexPath*) indexPath
{
	
	[tableView deselectRowAtIndexPath: indexPath animated: YES];
	
	if ((delegate != nil) && (movingIndexPath == nil))
	{
		if ([delegate respondsToSelector: @selector(outlineView:didSelectItem:inCell:atIndexPath:)])
		{
			MNOutlineViewCell* cell = (MNOutlineViewCell*)[tableView cellForRowAtIndexPath: indexPath];
			if (cell != nil)
			{
				
				id item = [self itemForPersistentObject: cell.outlineRowObject.item];
				[delegate outlineView: self didSelectItem: item inCell:cell atIndexPath:indexPath];
			}
		}
	}
}

- (UITableViewCellEditingStyle)origamiTableView:(OrigamiTableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if( editing_ ) return UITableViewCellEditingStyleDelete;
	
	return UITableViewCellEditingStyleNone;
}




//
//- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
//
//}
//
//- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
//    return NO;
//}


#pragma mark -
#pragma mark MNOutlineViewCellDelegate

- (void) outlineViewCellExpand: (MNOutlineViewCell*) cell
{
	
	BOOL shouldExpand = YES;
	if (delegate != nil)
	{
		if ([delegate respondsToSelector: @selector(outlineView:shouldExpandItem:)])
		{
			id item = [self itemForPersistentObject: cell.outlineRowObject.item];
			shouldExpand = [delegate outlineView: self shouldExpandItem: item];
		}
	}
	
	if (shouldExpand && (movingIndexPath == nil))
	{
		cell.expanded = YES;
		
		NSIndexPath* indexPath = [internalTableView indexPathForCell: cell];
		if (indexPath != nil)
		{
			
			NSMutableArray* paths = [NSMutableArray array];
			id item = [self itemForPersistentObject: cell.outlineRowObject.item];
			
			
			NSInteger rowsToInsert = [self calcNumberOfRowsForItem: item] - 1;
			
			rowsToInsert = MIN( rowsToInsert, (int)ceilf( self.bounds.size.height / internalTableView.rowHeight ) );
			
			
			for (NSInteger i = 0; i < rowsToInsert; i++)
			{
				NSIndexPath* indexToDelete = [NSIndexPath indexPathForRow: indexPath.row + i + 1
																				inSection: 0];
				[paths addObject: indexToDelete];
			}
			
			[self item: item setCollapsed: NO];
			
			
			
			
			
			CGFloat pointY = 0;
			BOOL customRowHeight = [self.delegate respondsToSelector:@selector( outlineView: heightForRowAtIndex:ofItem: ) ];
			CGFloat rowHeight = internalTableView.rowHeight;
			
			NSMutableArray* cells = [NSMutableArray array];
			for( NSUInteger hoge = 0; hoge < paths.count; hoge++ )
			{
				NSIndexPath* indexPathForCell = [paths objectAtIndex:hoge];
				
				MNOutlineViewCell* cell = (MNOutlineViewCell*)[self origamiTableView: internalTableView
																			  cellForRowAtIndexPath: indexPathForCell];
				
				
				
				if( customRowHeight )
				{
					OutlineRowObject *rowObj = [self rowObjectForPlainRow:indexPathForCell.row];
					
					NSNumber* cachedHeight = [rowHeightCache_ objectForKey: rowObj];
					if( cachedHeight )
					{
						rowHeight = [cachedHeight floatValue];
						
					}else
					{
						
						rowHeight = [self.delegate outlineView:self
												 heightForRowAtIndex:rowObj.index
																  ofItem:rowObj.parentRowObject.item];
						
						[rowHeightCache_ setObject:[NSNumber numberWithFloat:rowHeight] forKey:rowObj];
					}
				}
				
				
				cell.frame = CGRectMake(kDraggingWindowLeftMargin, pointY, internalTableView.bounds.size.width, rowHeight);
				[cell resizeSubviews];
				

				
//				[cell setAdditionalIndent:kLeftOffset];
				
				[cells addObject: cell];
				pointY += rowHeight;
				
			}
			
			
			[internalTableView expandCells: cells
											  from: indexPath
									  completion: ^(BOOL finished) { }];
			
			
			//            [self item: item setCollapsed: NO];
			//
			//            [internalTableView insertRowsAtIndexPaths: paths
			//                                     withRowAnimation: UITableViewRowAnimationBottom];
		}
		
		
		if( [delegate respondsToSelector:@selector(outlineViewWillBeginDragging:)] )
		{
			[delegate outlineViewWillBeginDragging: self];
		}
	}
}

- (void) outlineViewCellCollapse: (MNOutlineViewCell*) cell
{
	BOOL shouldCollapse = YES;
	id item = [self itemForPersistentObject: cell.outlineRowObject.item];
	
	if (delegate != nil)
	{
		if ([delegate respondsToSelector: @selector(outlineView:shouldCollapseItem:)])
		{
			shouldCollapse = [delegate outlineView: self shouldCollapseItem: item];
		}
	}
	
	shouldCollapse &= ![self itemIsCollapsed: item];
	
	if (shouldCollapse && (movingIndexPath == nil))
	{
		cell.expanded = NO;
		
		NSIndexPath* indexPath = [internalTableView indexPathForCell: cell];
		if (indexPath != nil)
		{
			NSMutableArray* paths = [NSMutableArray array];
			NSInteger rowsToDelete = [self calcNumberOfRowsForItem: item] - 1;
			
			rowsToDelete = MIN( rowsToDelete,  self.bounds.size.height / internalTableView.rowHeight  );
			
			for (NSInteger i = 0; i < rowsToDelete; i++)
			{
				NSIndexPath* indexToDelete = [NSIndexPath indexPathForRow: indexPath.row + i + 1
																				inSection: 0];
				[paths addObject: indexToDelete];
			}
			
			
			
			CGFloat pointY = 0;
			BOOL customRowHeight = [self.delegate respondsToSelector:@selector( outlineView: heightForRowAtIndex:ofItem: ) ];
			CGFloat rowHeight = internalTableView.rowHeight;
			
			
			NSMutableArray* cells = [NSMutableArray array];
			for( NSUInteger hoge = 0; hoge < paths.count; hoge++ )
			{
				NSIndexPath* indexPathForCell = [paths objectAtIndex:hoge];
				
				MNOutlineViewCell* cell = (MNOutlineViewCell*)[self origamiTableView: internalTableView
																			  cellForRowAtIndexPath: indexPathForCell];
				
				if( customRowHeight )
				{
					OutlineRowObject *rowObj = [self rowObjectForPlainRow:indexPathForCell.row];
					rowHeight = [self.delegate outlineView:self
											 heightForRowAtIndex:rowObj.index
															  ofItem:rowObj.parentRowObject.item];
				}
				
				
				cell.frame = CGRectMake(kDraggingWindowLeftMargin, pointY, internalTableView.bounds.size.width, rowHeight);
//				[cell setAdditionalIndent:kLeftOffset];

				[cell resizeSubviews];
				
				[cells addObject: cell];
				pointY += rowHeight;
				
			}
			
			CGFloat positionY = CGRectGetMaxY( [internalTableView rectForRowAtIndexPath: indexPath] );
			
			[self item: item setCollapsed: YES];
			
			[internalTableView collapseCells:cells
												  toY:positionY
										 completion:^(BOOL finished){}];
			
			//			  [self item: item setCollapsed: YES];
			
			//            [internalTableView deleteRowsAtIndexPaths: paths
			//                                     withRowAnimation: UITableViewRowAnimationTop];
		}
		
		
		if( [delegate respondsToSelector:@selector(outlineViewWillBeginDragging:)] )
		{
			[delegate outlineViewWillBeginDragging: self];
		}
	}
}

- (void) outlineViewCellCollapseOnSnapShotView: (MNOutlineViewCell*) cell
{
	BOOL shouldCollapse = YES;
	id item = [self itemForPersistentObject: cell.outlineRowObject.item];
	
	if (delegate != nil)
	{
		if ([delegate respondsToSelector: @selector(outlineView:shouldCollapseItem:)])
		{
			shouldCollapse = [delegate outlineView: self shouldCollapseItem: item];
		}
	}
	
	shouldCollapse &= ![self itemIsCollapsed: item];
	
	if (shouldCollapse && (movingIndexPath == nil))
	{
		cell.expanded = NO;
		
		NSIndexPath* indexPath = [internalTableView indexPathForCell: cell];
		if (indexPath != nil)
		{
			NSMutableArray* paths = [NSMutableArray array];
			NSInteger rowsToDelete = [self calcNumberOfRowsForItem: item] - 1;
			
			
			for (NSInteger i = 0; i < rowsToDelete; i++)
			{
				NSIndexPath* indexToDelete = [NSIndexPath indexPathForRow: indexPath.row + i + 1
																				inSection: 0];
				[paths addObject: indexToDelete];
			}
			
			
			CGFloat pointY = 0;
			BOOL customRowHeight = [self.delegate respondsToSelector:@selector( outlineView: heightForRowAtIndex:ofItem: ) ];
			CGFloat rowHeight = internalTableView.rowHeight;
			
			NSMutableArray* cells = [NSMutableArray array];
			for( NSUInteger hoge = 0; hoge < MIN( paths.count,  self.bounds.size.height / internalTableView.rowHeight  ); hoge++ )
			{
				NSIndexPath* indexPathForCell = [paths objectAtIndex:hoge];
				
				MNOutlineViewCell* cell = (MNOutlineViewCell*)[self origamiTableView: internalTableView
																			  cellForRowAtIndexPath: indexPathForCell];
				
				
				if( customRowHeight )
				{
					OutlineRowObject *rowObj = [self getOutlineRowObjectFromIndexPath:indexPathForCell];
					
					NSNumber* cachedHeight = [rowHeightCache_ objectForKey: rowObj];
					if( cachedHeight )
					{
						rowHeight = [cachedHeight floatValue];
						
					}else
					{
						
						rowHeight = [self.delegate outlineView:self
												 heightForRowAtIndex:rowObj.index
																  ofItem:rowObj.parentRowObject.item];
						
						[rowHeightCache_ setObject:[NSNumber numberWithFloat:rowHeight] forKey:rowObj];
					}
				}
				
				cell.frame = CGRectMake(kDraggingWindowLeftMargin, pointY, internalTableView.bounds.size.width, rowHeight);
//				[cell setAdditionalIndent:kLeftOffset];
				[cell resizeSubviews];
				
				[cells addObject: cell];
				pointY += rowHeight;
				
			}
			
			
			[self item: item setCollapsed: YES];
			
			[internalTableView deleteRowsAtIndexPaths: paths
											 withRowAnimation: UITableViewRowAnimationTop];
			
			
			[internalTableView collapseCells:cells
											  toView:snapShotView
										 completion:^(BOOL finished){}];
			
			
		}
	}
}


#pragma mark -
#pragma mark UIGestureRecognizerDelegate

- (BOOL) gestureRecognizer: (UIGestureRecognizer*) recognizer shouldReceiveTouch: (UITouch*) touch
{
	CGPoint touchPoint = [touch locationInView: internalTableView];
	NSIndexPath* touchedIndexPath = [internalTableView indexPathForRowAtPoint: touchPoint];
	
	if (touchedIndexPath != nil)
	{
		MNOutlineViewCell* touchedCell = (MNOutlineViewCell*)[internalTableView cellForRowAtIndexPath: touchedIndexPath];
		if (touchedCell != nil)
		{
			CGPoint pointInCell = [internalTableView convertPoint: touchPoint toView: touchedCell];
			return([touchedCell canMoveFromPoint: pointInCell]);
		}
	}
	
	return NO;
}

#pragma mark - Additional Origami Delegate


-(OutlineRowObject*)getOutlineRowObjectFromIndexPath:(NSIndexPath*)indexPath
{
	// return parent item
	
	OutlineRowObject* siblingRowObject = nil;
	OutlineRowObject* parentRowObject = nil;
	
	NSUInteger indexInSiblings = 0;
	
	OutlineRowObject* rowObj = [[[OutlineRowObject alloc] init] autorelease];
	
	if( indexPath.row == 0 )
	{
		siblingRowObject = nil;
		parentRowObject = nil;
	}
	else
	{
		NSUInteger siblingIndex = 0;
		siblingIndex = indexPath.row -1;
		siblingRowObject = [self rowObjectForPlainRow: siblingIndex];
		
		NSUInteger numberOfChildren = [dataSource outlineView: self numberOfChildrenOfItem: siblingRowObject.item];
		
		
		if( siblingRowObject.expanded && numberOfChildren > 0 )
		{
			parentRowObject = siblingRowObject;
			siblingRowObject = nil;
			indexInSiblings = 0;
		}
		else
		{
			parentRowObject = siblingRowObject.parentRowObject;
			indexInSiblings = siblingRowObject.index + 1;
			
		}
	}
	
	rowObj.index = indexInSiblings;
	
	if( parentRowObject )
		rowObj.itemLevel = parentRowObject.itemLevel + 1;
	else
		rowObj.itemLevel = 0;
	
	rowObj.parentRowObject = parentRowObject;
	
	return rowObj;
}

- (UITableViewCell*)origamiTableView:(OrigamiTableView *)tableView canInsertingRowAtIndexPath:(NSIndexPath *)indexPath;
{
	if( movingIndexPath ) return nil;
	
	if( self.editing ) return nil;
	
	if( ![self.delegate respondsToSelector:@selector( outlineView: canInsertingRowAtIndex: ofItem: preferredRowHeight:)] )
	{
		return nil;
	}
	
	MNOutlineViewCell* cell = nil;
	CGFloat rowHeight = internalTableView.rowHeight;
	
	OutlineRowObject* rowObj = [self getOutlineRowObjectFromIndexPath: indexPath];
	if( !rowObj ) return nil;
	
	
	id item = [self.delegate outlineView: self
					  canInsertingRowAtIndex: rowObj.index
											ofItem: rowObj.parentRowObject.item
							preferredRowHeight: &rowHeight];
	
	if( !item ) return nil;
	
	
	
	cell = [[[MNOutlineViewCell alloc] initWithStyle: UITableViewCellStyleDefault
												reuseIdentifier: nil] autorelease];
	
	cell.frame = CGRectMake(0, 0, internalTableView.bounds.size.width, rowHeight);
	
	
	cell.customView = [delegate outlineView: self viewForItem: item forRowAtIndexPath:indexPath];
	cell.expandable = NO;
	cell.cellDelegate = self;
	cell.outlineRowObject = rowObj;
	cell.expanded = NO;
	cell.moveable = YES;
	
	return cell;
}

- (void)origamiTableView:(OrigamiTableView*) tableView commitDeletingRowAtIndexPath:(NSIndexPath *)indexPath;
{
	
	
	if( [self.delegate respondsToSelector:@selector( outlineView: commitDeletionItem: forIndexPath:)] )
	{
		OutlineRowObject* rowObj = [self rowObjectForPlainRow: indexPath.row];
		
		NSInteger rowIncrement = [self.delegate outlineView: self
													commitDeletionItem: rowObj.item
															forIndexPath: indexPath];
		
		[self tableViewWillReload:nil];
		
		
		NSMutableArray* indexPaths = nil;
		
		if( rowIncrement == 0 )
		{
		}
		else if( rowIncrement < 0 )
		{
			
			indexPaths = [NSMutableArray arrayWithObject: indexPath];
			
			[internalTableView deleteRowsAtIndexPaths: indexPaths
											 withRowAnimation: UITableViewRowAnimationLeft];
			
			double delayInSeconds = 0.25;
			dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
			dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
				[self reloadData];
			});
			
			
		}
		else {
			
			indexPaths = [NSMutableArray array];
			
			for( NSUInteger hoge = 1; hoge < rowIncrement ; hoge++ )
			{
				NSIndexPath* anIndexPath = [NSIndexPath indexPathForRow: indexPath.row + hoge
																			 inSection: 0];
				
				[indexPaths addObject: anIndexPath];
			}
			
			[internalTableView insertRowsAtIndexPaths: indexPaths
											 withRowAnimation: UITableViewRowAnimationMiddle];
		}
		
	}
}

- (void)origamiTableView:(OrigamiTableView *)tableView willInsertRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	OutlineRowObject* rowObj = [self getOutlineRowObjectFromIndexPath: indexPath];
	
	if( [self.delegate respondsToSelector:@selector( outlineView: willInsertRowAtIndex: ofItem: )] )
	{
		
		[self.delegate outlineView: self
				willInsertRowAtIndex: rowObj.index
								  ofItem: rowObj.parentRowObject.item];
	}
	
	
}

- (void)origamiTableView:(OrigamiTableView *)tableView didInsertRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	OutlineRowObject* rowObj = [self getOutlineRowObjectFromIndexPath: indexPath];
	
	if( [self.delegate respondsToSelector:@selector( outlineView: didInsertRowAtIndex: ofItem: )] )
	{
		
		[self.delegate outlineView: self
				 didInsertRowAtIndex: rowObj.index
								  ofItem: rowObj.parentRowObject.item];
	}
	
	
}



- (CGFloat)origamiTableView:(OrigamiTableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
   NSInteger currentRowIndex = indexPath.row;
	if (movingIndexPath != nil)
	{
		if ( movingIndexPath.row < initialIndexPath.row )
		{
			if( indexPath.row > movingIndexPath.row && indexPath.row <= initialIndexPath.row )
			{
				currentRowIndex--;
			}
			
		}
		
		
		if ( movingIndexPath.row > initialIndexPath.row )
		{
			if( indexPath.row >= initialIndexPath.row && indexPath.row < movingIndexPath.row )
			{
				currentRowIndex++;
			}
			
		}
		
		
		if( movingIndexPath.row == indexPath.row )
		{
			currentRowIndex = initialIndexPath.row;
		}
		
	}
	
	NSNumber *num = [temporaryRowHeightCache_ objectForKey: @(currentRowIndex)];
	if( num )
	{
		//		NSLog(@"has cache %d",currentRowIndex);
		return num.floatValue;
	}
	
	
	OutlineRowObject *rowObj = [self rowObjectForPlainRow: currentRowIndex];
	
	if( !rowObj )
	{
		//(@"** Unknown Error **");
		return tableView.rowHeight;
	}
	
	CGFloat rowHeight = 0;
	NSNumber* cachedHeight = [rowHeightCache_ objectForKey: rowObj];
	if( cachedHeight )
	{
		rowHeight = [cachedHeight floatValue];
		
	}else
	{
		
		rowHeight = [self.delegate outlineView:self
								 heightForRowAtIndex:rowObj.index
												  ofItem:rowObj.parentRowObject.item];
		
		[rowHeightCache_ setObject:[NSNumber numberWithFloat:rowHeight] forKey:rowObj];
		[temporaryRowHeightCache_ setObject:[NSNumber numberWithFloat:rowHeight] forKey:@(currentRowIndex)];

	}
	
	return rowHeight;
}

- (NSString *)origamiTableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	if( [self.delegate respondsToSelector:@selector( outlineView: titleForDeleteConfirmationButtonForItem: atIndexPath:)] )
	{
		OutlineRowObject* rowObj = [self rowObjectForPlainRow: indexPath.row];
		
		NSString* string = [self.delegate outlineView: self
				titleForDeleteConfirmationButtonForItem: rowObj.item
													 atIndexPath: indexPath];
		
		return string;
	}
	
	return @"Delete";
	
}


#pragma mark - Collapsing

- (BOOL)origamiTableView:(OrigamiTableView *)tableView canCollapseCellsInRange:(NSRange)range;
{
	if( self.editing ) return NO;
	
	if( !canCollapse_ ) return NO;
	
	return YES;
}

- (UITableViewCell*)origamiTableView:(OrigamiTableView *)tableView collapsingCellForRowAtIndexPath:(NSIndexPath *)indexPath inRange:(NSRange)range
{
	
	MNOutlineViewCell* cell = nil;
	
	cell = [[[MNOutlineViewCell alloc] initWithStyle: UITableViewCellStyleDefault
												reuseIdentifier: nil] autorelease];
	
	OutlineRowObject* futureParentRowObj = nil;
	
	if( range.location > 0 )
		futureParentRowObj = [self rowObjectForPlainRow: range.location -1];
	
	OutlineRowObject* existingRowObj = [self rowObjectForPlainRow: indexPath.row];
	
	OutlineRowObject* rowObj = [[[OutlineRowObject alloc] init] autorelease];
	
	
	rowObj.item = existingRowObj.item;
	rowObj.index = 0;
	rowObj.expanded = existingRowObj.expanded;
	
	rowObj.itemLevel = futureParentRowObj.itemLevel + 1;
	
	cell.outlineRowObject = rowObj;
	
	NSUInteger numberOfChildren =[dataSource outlineView: self numberOfChildrenOfItem: rowObj.item];
	
	cell.customView = [delegate outlineView: self viewForItem: existingRowObj.item forRowAtIndexPath:indexPath];
	cell.expandable = (numberOfChildren > 0 && !rowObj.expanded );
	cell.cellDelegate = self;
	cell.outlineRowObject = rowObj;
	cell.expanded = rowObj.expanded;
	cell.moveable = YES;
	
	return cell;
	
}

- (void)origamiTableView:(OrigamiTableView *)tableView didCollapseCellsInRange:(NSRange)range
{
	OutlineRowObject* futureParentRowObj = nil;
	
	if( range.location > 0 )
	{
		futureParentRowObj = [self rowObjectForPlainRow: range.location - 1];
	}
	
	
	NSMutableArray* array = [NSMutableArray array];
	
	for( NSUInteger row = range.location; row < NSMaxRange(range); row++ )
	{
		OutlineRowObject* existingRowObj = [self rowObjectForPlainRow: row];
		//		id item = [dataSource outlineView: self
		//											 child: existingRowObj.index
		//											ofItem: existingRowObj.parentRowObject.item];
		
		[array addObject: existingRowObj];
		
	}
	
	// Now we can close the item.
	[self item: futureParentRowObj.item setCollapsed: YES];
	
	
	for( NSUInteger hoge = 0; hoge < array.count; hoge++ )
	{
		OutlineRowObject* rowObject = [array objectAtIndex:hoge];
		
		id item = rowObject.item;
		
		NSUInteger numberOfChildren = [dataSource outlineView: self
												 numberOfChildrenOfItem: futureParentRowObj.item];
		
		
		if( [rowObject.parentRowObject.item isEqual:futureParentRowObj.item] )
		{
			[delegate outlineView: self
							 moveItem: item
								toItem: futureParentRowObj.item
							  atIndex: numberOfChildren];
			
		}else
		{
			
			[delegate outlineView: self
							 moveItem: item
								toItem: futureParentRowObj.item
							  atIndex: numberOfChildren];
			
		}
	}
	
}


#pragma mark - Indenting

/*
- (void)___origamiTableView:(OrigamiTableView *)tableView indentAtIndexPath:(NSIndexPath *)indexPath withAllSiblings:(BOOL)withSiblings
{
	// Ask delegate first
	
	
	if( [self.delegate respondsToSelector:@selector(outlineView:canIndentAtIndex:ofItem:)] )
	{
		OutlineRowObject* rowObj = [self rowObjectForPlainRow: indexPath.row];
		if( !rowObj ) return;
		
		
		BOOL flag = [self.delegate outlineView: self
									 canIndentAtIndex: rowObj.index
												  ofItem: rowObj.parentRowObject.item];
		
		if( !flag ) return;
	}
	
	
	
	MNOutlineViewCell* cell = (MNOutlineViewCell*)[tableView cellForRowAtIndexPath: indexPath];
	
	if( indexPath.row == 0 )
	{
		[cell rejectSwipeRight];
		return;
	}
	
	if( self.editing ) return;
	
	
	
	OutlineRowObject* obj = cell.outlineRowObject;
	if( obj.index == 0 )
	{
		[cell rejectSwipeRight];
		return; // Cannot indent because this is top item in this hierarchy
	}
	id siblingItem = nil; // closest previous sibling
	
	// find last item with item level = ( cell.outlineRowObject.itemLevel + 1) in siblingItem
	
	siblingItem = [dataSource outlineView:self child:obj.index-1 ofItem: obj.parentRowObject.item];
	
	
	NSUInteger affectedRows = 1;
	NSUInteger numberOfChildren = [dataSource outlineView:self numberOfChildrenOfItem:obj.item];
	
	if( cell.expanded && numberOfChildren > 0 )
	{
		
		affectedRows += [self calcNumberOfRowsForItem: obj.item] -1;
	}
	
	// Prepare for animation
	NSMutableArray* cells = [NSMutableArray array];
	
	
	for( NSUInteger row = indexPath.row; row < indexPath.row + affectedRows; row++ )
	{
		MNOutlineViewCell* cell = (MNOutlineViewCell*)[internalTableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:row inSection:0]];
		
		if( cell )
		{
			cell.outlineRowObject.itemLevel += 1;
			[cells addObject: cell];
		}
	}
	
	CGRect finalRect = [internalTableView rectForRowAtIndexPath:indexPath];
	CGFloat pointY = finalRect.origin.y;
	
	BOOL flag = [internalTableView setupAndAddOrigamiLayerWithCells:cells
																			 subCells:nil
																				expand:YES
																					toY:pointY];
	
	if( !flag ) return;
	
	
	numberOfChildren = [dataSource outlineView: self
							  numberOfChildrenOfItem: siblingItem];
	
	
	
	BOOL shouldCollapse = ( [self itemIsCollapsed: siblingItem] && numberOfChildren > 0 );
	
	
	if( shouldCollapse )
	{
		[delegate outlineView: self
						 moveItem: obj.item
							toItem: siblingItem
						  atIndex: numberOfChildren];
		
		[internalTableView reloadAndCloseVisibleCells:0.4
													 alignBottom:NO
												 withCompletion:^(BOOL finished){ }];
		
	}else
	{
		
		CALayer* underlayer = [CALayer layer];
		[underlayer retain];
		underlayer.frame = internalTableView.origamiLayer.frame;
		underlayer.backgroundColor = internalTableView.backgroundColor.CGColor;
		[internalTableView.layer insertSublayer:underlayer below:internalTableView.origamiLayer];
		
		
		
		CGRect frame = internalTableView.origamiLayer.frame;
		internalTableView.origamiLayer.frame = CGRectMake(-30, frame.origin.y, frame.size.width, frame.size.height);
		// Animation
		OrigamiAnimation* animation = [self indentAnimation:NO forLayer: internalTableView.origamiLayer ];
		[animation retain];
		
		animation.processHandler = ^{
			
			[delegate outlineView: self
							 moveItem: obj.item
								toItem: siblingItem
							  atIndex: numberOfChildren];
			
		};
		
		animation.completionHandler = ^{
			
			[internalTableView closeOrigamiLayer];
			[underlayer removeFromSuperlayer];
			[underlayer release];
			[animation autorelease];
			
		};
		
		[animation startAnimation];
		
	}
	
}

- (void)__origamiTableView:(OrigamiTableView *)tableView indentAtIndexPath:(NSIndexPath *)indexPath withAllSiblings:(BOOL)withSiblings
{
	MNOutlineViewCell* cell = (MNOutlineViewCell*)[tableView cellForRowAtIndexPath: indexPath];
	
	if( indexPath.row == 0 )
	{
		[cell rejectSwipeRight];
		return;
	}
	if( self.editing ) return;
	
	
	
	OutlineRowObject* obj = cell.outlineRowObject;
	if( obj.index == 0 )
	{
		[cell rejectSwipeRight];
		return; // Cannot indent because this is top item in this hierarchy
	}
	id siblingItem = nil; // closest previous sibling
	
	// find last item with item level = ( cell.outlineRowObject.itemLevel + 1) in siblingItem
	
	siblingItem = [dataSource outlineView:self child:obj.index-1 ofItem: obj.parentRowObject.item];
	
	
	NSUInteger affectedRows = 1;
	NSUInteger numberOfChildren = [dataSource outlineView:self numberOfChildrenOfItem:obj.item];
	
	if( cell.expanded && numberOfChildren > 0 )
	{
		
		affectedRows += [self calcNumberOfRowsForItem: obj.item] -1;
	}
	
	// Prepare for animation
	NSMutableArray* cells = [NSMutableArray array];
	
	
	for( NSUInteger row = indexPath.row; row < indexPath.row + affectedRows; row++ )
	{
		NSIndexPath* anIndexPath = [NSIndexPath indexPathForRow:row inSection:0];
		MNOutlineViewCell* cell = (MNOutlineViewCell*)[internalTableView cellForRowAtIndexPath: anIndexPath];
		
		if( cell )
		{
			cell.outlineRowObject.itemLevel += 1;
			[cells addObject: cell];
		}
	}
	
	
	
	
	
	numberOfChildren = [dataSource outlineView: self
							  numberOfChildrenOfItem: siblingItem];
	
	
	
	BOOL shouldCollapse = ( [self itemIsCollapsed: siblingItem] && numberOfChildren > 0 );
	
	
	if( shouldCollapse )
	{
		CGRect finalRect = [internalTableView rectForRowAtIndexPath:indexPath];
		CGFloat pointY = finalRect.origin.y;
		
		[internalTableView setupAndAddOrigamiLayerWithCells:cells
																 subCells:nil
																	expand:YES
																		toY:pointY];
		
		
		[delegate outlineView: self
						 moveItem: obj.item
							toItem: siblingItem
						  atIndex: numberOfChildren];
		
		[internalTableView reloadAndCloseVisibleCells:0.4
													 alignBottom:NO
												 withCompletion:^(BOOL finished){ }];
		
	}else
	{
		// Animation
		OrigamiAnimation* animation1 = [self indentAnimationFirstHalf:NO forCells: cells ];
		[animation1 retain];
		
		animation1.completionHandler = ^{
			
			// Make cells identtiy
			for( MNOutlineViewCell* cell in cells)
			{
				cell.transform = CGAffineTransformIdentity;
			}
			
			// then move
			[delegate outlineView: self
							 moveItem: obj.item
								toItem: siblingItem
							  atIndex: numberOfChildren];
			
			[internalTableView layoutIfNeeded];
			
			// Prepare for animation
			NSMutableArray* newCells = [NSMutableArray array];
			
			
			for( NSUInteger row = indexPath.row; row < indexPath.row + affectedRows; row++ )
			{
				NSIndexPath* anIndexPath = [NSIndexPath indexPathForRow:row inSection:0];
				MNOutlineViewCell* cell = (MNOutlineViewCell*)[internalTableView cellForRowAtIndexPath: anIndexPath];
				
				if( cell )
				{
					cell.outlineRowObject.itemLevel += 1;
					[newCells addObject: cell];
				}
			}
			
			
			OrigamiAnimation* animation2 = [self indentAnimationSecondHalf:NO forCells: newCells ];
			[animation2 retain];
			
			animation2.completionHandler = ^{
				
				[animation2 autorelease];
				[animation1 autorelease];
			};
			
			[animation2 startAnimation];
			
		};
		
		
		
		
		[animation1 startAnimation];
		
	}
	
}

*/

//- (void)origamiTableView:(OrigamiTableView *)tableView unindentAtIndexPathWithAllSiblings:(NSIndexPath *)indexPath
//{
//	if( indexPath.row == 0 ) return;
//
//	MNOutlineViewCell* cell = (MNOutlineViewCell*)[tableView cellForRowAtIndexPath: indexPath];
//
//	OutlineRowObject* obj = cell.outlineRowObject;
//	if( obj.index == 0 ) return; // Cannot indent because this is top item in this hierarchy
//
//
//
//	// Prepare for animation
//	NSMutableArray* cells = [NSMutableArray array];
//
//
//	MNOutlineViewCell* topCell = nil;
//
//	for( MNOutlineViewCell* cell in internalTableView.visibleCells )
//	{
//
//		if( [cell.outlineRowObject isChildOf: obj.parentRowObject] )
//			[cells addObject: cell];
//
//		if( !topCell )
//		{
//			topCell = cell;
//		}
//		else
//		{
//			if( topCell.frame.origin.y > cell.frame.origin.y )
//				topCell = cell;
//		}
//	}
//
//	CGFloat pointY = topCell.frame.origin.y;
//
//	[internalTableView setupAndAddOrigamiLayerWithCells:cells
//															 subCells:nil
//																expand:YES
//																	toY:pointY];
//
//
//	NSUInteger numberOfChildren = [dataSource outlineView: self
//											 numberOfChildrenOfItem: obj.parentRowObject.item];
//
//	NSLog(@"numberOfChildren %d",numberOfChildren);
//
//	NSUInteger destinationIndex = obj.parentRowObject.index;
//	for( NSUInteger siblingIndex = 0; siblingIndex < numberOfChildren; siblingIndex++ )
//	{
//		id item = [dataSource outlineView:self
//											 child:0
//											ofItem:obj.parentRowObject.item];
//
//		[delegate outlineView: self
//						 moveItem: item
//							toItem: obj.parentRowObject.parentRowObject.item
//						  atIndex: destinationIndex + siblingIndex + 1];
//	}
//
//
//	[self indentAnimation:YES forLayer: internalTableView.origamiLayer ];
//
//
//}

-(void)setupIndentOrigamiLayerWithCells:(NSArray*)cellArray toY: (CGFloat)pointY
{
	
	
	if( cellArray.count == 0 )
	{
		NSException* exception = [NSException exceptionWithName:@"Error" reason:@"No cells to expand" userInfo:nil];
		[exception raise];
		return;
	}
	
	for( MNOutlineViewCell * cell in cellArray )
	{
		cell.moveable = NO;
	}
	
	UIImage *viewSnapShot = [UITableViewCell mn_cellSnapshot:cellArray
																  maxHeight: self.frame.size.height
																alignBottom: NO
														  backgroundColor:internalTableView.backgroundColor ];
	
	
	//set layers
	
	CGSize totalSize = viewSnapShot.size;
	
	if( internalTableView.origamiLayer )
	{
		[internalTableView closeOrigamiLayer];
	}
	
	OrigamiLayer* origamiLayer = [OrigamiLayer layer];
	
	NSUInteger numberOfFolds = MAX( 1, (int)floorf( cellArray.count /2.0 ) );
	
	if( totalSize.height == self.frame.size.height )
		numberOfFolds = 2;
	
	origamiLayer.frame = CGRectMake(0, pointY, totalSize.width, totalSize.height);
	origamiLayer.image = viewSnapShot;
	origamiLayer.numberOfFolds = numberOfFolds;
	
	[internalTableView.layer addSublayer:origamiLayer];
	
	//レイヤーを追加した後でこれを行わないと、表示が正しく行なわれない 誤
	//[self layoutIfNeeded];
	
	[origamiLayer setProgress: 0];
	
	internalTableView.origamiLayer = origamiLayer;
}

/*
- (void)___origamiTableView:(OrigamiTableView *)tableView unindentAtIndexPath:(NSIndexPath *)indexPath withAllSiblings:(BOOL)withSiblings
{
	// Ask delegate first
	
	if( [self.delegate respondsToSelector:@selector(outlineView:canUnindentAtIndex:ofItem:)] )
	{
		OutlineRowObject* rowObj = [self rowObjectForPlainRow: indexPath.row];
		if( !rowObj ) return;
		
		
		BOOL flag = [self.delegate outlineView: self
								  canUnindentAtIndex: rowObj.index
												  ofItem: rowObj.parentRowObject.item];
		
		if( !flag ) return;
	}
	
	
	
	
	
	MNOutlineViewCell* cell = (MNOutlineViewCell*)[tableView cellForRowAtIndexPath: indexPath];
	
	if( indexPath.row == 0 )
	{
		[cell rejectSwipeLeft];
		return;
	}
	
	if( self.editing ) return;
	
	
	
	
	OutlineRowObject* obj = cell.outlineRowObject;
	if( obj.itemLevel == 0 )
	{
		[cell rejectSwipeLeft];
		return; // Cannot indent because this is root item
		
	}
	OutlineRowObject* parent = obj.parentRowObject;
	
	
	
	NSUInteger affectedRows = 1;
	
	if( cell.expanded )
	{
		affectedRows += [self calcNumberOfRowsForItem: obj.item] -1;
	}
	
	// Prepare for animation
	NSMutableArray* cells = [NSMutableArray array];
	
	
	for( NSUInteger row = indexPath.row; row < indexPath.row + affectedRows; row++ )
	{
		NSIndexPath* rowPath = [NSIndexPath indexPathForRow:row inSection:0];
		MNOutlineViewCell* cell = (MNOutlineViewCell*)[internalTableView cellForRowAtIndexPath: rowPath];
		
		if( cell )
			[cells addObject: cell];
		
	}
	
	CGRect touchedRowRect = [internalTableView rectForRowAtIndexPath:indexPath];
	CGFloat pointY = touchedRowRect.origin.y;
	
	// Create origami layer
	[self setupIndentOrigamiLayerWithCells:cells toY:pointY];
	
	
	
	// keep yonger siblings level +1
	
	NSUInteger numberOfChildren = [dataSource outlineView: self
											 numberOfChildrenOfItem: parent.item];
	
	NSMutableArray* youngerSiblings = [NSMutableArray array];
	for( NSUInteger hoge = obj.index + 1; hoge < numberOfChildren; hoge++ )
	{
		id sibling = [dataSource outlineView: self
												 child: hoge
												ofItem: parent.item];
		
		[youngerSiblings addObject: sibling];
	}
	
	
	if( !cell.expanded && youngerSiblings.count > 0 )
	{
		// アニメーションイフェクトをここに
		// unindentすることにより、弟アイテムが自分の子になる
		// unindent animation -> close animation
		
		
		__block NSMutableArray* cellsToMoveLater = [[NSMutableArray alloc] init];
		__block NSMutableArray* footerCells = [[NSMutableArray alloc] init];
		
		void(^getCellsBlock)(void) = ^{
			
			
			NSUInteger totalNumberOfRows = [self calcTotalNumberOfRows];
			CGFloat originY = 0;
			
			for( NSUInteger row = indexPath.row +1; row < totalNumberOfRows ; row++ )
			{
				
				NSIndexPath* rowPath = [NSIndexPath indexPathForRow:row inSection:0];
				
				
				MNOutlineViewCell* aCell = nil;
				
				aCell = (MNOutlineViewCell*)[internalTableView cellForRowAtIndexPath:rowPath];
				
				if( !aCell )
				{
					aCell = (MNOutlineViewCell*)[self origamiTableView: internalTableView
														  cellForRowAtIndexPath: rowPath];
				}
				
				
				// cell is not in the siblings
				if( aCell.outlineRowObject.itemLevel < obj.itemLevel )
					break;
				
				
				if( aCell )
					[cellsToMoveLater addObject: aCell];
				
				
				CGFloat rowHeight = internalTableView.rowHeight;
				
				if( [self respondsToSelector:@selector( origamiTableView: heightForRowAtIndexPath:) ] )
				{
					rowHeight = [self origamiTableView: internalTableView
								  heightForRowAtIndexPath: rowPath];
				}
				
				
				CGRect frame = aCell.frame;
				frame.size = CGSizeMake(self.bounds.size.width, rowHeight);
				aCell.frame = frame;
				
				originY += rowHeight;
				
				
				// Outside bounds
				if( originY > self.bounds.size.height )
					break;
			}
			
			
			
			originY = 0;
			
			// get first footer item
			
			NSUInteger firstFooterRow = -1;
			for( NSUInteger row = indexPath.row +2; row < totalNumberOfRows ; row++ )
			{
				firstFooterRow = row;
				
				OutlineRowObject* rowObject = [self rowObjectForPlainRow: row];
				
				// cell is not in the siblings
				if( rowObject.itemLevel < obj.itemLevel )
					break;
				
			}
			
			
			originY = 0;
			
			if( firstFooterRow != -1)
				for( NSUInteger row = firstFooterRow; row < totalNumberOfRows ; row++ )
				{
					
					NSIndexPath* rowPath = [NSIndexPath indexPathForRow:row inSection:0];
					
					
					MNOutlineViewCell* aCell = nil;
					
					aCell = (MNOutlineViewCell*)[internalTableView cellForRowAtIndexPath:rowPath];
					
					if( !aCell )
					{
						aCell = (MNOutlineViewCell*)[self origamiTableView: internalTableView
															  cellForRowAtIndexPath: rowPath];
					}
					
					// Outside bounds
					if( originY > self.bounds.size.height )
						break;
					
					if( aCell )
						[footerCells addObject: aCell];
					
					
					CGFloat rowHeight = internalTableView.rowHeight;
					
					if( [self respondsToSelector:@selector( origamiTableView: heightForRowAtIndexPath:) ] )
					{
						rowHeight = [self origamiTableView: internalTableView
									  heightForRowAtIndexPath: rowPath];
					}
					
					if( aCell )
					{
						CGRect frame = aCell.frame;
						frame.size = CGSizeMake(self.bounds.size.width, rowHeight);
						aCell.frame = frame;
					}
					
					originY += rowHeight;
					
					
				}
			
		};
		
		void(^collapseAnimationBlock)(void) = ^{
			
			
			// Move
			
			for( NSUInteger hoge = 0; hoge < youngerSiblings.count; hoge++ )
			{
				
				id item = [youngerSiblings objectAtIndex: hoge];
				NSUInteger lastIndex = [dataSource outlineView: self
												numberOfChildrenOfItem: obj.item];
				
				[delegate outlineView: self
								 moveItem: item
									toItem: obj.item
								  atIndex: lastIndex];
			}
			
			
			[delegate outlineView: self
							 moveItem: obj.item
								toItem: parent.parentRowObject.item
							  atIndex: parent.index + 1];
			
			
			
			// align cells to animate, create layer, then anmate
			
			CGFloat originY = 0;
			for( NSUInteger hoge = 0; hoge < cellsToMoveLater.count; hoge++ )
			{
				UITableViewCell* cell = [cellsToMoveLater objectAtIndex: hoge];
				
				
				CGRect frame = cell.frame;
				frame.origin.y = originY;
				cell.frame = frame;
				
				originY += frame.size.height;
				
			}
			
			
			for( NSUInteger hoge = 0; hoge < footerCells.count; hoge++ )
			{
				UITableViewCell* cell = [footerCells objectAtIndex: hoge];
				
				
				CGRect frame = cell.frame;
				frame.origin.y = originY;
				cell.frame = frame;
				
				originY += frame.size.height;
				
			}
			
			
			CGFloat pointY = CGRectGetMaxY(touchedRowRect);
			
			
			// Create origami layer
			BOOL flag = [internalTableView setupAndAddOrigamiLayerWithCells: cellsToMoveLater
																					 subCells: nil
																						expand: YES
																							toY: pointY];
			
			if( !flag ) return;
			
			CALayer* footerLayer = [internalTableView layerForCells:footerCells alignBottom:NO];
			
			internalTableView.origamiLayer.footerLayer = footerLayer;
			
			CALayer* layerToAnimate = internalTableView.origamiLayer;
			[internalTableView.layer addSublayer:layerToAnimate];
			
			CGFloat startOriginY = internalTableView.origamiLayer.frame.origin.y;
			CGFloat endOriginY = internalTableView.origamiLayer.frame.origin.y;
			
			OrigamiAnimation *animation_ = [[OrigamiAnimation alloc] initWithDuration: 0.25 ];
			
			
			animation_.progressBlock = ^(CGFloat progress){
				
				
				[internalTableView.origamiLayer setProgress: progress];
				
				CGFloat duration = 1.0;
				CGFloat currentY = startOriginY*(1-progress/duration) + endOriginY*(progress/duration);
				
				CGRect frame = tableView.origamiLayer.frame;
				frame.origin.y = currentY;
				tableView.origamiLayer.frame = frame;
				
			};
			
			animation_.completionHandler = ^{
				
				[internalTableView closeOrigamiLayer];
				
				// Release
				[animation_ release];
				
			};
			
			
			[internalTableView.origamiLayer setProgress: 0];
			
			[animation_ startAnimation];
			
			[cellsToMoveLater autorelease];
			[footerCells autorelease];
		};
		
		// unindent animation here
		
		
		
		//
		CALayer* layerToAnimate = internalTableView.origamiLayer;
		CGSize layerSize = layerToAnimate.frame.size;
		CGFloat originY = layerToAnimate.frame.origin.y;
		
		
		OrigamiAnimation *animation_ = [[OrigamiAnimation alloc] initWithDuration:0.2];
		
		animation_.progressBlock = ^(CGFloat progress){
			
			layerToAnimate.frame = CGRectMake( -30*((-1+progress)*cosf(4*progress*M_PI_2) + 1 ), originY, layerSize.width, layerSize.height);
			[layerToAnimate removeAllAnimations];
		};
		
		animation_.processHandler = getCellsBlock;
		
		animation_.completionHandler = ^{
			
			[internalTableView closeOrigamiLayer];
			
			// Move self then reload
			
			//[internalTableView reloadData];
			
			
			collapseAnimationBlock();
			[animation_ release];
			
		};
		
		
		[animation_ startAnimation];
		
		
		
		// Animation
		
		//[self indentAnimation:YES forLayer: internalTableView.origamiLayer ];
	}
	else
	{
		
		CALayer* underlayer = [CALayer layer];
		[underlayer retain];
		underlayer.frame = internalTableView.origamiLayer.frame;
		underlayer.backgroundColor = internalTableView.backgroundColor.CGColor;
		[internalTableView.layer insertSublayer:underlayer below:internalTableView.origamiLayer];
		
		
		// Animation
		
		OrigamiAnimation* animation = [self indentAnimation:YES forLayer: internalTableView.origamiLayer ];
		[animation retain];
		
		animation.processHandler = ^{
			
			
			//単純移動
			
			for( NSUInteger hoge = 0; hoge < youngerSiblings.count; hoge++ )
			{
				
				id item = [youngerSiblings objectAtIndex: hoge];
				NSUInteger lastIndex = [dataSource outlineView: self
												numberOfChildrenOfItem: obj.item];
				
				[delegate outlineView: self
								 moveItem: item
									toItem: obj.item
								  atIndex: lastIndex];
			}
			
			
			
			[delegate outlineView: self
							 moveItem: obj.item
								toItem: parent.parentRowObject.item
							  atIndex: parent.index + 1];
			
			
		};
		
		
		animation.completionHandler = ^{
			
			[internalTableView closeOrigamiLayer];
			[underlayer removeFromSuperlayer];
			[underlayer release];
			[animation autorelease];
			
		};
		
		[animation startAnimation];
		
	}
	
	
	
}

*/

-(OrigamiAnimation*)indentAnimation:(BOOL)unindent forLayer:(CALayer*)layerBeforeReload
{
	[internalTableView.layer addSublayer:layerBeforeReload];
	CGSize layerSize = layerBeforeReload.frame.size;
	CGFloat originY = layerBeforeReload.frame.origin.y;
	
	
	OrigamiAnimation *animation = [[OrigamiAnimation alloc] initWithDuration:0.2];
	
	animation.progressBlock = ^(CGFloat progress){
		
		layerBeforeReload.frame = CGRectMake((unindent?-30:30)*((-1+progress)*cosf(4*progress*M_PI_2) + (unindent?1:0) ), originY, layerSize.width, layerSize.height);
		[layerBeforeReload removeAllAnimations];
	};
	
	animation.completionHandler = ^{};
	
	return [animation autorelease];
	
}

//-(OrigamiAnimation*)__indentAnimationFirstHalf:(BOOL)unindent forCells:(NSArray*)cells
//{
//
//	OrigamiAnimation *animation = [[OrigamiAnimation alloc] initWithDuration:0.5];
//
//	animation.progressBlock = ^(CGFloat progress){
//
//
//		CGFloat originX = (unindent?-30:30)*sinf(progress*M_PI_4);
//
//		for( MNOutlineViewCell* cell in cells)
//		{
//			cell.transform = CGAffineTransformMakeTranslation(originX, 0);
//
//		}
//	};
//
//
//	return animation;
//
//}
//
//
//-(OrigamiAnimation*)__indentAnimationSecondHalf:(BOOL)unindent forCells:(NSArray*)cells
//{
//	CGFloat originX = (unindent?-30:30);
//
//	for( MNOutlineViewCell* cell in cells)
//	{
//		cell.cellContentView.transform = CGAffineTransformMakeTranslation(originX, 0);
//		cell.transform = CGAffineTransformMakeTranslation(-originX, 0);
//
//	}
//
//
//
//	OrigamiAnimation *animation = [[OrigamiAnimation alloc] initWithDuration:0.5];
//
//	animation.progressBlock = ^(CGFloat progress){
//
//
//		CGFloat originX = (unindent?-30:30)*sinf((1-progress)*M_PI_4);
//
//		for( MNOutlineViewCell* cell in cells)
//		{
//			cell.cellContentView.transform = CGAffineTransformMakeTranslation(originX, 0);
//			cell.transform = CGAffineTransformMakeTranslation(-originX, 0);
//
//		}
//	};
//
//
//	return animation;
//
//}



-(void)origamiTableViewWillBeginDragging:(OrigamiTableView *)tableView
{
	if( [delegate respondsToSelector:@selector(outlineViewWillBeginDragging:)] )
	{
		[delegate outlineViewWillBeginDragging: self];
	}
}

-(void)setContentInset:(UIEdgeInsets)inset
{
	[internalTableView setContentInset: inset];
}

-(UIEdgeInsets)contentInset
{
	return internalTableView.contentInset;
}

-(void)setScrollIndicatorInsets:(UIEdgeInsets)inset
{
	[internalTableView setScrollIndicatorInsets: inset];
}

-(UIEdgeInsets)scrollIndicatorInsets
{
	return internalTableView.scrollIndicatorInsets;
}

-(void)setContentOffset:(CGPoint)offset
{
	[internalTableView setContentOffset: offset];
}

-(void)setContentOffset:(CGPoint)offset animated:(BOOL)animated
{
	
	[internalTableView setContentOffset: offset animated: animated];
}

-(CGPoint)contentOffset
{
	return internalTableView.contentOffset;
}

-(CGSize)contentSize
{
	return [internalTableView contentSize];
}

-(NSArray*)visibleCells
{
	return internalTableView.visibleCells;
}


-(void)flashScrollIndicators
{
	[internalTableView flashScrollIndicators];
}

- (void)scrollRectToVisible:(CGRect)rect animated:(BOOL)animated
{
	[internalTableView scrollRectToVisible:rect animated:animated];
	
}

@end
//
//@interface OrigamiTableView (HeaderSection)
//
//
//@end
//
//@implementation OrigamiTableView (HeaderSection)
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//	NSLog(@"tint %@",self.tintColor);
//	UIView* view = [[UIVisualEffectView alloc] initWithFrame:CGRectMake(0,0,320,20)];
//	view.backgroundColor = [UIColor colorWithWhite:0 alpha:0.05];
//	return view;
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//	return 20;
//}
//
//@end


//
//  OutlineRowObject.h
//  OutlineViewCoreDataTest
//
//  Created by Nishikata Masatoshi on 9/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OutlineRowObject : NSObject

-(BOOL)isChildOf:(OutlineRowObject*)object;

@property (nonatomic, retain) id item;
@property (nonatomic, retain) OutlineRowObject* parentRowObject;

@property (nonatomic, readwrite) NSUInteger itemLevel;
@property (nonatomic, readwrite) NSUInteger index;
@property (nonatomic, readwrite) BOOL expanded;
@property (nonatomic, readwrite) NSInteger plainRow;

@end
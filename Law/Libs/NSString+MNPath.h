//
//  TextView (coordinate extension).h
//  LinkTextViewSampleProject
//
//  Created by Masatoshi Nishikata on 12/02/06.
//  Copyright 2006 Masatoshi Nishikata. All rights reserved.
//

#if	TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#else
#import <Cocoa/Cocoa.h>
#endif

@interface NSString (MNPath)


// from NSString_ODCompareNumerically
//http://www.cocoabuilder.com/archive/message/cocoa/2002/1/16/61373
- (NSComparisonResult)compareNumerically:(NSString *)aString;
-(NSRange)fullRange;
-(unsigned)reverseLocation:(unsigned)locationFromLast;
-(NSString*)omitSpacesAtBothEnds;
-(NSString*)omitSpacesReturnsAndCommaAtBothEnds;
-(NSString*)uniqueFilenameForFolder:(NSString*)folderPath;
-(NSString*)uniquePathForFolder; // change path to unique path;
-(NSString*)folderPath;
-(NSString*)safeFilename;

- (NSComparisonResult)comparForFontFamilyName:(NSString *)aString;




@end

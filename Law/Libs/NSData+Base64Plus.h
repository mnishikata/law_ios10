//
//  NSString+Base64Plus.h
//  Tango
//
//  Created by Masatoshi Nishikata on 23/12/13.
//  Copyright (c) 2013 Catalystwo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Base64Plus)
-(NSString*)base64PlusEncoded;
+(NSData*)decodeFromBase64Plus:(NSString*)string;
+(NSString*)deflateString:(NSString*)string;
+(NSString*)inflateString:(NSString*)string;


@end

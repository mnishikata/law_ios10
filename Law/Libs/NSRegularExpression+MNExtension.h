//
//  NSRegularExpression+MNExtension.h
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 5/09/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSRegularExpression (MNExtension)

- (NSString *)stringByReplacingMatchesInString:(NSString *)string
													options:(NSMatchingOptions)options
													  range:(NSRange)range
												  template:(NSString *)templ
							  withMatchTransformation: (NSString* (^) (NSString* element, NSRange matchRange))transformation;

@end
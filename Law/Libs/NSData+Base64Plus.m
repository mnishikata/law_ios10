//
//  NSString+Base64Plus.m
//  Tango
//
//  Created by Masatoshi Nishikata on 23/12/13.
//  Copyright (c) 2013 Catalystwo. All rights reserved.
//

#import "NSData+Base64Plus.h"
#import "NSData+CocoaDevUsersAdditions.h"
@implementation NSData (Base64Plus)

/*
 tm4TONBCTdyw2LRUIyoBTQ==
 
 */

-(NSString*)base64PlusEncoded
{
	NSString* encodedString = [self base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
	NSMutableString* mstr = [[NSMutableString alloc] initWithString:encodedString];
	
	[mstr replaceOccurrencesOfString:@"\n" withString:@"@" options:0 range:NSMakeRange(0,mstr.length)];

//	NSString *str = [mstr substringToIndex:22];
	return mstr;
}

+(NSData*)decodeFromBase64Plus:(NSString*)string
{
	NSMutableString* mstr = [[NSMutableString alloc] initWithString:string];
	[mstr replaceOccurrencesOfString:@"@" withString:@"\n" options:0 range:NSMakeRange(0,mstr.length)];

	NSData* decoded = [[NSData alloc] initWithBase64EncodedString:mstr options:NSDataBase64DecodingIgnoreUnknownCharacters];
	
	return decoded;
}

+(NSString*)deflateString:(NSString*)string
{
	if( string == nil ) return nil;

	NSData* data = [string dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
	NSData* compressed = [data deflate];
	NSString* encoded = [compressed base64PlusEncoded];
	return encoded;
}

+(NSString*)inflateString:(NSString*)string
{
	if( string == nil ) return nil;
	
	NSData* data = [NSData decodeFromBase64Plus:string];
	NSString* decoded = [[NSString alloc] initWithData:[data inflate] encoding:NSUTF8StringEncoding];
	
	return decoded;
}

@end

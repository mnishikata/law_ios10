//
//  FFBadgedBarButtonItem.m
//  FilterFresh
//
//  Created by Mark Granoff on 2/22/14.
//  Copyright (c) 2014 Hawk iMedia. All rights reserved.
//

#import "FFBadgedBarButtonItem.h"

@interface FFBadgedBarButtonItem()

#define BITS 16

@property (nonatomic, strong) UIView *buttonView;
@property (nonatomic, strong) UILabel *badgeLabel;
@property (nonatomic, weak) id myTarget;
@property (nonatomic, readwrite) SEL myAction;

@end

@implementation FFBadgedBarButtonItem

-(id)initWithImage:(UIImage *)image target:(id)target action:(SEL)action
{
	image = [UIImage imageNamed:@"Refresh"];
	
	UIButton* button = [UIButton buttonWithType:UIButtonTypeSystem];
	_button = button;
	[button setImage:image forState:UIControlStateNormal];
	[button addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
	button.frame = CGRectMake(0, 0, image.size.width, image.size.height);
	
	UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
	[v addSubview:button];
	
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(v.frame.size.width - BITS, v.frame.size.height - BITS, BITS, BITS)];
	label.backgroundColor = [UIColor redColor];
	label.layer.cornerRadius = BITS/2;
	label.layer.masksToBounds = YES;
	label.userInteractionEnabled = NO;
	label.font = [UIFont systemFontOfSize:12];
	label.minimumScaleFactor = 0.5;
	label.adjustsFontSizeToFitWidth = YES;
	label.textColor = [UIColor whiteColor];
	label.textAlignment = NSTextAlignmentCenter;
	label.text = @"";
	label.hidden = YES;
	
	[v addSubview:label];
//	UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:target action:action];
//	tap.numberOfTapsRequired = 1;
//	[v addGestureRecognizer:tap];
	
	self = [super initWithCustomView:v];
	if (self) {
		self.buttonView = v;
		self.badgeLabel = label;
		
		self.myTarget = target;
		self.myAction = action;
	}
	
	return self;
}

-(void)buttonTapped:(UIButton*)sender
{
	if( [self.myTarget respondsToSelector: self.myAction] )
		[self.myTarget performSelector:self.myAction withObject:self];
}

-(void)setAlertStyle:(BOOL)flag {
	
	if( flag ) {
		_badgeLabel.backgroundColor = [UIColor redColor];
		[_button setImage:[UIImage imageNamed:@"Refresh"] forState:UIControlStateNormal];

	} else {
		
		[_button setImage:[UIImage imageNamed:@"DBChecked"] forState:UIControlStateNormal];

	}

	[self.customView setNeedsDisplay];
	[self.customView setNeedsLayout];

}

-(NSString *)badge
{
    if (_badgeLabel.hidden)
        return nil;
    
    return _badgeLabel.text;
}

-(void)setBadge:(NSString *)badge
{
    if (!badge || badge.length < 1) {
        _badgeLabel.hidden = YES;
    } else {
        _badgeLabel.text = badge;
        CGSize newSize = [_badgeLabel sizeThatFits:CGSizeMake(_buttonView.frame.size.width, BITS)];
        CGRect frame = _badgeLabel.frame;
        frame.origin.x = MIN(_buttonView.frame.size.width - BITS, MAX(0, _buttonView.frame.size.width - newSize.width));
        frame.size.width = MAX(BITS,newSize.width);
        _badgeLabel.frame = frame;
        _badgeLabel.hidden = NO;
        [_buttonView setNeedsDisplay];
    }
	
	
}

@end

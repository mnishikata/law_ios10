//
//  NSRegularExpression+MNExtension.m
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 5/09/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

#import "NSRegularExpression+MNExtension.h"

@implementation NSRegularExpression (MNExtension)

- (NSString *)stringByReplacingMatchesInString:(NSString *)string
													options:(NSMatchingOptions)options
													  range:(NSRange)range
												  template:(NSString *)templ
							  withMatchTransformation: (NSString* (^) (NSString* element, NSRange matchRange))transformation
{
	NSMutableString* mutableString = [string mutableCopy];
	NSInteger offset = 0; // keeps track of range changes in the string due to replacements.
	for (NSTextCheckingResult* result in [self matchesInString:string
																		options:0
																		  range:range])
	{
		NSRange resultRange = [result range];
		resultRange.location += offset; // resultRange.location is updated based on the offset updated below
		
		// implement your own replace functionality using
		// replacementStringForResult:inString:offset:template:
		// note that in the template $0 is replaced by the match
		NSString* match = [self replacementStringForResult:result
																inString:mutableString
																  offset:offset
																template:templ];
		
		// get the replacement from the provided block
		NSString *replacement = transformation(match, resultRange);
		
		// make the replacement
		if( ![replacement isEqualToString: match] )
			[mutableString replaceCharactersInRange:resultRange withString:replacement];
		
		// update the offset based on the replacement
		offset += ([replacement length] - resultRange.length);
	}
	return mutableString;
}

@end

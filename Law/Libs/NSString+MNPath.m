//
//  TextView (coordinate extension).m
//  LinkTextViewSampleProject
//
//  Created by Masatoshi Nishikata on 12/02/06.
//  Copyright 2006 Masatoshi Nishikata. All rights reserved.
//

#import "NSString+MNPath.h"

#define SELECTOR @selector(characterAtIndex:)

typedef unichar(* unichar_IMP)(id,SEL,...);
static inline BOOL isDecimalDigit(unichar c) { return c >= '0' && c <= '9';
}

static inline NSComparisonResult compareUnsigned (unsigned a, unsigned b) {
    return (a == b) ? NSOrderedSame
	: (a < b ? NSOrderedAscending : NSOrderedDescending);
}

static NSComparisonResult compareNumerically (NSString *s1, NSString *s2,
											  unsigned int offset1, unsigned int offset2,
											  unichar_IMP imp1, unichar_IMP imp2)
{
    unsigned int len1 = (unsigned int)[s1 length] - offset1;
    unsigned int len2 = (unsigned int)[s2 length] - offset2;
    unichar c1 = 0, c2 = 0;
	
    // skip all common, nonnumeric characters
    unsigned int i, count = MIN(len1, len2);
    for (i = 0; i < count; i++) {
        c1 = imp1(s1, SELECTOR, offset1 + i);
        c2 = imp2(s2, SELECTOR, offset2 + i);
        if (c1 != c2 || isDecimalDigit(c1) || isDecimalDigit(c2)) { break; }
    }
	
    // one string is prefix of the other string
    // so we just need to compare the two lengths
    if (i == count) { return compareUnsigned(len1, len2); }
	
    else {
        BOOL isDecimal1 = isDecimalDigit(c1);
        BOOL isDecimal2 = isDecimalDigit(c2);
		
        // both substrings have a numeric prefix
        if (isDecimal1 && isDecimal2) {
            unsigned long long v1 = c1 - '0';
            unsigned long long v2 = c2 - '0';
            unsigned int i1, i2;
			
            // calculate the integer values
            for (i1 = i + 1; i1 < len1; i1++) {
                unichar c = imp1(s1, SELECTOR, offset1 + i1);
                if (isDecimalDigit(c)) { v1 = (10 * v1) + c - '0'; }
                else { break; }
            }
            for (i2 = i + 1; i2 < len2; i2++) {
                unichar c = imp2(s2, SELECTOR, offset2 + i2);
                if (isDecimalDigit(c)) { v2 = (10 * v2) + c - '0'; }
                else { break; }
            }
			
            // recursive function call if both values are equal
            return (v1 == v2)
                ? compareNumerically(s1,s2,offset1+i1,offset2+i2,imp1,imp2)
                : ((v1 < v2) ? NSOrderedAscending : NSOrderedDescending);
			
			// both characters are nonnumeric and not equal
        } else { return compareUnsigned(c1, c2); }
    }
}



@implementation NSString (MNPath)

#pragma mark Coordinate System


- (NSComparisonResult)compareNumerically:(NSString *)aString
{
    if (self == aString) return NSOrderedSame;
    else return compareNumerically(self, aString, 0, 0,
								   (unichar_IMP)[self methodForSelector:SELECTOR],
								   (unichar_IMP)[aString methodForSelector:SELECTOR]);
}


- (NSComparisonResult)comparForFontFamilyName:(NSString *)aString
{
	if( ![aString hasPrefix:@"."] && [self hasPrefix:@"."]) return NSOrderedDescending;
	if( [aString hasPrefix:@"."] && ![self hasPrefix:@"."]) return NSOrderedAscending;

	return [self compare:aString];
}



-(NSRange)fullRange
{
	return NSMakeRange(0, [self length]);
}


-(unsigned)reverseLocation:(unsigned)locationFromLast
{
		
	return (unsigned int)[self length] - locationFromLast -1 ;
}

-(NSString*)omitSpacesReturnsAndCommaAtBothEnds
{
	NSMutableString* newStr = [[NSMutableString alloc] initWithString:self];
	[newStr autorelease];
	
	while( [newStr length] > 0 )
	{
		NSString* moji = [newStr substringToIndex:1];
		
		if( [ moji isEqualToString:@" " ] || [ moji isEqualToString:@"," ] || [ moji isEqualToString:@"\n" ])
			[newStr deleteCharactersInRange:NSMakeRange(0,1)];
		
		else
			break;
		
		
		
	}
	
	
	
	if( [newStr length] != 0 )
	{
		while( [newStr length] > 0 )
		{
			NSString* moji = [newStr substringFromIndex:[newStr reverseLocation:0]];
			
			if( [ moji isEqualToString:@" " ] || [ moji isEqualToString:@"," ] || [ moji isEqualToString:@"\n" ] )
				[newStr deleteCharactersInRange:NSMakeRange([newStr reverseLocation:0],1)];
			
			else
				break;
			
		}
	}
	
	
	return newStr;
}

-(NSString*)omitSpacesAtBothEnds
{
	NSMutableString* newStr = [[NSMutableString alloc] initWithString:self];
	[newStr autorelease];

	unichar letter[1];
	letter[0] =  0xFFFC; // NSAttachmentCharacter
	NSString* controlCode = [[[NSString alloc] initWithCharacters:letter length:1] autorelease];

	
	while( [newStr length] > 0 )
	{
		NSString* moji = [newStr substringToIndex:1];
		
		if( [ moji isEqualToString:@" " ] || [ moji isEqualToString:controlCode ]   )
			[newStr deleteCharactersInRange:NSMakeRange(0,1)];
		
		else
			break;
		

		
	}
	
	
	
	if( [newStr length] != 0 )
	{
	while( [newStr length] > 0 )
	{
		NSString* moji = [newStr substringFromIndex:[newStr reverseLocation:0]];
		
		if( [ moji isEqualToString:@" " ] )
			[newStr deleteCharactersInRange:NSMakeRange([newStr reverseLocation:0],1)];
		
		else
			break;
		
	}
	}
	
	
	return newStr;
}

-(NSString*)uniqueFilenameForFolder:(NSString*)folderPath
{
	NSString* fullpath = [folderPath stringByAppendingPathComponent:self];
	fullpath = [fullpath uniquePathForFolder];
	
	return [fullpath lastPathComponent];
}

-(NSString*)safeFilename
{
	//modify filename
	NSMutableString* filename = [NSMutableString stringWithString: self];
	
	
	[filename replaceOccurrencesOfString:@":" withString:@" " 
								 options:0 range:NSMakeRange(0, [filename length])];
	
	[filename replaceOccurrencesOfString:@"/" withString:@" " 
								 options:0 range:NSMakeRange(0, [filename length])];
	
	[filename replaceOccurrencesOfString:@"\\" withString:@" " 
								 options:0 range:NSMakeRange(0, [filename length])];

	
	if( [filename hasPrefix:@"."] )
		[filename insertString:@" " atIndex:0];
	
	/// Delete return code
	[filename replaceOccurrencesOfString:@"\n" withString:@"" 
								 options:0 range:NSMakeRange(0, [filename length])];
	
	return filename;
}

-(NSString*)uniquePathForFolder // change path to unique path
{

	NSFileManager* fm = [NSFileManager defaultManager];
	NSString* returnValue = [NSString stringWithString: self ];
	NSString* extension = [self pathExtension];
	NSString* format = @"%@-%d.%@";
	
	if( extension.length == 0 ) format = @"%@-%d%@";
	
	if( [fm fileExistsAtPath:returnValue] )
	{
		unsigned hoge =2;
		
		while(1)
		{
			returnValue = [NSString stringWithFormat:format, 
				[self stringByDeletingPathExtension], hoge , extension];
			
			if( ! [fm fileExistsAtPath:returnValue] )
				break;
			
			hoge++;
			
		}
	}
	

	return returnValue;
	
}

-(NSString*)folderPath
{
	NSString* filename = [self   lastPathComponent];
	NSString* folderForFile = [self substringToIndex:
		[self length] - [filename length] 
		];

	return folderForFile;
}



@end

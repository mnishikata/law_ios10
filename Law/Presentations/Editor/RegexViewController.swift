//
//  RegexViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 30/08/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import UIKit

let kFirstRegex = "^[\\p{Blank}]*第[0-9０-９一二三四五六七八九〇十百]+条[の0-9０-９一二三四五六七八九〇十百]*"
let kSecondRegex = "^[\\p{Blank}]*[()\\.．0-9０-９A-ZＡ-Ｚ一二三四五六七八九〇十百○の]+(?=[\\p{Blank}\\n])"
let kTOCRegex = "^[\\p{Blank}]*第[0-9０-９]+(編|章|節|款|目)"

class RegexViewController: UIViewController {
  
  @IBOutlet weak var firstRegex: UITextField!
  @IBOutlet weak var secondRegex: UITextField!
  @IBOutlet weak var tocRegex: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    
    updateUI()
				
    title = WLoc("Regex")
  }
  
  @IBAction func done(_ sender: AnyObject) {
    
    let ud = UserDefaults.standard
    
    if let text1 = firstRegex?.text {
      ud.set(text1, forKey: "EditorFirstRegex")
    }
    
    if let text2 = secondRegex?.text {
      ud.set(text2, forKey: "EditorSecondRegex")
    }
    
    dismiss(animated: true, completion: nil)
  }
  
  func updateUI() {
    
    let ud = UserDefaults.standard
    let customRegex1 = ud.string(forKey: "EditorFirstRegex") ?? kFirstRegex
    let customRegex2 = ud.string(forKey: "EditorSecondRegex") ?? kSecondRegex
    
    firstRegex?.text = customRegex1
    secondRegex?.text = customRegex2
  }
  
  @IBAction func reset(_ sender: AnyObject) {
    
    let ud = UserDefaults.standard
    ud.removeObject(forKey: "EditorFirstRegex")
    ud.removeObject(forKey: "EditorSecondRegex")
    updateUI()
  }
}

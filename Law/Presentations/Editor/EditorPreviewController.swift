//
//  EditorPreviewController
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 17/08/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import UIKit
import CloudKit

class EditorPreviewController: UIViewController, UIPopoverPresentationControllerDelegate {
	
	var packageBundle:String? = nil
	
	var finalHTML:String? = nil
	var lawTitle:String? = nil
	var edition:String? = nil
	var author:String? = nil
	var source:NSAttributedString? = nil
	var linkDictionaries:[LinkDictionary]? = nil
	
	var docRecordID:CKRecordID? = nil
	var myPkgRecordID:CKRecordID? = nil

	var htmlLoaded:Bool = false
	
	@IBOutlet weak var webView: UIWebView!
	
	//MARK:- Body
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Do any additional setup after loading the view.

		let _ = CloudKitDomain.shared // Instantiate before use ...writing action involves
    navigationItem.rightBarButtonItem?.title = WLoc("Upload")
		
		navigationItem.prompt = WLoc("EditorPreviewControllerMessage")
	}
  
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		if finalHTML != nil && htmlLoaded == false {
			
			webView.loadHTMLString( finalHTML! , baseURL: nil)
			htmlLoaded = true
		}
		
		title = ""
	}
	
	@IBAction func jump(_ sender:AnyObject?) {
		
		if let _ = presentedViewController {
			dismiss(animated: false, completion: nil)
		}
		
		let controller = JumpViewController(showTab:false)
		controller.jumpAction = { [weak self] text, tabIdentifier, excerptMode in
			
			self?.jumpToString( text )
			self?.dismiss(animated: false, completion: nil )
		}
		
		controller.cancelBlock = { [weak self] (swtichToFind:Bool) -> () in
			
			self?.dismiss(animated: false) {
				if swtichToFind == true {
					self?.search()
				}
			}
		}
		
		controller.modalPresentationStyle = .popover
		
		let popover = controller.popoverPresentationController
		popover?.permittedArrowDirections = .any
		popover?.barButtonItem = sender as? UIBarButtonItem
		popover?.delegate = self
		
		present(controller, animated: true, completion: nil)
	}
	
	func search() {
	}
	
	func jumpToString(_ jumpToString:String!) {
		
		// Convert の -> .
		
		let mstr = NSMutableString( string: jumpToString)
		
		mstr.replaceOccurrences(of: "の",
			with: ".",
			options: .literal,
			range: NSMakeRange(0, mstr.length))
		
		// Look in link dictionary
		for dict in linkDictionaries! {
			
			// string 48.2
			if dict.numerical == mstr as String {
				
				let string:NSString = dict.link as NSString
				let script = "window.location.href = '#\(string)'"
				evaluateJavaScript(script, completionHandler: nil)
				break
			}
		}
	}
	
	func evaluateJavaScript( _ javaScriptString: String!,
		completionHandler: ((Any?, Error?) -> Void)!) {
			
			if webView != nil {
				let result = webView.stringByEvaluatingJavaScript( from: javaScriptString )
				
				if completionHandler != nil { completionHandler( result, nil ) }
				
			}else {
				
				/*
				webView_wk.evaluateJavaScript( javaScriptString, completionHandler: completionHandler)
				*/
				
			}
	}
	
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/

	@IBAction func upload(_ sender: UIBarButtonItem) {
		
		if finalHTML == nil { return }
		if packageBundle == nil { return }
		if lawTitle == nil { return }
		if edition == nil { return }
		if source == nil { return }
		if linkDictionaries == nil { return }
		
		if GCNetworkReachability.forInternetConnection().isReachable() == false {
			displayErrorAlertIfNecessary( LawError.offline, in: self )
			return
		}
		
		let alertController = UIAlertController(title: WLoc("Upload"), message: WLoc("UploadFinalMessage"), preferredStyle: .actionSheet)
		
		let action1 = UIAlertAction(title: WLoc("Upload"), style: .default) { (action) -> Void in

			let controller = WaitingViewController( nibName: "WaitingViewController", bundle:nil)
			
			controller.modalPresentationStyle = UIModalPresentationStyle.popover
			let popover = controller.popoverPresentationController
			popover?.delegate = controller
			popover?.barButtonItem = sender
			
			self.present(controller, animated: true) {
				self.uploadMain() { (success:Bool) -> Void in
					controller.dismiss(animated: true) {
						
						if success == true {
							let _ = self.navigationController?.popToRootViewController( animated: true)
							let alert = UIAlertView(title: WLoc("Uploaded"), message: "", delegate: nil, cancelButtonTitle: "OK")
							
							alert.show()
							
						}else {
							displayErrorAlertIfNecessary( LawError.unableToUpload, in: self )
						}
					}
				}
			}
		}
		
		let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in
			let pboardText = UIPasteboard.general
			pboardText.string = ""
		}
		
		alertController.addAction(action1)
		alertController.addAction(cancel)
		alertController.popoverPresentationController?.barButtonItem = sender
		
		present(alertController, animated: true, completion: nil)
	}
	
	func uploadMain(_ completion:@escaping ((_ success:Bool)->Void)) {
		
    // Prepare files
    do {
      let md5 = finalHTML!.md5()
      let sourceURL =  "user:" + packageBundle!
      let infoPlist: DocProperty = [
        .edition: edition!,
        .md5: md5!,
        .sourceURL: sourceURL,
        .title: lawTitle!
      ]
      
      let infoPlistData = try PropertyListSerialization.data(fromPropertyList: infoPlist, format: .binary, options: 0)
      
			let mdict = NSMutableArray()
			
			for dict in self.linkDictionaries! {
				let obj = dict.dictionaryRepresentation
				
				mdict.add(obj)
			}
			
			let tocPlistData = try PropertyListSerialization.data(fromPropertyList: mdict, format: .binary, options: 0)
			
			
			// Final adjustment
			// Insert footer
			
			let footer = "<BR><footer><DIV ALIGN=\"right\"><font size=2><a href=\"mailto:support@catalystwo.com?subject=Law Report for \( packageBundle! )&body=This user-uploaded material “\( packageBundle! )” is considered to breach someone's copyright.\n(md5:\(md5))\">Report copyright infringement</a></font></div></footer><BR></BODY></HTML>"
			
			if let range = finalHTML!.range(of: "</BODY></HTML>") {
				finalHTML?.replaceSubrange(range, with: footer)
			}
		
			
			if let contents = (finalHTML!.data(using: String.Encoding.utf8, allowLossyConversion: true) as NSData?)?.deflate() {
				
				let filename = packageBundle! + ".html.comcatalystwopackage"
				let filePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(filename) as NSString
				
				let fm = FileManager.default
				if fm.fileExists(atPath: filePath as String) {
					try fm.removeItem(atPath: filePath as String)
				}
				
				try fm.createDirectory(atPath: filePath as String, withIntermediateDirectories: true, attributes: nil)
				
				let infoPath = filePath.appendingPathComponent(FilePackage.Filename.infoPlist)
				let tocPath = filePath.appendingPathComponent(FilePackage.Filename.index)
				let contentPath = filePath.appendingPathComponent(FilePackage.Filename.content)
				
				let _ = try? infoPlistData.write(to: URL(fileURLWithPath: infoPath), options: [])
				let _ = try? tocPlistData.write(to: URL(fileURLWithPath: tocPath), options: [])
				let _ = try? contents.write(to: URL(fileURLWithPath: contentPath), options: [])
				
				let packageURL = URL( fileURLWithPath: filePath as String)
				
				let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)
				queue.async {
					
					
					// Add to my list
					let attributedString = self.source
					
					CloudKitDomain.shared.addDocument( self.packageBundle!, title:self.lawTitle!, source:attributedString!, overwritingRecordID:self.myPkgRecordID ) {
						(success:Bool) -> Void in
						
						print("adding document \(success)")
						
						if success == true {
							// Upload bundle
							
							CloudKitDomain.shared.upload(md5!, title: self.lawTitle!, edition: self.edition!, packageURL: packageURL, isPrivate:true, creator:self.author ,overwritingRecordID:self.docRecordID) { (success:Bool) -> Void in
								print("uploading bundle  \(success)")
								
								do {
									// Move to my local library
									
									let fm =  FileManager.default
									let fp = FilePackage.shared
									let toURL = fp.packageURL(from: self.packageBundle! + ".html" )
									
									if fm.fileExists(atPath: toURL.path) {
										try fm.removeItem(atPath: toURL.path)
									}
									
									try fm.moveItem(at: packageURL, to: toURL)
									
									let _ = toURL.setExcludedFromBackup()

									// Add
									
									let fileEntry = FileEntry( packageURL: toURL, anchor:nil, elementId:nil, plist: infoPlist)

									fp.addToIndex(fileEntry)
									
								} catch {
								}
								
								DispatchQueue.main.async {
									if success == true {
										completion(true)
										return
										
									}else {
										displayErrorAlertIfNecessary( LawError.unableToUpload, in: self )
									}
								}
							}
							
						}else {
							
							DispatchQueue.main.async {
								completion(false)
								return
							}
						}
					}
				}
				
			}else {
				// Unknown error
			}
		}  catch {
			// Unknown error
			completion(false)
		}
	}
	
	
	func adaptivePresentationStyle(for controller: UIPresentationController)
		-> UIModalPresentationStyle {
			return .none
	}
	
	func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection:UITraitCollection)
		-> UIModalPresentationStyle {
			return .none
	}
	
	func popoverPresentationController( _ popoverPresentationController: UIPopoverPresentationController,
		willRepositionPopoverTo rect: UnsafeMutablePointer<CGRect>,
		in view: AutoreleasingUnsafeMutablePointer<UIView>) {
	}
}

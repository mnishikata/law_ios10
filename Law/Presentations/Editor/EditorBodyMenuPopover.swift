//
//  EditorBodyMenuPopover.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 22/08/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import UIKit

class EditorBodyMenuPopover: UIViewController, UIPopoverPresentationControllerDelegate, UITextFieldDelegate {

	@IBOutlet weak var toggleSwitch: UISwitch!
	@IBOutlet weak var toggleSwitchLabel: UILabel!
	@IBOutlet weak var toggleSwitchMessage: UILabel!
	@IBOutlet weak var pasteButton: UIButton!
	@IBOutlet weak var downloadButton: UIButton!
	
	weak var master:EditorBodyViewController? = nil
	
	var downloadAction:UIAlertAction? = nil
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Do any additional setup after loading the view.
		
		toggleSwitchLabel.text = WLoc("EditorBodyMenuPopoverToggleSwitch")
		toggleSwitchMessage.text = WLoc("EditorBodyViewControllerSwitchMessage")
		
		pasteButton.setTitle(WLoc("Paste"), for: UIControlState())
		downloadButton.setTitle(WLoc("Download from Website"), for: UIControlState())
		
		navigationItem.prompt = WLoc("Set Law contents")
		
		toggleSwitch.isOn = (master?.singleLine)!
	}
	
	override var preferredContentSize: CGSize {
		set {}
		get {
			return CGSize(width: 280,height: 240)
		}
	}
	
	@IBAction func toggle() {
		master?.singleLine = toggleSwitch.isOn
	}
	
	@IBAction func pasteAction(_ sender: AnyObject) {
		master?.pasteAction(sender)
		dismiss(animated: true, completion: nil)
	}
	
	@IBAction func loadFromICloud(_ sender: AnyObject) {
		var theMasterViewController:EditorBodyViewController? = master
		dismiss(animated: true) {
			theMasterViewController?.loadFromICloudDrive()
			theMasterViewController = nil
		}
	}
	
	@IBAction func download(_ sender: AnyObject) {
		//
		let alert = UIAlertController(title: WLoc("Download Website"), message: WLoc("Download Website Message"), preferredStyle: .alert)
		alert.addTextField { (field:UITextField) -> Void in
			
			if let URL = UIPasteboard.general.url {
				field.text = URL.absoluteString
			}
		}
		
		let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in }
		
		downloadAction = UIAlertAction(title: WLoc("Download"), style: .default) { (action) -> Void in
			
			let fields = alert.textFields
			let field = fields?.first as  UITextField?
			let string = field?.text
			
			self.dismiss(animated: true) {
				if string != nil && string!.isEmpty == false {
					self.master?.download(string!)
				}
			}
		}
		
		downloadAction!.isEnabled = false
		let fields = alert.textFields
		let field = fields?.first as  UITextField?
		field?.delegate = self
		
		alert.addAction(downloadAction!)
		alert.addAction(cancel)
		present(alert, animated: true, completion: nil)
	}
	
	func textField( _ textField: UITextField,
		shouldChangeCharactersIn range: NSRange,
		replacementString string: String) -> Bool {
			
		var enableDownloadAction = false
		if textField.text != nil {
			
			if textField.text!.hasPrefix("http") {
				enableDownloadAction = true
			}
		}
		
		downloadAction?.isEnabled = enableDownloadAction
		return true
	}
	
	@IBAction func clear(_ sender: AnyObject) {
		let alert = UIAlertController(title: WLoc("Clear"), message: WLoc("Clear Message"), preferredStyle: .alert)
		
			let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in }
		
			let clearAction = UIAlertAction(title: WLoc("Clear"), style: .destructive) { (action) -> Void in
				
				self.master?.clear()
				self.dismiss(animated: true, completion: nil)
			}

			alert.addAction(clearAction)
			alert.addAction(cancel)
			present(alert, animated: true, completion: nil)
	}
	

	// MARK:- Popover

	func adaptivePresentationStyle(for controller: UIPresentationController)
		-> UIModalPresentationStyle {
			return .none
	}
	
	func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection:UITraitCollection)
		-> UIModalPresentationStyle {
			return .none
	}
}

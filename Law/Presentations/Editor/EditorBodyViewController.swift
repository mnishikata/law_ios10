//
//  EditorBodyViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 20/08/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import UIKit

class EditorBodyViewController: UIViewController, UIDocumentPickerDelegate {
	
	var source:NSAttributedString? = nil
	var commpletionHandler:( (_ attributedString:NSAttributedString, _ singleLine:Bool, _ preferredTitle:String?) ->Void)?
	
	var singleLine:Bool = true
	var preferredTitle:String? = nil
	
	@IBOutlet weak var textView: UITextView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Do any additional setup after loading the view.
		
    //let item = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
    //navigationItem.rightBarButtonItem = item
		
    //let button = UIBarButtonItem(title: WLoc("Menu"), style: .plain, target: self, action: #selector(menu(_:)))
		navigationItem.leftBarButtonItem?.title = WLoc("Menu")
		navigationItem.prompt = WLoc("Set Law contents")
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear( animated )
		
		textView.attributedText = source
		registerNotifications()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		textView.becomeFirstResponder()
	}
	
	override func viewWillDisappear(_ animated:Bool) {
		super.viewWillDisappear(animated)
		unregisterNotifications()
		
		source = textView.attributedText
	}
	
	func registerNotifications() {
		let center = NotificationCenter.default
		center.addObserver(self, selector: #selector(keyboardWasShown(_:)), name: .UIKeyboardWillShow, object: nil)
		center.addObserver(self, selector: #selector(keyboardWasShown(_:)), name: .UIKeyboardWillChangeFrame, object: nil)
		center.addObserver(self, selector: #selector(keyboardIsHidden(_:)), name: .UIKeyboardWillHide, object: nil)
	}
	
	func unregisterNotifications() {
		let center = NotificationCenter.default
		center.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
		center.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
		center.removeObserver(self, name: .UIKeyboardWillChangeFrame, object: nil)
		center.removeObserver(self, name: .NSTextStorageDidProcessEditing, object: self.textView.textStorage)
	}
	
	var originalInsetBottom:CGFloat = 0
	var originalScrollIndicatorInsets:UIEdgeInsets = UIEdgeInsets()

	func keyboardWasShown( _ n:Notification ) {
		
		originalInsetBottom = textView.contentInset.bottom
		originalScrollIndicatorInsets = textView.scrollIndicatorInsets
		
		let d = (n as NSNotification).userInfo!
		var r = (d[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
		r = textView.convert(r, from:nil)
		textView.contentInset.bottom = r.size.height
		textView.scrollIndicatorInsets.bottom = r.size.height
		
	}
	
	func keyboardIsHidden( _ notification:Notification ) {
		
		textView.contentInset.bottom = originalInsetBottom
		textView.scrollIndicatorInsets = originalScrollIndicatorInsets
	}
	
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "showMenu" {
      textView.resignFirstResponder()
      
      let controller = segue.destination as! EditorBodyMenuPopover
      controller.master = self
      
      let popover = controller.popoverPresentationController
      popover?.delegate = controller
    }
  }
	
	func loadFromICloudDrive() {
		
		let controller = UIDocumentPickerViewController(documentTypes:[kUTTypePlainText as String]
			, in: UIDocumentPickerMode.import)
		controller.delegate = self
		
		present(controller, animated: true, completion: nil)
	}
	
	func documentPicker( _ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
		
		do {
			let path = url.path
			let string = try NSString(contentsOfFile: path, usedEncoding: nil)
			
			textView.text = string as String
			preferredTitle = (url.lastPathComponent as NSString?)?.deletingPathExtension as String?
			
		}catch {
		}
	}
	
	@IBAction func pasteAction(_ sender: AnyObject) {
		textView.paste(self)
	}
	
	func clear() {
		let attr = NSAttributedString(string: "")
		textView.attributedText = attr
	}
	
	func download(_ string:String) {
		
		let controller = WaitingViewController( nibName: "WaitingViewController", bundle:nil)
		
		controller.modalPresentationStyle = UIModalPresentationStyle.popover
		let popover = controller.popoverPresentationController
		popover?.delegate = controller
		popover?.permittedArrowDirections = [.up, .down]
		popover?.barButtonItem = navigationItem.leftBarButtonItem
		present(controller, animated: true, completion: nil)
		
		let _ = MNCloudKitDownloader.downloadFileAtURL( URL(string:string)! ) { (downloader, success, data) -> Void in
			
			if data != nil {
				do {
					let attr = try NSAttributedString(data: data!, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType], documentAttributes: nil)
					
					self.textView.insertText(attr.string)
					
				} catch {
					print("failed")
				}
			}
			
			controller.dismiss(animated: true, completion:nil )
		}
	}
	
  @IBAction func done(_ sender: AnyObject) {

		source = textView.attributedText
		dismiss(animated: true) {  [weak self] () -> Void in
			self?.commpletionHandler?(self!.source!, self!.singleLine, self!.preferredTitle) }
	}
}

//
//  EditorViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 19/08/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import UIKit
import CloudKit

class EditorViewController: UITableViewController, UIAlertViewDelegate, UIDocumentPickerDelegate {
  
  var packageBundle: String? = nil
  var lawTitle: String? = ""
  var edition: String? = ""
  var source: NSAttributedString? = nil
  var author: String? = {
    if let string = UserDefaults.standard.object(forKey: "EditorViewControllerAuthor") {
      return string as? String
    } else {
      return ""
    }
  }()
  var docRecordID: CKRecordID? = nil
	var myPkgRecordID: CKRecordID? = nil
	var singleLine: Bool = true
	
	//MARK:- Body
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Do any additional setup after loading the view.
		let barbutton = navigationItem.rightBarButtonItem
    barbutton?.title = WLoc("Preview")
		title = WLoc("Edit")
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		tableView.reloadData()
		navigationController?.isToolbarHidden = false
		updateUI()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	func updateUI() {
		
		var canGoNext = true
		if lawTitle == nil || lawTitle?.isEmpty == true { canGoNext = false }
		if source == nil || source?.string.isEmpty == true { canGoNext = false }

		navigationItem.rightBarButtonItem?.isEnabled = canGoNext
	}
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/
	
	func loadPlist(_ sender: AnyObject) {
		let controller = UIDocumentPickerViewController(documentTypes: ["com.apple.property-list"], in: .import)
		controller.delegate = self
		self.present(controller, animated: true, completion: nil)
		
	}
	
	func documentPicker( _ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
		
		if let data = try? Data(contentsOf: url) {
      let plist = try! PropertyListSerialization.propertyList(from: data, options: .mutableContainersAndLeaves, format: nil) as! [String: AnyObject]
						
			guard let html = plist[FilePackage.contentHTMLKey] as? String else { return }
			guard let pkgName = plist[FilePackage.packagenameKey] as? String else { return }
			guard var infoPlist = plist[FilePackage.Filename.infoPlist] as? DocProperty else { return }
			guard let tocPlist = plist[FilePackage.Filename.index] as? [AnyObject] else { return }
			
			let md5 = html.md5()

			infoPlist[.md5] = md5!
			infoPlist[.sourceURL] = "user:" + pkgName
			
			let infoPlistData = try! PropertyListSerialization.data(fromPropertyList: infoPlist, format: .binary, options: 0)
			let tocPlistData = try! PropertyListSerialization.data(fromPropertyList: tocPlist, format: .binary, options: 0)

			if let contents = (html.data(using: String.Encoding.utf8, allowLossyConversion: true) as NSData?)?.deflate() {
				
				let filename = pkgName + ".html.comcatalystwopackage"
				let filePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(filename) as NSString
				
				let fm = FileManager.default
				if fm.fileExists(atPath: filePath as String) {
					try! fm.removeItem(atPath: filePath as String)
				}
				
				try! fm.createDirectory(atPath: filePath as String, withIntermediateDirectories: true, attributes: nil)
				
        let infoPath = filePath.appendingPathComponent(FilePackage.Filename.infoPlist)
        let tocPath = filePath.appendingPathComponent(FilePackage.Filename.index)
        let contentPath = filePath.appendingPathComponent(FilePackage.Filename.content)
				
				let _ = try? infoPlistData.write(to: URL(fileURLWithPath: infoPath), options: [])
				let _ = try? tocPlistData.write(to: URL(fileURLWithPath: tocPath), options: [])
				let _ = try? contents.write(to: URL(fileURLWithPath: contentPath), options: [])
				
				let packageURL = URL( fileURLWithPath: filePath as String)
				
				let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)
				queue.async {
										
					// Add to my list
					
					// Upload bundle
					
					CloudKitDomain.shared.upload(md5!, title: infoPlist[.title] as! String, edition: infoPlist[.edition] as! String, packageURL: packageURL, isPrivate: true, creator: self.author ,overwritingRecordID: self.docRecordID) { (success: Bool) -> Void in
						print("uploading bundle  \(success)")
						
						do {
							
							// Move to my local library
							
							let fm = FileManager.default
							let fp = FilePackage.shared
							let toURL = fp.packageURL(from: pkgName + ".html" )
							
							if fm.fileExists(atPath: toURL.path) {
								try fm.removeItem(atPath: toURL.path)
							}
							
							try fm.moveItem(at: packageURL, to: toURL)
							
							let _ = toURL.setExcludedFromBackup()
							
							// Add
							
							let fileEntry = FileEntry( packageURL: toURL, anchor: nil, elementId: nil, plist: infoPlist)
							
							fp.addToIndex(fileEntry)
							
						} catch {
						}
						
						DispatchQueue.main.async {
							if success == true {
								print("success")
								return
								
							}else {
								displayErrorAlertIfNecessary( LawError.unableToUpload, in: self )
							}
						}
					}
				}
			}
		}else {
			// load
			try! FileManager.default.startDownloadingUbiquitousItem(at: url)
			print("download icloud item")
		}
		
	}
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "showPreview" {
      
      let ud = UserDefaults.standard
      
      ud.set(author, forKey: "EditorViewControllerAuthor")
      
      let date = Date()
      
      let formatter = DateFormatter()
      formatter.dateStyle = DateFormatter.Style.long
      formatter.timeStyle = .short
      formatter.locale = Locale(identifier: "ja_JP")
      formatter.calendar = Calendar(identifier: Calendar.Identifier.japanese)
      let henshuDateString = formatter.string(from: date)
      
      // Convert to HTML
      
      if packageBundle == nil { packageBundle = shortUUIDString() }
      
      let (htmlString, linkDictionaries) = process(source!.string, fileUUID: packageBundle!, singleLine: singleLine )
      
      
      // Append hdader and footer
      
      var rightPart = "<DIV ALIGN=\"right\">" + edition! + "<BR><SPAN class=\"brckt\">投稿者：👤" + ( author!.isEmpty ? "Anonymous" : author! )
      
      rightPart +=  "<br />" + henshuDateString + "</SPAN> </DIV><br /><p>"
      
      let header = "<HTML><HEAD><META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><META http-equiv=\"Content-Language\" content=\"ja\"><STYLE type=\"text/css\"></STYLE><TITLE>" + lawTitle! + "</TITLE></HEAD><BODY><B>" + lawTitle! + "</B><BR><BR>" + rightPart
      let footer = "</BODY></HTML>"
      let finalHTML = NSMutableString( string: header + htmlString + footer )
      
      finalHTML.replaceCSS(stripCSSInstead: false, zoomScale: 1.0 )
      
      let controller = segue.destination as! EditorPreviewController
      
      controller.finalHTML = finalHTML as String
      controller.packageBundle = packageBundle
      controller.lawTitle = lawTitle
      controller.edition = edition
      controller.source = source
      controller.author = author
      controller.linkDictionaries = linkDictionaries
      controller.docRecordID = docRecordID
      controller.myPkgRecordID = myPkgRecordID
      
      //navigationController?.pushViewController( controller, animated: true )
    }
  }
  
	struct SliceRange {
		var matchRange: NSRange? // Title, article number etc
		var bodyRange: NSRange // Whole body
	}
	
	func sliceString( _ string: NSString, withRegex regex: String) -> [SliceRange] {
		
		do {
			let regex = try NSRegularExpression(pattern: regex, options: [.anchorsMatchLines, .caseInsensitive])
			
			var startIndex: Int? = nil
			var itemRanges: [SliceRange] = []
			var previousRange: NSRange? = nil
			regex.enumerateMatches(in: string as String, options: [], range: NSMakeRange(0, string.length)) { (match, flag, obj) -> Void in
				
				if let range = match?.range {
					
					if startIndex == nil {
						
						let rangeToAdd = SliceRange(matchRange: nil, bodyRange: NSMakeRange(0, range.location))
						itemRanges.append( rangeToAdd )
						
						startIndex = range.location
						previousRange = range
						
					}else {
						let itemRange = NSMakeRange( startIndex!, range.location - startIndex!)
						let rangeToAdd = SliceRange(matchRange: previousRange, bodyRange: itemRange)
						itemRanges.append( rangeToAdd )
						
						startIndex = range.location
						previousRange = range
					}
				}
				
			}
			
			// Append the last
			
			if startIndex == nil {
				startIndex = 0
				previousRange = nil
			}
			
			let itemRange = NSMakeRange( startIndex!, string.length - startIndex!)
			
			let rangeToAdd = SliceRange(matchRange: previousRange, bodyRange: itemRange)
			
			itemRanges.append(rangeToAdd)
			
			return itemRanges
			
		} catch {
			return []
		}
	}
	
	func process(_ string: String, fileUUID: String, singleLine: Bool) -> (html: String, linkDictionaries: [LinkDictionary]) {
		
		let ud = UserDefaults.standard

		// Escape
		var mstr = NSMutableString(string: string)
		
		// Convert Kanji
		 mstr.convertKanjiNumberToArabicNumber(convertToZenkaku: true)
		
		mstr.replaceOccurrences(of: "\t", with: " ", options: [], range: NSMakeRange(0, mstr.length))
		
		mstr.replaceOccurrences(of: "script", with: "disable_scrpt_", options: [], range: NSMakeRange(0, mstr.length))
		
		mstr.esscapeCharactersInHtml()
		
		//appendParenthesisTag
		mstr.appendParenthesisTags()
		
		// Use mstr as start point
		
		let articleNumberRegexStringToExtract = ud.string(forKey: "EditorFirstRegex") ?? kFirstRegex
		let articleNumberRegexString = articleNumberRegexStringToExtract + "(?=[\\p{Blank}\\n])"

		var startIndex: Int? = nil
		var sliceRanges: [SliceRange] = sliceString( mstr, withRegex: articleNumberRegexString )
		
		// Convert
		
		let htmlString = NSMutableString()
		var linkDictionaries: [LinkDictionary] = []
		for slice: SliceRange in sliceRanges {
			
			// add preface
			if slice.matchRange == nil {
				let body = mstr.substring(with: slice.bodyRange) as NSString
				htmlString.append( body as String )
				continue
			}
			
			let itemString = mstr.substring(with: slice.bodyRange) as NSString
			
			// Convert title line ... Whole first line or only 第[0-9０-９]+条
			
			let wholeFirstLineIsTitle = singleLine
			
			var body = ""
			
			func addItem(_ body: String, hierarchy: Int, uuid: String) -> String {
				
				if hierarchy > 1 { return body }
				
				let slices: [SliceRange] = sliceString(body as NSString, withRegex: ud.string(forKey: "EditorSecondRegex") ?? kSecondRegex )
				
				var indexNumberForThisHierarchy = 1
				let convertedBody = NSMutableString()
				
				for sliceRange: SliceRange in slices {
					
					if sliceRange.matchRange == nil {
						
						convertedBody.append( (body as NSString).substring( with: sliceRange.bodyRange ))
						
						continue
					}
					
					let numberString = (body as NSString).substring(with: sliceRange.matchRange!)
					
					let subrange = NSMakeRange( NSMaxRange(sliceRange.matchRange!), NSMaxRange(sliceRange.bodyRange) - NSMaxRange(sliceRange.matchRange!))
					let bodyString = (body as NSString).substring(with: subrange)
					
					let className = (hierarchy == 0 ? "item" : "number" )
					let thisUUID = String(indexNumberForThisHierarchy) + "@" + uuid
					let aNameTag = "<A NAME=\"" + thisUUID + "\">"

					let stringToAdd = "<DIV class=\"\(className)\"><B>" + aNameTag + numberString + "</A></B>" + addItem(bodyString, hierarchy: hierarchy+1, uuid: thisUUID) + "</DIV>"
					
					/*
							regexが同じなので再帰はしない
					*/
					
					convertedBody.append(stringToAdd)
					indexNumberForThisHierarchy += 1
				}
				
				return convertedBody as String
			}
			
			// Generate Link Dictionary
			
			var titleString = mstr.substring( with: slice.matchRange! )
			
			let aString = NSMutableString(string: titleString as NSString)
			aString.replaceOccurrences(of: "\\p{blank}", with: "", options: .regularExpression, range: NSMakeRange(0, aString.length))
			
			titleString = aString as String
			
			let numericalTitle = titleString.convertTitleToNumbers()
			
			let uuid =  numericalTitle
			let linkDictionary = LinkDictionary( link: uuid, content: titleString, numerical: numericalTitle)
			linkDictionaries.append(linkDictionary)
			
			// Build a html snippet
			
			let linkTag =  "<A HREF=\"comcatalystwolaws:" + titleString.encodedArticleTitle + "@" + uuid + "@" + fileUUID + ".html\" class=\"bkmk\">"
			let aNameTag = "<A NAME=\"" + uuid + "\"></A>"
			
			if wholeFirstLineIsTitle == false {
				
				let theRest = mstr.substring( with: NSMakeRange(NSMaxRange( slice.matchRange! ), NSMaxRange( slice.bodyRange ) - NSMaxRange( slice.matchRange! )) )
				
				body = "<B>" + linkTag + titleString + "</A>" + aNameTag + "</B>" +  addItem(theRest, hierarchy: 0, uuid: uuid)
				
			}else {
				
				let firstLineRange = itemString.range( of: "^[^\n]*", options: [.regularExpression], range: NSMakeRange(0, itemString.length) )
				
				let firstLineString = itemString.substring( with: firstLineRange ) as NSString
				let titleRange = firstLineString.range(of: articleNumberRegexStringToExtract, options: [.regularExpression], range: NSMakeRange(0, firstLineString.length))
				
				titleString = firstLineString.substring( with: titleRange )
				let titleBody = firstLineString.substring( with: NSMakeRange(NSMaxRange(titleRange), firstLineString.length - NSMaxRange(titleRange)) )
				let theRest = itemString.substring( with: NSMakeRange(NSMaxRange( firstLineRange ), itemString.length - NSMaxRange( firstLineRange )) )
				
				
				body = "<B>" + linkTag +  titleString + "</A>" + aNameTag + titleBody + "</B>" + addItem(theRest, hierarchy: 0, uuid: uuid)
			}
			
			
			/*
			行頭において
			()[]0-9 で孤立している<漢数字含む>
			A-Z 一文字で孤立
			
			*/
			
			let header = "<DIV class=\"item\">"
			let footer = "</DIV><P>"
			
			htmlString.append( header + body + footer )
		}
		
		htmlString.replaceOccurrences(of: "\n", with: "<br>\n", options: [], range: NSMakeRange(0, htmlString.length))
		
		return (htmlString as String, linkDictionaries )
	}
	
	// MARK: - Table view data source
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 2
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if section == 0 { return 3 }
		return 1
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") ??   UITableViewCell(style: .value1, reuseIdentifier: "Cell")
		
		cell.accessoryType = .disclosureIndicator
		
		if indexPath.section == 0 {
			
			if indexPath.row == 0 {
				cell.textLabel!.text = WLoc("LawTitle")
				cell.detailTextLabel!.text = lawTitle
			}
			
			if indexPath.row == 1 {
				cell.textLabel!.text = WLoc("LawDescription")
				cell.detailTextLabel!.text = edition
			}
			
			if indexPath.row == 2 {
				cell.textLabel!.text = WLoc("LawEditor")
				cell.detailTextLabel!.text = author
			}
		}
		
		if indexPath.section == 1 {
			
			if indexPath.row == 0 {
				cell.textLabel!.text = WLoc("LawContents")
				
				if source == nil || (source!.string as NSString).length == 0 {
				cell.detailTextLabel!.text = WLoc("Not Set")
				} else {
					cell.detailTextLabel!.text = NSString( format: "%d 文字", (source!.string as NSString).length ) as String

				}
			}
		}
	
		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

		tableView.deselectRow(at: indexPath, animated: true)
		
		if indexPath.section == 1 {

      let controller = storyboard!.instantiateViewController(withIdentifier: "EditorBodyViewController") as! EditorBodyViewController
			controller.singleLine = singleLine
			controller.source = source
			controller.commpletionHandler = { (attributedString: NSAttributedString, singleLine: Bool, preferredTitle: String?) -> Void in
				self.source = attributedString
				self.tableView.reloadData()
				self.singleLine = singleLine
				
				if preferredTitle != nil {
					if self.lawTitle == nil || self.lawTitle?.isEmpty != false {
						self.lawTitle = preferredTitle
					}
				}
				
				self.updateUI()
			}
			
			let nav = CKNavigationController(rootViewController: controller)
			nav.modalPresentationStyle = .formSheet

			present(nav, animated: true, completion: nil)
		}
		
		if indexPath.section == 0 {
			
			if indexPath.row == 0 {
				
				let alert = UIAlertView(title: WLoc("法令名"), message: "法令名（例:万国著作権条約）", delegate: self, cancelButtonTitle: WLoc("Cancel"), otherButtonTitles: WLoc("OK"))
				
				alert.alertViewStyle = UIAlertViewStyle.plainTextInput
				let field = alert.textField(at: 0)
				alert.tag = 0
				field!.keyboardType = .default
				field!.text = lawTitle
				alert.show()
			}
			
			if indexPath.row == 1 {
				
				let alert = UIAlertView(title: WLoc("説明"), message: "改正日、副題等", delegate: self, cancelButtonTitle: WLoc("Cancel"), otherButtonTitles: WLoc("OK"))
				
				alert.alertViewStyle = UIAlertViewStyle.plainTextInput
				let field = alert.textField(at: 0)
				alert.tag = 1
				field!.keyboardType = .default
				field!.text = edition

				alert.show()
			}
			
			if indexPath.row == 2 {
				
				let alert = UIAlertView(title: WLoc("投稿者"), message: "あなたのお名前（筆名）", delegate: self, cancelButtonTitle: WLoc("Cancel"), otherButtonTitles: WLoc("OK"))
				
				
				alert.alertViewStyle = UIAlertViewStyle.plainTextInput
				let field = alert.textField(at: 0)
				alert.tag = 2
				field!.keyboardType = .default
				field!.text = author

				alert.show()
			}
		}
	}
	
	func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
		if buttonIndex != 1 { return }
		
		if alertView.tag == 0 {
			let field = alertView.textField(at: 0)
			let string = field!.text ?? ""
			if string.isEmpty == false  {
				lawTitle = string
			}
		}
		
		if alertView.tag == 1 {
			let field = alertView.textField(at: 0)
			let string = field!.text ?? ""
			if string.isEmpty == false  {
				edition = string
			}
		}

		if alertView.tag == 2 {
			let field = alertView.textField(at: 0)
			let string = field!.text ?? ""
			if string.isEmpty == false  {
				author = string
			}
		}
		
		tableView.reloadData()
		updateUI()
	}
	
	override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
		if section == 0 {
			return WLoc("LawTitleFooter")
		}
		
		if section == 1 {
			return WLoc("LawBodyFooter")
		}
		
		return nil
	}
}

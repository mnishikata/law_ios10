//
//  WaitingViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 14/08/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import UIKit

class WaitingViewController: UIViewController, UIPopoverPresentationControllerDelegate {
	
	@IBOutlet weak var loadingImageView: CircularProgressIndicator!
	
	var progress:Double  {
		set {
			loadingImageView?.progress = newValue
		}
		get {
			if loadingImageView != nil {
				return loadingImageView.progress
			}else {
				return 0.0
			}
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		loadingImageView.indeterminate = true
	}
	
	override var preferredContentSize: CGSize {
		set {}
		get {
			return CGSize(width: 60,height: 60)
		}
	}
	
	// MARK:- Popover
	
	func popoverPresentationControllerShouldDismissPopover( _ popoverPresentationController: UIPopoverPresentationController) -> Bool {
		return false
	}
	
	func adaptivePresentationStyle(for controller: UIPresentationController)
		-> UIModalPresentationStyle {
			return .none
	}
	
	func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection:UITraitCollection)
		-> UIModalPresentationStyle {
			return .none
	}
}

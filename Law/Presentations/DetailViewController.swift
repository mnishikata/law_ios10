//
//  DetailViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2014/08/25.
//  Copyright (c) 2014年 Catalystwo. All rights reserved.
//

import UIKit
import Foundation
import MobileCoreServices
import WebKit
import Firebase
import StoreKit

//@"http://law.e-gov.go.jp/cgi-bin/idxselect.cgi?IDX_OPT=1&H_NAME=%93%C1%8B%96&H_NAME_YOMI=%82%A0&H_NO_GENGO=H&H_NO_YEAR=&H_NO_TYPE=2&H_NO_NO=&H_FILE_NAME=S34HO121&H_RYAKU=1&H_CTG=1&H_YOMI_GUN=1&H_CTG_GUN=1"

//http://law.e-gov.go.jp/htmldata/S34/S34HO121_IDX.html

/*
 
 BUG: Split -> choose another law -> tab will not work
 */
//ERROR

class MyAction: NSObject, UIActivityItemSource {
  
  var htmlSource: String? = nil
  
  func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
    return Data()
  }
  
  func activityViewController( _ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivityType) -> Any? {
    
    if let data = htmlSource?.data(using: String.Encoding.shiftJIS, allowLossyConversion: true) {
      return data
    }
    return htmlSource
  }
  
  func activityViewController( _ activityViewController: UIActivityViewController, dataTypeIdentifierForActivityType activityType: UIActivityType?) -> String {
    
    return "public.html"
  }
}

enum ExcerptType: Int {
  case full = 0, excerptArticle, excerptSingleSubartcile
}

class DetailViewController: ViewControllerWithUtility, StackNavigation, DetailViewDelegate  {
  
  var viewModel: DetailViewModel! {
    didSet {
      viewModel.delegate = self
      lawTabView?.reload()
    }
  }
  
  func detailItemDidChange() {
    if let nav = navigationController as? StackNavigationController {
      nav.saveState()
    }
    lawTabView?.reload()
  }
  
  var searchBarButtonItem: UIBarButtonItem? = nil
  var jumpBarButtonItem: UIBarButtonItem? = nil
  var tabBarButtonItem: UIBarButtonItem? = nil
  var mokujiButtonItem: UIBarButtonItem? = nil
  var highlightingURLSpecifier: String? = nil
  
  weak var reviewBaseView_: UIView? = nil
  var pinchGesture_: UIPinchGestureRecognizer? = nil
  
  var utilityViewContollerToAddWhenViewLoaded: UIViewController? = nil
  var highlightViewController_: HighlightViewController? = nil
  
  
  /// View
  weak var webView_ui: SearchWebView! = nil
  weak var webView_wk: SearchWKWebView! = nil
  
  /// View Accessor
  var theWebView: WebSearcher! {
    if webView_ui != nil { return webView_ui }
    else { return webView_wk }
  }
  
  weak var lawTabView: LawTabView? = nil
  weak var dragginShade: UIView? = nil
  weak var myToolbar: UIToolbar? = nil
  func evaluateJavaScript( _ javaScriptString: String!, completionHandler: ((Any?, Error?) -> Void)!) {
    
    if webView_ui != nil {
      let result = webView_ui.stringByEvaluatingJavaScript( from: javaScriptString )
      if completionHandler != nil { completionHandler( result, nil ) }
      
    }else {
      webView_wk.evaluateJavaScript( javaScriptString, completionHandler: completionHandler)
    }
  }
  
  // MARK:- BODY
  
  required init(restorationInfo: [RestorationKey: Any]?) {
    super.init(nibName: nil, bundle: nil)
    self.viewModel = DetailViewModel(delegate: self)
    viewModel.traitCollection = self.traitCollection
    if restorationInfo != nil {
      viewModel.loadFromRestorationInfo(restorationInfo!)
    }
  }
  
  required init(coder: NSCoder) {
    super.init(nibName: nil, bundle: nil)
  }
  
  deinit {
    let center = NotificationCenter.default
    center.removeObserver(self)
    if isViewLoaded {
    theWebView.scrollView.removeObserver(self, forKeyPath: "contentOffset")
    }
  }
  
  func loadFromRestorationInfo(_ restorationInfo: [RestorationKey: Any]) {
    viewModel.loadFromRestorationInfo(restorationInfo)
    
    if let stackPlist = restorationInfo[.utilities] as? StackNavigationStorage {
      let viewControllers = StackNavigationViewModel.stackViewControllersFromPlist(stackPlist)
      if viewControllers.count > 0 {
        let nav = StackNavigationController(rootViewController: viewControllers[0], tab: nil )
        nav.setViewControllers(viewControllers, animated: false)
        utilityViewContollerToAddWhenViewLoaded = nav
        //				setUtilityViewController(nav)
      }
    }
  }
  
  func instantiateWebAndTab() {
    let backgroundColorScheme = viewModel.backgroundColorScheme
    self.view.backgroundColor = backgroundColorScheme.backgroundColor
    
    let webFrame = viewModel.webFrame(withViewBounds: contentView.bounds)

    if UserDefaults.standard.bool(forKey: "UseWKWebView_debug") {
      
      let view = SearchWKWebView(frame: webFrame)
      webView_wk = view
      webView_wk.highlightingDelegate = self
      
      webView_wk.autoresizingMask = [.flexibleHeight, .flexibleWidth]
      self.view.addSubview( webView_wk )
      
      webView_wk.navigationDelegate = self
      
      webView_wk.backgroundColor = backgroundColorScheme.backgroundColor
      webView_wk.scrollView.backgroundColor = backgroundColorScheme.backgroundColor
      if viewModel.contentOffsetY_ != nil {
      webView_wk.scrollView.contentOffset = CGPoint(x: 0, y: viewModel.contentOffsetY_!)
      }
      webView_wk.scrollView.addObserver(self, forKeyPath: "contentOffset", options: .new, context: nil)

      if backgroundColorScheme == .Dark || backgroundColorScheme == .Grey {
        webView_wk.isOpaque = false
      }
      contentView.addSubview( webView_wk )
      
    }else {
      
      /* Instantiate UIWebView */
      if webView_ui == nil {
        let view = SearchWebView(frame: webFrame)
        webView_ui = view
        webView_ui.scrollView.addObserver(self, forKeyPath: "contentOffset", options: .new, context: nil)
        webView_ui.scrollView.decelerationRate = UIScrollViewDecelerationRateNormal
        contentView.addSubview( view )
      }
      webView_ui!.frame = webFrame
      webView_ui.dataDetectorTypes = UIDataDetectorTypes()
      webView_ui.highlightingDelegate = self
      
      webView_ui.autoresizingMask = [.flexibleHeight, .flexibleWidth]
      webView_ui.delegate = self
      webView_ui.scrollView.backgroundColor = backgroundColorScheme.backgroundColor
      if viewModel.contentOffsetY_ != nil {
      webView_ui.scrollView.contentOffset = CGPoint(x: 0, y: viewModel.contentOffsetY_!)
      }
      webView_ui.backgroundColor = backgroundColorScheme.backgroundColor

      webView_ui.isOpaque = false
      webView_ui.translatesAutoresizingMaskIntoConstraints = false
    }
    
    // TOOLBAR
    if viewModel.insideUtilityViewController == false {
      
      let toolbar = UIToolbar(frame: viewModel.toolbarFrame(withViewBounds: self.view.bounds))
      toolbar.autoresizingMask = [.flexibleTopMargin, .flexibleWidth]
      toolbar.isTranslucent = false
      toolbar.backgroundColor = backgroundColorScheme.backgroundColor
      toolbar.barTintColor = backgroundColorScheme.backgroundColor
      //toolbar.setShadowImage(UIImage(), forToolbarPosition: UIBarPosition.bottom)
      // toolbar.setBackgroundImage(UIImage(), forToolbarPosition: .bottom, barMetrics: .default)
      // toolbar.setBackgroundImage(UIImage(), forToolbarPosition: .bottom, barMetrics: .compact)

      self.view.addSubview( toolbar )
      myToolbar = toolbar
    }
    
    //TODO: USE AUTOLAYOUT
    
    if viewModel.usesOldStyleTab == false {
      let frame = viewModel.lawTabViewFrame(withViewBounds: self.view.bounds)
      
      let tabBar: LawTabView
      
      tabBar = LawTabView.shared
      tabBar.frame = frame
      tabBar.colorScheme = backgroundColorScheme
      tabBar.delegate = self
      tabBar.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
      tabBar.translatesAutoresizingMaskIntoConstraints = false
      tabBar.reload()
      self.lawTabView = tabBar
      self.view.addSubview( tabBar )
      
      if myToolbar != nil {
        self.view.bringSubview(toFront: myToolbar!)
        TabDataSource.shared.tabBarDelegate = self
      }
    }else {
      lawTabView?.removeFromSuperview()
      lawTabView = nil
    }
  }
  
  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    if keyPath == "contentOffset" {
      if isViewLoaded == true, let webView = theWebView {
        if webView.scrollView.contentOffset.y != 0 {
          viewModel.contentOffsetY_ = webView.scrollView.contentOffset.y
        }
      }
    }else
    {
      super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    instantiateWebAndTab()
    
    // NAV
    
    let titleView = LawNavigationTitleView(frame: CGRect(x:0, y:0, width:2000, height:44))
    let title = viewModel.lawTitle ?? ""
    titleView.title = title
    titleView.colorScheme = viewModel.backgroundColorScheme
    titleView.target = self
    titleView.action = #selector(DetailViewController.reload(_:))
    navigationItem.titleView = titleView
    
    let backItem = UIBarButtonItem(title: title, style: .plain, target: nil, action: nil)
    navigationItem.backBarButtonItem = backItem
    
    if viewModel.insideUtilityViewController == false {
      let textSizeItem = UIBarButtonItem(image: UIImage(named: "TextSize"), style: .plain, target: self, action: #selector(textSize(_:)))
      navigationItem.rightBarButtonItem = textSizeItem
    }
    
    // Check for update
    viewModel.checkForUpdates {
      self.updateRightNavigationItem(false)
    }
    
    if pinchGesture_ == nil {
      let gesture = UIPinchGestureRecognizer(target: self, action: #selector(pinch(_:)))
      self.view.addGestureRecognizer(gesture)
      pinchGesture_ = gesture
    }
    
    if let isHttpURL = viewModel.isHttpURL {
      if  isHttpURL == false {
        
        loadFromFile() { [weak self] (success: Bool) -> Void in
          
          if success == true { return }
          self?.downloadFromWebsite()
          
        }  // Load from file end
        
      }else{
        downloadFromWebsite()
      }
    }

    let center = NotificationCenter.default
    center.addObserver(self, selector: #selector(reloadFromFile(_:)), name: FilePackage.FilePackageLoadedPackages, object: nil)
    center.addObserver(self, selector: #selector(didEnterBackground), name: .UIApplicationDidEnterBackground, object: nil)
    center.addObserver(self, selector: #selector(settingsDidChange), name: LawNotification.settingsDidChange, object: nil)
    center.addObserver(self, selector: #selector(highlightDidChange), name: LawNotification.highlightDidChangeExternally, object: nil)

    if utilityViewContollerToAddWhenViewLoaded != nil {
      setUtilityViewController(utilityViewContollerToAddWhenViewLoaded!, animated: false)
      utilityViewContollerToAddWhenViewLoaded = nil
    }
  }
  
  override func setUtilityViewController(_ controller: UIViewController?, animated: Bool = true) {
    
    let frame = viewModel.webFrame(withViewBounds: self.view.bounds, inUtility: controller != nil)
    webView_ui!.frame = frame
    super.setUtilityViewController(controller, animated: animated)
  }
  
  func review() {
    
    viewModel.didReview()
    
    reviewBaseView_?.removeFromSuperview()
    reviewBaseView_ = nil
    
    if #available(iOS 10.3, *) {
      SKStoreReviewController.requestReview()
    } else {
      
      let address = "itms-apps://itunes.apple.com/app/law/id916069107?ls=1&mt=8&action=write-review"
      let url = URL(string: address)!
      UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    // LOAD FROM RESTORATION INFO
    if viewModel.restoredState != nil {
      loadFromRestorationInfo(viewModel.restoredState!)
      loadFromFile({ (success) in })
      
      viewModel.restoredState = nil
    }
    
    viewModel.traitCollection = self.traitCollection

    // navigationController!.setToolbarHidden(insideUtilityViewController, animated: false)
    navigationController!.setToolbarHidden(true, animated: false)
    navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
    navigationItem.leftItemsSupplementBackButton = true
    
    updateToolbar(false)
    
    let center =  NotificationCenter.default
    center.addObserver(self, selector: #selector(orientationDidChange), name: LawNotification.splitViewOrientationDidChange, object: nil)
    center.addObserver(self, selector: #selector(tabIdentifiersDidChange), name: TabDataSource.tabIdentifiersDidChange, object: nil)
    center.addObserver(self, selector: #selector(ubiquitousKeyValueStoreDidChange), name: NSUbiquitousKeyValueStore.didChangeExternallyNotification, object: nil)
    
    
    if viewModel.doJump {
      //Execute later
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.001) {
        self.jump(self.jumpBarButtonItem)
      }
    }
    
    
    if viewModel.usesOldStyleTab == false && self.lawTabView != nil  {
      self.lawTabView!.delegate = self
      let frame = viewModel.lawTabViewFrame(withViewBounds: self.view.bounds)

      self.lawTabView!.frame = frame
      self.lawTabView?.reload()

      self.view.addSubview( self.lawTabView! )

      if myToolbar != nil {
        self.view.bringSubview(toFront: myToolbar!)
      }
    }
    
    reviewBaseView_?.superview?.bringSubview(toFront: reviewBaseView_!)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated);
    viewModel.viewAppeared = true
    
    let frame = viewModel.lawTabViewFrame(withViewBounds: self.view.bounds)
    self.lawTabView?.frame = frame
    self.lawTabView?.reload()
    
    // Only Me!
    splitViewController?.presentsWithGesture = ( navigationController?.viewControllers.count == 1 )
    
    if presentedViewController == nil {
      workaroundUIMenuIssue()
    }
    
    if viewModel.doJump {
      jump(jumpBarButtonItem)
    }
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    viewModel.viewAppeared = false
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    
    let nav = navigationController as? StackNavigationController
    nav?.saveState()
    
    findClear()
    
    let center = NotificationCenter.default
    center.removeObserver(self, name: LawNotification.splitViewOrientationDidChange, object: nil)
    center.removeObserver(self, name: TabDataSource.tabIdentifiersDidChange, object: nil)
    center.removeObserver(self, name: NSUbiquitousKeyValueStore.didChangeExternallyNotification, object: nil)
    
    super.viewWillDisappear(animated)
  }
  
  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    super.willTransition(to: newCollection, with: coordinator)
    viewModel.traitCollection = newCollection
  }
  
  func traitCollectionDidChange() {
    guard isViewLoaded == true else { return }
    
    lawTabView?.frame = viewModel.lawTabViewFrame(withViewBounds: self.view.bounds)
    myToolbar?.frame = viewModel.toolbarFrame(withViewBounds: self.view.bounds)
    webView_ui?.frame = viewModel.webFrame(withViewBounds: contentView.bounds)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
    viewModel.toc2_ = nil
    viewModel.rawSource_ = nil
    
    // IF NOT APPEARED (NOT FRONT MOST), REMOVE HTML FROM WEB VIEW
    if viewModel.viewAppeared == false && viewModel.restoredState == nil {
      if isViewLoaded {
        theWebView.myLoadHTMLString("", baseURL: nil)
      }
      
      viewModel.restoredState = restorationInfo()
      
      viewModel.htmlSource = nil
      viewModel.linkDictionaries = []
      //detailItem = nil x DETAIL ITEM IS USED WHEN EDITING NOTES
    }
  }
  
  func didEnterBackground() {
    
    if presentedViewController != nil && !(presentedViewController! is EGovDownloader) {
      dismiss(animated: false, completion: nil)
    }
  }
  
  func restorationInfo() -> [RestorationKey: Any] {
    var dict = viewModel.restorationInfo()
    if let utility = utilityViewController as? StackNavigationController {
      let plist = utility.viewModel.stackPlistFromViewControlelrs(utility.viewControllers)
      dict[.utilities] = plist
    }
    return dict
  }
  
  func updateRightNavigationItem(_ downloading: Bool) {
    
    if viewModel.insideUtilityViewController { return }
    
    let refreshImage = UIImage(named: "Refresh")!.colorizedImage(withTint: self.view.tintColor, alpha: 1.0, glow: false)
    let information = FFBadgedBarButtonItem(image: refreshImage, target: self, action: #selector(reload(_:)))
    guard let navTitle = navigationItem.titleView as? LawNavigationTitleView else { return }

    /* has update */
    if viewModel.requiresUpdates == .hasUpdate {
      navTitle.badgeType = .hasUpdate
      information?.badge = "1"
      information?.alertStyle = true
    }
    
    if viewModel.requiresUpdates == .upToDate {
      navTitle.badgeType = .upToDate

      information?.badge = nil
      information?.alertStyle = false
    }
    
    if viewModel.requiresUpdates == .unknown {
      navTitle.badgeType = .reload
      information?.badge = nil
    }
  }
  
  func mokuji(_ sender: UIBarButtonItem) {
    
    if presentedViewController != nil {
      dismiss(animated: false, completion: nil)
    }
    let controller = MyTabBarController(viewModel: viewModel)
    
    ////
    let toc = TOCTableViewController(viewModel: viewModel, viewSize: self.view.bounds.size)
    toc.jumpAction = { linkDictionary in
      let link = linkDictionary.link
      let script = "document.location.href = \"\(link)\""
      self.evaluateJavaScript(script, completionHandler:nil )
      self.dismiss(animated: true, completion: nil)
    }
    
    /////
    let bookmark = BookmarkTableViewController(detailViewModel: viewModel, viewSize: self.view.bounds.size)
    bookmark.jumpAction = { bookmark in
      self.viewModel.anchor = bookmark.anchor
      if let anchorStringScript = self.viewModel.javascriptForAnchor {
        self.evaluateJavaScript(anchorStringScript, completionHandler: nil)
      }
      self.dismiss(animated: true, completion: nil)
    }
    
    ////
    let highlgiht = HighlightListTableViewController(detailViewModel: viewModel, viewSize: self.view.bounds.size)
    highlgiht.jumpAction = { highlight in
      
      
      self.theWebView.blink(highlight.uuid)
      self.dismiss(animated: true, completion: nil)
    }
    
    controller.viewControllers = [toc, bookmark, highlgiht]
    
    let popover = controller.popoverPresentationController
    
    popover?.permittedArrowDirections = .any
    popover?.barButtonItem = sender
    popover?.delegate = self
    popover?.overrideTraitCollection = UITraitCollection(verticalSizeClass: .compact)
    popover?.backgroundColor = viewModel.backgroundColorScheme.popoverBackgroundColor
    
    present(controller, animated: true, completion: nil)
  }
  
  func settingsDidChange() {
    
    instantiateWebAndTab()
    theWebView.updateMenuItems()
    loadFromFile()  { (success: Bool) -> Void in }
    //self.view.tintColor
    updateToolbar(false)
  }
  
  func highlightDidChange(notification: NSNotification) {
    guard let dict = notification.userInfo else { return }
    guard let uuid = dict["uuid"] as? String else { return }
    guard let snippet = dict["snippet"] as? String else { return }
    theWebView.unhighlight( uuid, snippet: snippet)
  }
  
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    return  [.all]
  }
  
  func orientationDidChange(){
    
    if presentedViewController?.modalPresentationStyle == .popover && !(presentedViewController! is EGovDownloader) {
      presentedViewController?.dismiss(animated: true, completion: nil)
    }
    highlightViewController_ = nil;
  }
  
  func pinch(_ gesture: UIPinchGestureRecognizer) {
    
    let scale = gesture.scale
    
    if gesture.state == .changed {
      evaluateJavaScript(viewModel.javascript(toZoom: scale), completionHandler: nil)
    }
    
    if gesture.state == .ended {
      viewModel.webViewScale = viewModel.webViewScale * Double(scale)
    }
  }
  
  func reloadFromFile(_ notification: Notification) {
    
    guard isViewLoaded == true else { return }
    
    if let packageNames = (notification as NSNotification).userInfo?["ModifiedPackageNames" as NSString] as? NSArray,
      let packageName = viewModel.packageURL?.lastPathComponent,
      packageNames.index( of: packageName ) != NSNotFound {
      
      loadFromFile()  { (success: Bool) -> Void in }
    }
  }
  
  // LOAD FULL HTML OR AN EXCERPT
  func loadFromFile(_ completionHandler: ((_ success: Bool) -> Void)!)   {
    
    if viewModel.excerptType != .full && viewModel.anchor != nil {
      
      //TODO: HANDLING ELEMENT IS NOT ELEGANT
      viewModel.updateExcerptElement()
      
      if viewModel.excerptElement != nil {
        jumpToAnchorExcerpt(viewModel.anchor!, inElement: viewModel.excerptElement! )
        completionHandler(true)
        return
      }
    }
    
    if viewModel.excerptType != .full && viewModel.anchor == nil {
      return // START AS EMPTY PAGE
    }
    
    //		let useExcerptJump = NSUserDefaults.standardUserDefaults().boolForKey("JumpViewControllerExcerptDisplayMode")
    //		if useExcerptJump && startJump { return }
    
    let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)
    queue.async {
      guard let packageURL = self.viewModel.packageURL else { return }
      
      self.viewModel.loadFrom(url: packageURL)
      
      DispatchQueue.main.async {
        
        if let contentHtml = self.viewModel.htmlSource {
          // DEBUG
          #if (arch(i386) || arch(x86_64)) && os(iOS)
            
            if gDebugMode {
              let path = "/Users/Masa/Desktop/CurrentLawHTML.html";
              
              do {
                try contentHtml.write(toFile: path, atomically: false, encoding: String.Encoding.utf8.rawValue)
                
              }catch {
                print("Failed to write")
              }
            }
            
          #endif
          
          let title = contentHtml.titleOfHTML
          
          ObjectiveCClass.track(title as String)
          self.viewModel.htmlSource = contentHtml
          self.theWebView.myLoadHTMLString( NSMutableString(string: contentHtml), baseURL: nil)
          completionHandler(true)
          
        }else {
          completionHandler(false)
          
        }
      }
    }
  }
  
  func downloadFromWebsite() {
    
    
    //			let SAMPLE_URL = "http://law.e-gov.go.jp/htmldata/S34/S34HO121.html"
    //			let SAMPLE_INDEX_URL = "http://law.e-gov.go.jp/htmldata/S34/S34HO121_IDX.html"
    //
    //			var indexURL = NSURL.URLWithString(SAMPLE_INDEX_URL);
    //			var theURL = NSURL.URLWithString(SAMPLE_URL);
    
    
    // 1. packageURLがサイトURLの場合
    guard let lawEGovURL = viewModel.packageURL else { return }
    //NSURL.URLWithString("http://law.e-gov.go.jp/cgi-bin/idxselect.cgi?IDX_OPT=1&H_NAME=%8F%A4%95%57%96%40&H_NAME_YOMI=%82%A0&H_NO_GENGO=H&H_NO_YEAR=&H_NO_TYPE=2&H_NO_NO=&H_FILE_NAME=S34HO127&H_RYAKU=1&H_CTG=1&H_YOMI_GUN=1&H_CTG_GUN=1")
    
    var downloadingURL: URL? = nil
    
    let absoluteString = lawEGovURL.absoluteString
    if absoluteString.hasPrefix("http://law.e-gov.go.jp") == true {
      
      downloadingURL = lawEGovURL
      
    } else {
      
      // 1. packageURLがローカルファイルの場合
      // file:///
      
      let ( plist, _, _ ) =  FilePackage.shared.loadPackage(at: lawEGovURL, onlyPlist: true )
      
      if let sourcePath = plist?[DocPropertyKey.sourceURL] as? String {
        downloadingURL = URL(string: sourcePath )
      }
    }
    
    
    //
    // Start Downloading
    //
    
    if downloadingURL != nil {
      
      // This method is called *before* even this view controller is presented.  Do below code some time later
      
      //Execute later
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
        
        let controller = EGovDownloader(downloadingURL: downloadingURL!)
        controller.modalPresentationStyle = .popover
        let popover = controller.popoverPresentationController
        popover?.delegate = controller
        popover?.permittedArrowDirections = .any
        
        if self.navigationController != nil {
          popover?.sourceView = self.view
          popover?.sourceRect = CGRect( x: self.view.bounds.midX, y: self.navigationController!.navigationBar.frame.height, width: 1, height: 20)
        }
        
        controller.completionHandler = { [weak self] (success: Bool, error: Error?, packageURL: URL?) -> Void in
          guard let strongSelf = self else { return }

          defer {
            strongSelf.dismiss(animated: true, completion: {})
          }
          
          if self == nil { return }
          
          if success == true && packageURL != nil  {
            
            strongSelf.viewModel.loadFrom(url: packageURL!)
            
            if let htmlSource = strongSelf.viewModel.htmlSource {
              strongSelf.theWebView.myLoadHTMLString( NSMutableString(string:htmlSource), baseURL: nil)
            }
            strongSelf.title = strongSelf.viewModel.title
            
            // Update badge
            strongSelf.viewModel.requiresUpdates = .upToDate
            
            //Execute later
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
              strongSelf.updateRightNavigationItem(false)
            }
            
          }else if error != nil {
            ShowAlertMessage( "", error!.localizedDescription, self )
          }
        }
        
        self.present(controller, animated: true, completion: nil)
      }
    }
  }
  
  func reload(_ sender: UIView) {
    
    guard presentedViewController == nil else { return }
    guard viewModel.insideUtilityViewController == false else { return }
    guard GCNetworkReachability.forInternetConnection().isReachable() == true else {
      displayErrorAlertIfNecessary(LawError.offline, in: self)
      return
    }
    
    guard let packageURL = viewModel.packageURL else { return }
    
    // clear cache
    viewModel.toc2_ = nil
    
    let ( plist, _, _ ) = FilePackage.shared.loadPackage(at: packageURL,  onlyPlist: true )
    
    if let sourcePath = plist?[DocPropertyKey.sourceURL] as? String {
      
      let downloadingURL = URL(string: sourcePath )
      
      let alertController = UIAlertController(title: viewModel.reloadAlertTitle, message: viewModel.reloadAlertMessage, preferredStyle: .actionSheet)
      
      alertController.popoverPresentationController?.delegate = self
      alertController.popoverPresentationController?.sourceRect = sender.frame
      alertController.popoverPresentationController?.sourceView = sender.superview
      let action1 = UIAlertAction(title: WLoc("Retrieve"), style: .default) { (action) -> Void in
        
        FilePackage.shared.resetUpdates( packageURL )
        self.viewModel.requiresUpdates = .unknown
        self.updateRightNavigationItem(false)
        
        
        var controller: EGovDownloader?
        
        // 1. Download from Cloud Kit
        
        if sourcePath.hasPrefix("user:") {
          let fileUUID = (sourcePath as NSString).substring(from: 5)
          
          controller = EGovDownloader(packageBundle: fileUUID)
          
        }else {
          // 2. Download from eGov
          
          let packageBundle = sourcePath.getUrlQueryParameters()["H_FILE_NAME"]
          if packageBundle != nil && self.viewModel.editionToUpdate != nil && self.viewModel.editionToUpdate!.isEmpty == false {
            
            controller = EGovDownloader(downloadingURL: downloadingURL!, packageBundle: packageBundle!, edition: self.viewModel.editionToUpdate!)
            
          }else {
            
            controller = EGovDownloader(downloadingURL: downloadingURL!)
          }
        }
        
        controller!.modalPresentationStyle = .popover
        
        let popover = controller!.popoverPresentationController
        
        popover?.delegate = controller
        popover?.permittedArrowDirections = .up
        //				popover?.barButtonItem = sender //
        
        if self.navigationController != nil {
          popover?.sourceView = self.view
          popover?.sourceRect = CGRect( x: self.view.bounds.midX, y: 0, width: 1, height: 1)
        }
        controller!.completionHandler = { (success: Bool, error: Error?, packageURL: URL?) -> Void in
          
          self.dismiss(animated: true, completion: nil)
          
          if success == true && packageURL != nil {
            
            self.viewModel.loadFrom(url: packageURL!)
            if let htmlSource = self.viewModel.htmlSource {
              self.theWebView.myLoadHTMLString( NSMutableString(string: htmlSource), baseURL: nil)
            }
            self.title = self.viewModel.title
            self.viewModel.requiresUpdates = .upToDate
            
            //Execute later
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
              self.updateRightNavigationItem(false)
            }
            
          }else if error != nil {
            ShowAlertMessage( "", error!.localizedDescription, self )
          }
        }
        
        self.present(controller!, animated: true, completion: nil)
        
      }
      
      let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in
      }
      
      alertController.addAction(action1)
      alertController.addAction(cancel)
      present(alertController, animated: true, completion: nil)
    }
  }
  
  func tabIdentifiersDidChange() {
    updateToolbar(true)
  }
  
  /// MARK:- Nav button
  
  func displayDocument(_ packageURL: URL, anchorText: String) {
    
    if viewModel.excerptType != .full {
      let fp = FilePackage.shared

      let toc2 = FilePackage.shared.loadHTMLElements(for: packageURL)
      
      let ( _, linkDictionaries, rawSource) = fp.loadPackage(at: packageURL, onlyPlist: false, zoomScale: self.viewModel.webViewScale, rawHTML: true)
      let (_,element) = fp.anchor(for: anchorText, in: packageURL, toc2: toc2, toc1: linkDictionaries)
      
      if rawSource == nil { return }
      
      viewModel.excerptElement = element
    }
    
    
    viewModel.loadFrom(url: packageURL, anchorText: anchorText)

    (navigationItem.titleView as? LawNavigationTitleView)?.title = viewModel.title
    
    evaluateJavaScript("document.body.innerHTML = ''", completionHandler: nil)
    viewModel.hasFinishedLoading = false
    viewModel.startJump = true

    loadFromFile() { (success: Bool) -> Void in }
  }
  
  func jump(_ sender: Any?) {
    
    // AOHON
    
    let link = "comcatalystwoaohon:"
    let appLink = URL(string: link)!
    if UserDefaults.standard.bool(forKey:"AohonMessageDisplayed") != true && UIApplication.shared.canOpenURL(appLink) == false  {
      
      if viewModel.shouldSuggestsAohonReaderApp {
        let controller = UIAlertController( title: "おしらせ", message: WLoc("AohonMessage"), preferredStyle: .alert)
        let goAction = UIAlertAction(title: WLoc("AohonGo"), style: .default) { (action) -> Void in
          let appstoreLink = URL(string: "http://catalystwo.com/AohonReaderNewVersion.html")!
          UIApplication.shared.open(appstoreLink, options: [:], completionHandler: nil)
        }
        let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in }
        controller.addAction(goAction)
        controller.addAction(cancel)
        controller.modalPresentationStyle = .popover
        present(controller, animated: true, completion: nil)
        
        UserDefaults.standard.set(true, forKey:"AohonMessageDisplayed")
        return

      }
    }
    
    
    guard let toc2 = viewModel.toc2 else { return }
    let nav = navigationController as! StackNavigationController
    nav.saveState()
    
    if presentedViewController != nil {
      dismiss(animated: false, completion: nil)
    }
    
    let controller = JumpViewController(showTab: true)
    controller.toc2 = toc2
    controller.backgroundColorScheme = viewModel.backgroundColorScheme
    controller.jumpAction = { text, tabIdentifierOrDocItem, excerptModeDummy in
      
      if tabIdentifierOrDocItem == nil {

        let (anchor, element, script) = self.viewModel.anchor(for: text!)
        
        if anchor != nil {
          if self.viewModel.excerptType != .full {
            if self.viewModel.rawSource == nil { return }
            
            self.viewModel.excerptElement = element
            self.jumpToAnchorExcerpt(anchor!, inElement: element!)
            
          }else {
            if self.viewModel.hasFinishedLoading == false {
              self.viewModel.startJump = true
              self.viewModel.anchor = anchor // THIS CAUSES DID SET for detailItem because detailItem is struct
              //self.loadFromFile() { (success: Bool) -> Void in }
              //self.webView_wk.jumpTo(anchor!)
              
            }else {
              self.evaluateJavaScript(script, completionHandler: nil)
            }
          }
        }
        self.dismiss(animated: false) { [weak self] in
          
          if anchor != nil {
            Analytics.logEvent(AnalyticsEventSearch, parameters: [
              AnalyticsParameterItemName: (self?.viewModel.title as NSString? ?? "n/a" as NSString),
              AnalyticsParameterSearchTerm: anchor! as NSString
              ])
          }
          
          self?.workaroundUIMenuIssue()
        }
        return
      }
      
      let mstr = NSMutableString(string: text!)
      if let identifier = tabIdentifierOrDocItem as? String { //TODO:
        let fp = FilePackage.shared
        fp.encodeForTOC1(mstr)
        self.dismiss(animated: false) {  [weak self] in
          let _ = TabDataSource.shared.selectTabIdentifier( identifier, jumpTo: mstr as String )
          // SEE setRestoreIdentifier in StackNavigationController
          self?.workaroundUIMenuIssue()
        }
        
      }else if let dict = tabIdentifierOrDocItem as? DocItem {
        self.displayDocument(dict.packageURL! as URL, anchorText: text! )
        self.dismiss(animated: false) { [weak self] in
          self?.workaroundUIMenuIssue()
        }
      }
    }
    
    controller.cancelBlock = { (swtichToFind: Bool) -> () in
      
      self.dismiss(animated: false) { [weak self] in
        if swtichToFind == true { self?.search() }
        self?.workaroundUIMenuIssue()
      }
    }
    
    controller.pickerBlock = {
      self.dismiss(animated: false) { [weak self] in
        self?.elementPicker()
        self?.workaroundUIMenuIssue()
      }
    }
    
    
    controller.modalPresentationStyle = .popover
    
    if let popover = controller.popoverPresentationController {
      popover.permittedArrowDirections = .down
      if controller.popoverStyle || self.traitCollection.userInterfaceIdiom == .pad {
        popover.barButtonItem = sender as? UIBarButtonItem
      }else {
        popover.sourceView = self.view
        popover.sourceRect = CGRect( x: self.view.bounds.maxX - 20, y: self.view.bounds.maxY - controller.keyPadHeight + 44, width: 1, height: 1 )
      }
      popover.delegate = self
    }
    
    present(controller, animated: false, completion: nil)
  }
  
  func workaroundUIMenuIssue() {
    
    // https://forums.developer.apple.com/thread/17916
    // Work around for UIMenuController issue
    
    theWebView.scrollView.subviews.first!.becomeFirstResponder()
    OperationQueue.main.addOperation( { self.becomeFirstResponder() } )
  }
  
  func search() {
    
    if viewModel.excerptType != .full {
      viewModel.excerptType = .full
      loadFromFile { success in }
    }
    
    evaluateJavaScript("expanded") { [weak self] (obj, error) -> Void in
      guard let strongSelf = self else { return }
      if let _ = strongSelf.presentedViewController {
        strongSelf.dismiss(animated: false, completion: nil)
      }
      
      let contentViewController = SearchViewController()
      contentViewController.backgroundColorScheme = strongSelf.viewModel.backgroundColorScheme
      contentViewController.modalPresentationStyle = .popover
      contentViewController.searchViewControllerShouldCancelBlock = {
        strongSelf.dismiss(animated: true, completion: { strongSelf.workaroundUIMenuIssue() })
      }
      contentViewController.searchViewControllerExpandBlock = {
        let script = "display()"
        strongSelf.evaluateJavaScript(script, completionHandler: nil)
      }
      contentViewController.searchViewControllerDoSearchBlock = { (text: String?) -> () in
        
        if text != nil {
          strongSelf.findString( text!, position: nil )
        }
        strongSelf.dismiss(animated: false, completion: { strongSelf.workaroundUIMenuIssue() })
      }
      
      let popover = contentViewController.popoverPresentationController
      popover?.permittedArrowDirections = .any
      popover?.barButtonItem = strongSelf.searchBarButtonItem
      popover?.delegate = self
      
      if obj is NSNumber {
        
        if (obj as! NSNumber).boolValue == true {
          contentViewController.hideHeader()
        }
        
      }else	if obj as? String == "true" {
        contentViewController.hideHeader()
      }
      
      strongSelf.present(contentViewController, animated: false, completion: nil)
    }
  }
  
  func jumpToAnchorExcerpt(_ anchor: String, inElement element: ElementDescriptor) {
    guard let source = viewModel.rawSource else { debugPrint("** jumpToAnchorExcerpt source is nil"); return }
    
    let range = element.range
    if range.location == NSNotFound { debugPrint("** jumpToAnchorExcerpt range not found"); return }
    if NSMaxRange(range) > source.length { debugPrint("** jumpToAnchorExcerpt wrong range"); return }
    
    viewModel.anchor = anchor
    
    var anchorRange = element.lookForRange(for: anchor)
    
    let snippet = NSMutableString(string: source.substring(with: range))
    
    anchorRange.location -= range.location
    
    if anchorRange.location > 0 && NSMaxRange(anchorRange) <= snippet.length && element.anchorIsFirstArticle(anchor) == false {
      snippet.replaceCharacters(in: NSMakeRange(anchorRange.location, 0), with: "</br>📍")
    }
    
    
    snippet.insert("<html><head><STYLE type=\"text/css\"></STYLE></head><body><a name=\"\(element.anchor)\"></a>", at: 0)
    snippet.append("</body></html>")
    snippet.replaceCSS(stripCSSInstead: false, zoomScale: self.viewModel.webViewScale)
    
    snippet.convertHTMLtoIncludeNotes(allHighlights: viewModel.allHighlights, allNotes: viewModel.allNotes, showTooltip: false)
    
    viewModel.startJump = true
    theWebView.myLoadHTMLString(snippet, baseURL: nil)
    viewModel.hasFinishedLoading = false
    
  }
  
  var debugElementArray:[ElementDescriptor]? = nil
  func elementPicker() {
    guard let _ = viewModel.rawSource else { return }
    let fp = FilePackage.shared
    
    if debugElementArray == nil {
      debugElementArray = fp.loadHTMLElements(for: viewModel.packageURL!)
    }
    
    let controller = HTMLElementPicker( dataSource: debugElementArray! )
    controller.modalPresentationStyle = .popover
    controller.jumpToAnchorBlock = { (anchor, range, element) in
      if anchor != nil {
        self.jumpToAnchorExcerpt(anchor!, inElement: element )
        self.viewModel.excerptElement = element
      }
    }
    
    controller.willClose = {
      //			self.detailItem!.anchor = savedAnchor as String?
      //			self.startJump = true
      //			(self.theWebView as! UIWebView).loadHTMLString(self.theWebView.source()!, baseURL: nil)
      
    }
    let popover = controller.popoverPresentationController
    popover?.permittedArrowDirections = .any
    popover?.barButtonItem = jumpBarButtonItem
    popover?.delegate = self
    
    present(controller, animated: false, completion: nil)
  }
  
  func contentOffsetYDidChange() {
    guard isViewLoaded else { return }
    if viewModel.contentOffsetY_ == nil {
      let _ = navigationController?.popViewController(animated: true)
     return
    }
    
    if viewModel.contentOffsetY_ != theWebView.scrollView.contentOffset.y {
      theWebView.scrollView.contentOffset = CGPoint(x: 0, y: viewModel.contentOffsetY_!)
    }
  }
  
  func goBack() {
    viewModel.goBack()
    updateToolbar(false)
  }
  
  func goFoward() {
    
    viewModel.goFoward()
    updateToolbar(false)
  }
  
  func ubiquitousKeyValueStoreDidChange() {
    tabBarButtonItem = nil
    updateToolbar( false )
  }
  
  override var edgesForExtendedLayout: UIRectEdge {
    get { return [] }
    set {}
  }
  
  func updateToolbar( _ animated: Bool = false ) {
    
    let backgroundColorScheme = viewModel.backgroundColorScheme
    let color = backgroundColorScheme.backgroundColor
    
    //self.view.backgroundColor = color
    (theWebView as! UIView).backgroundColor = color

    navigationController?.navigationBar.barTintColor = color
    //navigationController?.navigationBar.barStyle = backgroundColorScheme.barStyle
    self.view.window?.tintColor = backgroundColorScheme.tintColor
    navigationController?.toolbar.barTintColor = color

    if let viewController = splitViewController?.viewControllers[0] as? UINavigationController {

      viewController.navigationBar.barTintColor = color
    }
    
    
    if let navTitle = navigationItem.titleView as? LawNavigationTitleView {
      navTitle.colorScheme = backgroundColorScheme
    }
    
    lawTabView?.colorScheme = backgroundColorScheme
    myToolbar?.backgroundColor = backgroundColorScheme.backgroundColor
    myToolbar?.barTintColor = backgroundColorScheme.backgroundColor
    myToolbar?.tintColor = backgroundColorScheme.tintColor
    theWebView.scrollView.indicatorStyle =  backgroundColorScheme.indicatorStyle
    
    if viewModel.findString_ != nil {
      
      // "Find" toolbar
      
      findLabelButton = UIBarButtonItem(title: viewModel.findTotalString_, style: .plain, target: nil, action: nil)
      findLabelButton!.isEnabled = false
      
      let clear = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(findClear))
      findPreviousButton = UIBarButtonItem(title: WLoc("Prev"), style: .plain, target: self, action:#selector(findPrevious))
      findNextButton = UIBarButtonItem(title: WLoc("Next"), style: .plain, target: self, action:#selector(findNext))
      
      let separator = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
      
      toolbarItems = [clear, findLabelButton!, separator, findPreviousButton!, findNextButton!];
      
      findPreviousButton!.isEnabled = theWebView.canFindPrevious()
      findNextButton!.isEnabled = theWebView.canFindNext()
      findLabelButton?.title = viewModel.findTotalString_

    }else {
      
      // Normal toolbar
      
      let goBack = UIBarButtonItem(image: UIImage(named: "BrowserLeft"), style: .plain, target: self, action:#selector(self.goBack))
      let goForward = UIBarButtonItem(image: UIImage(named: "BrowserRight"), style: .plain, target: self, action:#selector(self.goFoward))
      let tab = tabBarButtonItem ?? UIBarButtonItem(image: tabImage( TabDataSource.shared.all.count ), style: .plain, target: self, action:#selector(showTab(_:)))
      tabBarButtonItem = tab
      
      let search = searchBarButtonItem ?? UIBarButtonItem(image: UIImage(named: "Find"), style:.plain, target: self, action: #selector(self.search))
      search.isEnabled = true
      searchBarButtonItem = search
      
      let jump = jumpBarButtonItem ?? UIBarButtonItem(image: UIImage(named: "ToolbarJump"), style: .plain, target: self, action:#selector(jump(_:)))
      jumpBarButtonItem = jump
      
      let separator = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)

      let mokujiAction = mokujiButtonItem ?? UIBarButtonItem(barButtonSystemItem: .bookmarks,  target: self, action:#selector(mokuji(_:)))
      mokujiButtonItem = mokujiAction
      var hasTOC = true
      
      if let source = viewModel.htmlSource {
        let links = source.extractMidashi()
        if links.count == 0 { hasTOC = false }
      }
      
      if viewModel.usesOldStyleTab == false {
        self.setToolbarItems( [goBack, separator, goForward, separator, mokujiAction, separator, search, separator, jump], animated: animated)
        
      }else {
        self.setToolbarItems( [goBack, separator, goForward, separator, tab, separator, mokujiAction, separator, search, separator, jump], animated: animated)
      }
      goBack.isEnabled = ( viewModel.contentOffsetStack_.count > 0 );
      goForward.isEnabled = ( viewModel.contentOffsetForwardStack_.count > 0 );
    }
    
    updateRightNavigationItem(UIApplication.shared.isNetworkActivityIndicatorVisible)
  }
  
  override func setToolbarItems(_ toolbarItems: [UIBarButtonItem]?, animated: Bool) {
    myToolbar?.setItems(toolbarItems, animated: animated)
  }
  func tabImage(_ count: Int) -> UIImage {
    
    let size = CGSize(width: 30.0, height: 30.0);
    UIGraphicsBeginImageContextWithOptions(size, false, 0.0);
    
    let baseImage = UIImage(named:"TabButton")
    baseImage?.draw(at: CGPoint.zero)
    
    if count > 1 {
      let string = String(count) as NSString
      let attr = [NSFontAttributeName : UIFont.systemFont(ofSize: 14) ]
      
      let size = string.size(attributes: attr)
      let rect = CGRect(x: 5 + (17-size.width)/2,y: 9,width: 17,height: 17).integral
      string.draw(in: rect, withAttributes: attr)
    }
    
    let image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image!
  }
  
  func showTab(_ sender: UIBarButtonItem) {
    
    if let _ = presentedViewController {
      dismiss(animated: false, completion: nil)
    }
    
    let nav = navigationController as! StackNavigationController
    nav.saveState()
    
    let contentViewController = TabTableViewController()
    contentViewController.modalPresentationStyle = UIModalPresentationStyle.popover
    
    let popover = contentViewController.popoverPresentationController
    popover?.permittedArrowDirections = .any
    popover?.barButtonItem = sender
    popover?.delegate = self
    
    let backgroundColorScheme = viewModel.backgroundColorScheme
    
    popover?.backgroundColor = backgroundColorScheme.popoverBackgroundColor
    //contentViewController.view.backgroundColor = backgroundColorScheme.popoverBackgroundColor
    
    contentViewController.didSelectHandler = { [weak contentViewController] (identifier: String?) in
      
      contentViewController?.dismiss(animated: false) {
        
        let _ = TabDataSource.shared.selectTabIdentifier(identifier, jumpTo: nil)
        
      }
    }
    
    present(contentViewController, animated: true, completion: nil)
  }
  
  func findClear() {
    
    theWebView.removeAllHighlights()
    viewModel.findString_ = nil
  }
  
  var findNextButton: UIBarButtonItem? = nil
  var findPreviousButton: UIBarButtonItem? = nil
  var findLabelButton: UIBarButtonItem? = nil
  
  func findPrevious() {
    theWebView.findPrevious()
    viewModel.findPosition = theWebView.currentPosition()
  }
  
  func findNext() {
    theWebView.findNext()
    viewModel.findPosition = theWebView.currentPosition()
  }
  
  func statJumpToExcerptAnchor() {
    //TODO: HANDLING ELEMENT IS NOT ELEGANT
    // look for anchor in toc2
    if viewModel.excerptElement == nil && viewModel.toc2 != nil && viewModel.anchor != nil {
      for element in viewModel.toc2! {
        if element.lookForRange(for: viewModel.anchor!).location != NSNotFound {
          viewModel.excerptElement = element
          break
        }
      }
    }
    
    if viewModel.excerptElement != nil {
      viewModel.startJump = false
      
      jumpToAnchorExcerpt(viewModel.anchor!, inElement: viewModel.excerptElement! )
      
      if let elementId = viewModel.focusedElementId {
        
        //TODO: NOT ELEGANT
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
          self.theWebView.blink(elementId)
        }
      }
      
      return
    }
  }
  
  func didFinishLoading() {
    
    if presentedViewController == nil {
      workaroundUIMenuIssue()
    }
    
    updateToolbar(true)
    
    if viewModel.startSearch != nil && viewModel.startJump == false {
      findString(viewModel.startSearch!, position: nil)
    }
    
    if viewModel.startJump == true, let anchorStringScript = viewModel.javascriptForAnchor {
      // self.detailItem!.anchor! contains packageName as suffix
      
      Analytics.logEvent(AnalyticsEventSearch, parameters: [
        AnalyticsParameterItemName: (viewModel.lawTitle as NSString? ?? "n/a" as NSString),
        AnalyticsParameterSearchTerm: viewModel.anchor! as NSString

        ])
      
      
      if viewModel.excerptType != .full {
        evaluateJavaScript(anchorStringScript, completionHandler: nil)
        
      }else if viewModel.focusedElementId == nil {
        evaluateJavaScript(anchorStringScript, completionHandler: nil)
        
      }else if let elementId = viewModel.focusedElementId {
        theWebView.blink(elementId)
      }
      viewModel.startJump = false
    }
    

    if viewModel.contentOffsetY_ != nil {
    theWebView.scrollView.contentOffset = CGPoint(x: 0, y: viewModel.contentOffsetY_!)
    }
    
    if viewModel.findString_ != nil {
      findString( viewModel.findString_!, position: viewModel.savedFindPosition_ )
    }
    
    if viewModel.popNavigationWhenBack == true {
      
      viewModel.contentOffsetStack_.removeAll(keepingCapacity: false)
      viewModel.contentOffsetForwardStack_.removeAll(keepingCapacity: false)
      viewModel.contentOffsetStack_.append( nil )
      
      updateToolbar(false)
    }
    
    ////
    
    if viewModel.shouldReview() { // 30
      
      viewModel.didReview()

      if #available(iOS 10.3, *) {
        SKStoreReviewController.requestReview()
      }
      
      /*
       //// Review message
       let kHeight: CGFloat = 50
       
       let reviewBase = UIView(frame: CGRect(x: 0,y: self.view.bounds.size.height - kHeight, width: self.view.bounds.size.width, height: kHeight))
       reviewBase.backgroundColor = UIColor.white
       reviewBase.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
       self.view.addSubview(reviewBase)
       reviewBaseView_ = reviewBase
       
       let review = UILabel(frame: CGRect(x: 10,y: 0, width: self.view.bounds.size.width-20, height: kHeight))
       review.text = WLoc("Review App")
       review.numberOfLines = 0
       review.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)
       review.textAlignment = .center
       review.adjustsFontSizeToFitWidth = true
       review.minimumScaleFactor = 0.3
       review.backgroundColor = UIColor.white
       review.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
       reviewBase.addSubview(review)
       
       let button = UIButton(type: .custom)
       button.frame = CGRect(x: 0,y: 0, width: self.view.bounds.size.width, height: kHeight)
       
       button.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
       button.addTarget(self, action: #selector(self.review), for: .touchUpInside)
       
       reviewBase.addSubview(button)
       */
      
    }
    
  }
  
  func didFailLoading() {
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
    updateRightNavigationItem(false)
  }
  
  func addHistory() {
    viewModel.addHistory()
    updateToolbar(false)
  }
  
  func textSize(_ sender: UIBarButtonItem) {
    let contentViewController = TextSizeViewController()
    contentViewController.modalPresentationStyle = .popover
    contentViewController.backgroundColorScheme = viewModel.backgroundColorScheme
    contentViewController.delegate = self
    let fontType = UserDefaults.standard.string(forKey: "FontName")
    contentViewController.isGothic = fontType == "Gothic"

    let popover = contentViewController.popoverPresentationController
    popover?.permittedArrowDirections = [.any]
    popover?.barButtonItem = sender
    popover?.delegate = self

    present(contentViewController, animated: true, completion: nil)
  }
  
  //MARK:- Navigation
  
  func navigationInternal(_ url: NSURL) {
    
    addHistory()

    if let script = viewModel.javascript(forInternalLink: url) {
      evaluateJavaScript(script, completionHandler: { (obj, err) in
        
        if let err = err {
          print(err.localizedDescription)
        }
      })
    }
  }
  
  func showBookmarkGuide() {
    if viewModel.shouldShowBookmarkGuide == false { return }
    
    let controller = UIAlertController( title: WLoc("BookmarkGuideTitle"), message: WLoc("BookmarkGuideMessage"), preferredStyle: .alert)
    let goAction = UIAlertAction(title: WLoc("Not Show Again"), style: .default) { (action) -> Void in
      self.viewModel.shouldShowBookmarkGuide = false
    }
    let cancel = UIAlertAction(title: WLoc("Close"), style: .cancel) { (action) -> Void in }
    controller.addAction(goAction)
    controller.addAction(cancel)
    controller.modalPresentationStyle = .popover
    present(controller, animated: true, completion: nil)

  }
  
  func navigationArticleNumber( _ url: NSURL ) {
    // URL Example: "comcatalystwolaws:(99)@1U99R+@M29HO089.html"
    
    // PRELOAD
    var preloadedDeferenceVC: DereferenceViewController? = nil
    
    if viewModel.toc2 != nil && !viewModel.insideUtilityViewController && utilityViewController == nil {
      preloadedDeferenceVC = DereferenceViewController(href: url as URL, packageURL: viewModel.packageURL!, toc2: viewModel.toc2!)
    }

    let bookmark = viewModel.bookmark(forExternalLink: url)
    let aohonLink = self.viewModel.aohonReaderAppLink(forLink: url)
    
    let controller = UIAlertController( title: bookmark.title, message: nil, preferredStyle: .actionSheet)
    
    let aohonAction = UIAlertAction(title: WLoc("Aohon"), style: .default) { (action) -> Void in
      
      if aohonLink != nil && UIApplication.shared.canOpenURL(aohonLink!) {
        UIApplication.shared.open(aohonLink!, options: [:], completionHandler: nil)
      }else {
        let appstoreLink = URL(string: "https://itunes.apple.com/us/app/japanese-industrial-property/id1174701373?ls=1&mt=8")!
        UIApplication.shared.open(appstoreLink, options: [:], completionHandler: nil)
      }
    }
    
    
    let copyAction = UIAlertAction(title: WLoc("Copy to Clipboard"), style: .default) { (action) -> Void in
      
      let pb = UIPasteboard.general
      pb.url = url as URL

      let attributedString = self.viewModel.attributedLawTitle(for: url)
      pb.setAttributedString( attributedString, alternativeText: url.absoluteString)
    }
    
    let bookmarkAction = UIAlertAction(title: WLoc("Bookmark"), style: .default) { (action) -> Void in
      self.viewModel.addBookmark(bookmark)
      self.showBookmarkGuide()
    }
    
    let notesAction = UIAlertAction(title: WLoc("Notes"), style: .default) { (action) -> Void in
      self.openNotes(bookmark)
    }
    
    let bingAction = UIAlertAction(title: "Bing", style: .default) { (action) -> Void in
      if let url = self.viewModel.bingUrl(bookmark.title) {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
      }
    }
    
    let hanreiAction = UIAlertAction(title: WLoc("Hanrei"), style: .default) { (action) -> Void in
      if let url = self.viewModel.hanreiUrl(bookmark.title)  {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
      }
    }
    
    let hanreiAction2 = UIAlertAction(title: WLoc("Hanrei2"), style: .default) { (action) -> Void in
      if let url = self.viewModel.hanrei2Url(bookmark.title) {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
      }
    }
    
    //		let wikiBooksAction = UIAlertAction(title: WLoc("WikiBooks"), style: .Default) { (action) -> Void in
    //			self.wikiBooks(bookmark.title)
    //		}
    
    let debugAction = UIAlertAction(title: WLoc("Debug Action"), style: .default) { (action) -> Void in
      self.debugSetNotes()
    }
    
    let debugAction2 = UIAlertAction(title: WLoc("Store Restore"), style: .default) { (action) -> Void in
      self.debugStoreRestore()
    }
    
    let debugDerefAction = UIAlertAction(title: WLoc("Dereferences"), style: .default) { (action) -> Void in
      if preloadedDeferenceVC != nil {
        let nav = StackNavigationController(rootViewController: preloadedDeferenceVC!, tab: nil)
        self.setUtilityViewController(nav, animated: true)
        //				self.navigationController?.pushViewController(preloadedDeferenceVC!, animated: true)
      }
    }
    
    let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in }
    
    controller.addAction(bookmarkAction)
    controller.addAction(copyAction)
    controller.addAction(notesAction)
    if aohonLink != nil { controller.addAction(aohonAction) }
    controller.addAction(bingAction)
    //		controller.addAction(wikiBooksAction)
    controller.addAction(hanreiAction)
    controller.addAction(hanreiAction2)
    if preloadedDeferenceVC != nil { controller.addAction(debugDerefAction) }
    if gDebugMode == true {
      controller.addAction(debugAction)
      controller.addAction(debugAction2)
      
    }
    controller.addAction(cancel)
    controller.modalPresentationStyle = .popover
    let popover = controller.popoverPresentationController
    
    popover?.permittedArrowDirections = .any
    popover?.sourceView = (theWebView as! UIView)
    
    var rect = CGRect(x: 0,y: 0,width: 1,height: 1)
    rect.origin = theWebView.lastTappedPoint
    rect = rect.insetBy(dx: -20, dy: -20)
    
    popover?.sourceRect = rect
    
    present(controller, animated: true, completion: nil)
  }
  
  func navigationHighlight( _ uuid: String ) {
    
    let root = FilePackage.shared.documents
    
    let condition: ((_ dictionary: DocItem) -> Bool) = { (dictionary: DocItem) -> Bool   in
      return dictionary.uuid == uuid
    }
    
    let theDictionary = root.lookForWithCondition(condition)
    if theDictionary == nil { return /* unknown */ }
    
    let contentViewController = HighlightViewController()
    contentViewController.backgroundColorScheme = viewModel.backgroundColorScheme
    contentViewController.source = theDictionary
    contentViewController.delegate = self
    contentViewController.modalPresentationStyle = .popover

    highlightViewController_ = contentViewController
    
    // let _ = contentViewController.view // instantiate
    
    var rect = CGRect(x: 0,y: 0,width: 1,height: 1)
    rect.origin = theWebView.lastTappedPoint
    rect = rect.insetBy(dx: -20, dy: -20)
    
    let popover = contentViewController.popoverPresentationController
    popover?.permittedArrowDirections = [.down, .up]
    popover?.sourceView = (theWebView as! UIView )
    popover?.sourceRect = rect
    popover?.delegate = self
    
    present(contentViewController, animated: true, completion: nil)
  }
  
  func navigationOpenNext(_ URL: URL) {
    
    let root = FilePackage.shared.documents
    var nextDocument: DocItem? = nil
    guard let dict = URL.query?.getUrlQueryParameters() else { return }
    guard let linkDestination = LinkDestination(queryDictionary: dict) else { return }
    
    let packageName = (linkDestination.h_file as String).convertHFileKanjiToPkg()
    
    // 1. Look for Registered h_file
    
    var condition: ((_ dictionary: DocItem) -> Bool) = { (dictionary: DocItem) -> Bool in
      return dictionary.isLawEntity && dictionary.h_file == linkDestination.h_file
    }
    
    //
    nextDocument = root.lookForWithCondition(condition)
    
    // Open next file
    if nextDocument?.packageURLExistsFile == true {
      openNextPage( linkDestination )
      return
    }
    
    // Next document does not exists yet
    if nextDocument == nil {
      
      // 2. Look for Existing file
      condition = { (dictionary: DocItem) -> Bool in
        return dictionary.isLawEntity && dictionary.packageName?.hasPrefix( packageName ) == true
      }
      nextDocument = root.lookForWithCondition(condition)
      
      if nextDocument != nil {
        // Package found ... update link and save
        
        nextDocument!.h_file = linkDestination.h_file
        FilePackage.shared.saveIndex( saveToICloud: true )
        self.openNextPage( linkDestination )

        return
      }else {
        
        nextDocument = DocItem( queryWithPackageName: packageName, edition: nil)
      }
    }
    
    // 3. Download/Ask user which doc to open
    
    downloadFromCloudKit( nextDocument!, linkDestination: linkDestination ) { (success: Bool) -> Void in
      
      if success {
        
        condition = { (dictionary: DocItem) -> Bool in
          return dictionary.anchor == nil && dictionary.packageURL != nil && dictionary.packageName?.hasPrefix( packageName ) == true
        }
        
        nextDocument = FilePackage.shared.documents.lookForWithCondition(condition)
        
        if nextDocument != nil {
          
          // Package found ... update link and save
          nextDocument!.h_file = linkDestination.h_file
          FilePackage.shared.saveIndex( saveToICloud: true )
          self.openNextPage( linkDestination )
          
        }else {
          // Unknown
          self.navigationRetrievePackage( linkDestination, asPackageName: packageName )
        }
      }else {
        
        // Failed to download ... ask
        self.navigationRetrievePackage( linkDestination, asPackageName: packageName )
      }
    }
  }
  
  func navigationRetrievePackage( _ linkDestination: LinkDestination, asPackageName packageName: String ) {
    
    // 1. Look for CLoudKit
    
    // 2. Does not exits
    
    let message = NSString(format: WLoc("Is this law in local? %@ %@ %@") as NSString, linkDestination.h_file, packageName, linkDestination.referenceName)
    
    let controller = UIAlertController(title: WLoc("Reference"), message: message as String, preferredStyle: .actionSheet )
    let cancelAction = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in  }
    let action1 = UIAlertAction(title: WLoc("Read Online"), style: .default) { [weak self] (action) -> Void in
      self?.downloadFromRemote(linkDestination)
    }
    let action2 = UIAlertAction(title: WLoc("Link"), style: .default) {  [weak self] (action) -> Void in
      
      let controller = LinkToFileTableViewController(style: .grouped)
      controller.h_file = linkDestination.h_file
      controller.completionHandler = { () -> Void in
        self?.openNextPage( linkDestination )
      }
      
      //Execute later
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
        
        let nav = UINavigationController(rootViewController: controller)
        nav.modalPresentationStyle = .formSheet
        self?.present(nav, animated: true, completion: nil)
      }
    }
    
    controller.addAction(action1)
    controller.addAction(action2)
    controller.addAction(cancelAction)
    controller.modalPresentationStyle = .popover
    let popover = controller.popoverPresentationController
    popover?.permittedArrowDirections = .any
    popover?.sourceView = (theWebView as! UIView )
    
    var rect = CGRect(x: 0,y: 0,width: 1,height: 1)
    rect.origin = theWebView.lastTappedPoint
    rect = rect.insetBy(dx: -20, dy: -20)
    popover?.sourceRect = rect
    
    present(controller, animated: true, completion: nil)
  }
  
  func downloadFromCloudKit( _ dictionary: DocItem, linkDestination: LinkDestination, completion: @escaping ((_ success: Bool) -> Void)) {
    
    let controller = WaitingViewController( nibName: "WaitingViewController", bundle: nil)
    controller.modalPresentationStyle = .popover
    
    let popover = controller.popoverPresentationController
    popover?.delegate = controller
    popover?.permittedArrowDirections = [.up, .down]
    popover?.sourceView = (theWebView as! UIView )
    popover?.backgroundColor = viewModel.backgroundColorScheme.popoverBackgroundColor
    
    var rect = CGRect(x: 0,y: 0,width: 1,height: 1)
    rect.origin = theWebView.lastTappedPoint
    rect = rect.insetBy(dx: -20, dy: -20)
    popover?.sourceRect = rect
    
    present(controller, animated: true, completion: nil)
    
    CloudKitDomain.shared.downloadLaw( dictionary, progress: {
      [weak controller] (progress: Double) -> Void in
        controller?.progress = progress
      },
      completion: { [weak controller] (success: Bool, error: Error?) -> Void in
          controller?.dismiss(animated: true) {
          //				displayErrorAlertIfNecessary(error) // Do not show error here.
          completion(dictionary.packageURLExistsFile)
        }
      }
    )
  }
  
  //MARK:-
  
  func openNextPage( _ linkDestination: LinkDestination ) {
    
    if let nextViewModel = viewModel.nextPageViewModel(linkDestination) {
      
      // Link
      let controller = DetailViewController(restorationInfo: nil)
      controller.viewModel = nextViewModel
      self.navigationController!.pushViewController(controller, animated: true)
    }
  }
  
  func jumpToString(_ jumpToString: String! ) {
    
    // Convert の -> .
    let (_, _, script) = viewModel.anchor(for: jumpToString)
    
    if script != nil {
      evaluateJavaScript(script, completionHandler: nil)
    }
  }
  
  // MARK:- Actions

  func openNotes(_ bookmark: Bookmark) {
    let controller = NotesViewController()
    controller.delegate = self
    controller.title = bookmark.title
    controller.bookmark = bookmark
    
    let nav = UINavigationController(rootViewController: controller)
    nav.modalPresentationStyle = .formSheet
    present( nav, animated: true, completion: nil)
  }
  
  func debugStoreRestore() {
    
    let restoredState = restorationInfo()

    didReceiveMemoryWarning()
    viewModel.htmlSource = nil
    viewModel.linkDictionaries = []
    //viewModel.detailItem = nil
    theWebView.myLoadHTMLString("", baseURL: nil)
    
    //Execute later
    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
      
      self.loadFromRestorationInfo(restoredState)
      self.loadFromFile({ (success) in })
    }
  }
  
  func debugSetNotes() {
    
    print(viewModel.rawSource ?? "")
    
    if viewModel.toc2_ != nil {
      for element in viewModel.toc2_! {
        print(element.description)
      }
    }
    
    //
    var URL: NSURL? = nil
    let contentHTML = viewModel.htmlSource!
    var searchRange = NSMakeRange(0,contentHTML.length)
    
    repeat {
      
      autoreleasepool {
        
        let thisRange = contentHTML.range(of: "comcatalystwolaws:[^\"]*", options: [.regularExpression], range: searchRange)
        
        if thisRange.length != 0 {
          
          let thisURLString = contentHTML.substring(with: thisRange)
          URL = NSURL(string: thisURLString)
          
          debugPrint(thisURLString)
          
          searchRange = NSMakeRange(NSMaxRange(thisRange), contentHTML.length - NSMaxRange(thisRange))
          
          // Look for URL in source
          //  "comcatalystwolaws:(99)@1U99R+@M29HO089.html"
          
          let specifier = URL!.resourceSpecifier
          let comps = specifier?.components(separatedBy: "@")
          let decodedTitle =  ((comps?[0])! as NSString).replacingPercentEscapes(using: String.Encoding.utf8.rawValue)!.decodedArticleTitle
          let bookmark = Bookmark( title: decodedTitle, encodedURL: (comps?[1])!, clickedURL: URL as URL?)
          let text = "\(decodedTitle) \(thisURLString)"
          viewModel.setNotes(text, bookmark: bookmark)
          
        }else {
          searchRange.length = 0 // end loop
        }
      }
    } while searchRange.length != 0
  }
  
  /// MARK:-
  
  func findString(_ string: String, position: Int?) {
    guard string.isEmpty == false else { return }
    
    theWebView.highlightAllOccurencesOfString(string as NSString, position: position) { [weak self] (count: Int)->Void in
      guard let strongSelf = self else { return }
      let pos = position ?? 1
      strongSelf.viewModel.findCount = count + 1
      strongSelf.viewModel.findString_ = string
      strongSelf.viewModel.findPosition = pos
    }
  }
  
  func findDidChange() {
    if isViewLoaded == false { return }
    updateToolbar( true )
  }
  
  
  func downloadFromRemote(_ linkDestination: LinkDestination?) {
    
    let controller = SearchLawTableViewController()
    controller.presetString = linkDestination?.referenceName
    navigationController?.pushViewController(controller, animated: true)
  }
}

// MARK:- NotesViewControllerDelegate
extension DetailViewController: UIWebViewDelegate, WKNavigationDelegate {

  //
  // MARK: -  Web View Delegate
  //
  
  func webViewDidFinishLoad(_ webView: UIWebView) {
    if viewModel.excerptType == .full {
      viewModel.hasFinishedLoading = true
    }
    didFinishLoading()
  }
  
  func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    didFinishLoading()
  }
  
  func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
    didFailLoading()
  }
  
  func webView( _ webView: UIWebView,didFailLoadWithError error: Error) {
    didFailLoading()
  }
  
  func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {

    let request = navigationAction.request
    
    print(request.url ?? "")
    print("type \(navigationAction.navigationType)")
    
    var flag = self.webView( webView, shouldStartLoadWithRequest: request )
    
    if navigationAction.navigationType == .linkActivated {
      flag = false
      if let URL = request.url { navigationInternal( URL as NSURL ) }
    }
    
    decisionHandler( flag ? .allow : .cancel)
  }
  
  func webView( _ webView: UIWebView,
                shouldStartLoadWith request: URLRequest,
                navigationType: UIWebViewNavigationType) -> Bool {
    
    return self.webView( webView, shouldStartLoadWithRequest: request )
  }
  
  func webView( _ webView: Any,
                shouldStartLoadWithRequest request: URLRequest ) -> Bool {
    if "about:blank" == request.url!.absoluteString { return true }
    
    //
    
    let path: NSString? = request.url!.path as NSString?
    let scheme = request.url!.scheme
    
    print(scheme ?? "")
    
    if scheme == "intl" {
      if let URL = request.url { navigationInternal( URL as NSURL ) }
      return false
    }
    
    if scheme == "comcatalystwolaws" {
      if let URL = request.url { navigationArticleNumber( URL as NSURL ) }
      return false
    }
    
    if scheme == "highlight" {
      if let specifier = (request.url as NSURL?)?.resourceSpecifier {
        highlightingURLSpecifier = (request.url as NSURL?)?.resourceSpecifier
        navigationHighlight( specifier )
      }
      return false
    }
    
    if scheme == "notes" {
      //path == comcatalystwo...
      
      if let absoluteString: NSString = request.url?.absoluteString as NSString? {
        let url = URL(string: absoluteString.substring(from: 6))
        let specifier = (request.url as NSURL?)?.resourceSpecifier
        let comps = specifier?.components(separatedBy: "@")
        let decodedTitle = ((comps?[0])! as NSString).replacingPercentEscapes(using: String.Encoding.utf8.rawValue)!.decodedArticleTitle
        let bookmark = Bookmark( title: decodedTitle, encodedURL: (comps?[1])!, clickedURL: url)
        openNotes(bookmark)
      }
    }
    
    if path?.hasPrefix("/cgi-bin") == true  {
      
      if let URL = request.url { navigationOpenNext( URL ) }
      return false
      
      //  http://law.e-gov.go.jp
      
      //				// Avoid excessive access
      //				if  NSDate.timeIntervalSinceReferenceDate() - lastLoadedTime < 5 {
      //					return false
      //				}
      //
    }
    
    if scheme == "about" {
      
      // WKWebView calls this method twice when clicking on higlight
      if highlightingURLSpecifier != nil && ("blank%23" + highlightingURLSpecifier! == (request.url as NSURL?)?.resourceSpecifier) { return false }
    }
    
    addHistory()
    return true
  }

}

// MARK:- NotesViewControllerDelegate
extension DetailViewController: NotesViewControllerDelegate {
  
  func notesViewControllerWillClose( _ sender: NotesViewController, withChange: Bool ) {
    
    if sender.source != nil {
      
    }else if withChange == true {
      //viewModel.startContentOffset = theWebView.scrollView.contentOffset
      loadFromFile  { (success: Bool) -> Void in }
    }
  }
  
  func getNotes( _ sender: NotesViewController ) -> String? {
    
    if let source = sender.source {
      return source.notes
      
    }else if let bookmark = sender.bookmark  {
      return viewModel.getNotes(bookmark: bookmark)
    }
    
    return ""
  }
  
  func setNotes( _ sender: NotesViewController, text: String! ) {
    
    if let source = sender.source {
      viewModel.setNotes(text, sourceItem: source )
      theWebView.rehighlight( source )
      
    } else if let bookmark = sender.bookmark  {
      viewModel.setNotes(text, bookmark: bookmark )
    }
  }
  
  func deleteNotes( _ sender: NotesViewController ) {
    
    if let sourceItem = sender.source  {
      
      viewModel.deleteNotes(sourceItem: sourceItem)
      theWebView.rehighlight( sourceItem )
      
    }else if let bookmark = sender.bookmark {
      viewModel.deleteNotes(bookmark: bookmark)
    }
  }
}

//MARK:- HighlightingDelegate
extension DetailViewController: HighlightingDelegate {
  func highlightDefaultColor( ) -> HighlightColor {
    return viewModel.highlightDefaultColor
  }
  
  func highlight( _ sender: Any?, dictionary: DocItem ) {
    
    //		// Check if it can expand to adjacent area
    //
    //		// get siblings
    //
    //		let condition2 = {
    //			(thisDictionary:NSMutableDictionary)->AnyObject?  in
    //
    //			if thisDictionary["tag"] as String == dictionary["tag"] as String  { return thisDictionary }
    //
    //			return nil
    //		}
    //
    //		let siblings = root.collectWithCondition( condition2 )
    //
    //
    //
    //		let thisRange = ( sender.source() as NSString)?.rangeOfStringInHtmlUntilANameTag(
    //
    //
    //
    viewModel.addHighlight(sourceItem: dictionary)
  }
}

//MARK:- TextSizeViewControllerDelegate
extension DetailViewController: TextSizeViewControllerDelegate {
  
  func textSizeViewController( _ sender: TextSizeViewController, increaseFontSize: Bool ) {
    
    //let scales = [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, ]
    var scale: CGFloat = 1.0

    if increaseFontSize {
      if scale < 1.0 {
        scale = 1.2
        
      }else {
        scale = 1.05
      }
    }else {
      if scale < 1.0 {
      scale = 0.8
      }else {
        scale = 0.97
      }
    }
    evaluateJavaScript(viewModel.javascript(toZoom: CGFloat(scale)), completionHandler: nil)
    viewModel.webViewScale = viewModel.webViewScale * Double(scale)
  }
  
  func textSizeViewController( _ sender: TextSizeViewController, setBackgroundScheme scheme: BackgroundColorSchemeType ) {
    
    let ud = UserDefaults.standard
    ud.set(scheme.rawValue, forKey: "BackgroundColorScheme")
    ud.synchronize()
    NotificationCenter.default.post(name: LawNotification.settingsDidChange, object: self)
  }
  
  func textSizeViewController( _ sender: TextSizeViewController, setGothicFont gothic: Bool ) {
    
    let ud = UserDefaults.standard
    ud.set( gothic ? "Gothic" : "Mincho", forKey: "FontName")
    NotificationCenter.default.post(name: LawNotification.settingsDidChange, object: self)

  }

}

//MARK:- HighlightViewControllerDelegate
extension DetailViewController: HighlightViewControllerDelegate {
  
  func highlightViewController( _ sender: HighlightViewController, willCloseWithChange withChange: Bool ) {
    
    if withChange == true {
      theWebView.rehighlight( sender.source! )
      FilePackage.shared.saveIndex( saveToICloud: true )
    }
  }
  
  func highlightViewController( _ sender: HighlightViewController, setHighlight color: HighlightColor ) {
    
    guard let source = sender.source else { return }
    viewModel.setHighlight(sourceItem: source, color: color)
    theWebView.rehighlight( source )
  }
  
  func requestEditing(from sender: HighlightViewController ) {
    
    sender.dismiss(animated: true) {
      self.highlightViewController_ = nil;
      
      let controller = NotesViewController()
      controller.delegate = self
      controller.source = sender.source
      
      let nav = UINavigationController(rootViewController: controller)
      nav.modalPresentationStyle = .formSheet
      self.present( nav, animated: true, completion: nil)
    }
  }
  
  func highlightViewController( _ sender: HighlightViewController, deleteWithConfirm requiresConfirm: Bool ) {
    
    let theDictionary = sender.source
    let uuid = theDictionary?.uuid
    
    if uuid == nil {
      sender.dismiss(animated: true, completion: nil)
      return
    } // Unknown Error
    
    if requiresConfirm == true {
      
      let controller = UIAlertController( title: nil, message: WLoc("Unhighlight Message"), preferredStyle: .actionSheet)
      let deleteAction = UIAlertAction(title: WLoc("Unhighlight"), style: .destructive) { (action) -> Void in
        
        // 1. Unhighlight
        self.theWebView.unhighlight( uuid, snippet: theDictionary!.text)
        
        // 2. Delete index
        self.viewModel.deleteHighlight(withUuid: uuid!)
      }
      let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in }
      controller.addAction(deleteAction)
      controller.addAction(cancel)
      controller.modalPresentationStyle = .popover
      
      let popover = controller.popoverPresentationController
      popover?.permittedArrowDirections = .down
      
      controller.popoverPresentationController?.sourceRect = presentedViewController!.popoverPresentationController!.sourceRect
      controller.popoverPresentationController?.sourceView = presentedViewController!.popoverPresentationController?.sourceView
      
      sender.dismiss(animated: true) {
        
        self.highlightViewController_ = nil;
        self.present(controller, animated: true, completion: nil)
      }
      
    }else {
      
      // 1. Unhighlight
      theWebView.unhighlight( uuid, snippet: theDictionary!.text)
      
      // 2. Delete index
      self.viewModel.deleteHighlight(withUuid: uuid!)
      
      sender.dismiss(animated: true, completion: nil)
    }
  }
}

//MARK:- UIPopoverPresentationControllerDelegate
extension DetailViewController: UIPopoverPresentationControllerDelegate {
  func adaptivePresentationStyle(for controller: UIPresentationController)
    -> UIModalPresentationStyle {
      return .none
  }
  
  func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection)
    -> UIModalPresentationStyle {
      return .none
  }
  
  func popoverPresentationController( _ popoverPresentationController: UIPopoverPresentationController,
                                      willRepositionPopoverTo rect: UnsafeMutablePointer<CGRect>,
                                      in view: AutoreleasingUnsafeMutablePointer<UIView>) {
  }
  
  func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
    
    workaroundUIMenuIssue()
  }
}

//MARK:- LAW TAB
extension DetailViewController: LawTabViewSource, TabBarProtocol {
  
  func tabsDidUpdateExternally() {
    lawTabView?.reload()
  }
  
  func titlesForLawTabView(_ : LawTabView) -> [String] {
    
    let identifiers = TabDataSource.shared.all
    var titles: [String] = []
    for identifier in identifiers {
      let stackViewModel = StackNavigationViewModel()
      stackViewModel.tab = identifier
      
      if let title = stackViewModel.titles?.last, title != "" {
          titles.append(title)
      }else {
        titles.append(WLoc("<BLANK>"))
      }
    }
    return titles
  }
  
  func lawTabView(_ : LawTabView, didSelectItemAt itemIndex: Int?) {
    
    var identifiers = TabDataSource.shared.all

    if itemIndex == nil || ( itemIndex != nil && itemIndex! >= identifiers.count ) {
      let _ = TabDataSource.shared.selectTabIdentifier()
      return
    }
    
    let identifier = identifiers[itemIndex!]
    let _ = TabDataSource.shared.selectTabIdentifier(identifier, jumpTo: nil)
  }
  
  func lawTabViewIndexOfSelection(_ : LawTabView) -> Int? {
    let identifiers = TabDataSource.shared.all
    if TabDataSource.shared.current == nil { return nil }
    return identifiers.index(of: TabDataSource.shared.current!)
  }
  
  func lawTabViewPathStringForSelection(_ : LawTabView) -> String {
    guard let current = TabDataSource.shared.current else { return "" }
    
    let stackViewModel = StackNavigationViewModel()
    stackViewModel.tab = current
    
    return stackViewModel.pathString()
  }
  
  func lawTabView(_ : LawTabView, moveItemAt fromIndex: Int, to toIndex: Int) {
    TabDataSource.shared.moveTabAt(index: fromIndex, to: toIndex)
  }
  
  func lawTabView(_ : LawTabView, removeItemAt index: Int) -> Bool {
    let identifier = TabDataSource.shared.all[index]

    TabDataSource.shared.removeTabIdentifier(identifier)
    return true
  }
  
  func lawTabViewDraggingWillStart(_ : LawTabView) {
    let view = UIView(frame: CGRect(x: 0, y: lawTabView!.frame.minY - 30, width: lawTabView!.frame.size.width, height: 30))
    dragginShade = view
    dragginShade?.alpha = 0
    dragginShade?.isOpaque = false
    dragginShade?.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
    self.view.addSubview(dragginShade!)
    
    let label = UILabel(frame: view.bounds)
    label.textColor = UIColor.white
    label.textAlignment = .center
    label.text = WLoc("Tab Dragging Message")
    dragginShade?.addSubview(label)
    UIView.animate(withDuration: 0.2, animations: { self.dragginShade?.alpha = 1.0 })
  }
  
  func lawTabViewDraggingWillEnd(_ : LawTabView) {
    dragginShade?.removeFromSuperview()
    dragginShade = nil
  }
  
  
}

//
//  TabTableViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2/06/15.
//  Copyright (c) 2015 Catalystwo. All rights reserved.
//

import UIKit

let kRowHeight = 44.0

class TabTableViewController: UITableViewController {
  
  var didSelectHandler:((_ identifier:String?) -> Void)? = nil
  
  var backgroundColorScheme: BackgroundColorSchemeType {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    return backgroundColorScheme
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.view.backgroundColor = UIColor.clear
    self.tableView.backgroundColor = UIColor.clear
    
    if backgroundColorScheme.inverted {
      tableView?.separatorColor = UIColor(white: 1, alpha: 0.3)
      tableView?.indicatorStyle = .white
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    let center =  NotificationCenter.default
    center.addObserver(self, selector: #selector(ubiquitousKeyValueStoreDidChange), name: NSUbiquitousKeyValueStore.didChangeExternallyNotification, object: nil)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    let center = NotificationCenter.default
    center.removeObserver(self, name: NSUbiquitousKeyValueStore.didChangeExternallyNotification, object: nil)
  }
  
  override var preferredContentSize: CGSize {
    set {}
    get {
      let width:CGFloat = 300
      let viewHeight = CGFloat(( TabDataSource.shared.all.count + 1) * Int(kRowHeight))
      return CGSize(width: width, height: viewHeight)
    }
  }
  
  func ubiquitousKeyValueStoreDidChange() {
    self.tableView.reloadData()
  }
  
  // MARK: - Actions
  
  func editButton() {
    self.setEditing(!self.isEditing, animated: true)
	}
	
	func addButton() {
		didSelectHandler?(nil)
	}
	
	// MARK: - Table view data source
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 2
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if section == 0 { return TabDataSource.shared.all.count }
		return 1
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")  ??
			UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
		
		cell.accessoryType = UITableViewCellAccessoryType.none
		cell.selectionStyle = .default
		cell.showsReorderControl = true
		cell.backgroundColor = .clear
		
		if indexPath.section == 1 {
			
			cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Section1")
			
			cell.textLabel!.text = ""
			cell.selectionStyle = .none
			cell.backgroundColor = UIColor.clear
			
			let button = UIButton(type:.system)
			let image = UIImage(named:"AddTable")
			button.setImage(image, for: UIControlState())
			button.sizeToFit()
			button.addTarget(self, action: #selector(addButton), for: .touchUpInside)
			button.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin]
			cell.contentView.addSubview(button)
			
			let contentView = cell.contentView
			
			button.frame = CGRect(x: (contentView.bounds.size.width - button.frame.size.width)/2,
				y: (contentView.bounds.size.height - button.frame.size.height)/2,
				width: button.frame.size.width,
				height: button.frame.size.height)
			
			//
			
			if TabDataSource.shared.all.count > 1 {
				
				let editButton = UIButton(type:.system)
				let editImage = UIImage(named:"EditTable")
				editButton.setImage(editImage, for: UIControlState())
				editButton.sizeToFit()
				editButton.addTarget(self, action: #selector(TabTableViewController.editButton), for: .touchUpInside)
				
				editButton.autoresizingMask = .flexibleLeftMargin
				
				cell.contentView.addSubview(editButton)
				
				editButton.frame = CGRect(x: (contentView.bounds.size.width - editButton.frame.size.width - 10),
					y: (contentView.bounds.size.height - editButton.frame.size.height)/2,
					width: editButton.frame.size.width,
					height: editButton.frame.size.height)
			}
		}
		
		if indexPath.section == 0 {
			
			let identifier = TabDataSource.shared.all[indexPath.row]
			
			if identifier == TabDataSource.shared.current {
				cell.accessoryView = nil
				cell.accessoryType = UITableViewCellAccessoryType.checkmark
			}else {
//				let button = UIButton.buttonWithType(.Custom) as! UIButton
//				
//				let image = UIImage(named:"CCalEventCloseButton-flat")
//				button.setImage(image, forState: .Normal)
//				button.sizeToFit()
//				button.tag = indexPath.row
//				button.addTarget(self, action: "deleteRow:", forControlEvents: .TouchUpInside)
//				cell?.accessoryView = button
			}
			
      if let plist = TabDataSourceStorage<StackNavigationStorage>()[identifier] {
				
				var titleString:String = ""
				for restorationInfo in plist {
					
					let plist = restorationInfo[.plist] as? DocProperty
					if let title = plist?[.title] as? String  {
						if titleString.characters.count != 0 {
							titleString = titleString + " ▸ "
						}
						titleString = titleString + title
					}
				}
				
				cell.textLabel!.text =  titleString
				cell.textLabel!.numberOfLines = 2
				cell.textLabel!.minimumScaleFactor = 0.8
				cell.textLabel!.adjustsFontSizeToFitWidth = true
				cell.textLabel!.lineBreakMode = .byTruncatingMiddle
				cell.textLabel!.textAlignment = NSTextAlignment.left
				cell.textLabel!.textColor = backgroundColorScheme.textColor
        
        let view = UIView()
        view.backgroundColor = backgroundColorScheme.tableSelectionColor
        cell.selectedBackgroundView = view

			}
		}
		
		// Configure the cell...
		
		return cell
	}
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return CGFloat(kRowHeight)
	}

	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		if indexPath.section == 0 {
			let identifier = TabDataSource.shared.all[indexPath.row]
			
			didSelectHandler?(identifier)
		}
		
		if indexPath.section == 1 {
			return
		}
	}
	
	override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
		if indexPath.section == 0 { return true }
		return false
	}
	
	override func tableView( _ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		
		if indexPath.section == 0 {
			let identifier = TabDataSource.shared.all[indexPath.row]
			if identifier != TabDataSource.shared.current {
				return true
			}
		}
		return false
	}
	
	override func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
		
		if (proposedDestinationIndexPath as NSIndexPath).section == 1 {
			return IndexPath(row: TabDataSource.shared.all.count - 1, section: 0)
		}
		
		return proposedDestinationIndexPath
	}
	
	override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {

		let f = (sourceIndexPath as NSIndexPath).row, t = (destinationIndexPath as NSIndexPath).row

		if f == t { return }
		
		var identifiers = TabDataSource.shared.all
		let theIdentifier = identifiers[f]

		identifiers.remove(at: f)
		identifiers.insert(theIdentifier, at: t)
		
		let ud = UserDefaults.standard
		ud.set(identifiers, forKey: "TabIdentifiers")
		ud.synchronize()
		
		let sharedUD = NSUbiquitousKeyValueStore.default()
		sharedUD.set(identifiers, forKey: "TabIdentifiers")
		sharedUD.synchronize()
	}
	
	override func tableView( _ tableView: UITableView,
	                         commit editingStyle: UITableViewCellEditingStyle,
	                                            forRowAt indexPath: IndexPath) {
		
		let identifier = TabDataSource.shared.all[indexPath.row]
		TabDataSource.shared.removeTabIdentifier(identifier)
		tableView.deleteRows(at: [indexPath], with: .automatic)
	}
	
	func deleteRow(_ sender:UIButton) {
		let row = sender.tag
		let identifier = TabDataSource.shared.all[row]
		TabDataSource.shared.removeTabIdentifier(identifier)
		tableView.reloadData()
	}
	
	override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
		return WLoc("Close")
	}
}

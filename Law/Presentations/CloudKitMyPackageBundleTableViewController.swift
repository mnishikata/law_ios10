//
//  LawInSwift
//
//  Created by masa on 2014/09/09.
//  Copyright (c) 2014年 Catalystwo. All rights reserved.
//

import UIKit
import CloudKit

class CKNavigationController : UINavigationController {

	override func viewDidLoad() {
		navigationBar.barTintColor = kTintColor
		navigationBar.tintColor = UIColor.white
		
		isToolbarHidden = false
		toolbar.barTintColor = kTintColor
		toolbar.tintColor = UIColor.white


		let font = UIFont(name: "HelveticaNeue-Thin", size: 20.0)!
		let attr = [NSForegroundColorAttributeName : UIColor.white, NSFontAttributeName : font ]
		navigationBar.titleTextAttributes = attr
		
		self.view.tintColor = kTintColor
	}
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
}

class CloudKitMyPackageBundleTableViewController: UITableViewController {
	
	@IBOutlet var headerView: UIButton!

  func rowHeight(_ status: LawCloudKitFetchStatus) -> CGFloat {
    switch status {
    case .start:		return 60
    case .noResults:	return 240
    case .fetching:	return 60
    case .found:		return 240
    }
  }
  
  
	var md5s: [String] = []
	var showUniqueOnly = true
  lazy var dateFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.dateStyle = DateFormatter.Style.long
    dateFormatter.timeStyle = .short
    dateFormatter.locale = Locale(identifier: "ja_JP")
    dateFormatter.calendar = Calendar(identifier: Calendar.Identifier.japanese)
    return dateFormatter
  }()
	var presetString: String? = nil
	
	@IBOutlet weak var startButton: UIButton!
	@IBOutlet weak var messageLabel: UILabel!
	// MARK:- Body

	override func viewDidLoad() {
		super.viewDidLoad()
		
    let _ = CloudKitDomain.shared // Instantiate before use ...writing action involves

    //Bundle.main.loadNibNamed("CMHeaderView", owner: self, options: nil)
    //assert( headerView != nil, "** headerView is nil!! **")
		
    headerView.addTarget(self, action: #selector(openNew(_:)), for: .touchUpInside)
		
		startButton.setTitle(WLoc("Upload New Law"), for: UIControlState())
		messageLabel.text = WLoc("CloudKitMyPackageBundleTableViewControllerMessage")
		
		tableView.tableHeaderView = headerView
		title = WLoc("My Package Bundles")
		
		let item = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(close))
		navigationItem.leftBarButtonItem = item
	}
	
	func close() {
		dismiss(animated: true, completion: nil)
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		setNeedsStatusBarAppearanceUpdate()
		navigationController?.isToolbarHidden = false
		
		fetch()
	}
	
	override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
		return  [.all]
	}

	
	// MARK: - Table view data source
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		// #warning Potentially incomplete method implementation.
		// Return the number of sections.
		
		if( CloudKitDomain.shared.myBundleFetchStatus == .fetching ) { return 1 }
		return 1
	}
	
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		
		if section == 0  {
			
			if CloudKitDomain.shared.myBundleFetchStatus == .noResults  { return nil }
			if CloudKitDomain.shared.myBundleFetchStatus == .found {
				return WLoc("MyPackageBundleHistory") + WLoc("FoundStr") +  " \(CloudKitDomain.shared.myBundleRecords.count) " + WLoc("FoundStrSuffix") }
		}
		return nil
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if( CloudKitDomain.shared.myBundleFetchStatus == .fetching ) { return 1 }
		return CloudKitDomain.shared.myBundleRecords.count
	}
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		
		if CloudKitDomain.shared.myBundleFetchStatus == .fetching {
      return rowHeight(CloudKitDomain.shared.myBundleFetchStatus)
    }
		return 68
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		var cell = tableView.dequeueReusableCell(withIdentifier: "Cell") ??   UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
		
		if CloudKitDomain.shared.myBundleFetchStatus == .fetching {
			
			cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
			cell.frame = CGRect(x: 0,y: 0, width: 320, height: 120)
			let contentRect = cell.contentView.bounds as CGRect!
			
			// Loading animation
			
			let imageView = CircularProgressIndicator(frame: CGRect(x: 320-30-7, y: (44-30)/2, width: 30, height: 30) )
			imageView.autoresizingMask = [.flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
			imageView.indeterminate = true
			
			let imageSize = imageView.frame.size
			imageView.frame = CGRect( x: ((contentRect?.size.width)! - imageSize.width)/2, y: (contentRect?.size.height)! - imageSize.height - 10, width: imageSize.width, height: imageSize.height )
			
			cell.contentView.addSubview(imageView)
			cell.isUserInteractionEnabled = false
			
			return cell
		}
		
		
//		
//		if indexPath.section == 0 {
//			
//			cell = UITableViewCell(style: .Default, reuseIdentifier: nil)
//
//			
//			cell?.frame = CGRectMake(0,0,320,120)
//			cell?.imageView!.image = UIImage(named:"UploadLarge")
//			let contentRect = cell?.contentView.bounds as CGRect!
//			
//			
//			let label = UILabel()
//			label.text = WLoc("Upload New Law")
//			label.textColor = self.view.tintColor
//			label.font = UIFont.preferredFontForTextStyle(UIFontTextStyleHeadline)
//			label.textAlignment = .Center
//			label.numberOfLines = 0
//			label.frame = CGRectMake( 10, 10, contentRect.size.width - 20, contentRect.size.height - 20 )
//			label.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
//			cell?.contentView.addSubview(label)
//			return cell!
//		}

		
		
		let record = CloudKitDomain.shared.myBundleRecords[indexPath.row]
		cell.textLabel!.text = record.object(forKey: RecordType.LawDocPackageKey.title) as? String
		
		let modificationDate = record.modificationDate
		if modificationDate != nil {
			cell.detailTextLabel?.text = "更新日：" + dateFormatter.string(from: modificationDate!)
		}else {
			cell.detailTextLabel?.text = ""
		}
		
		cell.backgroundColor = UIColor.white
		cell.textLabel!.textColor = UIColor.black
		cell.detailTextLabel?.textColor = self.view.tintColor
		cell.selectionStyle = .default
		cell.isUserInteractionEnabled = true
		cell.textLabel?.numberOfLines = 2
		cell.textLabel?.adjustsFontSizeToFitWidth = true
		cell.textLabel?.minimumScaleFactor = 0.7
		cell.textLabel?.lineBreakMode = .byTruncatingMiddle
		cell.isUserInteractionEnabled = true
		cell.accessoryType = .disclosureIndicator
	
		return cell
	}
	
	func reload() {
		tableView.reloadData()
	}
	
	@IBAction func openNew(_ sender: AnyObject) {
		
		CloudKitDomain.shared.hasPrivateAccount { (hasPrivateAccount) -> Void in
			
			if hasPrivateAccount == true {
				let editor = self.storyboard!.instantiateViewController(withIdentifier: "EditorViewController") as! EditorViewController
				self.navigationController?.pushViewController(editor, animated: true)
				
				return
			}else {
				ShowAlertMessage( "", WLoc("iCloudAuthorizationMessage"), self )
			}
		}
	}
	

	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

		let record = CloudKitDomain.shared.myBundleRecords[indexPath.row]
		let controller = storyboard!.instantiateViewController(withIdentifier: "CloudKitPackageBundleViewController") as! CloudKitPackageBundleViewController
		controller.record = record
		navigationController?.pushViewController(controller, animated: true)
		tableView.reloadRows(at: [indexPath], with: .automatic)
		tableView.deselectRow(at: indexPath, animated: true)
	}

  //MARK:- Search
	
	func fetch() {
		
		if GCNetworkReachability.forInternetConnection().isReachable() == false {

			let error = LawError.offline
			displayErrorAlertIfNecessary(error, in: self)
      return
    }
    
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
    tableView.reloadData()
    
    CloudKitDomain.shared.fetchMyPackageBundles() { (error: Error?) -> Void in
      
      displayErrorAlertIfNecessary(error, in: self)
      self.tableView.reloadData()
      UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
  }
}

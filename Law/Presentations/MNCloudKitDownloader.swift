//
//  MNCloudKitDownloader.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2014/09/14.
//  Copyright (c) 2014年 Catalystwo. All rights reserved.
//

#if os(OSX)
	import Cocoa
	#elseif os(iOS)
	import UIKit
#endif

import CloudKit


class MNCloudKitDownloader: NSObject, NSURLConnectionDelegate, NSURLConnectionDataDelegate {
	
	var completionHandler: (_ downloader: MNCloudKitDownloader, _ success: Bool, _ data: Data?)->() = { (downloader: MNCloudKitDownloader, success: Bool, data: Data?) -> () in /* write completion handler here */ }
	
	var URL: Foundation.URL? = nil
	var token: TimeInterval? = nil
	
	class func downloadFileAtURL( _ URL: Foundation.URL, completion: @escaping (_ downloader: MNCloudKitDownloader, _ success: Bool, _ data: Data?)->Void) -> MNCloudKitDownloader {
		
//		NSURLCache.sharedURLCache().diskCapacity = 0
//		NSURLCache.sharedURLCache().removeAllCachedResponses()
		
		let downloader = MNCloudKitDownloader()
		
		downloader.completionHandler = completion
		downloader.URL = URL
		downloader.token = Date.timeIntervalSinceReferenceDate
		print("Downloading asset from URL \(URL)")

		let request = URLRequest(url: URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
		
		let _ = NSURLConnection(request: request, delegate: downloader, startImmediately: true)
		
		return downloader
	}

	
	// MARK:- Download Delegate
	
	var mutableData = NSMutableData()
	
	deinit{
		print("downloader dealloc")
	}
	
	func connection(_ connection: NSURLConnection, didReceive response: URLResponse) {
		mutableData.length = 0
	}
	
	func connection(_ connection: NSURLConnection, didReceive didReceiveData: Data) {
		mutableData.append(didReceiveData)
	}
	
	func connectionDidFinishLoading(_ connection: NSURLConnection) {
		print("\(connection.originalRequest.url!.path) done \(mutableData.length)")
		
		completionHandler(self, true, mutableData as Data)
	}
		
	func connection(_ connection: NSURLConnection, didFailWithError: Error) {
		mutableData.length = 0
		
		print("download error \(didFailWithError.localizedDescription)")
		showError(didFailWithError)
		
		completionHandler(self, false, nil)
	}
	
}

//
//  CloudKitPackageBundleViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 18/08/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import UIKit
import CloudKit

class CloudKitPackageBundleViewController: UIViewController {

	@IBOutlet weak var modificationLabel: UILabel!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var packageBundleLabel: UILabel!
	@IBOutlet weak var countLabel: UILabel!
	@IBOutlet weak var downloadingImageView: CircularProgressIndicator!
	@IBOutlet weak var messageLabel: UILabel!
  var record: CKRecord? = nil {//MyPackageBundleKey {
    didSet {  setupUI() }
  }
  var documentRecord: CKRecord? = nil //LawDocPackageKey
	var docRecordID: CKRecordID? = nil
	var source: NSAttributedString? = nil
	
	// MARK:- Body
	
  
  @IBAction func modifyRecord(_ sender: UIBarButtonItem) {
    guard let record = record else { return }
    
    if GCNetworkReachability.forInternetConnection().isReachable() == false {
      displayErrorAlertIfNecessary( LawError.offline, in: self )
      return
    }
    
    func openEditor() {
      
      let editor = storyboard!.instantiateViewController(withIdentifier: "EditorViewController") as! EditorViewController
      
      editor.packageBundle = self.record?[RecordType.MyPackageBundleKey.packageBundle] as? String ?? ""
      editor.lawTitle = self.record?[RecordType.MyPackageBundleKey.title] as? String ?? ""
      editor.edition = self.documentRecord?[RecordType.LawDocPackageKey.edition] as? String ?? ""
      editor.source = self.source!
      editor.author = self.documentRecord?[RecordType.LawDocPackageKey.creator] as? String ?? ""
      editor.docRecordID = self.docRecordID
      editor.myPkgRecordID = self.record!.recordID
      
      navigationController?.pushViewController(editor, animated: true)
    }
    
    if source != nil {
      openEditor()
      return
    }
    
    let controller = WaitingViewController( nibName: "WaitingViewController", bundle: nil)
    
    controller.modalPresentationStyle = .popover
    let popover = controller.popoverPresentationController
    popover?.delegate = controller
    popover?.permittedArrowDirections = [.up, .down]
    popover?.barButtonItem = sender
    present(controller, animated: true, completion: nil)
    
    CloudKitDomain.shared.downloadSource(for: record,
                                         progress: { [weak controller] progress in controller?.progress = progress }) { [weak self, weak controller] (attributedString, error ) -> Void in
                                          
                                          self?.source = attributedString
                                          controller?.dismiss(animated: true) {
                                            if attributedString != nil {
                                              openEditor()
                                            }else {
                                              if self != nil {
                                                displayErrorAlertIfNecessary(error, in: self!)
                                              }
                                            }
                                          }
                                          
    }
  }
  
  @IBAction func deleteRecord(_ sender: UIBarButtonItem) {
    
    let alertController = UIAlertController(title: WLoc("Delete Record?"),
                                            message: WLoc("Delete Record Message"), preferredStyle: .alert)

		let action1 = UIAlertAction(title: WLoc("Delete"), style: .destructive) { (action) -> Void in
			self.deleteMain()
		}
		
		let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in }
		
		alertController.addAction(action1)
		alertController.addAction(cancel)
		
		present(alertController, animated: true, completion: nil)
	}
	
	func deleteMain() {
		// 1. Delete bundle
		// 2. Delete from list
		// 3. Back
		
		navigationItem.rightBarButtonItem!.isEnabled = false
		
		packageBundleLabel.text = " "
		packageBundleLabel.isHidden = true
		countLabel.text = " "
		downloadingImageView.isHidden = false
		downloadingImageView.indeterminate = true
		
		CloudKitDomain.shared.deleteMyPackageBundle( record! ) {
			( success: Bool, error: Error? ) -> Void in
		
			self.downloadingImageView.indeterminate = false
			self.downloadingImageView.isHidden = true
			
			if success == true {
				let _ = self.navigationController?.popViewController(animated: true)
			}else {
				displayErrorAlertIfNecessary(error, in: self)
			}
		}
	}
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let _ = CloudKitDomain.shared // Instantiate before use ...writing action involves
    
    navigationItem.rightBarButtonItem?.title = WLoc("Modify")
    navigationItem.rightBarButtonItem?.isEnabled = false
    
    messageLabel.text = WLoc("CloudKitPackageBundleViewControllerMessage")
    
    setupUI()
  }
  
  func setupUI() {
    if isViewLoaded == false { return }
    if record == nil {
      titleLabel.text = ""
    }else {
      titleLabel.text = record![RecordType.MyPackageBundleKey.title] as? String
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    guard let record = record else { return }
    
    title = ""//record!.objectForKey("title") as! String!
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateStyle = DateFormatter.Style.long
		dateFormatter.timeStyle = .short
		dateFormatter.locale = Locale(identifier: "ja_JP")
		dateFormatter.calendar = Calendar(identifier: Calendar.Identifier.japanese)

		
		let modificationDate = record.modificationDate

		modificationLabel.text = WLoc("Modification Date:") + (dateFormatter.string(from: modificationDate!))
		
		// Fetch doc package
		countLabel.text = WLoc("Updating Download Count")
		countLabel.isHidden = true
		packageBundleLabel.text = " "
		packageBundleLabel.isHidden = true
		downloadingImageView.isHidden = false
		downloadingImageView.indeterminate = true
		
		let pkg = record[RecordType.MyPackageBundleKey.packageBundle] as? String ?? ""
		
		CloudKitDomain.shared.fetchRecord(withPackageBundle: pkg, desiredKeys:[
      RecordType.LawDocPackageKey.md5,
      RecordType.LawDocPackageKey.title,
      RecordType.LawDocPackageKey.packageBundle,
      RecordType.LawDocPackageKey.count,
      RecordType.LawDocPackageKey.edition,
      RecordType.LawDocPackageKey.creator
    ]) { (results, error) -> Void in
			
			DispatchQueue.main.async {
				
				self.downloadingImageView.indeterminate = false
				
				self.countLabel.isHidden = false
				self.downloadingImageView.isHidden = true
				self.packageBundleLabel.isHidden = false
				
				if results.count == 0 {
					// Unkonw error
					self.packageBundleLabel.text = "Not Found"
					self.countLabel.text = " "
					
					if error != nil {
						displayErrorAlertIfNecessary(error, in: self)
					}
					return
				}
				
				if let record = results.first {
					
					self.documentRecord = record
					self.docRecordID = record.recordID
					self.packageBundleLabel.text = record[RecordType.LawDocPackageKey.packageBundle] as? String ?? ""
					self.countLabel.text = WLoc("Download:") + String( record[RecordType.LawDocPackageKey.count] as? Int ?? 0 )
					self.navigationItem.rightBarButtonItem?.isEnabled = true
				}
			}
		}
	}

}

//
//  SearchWebView.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2014/08/28.
//  Copyright (c) 2014年 Catalystwo. All rights reserved.
//

import Foundation
import WebKit

/*
Link to this framework causes iTunes Connect error "System/Library/PrivateFrameworks/WebKit.framework/WebKit"

*/



protocol WebSearcher {
	
//	func source() -> String?
	func highlightAllOccurencesOfString( _ str: NSString, position: Int?, completionHandler: @escaping ((_ count: Int)->Void) ) -> Void
	func removeAllHighlights()
	func findPrevious()
	func findNext()
  func jumpTo(_ anchor: String)
	func currentPosition() -> Int
	func canFindNext() -> Bool
	func canFindPrevious() -> Bool
	var scrollView: UIScrollView { get }
	func myLoadRequest(_ request: URLRequest)
	func myLoadHTMLString( _ string: NSMutableString, baseURL: URL?)
	
	func blink( _ myId: String! ) -> Void
	func rehighlight( _ dictionary: DocItem ) -> Void
	func unhighlight( _ uuid: String!, snippet: String! ) -> Void
	
	var lastTappedPoint: CGPoint { get }
	func updateMenuItems()
	
}

protocol HighlightingDelegate : class {
	
	func highlightDefaultColor( ) -> HighlightColor
	func highlight( _ sender: Any?, dictionary: DocItem )
}

func javascriptCode() -> NSString? {
	
	let path = Bundle.main.path(forResource: "UIWebViewSearch", ofType: "js")
	var jsCode: NSString
	
	do {
		
		jsCode = try NSString(contentsOfFile: path!, encoding: String.Encoding.utf8.rawValue)
		
	} catch {
		
		// DECODING ERROR
		
		return nil
	}
	
	return jsCode
}


//
// I'm aware of "The UIWebView class should not be subclassed."  ... according to the document.
//
class SearchWebView : UIWebView, WebSearcher, UIGestureRecognizerDelegate {
	
	weak var highlightingDelegate: HighlightingDelegate? = nil
	
	var currentPosition_: Int = 0
	var maxPositions_: Int = 0
//	var htmlSource_: String?
	var lastTappedPoint: CGPoint = CGPoint.zero
	
	// Scroll Bar
	var verticalScrollBar: WKVerticalScrollBar? = nil
	var accessoryView: WKAccessoryView? = nil
	
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		let gesture = UITapGestureRecognizer( target: self, action: #selector(SearchWebView.tap(_:)))
		gesture.delegate = self
		gesture.numberOfTapsRequired = 1
		addGestureRecognizer(gesture)
		
		// Scroll Bar
    scrollView.contentSize = CGSize.zero
		verticalScrollBar = WKVerticalScrollBar( frame: CGRect(x: 0,y: 0,width: 10,height: 100) )
		
		let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
		let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
		
		verticalScrollBar!.setHandle( backgroundColorScheme.tintColor, for: UIControlState() )
		verticalScrollBar!.setHandle( backgroundColorScheme.tintColor.withAlphaComponent(0.5), for: .selected )
		verticalScrollBar!.handleHitWidth = 20
		verticalScrollBar!.scrollView = scrollView
		addSubview( verticalScrollBar! )
		
		accessoryView = WKAccessoryView( frame: CGRect(x: 0, y: 0, width: 65, height: 30) )
		accessoryView!.foregroundColor = UIColor( white: 0.2, alpha: 1.0 )
		verticalScrollBar!.handleAccessoryView = accessoryView!
		
		scrollView.addObserver(self, forKeyPath: "contentOffset", options: .new, context: nil)
		
		// Sync UD
		
		let keysToSync = ["DefaultDefine", "CustomDefineMessage", "CustomDefine1", "CustomDefine2", "CustomDefine1Title", "CustomDefine2Title", "CustomDefine1URL", "CustomDefine2URL"]
		
		let sharedUD = NSUbiquitousKeyValueStore.default()
		let ud = UserDefaults.standard
		for key in keysToSync {
			
			if let obj = sharedUD.object(forKey: key) {
				ud.set(obj, forKey: key)
			}
		}
		
		ud.synchronize()
    
    
    /// REGISTER INITIAL DEFAULTs
    
    let settingsBundlePath = Bundle.main.path(forResource: "Settings", ofType: "bundle")
    let settingsBundle = Bundle(path: settingsBundlePath!)
    let rootPlistPath = settingsBundle!.path(forResource: "Define", ofType: "plist")
    let settingsDict = NSDictionary(contentsOfFile: rootPlistPath!)
    let settingsItems = settingsDict!["PreferenceSpecifiers"] as! [NSDictionary]

    for itemDict in settingsItems {
      if let value = itemDict["DefaultValue"] {
        if let key = itemDict["Key"] as? String,
        ud.object(forKey: key) == nil {
        ud.set(value, forKey: key)
        }
      }
    }

    
	}
	
	// Not used
	required init?(coder: NSCoder) {
		super.init(coder: coder)
	}
	
	deinit {
		scrollView.removeObserver(self, forKeyPath: "contentOffset")
	}
	
	override func tintColorDidChange() {
		let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
		let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
		
		verticalScrollBar!.setHandle( backgroundColorScheme.tintColor, for: UIControlState() )
		verticalScrollBar!.setHandle( backgroundColorScheme.tintColor.withAlphaComponent(0.5), for: .selected )
	}
	var loadedHTML = false
	override func layoutSubviews() {
		super.layoutSubviews()
		
    if loadedHTML == false {
    scrollView.contentSize = CGSize.zero
    }

		let contentHeight = scrollView.frame.size.height - scrollView.contentInset.top - scrollView.contentInset.bottom
		verticalScrollBar?.frame = CGRect(x: 0, y: scrollView.contentInset.top, width: bounds.size.width, height: contentHeight)
	}
	
  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		
		if keyPath != "contentOffset" { return }
		
		let contentOffsetY = scrollView.contentOffset.y
		let contentHeight = scrollView.contentSize.height
		let frameHeight = scrollView.frame.size.height

		if contentHeight == frameHeight { return }
		
		let percent = (contentOffsetY / (contentHeight - frameHeight)) * 100
		let percentInt = Int(percent)
		accessoryView?.textLabel.text = String(percentInt) + "%"
	}
	
	func gestureRecognizer( _ gestureRecognizer: UIGestureRecognizer,
	                        shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return true
	}
	
	func tap(_ sender: UITapGestureRecognizer) {
		let point = sender.location(in: self)
		lastTappedPoint = point
	}
  
  func jumpTo(_ anchor: String) {
    
  }
  
	func highlightAllOccurencesOfString( _ str: NSString, position: Int?, completionHandler: @escaping ((_ count: Int)->Void) )  {
		
		let jsCode: NSString? = javascriptCode()
		if jsCode == nil { return completionHandler(0) }
		
		stringByEvaluatingJavaScript(from: jsCode! as String)
		
		let startSearch = NSString(format: "uiWebview_HighlightAllOccurencesOfString('%@')", str)
		stringByEvaluatingJavaScript(from: startSearch as String)
		
		let result: String? = stringByEvaluatingJavaScript(from: "uiWebview_SearchResultCount")
		
		if result != nil && (result! as NSString).length > 0 {
			
			currentPosition_ = Int( result! )! - 1
			maxPositions_ = currentPosition_
			
			if position != nil {
				currentPosition_ = maxPositions_ - position! + 1
			}
			let script = NSString(format: "blinkElement(a[%d])", currentPosition_ )
			stringByEvaluatingJavaScript(from: script as String)
		}
		else {
			currentPosition_ = 0
			maxPositions_ = 0
		}
		
		completionHandler(maxPositions_)
	}
	
	func removeAllHighlights() {
		stringByEvaluatingJavaScript( from: "uiWebview_RemoveAllHighlights()" )
	}
	
	func findPrevious() {
		if maxPositions_ <= currentPosition_ { return }
		currentPosition_ = currentPosition_ + 1;
		let previousScrollPosition = NSString(format: "blinkElement(a[%d])", currentPosition_ )
		stringByEvaluatingJavaScript(from: previousScrollPosition as String)
	}
	
	func findNext() {
		if currentPosition_ == 0 { return }
		currentPosition_ = currentPosition_ - 1;
		let nextScrollPosition = NSString(format: "blinkElement(a[%d])", currentPosition_ )
		stringByEvaluatingJavaScript(from: nextScrollPosition as String)
	}
	
	func currentPosition() -> Int {
		return maxPositions_ - currentPosition_ + 1
	}
	
	func canFindNext() -> Bool {
		if currentPosition_ == -1 { return false }
		if currentPosition_ == 0 { return false }
		return true
	}
	
	func canFindPrevious() -> Bool {
		if currentPosition_ == -1 { return false }
		if maxPositions_ <= currentPosition_ { return false }
		return true
	}
	
	func myLoadRequest(_ request: URLRequest) {
    loadedHTML = true
		loadRequest(request)
	}
	
	func myLoadHTMLString( _ string: NSMutableString, baseURL: URL?) {
    loadedHTML = true

		// Look for "附　則"
		//
		let ud = UserDefaults.standard
		ud.synchronize()
		
		var hideHusoku = true
		if let obj = ud.object(forKey: "HideHusoku") as? NSNumber { hideHusoku = obj.boolValue }
		
		if hideHusoku == true {
			
			// Insert Script
			/// Expand/Collapse Javscript
			
			let path = Bundle.main.path(forResource: "ExpandCollapseScript", ofType: "js")
			let script = try! NSString(contentsOfFile: path!, encoding: String.Encoding.utf8.rawValue)
			
			string.replaceOccurrences(of: "</STYLE>", with: "</STYLE>" + (script as String), options: NSString.CompareOptions.caseInsensitive, range: NSMakeRange(0,string.length))
			
			// Insert Button
			let range = string.range(of: "附　則.*（.*</A>", options: [.backwards, .regularExpression], range: NSMakeRange(0, string.length))
			
			if range.length > 0 {
				
				let start = "　<a href=\"javascript:display()\" id=\"husokuTap\">タップして表示</a><br><br><footer id = 'husoku' style=\"display:none;\">" // inset after <br>
				
				string.insert(start, at: NSMaxRange(range))
				
				let bodyTag = string.range(of: "</BODY>", options: [.caseInsensitive, .backwards], range: NSMakeRange(0, string.length))
				let end = "</footer>" // insert before </body>
				
				string.insert(end, at: bodyTag.location)
			}
		}
		
		//
		// Table
		//
		//		string.replaceOccurrencesOfString("<TABLE>", withString: "<div><TABLE>", options: NSStringCompareOptions.CaseInsensitiveSearch, range: NSMakeRange(0,string.length))
		//
		//		string.replaceOccurrencesOfString("</TABLE>", withString: "</TABLE></div>", options: NSStringCompareOptions.CaseInsensitiveSearch, range: NSMakeRange(0,string.length))
		
//		htmlSource_ = string as String

		loadHTMLString(string as String, baseURL: baseURL)
		
		updateMenuItems()
	}
	
//	func source()-> String? {
//		return htmlSource_
//	}
	
	func updateMenuItems() {
		
		// Build Menu Items
		
		let ud = UserDefaults.standard
		var menuItems: [UIMenuItem] = []
		
		let copyItem = UIMenuItem(title: WLoc("Copy"), action: #selector(SearchWebView.myCopy(_:)))
		menuItems.append(copyItem)
		
		let highlightItem = UIMenuItem(title: WLoc("Highlight"), action: #selector(SearchWebView.highlight))
		menuItems.append(highlightItem)
		
		if ud.object(forKey: "DefaultDefine") == nil || ud.bool(forKey: "DefaultDefine") != false {
			let defineItem = UIMenuItem(title: WLoc("Define"), action: #selector(SearchWebView.myDefine(_:)))
			menuItems.append(defineItem)
		}
		
		if ud.bool(forKey: "CustomDefine1") == true {
			
			let title = ud.string(forKey: "CustomDefine1Title") ?? "Custom1"
			let customItem1 = UIMenuItem(title: title, action: #selector(SearchWebView.myCustomDefine1(_:)))
			menuItems.append(customItem1)
		}
		
		if ud.bool(forKey: "CustomDefine2") == true {
			
			let title = ud.string(forKey: "CustomDefine2Title") ?? "Custom2"
			let customItem2 = UIMenuItem(title: title, action: #selector(SearchWebView.myCustomDefine2(_:)))
			menuItems.append(customItem2)
		}
		
		let shareItem = UIMenuItem(title: WLoc("Share"), action: #selector(SearchWebView.myShare(_:)))
		menuItems.append(shareItem)
		
		let shortcutItem = UIMenuItem(title: WLoc("Add Shortcut"), action: #selector(SearchWebView.myAddShortcut(_:)))
		menuItems.append(shortcutItem)
		
		UIMenuController.shared.menuItems = menuItems
	}
	
	// MARK:- HIGHLIGHT
	
	func blink( _ myId: String! ) {
		
		let jsCode: NSString? = javascriptCode()
		if jsCode == nil { return }
		
		stringByEvaluatingJavaScript(from: jsCode! as String)
		let script = "blinkText(\"" + myId + "\"); "
		stringByEvaluatingJavaScript(from: script)
	}
	
	func rehighlight( _ dictionary: DocItem ) {
		guard let jsCode = javascriptCode() else { return }
		
		stringByEvaluatingJavaScript(from: jsCode as String)
		
		let uuid = dictionary.uuid
		if uuid == nil { return } //Unknown Error
		
		var snippet = dictionary.text ?? ""
		let notes = dictionary.notes
		
		if notes != nil && notes!.characters.count > 0 {
			snippet = snippet + "💬"
		}
		
		if let color = dictionary.color {
			
			let className = color.className()
			let script1 = "replaceElementWithElement( \"" + uuid! + "\", \"" + snippet
			let script2 = "\", \"" + className + "\"  )"
			let script = script1 + script2
			
			stringByEvaluatingJavaScript( from: script )
		}
	}
	
	func unhighlight( _ uuid: String!, snippet: String! ) {
		guard let jsCode = javascriptCode() else { return }
		
		stringByEvaluatingJavaScript(from: jsCode as String!)
		
		//  snippet remove tags from snippet
		let script = "replaceElementWith( \"" + uuid + "\", \"" + snippet + "\" )"
		stringByEvaluatingJavaScript( from: script )
	}
	
	func highlightError() {
		
		let alert = UIAlertView(title: WLoc("Sorry"),
		                        message: WLoc("Highlight is not allowed for this selection."),
		                        delegate: nil,
		                        cancelButtonTitle: WLoc("OK") )
		alert.show()
	}
	
	func highlight() {
		guard let jsCode = javascriptCode() else { return }
		
		stringByEvaluatingJavaScript(from: jsCode as String!)
		
		// 1 get smallest possilbe source code that includes selection and A tag
		let prefix = stringByEvaluatingJavaScript(from: "getSelectionIndexAndNode()") as NSString?
		var selectedString = stringByEvaluatingJavaScript(from: "theString_")
		let suffix = stringByEvaluatingJavaScript(from: "suffix_") as NSString?
		let thisContents = stringByEvaluatingJavaScript(from: "thisContents_") as NSString?
		
		// Check validity
		if selectedString == nil { return }
		if thisContents == nil { return }
		if prefix == nil { return }
		if suffix == nil { return }

		// remove return (/n)
		selectedString = selectedString!.replacingOccurrences(of: "\n", with: "")
		
//		let actualPrefix = detectCommonPrefix( prefix, with: thisContents)
//		let actualSuffix = detectCommonPrefix( suffix, with: thisContents)
		let actualPrefix = NSString.detectCommonPrefixNSString( prefix!, with: thisContents!)
		let actualSuffix = NSString.detectCommonPrefixNSString( suffix!, with: thisContents!)

		let index = actualPrefix.length
		
		var selectedHTML: String? = nil
		if actualSuffix.length > index {
			selectedHTML = actualSuffix.substring( from: index ).lowercased()
		}
		
		if selectedHTML == nil {
			print("Unknown error")
			highlightError()
			return
		}
		
		// Does selectedHTML contains highlight already?
		
		if selectedHTML!.range(of: "u class=\"hilt") != nil {
			print("already has highlight")
			highlightError()
			return
		}
		
		// check if suffix contains selectedString... this should be true
		
		if actualSuffix.numberOfMatchesInHtml( selectedString as NSString! ) == 0 {
			print("error")
			highlightError()
			return
		}
		
		// Print
		
//		print("Prefix \(actualPrefix)")
//		print("選択したテキスト　\(selectedString)")
//		print("選択した実HTML　\(selectedHTML)")
		
		// 1.5 check validity
		
		if selectedHTML!.range(of: "br>") != nil {
			print("** return found **")
			highlightError()
			return
		}
		
		if selectedHTML!.range(of: "<div") != nil {
			print("** div found **")
			highlightError()
			return
		}

		
		//		if selectedHTML.rangeOfString("<span") != nil {
		//
		//			println("** tag found **")
		//			return
		//		}
		//
		//		if selectedHTML.rangeOfString("/span>") != nil {
		//
		//			println("** tag found **")
		//			return
		//		}
		
		
		// 2. look for a tag location
		var prefixContent: NSString = actualPrefix
		var ATag: String? = nil
		
		let range = actualPrefix.range(of: "a name=", options: [.caseInsensitive, .backwards] )
		if range.location != NSNotFound {
			
			// 3. get a tag ID
			
			let fromATagString = actualPrefix.substring(from: range.location)
			
			if let tagRange = fromATagString.range(of: "(?<=a name=\")[^\"]*", options: [.caseInsensitive, .regularExpression] ) {
				
				let myID = fromATagString.substring(with: tagRange)
				ATag = myID
				
				print("A TAG \(myID)")
			}
			
			if ATag == nil {
				print("** Where is the tag? **")
				return
			}
			
			prefixContent = fromATagString as NSString
		}
		
    guard nil != ATag else { return }
		
		// 3. Look for the count in the actual prefix
		
		/*
		
		search rule
		
		remove <tag>  from prefix =  trimmedPrefix
		
		*/
		let snippet = selectedHTML!.replacingOccurrences(of: "<[^<>]*>|\n", with: "", options: [.caseInsensitive, .regularExpression]  )
		let numberOfMatches = prefixContent.numberOfMatchesInHtml( snippet as NSString! )
		
		
		// Conclusion
		
		/*
		
		1. A tag
		2. Selected text
		3. Count after A tag
		
		*/
		
		
		let color = highlightingDelegate?.highlightDefaultColor() ?? .yellow
		let dictionary = DocItem(highlightWithColor: color, nearestAnchor: ATag!, matchCount: numberOfMatches, text: snippet)
		
		//
		// Finilizize
		//
		
		let className = color.className()
		
		let script1 = "replaceSelectionWith(\"" + selectedString! + "\", \""
		let script2 = dictionary.uuid! + "\", \"" + className + "\")"
		let script = script1 + script2
		
		stringByEvaluatingJavaScript(from: script)
		
		isUserInteractionEnabled = false
		isUserInteractionEnabled = true
		
		highlightingDelegate?.highlight( self, dictionary: dictionary )
		
		return
		
		// THis is costly
		
		//		println("1")
		//
		//		let prefix = self.stringByEvaluatingJavaScriptFromString("getSelectionPrefix()")
		//		let suffix = self.stringByEvaluatingJavaScriptFromString("suffix_")
		//		let string = self.stringByEvaluatingJavaScriptFromString("theString_")
		//
		//		let actualPrefix = detectCommonPrefix( prefix, with:suffix )
		//
		//		println("2")
		//
		//		let range = actualPrefix.rangeOfString("a name=", options: .CaseInsensitiveSearch | .BackwardsSearch )
		//
		//		println("3")
		//
		//		if range != nil {
		//
		//			let snippet = actualPrefix.substringFromIndex(range!.startIndex)
		//			println("\(snippet)")
		//
		//			println("4")
		//
		//			let range2 = snippet.rangeOfString("(?<=a name=\")[^\"]*", options: .CaseInsensitiveSearch | .RegularExpressionSearch )
		//
		//			let myID = snippet.substringWithRange(range2!)
		//
		//			println("\(myID)")
		//		}
	}
	
	
	//MARK:-
	
	override func canPerformAction( _ action: Selector, withSender sender: Any?) -> Bool {
		
		/*
		cut:
		copy:
		select:
		selectAll:
		paste:
		delete:
		_promptForReplace:
		_transliterateChinese:
		_showTextStyleOptions:
		_define:
		_addShortcut:
		_accessibilitySpeak:
		_accessibilitySpeakLanguageSelection:
		_accessibilityPauseSpeaking:
		_share:
		makeTextWritingDirectionRightToLeft:
		makeTextWritingDirectionLeftToRight:
		highlight
		
		*/
		
		let command = action.description
		
		if "selectAll:" == command { return false }
		if "_addShortcut:" == command { return false }
		if "_share:" == command { return false }
		if "_define:" == command { return false }
		if "copy:" == command { return false }
		
		if command == "highlight" {
			
			let startSearch = "document.getSelection().toString()"
			let result = stringByEvaluatingJavaScript(from: startSearch)
			
			if result?.range(of: "\n") != nil { return false }
			if result?.range(of: "　") != nil { return false }
			return true
		}
		
		if command == "myCopy:" || command == "myDefine:" || command == "myShare:" || command == "myAddShortcut:" || command == "myCustomDefine1:" || command == "myCustomDefine2:" {
			
			return true
		}
		
		if command == "customDefineMessage:" {
			//CustomDefineMessage
			if UserDefaults.standard.bool(forKey: "CustomDefineMessage") != true { return true }
		}
		
		if command == "_accessibilitySpeak:" { return false }
		if command == "_accessibilitySpeakLanguageSelection:" { return false }
		
		let flag =  super.canPerformAction(action, withSender: sender)
		return flag
	}
	
	override func target(forAction action: Selector, withSender sender: Any?) -> Any? {
		
		let flag = self.canPerformAction(action, withSender: sender)
		
		if flag == true { return self }
		return nil
	}
	
	func myCopy(_ sender: Any?) {
		if self.responds(to: #selector(copy(_:))) {
			self.perform(#selector(copy(_:)), with:sender)
		}
	}
	
	func myDefine(_ sender: Any?) {
		let selector = NSSelectorFromString("_define:")
		if self.responds(to: selector) {
			self.perform(selector, with: sender)
		}
	}
	
	func myShare(_ sender: Any?) {
		let selector = NSSelectorFromString("_share:")
		if self.responds(to: selector) {
			self.perform(selector, with: sender)
		}
	}
	
	func myAddShortcut(_ sender: Any?) {
		let selector = NSSelectorFromString("_addShortcut:")
		if self.responds(to: selector) {
			self.perform(selector, with: sender)
		}
	}
	
	func myCustomDefine1(_ sender: Any?) {
		
		let ud = UserDefaults.standard
		
		let startSearch = "document.getSelection().toString()"
		
		if let result = self.stringByEvaluatingJavaScript(from: startSearch) {
			
			let urlString = ud.string(forKey: "CustomDefine1URL") ?? ""
      let actualURLString = urlString.replacingOccurrences(of: "_q_", with: result)
      if let path = actualURLString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed), let url = URL(string: path) {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
      }
		}
		
	}
	
	func myCustomDefine2(_ sender: Any?) {
		
		let ud = UserDefaults.standard
		
		let startSearch = "document.getSelection().toString()"
		
		if let result = self.stringByEvaluatingJavaScript(from: startSearch) {

      let urlString = ud.string(forKey: "CustomDefine2URL") ?? ""
			let actualURLString = urlString.replacingOccurrences(of: "_q_", with: result)
			
      if let path = actualURLString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed), let url = URL(string: path) {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
      }
    }
		
	}
}

// MARK:- WK
class SearchWKWebView : WKWebView, WebSearcher, UIGestureRecognizerDelegate {
	
	var currentPosition_: Int = 0
	var maxPositions_: Int = 0
	var lastTappedPoint: CGPoint = CGPoint.zero
	
	weak var highlightingDelegate: HighlightingDelegate? = nil
	
//	var htmlSource_:String?
	
	
	// Scroll Bar
	var verticalScrollBar: WKVerticalScrollBar? = nil
	var accessoryView: WKAccessoryView? = nil
	
	init(frame: CGRect) {
		let config = WKWebViewConfiguration()
		//		config.selectionGranularity = WKSelectionGranularity.Character
		//		config.suppressesIncrementalRendering = true
		super.init(frame: frame, configuration: config)
		
		let gesture = UITapGestureRecognizer( target: self, action: #selector(SearchWebView.tap(_:)))
		gesture.delegate = self
		gesture.numberOfTapsRequired = 1
		self.addGestureRecognizer(gesture)
		
		
		// Scroll Bar
		verticalScrollBar = WKVerticalScrollBar( frame: CGRect(x: 0,y: 0,width: 10,height: 100) )
		
		let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
		let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
		
		
		verticalScrollBar!.setHandle( backgroundColorScheme.tintColor, for:UIControlState() )
		verticalScrollBar!.setHandle( backgroundColorScheme.tintColor.withAlphaComponent(0.5), for:.selected )
		
		verticalScrollBar!.handleHitWidth = 20
		
		verticalScrollBar!.scrollView = self.scrollView
		
		self.addSubview( verticalScrollBar! )
		
		accessoryView = WKAccessoryView( frame: CGRect(x: 0, y: 0, width: 65, height: 30) )
		accessoryView!.foregroundColor = UIColor( white:0.2, alpha:1.0 )
		verticalScrollBar!.handleAccessoryView = accessoryView!
		
		
		self.scrollView.addObserver(self, forKeyPath: "contentOffset", options: .new, context: nil)
		
		
		// Sync UD
		
		let keysToSync = ["DefaultDefine", "CustomDefineMessage", "CustomDefine1", "CustomDefine2", "CustomDefine1Title", "CustomDefine2Title", "CustomDefine1URL", "CustomDefine2URL"]
		
		let sharedUD = NSUbiquitousKeyValueStore.default()
		let ud = UserDefaults.standard
		for key in keysToSync {
			
			if let obj = sharedUD.object(forKey: key) {
				ud.set(obj, forKey: key)
			}
		}
		
		ud.synchronize()
	}
  
	required init?(coder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}
	
	deinit {
		self.scrollView.removeObserver(self, forKeyPath: "contentOffset")
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		let bounds = self.bounds
		
		let contentHeight = self.scrollView.frame.size.height - self.scrollView.contentInset.top - self.scrollView.contentInset.bottom
		verticalScrollBar?.frame = CGRect(x: 0, y: self.scrollView.contentInset.top, width: bounds.size.width, height: contentHeight)
	}
	
	func gestureRecognizer( _ gestureRecognizer: UIGestureRecognizer,
	                        shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return true
	}
	
	func tap(_ sender:UITapGestureRecognizer) {
		
		let point = sender.location(in: self)
		
		lastTappedPoint = point
	}
	
	
//	func source()-> String? {
//		return htmlSource_
//	}
	
  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		
		if keyPath != "contentOffset" { return }
		
		let contentOffsetY = self.scrollView.contentOffset.y
		let contentHeight = self.scrollView.contentSize.height
		let frameHeight = self.scrollView.frame.size.height
		
		if contentHeight == frameHeight { return }
		
		let percent = (contentOffsetY / (contentHeight - frameHeight)) * 100
		
		let percentInt = Int(percent)
		accessoryView?.textLabel.text = String(percentInt) + "%"
	}
  
  func jumpTo(_ anchor: String) {
    let path = Bundle.main.path(forResource: "UIWebViewSearch", ofType: "js")
    let jsCode = try! NSString(contentsOfFile: path!, encoding: String.Encoding.utf8.rawValue )
    self.evaluateJavaScript(
    jsCode as String) { (obj: Any?, error: Error?) -> Void in
    }
    
    let script = NSString(format: "jumpToAnchor(\"%@\")", anchor )
    self.evaluateJavaScript(script as String, completionHandler:nil)
    
  }
  
	func highlightAllOccurencesOfString( _ str:NSString, position:Int?, completionHandler:@escaping ((_ count:Int)->Void)  ) -> Void {
		
		let path = Bundle.main.path(forResource: "UIWebViewSearch", ofType: "js")
		let jsCode = try! NSString(contentsOfFile: path!, encoding: String.Encoding.utf8.rawValue )
		self.evaluateJavaScript(
		jsCode as String) { (obj: Any?, error: Error?) -> Void in
			
		}
		
		let startSearch = NSString(format: "uiWebview_HighlightAllOccurencesOfString('%@')", str)
		
		self.evaluateJavaScript(startSearch as String, completionHandler: { (obj: Any?, error: Error?) in
			
      //print(obj)
			
			let result = ( obj as! NSNumber ).intValue
			
			if result > 0 {
				
				self.currentPosition_ = result - 1
				self.maxPositions_ = self.currentPosition_
				
				if position != nil {
					
					self.currentPosition_ = self.maxPositions_ - position! + 1
					
				}
				
				
				let script = NSString(format: "blinkElement(a[%d])", self.currentPosition_ )
				self.evaluateJavaScript(script as String, completionHandler:nil)
				
			}
			else
			{
				self.currentPosition_ = 0
				self.maxPositions_ = 0
			}
			
			completionHandler(self.maxPositions_)
			
		})
		
		
		
	}
	
	func removeAllHighlights() {
		self.evaluateJavaScript( "uiWebview_RemoveAllHighlights()" , completionHandler:nil)
	}
	
	@IBAction func findPrevious() {
		
		if maxPositions_ <= currentPosition_ { return }
		currentPosition_ = currentPosition_ + 1;
		let previousScrollPosition = NSString(format: "a[%d].scrollIntoView()", currentPosition_)
		self.evaluateJavaScript(previousScrollPosition as String, completionHandler:nil)
		
	}
	
	@IBAction func findNext() {
		
		if currentPosition_ == 0 { return }
		currentPosition_ = currentPosition_ - 1;
		let nextScrollPosition = NSString(format: "a[%d].scrollIntoView()", currentPosition_)
		self.evaluateJavaScript(nextScrollPosition as String, completionHandler:nil)
		
	}
	
	func currentPosition() -> Int {
		return currentPosition_
	}
	
	func canFindNext() -> Bool {
		if currentPosition_ == -1 { return false }
		if currentPosition_ == 0 { return false }
		return true
	}
	
	func canFindPrevious() -> Bool {
		if currentPosition_ == -1 { return false }
		if maxPositions_ <= currentPosition_ { return false }
		return true
	}
	
	func myLoadRequest(_ request: URLRequest) {
		self.load(request)
	}
	
	func myLoadHTMLString( _ string: NSMutableString, baseURL: URL?) {
		
		// Look for "附　則"
		//
		let ud = UserDefaults.standard
		ud.synchronize()
		
		let obj = ud.object(forKey: "HideHusoku") as? NSNumber
		
		var hideHusoku = true
		
		if obj != nil { hideHusoku = obj!.boolValue }
		
		if hideHusoku == true {
			
			// Insert Script
			
			/// Expand/Collapse Javscript
			
			let path = Bundle.main.path(forResource: "ExpandCollapseScript", ofType: "js")
			let script = try! NSString(contentsOfFile: path!, encoding: String.Encoding.utf8.rawValue)
			
			string.replaceOccurrences(of: "</STYLE>", with: "</STYLE>" + (script as String), options: NSString.CompareOptions.caseInsensitive, range: NSMakeRange(0,string.length))
			
			
			// Insert Button
			
			let range = string.range(of: "附　則.*（.*</A>", options: [.backwards, .regularExpression], range: NSMakeRange(0, string.length))
			
			if range.length > 0 {
				
				let start = "　<a href=\"javascript:display()\" id=\"husokuTap\">タップして表示</a><br><br><footer id = 'husoku' style=\"display:none;\">" // inset after <br>
				
				string.insert(start, at: NSMaxRange(range))
				
				let bodyTag = string.range(of: "</BODY>", options: [.caseInsensitive, .backwards], range: NSMakeRange(0, string.length))
				let end = "</footer>" // insert before </body>
				
				string.insert(end, at: bodyTag.location)
			}
		}
		
//		htmlSource_ = string as String

		loadHTMLString(string as String, baseURL: baseURL)
		updateMenuItems()
	}
	
	func updateMenuItems() {

		// Build Menu Items
		
		let ud = UserDefaults.standard
		var menuItems:[UIMenuItem] = []
		
		let highlightItem = UIMenuItem(title: WLoc("Highlight"), action: #selector(SearchWebView.highlight))
		menuItems.append(highlightItem)
		
		
		if ud.object(forKey: "DefaultDefine") == nil || ud.bool(forKey: "DefaultDefine") != false {
			let defineItem = UIMenuItem(title: WLoc("Define"), action: #selector(SearchWebView.myDefine(_:)))
			menuItems.append(defineItem)
		}
  
		if ud.bool(forKey: "CustomDefine1") == true {
			
			let title = ud.string(forKey: "CustomDefine1Title") ?? "Custom1"
			let customItem1 = UIMenuItem(title: title, action: #selector(SearchWebView.myCustomDefine1(_:)))
			menuItems.append(customItem1)
		}
		
		if ud.bool(forKey: "CustomDefine2") == true {
			
			let title = ud.string(forKey: "CustomDefine2Title") ?? "Custom2"
			let customItem2 = UIMenuItem(title: title, action: #selector(SearchWebView.myCustomDefine2(_:)))
			menuItems.append(customItem2)
		}
		
		UIMenuController.shared.menuItems = menuItems
		//		UIMenuController.sharedMenuController().update()
	}
	
	// MARK:- HIGHLIGHT
	
	func blink( _ myId: String! ) {
		guard let jsCode = javascriptCode() else { return }
		
		evaluateJavaScript(jsCode as String, completionHandler:nil)
		
		let script = "blinkText(\"" + myId + "\"); "
		evaluateJavaScript(script, completionHandler:nil)
	}
	
	func rehighlight( _ dictionary:DocItem ) {
		guard let jsCode = javascriptCode() else { return }
		
		evaluateJavaScript(jsCode as String, completionHandler:nil)
		
		let uuid = dictionary.uuid
		if uuid == nil { return } // Unknown Error
		
		var snippet = dictionary.text ?? ""
		let notes = dictionary.notes
		
		if notes != nil && notes!.characters.count > 0  {
			snippet = snippet + "💬"
		}
		
		if let color = dictionary.color {
			
			let className = color.className()
			let script1 = "replaceElementWithElement( \"" + uuid! + "\", \"" + snippet
			let script2 = "\", \"" + className + "\"  )"
			let script = script1 + script2
			
			evaluateJavaScript( script, completionHandler:nil )
		}
	}
	
	func unhighlight( _ uuid:String!, snippet:String! ) {
		guard let jsCode = javascriptCode() else { return }

		evaluateJavaScript(jsCode as String!, completionHandler:nil)
		
		//  snippet remove tags from snippet
		let script = "replaceElementWith( \"" + uuid + "\", \"" + snippet + "\" )"
		evaluateJavaScript( script, completionHandler:nil )
	}
	
	func highlightError() {
		let alert = UIAlertView(title:WLoc("Sorry"), message:WLoc("Highlight is not allowed for this selection."), delegate:nil, cancelButtonTitle:WLoc("OK") )
		alert.show()
	}
	
	func highlight() {
		guard let jsCode = javascriptCode() else { return }
		
		evaluateJavaScript(jsCode as String!) { (anyObject, error) -> Void in
			
			self.evaluateJavaScript("getSelectionIndexAndNodeAndTheString_AndSuffix_AndThisContents_()") { (anyObject, error) -> Void in
				
				if anyObject == nil {
					self.highlightError()
					return
				}
				
				let array = (anyObject! as! NSString).components(separatedBy: "<$>")
				
				let prefix = array[0]
				var selectedString = array[1]
				let suffix = array[2]
				let thisContents = array[3]
				
				// Check validity
				if selectedString.isEmpty { return }
				
				// remove return (/n)
				
				selectedString = selectedString.replacingOccurrences(of: "\n", with: "")
				
				let actualPrefix = String.detectCommonPrefix( prefix, with: thisContents)
				let actualSuffix = String.detectCommonPrefix( suffix, with: thisContents)
				
				let index: String.Index = actualPrefix.characters.index(actualPrefix.startIndex, offsetBy: actualPrefix.characters.count)
				
				let selectedHTML = actualSuffix.substring( from:index ).lowercased()
				
				// Does selectedHTML contains highlight already?
				
				if selectedHTML.range(of: "u class=\"hilt") != nil {
					
					print("already has highlight")
					self.highlightError()
					
					return
				}
				
				// check if suffix contains selectedString... this should be true
				
				if actualSuffix.numberOfMatchesInHtml( selectedString as NSString! ) == 0 {
					
					print("error")
					self.highlightError()
					return
				}
				
				// Print
				
				print("Prefix \(actualPrefix)")
				print("選択したテキスト　\(selectedString)")
				print("選択した実HTML　\(selectedHTML)")
				
				// 1.5 check validity
				
				
				if selectedHTML.range(of: "br>") != nil {
					print("** return found **")
					self.highlightError()
					return
				}
				
				if selectedHTML.range(of: "<div") != nil {
					print("** div found **")
					self.highlightError()
					return
				}
				
				//		if selectedHTML.rangeOfString("<span") != nil {
				//
				//			println("** tag found **")
				//			return
				//		}
				//
				//		if selectedHTML.rangeOfString("/span>") != nil {
				//
				//			println("** tag found **")
				//			return
				//		}
				
				
				// 2. look for a tag location
				var prefixContent:NSString = actualPrefix as NSString
				var ATag:String? = nil
				
				let range = actualPrefix.range(of: "a name=", options: [.caseInsensitive, .backwards] )
				
				// 3. get a tag ID
				
				if range != nil {
					
					let fromATagString = actualPrefix.substring(from: range!.lowerBound)
					let tagRange = fromATagString.range(of: "(?<=a name=\")[^\"]*", options: [.caseInsensitive, .regularExpression] )
					
					let myID = fromATagString.substring(with: tagRange!)
					ATag = myID
					
					if ATag == nil {
						print("** Where is the tag? **")
						return
					}
					
					print("A TAG \(myID)")
					
					prefixContent = fromATagString as NSString
				}

				// 3. Look for the count in the actual prefix
				
				/*
				
				search rule
				
				remove <tag>  from prefix =  trimmedPrefix
				
				*/
				
				
				
				let snippet = selectedHTML.replacingOccurrences(of: "<[^<>]*>|\n", with: "", options: [.caseInsensitive, .regularExpression]  )
				let numberOfMatches = prefixContent.numberOfMatchesInHtml( snippet as NSString! )
				
				
				// Conclusion
				
				/*
				
				1. A tag
				2. Selected text
				3. Count after A tag
				
				*/
				
				
				let color:HighlightColor = self.highlightingDelegate?.highlightDefaultColor() ?? .yellow
				let dictionary = DocItem(highlightWithColor:color, nearestAnchor:ATag!, matchCount:numberOfMatches, text:snippet)
				
				
				//
				// Finilizize
				//
				
				let className = color.className()
				
				let script1 = "replaceSelectionWith(\"" + selectedString + "\", \""
				let script2 = dictionary.uuid! + "\", \"" + className + "\")"
				
				let script = script1 + script2
				
				self.evaluateJavaScript(script, completionHandler:nil)
				
				self.isUserInteractionEnabled = false
				self.isUserInteractionEnabled = true
				
				self.highlightingDelegate?.highlight( self, dictionary: dictionary )
				
				return
				
				
				// THis is costly
				
				//		println("1")
				//
				//		let prefix = self.stringByEvaluatingJavaScriptFromString("getSelectionPrefix()")
				//		let suffix = self.stringByEvaluatingJavaScriptFromString("suffix_")
				//		let string = self.stringByEvaluatingJavaScriptFromString("theString_")
				//
				//		let actualPrefix = detectCommonPrefix( prefix, with:suffix )
				//
				//		println("2")
				//
				//		let range = actualPrefix.rangeOfString("a name=", options: .CaseInsensitiveSearch | .BackwardsSearch )
				//
				//		println("3")
				//
				//		if range != nil {
				//
				//			let snippet = actualPrefix.substringFromIndex(range!.startIndex)
				//			println("\(snippet)")
				//
				//			println("4")
				//
				//			let range2 = snippet.rangeOfString("(?<=a name=\")[^\"]*", options: .CaseInsensitiveSearch | .RegularExpressionSearch )
				//
				//			let myID = snippet.substringWithRange(range2!)
				//
				//			println("\(myID)")
				//		}
				
			}
			
		}
		return
		
	}

	//MARK:-
	
	override var canBecomeFirstResponder:Bool {
		return true
	}
	
	override var canResignFirstResponder: Bool {
		return true
	}
	
	override var next:UIResponder? {
		
		let responder_:UIResponder? = super.next
		
		return responder_
		
		//
		//		while responder_ != nil {
		//
		//
		//			if responder_ != nil  {
		//
		//				let classString = NSStringFromClass(responder_!.dynamicType)
		//				print(classString)
		//			}
		//
		//			responder_ = responder_!.nextResponder()
		//
		//		}
		//
		//
		//		return UIApplication.sharedApplication().delegate as! UIResponder?
	}
	
	override func canPerformAction( _ action: Selector, withSender sender: Any?) -> Bool {
		
		/*
		cut:
		copy:
		select:
		selectAll:
		paste:
		delete:
		_promptForReplace:
		_transliterateChinese:
		_showTextStyleOptions:
		_define:
		_addShortcut:
		_accessibilitySpeak:
		_accessibilitySpeakLanguageSelection:
		_accessibilityPauseSpeaking:
		_share:
		makeTextWritingDirectionRightToLeft:
		makeTextWritingDirectionLeftToRight:
		highlight
		
		*/
		print(action)
    
		let command = action.description
		
		
		if "selectAll:" == command { return false }
		if "_addShortcut:" == command { return false }
		if "_share:" == command { return false }
		if "_define:" == command { return false }
		if "copy:" == command { return false }
		
		
		if command == "highlight" {
			
			
			//				let startSearch = "document.getSelection().toString()"
			//
			//				let result = self.stringByEvaluatingJavaScriptFromString(startSearch)
			//
			//				if result?.rangeOfString("\n") != nil { return false }
			//				if result?.rangeOfString("　") != nil { return false }
			
			return true
			
		}
		
		
		if command == "myCopy:" || command == "myDefine:" || command == "myShare:" || command == "myAddShortcut:" || command == "myCustomDefine1:" || command == "myCustomDefine2:" {
			
			return true
		}
		
		if command == "customDefineMessage:" {
			
			//CustomDefineMessage
			
			if UserDefaults.standard.bool(forKey: "CustomDefineMessage") != true { return true }
		}
		
		if command == "_accessibilitySpeak:" { return false }
		if command == "_accessibilitySpeakLanguageSelection:" { return false }
		
		
		let flag =  super.canPerformAction(action, withSender: sender)
		
		return flag
	}
	
	override func target(forAction action: Selector, withSender sender: Any?) -> Any? {
		
		let flag = self.canPerformAction(action, withSender: sender)
		
		if flag == true { return self }
		return nil
	}
	
	override func copy(_ sender:Any?) {
	}
	
	func myCopy(_ sender:AnyObject?) {
		if self.responds(to: #selector(copy(_:))) {
			self.perform(#selector(copy(_:)), with:sender)
		}
	}
	
	func myDefine(_ sender:AnyObject?) {
		let selector = NSSelectorFromString("_define:")
		if self.responds(to: selector) {
			self.perform(selector, with:sender)
		}
	}
	
	func myShare(_ sender:AnyObject?) {
		let selector = NSSelectorFromString("_share:")
		if self.responds(to: selector) {
			self.perform(selector, with:sender)
		}
	}
	
	func myAddShortcut(_ sender:AnyObject?) {
		let selector = NSSelectorFromString("_addShortcut:")
		if self.responds(to: selector) {
			self.perform(selector, with:sender)
		}
	}

	func myCustomDefine1(_ sender:AnyObject?) {
		
		let ud = UserDefaults.standard
		let startSearch = "document.getSelection().toString()"
		
		evaluateJavaScript( startSearch ) {(result: Any?, error: Error?) in
			
			if let _ = result {
				let urlString = (ud.string(forKey: "CustomDefine1URL") ?? "") as String
				
				let actualURLString = urlString.replacingOccurrences(of: "_q_", with: result as! String!)
        if let url = URL(string:actualURLString.addingPercentEncoding(  withAllowedCharacters: CharacterSet.urlQueryAllowed)!) {
          UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
			}
		}
	}
	
	func myCustomDefine2(_ sender:AnyObject?) {
		
		let ud = UserDefaults.standard
		let startSearch = "document.getSelection().toString()"
		
		evaluateJavaScript( startSearch ) {(result: Any?, error: Error?) in
			
			if let _ = result {
				let urlString = (ud.string(forKey: "CustomDefine2URL") ?? "") as String
				
				let actualURLString = urlString.replacingOccurrences(of: "_q_", with: result as! String!)
				
        if let url = URL(string:actualURLString.addingPercentEncoding(  withAllowedCharacters: CharacterSet.urlQueryAllowed)!) {
          UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
			}
		}
	}
}



//
//  ConflictSolver.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2/04/15.
//  Copyright (c) 2015 Catalystwo. All rights reserved.
//


class ConflictSolver: UITableViewController {

	var versions : [NSFileVersion]? = nil {
		didSet {
			tableView.reloadData()
			
			if versions?.count == 0 {
				close()
			}
		}
	}
	var completionHander: ((NSFileVersion?) -> Void)? = nil
	
	override func viewDidLoad() {
		
		super.viewDidLoad()
		
		let item = UIBarButtonItem(barButtonSystemItem: .cancel, target:self, action:#selector(ConflictSolver.close))
		navigationItem.rightBarButtonItem = item
		
		let header = UIView(frame: CGRect(x: 0,y: 0,width: 320,height: 170))
		header.autoresizingMask = .flexibleWidth
		
		let label = UILabel(frame: CGRect(x: 10,y: 10,width: 300,height: 80))
		label.text = WLoc("ConflictMessage")
		label.autoresizingMask = .flexibleWidth
		label.numberOfLines = 0
		label.adjustsFontSizeToFitWidth = true
		label.minimumScaleFactor = 0.5
		header.addSubview(label)
		
		let image = UIImage(named: "iCloud-Conflict")
		let imageView = UIImageView(image: image)
		imageView.frame = CGRect(x: 0,y: 85,width: 320,height: 81)
		imageView.autoresizingMask = .flexibleWidth
		imageView.contentMode = .scaleAspectFit
		header.addSubview(imageView)

		tableView.tableHeaderView = header
	}

	func close() {
		completionHander?(nil)
		self.dismiss(animated: true, completion: nil)
	}

	// MARK: - Table view data source
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if versions == nil { return 0 }
		return versions!.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") ?? UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
		
		let version = versions![indexPath.row]
		
		cell.textLabel!.text = version.localizedNameOfSavingComputer
		
		if let date = version.modificationDate {
			let formatter = DateFormatter()
			formatter.dateStyle = DateFormatter.Style.long
			formatter.timeStyle = .medium
			cell.detailTextLabel!.text = formatter.string(from: date)
		}
		
		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		let version = versions![indexPath.row] 
		completionHander?( version )
		dismiss(animated: true, completion: nil)
	}
}

//
//  EGovDownloader.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 4/08/15.
//  Copyright (c) 2015 Catalystwo. All rights reserved.
//

import Foundation

final class EGovDownloader: UIViewController {
  
  static var bgTask: UIBackgroundTaskIdentifier = 0
  static var downloading = false {
    didSet {
      if downloading == true {
        bgTask = UIApplication.shared.beginBackgroundTask(expirationHandler: nil)
      }else {
        if bgTask != 0 {
          UIApplication.shared.endBackgroundTask( bgTask )
          bgTask = 0
        }
      }
    }
  }
  //
  @IBOutlet weak var loadingImageView: CircularProgressIndicator!
  @IBOutlet weak var messageLabel: UILabel?
  @IBOutlet weak var label: UILabel?
  @IBOutlet weak var progressiveIndicator: UIProgressView?
  
  private var packageBundle: String? = nil
  private var downloadingURL: URL? = nil
  private var edition: String? = nil
  private var linkDictionaries: [LinkDictionary] = []
  
  override var preferredContentSize: CGSize {
    set {}
    get {
      if downloadingURL == nil { return CGSize(width: 60,height: 60) }
      return CGSize(width: 300,height: 94)
    }
  }
  
  var completionHandler: ((_ success: Bool, _ error: Error?, _ packageURL: URL?) -> Void)? = nil
  
  // MARK:- Initializers
  
  init(downloadingURL: URL, packageBundle: String? = nil, edition: String? = nil) {
    // Look for CKDatabase, ehten downlload from downloadingURL
    
    super.init(nibName: "EGovDownloader", bundle: nil)
    
    self.downloadingURL = downloadingURL
    self.packageBundle = packageBundle
    self.edition = edition
    modalPresentationStyle = .popover
    let _ = CloudKitDomain.shared // Instantiate before use ...writing action involves later
    EGovDownloader.downloading = true
  }
  
  init( packageBundle pkg: String ) {
    super.init(nibName: "WaitingViewController", bundle: nil)
    packageBundle = pkg
  }
  
  required init(coder: NSCoder) {
    super.init(nibName: nil, bundle: nil)
    modalPresentationStyle = .popover
  }
  
  deinit {
    print("deinit EGovDownloader")
    EGovDownloader.downloading = false
  }
  
  // MARK:- Body
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Setup UIWebView
    label?.text = WLoc("Downloading")
    messageLabel?.text = "Start downloading from eGov"
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    if GCNetworkReachability.forInternetConnection().isReachable() == false {
      displayErrorAlertIfNecessary( LawError.offline, in: self )
      cancel(self)
      return
    }
    
    if downloadingURL != nil {
      
      if packageBundle != nil {
        downloadFromCloudKitThenWebsite()
      }else {
        downloadFromWebsite()
      }
      
    }else {
      downloadFromCloudKit()
    }
  }
  
  // MARK:-
  
  @IBAction func cancel(_ sender: AnyObject) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
    updateLoadingImages(false)
    completionHandler?(false, nil, nil)
    completionHandler = nil
  }
  
  private func updateLoadingImages(_ downloading: Bool) {
    if downloading == true {
      loadingImageView.indeterminate = true
    }
  }
  
  //
  // MARK: -  Download
  //
  
  private func downloadFromCloudKitThenWebsite() {
    if packageBundle == nil || edition == nil { return }
    
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
    updateLoadingImages(true)
    
    let dict = DocItem( queryWithPackageName: packageBundle!, edition: edition )
    
    self.messageLabel?.text = "Checking iCloud"
    
    CloudKitDomain.shared.downloadLaw( dict, progress: { [weak self] progress in
      self?.loadingImageView.progress = progress
      
    }) { [weak self] (success: Bool, error: Error?) -> Void in
      
      DispatchQueue.main.async {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        if success == true {
          self?.completionHandler?(true, nil, dict.packageURL)
        }else {
          self?.downloadFromWebsite()
        }
      }
    }
  }
  
  private func downloadFromCloudKit() {
    if packageBundle == nil { return }
    
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
    updateLoadingImages(true)
    
    let dict = DocItem( queryWithPackageName: packageBundle!, edition: nil)
    
    CloudKitDomain.shared.downloadLaw( dict, progress: { [weak self] progress in
      self?.loadingImageView.progress = progress
      
    }) { [weak self] (success: Bool, error: Error?) -> Void in
      
      DispatchQueue.main.async {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        self?.completionHandler?(success, error, dict.packageURL)
      }
    }
  }
  
  private func downloadFromWebsite() {
    
    if downloadingURL == nil { return }
    
    
    //			let SAMPLE_URL = "http://law.e-gov.go.jp/htmldata/S34/S34HO121.html"
    //			let SAMPLE_INDEX_URL = "http://law.e-gov.go.jp/htmldata/S34/S34HO121_IDX.html"
    //
    //			var indexURL = NSURL.URLWithString(SAMPLE_INDEX_URL);
    //			var theURL = NSURL.URLWithString(SAMPLE_URL);
				
    // 1. packageURLがサイトURLの場合
    //let lawEGovURL = self.detailItem?.packageURL//NSURL.URLWithString("http://law.e-gov.go.jp/cgi-bin/idxselect.cgi?IDX_OPT=1&H_NAME=%8F%A4%95%57%96%40&H_NAME_YOMI=%82%A0&H_NO_GENGO=H&H_NO_YEAR=&H_NO_TYPE=2&H_NO_NO=&H_FILE_NAME=S34HO127&H_RYAKU=1&H_CTG=1&H_YOMI_GUN=1&H_CTG_GUN=1")
    
    // 1. packageURLがローカルファイルの場合
    // file:///
    
    
    let absoluteString:NSString? = downloadingURL!.absoluteString as NSString?
    
    if absoluteString == nil || absoluteString!.hasPrefix("http://law.e-gov.go.jp") == false {
      
      completionHandler?(false, nil, nil)
      return
    }
				
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
    updateLoadingImages(true)
    
    let _ = DownloadManager.sharedManager.addDownloadTask( "eGovFront", URL: downloadingURL!, progressHandler: { (progress: Double) -> Void in }) { [weak self] (success: Bool, data: Data?, error: Error?) -> Void in
      guard let strongSelf = self else { return }
      if success == false || data == nil {
        DispatchQueue.main.async {
          UIApplication.shared.isNetworkActivityIndicatorVisible = false
          strongSelf.updateLoadingImages(false)
        }
        strongSelf.completionHandler?(false, error, nil)
        return
      }
      
      let result = NSString(data: data!, encoding: String.Encoding.shiftJIS.rawValue)
      if result == nil {
        
        // Decoding error
        DispatchQueue.main.async {
          UIApplication.shared.isNetworkActivityIndicatorVisible = false
          strongSelf.updateLoadingImages(false)
        }
        strongSelf.completionHandler?(false, error, nil)
        return
      }

      strongSelf.downloadFromPageString(result as String!, theSourceURL: strongSelf.downloadingURL, messageHandler: {
        [weak self] (message: String) -> Void in  print(message)
        
        DispatchQueue.main.async { self?.messageLabel?.text = message }
      }) { [weak self] (success: Bool, error: Error?, packageURL: URL?) -> Void in
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        self?.updateLoadingImages(false)
        
        if success == true && packageURL != nil {
          FilePackage.shared.setUpToDate( packageURL! )
        }
        
        self?.completionHandler?(success, error, packageURL)
      }
      strongSelf.downloadingURL = nil
    }
  }

	private func downloadFromPageString(_ pageString: String, theSourceURL: URL?, messageHandler: @escaping ((_ message: String) -> Void) ,completionHandler: @escaping (_ success: Bool, _ error: Error?, _ packageURL: URL?) -> Void) {
		
		let (indexURL, contentURL): (URL?, URL?) = pageString.getSakuinAndData()
		
		if( indexURL == nil || contentURL == nil ) {
			completionHandler(false, nil, nil)
			return
		}
		
		messageHandler("Downloading Index")
    
    let _ = DownloadManager.sharedManager.addDownloadTask( "Index", URL: indexURL!, progressHandler: { (progress: Double) -> Void in }) {
      [weak self] (success: Bool, data: Data?, error: Error?) -> Void in
      
      if success == false  {
        DispatchQueue.main.async {
          completionHandler(false, error, nil)
        }
        return
      }
      
      // Process index file... Task A
      
      messageHandler("Processing")
      var indexResultHTML: NSString? = nil
      
      let  ( _, _, abc ) = NSString.processIndexData(data!, convert: true,  linkDictionaries: &self!.linkDictionaries)
      
      indexResultHTML = abc
      
      // Download Contents... Task B
      
      messageHandler("Downloading Body")
      
      let _ = DownloadManager.sharedManager.addDownloadTask( "Content", URL: contentURL!, progressHandler: {
        [weak self] (progress: Double)->Void in
        
        DispatchQueue.main.async {
          self?.progressiveIndicator?.setProgress(Float(progress), animated: true)
        }
        
      }) { (success: Bool, data: Data?, error: Error?) -> Void in
        
        if success == false {
          DispatchQueue.main.async {
            completionHandler(false, error, nil)
          }
          return
        }
        
        if data == nil || self == nil {
          DispatchQueue.main.async {
            completionHandler(false, LawError.unknown, nil)
          }
          return
        }
        
        
        let name = contentURL?.lastPathComponent as NSString?
        let packageURL = FilePackage.shared.packageURL(from: name! as String )
        
        // Reconstruct link
        
        messageHandler("Reconstructing Links")
        
        let (convertedHTML, title, edited) = NSString.processContentData(data!, packageName: (packageURL.lastPathComponent as NSString).deletingPathExtension ,convert: true, linkDictionaries: &self!.linkDictionaries)
        
        messageHandler("Saving")
        
        // Save Contents
        var plist: DocProperty = [
          .title: title,
          .edition: edited
        ]
        
        if theSourceURL?.path != nil {
          plist[.sourceURL] = theSourceURL!.absoluteString
        }
        
        //
        // Save ... Task A + Task B
        //
        
        print("Cobining")
        
        convertedHTML.replaceCSS(stripCSSInstead: true, zoomScale: 1.0)
        
        let _ = FilePackage.shared.savePackage(name: contentURL?.lastPathComponent as NSString?, plist: plist, indexHtml: indexResultHTML, contentHtml: convertedHTML )
        
        DispatchQueue.main.async {
          completionHandler(true, nil, packageURL)
        }
      }
    }
    
    // [1]  start downloading index
    print("start download")
  }
}

// MARK:- Popover
extension EGovDownloader: UIPopoverPresentationControllerDelegate {
  
  func popoverPresentationControllerShouldDismissPopover( _ popoverPresentationController: UIPopoverPresentationController) -> Bool {
    return false
  }
  
  func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
    return .none
  }
  
  func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
    return .none
  }
}

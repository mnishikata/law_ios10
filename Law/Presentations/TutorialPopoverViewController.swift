//
//  TutorialPopoverViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2/09/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import UIKit

class TutorialPopoverViewController: UIViewController, UIPopoverPresentationControllerDelegate {
	
	@IBOutlet weak var message: UILabel!
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Do any additional setup after loading the view.
		
		message.text = WLoc("Plus Button Tutorial")
	}
	
	override var preferredContentSize: CGSize {
		set {}
		get {
			return CGSize(width: 250,height: 120)
		}
		
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	// MARK:- Popover
	
	func popoverPresentationControllerShouldDismissPopover( _ popoverPresentationController: UIPopoverPresentationController) -> Bool {
		return true
	}
	
	func adaptivePresentationStyle(for controller: UIPresentationController)
		-> UIModalPresentationStyle {
			return .none
	}
	
	func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection:UITraitCollection)
		-> UIModalPresentationStyle {
			return .none
	}
}

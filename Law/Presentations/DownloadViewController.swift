//
//  DownloadViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2014/08/29.
//  Copyright (c) 2014年 Catalystwo. All rights reserved.
//

import UIKit

class DownloadViewController: UIViewController, UIWebViewDelegate {

	@IBOutlet weak var textField: UITextField!
	@IBOutlet weak var webView: UIWebView!
	
	required init() {
		super.init(nibName: "DownloadViewController", bundle: nil)
	}
	
	required init(coder:NSCoder) {
		super.init(nibName: nil, bundle: nil)
	}
	
	@IBAction func click(sender: AnyObject) {
		
		let moji = "民"
		let data = moji.dataUsingEncoding(NSShiftJISStringEncoding, allowLossyConversion: false)
		let sjis:NSString = NSString(data: data!, encoding: NSUTF8StringEncoding)!
		
		print("sjis \(sjis)")
		
		let javacode = "document.index.H_NAME.value = \"\(sjis)\"; document.getElementById('submitbutton').click()"
		print("javacode \(javacode)")

//		self.webView.stringByEvaluatingJavaScriptFromString(javacode)
		
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	override func viewDidAppear(animated: Bool) {
		super.viewDidAppear(animated)
		
		let path = NSBundle.mainBundle().pathForResource("searchform", ofType: "html")

		do {
			let string = try NSString(contentsOfFile: path!, encoding: NSShiftJISStringEncoding)
			webView.loadHTMLString(string as String, baseURL: nil)

		} catch {
		}
	}
	
	//
	// MARK: -  Web View Delegate
	//
	
	func webView( webView: UIWebView,
		shouldStartLoadWithRequest request: NSURLRequest,
		navigationType: UIWebViewNavigationType) -> Bool {
	
		return true
	}
}

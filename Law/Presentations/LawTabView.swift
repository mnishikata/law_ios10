//
//  LawTabView.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 22/09/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import UIKit

class LawTabCell: UICollectionViewCell {
  
  var myTintColor: UIColor!
  weak var label: UILabel!
  weak var selectionBar: UIView!
  var text: String? {
    get {
      return label?.text
    }
    set {
      label?.text = newValue
      setNeedsLayout()
    }
  }
  
  override var isSelected: Bool {
    didSet {
      if isSelected {
        selectionBar?.backgroundColor = myTintColor
      }
      selectionBar?.isHidden = !isSelected
      
      label?.alpha = isSelected ? 1.0 : 0.6
    }
  }
  
  override var isHighlighted: Bool {
    didSet {
        selectionBar?.backgroundColor = myTintColor
      
      if isHighlighted == false && isSelected {
        label?.alpha = 1.0
      }else {
        label?.alpha = isHighlighted ? 1.0 : 0.6
      }
    }
  }
  static var font: UIFont = UIFont.preferredFont(forTextStyle: .subheadline)
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    let label = UILabel(frame: CGRect(x: 0, y: 2.0, width: frame.size.width, height: frame.size.height - 4.0))
    label.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
    label.isUserInteractionEnabled = false
    label.numberOfLines = 1
    label.backgroundColor = .clear
    label.textAlignment = .center
    label.adjustsFontSizeToFitWidth = true
    label.allowsDefaultTighteningForTruncation = true
    label.minimumScaleFactor = 0.8
    label.font = LawTabCell.font
    self.label = label
    contentView.addSubview(label)
    
    let selectionFrame = CGRect(x: 0, y: 7, width: frame.size.width - 0, height: 2.0)
    let selectionBar = UIView(frame: selectionFrame)
    selectionBar.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
    selectionBar.isHidden = true
    selectionBar.isUserInteractionEnabled = false
    selectionBar.backgroundColor = myTintColor
    self.selectionBar = selectionBar
    contentView.addSubview(selectionBar)
    contentView.backgroundColor = UIColor.clear
    
    
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}

protocol LawTabViewSource: NSObjectProtocol {
  
  func titlesForLawTabView(_ : LawTabView) -> [String]
  func lawTabView(_ : LawTabView, didSelectItemAt: Int?)
  func lawTabViewIndexOfSelection(_ : LawTabView) -> Int?
  func lawTabViewPathStringForSelection(_ : LawTabView) -> String
  func lawTabView(_ : LawTabView, moveItemAt: Int, to: Int)
  func lawTabView(_ : LawTabView, removeItemAt: Int) -> Bool

  func lawTabViewDraggingWillStart(_ : LawTabView)
  func lawTabViewDraggingWillEnd(_ : LawTabView)

}

class LawTabView: UIView {
  static var shared = LawTabView(frame: CGRect(x:0, y:0, width: 100, height: 44))
  
  weak var delegate: LawTabViewSource? = nil 
  var flowLayout: LXReorderableCollectionViewFlowLayout!
  weak var collectionView: UICollectionView!
  weak var separator: UIView!
  weak var pathView: UILabel!
  var colorScheme: BackgroundColorSchemeType! {
    didSet {
      collectionView?.backgroundColor = colorScheme.backgroundColor
      self.backgroundColor = colorScheme.backgroundColor
      self.tintColor = colorScheme.tintColor
      pathView?.textColor = colorScheme.subTextColor

      self.separator.backgroundColor = colorScheme.secondaryBackgroundColor
      // reload()
    }
  }
  var textColor: UIColor {
    return colorScheme.textColor
  }
  
  var tabs: [String] {
    get {
      return delegate?.titlesForLawTabView(self) ?? []
    }
  }
  
  init(frame: CGRect, colorScheme: BackgroundColorSchemeType? = nil) {
    super.init(frame: frame)
    
    if colorScheme != nil {
      self.colorScheme = colorScheme
    }
    self.isOpaque = false
    flowLayout = LXReorderableCollectionViewFlowLayout()
    flowLayout.scrollDirection = .horizontal
    flowLayout.minimumLineSpacing = 0.0;
    flowLayout.minimumInteritemSpacing = 5.0;
    flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5.0, bottom: 0, right: 5.0)
    
    //tintAdjustmentMode = .dimmed
    
    // COLLECTION VIEW
    var bounds = self.bounds
    bounds.size.height = 44
    let collectionView = UICollectionView(frame: bounds, collectionViewLayout: flowLayout)
    collectionView.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
    collectionView.dataSource = self
    collectionView.delegate = self
    collectionView.bounces = true
    //collectionView.tintAdjustmentMode = .dimmed
    collectionView.allowsSelection = true
    collectionView.allowsMultipleSelection = false
    collectionView.showsVerticalScrollIndicator = false
    collectionView.showsHorizontalScrollIndicator = false
    if colorScheme != nil {
    collectionView.backgroundColor = colorScheme!.backgroundColor
    }
    collectionView.isOpaque = true
    collectionView.register(LawTabCell.self, forCellWithReuseIdentifier: "Cell")
    self.addSubview(collectionView)
    self.collectionView = collectionView
    
    // SEPARATOR
    let thickness = 1.0 / UIScreen.main.scale
    let view = UIView(frame: CGRect(x: 0, y: 0, width: bounds.size.width, height: thickness))
    view.backgroundColor = .lightGray
    view.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
    separator = view
    self.addSubview(view)

    // PATH
    let pathView = UILabel(frame: CGRect(x: 10, y: 36, width: bounds.size.width, height: 20))
    pathView.backgroundColor = .clear
    pathView.lineBreakMode = .byTruncatingMiddle
    pathView.font = UIFont.preferredFont(forTextStyle: .footnote)
    if colorScheme != nil {
    pathView.textColor = colorScheme!.subTextColor
    }
    pathView.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
    self.pathView = pathView
    self.addSubview(pathView)
    
    self.layer.shadowRadius = 3.0
    self.layer.shadowColor = UIColor.black.cgColor
    self.layer.shadowOpacity = 0.1
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    
    let viewHeight = self.bounds.height
    flowLayout.itemSize = CGSize(width: 60, height: viewHeight)
    flowLayout.invalidateLayout()
  }
  
  func reload() {
    
    collectionView.reloadData()
    
    guard let delegate = delegate else {
      pathView?.text = ""
      return
    }
    
    if let index = delegate.lawTabViewIndexOfSelection(self) {
      
      let selectedItems = collectionView.indexPathsForSelectedItems
      let indexPath = IndexPath(item: index, section: 0)
      
      if selectedItems?.contains(indexPath) != true {
        
        collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
        
        if let cell = collectionView.cellForItem(at: indexPath) as? LawTabCell {
          cell.myTintColor = colorScheme.tintColor
          cell.isSelected = true
        }
      }
    }
    pathView?.text = delegate.lawTabViewPathStringForSelection(self)
  }
}

extension LawTabView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 2
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if section == 1 { return 1 }
    return tabs.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let item = indexPath.item
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! LawTabCell
    
    if indexPath.section == 1 {
      cell.text = "┼"
      cell.myTintColor = colorScheme.tintColor
      cell.label.textColor = colorScheme.textColor
      cell.isSelected = false
      return cell
    }
    let tabs = self.tabs
    if item >= tabs.count { return cell }
    cell.text = tabs[item]
    cell.myTintColor = colorScheme.tintColor
    cell.isSelected = (delegate?.lawTabViewIndexOfSelection(self) == item)
    if cell.isSelected {
      cell.label.textColor = colorScheme.tintColor

    }else {
      cell.label.textColor = textColor
    }
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let itemHeight = collectionView.bounds.size.height - 0

    if indexPath.section == 1 { return CGSize(width: 60, height: itemHeight) }

    guard indexPath.item < tabs.count else { return CGSize(width: 60, height: itemHeight) } // UNKNOWN ERROR

    let title = tabs[indexPath.item] as NSString
    let size = title.size(attributes: [NSFontAttributeName: LawTabCell.font])
    
    return CGSize(width: min( 120, size.width + 20.0 ), height: itemHeight);
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if indexPath.section == 1 {
      delegate?.lawTabView(self, didSelectItemAt: nil)
      
    }else {
      delegate?.lawTabView(self, didSelectItemAt: indexPath.item)
    }
  }

  
  func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
  }
  
}

extension LawTabView: LXReorderableCollectionViewDataSource, LXReorderableCollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView!, itemAt fromIndexPath: IndexPath!, willMoveTo toIndexPath: IndexPath!) {
    
  }
  
  func collectionView(_ collectionView: UICollectionView!, itemAt fromIndexPath: IndexPath!, didMoveTo toIndexPath: IndexPath!) {
    delegate?.lawTabView(self, moveItemAt: fromIndexPath.item, to: toIndexPath.item)
  }
  
  func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
    // DRAGGING START
    if tabs.count == 1 { return false }
    let canDrag = indexPath.section == 0
    
    if canDrag {
      delegate?.lawTabViewDraggingWillStart(self)
    }
    
    return canDrag
  }
  
  func collectionView(_ collectionView: UICollectionView!, itemAt fromIndexPath: IndexPath!, canMoveTo toIndexPath: IndexPath!) -> Bool {
    return toIndexPath.section == 0
  }

  func collectionView(_ collectionView: UICollectionView!, layout collectionViewLayout: UICollectionViewLayout!, didEndDraggingItemAt indexPath: IndexPath!) {
    reload()
  }
  
  func collectionView(_ collectionView: UICollectionView!, layout collectionViewLayout: UICollectionViewLayout!, willEndDraggingItemAt indexPath: IndexPath!) -> Bool {
    // DRAGGING WILL END
    delegate?.lawTabViewDraggingWillEnd(self)

    let pointValue = flowLayout.value(forKeyPath: "currentView.center") as! NSValue
    
    if pointValue.cgPointValue.y < 0 || pointValue.cgPointValue.y > self.bounds.size.height {
      if delegate?.lawTabView(self, removeItemAt: indexPath.row) == true {
        reload()
        return true
      }
    }
    return false
  }
}

//
//  BackupListTableViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 30/08/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import UIKit

class BackupListTableViewController: UITableViewController {

	var backups:[URL] = []
	var dateFormatter:DateFormatter? = nil

	func loadBackups() {
		
		backups = FilePackage.shared.allBackupFiles
		
		backups.sort{ (url1:URL, url2:URL) -> Bool in
			
			do {
				var obj:AnyObject?
				try (url1 as NSURL).getResourceValue( &obj, forKey: .contentModificationDateKey )
				let date1 = obj as! Date
				try (url2 as NSURL).getResourceValue( &obj, forKey: .contentModificationDateKey )
				let date2 = obj as! Date
				
				return  date1.compare( date2 ) == .orderedDescending
				
			} catch {
				return false // Unknown Error
			}
		}
	}
	
    override func viewDidLoad() {
		super.viewDidLoad()
		
		// Uncomment the following line to preserve selection between presentations
		// self.clearsSelectionOnViewWillAppear = false
		
		// Uncomment the following line to display an Edit button in the navigation bar for this view controller.
		// self.navigationItem.rightBarButtonItem = self.editButtonItem()
		
		loadBackups()
		
		dateFormatter = DateFormatter()
//		dateFormatter!.dateStyle = NSDateFormatterStyle.LongStyle
//		dateFormatter!.timeStyle = .ShortStyle
		dateFormatter!.dateFormat = "M月d日 ah:mm"
//		dateFormatter!.locale = NSLocale(localeIdentifier: "ja_JP")
//		dateFormatter!.calendar = NSCalendar(identifier: NSCalendarIdentifierJapanese)
		
		let barbutton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
		navigationItem.rightBarButtonItem = barbutton
		
		title = WLoc("Backup Files")
		
		let toolbarButton = UIBarButtonItem(title: "Backup Now", style: .plain, target: self, action: #selector(backupNow))
		
		toolbarItems = [toolbarButton]
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.isToolbarHidden = false
	}
  
  func done() {
    dismiss(animated: true, completion: nil)
  }
  
  func backupNow() {
    FilePackage.shared.keepBackupForFileAt()
    loadBackups()
    tableView.reloadData()
  }

  // MARK: - Table view data source
  
  override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
    return WLoc("BackupFooter")
  }
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // #warning Incomplete implementation, return the number of rows
    return backups.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") ??  UITableViewCell(style: .value1, reuseIdentifier: "Cell")
    let url = backups[indexPath.row]
    
    var obj:AnyObject? = nil
    do {
      try (url as NSURL).getResourceValue( &obj, forKey: .contentModificationDateKey )
      let date = obj as! Date
      
      cell.textLabel!.text = dateFormatter?.string(from: date)
      
      try (url as NSURL).getResourceValue( &obj, forKey: .fileSizeKey )
      let size = obj as! NSNumber
      
      let numformatter = ByteCountFormatter()
      cell.detailTextLabel!.text = numformatter.string( fromByteCount: size.int64Value ) as String
      
    } catch {
      cell.textLabel!.text = "?"
      cell.detailTextLabel!.text = ""
    }
    
    return cell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    let url = backups[indexPath.row]
    
    let alertController = UIAlertController(title: WLoc("Recover?"), message: WLoc("RecoverMessage"), preferredStyle: .alert)
    
		let action1 = UIAlertAction(title: WLoc("Recover"), style: .default) { (action) -> Void in
			
			FilePackage.shared.recover(from: url)
			
			tableView.deselectRow(at: indexPath, animated: true)
			self.dismiss(animated: true, completion: nil)
		}
		
		let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in
			tableView.deselectRow(at: indexPath, animated: true)
		}
		
		alertController.addAction(action1)
		alertController.addAction(cancel)
		
		present(alertController, animated: true, completion: nil)
	}

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

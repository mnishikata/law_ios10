//
//  ViewControllerWithUtility.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 5/05/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import Foundation
import UIKit

class ViewControllerWithUtility: UIViewController {
	
	private var baseView: UIView? = nil
	private var utilityViewController_: UIViewController? = nil
	var utilityViewController: UIViewController? {
		return utilityViewController_
	}
	
	
	weak var contentView: UIView!
	var fullHeightConstraint: NSLayoutConstraint!
	var splitHeightConstraint: NSLayoutConstraint!

	override func viewDidLoad() {
		super.viewDidLoad()
		
		let view = UIView(frame: self.view.bounds)
		self.view.addSubview(view)
		view.translatesAutoresizingMaskIntoConstraints = false
		contentView = view

		let topConstraint = NSLayoutConstraint(item: contentView, attribute: .top, relatedBy: .equal, toItem: topLayoutGuide, attribute: .top, multiplier: 1.0, constant: 0.0)
		self.view.addConstraint(topConstraint)
		
		fullHeightConstraint = NSLayoutConstraint(item: contentView, attribute: .height, relatedBy: .equal, toItem: self.view, attribute: .height, multiplier: 1.0, constant: 0.0)
		self.view.addConstraint(fullHeightConstraint)

		splitHeightConstraint = NSLayoutConstraint(item: contentView, attribute: .height, relatedBy: .equal, toItem: self.view, attribute: .height, multiplier: 0.5, constant: 0.0)
		self.view.addConstraint(splitHeightConstraint)
		splitHeightConstraint.isActive = false

		let leftConstraint = NSLayoutConstraint(item: contentView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1.0, constant: 0.0)
		self.view.addConstraint(leftConstraint)

		let rightConstraint = NSLayoutConstraint(item: contentView, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1.0, constant: 0.0)
		self.view.addConstraint(rightConstraint)

	}
	
	override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
		
		if utilityViewController_ != nil {
//			let tc1 = self.traitCollection;
//			let tc2 = UITraitCollection(verticalSizeClass: .Compact)
//			let traits = UITraitCollection(traitsFromCollections: [tc1, tc2])
			//setOverrideTraitCollection(traits, forChildViewController: utilityViewController!)
		}
		super.viewWillTransition(to: size, with: coordinator)
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
    
		// ADJUST CONSTAINTS
		if utilityViewController_ != nil && splitHeightConstraint.isActive == false {
			fullHeightConstraint.isActive = false
			splitHeightConstraint.isActive = true
			
			self.view.layoutSubviews()
		}
		
		if utilityViewController_ == nil && splitHeightConstraint.isActive == true {
			fullHeightConstraint.isActive = true
			splitHeightConstraint.isActive = false
			
			self.view.layoutSubviews()
		}
	}
	
	func setUtilityViewController(_ controller: UIViewController?, animated: Bool = true) {
		
		if controller == nil {
			
			if utilityViewController_ != nil {
				
				utilityViewController_!.willMove(toParentViewController: nil)
				
				//setOverrideTraitCollection(nil, forChildViewController: utilityViewController!)
				
				UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 1.5, initialSpringVelocity: 0.5, options: [], animations: {
					self.fullHeightConstraint.isActive = true
					self.splitHeightConstraint.isActive = false

					self.view.layoutSubviews()
					self.baseView?.transform = CGAffineTransform(translationX: 0, y: self.view.bounds.size.height/2)
				}) { success in
					self.baseView?.removeFromSuperview()
					self.utilityViewController_?.removeFromParentViewController()
					self.utilityViewController_ = nil
				}
			}
			
		}else {
			
			utilityViewController_ = controller!
			addChildViewController(controller!)
      
			baseView = UIView(frame: controller!.view.frame)
			baseView!.translatesAutoresizingMaskIntoConstraints = false
			var frame = controller!.view.frame
			frame.origin.y = 10
			frame.size.height -= 10
			frame.origin.x = 0
			controller!.view.frame = frame
			baseView!.addSubview(controller!.view)
			baseView!.isOpaque = false
			baseView!.backgroundColor = UIColor.clear
			let closeButton = UIButton(type: .system)
			closeButton.setImage(UIImage(named: "CloseUtility"), for: UIControlState())
			closeButton.frame = CGRect(x: baseView!.bounds.maxX-33,y: 14,width: 30,height: 30)
			closeButton.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]
			closeButton.addTarget(self, action: #selector(ViewControllerWithUtility.close), for: .touchUpInside)
			baseView!.addSubview(closeButton)
//			let tc1 = self.traitCollection;
//			let tc2 = UITraitCollection(verticalSizeClass: .Compact)
//			let traits = UITraitCollection(traitsFromCollections: [tc1, tc2])
			//setOverrideTraitCollection(traits, forChildViewController: controller!)
			if animated {
				baseView!.transform = CGAffineTransform(translationX: 0, y: self.view.bounds.size.height/2)
			}
			self.view.addSubview(baseView!)
			controller!.view.layer.cornerRadius = 6.0
			controller!.view.layer.masksToBounds = true
      controller!.view.layer.borderWidth = 1.0
			controller!.view.layer.borderColor = UIColor(white: 0.5, alpha: 0.3).cgColor
        
			baseView!.layer.shadowOpacity = 0.6
			baseView!.layer.shadowColor = UIColor(white: 0, alpha: 1).cgColor
			baseView!.layer.shadowOffset = CGSize(width: 0,height: 2)
			baseView!.layer.shadowRadius = 5.0

			let bottomConstraint = NSLayoutConstraint(item: baseView!, attribute: .bottom, relatedBy: .equal, toItem: bottomLayoutGuide, attribute: .top, multiplier: 1.0, constant: -8.0)
			self.view.addConstraint(bottomConstraint)
			let heightConstraint = NSLayoutConstraint(item: baseView!, attribute: .height, relatedBy: .equal, toItem: self.view, attribute: .height, multiplier: 0.5, constant: 0.0)
			self.view.addConstraint(heightConstraint)
			
			let widthConstraint = NSLayoutConstraint(item: baseView!, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1.0, constant: 8.0)
			self.view.addConstraint(widthConstraint)
			
			let rightConstraint = NSLayoutConstraint(item: baseView!, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1.0, constant: -8.0)
			self.view.addConstraint(rightConstraint)
			
			
			self.view.layoutSubviews()
			
			
			if animated {
				UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 1.5, initialSpringVelocity: 0.5, options: [], animations: {

					self.baseView?.transform = CGAffineTransform.identity
				}) { success in
					self.fullHeightConstraint.isActive = false
					self.splitHeightConstraint.isActive = true
					
					self.view.setNeedsLayout()
					self.utilityViewController_?.didMove(toParentViewController: self)
				}
			}else {
				self.view.setNeedsLayout()
				self.utilityViewController_?.didMove(toParentViewController: self)
			}
			
		}
	}
	/*
  override func overrideTraitCollection(forChildViewController childViewController: UIViewController) -> UITraitCollection? {
    let traitCollection = UITraitCollection(verticalSizeClass: .compact)
    return traitCollection
  }
  */
	func close(_ sender: AnyObject) {
		setUtilityViewController(nil)
	}
}

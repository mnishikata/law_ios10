//
//  CircularProgressIndicator.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 22/09/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import Foundation

final class CircularProgressIndicator: UIView {
  
  var indeterminate: Bool = true {
    didSet {
      mDisplayLink?.invalidate()
      mDisplayLink = nil
      
      animationStartProgress = nil
      animationCurrentProgress = 0
      animationInterval = 0
      
      if indeterminate == true {
        mDisplayLink = CADisplayLink(target: self, selector: #selector(self.displayAnimation))
        mDisplayLink!.frameInterval = 1
        mDisplayLink!.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
      }
    }
  }
  var mDisplayLink: CADisplayLink? = nil
  var previousProgress: Double = 0.0
  var animationStartProgress: Double? = 0.0
  var animationEndProgress: Double = 0.0
  var animationCurrentProgress: Double = 0.0
  let animationSpeed = 0.1
  var animationInterval: Double = 0
  
  var progress: Double = 0.0 {
    willSet {
      previousProgress = progress
      
      if indeterminate == true && previousProgress > 0  {
        indeterminate = false
      }
    }
    
    didSet {
      animationStartProgress =  animationCurrentProgress
      animationEndProgress = progress
      
      if progress > 0 && mDisplayLink == nil { // Animation start
        animationInterval = 0
        
        DispatchQueue.main.async {
          self.mDisplayLink?.invalidate()
          self.mDisplayLink = CADisplayLink(target: self, selector: #selector(self.displayAnimation))
          self.mDisplayLink!.frameInterval = 1
          self.mDisplayLink!.add(to: .current, forMode: .commonModes)
        }
      }
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    backgroundColor = UIColor.clear
    layer.drawsAsynchronously = true
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder:aDecoder)
    
    backgroundColor = UIColor.clear
    layer.drawsAsynchronously = true
  }
  
  override func removeFromSuperview() {
    super.removeFromSuperview()
    mDisplayLink?.invalidate()
    mDisplayLink = nil
  }
  
  private func draw(from startAngle: CGFloat, to endAngle: CGFloat ) {
    let ctx = UIGraphicsGetCurrentContext()
    
    let point = CGPoint(x: 15,y: 15)
    let radius: CGFloat = 12.0
    let path = UIBezierPath(arcCenter: point, radius: radius, startAngle: startAngle - CGFloat(M_PI_2), endAngle: endAngle - CGFloat(M_PI_2), clockwise: true)
    
    path.lineWidth = 1.0
    ctx?.setStrokeColor(tintColor.cgColor)
    path.stroke()
  }
  
  func displayAnimation() {
    
    if superview == nil { return }
    
    if indeterminate == true {
      setNeedsDisplay()
      return
    }
    
    if animationStartProgress == nil { return }
    
    let thisInterval = Date().timeIntervalSinceReferenceDate
    if animationInterval == 0 { animationInterval = thisInterval }
    
    let elapsed = thisInterval - animationInterval
    
    // Max 0.5 sec
    
    if animationCurrentProgress >= animationEndProgress {
      // Finish
      
      mDisplayLink?.invalidate()
      mDisplayLink = nil
      return
    }
    
    let speed = ( animationEndProgress - animationStartProgress! ) / 0.5
    animationCurrentProgress = min( animationEndProgress,  animationStartProgress! + speed * elapsed )
    
    setNeedsDisplay()
  }
  
  override func draw(_ rect: CGRect) {
    
    if indeterminate == true {
      
      let thisInterval = Date().timeIntervalSinceReferenceDate
      if animationInterval == 0 { animationInterval = thisInterval }
      
      let elapsed = thisInterval - animationInterval
      let fraction: Double = 1.0/36.0 * 2.0 * M_PI
      let animationCount = elapsed.truncatingRemainder(dividingBy: 1)  * 36
      let startAngle = CGFloat(Double(animationCount) * fraction)
      let endAngle = CGFloat(fraction * 33) + startAngle
      
      draw(from: startAngle, to: endAngle)
      
      return
    }
    
    if animationStartProgress == nil { return }
    
    let endAngle = 2 * M_PI * animationCurrentProgress
    
    // Draw base circle
    let ctx = UIGraphicsGetCurrentContext();
    
    let point = CGPoint(x: 15,y: 15)
    let radius: CGFloat = 12.0
    let path = UIBezierPath(arcCenter: point, radius: radius, startAngle: 0, endAngle: CGFloat(2 * M_PI), clockwise: true)
    
    path.lineWidth = 1.0
    ctx?.setStrokeColor(tintColor.withAlphaComponent(0.1).cgColor)
    path.stroke()
    
    draw(from: 0, to: CGFloat(endAngle))
  }
}




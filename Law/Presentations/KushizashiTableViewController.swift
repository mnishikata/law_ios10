//
//  KushizashiTableView.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2/04/15.
//  Copyright (c) 2015 Catalystwo. All rights reserved.
//

extension RestorationKey {

  static let kushizashiTableViewControllerFindString = "KushizashiTableViewControllerFindString"

}

class KushizashiTableViewController: UITableViewController, UISearchBarDelegate, StackNavigation {
  
	
	var allDocuments_:[DocItem] = {
    return FilePackage.shared.documents.collectWithCondition { (dictionary) -> Any? in
      
      if dictionary.isLawEntity { return dictionary }
      else { return nil }
      
      } as? [DocItem]
    }()!
	var results_:[ (DocItem, Int, String) ] = []
	var findString_ = ""
	weak var searchBar_:UISearchBar? = nil
	var searching_ = false
	var cancelFlag = false
	var didSelectHandler:((_ text:String, _ dictionary:DocItem) -> Void)? = nil
	var history_:NSMutableArray? = nil
	var displayingHistory_ = false
	
  var backgroundColorScheme: BackgroundColorSchemeType = .Paper
	// MARK:- Body
	
  
  required init(restorationInfo: [RestorationKey: Any]?) {
		super.init(nibName: nil, bundle: nil)
		
		if restorationInfo == nil { return }
		
		let obj = restorationInfo![.kushizashiTableViewControllerFindString] as? String
		
		if obj != nil {
			findString_ = obj as String!
		}
	}
	
	required init(coder:NSCoder) {
		super.init(nibName: nil, bundle: nil)
	}
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
    
		//
		
		history_ = NSMutableArray()
		let savedArray = UserDefaults.standard.object(forKey: "SavedHistory") as? NSArray
		if savedArray != nil {
			
			history_ = savedArray!.mutableCopy() as? NSMutableArray
		}
		
		// Search bar
		
		let searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
		searchBar.autoresizingMask = .flexibleWidth
		searchBar.delegate = self
		searchBar.placeholder = WLoc("KushizashiPlaceholder")
    if backgroundColorScheme.inverted {
      searchBar.keyboardAppearance = .dark
      searchBar.barStyle = .blackTranslucent
      tableView?.indicatorStyle = .white
      tableView?.separatorColor = UIColor(white: 1, alpha: 0.3)
    }
    tableView?.tableHeaderView = searchBar
		searchBar_ = searchBar
		
		navigationItem.title = WLoc("KushizashiSearch")
		
		// Search
		
		if findString_.isEmpty == false {
			
			searchBar_!.text = findString_
			search(findString_)
		}
	}
	
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		view.backgroundColor = backgroundColorScheme.tableBackgroundColor
		
		navigationController?.navigationBar.barTintColor = backgroundColorScheme.tableBackgroundColor
		navigationController?.toolbar.barTintColor = backgroundColorScheme.tableBackgroundColor
		navigationController?.navigationBar.barStyle = backgroundColorScheme.barStyle
		navigationController?.toolbar.barStyle = .default
		navigationController?.setToolbarHidden(false, animated: false)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		
		if searchBar_?.text == nil || searchBar_?.text!.isEmpty == true {
			searchBar_?.becomeFirstResponder()
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
	}
	
	func restorationInfo() -> [RestorationKey: Any] {
		
    return [.kushizashiTableViewControllerFindString: findString_]
	}
	
	// MARK:- Search Bar & Scroll
	
	func searchBar( _ searchBar: UISearchBar, textDidChange searchText: String) {
		
		findString_ = searchText
		
		if searchText.isEmpty == true {
			results_.removeAll(keepingCapacity: false)
			self.tableView.reloadData()
		}
	}
	
	func searchBarSearchButtonClicked( _ searchBar: UISearchBar) {
		if self.searching_ == true { return }
		if searchBar_?.text == nil { return }
		
		searching_ = true
		findString_ = searchBar_!.text!
		searchBar_?.resignFirstResponder()
		
		tableView.reloadData()
		
		history_?.remove( findString_ )
		history_?.insert( findString_, at:0 )
		UserDefaults.standard.set( history_, forKey:"SavedHistory")
		
		search( self.findString_ )
	}
	
	override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
		searchBar_?.resignFirstResponder()
	}
	
	// MARK:- Table View Delegate and DataSource
	
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		
		if displayingHistory_ == true {
			if section == 0 && history_ != nil && history_!.count > 0 {
				return WLoc("History")
			}
			return nil
		}
		
		if searching_ == false && results_.count > 0 && findString_.isEmpty == false {
			return WLoc("Found %@", withArguments: searchBar_!.text! )
		}
		
		if searching_ == false {
			return WLoc("Not Found")
		}
		
		return nil
	}
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		if searching_ == false && results_.count == 0 && findString_.isEmpty == true {
			displayingHistory_ = true
			
		}else {
			displayingHistory_ = false
		}
		
		if displayingHistory_ == true { return 2 }
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		if searching_ { return 1 }
		
		if displayingHistory_ == true {
			
			if section == 1 {
				
				if history_ != nil && history_!.count > 0 { return 1 }
				else { return 0 }
			}
			return history_!.count
		}
		
		return results_.count
	}
	
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
		if cell == nil {
			cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
			cell?.backgroundColor = backgroundColorScheme.tableBackgroundColor
			cell?.textLabel?.numberOfLines = 2
			cell?.textLabel?.minimumScaleFactor = 0.75
			cell?.textLabel?.adjustsFontSizeToFitWidth = true
			cell?.textLabel?.lineBreakMode = .byTruncatingMiddle
			cell?.indentationLevel = 0
			cell?.isUserInteractionEnabled = true
			cell?.textLabel?.textColor = backgroundColorScheme.textColor
			cell?.contentView.backgroundColor = backgroundColorScheme.tableBackgroundColor
			
      if backgroundColorScheme.inverted {
        let view = UIView()
        view.backgroundColor = backgroundColorScheme.tableSelectionColor
        cell?.selectedBackgroundView = view
      }
      
			let label = UILabel(frame: CGRect(x: 10,y: 4,width: cell!.contentView.bounds.size.width-20,height: cell!.contentView.bounds.size.height - 8))
			label.tag = 99
			label.autoresizingMask = .flexibleWidth
			label.lineBreakMode = .byTruncatingMiddle
			label.minimumScaleFactor = 0.8
			label.adjustsFontSizeToFitWidth = true
			label.numberOfLines = 0
			label.textColor = backgroundColorScheme.textColor
			if #available(iOS 9.0, *) {
				label.allowsDefaultTighteningForTruncation = true
			} else {
				// Fallback on earlier versions
			}
			cell!.contentView.addSubview( label )
		}
		
		if searching_ {
			
			cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
			cell?.contentView.backgroundColor = backgroundColorScheme.tableBackgroundColor
      cell?.backgroundColor = backgroundColorScheme.tableBackgroundColor

			cell?.frame = CGRect(x: 0,y: 0,width: 320,height: 44)
			let contentRect = cell?.contentView.bounds as CGRect!
			
			// Loading animation
			
			let imageView = CircularProgressIndicator(frame: CGRect(x: 320-30-7, y: (44-30)/2 , width: 30, height: 30) )
			imageView.autoresizingMask = [.flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
			imageView.indeterminate = true
			imageView.tintColor = backgroundColorScheme.tintColor
			let imageSize = imageView.frame.size
			imageView.frame = CGRect( x: ((contentRect?.size.width)! - imageSize.width)/2, y: ((contentRect?.size.height)! - imageSize.height)/2, width: imageSize.width, height: imageSize.height )
			
			cell!.contentView.addSubview(imageView)
			cell!.isUserInteractionEnabled = false
			cell!.textLabel?.text = nil
			
			return cell!
		}
		
		let theLabel = cell?.viewWithTag(99) as? UILabel
		theLabel?.frame = CGRect(x: 15,y: 4,width: cell!.contentView.bounds.size.width-25,height: cell!.contentView.bounds.size.height - 8)
		
		if displayingHistory_ == true {
			if indexPath.section == 1 {
				let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.contentView.backgroundColor = backgroundColorScheme.tableBackgroundColor
				cell.textLabel!.textAlignment = .center
				cell.textLabel!.text = WLoc("Clear History")
				cell.textLabel?.textColor = backgroundColorScheme.tintColor
        cell.textLabel?.backgroundColor = .clear
        cell.backgroundColor = backgroundColorScheme.tableBackgroundColor

        if backgroundColorScheme.inverted {
          let view = UIView()
          view.backgroundColor = backgroundColorScheme.tableSelectionColor
          cell.selectedBackgroundView = view
        }
        
				return cell
			}
			theLabel?.textColor = backgroundColorScheme.textColor
			theLabel?.text = history_!.object( at: indexPath.row ) as? String
			return cell!
		}
		
		
		let (dict, count, _) = results_[indexPath.row]
		
		if count == 0 && dict.highlight == true {
			
			// Higlihgt item
			var attributes:[String : Any] = [:]
			
			let color = dict.color
			
			if color != nil {
				
				if color! == .underline {
					attributes[NSUnderlineColorAttributeName] = UIColor.red
					attributes[NSUnderlineStyleAttributeName] = NSUnderlineStyle.styleThick.rawValue
				}else {
					attributes[NSBackgroundColorAttributeName] = color!.color()
				}
			}
			
			let attrString = NSAttributedString( string: dict.title as String, attributes:attributes )
			
			theLabel?.attributedText = attrString
			theLabel?.frame = CGRect(x: 40,y: 4,width: cell!.contentView.bounds.size.width-50,height: cell!.contentView.bounds.size.height - 8)
			
		}else {
			
			if count == 0 {
				
				cell?.isUserInteractionEnabled = true
				theLabel?.text = (dict.title as String)
				theLabel?.textColor = backgroundColorScheme.subTextColor
				
			}else {
				
				var attributes:[String : Any] = [:]
        attributes[NSForegroundColorAttributeName] = backgroundColorScheme.textColor
				
				let attrString = NSMutableAttributedString( string: dict.title as String, attributes:attributes )
				
        attributes[NSForegroundColorAttributeName] = backgroundColorScheme.subTextColor
				let countAttrString = NSAttributedString( string: " (" + String(count) + ")", attributes:attributes )
				
				attrString.append(countAttrString)
				
				theLabel?.attributedText = attrString
			}
		}
		
		return cell!
	}
	
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		if displayingHistory_ == true {
			
			if indexPath.section == 1 {
				
				let alertController = UIAlertController(title: WLoc("Delete History"), message: WLoc("Delete History Message"), preferredStyle: .alert)
				
				let action1 = UIAlertAction(title: WLoc("Delete"), style: .destructive) { (action) -> Void in
					
					UserDefaults.standard.removeObject( forKey: "SavedHistory")
					
					self.history_?.removeAllObjects()
					self.tableView.reloadData()
				}
				
				let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in }
				
				alertController.addAction(action1)
				alertController.addAction(cancel)
				
				self.present(alertController, animated: true, completion: nil)
				
				tableView.deselectRow(at: indexPath, animated: true)
				
				return
			}
			searchBar_!.text = history_!.object( at: indexPath.row ) as? String
			searchBarSearchButtonClicked( searchBar_! )
			
		}else {
			
			searchBar_?.resignFirstResponder()
			
			let (dict, _, _) = results_[indexPath.row]
			
			if searchBar_?.text != nil {
				didSelectHandler?(searchBar_!.text!, dict )
			}
			
			let indexPath = self.tableView.indexPathForSelectedRow
			
			if indexPath != nil {
				self.tableView.deselectRow(at: indexPath!, animated: true)
			}
		}
	}
	
	override func tableView( _ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		return displayingHistory_
	}
	
	override func tableView( _ tableView: UITableView,
	                         commit editingStyle: UITableViewCellEditingStyle,
	                                            forRowAt indexPath: IndexPath) {
		
		guard history_ != nil else { return }
		
		history_!.removeObject( at: indexPath.row )
		UserDefaults.standard.set( history_, forKey:"SavedHistory")
		
		if history_!.count == 0 {
			tableView.reloadData()
		}else {
			tableView.deleteRows(at: [indexPath], with: .automatic)
		}
	}
	
	// MARK:- Search
	
	func search(_ text:String) {
		
		//KushizashiTableView
		if Thread.isMainThread == false {
			assert( Thread.isMainThread == true, "** Call on main thread! **")
			return
		}
		
		print("Look for \(text)")
		
		let zenkakuText = text.toZenkaku()
		let regex:NSRegularExpression
		do {
			regex = try NSRegularExpression(pattern: NSRegularExpression.escapedPattern(for: zenkakuText), options: [])
		} catch {
			return
		}
		
		var results:[ (DocItem, Int, String ) ] = []
		
		func findIn(_ theDictionary:DocItem, notesDictionary: [String:[DocItem]]) {
			
			autoreleasepool{
				var highlightResults:[ (DocItem, Int, String ) ] = []
				
				////
				if cancelFlag == true { return }
				
        let ( plist, _, contentHtml) = FilePackage.shared.loadPackage(at: theDictionary.packageURL, onlyPlist: false)
				
				if plist == nil { return }
				if contentHtml == nil { return }
				
				let range:NSRange = NSMakeRange(0, contentHtml!.length )
				var numberOfMatches = regex.numberOfMatches( in: contentHtml! as String, options:[], range:range )
				
				let title = plist![DocPropertyKey.title] as! String
				
				// Look for notes
				
				let notesForThisItem = notesDictionary[theDictionary.packageName!] ?? []
				
				DispatchQueue.concurrentPerform(iterations: notesForThisItem.count)  { (i:size_t) in
					
					let dictionary = notesForThisItem[i]
					var notesContains = false
					
					let string = dictionary.notes
					
					let numberOfMatchesInNotes = regex.numberOfMatches( in: string!, options:[], range:NSMakeRange(0, (string! as NSString).length ) )
					
					if numberOfMatchesInNotes > 0 {
						notesContains = true
					}
					
					if  dictionary.highlight == false {
						
						if notesContains == true {
							numberOfMatches = numberOfMatches + numberOfMatchesInNotes
							return
						}
						
					}else if notesContains == true {
						
						// THREAD SAFE
						objc_sync_enter(self)
						highlightResults.append( (dictionary, 0 as Int, dictionary.title as String ) )
						objc_sync_exit(self)
					}
					
				}
				
				// THREAD SAFE
				objc_sync_enter(self)
				
				if highlightResults.count > 0 {
					results.append( (theDictionary, numberOfMatches as Int, title ) )
					results = results + highlightResults
					
				}else if numberOfMatches != 0  {
					results.append( (theDictionary, numberOfMatches as Int, title ) )
				}
				
				objc_sync_exit(self)
			}
		}
		
		// wait until finishedDictionary.count == allDocuments_.count
		let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.utility)
		queue.async {
			// 1. Get notes
			
			let root = FilePackage.shared.documents
			var notesDictionary = [String:[DocItem]]()
			let _ = root.collectWithCondition { (doc) -> Any? in
				if  doc.notes != nil {
					var packages:[DocItem]? = notesDictionary[doc.packageName!]
					
					if packages == nil {
						packages = [DocItem]()
					}
					
					packages!.append(doc)
					notesDictionary[doc.packageName!] = packages
					
				}
				return nil
				} as! [DocItem]
			
			// 2. Dispatch
			DispatchQueue.concurrentPerform( iterations: self.allDocuments_.count,  execute: { (i:size_t) in
				
				if self.cancelFlag == true { return }
				findIn( self.allDocuments_[i], notesDictionary: notesDictionary )
				}
			)
			
			DispatchQueue.main.async {
				
				self.searching_ = false
				self.results_ = results
				self.tableView.reloadData()
			}
		}
	}
}

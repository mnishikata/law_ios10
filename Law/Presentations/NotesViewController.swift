//
//  NotesViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 4/04/15.
//  Copyright (c) 2015 Catalystwo. All rights reserved.
//

import UIKit

extension UINavigationController {
  open override var childViewControllerForStatusBarStyle: UIViewController? {
    return visibleViewController
  }
}

protocol NotesViewControllerDelegate: class  {
	func notesViewControllerWillClose( _ sender: NotesViewController, withChange: Bool )
	func getNotes( _ sender: NotesViewController ) -> String?
	func setNotes( _ sender: NotesViewController, text: String! )
	func deleteNotes( _ sender: NotesViewController )
}

class NotesViewController: UIViewController {

	
	// Property
	weak var delegate: NotesViewControllerDelegate?
	var originalNotes: String?
	var source: DocItem? = nil
	var bookmark: Bookmark? = nil
	
  var backgroundColorScheme: BackgroundColorSchemeType {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    return backgroundColorScheme
  }
  
	// User interface

	@IBOutlet weak var textView: UITextView!

	var undoButton: UIBarButtonItem? = nil
	
  /// MARK:-
  init() {
   super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    if backgroundColorScheme.inverted {
      return UIStatusBarStyle.lightContent
    }
    return .default
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let textView = UITextView(frame: self.view.bounds)
    textView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    textView.font = UIFont.preferredFont(forTextStyle: .body)
    textView.textColor = backgroundColorScheme.textColor
    if backgroundColorScheme.inverted {
      textView.keyboardAppearance = .dark
      textView.backgroundColor = backgroundColorScheme.backgroundColor
    }
    self.view.addSubview(textView)
    self.textView = textView
    
    originalNotes = delegate?.getNotes( self )
    textView.text = originalNotes
    
    let item = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(close))
    let action = UIBarButtonItem(barButtonSystemItem: .action,  target: self, action: #selector(export(_:)))
    let trashButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(trash(_:)))
    let undoButton = UIBarButtonItem( title: WLoc("Undo"), style: .plain, target: self, action: #selector(undo))
    self.undoButton = undoButton
    
    self.title = WLoc("Notes")

    navigationItem.rightBarButtonItems = [item]
    navigationItem.leftBarButtonItems = [undoButton, action,  trashButton]
    
  }
	
	func registerNotifications() {
		let center = NotificationCenter.default
		center.addObserver(self, selector: #selector(keyboardWasShown(_:)), name: .UIKeyboardWillShow, object: nil)
		center.addObserver(self, selector: #selector(keyboardWasShown(_:)), name: .UIKeyboardWillChangeFrame, object: nil)
		center.addObserver(self, selector: #selector(keyboardIsHidden(_:)), name: .UIKeyboardWillHide, object: nil)
		center.addObserver(self, selector: #selector(textStorageDidEdit), name: .NSTextStorageDidProcessEditing, object: textView.textStorage)
	}
	
	
	func unregisterNotifications() {
		let center = NotificationCenter.default
		center.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
		center.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
		center.removeObserver(self, name: .UIKeyboardWillChangeFrame, object: nil)
		center.removeObserver(self, name: .NSTextStorageDidProcessEditing, object: textView.textStorage)
	}
	
	override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    registerNotifications()
    if backgroundColorScheme.inverted {
      navigationController?.navigationBar.barTintColor = backgroundColorScheme.tableBackgroundColor
      navigationController?.toolbar.barTintColor = backgroundColorScheme.tableBackgroundColor
    }
    navigationController?.navigationBar.barStyle = backgroundColorScheme.barStyle
    navigationController?.view.tintColor = backgroundColorScheme.tintColor
    navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: backgroundColorScheme.textColor, NSFontAttributeName: UIFont.preferredFont(forTextStyle: .headline)]
	}
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		let center = NotificationCenter.default
		center.addObserver(self, selector: #selector(textStorageDidEdit), name: .NSTextStorageDidProcessEditing, object: textView.textStorage)

		textView.becomeFirstResponder()
    setNeedsStatusBarAppearanceUpdate()
		undoButton?.isEnabled = textView.undoManager!.canUndo
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		unregisterNotifications()
	}
	
	func textStorageDidEdit() {
		undoButton?.isEnabled = textView.undoManager!.canUndo
	}
	
	var originalInsetBottom: CGFloat = 0
	var originalScrollIndicatorInsets: UIEdgeInsets = UIEdgeInsets()
	
	func keyboardWasShown( _ n: Notification ) {
		
		originalInsetBottom = textView.contentInset.bottom
		originalScrollIndicatorInsets = textView.scrollIndicatorInsets
		
		let d = (n as NSNotification).userInfo!
		var r = (d[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
		r = textView.convert(r, from: nil)
		textView.contentInset.bottom = r.size.height
		textView.scrollIndicatorInsets.bottom = r.size.height
	}
	
	func keyboardIsHidden( _ notification: Notification ) {
		textView.contentInset.bottom = originalInsetBottom
		textView.scrollIndicatorInsets = originalScrollIndicatorInsets
	}

	func undo() {
		textView.undoManager!.undo()
		undoButton?.isEnabled = textView.undoManager!.canUndo
	}

	func export(_ sender: UIBarButtonItem) {
		
		textView.resignFirstResponder()
		
		let controller = UIActivityViewController(activityItems: [textView!.text], applicationActivities: [])
		controller.modalPresentationStyle = UIModalPresentationStyle.popover
		let popover = controller.popoverPresentationController
		popover?.barButtonItem = sender
		present(controller, animated: true, completion: nil)
  }
  
  func close() {
    
    let text = textView.text
    var withChange = false
    if text != originalNotes {
      if text?.isEmpty != false {
        delegate?.deleteNotes( self )
        
      }else {
        delegate?.setNotes( self, text: text )
      }
      withChange = true
      
    } else {
      withChange = false
    }
    
    delegate?.notesViewControllerWillClose( self, withChange: withChange )
    dismiss(animated: true, completion: nil)
  }
  
  
  func trash(_ sender: UIBarButtonItem) {
    
    textView.resignFirstResponder()
    
    let controller = UIAlertController(title: WLoc("Delete Notes"), message: WLoc("Are you sure you want to delete this notes?"), preferredStyle: .actionSheet)
    
    let action1 = UIAlertAction(title: WLoc("Delete"), style: .destructive) { (action) -> Void in
      
      self.delegate?.deleteNotes( self )
      self.delegate?.notesViewControllerWillClose( self, withChange: true )
      self.dismiss(animated: true, completion: nil)
    }
    
    let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in
      let pboardText = UIPasteboard.general
      pboardText.string = ""
    }
    
    controller.addAction(action1)
    controller.addAction(cancel)
    controller.popoverPresentationController?.barButtonItem = sender
    present(controller, animated: true, completion: nil)
  }
}

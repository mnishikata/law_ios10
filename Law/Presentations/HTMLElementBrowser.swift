//
//  HTMLElementBrowser.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 13/04/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import Foundation

class HTMLElementBrowser : UITableViewController {

	var dataSource:[ElementDescriptor]!
	var source:NSString!
	
	init( dataSource:[ElementDescriptor], source:NSString  ) {
		
		super.init( style: UITableViewStyle.plain )
		self.dataSource = dataSource
		self.source = source.replacingOccurrences(of: "\n", with: "") as NSString!
	}
	
	override init( nibName:String?, bundle:Bundle? ) {
		super.init( nibName: nibName, bundle:bundle)
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder:aDecoder)
	}
	
	// MARK: - Table view data source
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return max( 1, dataSource.count )
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
    let element = dataSource[indexPath.row]

		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") ?? UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
		cell.backgroundColor = UIColor.clear
		cell.textLabel!.text = element.title(coded: false) as String

		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		let element = dataSource[indexPath.row]

		if false {
//			let controller = HTMLElementBrowser(dataSource:articles as! [NSDictionary], source:source)
//			navigationController?.pushViewController(controller, animated: true)
		}else {
			// SHOW XTML

			let snippet = NSMutableString(string:source.substring(with: element.range))
			snippet.insert("<html><head><STYLE type=\"text/css\"></STYLE></head><body>", at: 0)
			snippet.append("</body></html>")
			snippet.replaceCSS(stripCSSInstead:false, zoomScale:1.0)
			
			let controller = SnippetViewController(html: snippet)
			navigationController?.pushViewController(controller, animated: true)

		}
	}
}

class SnippetViewController : UIViewController {

	var html:NSString!
	var webView:SearchWebView? = nil

	init( html:NSString  ) {
		
		super.init( nibName: nil, bundle:nil )
		self.html = html
	}
	required init?(coder aDecoder: NSCoder) {
		super.init(coder:aDecoder)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let view = SearchWebView(frame: self.view.bounds)
		webView = view
		webView!.dataDetectorTypes = UIDataDetectorTypes()
		webView!.autoresizingMask = [.flexibleHeight, .flexibleWidth]
		self.view.addSubview( webView! )
		
		webView!.backgroundColor = UIColor.white
		webView!.isOpaque = false
 
		webView!.loadHTMLString(html as String, baseURL: nil)
	}
}

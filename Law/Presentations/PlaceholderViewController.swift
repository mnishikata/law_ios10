//
//  PlaceholderViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 14/08/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import UIKit

class PlaceholderViewController: UIViewController, StackNavigation {
	
  @IBOutlet weak var label: UILabel!
  var backgroundColorScheme: BackgroundColorSchemeType = .Paper
  
  init(backgroundColorScheme: BackgroundColorSchemeType) {
    super.init(nibName: "PlaceholderViewController", bundle: nil)
    self.backgroundColorScheme = backgroundColorScheme
  }
  required init(restorationInfo: [RestorationKey: Any]?) {
    super.init(nibName: "PlaceholderViewController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func restorationInfo() -> [RestorationKey: Any] {
    return [:]
  }
  
	override func viewDidLoad() {
		super.viewDidLoad()
		
    label.textColor = backgroundColorScheme.textColor
		
		navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
		navigationItem.leftItemsSupplementBackButton = true
		
		let color = backgroundColorScheme.backgroundColor
		self.view.backgroundColor = color
		navigationController?.navigationBar.barTintColor = color
		navigationController?.toolbar.barTintColor = color
		navigationController?.navigationBar.barStyle = backgroundColorScheme.barStyle
		
	}
}

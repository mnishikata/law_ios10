//
//  WebViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 25/08/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController,  UIWebViewDelegate {
	
	var searchTerm:String
	var firstPage:Bool = true
	
	var webView:SearchWebView? = nil
	
	init(searchTerm:String) {
		
		self.searchTerm = searchTerm
		super.init(nibName: nil, bundle: nil)
		
		self.title = WLoc("eGovWebsite")
	}

	required init?(coder aDecoder: NSCoder) {
		
		self.searchTerm = ""
		super.init(coder:aDecoder)
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		let view = SearchWebView(frame: self.view.bounds)
		webView = view
		webView!.dataDetectorTypes = UIDataDetectorTypes()
		webView!.autoresizingMask = [.flexibleHeight, .flexibleWidth]
		self.view.addSubview( webView! )
		
		webView!.delegate = self
		webView!.backgroundColor = UIColor.white
		webView!.isOpaque = false
		
		// Do any additional setup after loading the view.
		
		let file = Bundle.main.path(forResource: "EGovForm", ofType: "html")
		
		do {
			let string = try NSMutableString(contentsOfFile: file!, encoding: String.Encoding.shiftJIS.rawValue)
			
			string.replaceOccurrences(of: "###VALUE###", with: searchTerm, options: [], range: NSMakeRange(0, string.length ))
			
			let temp = (NSTemporaryDirectory() as NSString).appendingPathComponent("temp.html")
			let data = string.data(using: String.Encoding.shiftJIS.rawValue)
			
			let _ = try? data?.write(to: URL(fileURLWithPath: temp), options: [])
			
			let request = URLRequest(url: URL(fileURLWithPath: temp))
			webView!.delegate = self
			webView!.loadRequest(request)
			
		} catch {
			// Unknonw weror
		}
		
		let button = UIBarButtonItem(title: WLoc("Create Law"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(WebViewController.upload(_:)))
		let uploadButton = UIBarButtonItem(image: UIImage(named:"Upload"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(WebViewController.upload(_:)))
		let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
		toolbarItems = [uploadButton, button, space]

		let done = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target:self, action: #selector(WebViewController.close))
		navigationItem.rightBarButtonItem = done
	}
	
	func close() {
		let _ = navigationController?.popToRootViewController(animated: true)
	}
	
	func upload(_ sender:AnyObject) {
		
    let storyboard = UIStoryboard(name: "Editor", bundle: nil)
    let nav = storyboard.instantiateInitialViewController() as! CKNavigationController
		present( nav, animated:true, completion:nil )
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	func webViewDidFinishLoad(_ webView: UIWebView) {
		
		if firstPage == true {
			let script = "document.getElementById(\"button\").click()"
			webView.stringByEvaluatingJavaScript( from: script )
			
			firstPage = false
		}
	}
	
	func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool
	{
		print("request \(request.url)")
		
		if "http://law.e-gov.go.jp/cgi-bin/idxsearch.cgi" == request.url?.absoluteString {
			return true
		}
		if request.url?.absoluteString.hasPrefix("file:") == true { return true }
		if request.url?.absoluteString == nil { return false }
		
		let comps = (request.url?.absoluteString)!.getUrlQueryParameters()
		
		if comps["H_FILE_NAME"] != nil  {
			
			// Already download??
			
			let alertController = UIAlertController(title: WLoc("Download?"), message: WLoc("Download? message"), preferredStyle: .actionSheet)

			let action1 = UIAlertAction(title: WLoc("Download"), style: .default) { (action) -> Void in

				//Execute later
				DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {

					let controller = EGovDownloader(downloadingURL:request.url!)

					controller.modalPresentationStyle = .popover
					let popover = controller.popoverPresentationController
					popover?.delegate = controller
					popover?.permittedArrowDirections = .any
					
					let point = (webView as! SearchWebView).lastTappedPoint
					
					popover?.sourceView = webView
					popover?.sourceRect = CGRect(x: point.x, y: point.y, width: 1,height: 1)

					controller.completionHandler =  { [weak self] (success:Bool, error: Error?, packageURL:URL?) -> Void in
					
						defer {
							self?.dismiss(animated: true, completion: {})
						}

						if self == nil { return }
						if success == true && packageURL != nil {
						}else if error != nil {
							ShowAlertMessage( "", error!.localizedDescription, self )
						}
					}
					self.present(controller, animated: true, completion: nil)
				}
			}

			
			let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in }
			
			alertController.addAction(action1)
			alertController.addAction(cancel)
			
			let popover = alertController.popoverPresentationController
			popover?.sourceView = webView
			popover?.sourceRect = CGRect( x: (self.webView?.lastTappedPoint.x)!, y: (self.webView?.lastTappedPoint.y)!, width: 1,height: 1 )
			
			present(alertController, animated: true, completion: nil)
			return false
		}
		
		if navigationType == .linkClicked {
			UIApplication.shared.openURL(request.url!)
			return false
		}
		return false
	}
}

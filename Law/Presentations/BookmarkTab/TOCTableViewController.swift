//
//  BookmarkTableViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 27/08/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import UIKit

class TOCTableViewController: UITableViewController {
  var viewModel: TOCViewModel = TOCViewModel()
  var jumpAction: ((_ linkDictionary: LinkDictionary)->Void)? = nil

	init( viewModel: DetailViewModel, viewSize: CGSize  ) {
		super.init( style: UITableViewStyle.plain )
		self.viewModel.viewSize = viewSize
    self.viewModel.linkDictionaries = viewModel.htmlSource?.extractMidashi() ?? []
    
    self.tabBarItem = UITabBarItem(title: WLoc("TOC"), image: UIImage(named:"TabTOC"), tag: 0)
  }
	
	override init( nibName:String?, bundle:Bundle? ) {
		super.init( nibName: nibName, bundle:bundle)
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder:aDecoder)
	}
	
	override var preferredContentSize: CGSize {
		set {}
		get {
      var contentSize = viewModel.preferredContentSize
      contentSize.height += self.tabBarController!.tabBar.frame.height
      return contentSize
    }
	}
	
  override func viewDidLoad() {
    super.viewDidLoad()
    
    tableView.backgroundColor = UIColor.clear
    tableView.contentInset = UIEdgeInsetsMake(0, 0, self.tabBarController!.tabBar.frame.height, 0)
    tableView.scrollIndicatorInsets = UIEdgeInsetsMake(viewModel.headerHeight, 0, self.tabBarController!.tabBar.frame.height, 0)
    
    if viewModel.backgroundColorScheme.inverted {
      tableView.indicatorStyle = .white
      tableView.separatorColor = UIColor(white: 1, alpha: 0.3)
    }
  }
  
  // MARK: - Table view data source
	
  override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if viewModel.linkDictionaries.count == 0 { return nil }
    let style: UIBlurEffectStyle = viewModel.backgroundColorScheme.inverted ? .dark : .light
    let blur = UIBlurEffect(style: style)
    let view = UIVisualEffectView(frame: CGRect(x: 0, y: 0, width: 320, height: viewModel.headerHeight))
    view.effect = blur
    view.backgroundColor = .clear
    let label = UILabel(frame: CGRect(x: 10, y: 0, width: 100, height: viewModel.headerHeight))
    label.autoresizingMask = [.flexibleRightMargin]
    label.textColor = viewModel.backgroundColorScheme.subTextColor
    label.text = WLoc("TOC")
    label.font = UIFont.preferredFont(forTextStyle: .subheadline)
    view.contentView.addSubview(label)
    
    return view
  }
  
  override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if viewModel.linkDictionaries.count == 0 { return 0 }
    return viewModel.headerHeight
  }
  
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return max( 1, viewModel.linkDictionaries.count )
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		if viewModel.linkDictionaries.count == 0 {
			let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
			cell.textLabel!.text = WLoc("No Toc")
			cell.textLabel!.textAlignment = NSTextAlignment.center
			cell.textLabel!.textColor  = viewModel.backgroundColorScheme.subTextColor
			cell.isUserInteractionEnabled = false
			cell.backgroundColor = UIColor.clear
			
			return cell
		}
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") ?? UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
		
		cell.backgroundColor = UIColor.clear
				
    let (attributedString, subtitle) = viewModel.bodyAttributedString(at: indexPath.row)
		cell.textLabel!.attributedText = attributedString

		if subtitle.isEmpty == false {
			cell.detailTextLabel!.text = subtitle
		}else {
			cell.detailTextLabel!.text = nil
		}
		cell.detailTextLabel!.textColor = viewModel.backgroundColorScheme.subTextColor
    
    if viewModel.backgroundColorScheme.inverted {
      let view = UIView()
      view.backgroundColor = viewModel.backgroundColorScheme.tableSelectionColor
      cell.selectedBackgroundView = view
    }
    
		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		let dict = viewModel.linkDictionaries[indexPath.row]
    jumpAction?(dict)
  }
}

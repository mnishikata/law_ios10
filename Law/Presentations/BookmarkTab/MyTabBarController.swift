//
//  MyTabBarController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 15/04/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import UIKit

class MyTabBarController: UITabBarController, UITabBarControllerDelegate {
  
  init(viewModel: DetailViewModel) {
    super.init( nibName: nil, bundle:nil)
    self.delegate = self
    modalPresentationStyle = .popover
    tabBar.barStyle = viewModel.backgroundColorScheme.barStyle
    tabBar.barTintColor = .clear//viewModel.backgroundColorScheme.popoverBackgroundColor
    tabBar.tintColor = viewModel.backgroundColorScheme.tintColor
    tabBar.backgroundImage = UIImage()
    tabBar.shadowImage = UIImage()
    
    let style: UIBlurEffectStyle = viewModel.backgroundColorScheme.inverted ? .dark : .light
    let blur = UIBlurEffect(style: style)
    let view = UIVisualEffectView(frame: tabBar.bounds)
    view.effect = blur
    view.backgroundColor = .clear
    
    
    tabBar.insertSubview(view, at: 0)
  }
  
  override var viewControllers: [UIViewController]? {
    didSet {
      selectedIndex = UserDefaults.standard.integer(forKey: "MyTabBarControllerSelectedIndex")
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder:aDecoder)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = .clear
    //self.view.tintColor = viewModel.backgroundColorScheme.tintColor
    
  }
  
  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    
    super.willTransition(to: newCollection, with: coordinator)
    
    
    coordinator.animate(alongsideTransition: { (context) in
      
    }, completion: { context in
      if let size = self.selectedViewController?.preferredContentSize {
        self.preferredContentSize = size
      }
    })
  }
  
  func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
    self.preferredContentSize = viewController.preferredContentSize
    
    return true
  }
  
  func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
    if let idx = viewControllers?.index(of: viewController) {
      UserDefaults.standard.set(idx, forKey: "MyTabBarControllerSelectedIndex")
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    if let size = selectedViewController?.preferredContentSize {
      self.preferredContentSize = size
    }
  }
}

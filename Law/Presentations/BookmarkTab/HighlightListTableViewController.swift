//
//  HighlightListTableViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 8/04/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import UIKit

class HighlightListTableViewController: UITableViewController {
  
  var viewModel: HighlightListTableViewModel!
  var jumpAction: ((_ highlight: DocItem)->Void)? = nil
  weak var editButton: UIButton?

  init(detailViewModel: DetailViewModel, viewSize: CGSize) {
    self.viewModel = HighlightListTableViewModel(viewModel: detailViewModel)
    self.viewModel.viewSize = viewSize

    super.init(style: .plain)
    let item = UITabBarItem(title: WLoc("Highlights"), image: UIImage(named:"TabHighlightOff"), selectedImage: UIImage(named:"TabHighlightOn"))
    self.tabBarItem = item
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder:aDecoder)
  }
  
  override var preferredContentSize: CGSize {
    set {}
    get {
      var contentSize = viewModel.preferredContentSize
      contentSize.height += self.tabBarController!.tabBar.frame.height
      return contentSize
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    let nib = UINib(nibName: "HighlightCell", bundle: nil)
    self.tableView.register(nib, forCellReuseIdentifier: "HighlightCell")
    
    tableView.backgroundColor = UIColor.clear
    tableView.contentInset = UIEdgeInsetsMake(0, 0, self.tabBarController!.tabBar.frame.height, 0)
    tableView.scrollIndicatorInsets = UIEdgeInsetsMake(viewModel.headerHeight, 0, self.tabBarController!.tabBar.frame.height, 0)
    
    if viewModel.backgroundColorScheme.inverted {
      tableView?.separatorColor = UIColor(white: 1, alpha: 0.3)
      tableView?.indicatorStyle = .white
    }
    title = WLoc("Highlights")
  }
  
  // MARK: - Table view data source
  override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if viewModel.numberOfHighlights == 0 { return nil }
    let style: UIBlurEffectStyle = viewModel.backgroundColorScheme.inverted ? .dark : .light
    let blur = UIBlurEffect(style: style)
    let view = UIVisualEffectView(frame: CGRect(x: 0, y: 0, width: 320, height: viewModel.headerHeight))
    view.effect = blur
    view.backgroundColor = .clear
    let label = UILabel(frame: CGRect(x: 10, y: 0, width: 200, height: viewModel.headerHeight))
    label.autoresizingMask = [.flexibleRightMargin]
    label.textColor = viewModel.backgroundColorScheme.subTextColor
    label.text = WLoc("Highlights")
    label.font = UIFont.preferredFont(forTextStyle: .subheadline)
    view.contentView.addSubview(label)
    
    let button = UIButton(type: .system)
    
    button.addTarget(self, action: #selector(edit), for: UIControlEvents.touchUpInside)
    button.setTitle(WLoc("Edit"), for: .normal)
    button.sizeToFit()
    button.autoresizingMask = [.flexibleLeftMargin]
    
    let width = button.frame.size.width
    button.frame = CGRect(x: 310 - width, y: 0, width: width, height: viewModel.headerHeight)
    view.contentView.addSubview(button)
    editButton = button
    return view
  }
  
  func edit() {
    setEditing(!self.isEditing, animated: true)
    editButton?.isSelected = isEditing
    if isEditing {
      editButton?.setTitle(WLoc("Done"), for: .normal)
      
    }else {
      editButton?.setTitle(WLoc("Edit"), for: .normal)
    }
    
    editButton?.sizeToFit()
    if let width = editButton?.frame.size.width {
      editButton?.frame = CGRect(x: editButton!.superview!.bounds.size.width - 10 - width, y: 0, width: width, height: viewModel.headerHeight)
    }
  }
  
  override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if viewModel.numberOfHighlights == 0 { return 0 }
    return viewModel.headerHeight
  }
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // #warning Incomplete implementation, return the number of rows
    return max( 1, viewModel.numberOfHighlights)
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if viewModel.numberOfHighlights == 0 {
      return 44
    }
    return viewModel.rowHeight
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if viewModel.numberOfHighlights == 0 {
      let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
      cell.textLabel!.text = WLoc("No Highlight")
      cell.textLabel!.textAlignment = NSTextAlignment.center
      cell.textLabel!.textColor  = viewModel.backgroundColorScheme.subTextColor
      cell.isUserInteractionEnabled = false
      cell.backgroundColor = UIColor.clear
      
      return cell
    }
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "HighlightCell", for: indexPath)
    cell.backgroundColor = .clear
    
    if viewModel.backgroundColorScheme.inverted {
      let view = UIView()
      view.backgroundColor = viewModel.backgroundColorScheme.tableSelectionColor
      cell.selectedBackgroundView = view
    }
    
    let label = cell.viewWithTag(1) as! UILabel
    
    label.attributedText = viewModel.highlightAttributedString(at: indexPath.row)
    return cell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if let highlight = viewModel[indexPath.row] {
      jumpAction?(highlight)
    }
    if tableView.indexPathForSelectedRow != nil {
      tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: true)
    }
  }

  override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    // Return false if you do not want the specified item to be editable.
    if viewModel.numberOfHighlights == 0 { return false }
    return true
  }
  
  // Override to support editing the table view.
  override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
      // Delete the row from the data source
      
      viewModel.delete(at: indexPath.row)
      tabBarController?.preferredContentSize = preferredContentSize
      if viewModel.numberOfHighlights == 0 {
        tableView.reloadData()
      }else {
        tableView.deleteRows(at: [indexPath], with: .fade)
      }
    } else if editingStyle == .insert {
      // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
  }
}

//
//  TextSizeViewController.swift
//  Law
//
//  Created by Masatoshi Nishikata on 9/05/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import UIKit

protocol TextSizeViewControllerDelegate : class {
  
  func textSizeViewController( _ sender: TextSizeViewController, increaseFontSize: Bool )
  func textSizeViewController( _ sender: TextSizeViewController, setBackgroundScheme: BackgroundColorSchemeType )
  func textSizeViewController( _ sender: TextSizeViewController, setGothicFont: Bool )

}


class TextSizeViewController: UIViewController {
  @IBOutlet weak var schemeButton: BackgroundSchemeButton!
  
  @IBOutlet weak var scheme0: BackgroundSchemeButton!
  @IBOutlet weak var scheme1: BackgroundSchemeButton!
  @IBOutlet weak var scheme2: BackgroundSchemeButton!
  @IBOutlet weak var scheme3: BackgroundSchemeButton!
  @IBOutlet weak var scheme4: BackgroundSchemeButton!
  @IBOutlet weak var mincho: UIButton!
  @IBOutlet weak var gothic: UIButton!
  
  weak var delegate: TextSizeViewControllerDelegate?
  var backgroundColorScheme: BackgroundColorSchemeType = .Paper {
    didSet {
      if isViewLoaded {
        if let popover = self.popoverPresentationController {
          popover.backgroundColor = backgroundColorScheme.popoverBackgroundColor
        }
        
        self.view.tintColor = backgroundColorScheme.tintColor
        
        updateUI()
      }
    }
  }

  var isGothic: Bool = false {
    didSet {
      updateUI()
    }
  }
  
  
  override var preferredContentSize: CGSize {
    set {}
    get {
      return CGSize(width: 251, height: 198)
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  init() {
    super.init(nibName: "TextSizeViewController", bundle: nil)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if let popover = self.popoverPresentationController {
      popover.backgroundColor = backgroundColorScheme.popoverBackgroundColor
    }
    self.view.backgroundColor = .clear
    self.view.tintColor = backgroundColorScheme.tintColor
    
    scheme0.scheme = .White
    scheme1.scheme = .Paper
    scheme2.scheme = .Sepia
    scheme3.scheme = .Grey
    scheme4.scheme = .Dark
    
    updateUI()
    
  }
  
  func updateUI() {
    guard isViewLoaded == true else { return }
    
    scheme0.isSelected = false
    scheme1.isSelected = false
    scheme2.isSelected = false
    scheme3.isSelected = false
    scheme4.isSelected = false
    
    if scheme0.scheme == backgroundColorScheme { scheme0.isSelected = true }
    if scheme1.scheme == backgroundColorScheme { scheme1.isSelected = true }
    if scheme2.scheme == backgroundColorScheme { scheme2.isSelected = true }
    if scheme3.scheme == backgroundColorScheme { scheme3.isSelected = true }
    if scheme4.scheme == backgroundColorScheme { scheme4.isSelected = true }
    
    
    var font = UIFont(name: "Hiragino Mincho ProN", size: 18) ?? UIFont.systemFont(ofSize: 18)
    var attr = NSAttributedString(string: "明朝体", attributes: [NSFontAttributeName : font, NSForegroundColorAttributeName: backgroundColorScheme.tintColor])
    mincho.setAttributedTitle(attr, for: .normal)
    mincho.isSelected = isGothic ? false : true
    
    font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 18) ?? UIFont.systemFont(ofSize: 18)
    attr = NSAttributedString(string: "ゴシック体", attributes: [NSFontAttributeName : font, NSForegroundColorAttributeName: backgroundColorScheme.tintColor])
    gothic.setAttributedTitle(attr, for: .normal)
    gothic.isSelected = isGothic ? true : false
  }

  @IBAction func changeFontSize(_ sender: UIButton) {
    self.delegate?.textSizeViewController(self, increaseFontSize: (sender.tag > 0) )
  }
  
  @IBAction func changeBackgroundColor(_ sender: UIButton) {
    let tag = sender.tag
    var scheme: BackgroundColorSchemeType = backgroundColorScheme
    switch tag {
    case 0: scheme = .White
    case 1: scheme = .Paper
    case 2: scheme = .Sepia
    case 3: scheme = .Grey
    case 4: scheme = .Dark
    default: scheme = backgroundColorScheme
    }
    
    self.delegate?.textSizeViewController(self, setBackgroundScheme: scheme)
    self.backgroundColorScheme = scheme
  }
  
  @IBAction func changeFont(_ sender: UIButton) {
    isGothic = sender.tag == 0 ? true : false
    self.delegate?.textSizeViewController(self, setGothicFont: isGothic)
  }
  
}

class BackgroundSchemeButton: UIButton {
  
  var scheme: BackgroundColorSchemeType? = nil {
    didSet {
      setNeedsDisplay()
    }
  }
  
  override func draw(_ rect: CGRect) {
    guard scheme != nil else { return }
    guard let context = UIGraphicsGetCurrentContext() else { return }
    
    var circleRect = rect.integral.insetBy(dx: 5, dy: 5 );
    
    context.saveGState()
    
    scheme!.backgroundColor.set()
    context.fillEllipse(in: circleRect)
    
    scheme!.textColor.withAlphaComponent(0.3).set()
    context.setLineWidth(0.5)
    context.strokeEllipse(in: circleRect)
    
    if isSelected == true {
      circleRect = rect.integral.insetBy(dx: 2, dy: 2 )
      context.setLineWidth(2 )
      
      let frameColor = tintColor! as UIColor!
      context.setStrokeColor((frameColor?.cgColor)! )
      context.strokeEllipse(in: circleRect)
    }
    
    context.restoreGState();
  }
  
  override func titleRect( forContentRect contentRect: CGRect) -> CGRect {
    return CGRect.zero
  }
}

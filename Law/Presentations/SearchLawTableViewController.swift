//
//  SearchLawTableViewController
//  LawInSwift
//
//  Created by masa on 2014/09/09.
//  Copyright (c) 2014年 Catalystwo. All rights reserved.
//

import UIKit
import CloudKit

class SearchLawTableViewController: UITableViewController, UISearchBarDelegate {
  
  func rowHeight(_ status: LawCloudKitFetchStatus) -> CGFloat {
    switch status {
    case .start:		return 60
    case .noResults:	return 90
    case .fetching:	return 60
    case .found:		return 110
    }
  }
  
  @IBOutlet weak var searchBar: UISearchBar!
  
  //var records_: [CKRecord] = [] // LawDocPackageKey
  //var fetched_: LawCloudKitFetchStatus = .start
  var filtererdRecords_: [CKRecord]? = nil

  var md5s: [String] = []

  var showUniqueOnly = true
  var indicatorIncrementor = 0
  var presetString: String? = nil
  //var overflow = false
  var lawDownloadImage: UIImage? = nil
  var lawDownloadedImage: UIImage? = nil
  var kanjiConversionCache = NSCache<NSString, NSString>()
  
  // MARK:- Body
  
	required init() {
		super.init(nibName: "SearchLawTableViewController", bundle: nil)
		
		let _ = CloudKitDomain.shared // Instantiate before use ...writing action involves
	}
	
	required init(coder: NSCoder) {
		super.init(nibName: nil, bundle: nil)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
    
    title = WLoc("Download From Cloud")
    
    searchBar.placeholder = WLoc("Search")
    searchBar.showsScopeBar = true
    searchBar.scopeButtonTitles = [WLoc("First Letter"), WLoc("Keyword Search")]
    searchBar.selectedScopeButtonIndex = 1
    
    let button = UIBarButtonItem(title: WLoc("ShowUnique"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(unique(_:)))
    
    toolbarItems = [button]
    
    let backButton = UIBarButtonItem(title: WLoc("Back"), style: UIBarButtonItemStyle.plain, target: nil, action: nil)
    navigationItem.backBarButtonItem = backButton
    
    CloudKitDomain.shared.hasPrivateAccount() { (hasPrivateAccount: Bool)->Void in
      
      if hasPrivateAccount == true {
        let uploadButton = UIBarButtonItem(image: UIImage(named: "Upload"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.upload(_:)))
        
        self.navigationItem.rightBarButtonItem = uploadButton
      }
    }
  }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		collectMD5s()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    if presetString?.isEmpty == false {
      searchBar.text = presetString
      searchBar.selectedScopeButtonIndex = 0
      
      // Start search
      searchBarSearchButtonClicked( searchBar )
      presetString = nil
      
    }else if searchBar.text == nil || searchBar.text!.characters.count == 0 {
      searchBar.becomeFirstResponder()
    }
  }
  
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    return  [.all]
  }
  
  func upload(_ sender: AnyObject?) {
    
    let storyboard = UIStoryboard(name: "Editor", bundle: nil)
    let nav = storyboard.instantiateInitialViewController() as! CKNavigationController
    present( nav, animated: true, completion: nil )
  }
  
  func unique(_ sender: AnyObject?) {
    
    let alertController = UIAlertController(title: WLoc("ShowUniqueDialogue"), message: WLoc("ShowUniqueDialogueMessage"), preferredStyle: .alert)
    
    let action1 = UIAlertAction(title: WLoc("Show All"), style: .default) { (action) -> Void in
      self.showUniqueOnly = false
      self.tableView.reloadData()
    }
    
    let action2 = UIAlertAction(title: WLoc("Show Unique"), style: .default) { (action) -> Void in
      self.showUniqueOnly = true
      self.tableView.reloadData()
    }
    
    if showUniqueOnly == true {
      alertController.addAction(action1)
    }else {
      alertController.addAction(action2)
    }
    
    present(alertController, animated: true, completion: nil)
  }
  
  func close() {  let _ = navigationController?.popToRootViewController(animated: true) }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    kanjiConversionCache.removeAllObjects()
  }
  
  // MARK: -
  
  override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
    searchBar.resignFirstResponder()
  }
  
  // MARK: - Table view data source
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    if( CloudKitDomain.shared.fetchStatus == .fetching ) { return 1 }
    return 2
  }
  
  override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    
    if section == 0  {
      
      if CloudKitDomain.shared.fetchStatus == .noResults  { return nil }
      if CloudKitDomain.shared.fetchStatus == .found {
        return WLoc("FoundStr") +  " \(filtererdRecords.count) " + WLoc("FoundStrSuffix")
        
      }
    }
    return nil
  }

  var filtererdRecords: [CKRecord] {
    if showUniqueOnly == false { return CloudKitDomain.shared.fetchedRecords }
    if filtererdRecords_ != nil { return filtererdRecords_! }
    
    filtererdRecords_ = CloudKitDomain.shared.uniqueRecords
    return filtererdRecords_!
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // #warning Incomplete method implementation.
    // Return the number of rows in the section.
    
    if( CloudKitDomain.shared.fetchStatus == .fetching  ) { return 1 }
    
    if section == 1 {
      return CloudKitDomain.shared.fetchStatus == .start ? 1 : 2
    }
    return filtererdRecords.count
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if indexPath.section == 1 {
      return indexPath.row == 0 ? rowHeight(CloudKitDomain.shared.fetchStatus) : 56
    }
    return 68
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		var cell = tableView.dequeueReusableCell(withIdentifier: "Cell") 
		if cell == nil {
			cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
			
			cell!.textLabel?.numberOfLines = 2
			cell!.textLabel?.adjustsFontSizeToFitWidth = true
			cell!.textLabel?.minimumScaleFactor = 0.7
			cell!.textLabel?.lineBreakMode = .byTruncatingMiddle
		}
		
		if CloudKitDomain.shared.fetchStatus == .fetching {
			
			cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)

			cell!.frame = CGRect(x: 0,y: 0, width: 320, height: 120)
			let contentRect = cell!.contentView.bounds
			
			// Loading animation
			
			let imageView = CircularProgressIndicator(frame: CGRect(x: 320-30-7, y: (44-30)/2, width: 30, height: 30) )
			imageView.autoresizingMask = [.flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin, .flexibleBottomMargin]
			imageView.indeterminate = true
			
			let imageSize = imageView.frame.size
			imageView.frame = CGRect( x: (contentRect.size.width - imageSize.width)/2, y: (contentRect.size.height - imageSize.height)/2, width: imageSize.width, height: imageSize.height )
			
			cell?.contentView.addSubview(imageView)
			cell?.isUserInteractionEnabled = false
			
			return cell!
		}
		
		if indexPath.section == 1 {
			
			cell = UITableViewCell(style: .default, reuseIdentifier: nil)

			if indexPath.row != 0 {
				if indexPath.row == 1 {
					cell!.textLabel!.font = UIFont.boldSystemFont(ofSize: 18)
					cell!.textLabel!.text = WLoc("Download from eGov")
					cell!.imageView!.image = UIImage(named: "LawEGov")
				}
				
				if indexPath.row == 2 {
					cell!.textLabel!.font = UIFont.boldSystemFont(ofSize: 18)
					cell!.textLabel!.text = WLoc("Upload Center")
					cell!.imageView!.image = UIImage(named: "Upload")
				}
				
				cell!.textLabel!.textColor = self.view.tintColor
				cell!.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
				
				return cell!
			}
			
			if CloudKitDomain.shared.fetchStatus == .start {
				
				cell!.frame = CGRect(x: 0,y: 0,width: 320,height: 120)
				let contentRect = cell!.contentView.bounds
				
				let label = UILabel()
				label.text = WLoc("LawCloudKitFetchStateStart")
				label.textColor = UIColor.lightGray
				label.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
				label.textAlignment = .center
				label.numberOfLines = 0
				label.frame = CGRect( x: 10, y: 10, width: contentRect.size.width - 20, height: contentRect.size.height - 20 )
				label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
				cell!.contentView.addSubview(label)
				cell!.isUserInteractionEnabled = false
			}
			
			if CloudKitDomain.shared.fetchStatus == .found {

				cell!.frame = CGRect(x: 0,y: 0,width: 320,height: 80)
				let contentRect = cell!.contentView.bounds

				let label = UILabel()
				var title = WLoc("LawCloudKitFetchStateFound")
				var hasUsersItem = false

				for record in filtererdRecords {
					if let creator = record[RecordType.LawDocPackageKey.creatorUUID] as? String {
						if creator.isEmpty == false {
							hasUsersItem = true
							break
						}
					}
				}
				
				if hasUsersItem == true {
					title = title + WLoc("Has users item")
				}
				
				label.text = title
				label.textColor = UIColor.lightGray
				label.numberOfLines = 0
				label.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
				label.frame = CGRect( x: 10, y: 10, width: contentRect.size.width - 20, height: contentRect.size.height - 10 )
				label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
				cell?.contentView.addSubview(label)

			}
			
			if CloudKitDomain.shared.fetchStatus == .noResults  {
				
				cell!.frame = CGRect(x: 0,y: 0,width: 320,height: 80)
				let contentRect = cell!.contentView.bounds
				
				let label = UILabel()
				label.text = WLoc("LawCloudKitFetchStateNoResults")
				label.numberOfLines = 0
				label.frame = CGRect( x: 10, y: 10, width: contentRect.size.width - 20, height: contentRect.size.height - 10 )
				label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
				cell!.contentView.addSubview(label)
			}
	
			return cell!
		}
		
		//
		// Main Rows
		//
		
		let record = filtererdRecords[indexPath.row]
		let creatorUUID = record[RecordType.LawDocPackageKey.creatorUUID] as? String
		let creator = record[RecordType.LawDocPackageKey.creator] as? String ?? ""
		let title = record[RecordType.LawDocPackageKey.title] as? String ?? ""
		
		if creatorUUID?.isEmpty == false {
			cell!.textLabel!.text = title + "👤" + creator
			
		}else {
			cell!.textLabel!.text = title
		}
		
		var editionString = record[RecordType.LawDocPackageKey.edition] as? String
		
		if editionString != nil {
			var kanjiEditionString = kanjiConversionCache.object(forKey: editionString! as NSString) as? String
			
			if kanjiEditionString == nil {
				kanjiEditionString = editionString!.transliterateToArabicNumber()
				kanjiConversionCache.setObject(kanjiEditionString! as NSString, forKey: editionString! as NSString)
			}
			
			editionString = kanjiEditionString
		}
		
		cell!.detailTextLabel?.text = editionString
		cell!.isUserInteractionEnabled = true
		cell!.backgroundColor = UIColor.white
		cell!.textLabel!.textColor = UIColor.black
		cell!.selectionStyle = .default
		cell!.detailTextLabel?.textColor = #colorLiteral(red: 0, green: 0.3176470588, blue: 0.9019607843, alpha: 0.7)
		// Set accessory view
		
		let progress = CloudKitDomain.shared.downloadingProgress(record)

		if progress != nil {
			
			let iconView = CircularProgressIndicator(frame: CGRect(x: 0,y: 0, width: 30, height: 30))
			cell?.accessoryView = iconView
			
			if progress != nil && progress! > 0.1 {
				iconView.progress = progress!
				
			}else {
				iconView.indeterminate = true
			}

		}else {
			
			// Download icon
			// Already downloaded
			
			if (cell!.accessoryView is UIImageView) != true {
				
				if lawDownloadImage == nil {
          lawDownloadImage = UIImage(named: "LawDownload")!.colorizedImage(withTint: self.view.tintColor, alpha: 1.0, glow: false)
				}
				
				cell!.accessoryView = UIImageView(image: lawDownloadImage)
			}

			let myMd5 = record[RecordType.LawDocPackageKey.md5] as? String ?? ""
			if md5s.contains(myMd5) {
				
				cell!.textLabel!.textColor = UIColor.gray
				cell!.detailTextLabel?.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
				cell!.selectionStyle = .none
				cell!.isUserInteractionEnabled = false
				cell!.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)
      
				if lawDownloadedImage == nil {
					lawDownloadedImage = UIImage(named: "DBChecked")!.colorizedImage(withTint: UIColor.lightGray, alpha: 0.5, glow: false)
				}
				
				if let imageView = cell?.accessoryView as? UIImageView {
					if imageView.image != lawDownloadedImage {
						imageView.image = lawDownloadedImage
					}
				}
				
			}else {
				
				//"Download"
				
				if lawDownloadImage == nil {
          lawDownloadImage = UIImage(named: "LawDownload")!.colorizedImage(withTint: self.view.tintColor, alpha: 1.0, glow: false)
				}
				
				if let imageView = cell?.accessoryView as? UIImageView {
					if imageView.image != lawDownloadImage {
						imageView.image = lawDownloadImage
					}
				}
			}
		}
		
		return cell!
	}
	
	func reload() {
		collectMD5s()
		tableView.reloadData()
  }
  
  func collectMD5s() {
    
    md5s = FilePackage.shared.documents.collectWithCondition {
      
      if $0.isLawEntity == true && $0.packageURL != nil {
        
        let ( plist, _, _) = FilePackage.shared.loadPackage(at: $0.packageURL!, onlyPlist: true )
        let md5 = plist?[.md5] as? String ?? ""
        return md5
        
      }else {
        return nil
      }
      } as! [String]
  }
  
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		if indexPath.section == 1 &&  ( indexPath.row == 1 || indexPath.row == 0  ) {
			
			if searchBar.text?.isEmpty == false {
				let controller = WebViewController( searchTerm: searchBar.text! )
				navigationController?.pushViewController(controller, animated: true)
			}
			
			tableView.deselectRow(at: indexPath, animated: true)

//			let str = "http://www.catalystwo.com/LawBrowser_ja.html"
//			
//			UIApplication.sharedApplication().openURL( NSURL(string:str)!)
//
//			self.navigationController?.popToRootViewControllerAnimated(true)
			return
		}
		
		if indexPath.section == 1 &&  indexPath.row == 2  {
			upload(nil)
			return
		}
		
		let record = filtererdRecords[indexPath.row]
	
		UIApplication.shared.isNetworkActivityIndicatorVisible = true
		indicatorIncrementor += 1
		
		// Download package
    CloudKitDomain.shared.downloadAsset(for: record, progress: { [weak self] (progress: Double) -> Void in
			
			if let cell = self?.tableView.cellForRow(at: indexPath) {
				
				if let indicator = cell.accessoryView as? CircularProgressIndicator {
					indicator.progress = progress
				}
			}
			
		}) { [weak self] (success: Bool, error: Error?) -> Void in
			
			print("** download finished ** \(Date().timeIntervalSinceReferenceDate)")
			
			defer {
				UIApplication.shared.isNetworkActivityIndicatorVisible = false
				if self != nil {
					displayErrorAlertIfNecessary(error, in: self!)
				}
			}
			
			guard self != nil else { return }
			
			self!.indicatorIncrementor =  self!.indicatorIncrementor - 1
			
			print(self!.indicatorIncrementor)
			
			self!.collectMD5s()
			
			// Reload only if dislaying
			if self!.tableView.cellForRow(at: indexPath) != nil {
				self!.tableView.reloadRows(at: [indexPath], with: .none)
			}
		}
		
		
		tableView.reloadRows(at: [indexPath], with: .automatic)
		tableView.deselectRow(at: indexPath, animated: true)
		
		let creator = record[RecordType.LawDocPackageKey.creatorUUID] as? String
		
		if creator?.isEmpty == false {
			let ud = UserDefaults.standard
			if true != ud.bool(forKey: "CloudKitTableViewControllerUsersAlert") {
				
				ud.set(true, forKey: "CloudKitTableViewControllerUsersAlert")
				ShowAlertMessage( "", WLoc("This is users item"), self)
			}
		}
	}
	
	//MARK:- Search
	func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
	
    //CloudKitDomain.shared.fetchStatus = .start
		DispatchQueue.main.async(execute: { self.tableView.reloadData() })
	}
	
	
	func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
		searchBarSearchButtonClicked( searchBar )
	}
	
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		
		if searchBar.text == nil || searchBar.text!.isEmpty == true {
			CloudKitDomain.shared.clearFetch()
			filtererdRecords_ = nil

			tableView.reloadData()
			return
		}
		
		searchBar.resignFirstResponder()
		
		// Check online status
		if GCNetworkReachability.forInternetConnection().isReachable() == false {

			displayErrorAlertIfNecessary( LawError.offline , in: self)
			return
		}
		
		indicatorIncrementor += 1
		UIApplication.shared.isNetworkActivityIndicatorVisible = true

		var tokenSearch = true
		
		if searchBar.selectedScopeButtonIndex == 0 { tokenSearch = false }
		
    CloudKitDomain.shared.fetch( searchBar.text!, tokenSearch: tokenSearch) { [weak self] (error: Error?)->Void in
      
      self?.filtererdRecords_ = nil
      self?.tableView.reloadData()
      
      UIApplication.shared.isNetworkActivityIndicatorVisible = false
      
      if self != nil { displayErrorAlertIfNecessary(error, in: self!) }
    }
    
    tableView.reloadData()
  }
}

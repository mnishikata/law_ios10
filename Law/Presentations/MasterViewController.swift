//
//  MasterViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2014/08/25.
//  Copyright (c) 2014年 Catalystwo. All rights reserved.
//

import UIKit
import Firebase
import InAppSettingsKit

// MARK:- Body

class MasterViewController: UIViewController, UIGestureRecognizerDelegate, MasterViewDelegate {
  var viewModel: MasterViewModel!
	var tableView: MNOutlineView? = nil
  //var dirty = false

	var appSettingsViewController_: IASKAppSettingsViewController? = nil
	
	var viewAppeared = false
	var viewDidAppearedHandler: (()->Void)? = nil
	
	weak var detailNavigationController: StackNavigationController? = nil
	var documentInteractionController_: UIDocumentInteractionController? = nil

	var conflictSolver_: ConflictSolver? = nil
	
	var tutorialController: TutorialPopoverViewController? = nil

	var bannerView_: GADBannerView? = nil
	var bannerLoadingFailedCount: Int = 0
	
  weak var myToolbar: UIToolbar? = nil

  
	// MARK:-
  func tableDidUpdate() {
    tableView?.reloadData()
  }
  
	required init() {
		super.init(nibName: nil, bundle: nil)
		
    viewModel = MasterViewModel(delegate: self)
    
		
		//Configure AdColony once on app launch
		if viewModel.adTypeIsVideo {
//		AdColony.configureWithAppID("app91bd0896eafa445796", zoneIDs: ["vz3878af05983e4d909d"], delegate: nil, logging: true)
		}
		
		//Clear temporary file from previous usage
		// Clean up
		
		let temp = (NSTemporaryDirectory() as NSString).appendingPathComponent("Attachment.comcatalystwolawsindex")
		
		if FileManager.default.fileExists(atPath: temp) {
			do {
				try FileManager.default.removeItem(atPath: temp)
				
			}catch {
			}
		}
		edgesForExtendedLayout = UIRectEdge()
		automaticallyAdjustsScrollViewInsets = false
	}
	
	required init(coder: NSCoder) {
		super.init(nibName: nil, bundle: nil)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    var frame = self.view.bounds

    let outlineView = MNOutlineView(frame: viewModel.tableViewFrame(withViewBounds: frame))
    outlineView.delegate = self
    outlineView.dataSource = self
    MNOutlineView.setFlatTintColor(viewModel.backgroundColorScheme.tintColor)
    outlineView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    outlineView.showGripperOnlyWhenEditing = true
    outlineView.canCollapse = false
    outlineView.tableView.contentInset = UIEdgeInsetsMake(0, 0, 20, 0)
    
    tableView = outlineView
    
    self.view.addSubview(tableView!)
    
    // Law XML
    let lawXmlBannerView = UIView(frame: CGRect(x:0, y:outlineView.frame.maxY, width:outlineView.frame.size.width, height:28))
      lawXmlBannerView.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]

     lawXmlBannerView.backgroundColor = #colorLiteral(red: 0.02352941176, green: 0.1803921569, blue: 0.2509803922, alpha: 1)
    let button = UIButton(type: .custom)
    button.frame = CGRect(x:(outlineView.frame.size.width-204)/2, y:0, width:204, height:28)
    button.setImage(UIImage(named:"eLawsBanner"), for: .normal)
    button.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleTopMargin]
    button.addTarget(self, action: #selector(lawXML), for: .touchUpInside)
    lawXmlBannerView.addSubview(button)
    self.view.addSubview(lawXmlBannerView)
    
    // Setup UI
    navigationItem.leftBarButtonItem = editButtonItem
		let item = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addFromCloudKit))
		navigationItem.rightBarButtonItems = [item]
		title = WLoc("Laws")
	
		// Gesture
    let gesture = UISwipeGestureRecognizer(target: self, action: #selector(backToDetailViewControlellerOnPhone))
    gesture.direction = .left
    gesture.delegate = self
    self.view.addGestureRecognizer(gesture)
    
    updateTintColor()

    
    // TOOLBAR
    let backgroundColorScheme = viewModel.backgroundColorScheme

    let toolbar = UIToolbar(frame: viewModel.toolbarFrame(withViewBounds: self.view.bounds))
    toolbar.autoresizingMask = [.flexibleTopMargin, .flexibleWidth]
    toolbar.isTranslucent = false
    toolbar.backgroundColor = backgroundColorScheme.tableBackgroundColor
    toolbar.barTintColor = backgroundColorScheme.tableBackgroundColor
    self.view.addSubview( toolbar )
    myToolbar = toolbar
    
    updateToolbar()
    
    let backItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
    navigationItem.backBarButtonItem = backItem
		
		// Google Ad
		if viewModel.bannerHeight != 0 {
			loadBanner()
		}
	}
	
  func lawXML() {
    let str = "https://itunes.apple.com/app/id1272669652"
    let url = URL(string: str)!
    UIApplication.shared.open(url, options: [:], completionHandler: nil)
  }
  
  func loadBanner() {
    let banner = GADBannerView(adSize: kGADAdSizeLargeBanner)
    banner.adUnitID = "ca-app-pub-5527180066718750/8187333025"
    banner.rootViewController = self
    
    let request = GADRequest()
    request.testDevices = [kGADSimulatorID]
    banner.delegate = self
    banner.load(request)
    banner.frame = viewModel.bannerViewFrame(withViewBounds: self.view.bounds)
    banner.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
    
    bannerView_ = banner
  }
  
	func updateToolbar() {
    let backgroundColorScheme = viewModel.backgroundColorScheme

		let separator = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
		let settings = UIBarButtonItem(image: UIImage(named: "Settings"), style: .plain, target: self, action: #selector(settings(_:)))
		let search = UIBarButtonItem(image: UIImage(named: "MetaFind"), style: .plain, target: self, action: #selector(self.search))
		let viewAd = UIBarButtonItem(title: WLoc("See Ad"), style: .plain, target: self, action: #selector(viewAd(_:)))

		if viewModel.adTypeIsVideo {
			setToolbarItems( [settings, separator, viewAd, separator, search], animated: false)

		}else {
			setToolbarItems( [settings, separator, search], animated: false)
		}
    
    myToolbar?.backgroundColor = backgroundColorScheme.tableBackgroundColor
    myToolbar?.barTintColor = backgroundColorScheme.tableBackgroundColor
	}
  
  override func setToolbarItems(_ toolbarItems: [UIBarButtonItem]?, animated: Bool) {
    myToolbar?.setItems(toolbarItems, animated: animated)
  }
  
	func updateTintColor() {
		
		let backgroundColorScheme = viewModel.backgroundColorScheme
    
    tableView?.backgroundColor = backgroundColorScheme.tableBackgroundColor
    tableView?.setSelectionBackgroundColor(backgroundColorScheme.tableSelectionColor)
    tableView?.tableView.indicatorStyle = backgroundColorScheme.inverted ? .white : .black
      
    navigationController?.navigationBar.barTintColor = backgroundColorScheme.tableBackgroundColor
		navigationController?.toolbar.barTintColor = backgroundColorScheme.tableBackgroundColor
		
		self.view.backgroundColor = backgroundColorScheme.backgroundColor
		navigationController?.navigationBar.barStyle = backgroundColorScheme.barStyle
    navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: backgroundColorScheme.textColor, NSFontAttributeName: UIFont.preferredFont(forTextStyle: .headline)]
		navigationController?.toolbar.barStyle = backgroundColorScheme.barStyle
		
		self.view.window?.tintColor = backgroundColorScheme.tintColor
    
    myToolbar?.backgroundColor = backgroundColorScheme.tableBackgroundColor
    myToolbar?.barTintColor = backgroundColorScheme.tableBackgroundColor
    
    MNOutlineView.setFlatTintColor(backgroundColorScheme.tintColor)
    tableView?.reloadData()
	}
	
  override var edgesForExtendedLayout: UIRectEdge {
    get { return [] }
    set {}
  }
  
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		navigationController?.setToolbarHidden(true, animated: false)
		
		let center = NotificationCenter.default
		center.addObserver(self, selector: #selector(updateTintColor), name: LawNotification.settingsDidChange, object: nil)
		center.addObserver(self, selector: #selector(didEnterBackground), name: .UIApplicationDidEnterBackground, object: nil)

		updateTintColor()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		let center = NotificationCenter.default
		center.addObserver(self, selector: #selector(save), name: .UIApplicationWillResignActive, object: nil)
	
		viewAppeared = true
		
		viewDidAppearedHandler?()
		viewDidAppearedHandler = nil
	
		// Show quick tutorial popup
		if UserDefaults.standard.bool(forKey: "TutorialPopoverViewController") != true {
			//Execute later
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)  {
				
				if self.presentedViewController == nil {
					let controller = TutorialPopoverViewController( nibName: "TutorialPopoverViewController", bundle: nil )
					controller.modalPresentationStyle = .popover
					
					let popover = controller.popoverPresentationController
					popover?.delegate = controller
					popover?.sourceView = self.view
					popover?.permittedArrowDirections = [.up]
					popover?.sourceRect = CGRect(x: self.view.bounds.size.width - 25, y: 64, width: 1, height: 1)
					popover?.passthroughViews = [self.navigationController!.navigationBar]
					self.present(controller, animated: true, completion: {
						
						UserDefaults.standard.set(true, forKey: "TutorialPopoverViewController")
						
					})
					
					self.tutorialController = controller
				}
			}
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		viewAppeared = false
    viewModel.saveIfNecessary()
		
		let center = NotificationCenter.default
		center.removeObserver(self, name: .UIApplicationWillResignActive, object: nil)
		center.removeObserver(self, name: LawNotification.settingsDidChange, object: nil)
		center.removeObserver(self, name: .UIApplicationDidEnterBackground, object: nil)
		
		tutorialController?.dismiss(animated: true, completion: nil)
		tutorialController = nil
	}
  
  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    viewModel.traitCollection = newCollection
    super.willTransition(to: newCollection, with: coordinator)
  }
  
  func traitCollectionDidChange() {
    if isViewLoaded == false { return }
    
    myToolbar?.frame = viewModel.toolbarFrame(withViewBounds: self.view.bounds)
    tableView?.frame = viewModel.tableViewFrame(withViewBounds: self.view.bounds)
  }
	
	func didEnterBackground() {
		if presentedViewController != nil {
			dismiss(animated: false, completion: nil)
		}
	}
	
	override func setEditing(_ editing: Bool, animated: Bool) {
		
		super.setEditing(editing, animated: animated)
		tableView?.setEditing(editing, animated: animated)
		
		if editing == true {
			let item = UIBarButtonItem(title: WLoc("New Folder"), style: .plain, target: self, action: #selector(addFolder))
			navigationItem.rightBarButtonItems = [item]
			
		}else
		{
			let item = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(addFromCloudKit))
			navigationItem.rightBarButtonItems = [item]
		}
	}

	func gestureRecognizer( _ gestureRecognizer: UIGestureRecognizer,
		shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return true
	}
	
	
  func openWithDetailItem( url: URL, anchor: String? = nil, elementId: String? = nil, startJump: Bool, search: String? = nil ) {
		
		// 1. Detail View Controller is displaying the same package
		//
		// 2. Create new detail view controller
		
		func showDetailNavigation() {
			//		// Show detail nav if not shown yet
			//
			if splitViewController?.viewControllers.count == 1 {
				if let viewController = splitViewController?.viewControllers[0] as? UINavigationController {
					if viewController.viewControllers.index( of: detailNavigationController! ) == nil {
						splitViewController?.showDetailViewController(detailNavigationController!, sender: self)
					}
				}
			}
			
			// Hide master on iPad
			if (splitViewController?.displayMode == .primaryOverlay){
				splitViewController?.preferredDisplayMode = .primaryHidden
				splitViewController?.preferredDisplayMode = .automatic
			}
		}
		
    let (dictionary,_,_) = FilePackage.shared.loadPackage(at: url, onlyPlist: true)
    if dictionary == nil { return }
    let item = FileEntry( packageURL: url, anchor: anchor, elementId: elementId, plist: dictionary! )
    // 1.
    
    
    if let dvc = detailNavigationController?.viewControllers[0] as? DetailViewController {
      
      if dvc.viewModel.packageURL == url {
        
        dvc.viewModel.startJumpOnViewDidAppear = .onViewDidAppear
        
        showDetailNavigation()
        dvc.viewModel.startJump = startJump
        dvc.viewModel.startSearch = search
        dvc.viewModel.anchor = item.anchor
        
        return
      }
    }
    
    
    // 2.
    
    // Prepare view controller
    let controller = DetailViewController(restorationInfo: nil)
    controller.viewModel.loadFrom(url: url)
    controller.viewModel.traitCollection = self.traitCollection
    controller.viewModel.startJump = startJump
    controller.viewModel.startSearch = search
    controller.viewModel.anchor = item.anchor
    
    detailNavigationController?.setViewControllers( [controller], animated: false )
    showDetailNavigation()
  }
  
  
  
  // MARK:-
	
	func addFromCloudKit() {
		let controller = SearchLawTableViewController()
		navigationController?.pushViewController(controller, animated: true)
	}
	
	func addFolder() {
		
		let alertController = UIAlertController(title: WLoc("New Folder"), message: WLoc("New Folder Message"), preferredStyle: .alert)
		let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in }
		let action1 = UIAlertAction(title: WLoc("Create Folder"), style: .default) { (action) -> Void in
			
			let fields = alertController.textFields
			if let field = fields?.first as  UITextField? {
				
				let string = field.text ?? ""
        self.viewModel.addFolder(name: string)
				self.tableView?.reloadData()
				self.tableView?.move(toLast: true);
			}
		}
		
		alertController.addAction(action1)
		alertController.addAction(cancel)
		alertController.addTextField { (field: UITextField) -> Void in
			field.keyboardType = .default
		}
		
		present(alertController, animated: true, completion: nil)
	}
	
	func export() {
		
    guard let data = viewModel.serializedData else { return }
		
		let temp = (NSTemporaryDirectory() as NSString).appendingPathComponent("Attachment.comcatalystwolawsindex")
		let url = URL(fileURLWithPath: temp)
		
		do {
			try data.write(to: url, options: [])
    } catch {
      return
    }
    
    if let sender = myToolbar?.items?.first {
      documentInteractionController_ = UIDocumentInteractionController(url: url)
      documentInteractionController_!.presentOptionsMenu(from: sender, animated: true)
    }
  }

	
	func save() {
    viewModel.saveIfNecessary()
	}
	
	func conflictHandler() -> ICloudConflictNotificationHandler {
		
		return { [unowned self] ( allVersions: [NSFileVersion], decisionHandler: @escaping ICloudConflictDecisionHandler) in
			
			if self.conflictSolver_ == nil {
				let solver = ConflictSolver()
				self.conflictSolver_ = solver
				
				let nav = UINavigationController(rootViewController: self.conflictSolver_!)
				nav.modalPresentationStyle = .formSheet
				
				self.showViewController(nav)
			}
			
			weak var theSolver = self.conflictSolver_

			theSolver?.versions = allVersions
			theSolver?.completionHander = { [unowned self] (fileVersion: NSFileVersion?) -> Void in
				
				if fileVersion != nil {
					decisionHandler(fileVersion!)
				}
				
				self.conflictSolver_ = nil
			}
		} 
	}
	
	func showViewController(_ controller: UIViewController) {
		
		if splitViewController != nil {
			splitViewController!.present(controller, animated: true, completion: {})
		}else {
			viewDidAppearedHandler = { [unowned self] in
				self.splitViewController?.present(controller, animated: true, completion: {})
			}
		}
	}
	
	func reload() {
		// Load docs
		let _ = FilePackage.shared.loadIndex( conflictHandler(), updateHandler: { self.tableView?.reloadData()  } )
	}
	
	func appSettingsViewController() -> IASKAppSettingsViewController {
		
		if appSettingsViewController_ == nil {
			appSettingsViewController_ = IASKAppSettingsViewController()
			appSettingsViewController_!.delegate = self
			
			// Sync UD
			
			let keysToSync = ["DefaultDefine", "CustomDefineMessage", "CustomDefine1", "CustomDefine2", "CustomDefine1Title", "CustomDefine2Title", "CustomDefine1URL", "CustomDefine2URL"]
			
			let sharedUD = NSUbiquitousKeyValueStore.default()
			let ud = UserDefaults.standard
			for key in keysToSync {
				
				if let obj = sharedUD.object(forKey: key) {
					ud.set(obj, forKey: key)
				}
			}
			
			ud.synchronize()
		}
		return appSettingsViewController_!
	}
	
	func backToDetailViewControlellerOnPhone() {
		
		if TabDataSource.shared.current == nil { return }
		
		// Detail Nav
		
		if splitViewController?.displayMode == .allVisible && splitViewController?.viewControllers.count == 1 {
			if let nav = splitViewController?.viewControllers[0] as? UINavigationController {
				if nav.viewControllers.index( of: detailNavigationController! ) == nil {
					splitViewController?.showDetailViewController(detailNavigationController!, sender: self)
				}
			}
		}
	}
	
	func search() {
		
		if presentedViewController != nil {
			dismiss(animated: false, completion: nil)
		}
		
		let controller = KushizashiTableViewController(restorationInfo: nil)
    controller.backgroundColorScheme = viewModel.backgroundColorScheme
		controller.didSelectHandler = { ( text: String, dictionary: DocItem) -> Void  in
			let startJump = dictionary.highlight == true
      self.openWithDetailItem( url: dictionary.packageURL!, startJump: startJump, search: text )
		}
		
		navigationController?.pushViewController(controller, animated: true)
	}
	
	func settings(_ sender: UIBarButtonItem) {
		
		let controller = GearViewController()
		controller.masterViewController = self
		
    let nav = UINavigationController(rootViewController: controller)
		nav.modalPresentationStyle = .formSheet
		present(nav, animated: true, completion: nil)
	}

	func setPlaceholderViewController() {
    let dvc = PlaceholderViewController(backgroundColorScheme: viewModel.backgroundColorScheme)
		detailNavigationController?.setViewControllers([dvc], animated: true)
	}
	
	func viewAd( _ sender: UIBarButtonItem ) {
//		AdColony.playVideoAdForZone("vz3878af05983e4d909d", withDelegate: nil)

	}

}

// MARK:- Outline
extension MasterViewController: MNOutlineViewDelegate, MNOutlineViewDataSource {
  
  func outlineView(_ outlineView: MNOutlineView!, child index: Int, ofItem item: Any!) -> Any! {
    return viewModel.outlineView(child: index, ofItem: item as! DocItem!)
  }
  
  func outlineView(_ outlineView: MNOutlineView!, isItemExpandable item: Any!) -> Bool {
    return viewModel.outlineView(isItemExpandable: item as! DocItem!)
  }
  
  func outlineView(_ outlineView: MNOutlineView!, isItemExpanded item: Any!) -> Bool {
    return viewModel.outlineView(isItemExpanded: item as! DocItem!)
  }
  
  func outlineView(_ outlineView: MNOutlineView!, shouldExpandItem item: Any!) -> Bool { return true }
  func outlineView(_ outlineView: MNOutlineView!, shouldCollapseItem item: Any!) -> Bool { return true }
  func outlineView(_ outlineView: MNOutlineView!, didExpandItem item: Any!)  {
    viewModel.outlineView(didExpandItem: item as! DocItem!)
  }
  
  func outlineView(_ outlineView: MNOutlineView!, didCollapseItem item: Any!)  {
    viewModel.outlineView(didCollapseItem: item as! DocItem!)
  }
  
  func outlineView(_ outlineView: MNOutlineView!, numberOfChildrenOfItem item: Any!) -> Int {
    return viewModel.outlineView(numberOfChildrenOfItem: item as! DocItem!)
  }
  
  func outlineView(_ outlineView: MNOutlineView!, textForItem item: Any!) -> Any! {
    return viewModel.outlineView(textForItem: item as! DocItem!)

  }
  
  // Delegate
  
  func outlineView(_ outlineView: MNOutlineView!, viewForItem item: Any!, forRowAt indexPath: IndexPath!) -> UIView! {
    let baseView = UIView.cell(with: viewModel, item: item as! DocItem)
    return baseView
  }
  
  func outlineView(_ outlineView: MNOutlineView!, moveItem item: Any!, toItem parentObj: Any!, at index: Int) {
    
    let originalItem = item as? DocItem
    guard originalItem != nil else {
      tableView?.reloadData()
      return
    }
    
    viewModel.moveItem(item as! DocItem, toItem: parentObj, at: index)
  }
  
  func outlineView(_ outlineView: MNOutlineView!, setItem item: Any!, expanded flag: Bool) {
    viewModel.set(expanded: flag, forItem: item as! DocItem)
  }
  
  func outlineView(_ outlineView: MNOutlineView!, heightForRowAt index: UInt, ofItem parentItem: Any!) -> CGFloat {
    return viewModel.cellHeight
  }
  
  func outlineView(_ outlineView: MNOutlineView!, titleForDeleteConfirmationButtonForItem item: Any!, at indexPath: IndexPath!) -> String! {
    return WLoc("Delete")
  }
  
  func outlineView(_ outlineView: MNOutlineView!, didSelectItem item: Any!, in cell: MNOutlineViewCell!, at indexPath: IndexPath!) {
    
    let dict = item as! DocItem
    
    // Folder
    if dict.folder == true && outlineView.editing == false {
      if dict.items != nil && dict.items!.count > 0 {
        if dict.expanded {
          outlineView.outlineViewCellCollapse(cell);
        } else{
          outlineView.outlineViewCellExpand(cell);
        }
      }
      return
    }
    
    // Editing
    if outlineView.editing == true {
      
      // Notes
      
      if dict.notes != nil {
        
        //				let dictionary = NSDictionary(object: dict.title, forKey: "title")
        //
        //				let object:FileEntry = FileEntry( packageURL: dict.packageURL!, anchor: dict.anchor, plist:dictionary )
        //				let anchor = dict.anchor
        //				let bookmarkingURL:(title:String, encodedURL:String) = ( title: "dummy", encodedURL: anchor!)
        //
        //
        //				let controller = NotesViewController(nibName:"NotesViewController" , bundle:nil)
        //				controller.bookmarkDelegate = self
        //				controller.fileEntry = object
        //				controller.bookmark = bookmarkingURL
        //				controller.title = dict["title"] as String?
        //
        //
        //				let nav = UINavigationController(rootViewController: controller)
        //				controller.modalPresentationStyle = .FormSheet
        //				self.presentViewController( nav, animated:true, completion:nil)
        
        
        return
      }
      
      if dict.highlight == true { return }
      
      // Others
      let alertController = UIAlertController(title: WLoc("Rename"), message: "", preferredStyle: .alert)
      let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in
      }
      
      let action1 = UIAlertAction(title: WLoc("Rename"), style: .default) { ( action) -> Void in
        
        let fields = alertController.textFields
        if let field = fields?.first as  UITextField? {
          
          let string = field.text
          if string!.isEmpty == true  { return }
          
          dict.title = string!
          FilePackage.shared.saveIndex( saveToICloud: true )
          self.tableView?.reloadData()
        }
      }
      
      alertController.addAction(action1)
      alertController.addAction(cancel)
      alertController.addTextField { (field: UITextField) -> Void in
        
        field.text = dict.originalTitle as String
        field.clearButtonMode = .whileEditing
        field.keyboardType = .default
      }
      
      present(alertController, animated: true, completion: nil)
      
      return
    }
    
    // Download from CloudKit
    let status = viewModel.fileExists(for: dict)
    
    
    
    //		//DEBUG
    //
    //		let wrapper = WatchConnectivi tyManager.sharedConnectivityManager.directoryFileWrapperToSendToWatchAt(dict.packageURL!)!
    //		print("--- wrapper \(wrapper.serializedRepresentation!.length)")
    //		print("--- deflate \(wrapper.serializedRepresentation!.deflate()!.length)")
    //
    //		let bzipdata = try! BZipCompression.compressedDataWithData(wrapper.serializedRepresentation!, blockSize: BZipDefaultBlockSize, workFactor: BZipDefaultWorkFactor)
    //
    //		print("--- bzipdata \(bzipdata.length)")
    
    if status == .no {
      
      guard dict.packageName != nil else {
        // Unknown
        tableView?.reloadData()
        return
      }
      
      // Start Download when Online
      if GCNetworkReachability.forInternetConnection().isReachable() == false {
        displayErrorAlertIfNecessary( LawError.offline, in: self )
        return
      }
    
      // Start download
      
      UIApplication.shared.isNetworkActivityIndicatorVisible = true

      viewModel.download(forItem: dict, progress: { progress in
        
        if let indicator = cell.viewWithTag(99) {
          (indicator as! CircularProgressIndicator).progress = progress
        }
        
      }, completion: { (success: Bool, error: Error?) -> Void in
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        self.tableView?.reloadData()
        
        // Failed in CloudKit... Download from eGov
        if success == false {
          
          let sourceURLString = dict.sourceURLString
          if sourceURLString?.hasPrefix("http://law.e-gov.go.jp/cgi-bin/") == true {
            
            let url = URL(string: sourceURLString! as String)
            //let fileEntry = FileEntry( packageURL: url!, anchor: nil, elementId: nil, plist: plist );
            self.openWithDetailItem( url: url!, startJump: false )
            
          }else {
            
            // Show Error
            DispatchQueue.main.async {
              displayErrorAlertIfNecessary(error, in: self)
            }
          }
        } else {
          
          //					let dictionary = NSDictionary(object: dict.originalTitle, forKey: "title")
          //
          //					let object:FileEntry = FileEntry( packageURL: dict.packageURL!, anchor: dict.anchor, elementId:nil, plist:dictionary )
          //
          //					self.openWithDetailItem( object, startJump: true, search: nil )
          
        }
        
      })
      
      tableView?.reloadData()
      
      return
    }
    
    // Open
    
    var myId: String? = nil
    var anchor = dict.anchor
    
    if dict.highlight == true {
      myId = dict.uuid
      anchor = dict.nearestAnchor
    }
    
    openWithDetailItem( url: dict.packageURL!, anchor: anchor, elementId: myId, startJump: true)
  }

  func outlineView(_ outlineView: MNOutlineView!, commitDeletionItem itemToDelete: Any!, for indexPath: IndexPath!) -> Int {
    
    viewModel.deleteItem(itemToDelete as! DocItem)
    outlineView.reloadData()

    return 0
  }
}

//MARK:- IASKSettingsDelegate
extension MasterViewController: IASKSettingsDelegate {
  func settingsViewControllerDidEnd( _ sender: IASKAppSettingsViewController) {
    
    // Upload to NSUbiquitousKeyValueStore
    
    /*
     Synchronize NSUbiquitousKeyValueStore
     DefaultDefine
     CustomDefineMessage
     CustomDefine1
     CustomDefine2
     CustomDefine1Title
     CustomDefine2Title
     CustomDefine1URL
     CustomDefine2URL
     */
    
    let keysToSync = ["DefaultDefine", "CustomDefineMessage", "CustomDefine1", "CustomDefine2", "CustomDefine1Title", "CustomDefine2Title", "CustomDefine1URL", "CustomDefine2URL"]
    
    let sharedUD = NSUbiquitousKeyValueStore.default()
    let ud = UserDefaults.standard
    for key in keysToSync {
      
      if let obj = ud.object(forKey: key) {
        sharedUD.set(obj, forKey: key)
      }
    }
    
    sharedUD.synchronize()
    
    // CHECK APP ICON
    if #available(iOS 10.3, *) {
      if UIApplication.shared.supportsAlternateIcons {
        let appIconName = self.viewModel.appIconName
        
        if UIApplication.shared.alternateIconName != appIconName {
          UIApplication.shared.setAlternateIconName(appIconName) { err in
            self.viewModel.appIconName = UIApplication.shared.alternateIconName
            print("** error \(String(describing: err?.localizedDescription))")
            
          }
        }
      }
    }
    
    UIApplication.shared.isIdleTimerDisabled = ud.bool(forKey: "AvoidScreenLock")
    dismiss( animated: true ) {
      let center = NotificationCenter.default
      center.post(name: LawNotification.settingsDidChange, object: self)
      
      if UserDefaults.standard.object(forKey: "AdType") as? String != "Banner" {
        self.bannerView_?.removeFromSuperview()
        self.bannerView_?.delegate = nil
        self.bannerView_ = nil
        
        self.tableView?.frame = self.viewModel.tableViewFrame(withViewBounds: self.view.bounds)
      }else {
        
        if self.bannerView_ == nil {
        self.tableView?.frame = self.viewModel.tableViewFrame(withViewBounds: self.view.bounds)
        self.loadBanner()
        }
      }
    }
  }
  
}

//MARK:- Popover
extension MasterViewController: UIPopoverPresentationControllerDelegate {
	
	func adaptivePresentationStyle(for controller: UIPresentationController)
		-> UIModalPresentationStyle {
			return .none
	}
	
	func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection)
		-> UIModalPresentationStyle {
			return .none
	}
	
	func popoverPresentationController( _ popoverPresentationController: UIPopoverPresentationController,
		willRepositionPopoverTo rect: UnsafeMutablePointer<CGRect>,
		in view: AutoreleasingUnsafeMutablePointer<UIView>) {
	}

}

//MARK:- Banner View Delegate
extension MasterViewController: GADBannerViewDelegate {
	
	/// Tells the delegate an ad request loaded an ad.
	func adViewDidReceiveAd(_ bannerView: GADBannerView) {
	debugPrint("adViewDidReceiveAd")
		
		if let button = bannerView.viewWithTag(99) {
			button.removeFromSuperview()
		}
		
		
		var frame = self.view.bounds
//		frame.origin.y += 20
//		frame.size.height -= 20
		frame.origin.y -= bannerView.frame.size.height
		
//		tableView!.frame = frame
		
		var bannerFrame = bannerView.frame
		bannerFrame.size.width = frame.size.width
		bannerFrame.origin.y = 0
		bannerView.frame = bannerFrame
		self.view.addSubview(bannerView )
	}
	
	/// Tells the delegate an ad request failed.
  func adView(_ bannerView: GADBannerView,
              didFailToReceiveAdWithError error: GADRequestError) {
			print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
			
			bannerLoadingFailedCount += 1
			
			let image = UIImage(named: "banner2.jpg")
			if let _ = bannerView.viewWithTag(99) {
			}else {
				let button = UIButton(type: .custom)
				button.setImage(image, for: UIControlState())
				button.addTarget(self, action: #selector(viewAmazon(_:)), for: .touchUpInside)
				button.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin]
				button.imageView?.contentMode = .scaleAspectFit
				button.sizeToFit()
				button.tag = 99
				button.frame = CGRect(x: 0,y: 0,width: self.view.bounds.size.width, height: 50)
				bannerView.addSubview( button )
			}
			self.view.addSubview( bannerView )
			
			// Online?
			
			//			let reachable = GCNetworkReachability.reachabilityForInternetConnection().isReachable()
			//			
//			
//			if bannerLoadingFailedCount > 2 && reachable == true  {
//				
//				var frame = self.view.bounds
////				frame.size.height -= bannerView.frame.size.height
////				tableView!.frame = frame
//				
//				var bannerFrame = bannerView.frame
//				bannerFrame.size.width = frame.size.width
//				bannerFrame.origin.y = 0
//				bannerView.frame = bannerFrame
//				bannerView.backgroundColor = UIColor.cyanColor()
//				self.view.addSubview( bannerView )
//				
//			}
//			
//			if reachable == false {
//				bannerLoadingFailedCount = 0
//				
//				
//			}
	}
	
	func viewAmazon(_ sender: Any?) {
		let url = URL(string: "http://www.amazon.co.jp")
    UIApplication.shared.open(url!, options: [:], completionHandler: nil)
	}
	
	/// Tells the delegate that a full screen view will be presented in response
	/// to the user clicking on an ad.
	func adViewWillPresentScreen(_ bannerView: GADBannerView) {
		print("adViewWillPresentScreen")
	}
	
	/// Tells the delegate that the full screen view will be dismissed.
	func adViewWillDismissScreen(_ bannerView: GADBannerView) {
		print("adViewWillDismissScreen")
	}
	
	/// Tells the delegate that the full screen view has been dismissed.
	func adViewDidDismissScreen(_ bannerView: GADBannerView) {
		print("adViewDidDismissScreen")
	}
	
	/// Tells the delegate that a user click will open another app (such as
	/// the App Store), backgrounding the current app.
	func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
		print("adViewWillLeaveApplication")
	}
}


extension UIView {

  class func cell(with viewModel: MasterViewCellModelProtocol, item: DocItem) -> UIView {
    let backgroundColorScheme = viewModel.backgroundColorScheme
    
    let dict = item
    let baseWidth: CGFloat = 320
    let baseView = UIView(frame: viewModel.baseFrame(baseWidth: baseWidth))
    baseView.autoresizingMask = .flexibleWidth
    baseView.backgroundColor = UIColor.clear
    
    let label = UILabel(frame: viewModel.labelFrame(baseWidth: baseWidth, forItem: dict))
    label.autoresizingMask = .flexibleWidth
    label.lineBreakMode = .byTruncatingMiddle
    label.minimumScaleFactor = 0.8
    label.adjustsFontSizeToFitWidth = true
    label.numberOfLines = 0
    if #available(iOS 9.0, *) {
      label.allowsDefaultTighteningForTruncation = true
    } else {
      // Fallback on earlier versions
    }
    baseView.addSubview( label )
    
    // Small Font for Bookmark
    
    if dict.highlight == true {
      let attributedText = NSAttributedString(string: dict.title as String, attributes: viewModel.highlightAttributes(for: dict))
      label.attributedText = attributedText
      
    }else {
      // Setup text
      label.font = viewModel.font(for: dict)
      label.text = dict.title
      label.textColor = backgroundColorScheme.textColor
    }
    
    // Download button
    let status = viewModel.fileExists(for: dict)
    let accessoryViewFrame = viewModel.accessoryViewFrame(baseWidth: baseWidth)
    
    if status == .downloading {
      
      let imageView = CircularProgressIndicator(frame: accessoryViewFrame )
      imageView.tag = 99
      imageView.autoresizingMask = .flexibleLeftMargin
      baseView.addSubview( imageView )
      
      // Loading animation
      imageView.indeterminate = true
    }
    
    if status == .no {
      
      let imageView = UIImageView(frame: accessoryViewFrame )
      
      imageView.autoresizingMask = .flexibleLeftMargin
      baseView.addSubview( imageView )
      
      // "Download from Cloud" Image
      imageView.image = viewModel.downloadImage
    }
    return baseView

  }
}

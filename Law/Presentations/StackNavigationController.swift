//
//  StackNavigationController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2/06/15.
//  Copyright (c) 2015 Catalystwo. All rights reserved.
//

import UIKit

class StackNavigationController: UINavigationController, TabDataSourceProtocol {
  
  //var restoreIdentifier_: String? = nil
  var hasLoadedContents: Bool = false
  
  var viewModel = StackNavigationViewModel()
  
  init(rootViewController useThisViewControllerWhenNotRestored: UIViewController, tab: Tab?) {
    super.init(rootViewController: useThisViewControllerWhenNotRestored)
    if tab != nil {
      TabDataSource.shared.delegate = self
      let _ = setRestoreIdentifier(tab!, jumpTo: nil)
    }
  }
  
  func tabWillSwitch() {
    saveState()
  }
  
  func load(tab: Tab, jumpTo article: String? = nil) {

    let hasLoaded = setRestoreIdentifier(tab, jumpTo: article)
    _ = self.view.window!.rootViewController as! UISplitViewController
    
    if hasLoaded == false {
      // Setup detail
      let dvc = PlaceholderViewController(backgroundColorScheme: viewModel.backgroundColorScheme)
      
      setViewControllers([dvc], animated: false)
      
      let svc = self.view.window!.rootViewController as! UISplitViewController
      
      if UIDevice.current.userInterfaceIdiom == .pad {
        
        let buttonItem = svc.displayModeButtonItem
        UIApplication.shared.sendAction(buttonItem.action!, to: buttonItem.target, from: nil, for: nil)
      }else {
        let _ = navigationController?.popToRootViewController(animated: true)
      }
    }
  }
  
  private func setRestoreIdentifier(_ identifier: Tab, jumpTo: String?) -> Bool {
    
    hasLoadedContents = false
    
    if viewModel.tab == identifier { return true }
    
    // save existing property
    if ( viewModel.tab != "" && viewModel.tab != identifier ) {
      saveState()
    }
    
    // Set new
    viewModel.tab = identifier
    
    // Check UD
    
    guard let plist = TabDataSourceStorage<StackNavigationStorage>()[identifier] else { return false }
    
    let viewControllers = StackNavigationViewModel.stackViewControllersFromPlist(plist)
    
    if viewControllers.count > 0 {
      
      if jumpTo != nil {
        for controller in viewControllers.reversed() {
          if let dvc = controller as? DetailViewController {
            
            if let packageUrl = dvc.viewModel.packageURL {
              let fp = FilePackage.shared
              let (anchor,element) = fp.anchor(for: jumpTo!, in: packageUrl, toc2: dvc.viewModel.toc2, toc1: dvc.viewModel.linkDictionaries)
              
              dvc.viewModel.startJump = true
              if dvc.viewModel.excerptType != .full {
                dvc.viewModel.excerptElement = element
                dvc.viewModel.contentOffsetY_ = nil
                //dvc.viewModel.startContentOffset = nil
                dvc.viewModel.loadFrom(url: packageUrl)
                dvc.viewModel.anchor = anchor
              }
              
              if dvc.viewModel.excerptType == .full {
                dvc.viewModel.loadFrom(url: packageUrl)
                dvc.viewModel.anchor = anchor
              }
            }
            break
          }
        }
      }
      
      setViewControllers(viewControllers, animated: false)
      hasLoadedContents = true
      
      return true
    }
    return false
  }
  
  required init(coder: NSCoder) {
    super.init(nibName: nil, bundle: nil)
    //self.navigationBar.isTranslucent = false
  }
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    //self.navigationBar.isTranslucent = false
    
  }
  /*
   override func overrideTraitCollection(forChildViewController childViewController: UIViewController) -> UITraitCollection? {
   let traitCollection = UITraitCollection(verticalSizeClass: .compact)
   return traitCollection
   }
   */

  
  
  func saveState() {
    guard let identifier = viewModel.tab else { return }
    let infoStack = viewModel.stackPlistFromViewControlelrs(viewControllers)
    
    TabDataSourceStorage<StackNavigationStorage>()[identifier] = infoStack
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    let center =  NotificationCenter.default
    center.addObserver(self, selector: #selector(saveState), name: .UIApplicationWillResignActive, object: nil)
    
    updateTintColor()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    updateTintColor()
  }
  
  override func setNavigationBarHidden(_ hidden: Bool, animated: Bool) {
    super.setNavigationBarHidden(false, animated: false)
  }
  
  func updateTintColor() {
    
    let color = viewModel.backgroundColorScheme.backgroundColor
    
    self.view.backgroundColor = color
    
    navigationController?.navigationBar.barTintColor = color
    navigationController?.toolbar.barTintColor = color
    navigationController?.navigationBar.barStyle = viewModel.backgroundColorScheme.barStyle
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    
    let center = NotificationCenter.default
    center.removeObserver(self, name: .UIApplicationWillResignActive, object: nil)
  }
}


//
//  SearchViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 13/05/15.
//  Copyright (c) 2015 Catalystwo. All rights reserved.
//

import UIKit



class SearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

	@IBOutlet weak var searchBar: UISearchBar!
	@IBOutlet weak var tableView: UITableView!

  var backgroundColorScheme: BackgroundColorSchemeType = BackgroundColorSchemeType.Paper

	var searchViewControllerShouldCancelBlock: (()->())? = nil
	var searchViewControllerDoSearchBlock: ((_ text:String?)->())? = nil
	var searchViewControllerExpandBlock: (()->())? = nil

	var dataSource_:NSMutableArray = {
		var array = NSMutableArray()
		
		let ud = UserDefaults.standard
		ud.synchronize()

		if let savedArray = ud.object(forKey: "SavedHistory") as? NSArray {
			array = savedArray.mutableCopy() as! NSMutableArray
		}
		
		return array
	}()
	var hideHeader_:Bool = false
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	init() {
		super.init(nibName: "SearchViewController", bundle: nil)
	}
	
	deinit {
		print("= Dealloc SearchViewController")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let ud = UserDefaults.standard
		ud.synchronize()
		
		tableView.backgroundColor = UIColor.clear
		
		var hideHusoku = true
		if let obj = ud.object(forKey: "HideHusoku") as? NSNumber  { hideHusoku = obj.boolValue }
		
		if hideHusoku == true && hideHeader_ == false {
			
		}else {
			tableView?.tableHeaderView = nil
		}
		
		searchBar.searchBarStyle = .minimal
		searchBar.placeholder = WLoc("Search")
    if backgroundColorScheme.inverted {
      searchBar.keyboardAppearance = .dark
      searchBar.barStyle = .blackTranslucent
    }
    
    if let popover = self.popoverPresentationController {
      popover.backgroundColor = backgroundColorScheme.popoverBackgroundColor
    }
    if backgroundColorScheme.inverted {
      self.tableView.indicatorStyle = .white
      self.tableView.separatorColor = UIColor(white: 1, alpha: 0.3)
    }
	}
	
	override func viewDidAppear(_ animated:Bool) {
		super.viewDidAppear( animated )
		searchBar.becomeFirstResponder()
	}
	
	override var preferredContentSize: CGSize {
		set {}
		get {
			return CGSize(width: 320,height: 480)
		}
	}
	
	@IBAction func husoku(_ sender: AnyObject) {
		
		searchViewControllerExpandBlock?()
		tableView.tableHeaderView = nil
	}
	
	func hideHeader() {
		tableView?.tableHeaderView = nil
		hideHeader_ = true
	}
	
	// MARK: - Search bar
	
	func searchBarCancelButtonClicked( _ searchBar: UISearchBar) {
		
		searchViewControllerShouldCancelBlock?()
		
	}
	
	func searchBarSearchButtonClicked( _ searchBar: UISearchBar) {
		
		if searchBar.text != nil && searchBar.text!.characters.count > 0 {
			
			dataSource_.remove( searchBar.text! )
			dataSource_.insert( searchBar.text!, at:0 )
			
			UserDefaults.standard.set( dataSource_, forKey:"SavedHistory")

			searchViewControllerDoSearchBlock?( searchBar.text )
		}
	}
	
	// MARK: - Table Delegate
	
	func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
		
		if searchBar.isFirstResponder {
			searchBar.resignFirstResponder()
		}
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let ud = UserDefaults.standard
		if indexPath.section == 0 {
			
			let text = dataSource_.object( at: indexPath.row ) as! String
			
			dataSource_.remove( text )
			dataSource_.insert( text, at:0 )
			ud.set( dataSource_, forKey:"SavedHistory")
			searchViewControllerDoSearchBlock?( text  )
		}
		
		if indexPath.section == 1 {
			
			let alertController = UIAlertController(title: WLoc("Delete History"), message: WLoc("Delete History Message"), preferredStyle: .alert)
			
			let action1 = UIAlertAction(title: WLoc("Delete"), style: .destructive) { (action) -> Void in
				ud.removeObject( forKey: "SavedHistory")
				self.dataSource_.removeAllObjects()
				self.tableView.reloadData()
			}
			
			let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in }
			
			alertController.addAction(action1)
			alertController.addAction(cancel)
			
			present(alertController, animated: true, completion: nil)
			tableView.deselectRow(at: indexPath, animated: true)
		}
	}
	
	// MARK: - TableView Data Source
	
	func tableView( _ tableView: UITableView,  canEditRowAt indexPath: IndexPath) -> Bool {
	
		if indexPath.section == 0 { return true }
	
		return false
	}
	
	func tableView( _ tableView: UITableView,
	                commit editingStyle: UITableViewCellEditingStyle,
	                                   forRowAt indexPath: IndexPath) {
		
		dataSource_.removeObject( at: indexPath.row )
		UserDefaults.standard.set( dataSource_, forKey:"SavedHistory")
		
		if dataSource_.count == 0 {
			tableView.reloadData()
		}else {
			tableView.deleteRows(at: [indexPath], with: .automatic)
		}
	}
	
	
	func numberOfSections( in tableView: UITableView) -> Int {
		if dataSource_.count > 0 { return 2 }
		return 0
	}
	
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		if dataSource_.count == 0 || section != 0 { return nil }
		return WLoc("History")
	}
  
  func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
    let header = view as! UITableViewHeaderFooterView
    header.backgroundView?.backgroundColor = backgroundColorScheme.tableBackgroundColor
    header.textLabel?.textColor = backgroundColorScheme.textColor
  }
  
	func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if section == 0 {
			return dataSource_.count
		}
		return 1
	}
	
	func tableView( _ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
			
		if indexPath.section == 0 {
      let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")  ??
        UITableViewCell(style: .default, reuseIdentifier: "Cell")
      
      cell.backgroundColor = UIColor.clear
      cell.textLabel!.text = dataSource_[ indexPath.row ] as? String
      cell.textLabel!.textColor = backgroundColorScheme.textColor
      
      if backgroundColorScheme.inverted {
        let view = UIView()
        view.backgroundColor = backgroundColorScheme.tableSelectionColor
        cell.selectedBackgroundView = view
      }
      
      return cell
      
		} else  {
			let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
			cell.textLabel!.textAlignment = .center
			cell.textLabel!.text = WLoc("Clear History")
			cell.textLabel?.textColor = self.view.tintColor
			cell.backgroundColor = UIColor.clear
      
      if backgroundColorScheme.inverted {
        let view = UIView()
        view.backgroundColor = backgroundColorScheme.tableSelectionColor
        cell.selectedBackgroundView = view
      }
			return cell
		}
	}
}

//
//  LawNavigationTitleView.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 23/09/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import UIKit

enum LawNavigationTitleBadgeType {
  case reload, upToDate, hasUpdate, none
  
  func imageWithTint(_ tint:UIColor) -> UIImage? {
    switch self {
    case .reload: return UIImage(named: "NavTitleReload")?.colorizedImage(withTint: tint, alpha: 1.0, glow: false)
    case .upToDate: return UIImage(named: "NavTitleUpToDate")?.colorizedImage(withTint: tint, alpha: 1.0, glow: false)
    case .hasUpdate: return UIImage(named: "NavTitleBadge")
    case .none: return nil
    }
  }
  
  func imageLocationTo(_ rect: CGRect) -> CGRect? {
    switch self {
    case .reload: return CGRect(x: rect.maxX + 5, y: rect.midY - 7.5, width:15, height:15)
    case .upToDate: return CGRect(x: rect.maxX + 5, y: rect.midY - 7.5, width:15, height:15)
    case .hasUpdate: return CGRect(x: rect.maxX - 5.0, y: 0, width:20, height:20)
    case .none: return nil
    }
  }
}

class LawNavigationTitleView: UIView {
  
  var title: String = "" {
    didSet {
      sizeToFit()
      setNeedsDisplay()
    }
  }
  
  var badgeType: LawNavigationTitleBadgeType = .none {
    didSet {
      setNeedsDisplay()
    }
  }
  
  weak var imageView: UIImageView!

  weak var target: AnyObject? = nil
  var action: Selector? = nil
  var colorScheme: BackgroundColorSchemeType! {
    didSet {
      setNeedsDisplay()
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.isOpaque = false
    self.backgroundColor = UIColor.clear
    self.contentMode = .redraw
    tintAdjustmentMode = .dimmed
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    _ = target?.perform(action, with: self.superview)
  }
  
  var maximumWidth: CGFloat {
    let frame = self.bounds
    return frame.size.width - 20
  }
  
  func fontAndTitleSize(_ size: CGSize) -> (UIFont, CGSize, Bool) {
    let baseFont = UIFont.preferredFont(forTextStyle: .headline)
    var pointSize = baseFont.pointSize
    while true {
      let font = baseFont.withSize(pointSize)
      let attr = [NSFontAttributeName: font]
      let rect = (title as NSString).boundingRect(with: CGSize(width: size.width - 40, height: CGFloat.greatestFiniteMagnitude), options: [.truncatesLastVisibleLine, .usesLineFragmentOrigin], attributes: attr, context: nil)
      
      if rect.size.height < self.bounds.size.height {
        return (font, rect.size, false)
      }
      
      if pointSize < 12 {
        return (font, CGSize(width: size.width - 20, height: size.height), true)
      }
      
      pointSize -= 2
    }
  }
  
  override func sizeThatFits(_ size: CGSize) -> CGSize {
    
    var (_, drawingSize, overflow) = fontAndTitleSize(size)
    if overflow == false {
      drawingSize.width += 20 + 20
    }else
    {
      drawingSize.width += 20
    }
    drawingSize.height = size.height
    return drawingSize
  }
  
  override func draw(_ rect: CGRect) {
    guard superview != nil else { return }
    
    // if superview!.bounds.size.width != UIScreen.main.bounds.size.width { return }
    
    let (font, drawingSize, overflow) = fontAndTitleSize(rect.size)
    let myCenter = CGPoint(x: self.frame.midX, y: self.frame.midY)
    let superCenter = CGPoint(x: superview!.bounds.midX, y: superview!.bounds.midY)
    let dif = myCenter.x - superCenter.x
    
    //dif += superview!.bounds.size.width - UIScreen.main.bounds.size.width
    
    
    // print(superview!.bounds.size.width)

    var drawingRect = CGRect.zero
    drawingRect.origin.x = (self.bounds.size.width - drawingSize.width) / 2 + ( overflow ? 0 : 20 )
    drawingRect.origin.y = (self.bounds.size.height - drawingSize.height) / 2
    drawingRect.size = drawingSize
    
    //drawingRect.origin.x -= dif
    
    if drawingRect.origin.x < 0 { drawingRect.origin.x = 0 }
    if drawingRect.maxX > maximumWidth {
      drawingRect.origin.x = maximumWidth - drawingRect.size.width
    }
    
    let attr = [NSFontAttributeName: font, NSForegroundColorAttributeName: colorScheme.textColor] as [String : Any]
    
    (title as NSString).draw(with: drawingRect, options: [.truncatesLastVisibleLine, .usesLineFragmentOrigin], attributes: attr, context: nil)
    
    if let image = badgeType.imageWithTint(colorScheme.tintColor),
       let rect = badgeType.imageLocationTo(drawingRect){

      image.draw(at: rect.origin)
    }
  }
  
}

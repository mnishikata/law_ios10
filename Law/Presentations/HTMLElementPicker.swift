//
//  HTMLElementPicker.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 13/04/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import Foundation

class HTMLElementPicker : UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
	@IBOutlet weak var picker: UIPickerView!
	var dataSource:[ElementDescriptor]!
	weak var selectedElement:ElementDescriptor? = nil
	var selectedArticle:Int? = nil
	var jumpToAnchorBlock: ((_ anchor: String?, _ range: NSRange, _ inElement:ElementDescriptor)->Void)? = nil
	var willClose: (()->Void)? = nil

	init( dataSource:[ElementDescriptor]) {
		
		super.init( nibName:"HTMLElementPicker", bundle:nil )
		self.dataSource = dataSource
	}
	
	override init( nibName:String?, bundle:Bundle? )
	{
		super.init( nibName: nibName, bundle:bundle)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override var preferredContentSize: CGSize {
		set {}
		get {
			return CGSize(width: 270,height: 150)
		}
	}
	
	override func viewDidLoad() {
		picker.delegate = self
		picker.dataSource = self
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		willClose?()
	}
	
	func numberOfComponents( in pickerView: UIPickerView) -> Int {
		return 2
	}
	
	func pickerView( _ pickerView: UIPickerView,
	                  widthForComponent component: Int) -> CGFloat {
		if component == 1 { return 70 }
		else { return 180 }
	}
	
	func pickerView( _ pickerView: UIPickerView,
	                  numberOfRowsInComponent component: Int) -> Int {
		if component == 0 { return dataSource.count + 1 }
		
		if component == 1 {
			if selectedElement == nil {
				return 0
			}else {
				return selectedElement!.count + 1
			}
		}
		return 0
	}
	
	func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
		
		let pickerLabel = (view as? UILabel) ?? UILabel()
		pickerLabel.textColor = UIColor.black
		pickerLabel.backgroundColor = UIColor.clear
		pickerLabel.font = UIFont.systemFont(ofSize: 18)
		pickerLabel.textAlignment = .center
		if component == 0 {
			if row == 0 {
				pickerLabel.text = WLoc("Jump")
				return pickerLabel
			}
			
			let element = dataSource[row-1]
			var title = element.title(coded: false)

			title = title.replacingOccurrences(of: "第", with: "")
			title = title.replacingOccurrences(of: "条", with: "")

			pickerLabel.text = title
			return pickerLabel
		}
		
		if component == 1 {
			pickerLabel.textAlignment = .left

			if selectedElement == nil {
				pickerLabel.text = "-"
				return pickerLabel
			}else {
				if row == 0 {
					pickerLabel.text = WLoc("All Articles")
					return pickerLabel
				}
				let article = selectedElement![row-1]
				let title = article.title(coded: false)
				if title == selectedElement?.title(coded: false) {
					pickerLabel.text = "１項"
					return pickerLabel
				}
				if article.gou! {
					pickerLabel.text = "   " + (title as String) + "号"
					
				}else {
					pickerLabel.text = (title as String) + "項"
				}
				return pickerLabel
			}
		}
		pickerLabel.text = ""
		return pickerLabel
	}
	
	func pickerView( _ pickerView: UIPickerView,
	                  didSelectRow row: Int,
	                               inComponent component: Int) {
		if component == 0 {
			if row > 0 {
				selectedElement = dataSource[row-1]
				selectedArticle = nil
				pickerView.selectRow(0, inComponent: 1, animated: false)
				pickerView.reloadComponent(1)
			}
		}
		
		if component == 1 {
			if row == 0 { selectedArticle = nil }
			else { selectedArticle = row - 1 }
		}
		
		// SEND MESSAGE HEER
		if let element = selectedElement {
			if selectedArticle == nil {
				jumpToAnchorBlock?(element.anchor, element.range, element )
			}else {
				jumpToAnchorBlock?(element[selectedArticle!].anchor, element[selectedArticle!].range, element)
			}
		}
	}
}

//
//  ReferenceViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2014/08/28.
//  Copyright (c) 2014年 Catalystwo. All rights reserved.
//

import UIKit

class ReferenceViewController: UIViewController, UIWebViewDelegate, NSURLConnectionDelegate,NSURLConnectionDataDelegate {

	@IBOutlet weak var webView: UIWebView!
	weak var delegate:NSObject?
	var downloadingURL:NSURL? = nil
	var webViewScale:Double!

	var loadRequest:NSURLRequest? = nil

	override func viewDidLoad() {
		super.viewDidLoad()
		
		let gesture = UIPinchGestureRecognizer(target: self, action: Selector("pinch:"))
		self.view.addGestureRecognizer(gesture)

		let doubleVal:Double? = NSUserDefaults.standardUserDefaults().doubleForKey("WebViewZoomScale")
		
		if doubleVal != nil { webViewScale = doubleVal }
		else { webViewScale = 1.0 }
		
		if webViewScale < 0.2 { webViewScale = 1.0 }
		if webViewScale > 5.0 { webViewScale = 1.0 }
		
		
		let item = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: Selector("close"))
		
		webView.delegate = self
		
		self.navigationItem.leftBarButtonItem = item
		
		self.title = WLoc("Reference")
		
		if loadRequest != nil { webView!.loadRequest(loadRequest!); loadRequest = nil }
	}
	
	func pinch(gesture:UIPinchGestureRecognizer) {
		
		let scale = gesture.scale
		
		if gesture.state == .Changed {
			
			let script = "document.body.style.zoom = \"\(webViewScale * Double(scale)*100)%\" "
			self.webView.stringByEvaluatingJavaScriptFromString(script)
		}
		
		if gesture.state == .Ended {
			webViewScale = webViewScale * Double(scale)
			NSUserDefaults.standardUserDefaults().setDouble(Double(webViewScale), forKey: "WebViewZoomScale")
		}
	}
	
	
	func close() {
		
		dismissViewControllerAnimated(true) {
			let detailViewController:DetailViewController? = self.delegate as? DetailViewController
			detailViewController?.referenceDidClose()
		}
	}
	
	
	func loadURL(URL:NSURL) {
		
		downloadingURL = URL
		loadRequest = NSURLRequest(URL: URL, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 60.0)
		
		if webView != nil { webView!.loadRequest(loadRequest!); loadRequest = nil }

	}



	//
	// MARK: -  Web View Delegate
	//
	
	func webViewDidFinishLoad(webView: UIWebView) {
		
		let script = "document.body.style.zoom = \"\(webViewScale*100)%\" "
		self.webView.stringByEvaluatingJavaScriptFromString(script)

		
		if downloadingURL != nil {
			
			let result:String? = webView.stringByEvaluatingJavaScriptFromString("document.documentElement.outerHTML")
			
			if result != nil { downloadURL(result!) }
			
		}
	}
	
	func webView( webView: UIWebView,
		didFailLoadWithError error: NSError?) {
			UIApplication.sharedApplication().networkActivityIndicatorVisible = false
			
			downloadingURL = nil
	}
	
	func downloadURL(pageString: String) {
		
		downloadingURL = nil
		
		
		let ( indexURL, contentURL ) = pageString.getInyoTitleAndInyoData()
		
		if( indexURL == nil || contentURL == nil ) {
			return
		}
		
		UIApplication.sharedApplication().networkActivityIndicatorVisible = true
		
		
		print("Downloading \(indexURL)")
		
		DownloadManager.sharedManager().addDownloadTask( "Index", URL: indexURL!, progressHandler:{ (progress:Double)->Void in }, completionHandler:{(success:Bool, data:NSData?, error:NSError?)->Void in
			
			if success == false {
				UIApplication.sharedApplication().networkActivityIndicatorVisible = false
				
				showError(error)
				return
			}
			
			
			// Update local file
			var inyoTitle = self.getInyoTitle(data!, convert: true)
			
			if inyoTitle == nil {
				inyoTitle = ""
			}
			
			// Download Contents
			DownloadManager.sharedManager().addDownloadTask( "Content", URL: contentURL!, progressHandler:{ (progress:Double)->Void in }, completionHandler:{(success:Bool, data:NSData?, error:NSError?)->Void in
				
				if success == false {
					UIApplication.sharedApplication().networkActivityIndicatorVisible = false
					
					showError(error)
					return
				}
				
				// Reconstruct link
				
				let convertedHTML = self.processInyoData(data!, convert: true)
				
				
				// Conbine inyoTitle and convertedHTML
				
				self.appendInyoTitle( inyoTitle!, to: convertedHTML )
				
				dispatch_async(dispatch_get_main_queue(), {
					self.webView.loadHTMLString(convertedHTML as String, baseURL: contentURL)
					
				})
				
				
				UIApplication.sharedApplication().networkActivityIndicatorVisible = false
				
			})
			
			
		})
		
		
//		downloadDidEndClousre = { ()->() in
//			
//			// Update local file
//			
//			
//			let inyoTitle = self.processInyoTitle(self.mutableData, convert: true)
//			
//			
//			// Download Contents
//			
//			let request = NSURLRequest(URL: contentURL!, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 60.0);
//			
//			
//			self.downloadDidEndClousre = { () -> () in
//				
//				// Reconstruct link
//				
//				let convertedHTML = self.processInyoData(self.mutableData, convert: true)
//				
//				
//				// Conbine inyoTitle and convertedHTML
//				
//				self.appendInyoTitle( inyoTitle!, to: convertedHTML )
//				
//				dispatch_async(dispatch_get_main_queue(), {
//					self.webView.loadHTMLString(convertedHTML, baseURL: contentURL)
//					
//				})
//				
//				// Save
//				
//				
//				
//				UIApplication.sharedApplication().networkActivityIndicatorVisible = false
//				
//			}
//			
//			// [2] Download Contents
//			NSURLConnection(request: request, delegate: self, startImmediately: true)
//			
//			
//		}
//		
//		// [1]  start downloading index
//		println("start download")
//		
//		NSURLConnection(request: request, delegate: self, startImmediately: true)

	}
	
//	// MARK:- Download Delegate
//	
//	var downloadDidEndClousre:()->()? = { () -> () in /* write completion handler here */ }
//	var mutableData = NSMutableData()
//	
//	func connection(connection: NSURLConnection!, didReceiveResponse response: NSURLResponse!) {
//		
//		mutableData.length = 0
//	}
//	
//	func connection(connection: NSURLConnection!, didReceiveData: NSData!) {
//		
//		mutableData.appendData(didReceiveData)
//		
//	}
//	
//	func connectionDidFinishLoading(connection: NSURLConnection!) {
//		println("\(connection.originalRequest.URL.path) done \(mutableData.length)")
//		downloadDidEndClousre()
//		
//	}
//	
//	
//	
//	func connection(connection: NSURLConnection!, didFailWithError: NSError!) {
//		mutableData.length = 0
//		
//		println("download error")
//		showError(didFailWithError)
//	}
	
	// MARK:- Processing
	
	
	func getInyoTitle( data: NSData, convert:Bool ) -> String? {
		
		print("process \(data.length)")
    
    var htmlString:NSMutableString? = NSMutableString(data: data, encoding: NSASCIIStringEncoding)
    if htmlString == nil { return (nil) }
    
    let theType = string!.encodingType
    htmlString = NSMutableString(data: data, encoding: theType)
    
    if htmlString == nil { return (nil) }
    
    findAndConvertKanji( &string!, convertToZenkaku: true)
    
    // First bold sting in html
    // Get information if avaialbel
    
    let regex1 = "(?<=<B>).*(?=</B>)"
    
    let range1 = htmlString.range(of: regex1, options: [.regularExpression, .caseInsensitive])
    
    if ( range1.location == NSNotFound ) {
      return nil
    }
    
    let string1:NSString = htmlString.substring(with: range1)
    return (string1.replacingPercentEscapes(using: String.Encoding.utf8.rawValue)  )
    
  }
  
	func processInyoData( data: NSData, convert: Bool ) -> NSString {
		
		var theString:NSMutableString? = NSMutableString(data: data, encoding: NSASCIIStringEncoding)
		
		if theString == nil { return "" }
		
		let theType = theString!.encodingType
		theString = NSMutableString(data: data, encoding: theType)
		
		
		if ( convert == true &&  (theString != nil) ) {
			
			replaceCSS( &theString!, stripCSS:false)
			findAndConvertKanji( &theString!, convertToZenkaku: true)
			//			string = convertLink(string)
			
			// Reconstruct link
			if theString == nil { return "" }
			
			theString = theString!.appendParenthesisTags()
		}
		
		if theString == nil { return "" }
		
		return theString!
	}
	
	
	func appendInyoTitle( title:NSString, to toString:NSString ) -> NSString {
		
		let range:NSRange = toString.rangeOfString("<BODY>", options: .CaseInsensitiveSearch)
		
		if range.location == NSNotFound { return toString }
		
		let mstr = toString.mutableCopy() as! NSMutableString
		
		mstr.replaceCharactersInRange(range, withString: "<BODY>\n<B>\(title)</B>")
		
		return mstr as NSString
	}
	
	

}

//
//  SwipeNavigationController.m
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2014/09/04.
//  Copyright (c) 2014年 Catalystwo. All rights reserved.
//

#import "SwipeNavigationController.h"
//#import "SSWAnimator.h"

@interface SwipeNavigationController ()

@end

@implementation SwipeNavigationController


-(id)initWithRootViewController:(UIViewController *)rootViewController
{
	self = [super initWithRootViewController: rootViewController];
	if (self) {
		// Custom initialization
		
//		self.navigationBarHidden = YES;
	}
	return self;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	
//	if( [self swipeAnywhere] )
//	{
//		swiper_ = [[SloppySwiper alloc] initWithNavigationController:self];
//		self.delegate = swiper_;
//		swiper_.animator.delegate = self;
//		
//	}
	
	
	self.transitioningDelegate = self;
}

-(BOOL)swipeAnywhere
{
	return NO;
	
//	//SwipeAnywhere
//	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
//	
//	[ud synchronize];
//	
//	if( [ud objectForKey:@"SwipeAnywhere"] && [ud boolForKey:@"SwipeAnywhere"] == NO )
//	{
//		return NO;
//	}
//	
//	
//	return YES;
}

-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
	[super pushViewController:viewController animated:animated];
//	if( [self swipeAnywhere] == YES && !swiper_ )
//	{
//		swiper_ = [[SloppySwiper alloc] initWithNavigationController:self];
//		self.delegate = swiper_;
//		swiper_.animator.delegate = self;
//		
//	}
//	else if( [self swipeAnywhere] == NO && swiper_ )
//	{
//		[swiper_.panRecognizer removeTarget:nil action:nil];
//		self.delegate = nil;
//		swiper_ = nil;
//		swiper_.animator.delegate = nil;
//	}

}



//-(void)transistionAnimationDidEnd:(SSWAnimator*)sender
//{
//	if( !self.presentingViewController )
//	{
//		if( self.documentClosingBlock)
//		{
//			self.documentClosingBlock();
//			
//		}
//		
//	}
//	
//	[[[[UIApplication sharedApplication] delegate] window].rootViewController setNeedsStatusBarAppearanceUpdate];
//	
//}

//-(UIPanGestureRecognizer*)panGestureRecognizer
//{
//	return swiper_.panRecognizer;
//}


-(UIModalPresentationStyle)modalPresentationStyle
{
	return UIModalPresentationCustom;
}

#pragma mark - Animation

//- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
//																						presentingController:(UIViewController *)presenting
//																							 sourceController:(UIViewController *)source
//{
//	
//	if( !openAnimator_)
//		openAnimator_ = [[SSWOpenAnimator alloc] init];
//	
//	return openAnimator_;
//	
//}

//- (id<UIViewControllerInteractiveTransitioning>)interactionControllerForPresentation:(id<UIViewControllerAnimatedTransitioning>)animator
//{
//
//	if( !interactionController_ )
//	{
//	interactionController_ = [[UIPercentDrivenInteractiveTransition alloc] init];
//	interactionController_.completionCurve = UIViewAnimationCurveEaseOut;
//	}
//
//	return interactionController_;
//}

//- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
//{
//	return swiper_.animator;
//}
//
//- (id<UIViewControllerInteractiveTransitioning>)interactionControllerForDismissal:(id<UIViewControllerAnimatedTransitioning>)animator
//{
//	return swiper_.interactionController;
//}

@end
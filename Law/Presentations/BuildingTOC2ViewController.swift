//
//  BuildingTOC2ViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 30/04/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import Foundation


class BuildingTOC2ViewController : UIViewController {
	
	static var buildingTOC2 = false {
		didSet {
			if buildingTOC2 == true {
				bgTask = UIApplication.shared.beginBackgroundTask(expirationHandler: nil)
			}else {
				if bgTask != 0 {
					UIApplication.shared.endBackgroundTask( bgTask )
					bgTask = 0
				}
			}
		}
	}
	static var bgTask:UIBackgroundTaskIdentifier = 0
	static var startTime:TimeInterval = 0

	@IBOutlet weak var excerptMode: UISwitch!
	@IBOutlet weak var label: UILabel!
	@IBOutlet weak var indicator: UIActivityIndicatorView!
	required init() {
		super.init(nibName: "BuildingTOC2ViewController", bundle: nil)
	}
	
	required init(coder:NSCoder) {
		super.init(nibName: nil, bundle: nil)
    self.preferredContentSize = CGSize(width: 320, height: 480 )
	}
	
	@IBAction func close(_ sender: AnyObject) {
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func update(_ sender:UIButton) {
		updateOverwriting(false)
	}
	
  @IBOutlet weak var updateButton: FlatButton!
	@IBAction func updateAll(_ sender:UIButton) {
    updateButton.isHidden = true
		updateOverwriting(true)
	}

  override func viewDidLoad() {
    super.viewDidLoad()
    let scheme = BackgroundColorSchemeType()
    updateButton.backgroundColor = scheme.tintColor
  }
	
	func updateOverwriting(_ overwrite:Bool) {
    
		if BuildingTOC2ViewController.buildingTOC2 {
			indicator.startAnimating()
			return
		}
		
		indicator.startAnimating()
		
		BuildingTOC2ViewController.buildingTOC2 = true
		BuildingTOC2ViewController.startTime = Date.timeIntervalSinceReferenceDate
		
		let fp = FilePackage.shared

    fp.updateOverwriting(overwrite, progress: { (count, startCount) in
      self.label.text = "\(startCount - count) / \(startCount)"

    } , completion: {
    
      self.indicator.stopAnimating()
      self.label.text = "終わりました （" + String(
        Int(Date.timeIntervalSinceReferenceDate - BuildingTOC2ViewController.startTime ) ) + "秒）"
      
      BuildingTOC2ViewController.buildingTOC2 = false
      
      UserDefaults.standard.set(true, forKey:"UpdatedTOC2ForVersion3")
      UserDefaults.standard.synchronize()
      
      self.updateButton.isHidden = false
      self.updateButton.removeTarget(self, action: nil, for: .touchUpInside)
      self.updateButton.addTarget(self, action: #selector(self.close(_:)), for: .touchUpInside)
      self.updateButton.setTitle(WLoc("Start Using"), for: .normal)

    })

  }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
    /*
		excerptMode.isOn = (UserDefaults.standard.object(forKey: "ExcerptMode") as? String == "Yes")
		*/
    
		let button = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(close))
		navigationItem.rightBarButtonItem = button
		
		if BuildingTOC2ViewController.buildingTOC2 {
			indicator.startAnimating()
			return
		}
	}

	@IBAction func changeExcerptMode(_ sender:UISwitch) {
		//UserDefaults.standard.set(excerptMode.isOn ? "Yes" : "No", forKey: "ExcerptMode")
	}
}

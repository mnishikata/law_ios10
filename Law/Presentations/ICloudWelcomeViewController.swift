//
//  ICloudWelcomeViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2/01/15.
//  Copyright (c) 2015 Catalystwo. All rights reserved.
//

import UIKit

class ICloudWelcomeViewController: UIViewController {
	
	@IBOutlet weak var messageLabel: UILabel!
	@IBOutlet weak var syncButton: FlatButton!
	
	var completionHandler:( (_ accept:Bool) ->Void)? = nil
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Do any additional setup after loading the view.
		
		syncButton.setTitle( WLoc("Start iCloud Sync"), for: .normal )
		messageLabel.text = WLoc("iCloud First Message")
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	@IBAction func syncAction(_ sender: AnyObject) {
		dismiss( animated: true, completion: {
			
			self.completionHandler?(true)
		})
	}
	
	/*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  JumpViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 28/12/14.
//  Copyright (c) 2014 Catalystwo. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox

/*
extension UITextField {
  func modifyClearButton(with image : UIImage) {
    let clearButton = UIButton(type: .custom)
    clearButton.setImage(image, for: .normal)
    clearButton.frame = CGRect(x: 0, y: 0, width: 15, height: 15)
    clearButton.contentMode = .scaleAspectFit
    clearButton.addTarget(self, action: #selector(UITextField.clear(_:)), for: .touchUpInside)
    rightView = clearButton
    rightViewMode = .whileEditing
  }
  
  func clear(_ sender : AnyObject) {
    self.text = ""
    sendActions(for: .editingChanged)
  }
}
*/

class JumpViewController: UIViewController, APNumberPadDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
	
	@IBOutlet weak var tabPickerView: UIPickerView?
	
	// User Interface for Popover
	@IBOutlet weak var inputLabel: UILabel?
	
	// User Interface for Keypad
	@IBOutlet weak var helpLabel: UILabel?
	@IBOutlet weak var textField: UITextField?
	@IBOutlet weak var displayTargetButton: UIButton!
	
	@IBOutlet weak var tocButton: UIButton!
	var keypadView: APNumberPad? = nil
	var jumpAction: ((_ text: String?, _ tabIdentifierOrDocItem: AnyObject?, _ excerpt: Bool)->Void)? = nil
	var cancelBlock: ((_ swtichToFind: Bool)->())? = nil
	var pickerBlock: (()->())? = nil
	var toc2: [ElementDescriptor]? = nil
	var connector: String? = nil
	var connectorStatus = ConnectorStatus(string: "")
	
	var inputtedString: String = ""
	var showTab_: Bool = true
	var excerptDisplayMode = UserDefaults.standard.bool(forKey: "JumpViewControllerExcerptDisplayMode")
	let keypadWidth: CGFloat = 320
	var allDocuments: NSArray? = nil
  var currentDocumentInfo: [RestorationKey: Any]? = nil
	var shortcutsToDocuments = true
  var backgroundColorScheme: BackgroundColorSchemeType = .Paper

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	init(showTab: Bool) {
		showTab_ = showTab
		super.init(nibName: nil, bundle: nil)
		
		shortcutsToDocuments = (UserDefaults.standard.object(forKey: "ShortcutDestination") as? String == "All Documents" )
		
    if shortcutsToDocuments == true {
      let condition: ((_ dictionary: DocItem) -> Any?) = {
        (dictionary: DocItem) -> Any?   in
        
        if dictionary.isLawEntity { return dictionary }
        
        return nil
      }
      
      let array = FilePackage.shared.documents.collectWithCondition(condition)
      allDocuments = array as NSArray?
      if let tab = TabDataSource.shared.current {
        if let plist = TabDataSourceStorage<StackNavigationStorage>()[tab] {
          let restorationInfo = plist.last
          currentDocumentInfo = restorationInfo
        }
      }
    }
	}
	
	override func loadView() {
		
		if popoverStyle {
			Bundle.main.loadNibNamed("JumpViewControllerPopover", owner: self, options: nil)
			
		}else {
			Bundle.main.loadNibNamed("JumpOnPhoneViewController", owner: self, options: nil)
		}
	}
	
  var popoverStyle: Bool {
		
		if self.traitCollection.userInterfaceIdiom == .pad || UIScreen.main.applicationFrame.size.width > 400.0  {
			return true
		}else {
			return false
		}
	}
	
	override var preferredContentSize: CGSize {
    
    set {}
    get {
      var identifiers = TabDataSource.shared.all
      if let tab = TabDataSource.shared.current {
        if identifiers.contains(tab) == false {
          identifiers.insert(tab, at: 0)
        }
      }
      
			var showsPicker = true
			
			if shortcutsToDocuments == true {
				if showTab_ == false || allDocuments!.count <= 1 {
					showsPicker = false
				}
			}else {
				if showTab_ == false || identifiers.count <= 1 {
					showsPicker = false
				}
			}
			
			if popoverStyle {
				if self.traitCollection.verticalSizeClass == .compact {
					if showsPicker == false {
						return CGSize(width: keypadWidth, height: 280)
					}
					return CGSize(width: keypadWidth + 180, height: 280)
				}
				
				if showsPicker == false {
					return CGSize(width: keypadWidth,height: 320)
				}
				return CGSize(width: keypadWidth,height: 320+162)
				
      }else {
        var identifiers = TabDataSource.shared.all
        
        if let tab = TabDataSource.shared.current {
          if identifiers.contains(tab) == false {
            identifiers.insert(tab, at: 0)
          }
        }
				
				if showsPicker == false { return CGSize(width: keypadWidth, height: 90) }
				
				return CGSize(width: keypadWidth,height: 210) // 150 for short version
			}
		}
	}
	
	
	func deferLoadingViewComponents() {
		
		let viewWidth = self.view.bounds.size.width
		let pickerView = UIPickerView(frame: CGRect(x: 0, y: 78, width: viewWidth, height: 162)) /// 91
		pickerView.dataSource = self
		pickerView.delegate = self
		pickerView.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
		self.view.addSubview(pickerView)
		
		tabPickerView = pickerView
		
		//// Adjust views
		
		if popoverStyle {
			if self.traitCollection.verticalSizeClass == .compact {
				
				tabPickerView!.frame = CGRect(x: 0, y: self.view.bounds.size.height - 162, width: self.view.bounds.size.width - keypadWidth , height: 162)
				tabPickerView!.autoresizingMask = [.flexibleTopMargin, .flexibleRightMargin]
				
			}else {
				tabPickerView!.frame = CGRect(x: 0, y: 78, width: viewWidth, height: 162)
			}
			
		}else {
			tabPickerView!.frame = CGRect(x: 0, y: 50, width: viewWidth, height: 162)
		}
		
		tabPickerView!.setNeedsLayout()
		
		// Setup Pickerview
		
    var identifiers = TabDataSource.shared.all
    
    if let tab = TabDataSource.shared.current {
      if identifiers.contains(tab) == false {
        identifiers.insert(tab, at: 0)
      }
    }
    
    if shortcutsToDocuments == false {
      
			if showTab_ == false || identifiers.count <= 1 {
				tabPickerView!.isHidden = true
			}else {
				
				helpLabel?.isHidden = true
				
				if let tab = TabDataSource.shared.current {
					if let row = identifiers.index(of: tab) {
						tabPickerView!.selectRow(row, inComponent: 0, animated: false)
					}
				}
				
				// Setup icon on picker
				
				let icon = UIImageView(frame: CGRect(x: 0,y: (tabPickerView!.bounds.size.height - 28 )/2,width: 28,height: 28))
				icon.image = UIImage(named: "TabSmallIcon")?.colorizedImage(withTint: backgroundColorScheme.tintColor, alpha: 1.0, glow: false)
				icon.autoresizingMask = [.flexibleRightMargin ]
				tabPickerView!.addSubview(icon)
			}
			
			
		}else {
			
			if showTab_ == false || allDocuments!.count <= 1 {
				tabPickerView!.isHidden = true
			}else {
				
				var row: Int? = nil
				
				if let lastPathComponent = currentDocumentInfo?[.packageURL] as? String {
					for dictionary in allDocuments! {
						if (dictionary as! DocItem).packageName == lastPathComponent {
							
							row = allDocuments!.index(of: dictionary)
							break
						}
					}
				}
				
				if row != nil {
					tabPickerView!.selectRow(row!, inComponent: 0, animated: false)
				}
			}
		}
	}
  
  var keyPadHeight: CGFloat = 240

	override func viewDidLoad() {
		super.viewDidLoad()
		
    var className: NSString? = nil
    if backgroundColorScheme.inverted { className = "APDarkPadStyle" }

		if popoverStyle {
			// Keyboard
			
			keypadView = APNumberPad(delegate: self, numberPadStyleClass: className as String!, iPadStyle: true)
			
			if self.traitCollection.verticalSizeClass == .compact {
				
				keyPadHeight = 180
				
				keypadView!.frame = CGRect(x: self.view.bounds.size.width - keypadWidth, y: self.view.bounds.size.height - keyPadHeight, width: keypadWidth, height: keyPadHeight)
				keypadView!.autoresizingMask = [.flexibleTopMargin, .flexibleLeftMargin]
				
			}else {
				keypadView!.frame = CGRect(x: 0, y: self.view.bounds.size.height - keyPadHeight, width: self.view.bounds.size.width, height: keyPadHeight)
				keypadView!.autoresizingMask = [.flexibleTopMargin, .flexibleWidth]
			}
			
			self.view.addSubview(keypadView!)
			
		}else {
			let placeholderText = WLoc("Jump to article")
      textField?.attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [NSForegroundColorAttributeName: backgroundColorScheme.subTextColor, NSFontAttributeName: textField!.font])
      textField?.textColor = backgroundColorScheme.textColor
      
			// InputView
			keypadView = APNumberPad(delegate: self, numberPadStyleClass: className as String!, iPadStyle: false)
			
			textField!.inputView = keypadView!
		}
		
		// Ohter setup
		
		inputLabel?.text = inputtedString
    inputLabel?.textColor = backgroundColorScheme.textColor
    helpLabel?.textColor = backgroundColorScheme.subTextColor
    
		self.view.backgroundColor = UIColor.clear
    if let popover = self.popoverPresentationController {
      popover.backgroundColor = backgroundColorScheme.popoverBackgroundColor
    }
    
		// Setup Pickerview
		
    var identifiers = TabDataSource.shared.all
    
    if let tab = TabDataSource.shared.current {
      if identifiers.contains(tab) == false {
        identifiers.insert(tab, at: 0)
      }
    }
    
		if shortcutsToDocuments == false {
			if showTab_ == false || identifiers.count <= 1 {
				
			}else {
				helpLabel?.isHidden = true
			}
			
		}else {
			if showTab_ == false || allDocuments!.count <= 1 {
				
			}else {
				helpLabel?.isHidden = true
				var row: Int? = nil
				
				if let lastPathComponent = currentDocumentInfo?[.packageURL] as? String {
					for dictionary in allDocuments! {
						if (dictionary as! DocItem).packageName == lastPathComponent {
							row = allDocuments!.index(of: dictionary)
							break
						}
					}
				}
				
				if row != nil {
				}
			}
		}
		
		// VERSION 3
		
		tocButton.isHidden = true//(toc2 == nil)
		displayTargetButton.isHidden = true//(toc2 == nil)

		updateUI()
	}
	
	func insertText(_ string: String!) {
		
		inputtedString = inputtedString + string
		inputLabel?.text = inputtedString
		textField?.text = inputtedString
		connectorStatus.transitionWithString(string)
		
		updateUI()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		textField?.becomeFirstResponder()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		deferLoadingViewComponents()
	}
	
	func enableConnectorButton() -> Bool {
		return toc2 != nil
	}
	
	//MARK:- Actions
	
	func inputConnector() {
		if toc2 == nil { return }
		
		if connectorStatus.keyTopString == "号" && inputtedString.hasSuffix("項") {
			inputtedString = String( inputtedString.characters.dropLast() )
		}

		insertText(connectorStatus.keyTopString)
	}
	
	func jumpTapped() {
    guard let tab = TabDataSource.shared.current else { return }
		if shortcutsToDocuments == false {
			var tabIdentifier: String? = nil
			
			var identifiers = TabDataSource.shared.all
			
			if identifiers.contains(tab) == false {
				identifiers.insert(tab, at: 0)
			}
			
			if showTab_ == false || identifiers.count <= 1 {
				tabIdentifier = nil
				
			}else {
				
				if tabPickerView != nil {
					tabIdentifier = identifiers[tabPickerView!.selectedRow(inComponent: 0)]
					if tabIdentifier == tab { tabIdentifier = nil }
				}
			}
			
			jumpAction?(inputtedString, tabIdentifier as NSString?, excerptDisplayMode)
			
		}else {
			
			var dictionary: DocItem? = nil
			
			if showTab_ == false || allDocuments!.count <= 1 {
				
				dictionary = nil
				
			}else {
				
				if let lastPathComponent = currentDocumentInfo?[.packageURL] as? String {
					
					if tabPickerView != nil {
						dictionary = allDocuments![tabPickerView!.selectedRow(inComponent: 0)] as? DocItem
						if dictionary?.packageName == lastPathComponent { dictionary = nil }
					}
				}
			}
			jumpAction?(inputtedString, dictionary, excerptDisplayMode)
		}
	}
	@IBAction func changeDisplayTarget(_ sender: AnyObject) {
		excerptDisplayMode = !excerptDisplayMode
		UserDefaults.standard.set(excerptDisplayMode, forKey: "JumpViewControllerExcerptDisplayMode")
		updateUI()
	}
	
	func deleteBackward() {
		
		if inputtedString == "" { return }
		inputtedString = String( inputtedString.characters.dropLast() )
		inputLabel?.text = inputtedString
		textField?.text = inputtedString
		connectorStatus = ConnectorStatus(string: inputtedString)

		updateUI()
	}
	
	@IBAction func close(_ sender: AnyObject) {
		cancelBlock?(false)
	}
	
	func dismissKeyboard() { //aka jump
		cancelBlock?(false)
	}
	
	func findTapped() {  //aka jump
		cancelBlock?(true)
	}
	
	@IBAction func pickerTapped(_ sender: AnyObject) {  //aka jump
		pickerBlock?()
	}
	
	func numberPadTouchesBegan() {
		AudioServicesPlaySystemSound(0x450);
	}
	
	/*
	func hasText() -> Bool {
	
	if inputtedString.isEmpty == false { return true }
	
	
	var identifiers = all
	
	if identifiers.contains(current!) == false {
	identifiers.insert(current!, atIndex: 0)
	}
	
	
	let tabIdentifier = identifiers[tabPickerView.selectedRowInComponent(0)]
	
	if tabIdentifier == current { return false }
	
	return true
	}
	
:
	*/
	
	func textFieldShouldClear(_ textField: UITextField) -> Bool {
		inputtedString = ""
		connectorStatus = ConnectorStatus(string: inputtedString)

		updateUI()
		return true
	}
	
	func updateUI() {
		textField?.inputView?.setNeedsLayout()
		keypadView?.setRightButtonTitle(connectorStatus.keyTopString)
		displayTargetButton.setImage( excerptDisplayMode ? UIImage(named: "JumpRadioButtons_selected") : UIImage(named: "JumpRadioButtons"), for: UIControlState())
	}
	
	//MARK:- Tab Picker
	
	func pickerView( _ pickerView: UIPickerView,
	                 didSelectRow row: Int,
	                              inComponent component: Int) {
		
		updateUI()
	}
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

		if shortcutsToDocuments == false {
      guard let tab = TabDataSource.shared.current else { return 0 }

			var identifiers = TabDataSource.shared.all
			if identifiers.contains(tab) == false {
				identifiers.insert(tab, at: 0)
			}
			return identifiers.count
			
		}else {
			return allDocuments!.count
		}
	}
	
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}
	
	func pickerView( _ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
		
		let width = pickerView.bounds.size.width
		
		let baseView = UIView( frame: CGRect(x: 0,y: 0,width: width,height: 30) )
		baseView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		
		
		let label = UILabel(frame: CGRect(x: 30,y: 0,width: width-40,height: 30))
		label.lineBreakMode = NSLineBreakMode.byTruncatingMiddle
		label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		label.minimumScaleFactor = 0.8
		label.adjustsFontSizeToFitWidth = true
		label.numberOfLines = 0
		if #available(iOS 9.0, *) {
			label.allowsDefaultTighteningForTruncation = true
		} else {
			// Fallback on earlier versions
		}
		baseView.addSubview(label)
		
		if shortcutsToDocuments == false {
      guard let tab = TabDataSource.shared.current else { return baseView }
			var identifiers = TabDataSource.shared.all
			
			if identifiers.contains(tab) == false {
				identifiers.insert(tab, at: 0)
      }
      
      let identifier = identifiers[row]
      let isSelected = ( identifier == tab )
      
      let stackViewModel = StackNavigationViewModel()
      stackViewModel.tab = identifier

      var titleString = stackViewModel.pathString(omitRoot: false)
      
      if isSelected == true {
        label.textColor = backgroundColorScheme.tintColor
        titleString = titleString + " ✔︎"
      }
      else {
        label.textColor = backgroundColorScheme.textColor
      }
      
      label.text = titleString
      
      return baseView
      
    } else {
			
			var titleString: String = (allDocuments![row] as! DocItem).title
			
			if let lastPathComponent = currentDocumentInfo?[.packageURL] as? String {
				
				let dictionary = allDocuments![row] as! DocItem
				
				if dictionary.packageName == lastPathComponent {
					
					label.textColor = backgroundColorScheme.tintColor
					titleString = titleString + " ✔︎"
        }else {
          label.textColor = backgroundColorScheme.textColor
        }
			}
			label.text = titleString
		}
		
		return baseView
	}
}


//
//  DereferenceViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 20/04/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//
/*
商法508が逆参照されない。
*/
import Foundation

extension RestorationKey {

  static let dereferenceViewControllerResults = "resultsDictionary"
}


class DereferenceViewController : UITableViewController, StackNavigation {
  
  enum RestorationKeys {
    static let title = "title"
    static let subArray = "subArray"
    static let subTitle = "subTitle"
    static let linkURL = "linkURL"
    static let detailedTitle = "detailedTitle"
    static let arttitle = "arttitle"
  }
  
	var allDocuments_: [DocItem] = {
		return FilePackage.shared.documents.collectWithCondition {
       $0.isLawEntity ? $0 : nil
		} as? [DocItem]
	}()!
	
	@IBOutlet weak var textView: UITextView!
	var searching_ = false
	var results_ = [DereferenceItem]()
	var cancelFlag = false
	var href: URL!
	var packageURL: URL!
	var toc2: [ElementDescriptor]!
	var targetElement: ElementDescriptor!
	var backgroundColorScheme: BackgroundColorSchemeType = {
		let ud = UserDefaults.standard
		let obj = ud.object(forKey: "BackgroundColorScheme") as? String
		let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
		return backgroundColorScheme
	}()
	
	init(href: URL, packageURL: URL, toc2: [ElementDescriptor]) {
		super.init(style: .grouped)
		self.href = href //"comcatalystwolaws:(6)@1o30x6R+@S23HO131.html"
		self.packageURL = packageURL
		self.toc2 = toc2
		searchDereferences()
	}
	
  required init(restorationInfo: [RestorationKey: Any]?) {
		super.init(style: .grouped)
		guard let dict = restorationInfo else { return }
		guard let results = dict[.dereferenceViewControllerResults] as? [[String: Any]] else { return }

		results_ = DereferenceViewController.dictionaryToResults(results)
	}
	
	func restorationInfo() -> [RestorationKey: Any] {
		
		let resultsDict = resutlsToDictionary(results_)
		
		return [.dereferenceViewControllerResults : resultsDict ]
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	deinit {
		print("*** DereferenceViewController DEINIT *** ")
	}
	
	override func viewDidLoad() {
		tableView.register(UINib(nibName: "DereferenceCell", bundle: nil), forCellReuseIdentifier: "DereferenceCell")
	}
	
  class func dictionaryToResults(_ dictionary: [[String: Any]]) -> [DereferenceItem] {
    return dictionary.map {
      let title = $0[RestorationKeys.title] as! String
      let subArrayDict = $0[RestorationKeys.subArray] as! [[String: Any]]
      let subArray = subArrayDict.map {
        let subTitle = $0[RestorationKeys.subTitle] as! String
        let linkURL = $0[RestorationKeys.linkURL] as! String
        let detailedTitle = $0[RestorationKeys.detailedTitle] as? String ?? ""
        let arttitle = $0[RestorationKeys.arttitle] as? String ?? ""
        
        return ( subTitle, detailedTitle, linkURL, arttitle )
        } as [DereferenceSubitem]
      
      return ( title: title, subArray: subArray )
    }
  }
  
  func resutlsToDictionary(_ results: [DereferenceItem]) -> [[String: Any]] {
    let resultsPlist = results.map {
      
      let title = $0.title
      let subArray = $0.subArray
      
      let subArrayDict = subArray.map {
        let subTitle = $0.title
        let linkURL = $0.linkURL
        let detailedTitle = $0.detailedTitle
        let arttitle = $0.arttitle
        
        return [ RestorationKeys.subTitle : subTitle,
                 RestorationKeys.detailedTitle: detailedTitle,
                 RestorationKeys.linkURL : linkURL,
                 RestorationKeys.arttitle : arttitle ]
        } as [[String: Any]]
      
      return [RestorationKeys.title: title, RestorationKeys.subArray: subArrayDict]
      
      } as [[String: Any]]
    
    return resultsPlist
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController!.setToolbarHidden(true, animated: false)
		
		let color = backgroundColorScheme.backgroundColor
		
		tableView.backgroundColor = color
		
		self.view.backgroundColor = color
		navigationController?.navigationBar.barTintColor = color
    navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: backgroundColorScheme.textColor, NSFontAttributeName: UIFont.preferredFont(forTextStyle: .headline)]
		navigationController?.toolbar.barTintColor = color
		navigationController?.navigationBar.barStyle = backgroundColorScheme.barStyle
		self.title = WLoc("Dereferences")
    
    if backgroundColorScheme.inverted {
      tableView.indicatorStyle = .white
      tableView.separatorColor = UIColor(white: 1, alpha: 0.3)
    }
	}
	
	func searchDereferences() {
		NSLog("* searchDereferencesTo")
		searching_ = true
		tableView.reloadData()
		
		let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.utility)
		queue.async {
			// 1. Get 
			
			let fp = FilePackage.shared
			let selfLastPathComponent = self.packageURL.lastPathComponent
      let ( _, _, rawSource) = fp.loadPackage(at: self.packageURL, onlyPlist: false, zoomScale: 1.0, rawHTML: true)
			if rawSource == nil { return }
						
			for element in self.toc2 {
				if self.href.absoluteString.range(of: "@" + (element.anchor as String) + "@") != nil  {
					self.targetElement = element
					break
				}
			}
			
			if self.targetElement == nil {
				debugPrint("** UNKNOWN targetElement is nil")
				return // UNKNOWN ERR
			}
			
			// 2. Dispatch
			let root = FilePackage.shared.documents
			let docItem = root.lookForWithCondition  {
				
				// $0.packageURL == self.packageURL  DOES NOT WORK IN SOME CASES
				$0.packageURL?.lastPathComponent == selfLastPathComponent
			}
			
			if docItem == nil {
				debugPrint("** UNKNOWN docItem is nil")
				return
			}
			
			if docItem?.h_file == nil {
				let pkg = ((selfLastPathComponent as NSString).deletingPathExtension as NSString).deletingPathExtension
				docItem?.h_file = pkg.convertPkgNameToHFileKanji()
			}

			DispatchQueue.concurrentPerform( iterations: self.allDocuments_.count)  { (i: size_t) in
				
				if self.cancelFlag == true { return }
				
				// PROCEDURE
				let doc: DocItem = self.allDocuments_[i]
				if doc.packageURL == nil { return }
				let packageURL = doc.packageURL! as URL
				
				if packageURL.lastPathComponent == selfLastPathComponent {
					// INTERNAL LINK
					for element in self.toc2 {
						objc_sync_enter(self)
            self.targetElement.addDereferences(toOther: element, doc: doc, internalLink: true, owner_h_file: docItem!.h_file!)
						objc_sync_exit(self)
					}
					
				}else if let elements = FilePackage.shared.loadHTMLElements(for: packageURL) {
					// EXTERNAL LINK
					for element in elements {
						objc_sync_enter(self)

            self.targetElement.addDereferences(toOther: element, doc: doc, internalLink: false, owner_h_file: docItem!.h_file!)
						objc_sync_exit(self)

					}
				}
			}
			
			DispatchQueue.main.async {
				let results = self.targetElement.dereferences()
				self.results_ = results
				self.searching_ = false
				self.tableView.reloadData()
			}
		}
	}
	
	
	// TABLE VIEW DELEGATE
	
	override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		if searching_ { return nil }
		
		let title = "   " + results_[section].title
		let label = UILabel(frame: CGRect(x: 0,y: 0,width: tableView.bounds.size.width,height: 40))
		label.textColor = self.view.tintColor
		label.autoresizingMask = [.flexibleWidth]
		label.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.subheadline)
		label.backgroundColor = UIColor.clear
		label.minimumScaleFactor = 0.75
		label.adjustsFontSizeToFitWidth = true

		label.text = title
		label.sizeToFit()
		return label
	}
	
	override func tableView( _ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 40
	}
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 50
	}
	
	override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
		if searching_ { return nil }
		
		if section + 1 < results_.count { return nil }
		return WLoc("Dereferences Footer")
	}

	override func numberOfSections(in tableView: UITableView) -> Int {
		if searching_ { return 1 }

		return results_.count
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if searching_ { return 1 }
		let subarray = results_[section].subArray
		if subarray.count == 0 { return 1 }
		return subarray.count
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "DereferenceCell", for: indexPath)
		
		let titleLabel = cell.viewWithTag(1) as? UILabel
		let detailLabel = cell.viewWithTag(2) as? UILabel
		let arttitleLabel = cell.viewWithTag(3) as? UILabel

		titleLabel?.text = ""
		detailLabel?.text = ""
		arttitleLabel?.text = ""

    cell.backgroundColor = .clear
		cell.contentView.backgroundColor = backgroundColorScheme.tableBackgroundColor
		titleLabel?.textColor = backgroundColorScheme.textColor
		detailLabel?.textColor = backgroundColorScheme.textColor
		arttitleLabel?.textColor = backgroundColorScheme.subTextColor

    if backgroundColorScheme.inverted {
      let view = UIView()
      view.backgroundColor = backgroundColorScheme.tableSelectionColor
      cell.selectedBackgroundView = view
    }
    
    if searching_ {
			titleLabel?.text = WLoc("Looking Up")
			cell.isUserInteractionEnabled = false
			return cell
		}
		
		let section = indexPath.section
		let row = indexPath.row
		let subarray = results_[section].subArray
		
		if subarray.count == 0 {
			titleLabel?.text = WLoc("No dereferences")
			cell.isUserInteractionEnabled = false
			return cell
		}
		
		if row == 0 {
			titleLabel?.text = subarray[row].title
		}else {
			if subarray[row-1].title == subarray[row].title {
				titleLabel?.text = ""
			}else {
				titleLabel?.text = subarray[row].title
			}
		}
		var detailedTitle = subarray[row].detailedTitle
		let arttitle = subarray[row].arttitle

		detailedTitle = detailedTitle.replacingOccurrences(of: "条第", with: "条")
		detailedTitle = detailedTitle.replacingOccurrences(of: "項第", with: "項")
	
		if detailedTitle.hasPrefix("第") {
			detailedTitle = detailedTitle.substring(from: detailedTitle.index(detailedTitle.startIndex, offsetBy: 1))
		}
		
		detailLabel?.text = detailedTitle
		arttitleLabel?.text = arttitle
		cell.isUserInteractionEnabled = true

		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		var success = false
		defer {
			if success == false {
			tableView.deselectRow(at: indexPath, animated: true)
			}
		}
		
		if searching_ { return }
		if results_.count == 0 { return }

		let section = indexPath.section
		let row = indexPath.row
		let subarray = results_[section].subArray
		if subarray.count == 0 { return }
		let linkURL = subarray[row].linkURL
		
		guard let url = NSURL(string: linkURL) else { return }
		
		
		let scheme = url.scheme
		let specifier = url.resourceSpecifier as NSString?
		
		if scheme != "comcatalystwolaws" || specifier == nil { return }
		
		if specifier == nil  { return }
		if (specifier! as NSString).length == 0 { return }
		
		let comps = specifier!.components(separatedBy: "@")
		let title = comps[0] as NSString
		let packageName = comps.last as NSString!
		let bodyRange = NSMakeRange(title.length + 1, specifier!.length - title.length - (packageName?.length)! - 2)
		let anchorString = specifier!.substring( with: bodyRange )

		print("opening \(title) = \(anchorString) in file \(packageName)")

		// Opening Documents
		
		let fp = FilePackage.shared
		let packageURL = fp.packageURL(from: packageName as! String)
		
		let controller = DetailViewController(restorationInfo: nil)
		controller.viewModel.startJump = true
		controller.viewModel.popNavigationWhenBack = true
		controller.viewModel.insideUtilityViewController = true
		controller.viewModel.excerptType = .excerptArticle
    controller.viewModel.loadFrom(url: packageURL)
    controller.viewModel.anchor = anchorString

		navigationController?.pushViewController( controller, animated: true)
		success = true
	}
}

//
//  PopoverSettingsViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 15/08/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import UIKit
import Social
import StoreKit

class GearViewController: UITableViewController {

  var masterViewController: MasterViewController? = nil
	var documentInteractionController_: UIDocumentInteractionController? = nil

  var showQuickNotes = false

  //@IBOutlet weak var uploadButton: UIButton!, appLabel: UILabel!, fileSIzeLabel: UILabel!
  //@IBOutlet weak var settings: UIButton!, sendButton: UIButton!, linkButton: UIButton!, backup: UIButton!
	
  //@IBOutlet weak var debugButton: UIButton!
	//MARK:- Body
  
  init() {
    super.init(style: .grouped)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
	func showBackupList() {
    guard let master = masterViewController else { return }

    masterViewController!.dismiss(animated: true) {
      
			let controller = BackupListTableViewController(style: .grouped)

			let nav = UINavigationController( rootViewController: controller )
			nav.modalPresentationStyle = .formSheet
      master.appSettingsViewController().showDoneButton = true;
			master.present( nav, animated: true, completion: nil )
  }  
	}
	
	func debug() {
    guard let master = masterViewController else { return }

    masterViewController!.dismiss(animated: true) {
      let controller = BuildingTOC2ViewController( )
      controller.modalPresentationStyle = .formSheet
      master.setPlaceholderViewController()
      master.present( controller, animated: true, completion: nil )
    }
	}
	
  func useWkWebView() {
    let useWkWebView = UserDefaults.standard.bool(forKey: "UseWKWebView_debug")
    UserDefaults.standard.set(!useWkWebView, forKey: "UseWKWebView_debug")
     self.tableView.reloadData()
  }
  
  func help() {
    let address = WLoc("HelpURL")
    UIApplication.shared.openURL( URL(string: address)! )
  }
  
	func appStore() {
    let controller = SKStoreProductViewController()
    controller.loadProduct(withParameters: [SKStoreProductParameterITunesItemIdentifier:916069107], completionBlock: nil)
    controller.delegate = self
    present(controller, animated: true, completion: nil)
/*
		let address = "itms-apps://itunes.apple.com/app/law/id916069107?ls=1&mt=8"
		UIApplication.shared.openURL( URL(string: address)! )
 */
	}
  
  @IBAction func upload(_ sender: AnyObject) {
    guard let master = masterViewController else { return }

    masterViewController!.dismiss(animated: true) {
      
      let storyboard = UIStoryboard(name: "Editor", bundle: nil)
      let nav = storyboard.instantiateInitialViewController() as! CKNavigationController
      
      master.present( nav, animated: true, completion: nil )
    }
  }
  
  func twitter() {
    
    let controller = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
    let address = "itms-apps://itunes.apple.com/app/law/id916069107?ls=1&mt=8"
    
    controller?.add( URL(string: address)! )
    controller?.setInitialText("#法令ブラウザ ")
    present(controller!, animated: true, completion: nil)
  }
  
  func facebook() {
    let controller = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
    let address = "itms-apps://itunes.apple.com/app/law/id916069107?ls=1&mt=8"
    
    controller?.add( URL(string: address)! )
    controller?.setInitialText("#法令ブラウザ ")
    present(controller!, animated: true, completion: nil)
  }
	
	func settings() {
    guard let master = masterViewController else { return }

    masterViewController!.dismiss(animated: true) {
			let nav = UINavigationController( rootViewController: master.appSettingsViewController() )
			nav.modalPresentationStyle = .formSheet
      master.appSettingsViewController().showDoneButton = true;
			master.present( nav, animated: true, completion: nil )
    }
	}
	
	func send() {
    guard let master = masterViewController else { return }

    masterViewController!.dismiss(animated: true, completion: {
      master.export()
    })
	}
	
	func link() {
    guard let master = masterViewController else { return }

    masterViewController!.dismiss(animated: true) {
      
      let controller = LinkToFileTableViewController(style: .grouped)
      
      let nav = UINavigationController(rootViewController: controller)
      nav.modalPresentationStyle = .formSheet
      
      master.present(nav, animated: true, completion: nil)
    }
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Do any additional setup after loading the view.
		/*
		appLabel.text = WLoc("Laws") + " " + ObjectiveCClass.versionString()
		fileSIzeLabel.text = "Library Size:" + getFolderSize()  /*+ ", RAM:" + ObjectiveCClass.ramUsage()*/
		settings.setTitle(WLoc("Settings"), for: .normal)
		sendButton.setTitle(WLoc("Send Data"), for: .normal)
		linkButton.setTitle(WLoc("Link..."), for: .normal)
		backup.setTitle(WLoc("Backup"), for: .normal)
    
    debugButton.isHidden = UserDefaults.standard.bool(forKey: "UpdatedTOC2ForVersion3")
    #if DEBUG
      debugButton.isHidden = false
    #endif
    */
		let _ = CloudKitDomain.shared // Instantiate
    
    
    self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
    
    let link = "comcatalystwohandwritingkeyboard:"
    let appLink = URL(string: link)!
    
    if UIApplication.shared.canOpenURL(appLink) == false  {
      showQuickNotes = true
    }
    
    
    let t1 = Date.timeIntervalSinceReferenceDate
    if t1 < 510022561.0 + 9 * 24 * 60 * 60 { // 509957655.55073798 + 9 days
     showQuickNotes = false
    }
  }
  
  func done() {
    self.dismiss(animated: true, completion: nil)
  }
  
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		 CloudKitDomain.shared.hasPrivateAccount { (hasPrivateAccount: Bool)->Void in
			
				print("hasPrivateAccount \(hasPrivateAccount)")
      //self.uploadButton.isHidden = !hasPrivateAccount
		}
	}
	
	// MARK:-
	func getFolderSize() -> String {
		let documentsDirectoryURL =  try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
		var bool: ObjCBool = false
		if FileManager().fileExists(atPath: documentsDirectoryURL.path, isDirectory: &bool) {
			if bool.boolValue {
				let fileManager =  FileManager.default
        var folderFileSizeInBytes: UInt = 0
				if let filesEnumerator = fileManager.enumerator(at: documentsDirectoryURL, includingPropertiesForKeys: nil, options: [], errorHandler: {
					(url, error) -> Bool in
					print(url.path)
					print(error.localizedDescription)
					return true
				}) {
					while let fileURL = filesEnumerator.nextObject() as? URL {
						do {
							let attributes = try fileManager.attributesOfItem(atPath: fileURL.path)
              if let size = attributes[.size] as? NSNumber {
							folderFileSizeInBytes += size.uintValue
              }
						} catch let error as NSError {
							print(error.localizedDescription)
						}
					}
				}
				let  byteCountFormatter =  ByteCountFormatter()
				byteCountFormatter.allowedUnits = .useAll
				byteCountFormatter.countStyle = .binary
				let folderSizeToDisplay = byteCountFormatter.string(fromByteCount: Int64(folderFileSizeInBytes))
				print(folderSizeToDisplay)  // "X,XXX,XXX bytes"
				
				return folderSizeToDisplay
			}
		}
		
		return ""
	}
  
  
  
  // MARK: - Table view data source
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    // #warning Potentially incomplete method implementation.
    // Return the number of sections.
    
    return 4
  }
  
  override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    switch section {
    case 0: return ""
    case 1: return ""
    case 2: if showQuickNotes { return "FROM US" } else { return "" }
    case 3: return "SHARE"
    default: break
    }
    return nil
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch section {
    case 0: return 2
    case 1: if UserDefaults.standard.bool(forKey: "UpdatedTOC2ForVersion3") != true { return 4 } else {
      #if DEBUG
        return 5
      #endif
      return 3
      }
    case 2:
      if showQuickNotes { return 1 }
      else { return 0 }
    case 3: return 3
    default: break
    }
    return 0
  }
  
  /*
  override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
    if section == 0 {
      return "Library Size:" + getFolderSize()
    }
    
    return nil
  }
  */
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") ??   UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
    cell.imageView?.image = nil
    
    switch indexPath.section {
    case 0:
      switch indexPath.row {
      case 0:
        cell.imageView?.image = UIImage(named:"LawRoundIcon")
        cell.textLabel?.text = WLoc("Laws") + " " + ObjectiveCClass.versionString()
      case 1:
        cell.imageView?.image = UIImage(named:"Help")
        cell.textLabel?.text = WLoc("Help")
      default: break
      }
      
    case 1:
      switch indexPath.row {
      case 0: cell.textLabel?.text = WLoc("Settings")
      case 1: cell.textLabel?.text = WLoc("Backup")
      case 2: cell.textLabel?.text = WLoc("Link...")
      case 3: cell.textLabel?.text = WLoc("rebuild index")
      case 4: cell.textLabel?.text = WLoc("Use WKWebView")
      let useWkWebView = UserDefaults.standard.bool(forKey: "UseWKWebView_debug")
      cell.detailTextLabel?.text = useWkWebView ? "Yes" : "No"

      default: break
      }
    case 2:
      let gifView = UIImageView(frame: CGRect(x: 0, y:0, width: 162, height: 162))
      gifView.autoresizingMask = [.flexibleRightMargin]
      cell.contentView.addSubview(gifView)
      gifView.gifPath = Bundle.main.path(forResource: "quicknotes", ofType: "gif")
      gifView.startGIF()
      
      let textLabel = UILabel(frame: CGRect(x: 170, y:10, width: cell.bounds.size.width - 170, height: 80))
      textLabel.autoresizingMask = [.flexibleWidth]
      textLabel.numberOfLines = 0
      textLabel.text = "通知センターからすぐメモできるクイックメモウィジェット"
      cell.contentView.addSubview(textLabel)

      
      let badge = UIImageView(image: UIImage(named:"store"))
      badge.frame = CGRect(x: 162, y:90, width: cell.bounds.size.width - 162, height: 70)
      badge.autoresizingMask = [.flexibleWidth]
      badge.contentMode = .scaleAspectFit
      cell.contentView.addSubview(badge)

    case 3:
      switch indexPath.row {
      case 0: cell.textLabel?.text = "Facebook"
      cell.imageView?.image = UIImage(named:"facebook")

      case 1: cell.textLabel?.text = "Twitter"
      cell.imageView?.image = UIImage(named:"twitter")

      case 2: cell.textLabel?.text = WLoc("Send Data")

      default: break
      }
    default: break
    }
    return cell
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if indexPath.section == 2 { return 162 }
    return 44
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    switch indexPath.section {
    case 0:
      switch indexPath.row {
      case 0: appStore()
      case 1: help()
      default: break
      }
    case 1:
      switch indexPath.row {
      case 0: settings()
      case 1: showBackupList()
      case 2: link()
      case 3: debug()
      case 4: useWkWebView()
      default: break
      }
    case 2:
      let controller = SKStoreProductViewController()
      controller.loadProduct(withParameters: [SKStoreProductParameterITunesItemIdentifier:326099922], completionBlock: nil)
      controller.delegate = self
      present(controller, animated: true, completion: nil)
    case 3:
      switch indexPath.row {
      case 0: facebook()
      case 1: twitter()
      case 2: send()

      default: break
      }
    default: break
    }
    
    tableView.deselectRow(at: indexPath, animated: true)
  }
}

extension GearViewController: SKStoreProductViewControllerDelegate {
  func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
   dismiss(animated: true, completion: nil)
  }
}

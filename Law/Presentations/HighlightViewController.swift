//
//  HIghlightViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 11/04/15.
//  Copyright (c) 2015 Catalystwo. All rights reserved.
//

import UIKit

protocol HighlightViewControllerDelegate : class {
  
  func highlightViewController( _ sender: HighlightViewController, willCloseWithChange: Bool )
  func highlightViewController( _ sender: HighlightViewController, setHighlight: HighlightColor )
  func requestEditing(from: HighlightViewController )
  func highlightViewController( _ sender: HighlightViewController, deleteWithConfirm: Bool )
  
}

class HighlightViewController: UIViewController {
  
  @IBOutlet weak var button0: HighlightButton!
  @IBOutlet weak var button1: HighlightButton!
  @IBOutlet weak var button2: HighlightButton!
  @IBOutlet weak var button3: HighlightButton!
  @IBOutlet weak var button5: HighlightButton!
  @IBOutlet weak var button4: HighlightButton!
  @IBOutlet weak var textView: UITextView!
  
  var backgroundColorScheme: BackgroundColorSchemeType = .Paper

  private var changedFlag: Bool = false
  var source: DocItem? {
    didSet {
      if (source?.notes != nil) && source!.notes!.isEmpty != false {
        textView?.text = source!.notes!
        textView?.isHidden = false
        
      }else {
        textView?.text = ""
        textView?.isHidden = true
      }
    }
  }
  weak var delegate: HighlightViewControllerDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  init() {
    super.init(nibName: "HighlightViewController", bundle: nil)
  }
  
  
  @IBAction func unhighlight(_ sender: UIButton) {
    
    let requiresConfirm = ( source?.notes != nil ) && ( source!.notes!.characters.count > 0 )
    delegate?.highlightViewController( self, deleteWithConfirm: requiresConfirm )
  }
  
  @IBAction func highlight(_ sender: UIButton) {
    
    changedFlag = true
    
    let tag = sender.tag
    let color = HighlightColor(rawValue: tag)
    
    delegate?.highlightViewController( self, setHighlight: color )
    
    let array: [HighlightButton] = [button0, button1, button2, button3, button4, button5]
    for button in array {
      button.isSelected = button.color == color
      button.setNeedsDisplay()
    }
  }
  
  @IBAction func edit(_ sender: UIButton) {
    
    changedFlag = true
    delegate?.requestEditing(from: self )
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if (source?.notes != nil) && (source!.notes!.characters.count > 0) {
      textView?.text = source!.notes!
      textView?.isHidden = false
      
    }else {
      textView?.text = ""
      textView?.isHidden = true
    }
    
    button0.color = .yellow
    button1.color = .green
    button2.color = .blue
    button3.color = .red
    button4.color = .purple
    button5.color = .underline
    
    let color = source?.color
    
    let array: [HighlightButton] = [button0, button1, button2, button3, button4, button5]
    for button in array {
      
      if button.color == color {
        button.isSelected = true
      }else {
        button.isSelected = false
      }
      button.setNeedsDisplay()
    }
    
    if let popover = self.popoverPresentationController {
      popover.backgroundColor = backgroundColorScheme.popoverBackgroundColor
    }
    self.view.backgroundColor = .clear
    self.view.tintColor = backgroundColorScheme.tintColor
    self.textView.textColor = backgroundColorScheme.textColor

  }
  
  override var preferredContentSize: CGSize {
    set {}
    get {
      
      let notes = source?.notes
      
      if (notes == nil || notes?.isEmpty == true) {
        return CGSize(width: 300, height: 52)
      }
      
      let frame = textView.frame
      var width: CGFloat = 300
      
      let newSize = textView.sizeThatFits( CGSize(width: width - 16, height: 1000000) )
      var height = newSize.height
      
      if width > newSize.width { width = newSize.width }
      if width < 300 { width = 300 }
      if height > 250 { height = 250 }
      
      let viewHeight = frame.origin.y + height + ( self.view.bounds.maxY - frame.maxY )
      
      return CGSize(width: width, height: viewHeight)
    }
  }
  
  func export(_ sender: UIBarButtonItem) {
    
    let controller = UIActivityViewController(activityItems: [textView!.text], applicationActivities: [])
    controller.modalPresentationStyle = .popover
    let popover = controller.popoverPresentationController
    popover?.barButtonItem = sender
    
    present(controller, animated: true, completion: nil)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    let width = self.view.frame.size.width
    let newSize = textView.sizeThatFits( CGSize(width: width, height: 1000000) )
    let height = newSize.height
    
    if height > 250 {  textView.flashScrollIndicators() }
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    delegate?.highlightViewController(self, willCloseWithChange: changedFlag )
  }
}

class HighlightButton: UIButton {
  
  var color: HighlightColor? = nil {
    didSet {
      setNeedsDisplay()
    }
  }
  
  override func draw(_ rect: CGRect) {
    guard color != nil else { return }
    guard let context = UIGraphicsGetCurrentContext() else { return }
    
    var circleRect = rect.integral.insetBy(dx: 5, dy: 5 );
    
    context.saveGState()
    
    if color == .underline {
      
      UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).set()
      context.fillEllipse(in: circleRect);
      let image = UIImage(named: "HighlightButtonUnderline")
      image?.draw( at: rect.origin )
    }else {
      color!.color().set()
      context.fillEllipse(in: circleRect)
    }
    
    if isSelected == true {
      circleRect = rect.integral.insetBy(dx: 2, dy: 2 )
      context.setLineWidth(2 )
      
      let frameColor = tintColor! as UIColor!
      context.setStrokeColor((frameColor?.cgColor)! )
      context.strokeEllipse(in: circleRect)
    }
    
    context.restoreGState();
  }
  
  override func titleRect( forContentRect contentRect: CGRect) -> CGRect {
    return CGRect.zero
  }
}

//
//  SwipeNavigationController.h
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2014/09/04.
//  Copyright (c) 2014年 Catalystwo. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "SloppySwiper.h"
//@class SSWOpenAnimator;


@interface SwipeNavigationController : UINavigationController <UIViewControllerTransitioningDelegate>
{
//	SloppySwiper *swiper_;
	UIPercentDrivenInteractiveTransition * interactionController_;
//	SSWOpenAnimator* openAnimator_;
	
}
//-(UIPanGestureRecognizer*)panGestureRecognizer;

@property (nonatomic, copy) void (^documentClosingBlock)(void);
@end


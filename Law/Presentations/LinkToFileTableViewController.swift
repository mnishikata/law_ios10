//
//  LinkToFileTableViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2014/09/03.
//  Copyright (c) 2014年 Catalystwo. All rights reserved.
//

import UIKit

class LinkToFileTableViewController: UITableViewController {
  
  var files: [DocItem] = []
  var h_file: String? = nil
  
  var completionHandler = { () -> Void in /* write completion handler here */ return }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let item = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(close))
    navigationItem.rightBarButtonItem = item
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    if h_file != nil {
      navigationItem.prompt = WLoc("Choose document to link")
    } else {
      navigationItem.prompt = WLoc("Tap to Unlink")
    }
  }
  
  func close() {
    dismiss(animated: true, completion: nil)
  }
  
  // MARK: - Table view data source
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    
    let array = FilePackage.shared.documents.collectWithCondition {
      $0.isLawEntity ? $0 : nil
    }
    files = array as! [DocItem]
    
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // #warning Incomplete method implementation.
		// Return the number of rows in the section.
		return files.count
	}
	
	override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
		return WLoc("LinkToFileTableViewControllerFooter")
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		var cell = tableView.dequeueReusableCell(withIdentifier: "Cell") 
		if cell == nil {
			cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
		}
		
		let dict = files[indexPath.row]
		
		cell?.textLabel!.text = dict.originalTitle
		cell?.textLabel!.font = UIFont.boldSystemFont(ofSize: 18)
		cell?.detailTextLabel?.text = nil
    
    // Look for the item
    
    let theItem: DocItem? = FilePackage.shared.documents.lookForWithCondition {
      $0.isLawEntity == true && $0.packageURL?.lastPathComponent == dict.packageURL?.lastPathComponent
    }
    
    if theItem != nil {
      let a_h_file = theItem!.h_file as NSString?
      
      if a_h_file != nil && a_h_file!.length > 0 {
				cell?.detailTextLabel?.text = WLoc("Linked to ") + (a_h_file! as String)
				cell?.detailTextLabel?.font = UIFont.systemFont(ofSize: 14)
			}
		}
		
		if h_file == nil {
			if cell?.detailTextLabel?.text == nil {
				
				cell?.textLabel!.textColor = UIColor.lightGray
				cell?.selectionStyle = .none
			}else {
				
				cell?.textLabel!.textColor = UIColor.black
				cell?.selectionStyle = .default
				
			}
		}
		
		// Configure the cell...
		
		return cell!
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		let dict = files[indexPath.row]
		
		if h_file == nil {
			
			dict.h_file = nil
			FilePackage.shared.saveIndex( saveToICloud: true )
			tableView.reloadRows(at: [indexPath], with: .automatic)
			return
		}
		
		// Remove existing h_file
		
		for anItem in files {
			if anItem.h_file == h_file {
				anItem.h_file = nil
			}
		}
		
		// Add
		
		// Look for the item
		dict.h_file = h_file
		FilePackage.shared.saveIndex( saveToICloud: true )
		
		completionHandler()
		dismiss(animated: true, completion: nil)
	}
	

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}

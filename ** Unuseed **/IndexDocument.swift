//
//  IndexDocument.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 2/04/15.
//  Copyright (c) 2015 Catalystwo. All rights reserved.
//

import Foundation

class IndexDocument : UIDocument {
	
	var myDocumentItems_:NSMutableDictionary? = nil
	
	// Writing
	override func contentsForType( typeName: String,
		error outError: NSErrorPointer) -> AnyObject? {
			
			if myDocumentItems_ == nil {
			
				return nil
			}
			
			let data = NSPropertyListSerialization.dataFromPropertyList( myDocumentItems_!, format: .XMLFormat_v1_0, errorDescription: nil) as NSData?

			if data == nil { return nil }

			return data
			
	}
	
	// Reading
	override func loadFromContents( contents: AnyObject,
		ofType typeName: String,
		error outError: NSErrorPointer) -> Bool {
			
			var item = NSMutableDictionary()
			item.setObject(NSMutableArray(), forKey:"items")

			let plist = NSPropertyListSerialization.propertyListFromData(contents as NSData,
				mutabilityOption: .MutableContainersAndLeaves, format: nil, errorDescription: nil) as NSMutableDictionary?
			
			if plist != nil { item = plist! }
			else { return false }
			
			item.upgradeToContainMD5()
			
			myDocumentItems_ = item
			
			return true
	}
	
	func documentItems() -> NSMutableDictionary? {
		
		return myDocumentItems_
	}
	
//	func lookForPackageName(packageName:String) -> NSMutableDictionary? {
//
//		return myDocumentItems_?.lookForPackageName(packageName)
//	}
//	
//	func containsFileEntry(entry:FileEntry) -> NSMutableDictionary? {
//		return myDocumentItems_?.containsFileEntry(entry)
//
//	}
	
}
//
//  DCModalSegue.h
//  DCSemiModalSegue
//
//  Created by zeta on 13/3/1.
//  Copyright (c) 2013年 zeta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCModalViewController : UIViewController

@property (nonatomic, strong) UIImageView * screenshot;
@property (nonatomic, strong) UIViewController* destinationController;
@property (nonatomic, strong) UIViewController* sourceController;
@property (nonatomic, copy) void (^ __nullable completion)(void);

@end

@interface UIViewController (DCModalSegue)
- (void)performOn:(UIViewController*)sourceController to:(UIViewController*)destinationController completion:(void (^ __nullable)(void))completion;
@end
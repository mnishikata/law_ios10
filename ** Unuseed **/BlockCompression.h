/*
    Copyright (C) 2015 Apple Inc. All Rights Reserved.
    See LICENSE.txt for this sample’s licensing information
    
    Abstract:
    Provides code that performs block compression.
*/

#ifndef BlockCompression_h
#define BlockCompression_h

#include "compression.h"

float doBlockCompression(FILE* fi, FILE* fo, compression_algorithm algorithm);

#endif

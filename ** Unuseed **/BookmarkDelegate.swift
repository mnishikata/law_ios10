//
//  BookmarkDelegate.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 3/04/15.
//  Copyright (c) 2015 Catalystwo. All rights reserved.
//

import Foundation


protocol BookmarkDelegate : class {
	
	func reload()
	func addBookmark(entry:FileEntry, bookmark:(title:String, encodedURL:String))
	
	
	func addHighlight( entry:FileEntry, dictionary:DocItem)
	func allHighlights(entry:FileEntry) -> [DocItem]?
	func removeHighlight( entry:FileEntry, uuid:String)
	func updateHighlight( )

	func allNotes(entry:FileEntry) -> [DocItem]?
	func notes(entry:FileEntry, bookmark:( title:String, encodedURL:String)) -> String?
	func setNotes( text:String!, entry:FileEntry, bookmark:(title:String, encodedURL:String), clickedURL:NSURL)
	func deleteNotes( entry:FileEntry, bookmark:(title:String, encodedURL:String))

}


//
//  JumpOnPhoneViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 13/05/15.
//  Copyright (c) 2015 Catalystwo. All rights reserved.
//

import UIKit

class JumpOnPhoneViewController : UIViewController,  UITextFieldDelegate, APNumberPadDelegate, UIPickerViewDataSource, UIPickerViewDelegate{

	
	@IBOutlet weak var helpLabel: UILabel?
	@IBOutlet weak var textField: UITextField?
	@IBOutlet weak var jumpLabel: UILabel?
	
	@IBOutlet weak var tabPickerView: UIPickerView!

	
	
	var cancelBlock: ((swtichToFind:Bool)->())? = nil
	var jumpAction: ((text:String!, tabIdentifier:String?)->Void)? = nil

	var inputtedString:String = ""
	var showTab_:Bool = true
	let theWidth_:CGFloat = 280

	
	required init?(coder aDecoder: NSCoder)
	{
		super.init(coder: aDecoder)
	}
	
	
	init(showTab:Bool)
	{
		showTab_ = showTab
		super.init(nibName: "JumpOnPhoneViewController", bundle: nil)
		
	}
	
	override func loadView() {
		super.loadView()
		
		print(self.traitCollection.userInterfaceIdiom)
	}
	
	override var preferredContentSize: CGSize {
		
		set {}
		get {
			
			var identifiers = all
			
			if identifiers.contains(current!) == false {
				identifiers.insert(current!, atIndex: 0)
			}

			
	
			if showTab_ == false || identifiers.count <= 1 { return CGSizeMake(theWidth_,90) }
			
			return CGSizeMake(theWidth_,210) // 150 for short version
		}
		
	}
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
	
		jumpLabel?.text = WLoc("Jump")
		textField?.placeholder = WLoc("Jump to article")
		
		// InputView
		
		let className:NSString? = nil
		
		let inputView = APNumberPad(delegate: self, numberPadStyleClass: className as String!, iPadStyle:false)
		
		textField!.inputView = inputView
		
		
		
		
		
		var identifiers = all
		
		if identifiers.contains(current!) == false {
			identifiers.insert(current!, atIndex: 0)
		}

		
		if showTab_ == false || identifiers.count <= 1 {
			tabPickerView.hidden = true
		}else {
			helpLabel?.hidden = true
			
			if current != nil {
				if let row = identifiers.indexOf(current!) {
					tabPickerView.selectRow(row, inComponent: 0, animated: false)
				}
			}
			
			
			// Setup icon on picker

			let icon = UIImageView(frame: CGRectMake(0,(tabPickerView.bounds.size.height - 28 )/2,28,28))
			icon.image = UIImage(named:"TabSmallIcon")
			icon.autoresizingMask = [.FlexibleRightMargin ]
			tabPickerView.addSubview(icon)
			
		}
		

	}
	
	override func viewDidAppear(animated:Bool) {
		
		super.viewDidAppear( animated )
		

	}


	
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	
	func insertText(text: String!) {
		
		inputtedString = inputtedString + text
		textField?.text = inputtedString
	}
	
	func jumpTapped() //aka jump 
	{
		var tabIdentifier:String? = nil
		
		var identifiers = all
		
		if identifiers.contains(current!) == false {
			identifiers.insert(current!, atIndex: 0)
		}
		
		if showTab_ == false || identifiers.count <= 1 {
			
			tabIdentifier = nil
			
		}else {
			
			tabIdentifier = identifiers[tabPickerView.selectedRowInComponent(0)]
			if tabIdentifier == current { tabIdentifier = nil }
		}
		
		
		jumpAction?(text: inputtedString, tabIdentifier: tabIdentifier)
	}
	
	
	@IBAction func close(sender: AnyObject) {
		
		cancelBlock?(swtichToFind:false)

	}
	
	func dismissKeyboard() //aka jump
	{
		cancelBlock?(swtichToFind:false)

	}
	
	func findTapped() //aka jump
	{
		cancelBlock?(swtichToFind:true)

	}
	
	func deleteBackward() //aka jump
	{
		if inputtedString.isEmpty == true { return }
		
		inputtedString = String( inputtedString.characters.dropLast() )
		
		textField?.text = inputtedString
		updateUI()
	}
	
	func updateUI() {
		textField?.inputView?.setNeedsLayout()

	}
	
	func hasText() -> Bool {
		
		if inputtedString.isEmpty == false { return true }
		

		var identifiers = all
		
		if identifiers.contains(current!) == false {
			identifiers.insert(current!, atIndex: 0)
		}
		
		
		let tabIdentifier = identifiers[tabPickerView.selectedRowInComponent(0)]

		if tabIdentifier == current { return false }
		
		return true
	}
	
	func numberPadTouchesBegan() {
		
	}
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

	
	func textFieldShouldClear(textField: UITextField) -> Bool {
		inputtedString = ""
		
		updateUI()
		
		return true
	}
	
	
	//MARK:- Tab Picker
	
	func pickerView( pickerView: UIPickerView,
		didSelectRow row: Int,
		inComponent component: Int) {
			
			updateUI()
			
	}
	
	func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		
		var identifiers = all
		
		if identifiers.contains(current!) == false {
			identifiers.insert(current!, atIndex: 0)
		}
		
		return identifiers.count + 1

	}
	
	
	func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
		
		return 1
	}
	
	func pickerView( pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
	
    var identifiers = all
    
    if identifiers.contains(current!) == false {
      identifiers.insert(current!, atIndex: 0)
    }


    let baseView = UIView( frame:CGRectMake(0,0,theWidth_,30) )
    baseView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]

    
    let label = UILabel(frame: CGRectMake(30,0,theWidth_-40,30))
    label.lineBreakMode = NSLineBreakMode.ByTruncatingMiddle
    label.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
    label.minimumScaleFactor = 0.8
    if #available(iOS 9.0, *) {
        label.allowsDefaultTighteningForTruncation = true
    } else {
        // Fallback on earlier versions
    }
    label.adjustsFontSizeToFitWidth = true
    label.numberOfLines = 0
    baseView.addSubview(label)
    
    let identifier = identifiers[row]
    let isSelected = ( identifier == current )
    var titleString = TabDataSource.shared.pathString(forTab: identifier, omitRoot: false)
				
    if isSelected == true {
      label.textColor = self.view.tintColor
      titleString = titleString + " ✔︎"
    }
    
    label.text = titleString

    return baseView
    
  }
  
}

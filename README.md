https://itunes.apple.com/us/app/法令ブラウザ-書き込み六法アプリ/id916069107?ls=1&mt=8


Including
Civil Law
Civil Procedure Law
Criminal Law
Criminal Procedure Law
Commercial Law
The Constitution of Japan
And any laws and ordinances can be downloaded from Ministry of Internal Affairs of Japan (*)

《Functionalities》
• Jump directly with an article number
• Find
• Organize documents and bookmarks in folders
• Link between documents
• Store bookmarks internally or external URL to link directly to this application
• Change fonts
• iCloud sync
• Upload data to Cloud. (requires iCloud account) ... If you want to add more laws other than eGov supplies, you can compose and slice text with regular expressions to build documents then upload to Cloud to share.
• Automatic update notifications... when eGov updates website, the app detects and notify you when you open the document.
• Automatic backup.  The application keeps daily backup files compressed in bz2 format up to 10 days. 



==TECHNICAL INFO===
This application includes 15,000 lines of ORIGINAL Swift codes including 30+ view controller subclasses.
Based on MVVM + DDD(CloudKit/iCloud/Local) architecture.
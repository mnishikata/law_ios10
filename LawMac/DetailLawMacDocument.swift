//
//  Document.swift
//  MyTestDocumentApp
//
//  Created by Masatoshi Nishikata on 6/08/15.
//  Copyright (c) 2015 Catalystwo. All rights reserved.
//

import Cocoa
import WebKit

class DetailLawMacDocument: NSDocument {
  
  @IBOutlet weak var webView: WebView!
  
  var detailItem: FileEntry? {
    didSet {
      // Update the view.
      
      
      startContentOffset = nil
      
      findString_ = nil
      
      loadFromFile( { (success:Bool) -> Void in
        
        if success == false {
          
          //...
        }
        
      } ) // Load from file end
    }
  }
  
  var linkDictionaries : [LinkDictionary] = []
  
  var startContentOffset:CGPoint? = nil
  var findString_:String? = nil
  
  var contentOffsetStack_:[CGFloat] = []
  var contentOffsetForwardStack_:[CGFloat] = []
  
  override var inViewingMode:Bool {
    return true
  }
  
  override init() {
    super.init()
    // Add your subclass-specific initialization here.
    
  }
  
  override func encodeRestorableStateWithCoder( coder: NSCoder) {
    super.encodeRestorableStateWithCoder( coder )
    
  }
  
  override func restoreStateWithCoder( coder: NSCoder) {
    super.restoreStateWithCoder( coder )
    
    let ( plist, _, _) = FilePackage.shared().loadPackage(at: self.fileURL, onlyPlist: true)
    
    let object:FileEntry = FileEntry( packageURL: self.fileURL!, anchor: nil, elementId:nil, plist:plist! )
    
    self.detailItem = object
  }
  
  override func windowControllerDidLoadNib(aController: NSWindowController) {
    super.windowControllerDidLoadNib(aController)
    // Add any code here that needs to be executed once the windowController has loaded the document's window.
    
    //		self.windowForSheet?.titleVisibility = NSWindowTitleVisibility.Hidden
    ////
    //		self.windowForSheet?.titlebarAppearsTransparent = true
    //
    //		(self.windowForSheet?.contentView as! NSView).wantsLayer = true
    
    
    WebView.registerURLSchemeAsLocal("itnl:")
  }
  
  override class func autosavesInPlace() -> Bool {
    return false
  }
  
  override var windowNibName: String? {
    // Returns the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this property and override -makeWindowControllers instead.
    return "DetailLawMacDocument"
  }
  
  override func dataOfType(typeName: String) throws -> NSData {
    throw NSError(domain: NSOSStatusErrorDomain, code: unimpErr, userInfo: nil)
  }
  
  
  
  override func readFromURL( absoluteURL: NSURL,
                             ofType typeName: String) throws {
    
    print("readFromURL \(absoluteURL) \(typeName)")
  }
  
  //MARK:-
  
  func loadFromFile(completionHandler: ((success:Bool) -> Void)!)   {
    
    let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    dispatch_async(queue, {
      
      let packageURL = self.detailItem?.packageURL
      
      let (plist, linkDictionaries, contentHtml): (NSDictionary?, [LinkDictionary]?, String?) = FilePackage.shared().loadPackage(at: self.detailItem?.packageURL, onlyPlist: false)
      
      if linkDictionaries == nil || contentHtml == nil {
        dispatch_async(dispatch_get_main_queue(), {
          completionHandler(success:false)
        })
        return
      }
      
      self.linkDictionaries = linkDictionaries!
      
      let allHighlights = FilePackage.shared().allHighlights(self.detailItem!)
      let allNotes = FilePackage.shared().allNotes(self.detailItem!)
      
      let contentHTMLWithNotes = contentHtml!.convertHTMLtoIncludeNotes(allHighlights: allHighlights, allNotes: allNotes, showTooltip: true)
      
      
      dispatch_async(dispatch_get_main_queue(), {
        
        if contentHTMLWithNotes != nil {
          let title = contentHTMLWithNotes!.titleOfHTML
          
          self.setDisplayName(title as String)
          self.windowForSheet?.title = title as String
          
          self.webView.mainFrame.loadHTMLString(contentHTMLWithNotes, baseURL: nil)
          completionHandler(success:true)
          
        }else {
          completionHandler(success:false)
          
        }
      })
    })
    
  }
  
  
  func addHistory() {
    
    //		contentOffsetForwardStack_.removeAll(keepCapacity: false)
    //		var num:CGFloat? = webView.mainFrame.frameView.documentView.enclosingScrollView?.
    //
    //		if contentOffsetStack_.count > 0 {
    //			if contentOffsetStack_.last == num { num = nil }
    //		}
    //
    //		if num != nil {
    //			contentOffsetStack_.append(num!)
    //		}
    //		updateToolbar(false, findTitle:nil)
    
  }
  
  func navigationHighlight( uuid:String ) {
    
    let root = FilePackage.shared().documents()
    
    let condition:((dictionary:DocItem)->Bool) = {
      (dictionary:DocItem)->Bool   in
      
      if dictionary.uuid == uuid  { return true }
      
      return false
    }
    
    
    let theDictionary = root.lookForWithCondition(condition)
    
    if theDictionary == nil { return /* unknown */ }
    
    let notesString = theDictionary?.notes as! String?
    
    
    print("\(notesString)")
  }
  
  
  func evaluateJavaScript( javaScriptString: String!,
                           completionHandler: ((AnyObject!,
    NSError!) -> Void)?) {
    
    let result = webView.stringByEvaluatingJavaScriptFromString( javaScriptString )
    
			 completionHandler?( result, nil )
    
    
  }
  
  //青リンクを踏んだとき
  override func webView( webView: WebView!,
                         decidePolicyForNewWindowAction actionInformation: [NSObject : AnyObject]!,
                         request request: NSURLRequest!,
                         newFrameName frameName: String!,
                         decisionListener listener: WebPolicyDecisionListener!) {
    
    //ここは無視してjavascriptで呼ぶ
    listener.ignore()
    
    let address = request.URL!.absoluteString as String!
    let script = "document.location.href = \"\(address)\""
    
    print("\(script)")
    evaluateJavaScript(script, completionHandler: nil)
    
    
    
  }
  
  //	override func webView( sender: WebView!,
  //		identifierForInitialRequest request: NSURLRequest!,
  //		fromDataSource dataSource: WebDataSource!) -> AnyObject! {
  //
  //
  //			println("identifierForInitialRequest")
  //
  //			return "X"
  //	}
  
  override func webView( sender: WebView!,
                         shouldPerformAction action: Selector,
                         fromSender fromObject: AnyObject!) -> Bool {
    
    print("shouldPerformAction")
    
    return false
  }
  
  override func webView( webView: WebView!,
                         decidePolicyForNavigationAction actionInformation: [NSObject : AnyObject]!,
                         request request: NSURLRequest!,
                         frame frame: WebFrame!,
                         decisionListener listener: WebPolicyDecisionListener!) {
    
    let scheme = request.URL!.scheme
    let path:NSString? = request.URL!.path
    
    
    
    
    
    if "about:blank" == request.URL!.absoluteString { listener.use(); return }
    
    
    
    
    
    if scheme == "intl" {
      
      listener.ignore()
      
      addHistory()
      
      var specifier = request.URL!.resourceSpecifier as NSString?
      
      if specifier == nil { return  }
      
      if specifier!.hasPrefix("%23") {
        
        specifier = specifier?.substringFromIndex(3)
      }
      
      
      let script = "document.location.href = \"#\(specifier!)\""
      
      print("\(script)")
      evaluateJavaScript(script, completionHandler: nil)
      
      return
    }
    
    if scheme == "comcatalystwolaws" {
      
      listener.ignore()
      
      return
      
    }
    
    
    if scheme == "highlight" {
      
      listener.ignore()
      
      if let specifier = request.URL?.resourceSpecifier {
        
        navigationHighlight( specifier )
      }
      
      return
      
    }
    
    if scheme == "notes" {
      
      listener.ignore()
      
      return
      
    }
    
    
    if path?.hasPrefix("/cgi-bin") == true  {
      
      listener.ignore()
      
      return
      
    }
    
    if scheme == "applewebdata" {
      
      listener.use()
      
      return
    }
  }
  
  
}



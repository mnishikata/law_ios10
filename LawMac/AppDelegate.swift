//
//  AppDelegate.swift
//  LawMac
//
//  Created by Masatoshi Nishikata on 2015/01/24.
//  Copyright (c) 2015年 Catalystwo. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

	@IBOutlet weak var window: NSWindow!


	var outlineWindowController: OutlineWindowController!
	
	
	@IBAction func openBrowser(sender: AnyObject) {
		
		self.outlineWindowController.showWindow(nil)

	}
	
	func applicationDidFinishLaunching(aNotification: NSNotification) {
		
		
		// Insert code here to initialize your application
		
		
		// Setup 
		
		let ud = NSUserDefaults.standardUserDefaults()
		//install
		FilePackage.shared().installSampleFiles()

		ud.setBool(true, forKey:"iCloudSync")
		ud.synchronize()

		
		if ud.objectForKey("Disclaimer") == nil {
			
			let alert = NSAlert()
			alert.messageText = WLoc("Disclaimer")
			alert.informativeText = WLoc("DisclaimerMessage")
				alert.alertStyle = NSAlertStyle.InformationalAlertStyle
			alert.addButtonWithTitle(WLoc("Agree"))
			alert.runModal()

			ud.setObject("Agree", forKey: "Disclaimer")
			
		}
		

		// Load Data
		
		
		let conflictHandler:ICloudConflictNotificationHandler = { ( allVersions:[AnyObject], decisionHandler:ICloudConflictDecisionHandler) in

			print("Handle Conflict here")
			
		}
		
		func closure() -> Void {
			print("Did Load")
			
			
			if self.outlineWindowController == nil {
				
				self.outlineWindowController = OutlineWindowController(windowNibName: "OutlineWindowController")
				self.outlineWindowController.showWindow(nil)
				
			}

			outlineWindowController.reload()

		
		}

		FilePackage.shared().loadIndex( conflictHandler, updateHandler:closure )

	}

	func applicationWillTerminate(aNotification: NSNotification) {
		// Insert code here to tear down your application
	}


}


//
//  AnimationImageView.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 12/08/15.
//  Copyright (c) 2015 Catalystwo. All rights reserved.
//

import Cocoa

class AnimationImageView: NSView {
	
	var animationImages: [NSImage]? = nil
	var animationDuration: Double = 1.0
	var _timer: NSTimer? = nil
	var _animationCount: Int = 0
	var image: NSImage? = nil {
		didSet {
			animating = false
			_timer?.invalidate()
			_timer = nil
		}
	}
	var animating: Bool = false
	
    override func drawRect(dirtyRect: NSRect) {
        super.drawRect(dirtyRect)
		var imageToDraw = image
		
		if animating == true {
			imageToDraw = animationImages![_animationCount]
		}
		imageToDraw?.drawInRect(self.bounds)
	}
	
	func startAnimating() {

    if animationImages == nil || animationImages!.isEmpty { return }
		animating = true
		let time = animationDuration / Double((animationImages!).count)
		_timer = NSTimer.scheduledTimerWithTimeInterval( time, target: self, selector: Selector("timerFired"), userInfo: nil, repeats: true)
	}
	
	func timerFired() {
		self.display()
		
		_animationCount++
		if (animationImages!).count <= _animationCount {
			_animationCount = 0
		}
	}
}

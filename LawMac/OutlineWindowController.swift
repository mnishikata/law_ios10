//
//  OutlineWindowController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 6/08/15.
//  Copyright (c) 2015 Catalystwo. All rights reserved.
//

import Cocoa

class OutlineWindowController: NSWindowController, NSOutlineViewDataSource, NSOutlineViewDelegate {

	@IBOutlet weak var outlineView: NSOutlineView!
	

	var downloadingPackageNames:[String] = []

	func reload() {
		self.outlineView.reloadData()
	}

    override func windowDidLoad() {
        super.windowDidLoad()

		FilePackage.shared().bookmarkingDidFinishHandler = { self.outlineView.reloadData() }

		
		
		///theTableColumn
		
//		let tableColumn = outlineView.tableColumnWithIdentifier("theTableColumn")
//		let imageAndTextCell:ImageAndTextCell = ImageAndTextCell(textCell:"")
//		imageAndTextCell.editable = true
//		tableColumn!.dataCell = imageAndTextCell

		
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
		outlineView.reloadData()
		
		
    }

	
	func outlineView( outlineView: NSOutlineView,
		numberOfChildrenOfItem item: AnyObject?) -> Int {
			
			
			if item == nil {
				if FilePackage.shared().documents().items != nil { return FilePackage.shared().documents().items!.count }
				else { return 0 }
			}
			
			let items = (item as! NSMutableDictionary).items
			
			
			if items != nil { return items!.count }
			
			return 0
			
	}
	
	func outlineView(outlineView: NSOutlineView, viewForTableColumn tableColumn: NSTableColumn?, item: AnyObject) -> NSView? {
		
//		let cell = outlineView.makeViewWithIdentifier("DataCell", owner:self) as! NSTableCellView?

		let dict = item as! NSMutableDictionary

		let baseView = NSView(frame: CGRectMake(0,0,320,30))
		baseView.autoresizingMask = [NSAutoresizingMaskOptions.ViewWidthSizable, .ViewHeightSizable]
		let label = NSTextField(frame: CGRectMake(0,0,320,30))
		label.autoresizingMask = [.ViewWidthSizable, .ViewHeightSizable]
		label.lineBreakMode = .ByTruncatingMiddle
		label.bezeled = false
		label.backgroundColor = NSColor.clearColor()
		label.editable = false
		
		baseView.addSubview( label )
		
		
		
		let title = dict.title
		
		var font = NSFont.boldSystemFontOfSize(NSFont.systemFontSize())
		
		if (item as! NSMutableDictionary).anchor != nil {
			font = NSFont.userFontOfSize(NSFont.systemFontSize())!
			
		}
		
		
		var textColor = NSColor.blackColor()
		let attributes = NSMutableDictionary()
		// Small Font for Bookmark
		if dict.anchor != nil {
			font = NSFont.userFontOfSize(NSFont.systemFontSize())!
		}
		
		
		if dict.highlight == true {
			font = NSFont.userFontOfSize(NSFont.systemFontSize())!
			
			let color = dict.color
			
			if color != nil {
				
				if color! == .Underline {
					attributes.setObject( NSColor.redColor(), forKey: NSUnderlineColorAttributeName  )
					attributes.setObject( NSUnderlineStyle.StyleThick, forKey: NSUnderlineStyleAttributeName  )
					
				}else {
					attributes.setObject( color!.color(), forKey: NSBackgroundColorAttributeName  )
				}
			}
			
		}
		
		
//		// White text for Dark mode
//		if darkMode == true { textColor = UIColor.whiteColor() }
		
		attributes.setObject( font, forKey: NSFontAttributeName  )
		attributes.setObject( textColor, forKey: NSForegroundColorAttributeName  )
		
		let attributedText = NSAttributedString(string:title as String, attributes:attributes as [NSObject : AnyObject])

		label.attributedStringValue = attributedText

		
		// Download button
				
		if dict.packageURLExistsFile == false {
			
			label.frame = CGRectMake(0,0,320-30,30)
			
			let imageView = AnimationImageView(frame: CGRectMake(320-30, 0, 30, 30) )
			
			imageView.autoresizingMask = NSAutoresizingMaskOptions.ViewMinXMargin
			baseView.addSubview( imageView )
			
			let md5 = dict.md5
			let cloud = dict.cloud
			
			
			
			if dict.packageName != nil && DownloadState.Downloading == cloud && downloadingPackageNames.contains( dict.packageName! ) == true  {
				
				// Loading animation
				let images = loadingImages( NSColor(red: 0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1))
				imageView.animationImages = images
				imageView.animationDuration = 1.0
				imageView.startAnimating()
				
				
			}else {
				
				// "Download from Cloud" Image
				let image = colorizedImage( NSImage(named: "LawDownload")!, withTintColor:NSColor(red: 0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1), alpha:1.0)
				
				imageView.image = image
				
			}
			
			
		}
		
		
		return baseView

	}
	
	func outlineView( outlineView: NSOutlineView,
		child index: Int,
		ofItem item: AnyObject?) -> AnyObject {
			
			if item == nil {
				
				return FilePackage.shared().documents().items![index]
			}
			let items = (item as! NSMutableDictionary).items
			if items != nil { return items![index] }
			return NSNull()
	}


	func outlineView( outlineView: NSOutlineView,
		isItemExpandable item: AnyObject) -> Bool {
			
			let dict = item as! NSMutableDictionary
			if dict.items == nil || dict.items!.count == 0 { return false }
			return true
	}
	
//	func outlineView( outlineView: NSOutlineView,
//		objectValueForTableColumn tableColumn: NSTableColumn?,
//		byItem item: AnyObject?) -> AnyObject? {
//			
//			let title = (item as! NSMutableDictionary).title
//
//			return title
//	}
	


//	func outlineView(outlineView: NSOutlineView, willDisplayCell cell: AnyObject, forTableColumn tableColumn: NSTableColumn?, item: AnyObject) {
//		
//		
//		let theCell = cell as! ImageAndTextCell
//		
//		theCell.title = "test"
//	}
	
	func outlineView(outlineView: NSOutlineView, itemForPersistentObject object: AnyObject) -> AnyObject! {
		
		let uuid = object as! String
		
		let condition:((dictionary:NSMutableDictionary)->Bool) = {
			(dictionary:NSMutableDictionary)->Bool   in
			
			if dictionary.uuid == uuid { return true }
			
			return false
		}
		
		let theDictionary = FilePackage.shared().documents().lookForWithCondition(condition)

		return theDictionary
	}
	
	func outlineView(outlineView: NSOutlineView, persistentObjectForItem item: AnyObject?) -> AnyObject! {
		
		let dict = item as! NSMutableDictionary

		return dict.uuid
	}

	
	//MARK:- 
	
	func outlineView( outlineView: NSOutlineView,
		shouldEditTableColumn tableColumn: NSTableColumn?,
		item: AnyObject) -> Bool {
			
			return false
	}
	
	func outlineViewSelectionDidChange(notification: NSNotification) {
		
		let item: AnyObject? = outlineView.itemAtRow(outlineView.selectedRow)
		if item == nil { return }
		
		let dict = item as! NSMutableDictionary
		
		let editing = false /* outlineView.editing */
		
		// Folder
		if dict.folder == true && editing == false {
			if dict.items != nil && dict.items!.count > 0 {
				if dict.expanded {
					outlineView.collapseItem(item);
				} else{
					outlineView.expandItem(item);
				}
			}
			
			return
		}
		
		
		// Editing
		if editing == true {
			
//			
//			
//			// Notes
//			
//			if dict["notes"] != nil {
//				
//				//				let dictionary = NSDictionary(object: dict.title, forKey: "title")
//				//
//				//				let object:FileEntry = FileEntry( packageURL: dict.packageURL!, anchor: dict.anchor, plist:dictionary )
//				//				let anchor = dict.anchor
//				//				let bookmarkingURL:(title:String, encodedURL:String) = ( title: "dummy", encodedURL: anchor!)
//				//
//				//
//				//				let controller = NotesViewController(nibName:"NotesViewController" , bundle:nil)
//				//				controller.bookmarkDelegate = self
//				//				controller.fileEntry = object
//				//				controller.bookmark = bookmarkingURL
//				//				controller.title = dict["title"] as String?
//				//
//				//
//				//				let nav = UINavigationController(rootViewController: controller)
//				//				controller.modalPresentationStyle = .FormSheet
//				//				self.presentViewController( nav, animated:true, completion:nil)
//				
//				
//				return
//			}
//			
//			if dict["hilt"] != nil {
//				return
//			}
//			
//			// Others
//			
//			renamingFolder = dict
//			let alert = UIAlertView(title: WLoc("Rename"), message: "", delegate: self, cancelButtonTitle: WLoc("Cancel"), otherButtonTitles: WLoc("Rename"))
//			
//			alert.alertViewStyle = UIAlertViewStyle.PlainTextInput
//			let field = alert.textFieldAtIndex(0)
//			field!.text = dict.originalTitle as String
//			field!.clearButtonMode = .WhileEditing
//			alert.tag = 6
//			field!.keyboardType = .Default
//			
//			alert.show()
			
			return
		}
		
		
		
		// Download from CloudKit
		let exists = NSFileManager.defaultManager().fileExistsAtPath(dict.packageURL!.path!)
		
		
		if exists == false {
			
			if dict.packageName == nil {
				outlineView.reloadData()
				return
			}
			
			// Start Download when Online
			
			if GCNetworkReachability.reachabilityForInternetConnection().isReachable() == false {

				return
			}
			
			// If it is currently downloading
			
			//			if .Downloading == dict.cloud {
			//				self.tableView?.reloadData()
			//				return
			//			}
			
			if downloadingPackageNames.contains( dict.packageName! ) {
				outlineView.reloadData()
				return
			}
			
			// Start download
			
			
			dict.cloud = .Downloading
			
			downloadingPackageNames.append( dict.packagName! )
			
			outlineView.reloadData()
			
			
			
			CloudKitDomain.shared.downloadLaw( dict,  completion: { (success:Bool) -> Void in
				
				if let pacakgeIndex = self.downloadingPackageNames.indexOf( dict.packageName! )
				{
					self.downloadingPackageNames.removeAtIndex( pacakgeIndex )
				}
				
				
				if success == true {
					dict.cloud = .Installed
					
				} else {
					dict.cloud = .InCloud
				}
				
				
				print("download finished \(success) for \(dict)")
				self.outlineView.reloadData()
				
				
				// Failed in CloudKit... Download from eGov
				if success == false {
					
					let sourceURLString = dict.sourceURLString
					if sourceURLString?.hasPrefix("http://law.e-gov.go.jp/cgi-bin/") == true {
						
						let plist = NSDictionary(object: "", forKey: "title")
						let url = NSURL(string: sourceURLString! as String)
						
						let fileEntry:FileEntry = FileEntry( packageURL: url!, anchor:nil, elementId:nil, plist: plist );
						
						self.openWithDetailItem( fileEntry, startJump: false, search: nil )
						
					}
					
				} else {
					
					
					let dictionary = NSDictionary(object: dict.originalTitle, forKey: "title")
					
					let object:FileEntry = FileEntry( packageURL: dict.packageURL!, anchor: dict.anchor, elementId:nil, plist:dictionary )
					
					self.openWithDetailItem( object, startJump: true, search: nil )
					
				}
				
			})
			
			
			return
		}
		
		// Open
		
		let detailItemTitle = dict.originalTitle
		
		let dictionary = NSDictionary(object: detailItemTitle, forKey: "title")
		
		var myId:String? = nil
		
		if dict.highlight == true {
			myId = dict.uuid
		}
		
		
		let object:FileEntry = FileEntry( packageURL: dict.packageURL!, anchor: dict.anchor, elementId:myId, plist:dictionary )
		
		openWithDetailItem( object, startJump: true, search: nil )

	}
		
	func openWithDetailItem( item: FileEntry, startJump:Bool, search: String? ) {
		
		
		print("open detail item here")
		
		
//		// Prepare view controller
		var error:NSError? = nil
		let doc: AnyObject? = NSDocumentController.sharedDocumentController().makeDocumentWithContentsOfURL(item.packageURL, ofType: "com.catalystwo.Laws.packagebundle", error: &error)
		
		print("\(doc) - \(error)")
		
		let theDoc = doc as! DetailLawMacDocument
//		doc.detailItem = item
		NSDocumentController.sharedDocumentController().addDocument(theDoc)

		theDoc.makeWindowControllers()

		
		theDoc.detailItem = item
		
		theDoc.showWindows()

//
//		(NSDocumentController.sharedDocumentController() as! NSDocumentController).openDocumentWithContentsOfURL(item.packageURL, display: true) { (doc:NSDocument!, documentWasAlreadyOpen:Bool, error:NSError!) -> Void in
//			
//			(doc as! DetailLawMacDocument).detailItem = item
//			(doc as! DetailLawMacDocument).bookmarkingDelegate = self
//			println("\(doc) - \(error)")
//			
//		}

		
		
		
//		
//		controller.bookmarkingDelegate = self
//		controller.startJump = startJump
//		controller.startSearch = search
//		controller.detailItem = item
//		
//		
//		detailNavigationController?.setViewControllers( [controller], animated:false )
//		
//		self.splitViewController?.showDetailViewController(detailNavigationController, sender: self)
//		// Hide master
//		
//		if self.traitCollection.userInterfaceIdiom == .Pad {
//			let buttonItem = self.splitViewController?.displayModeButtonItem()
//			UIApplication.sharedApplication().sendAction(buttonItem!.action, to: buttonItem!.target, from: nil, forEvent: nil)
//		}
		
	}
	
	
	
//	// MARK:- Bookmark Delegate
//	
//	
//	func addBookmark(entry:FileEntry, bookmark:(title:String, encodedURL:String)) {
//		
//		self.reload()
//		let root = FilePackage.shared().documents()
//		
//		
//		var condition:((dictionary:NSMutableDictionary)->Bool) = {
//			(dictionary:NSMutableDictionary)->Bool   in
//			
//			if dictionary.anchor == nil && dictionary.packageName == entry.packageURL.lastPathComponent! { return true }
//			
//			return false
//		}
//		
//		
//		let theParentItem = root.lookForWithCondition(condition)
//		
//		
//		let dict = NSMutableDictionary()
//		var originalTitle = (theParentItem?.title as NSString?)
//		
//		if originalTitle == nil { originalTitle = "Untitled" }
//		
//		if originalTitle!.length > 7 { originalTitle = originalTitle!.substringToIndex(7) + "…" }
//		
//		dict.title = (originalTitle! as String) + bookmark.title
//		dict.originalTitle = (entry.plist["title"] as! String)
//		dict.anchor = bookmark.encodedURL
//		dict.packageName = entry.packageURL.lastPathComponent!
//		dict.setObject( shortUUIDString(), forKey:"uuid")
//		
//		
//		
//		if theParentItem != nil { theParentItem!.addItem(dict) }
//		else { FilePackage.shared().documents().addItem(dict) }
//		
//		FilePackage.shared().saveIndex( saveToICloud: true )
//		
//		outlineView.reloadData()
//		
//	}
//	
//	func setNotes( text:String!, entry:FileEntry, bookmark:(title:String, encodedURL:String), clickedURL:NSURL) {
//		
//		self.reload()
//		let root = FilePackage.shared().documents()
//		
//		
//		// 1. Already Exists
//		
//		
//		var condition:((dictionary:NSMutableDictionary)->Bool) = {
//			(dictionary:NSMutableDictionary)->Bool   in
//			
//			
//			if dictionary.anchor == bookmark.encodedURL && dictionary.packageName == entry.packageURL.lastPathComponent! { return true }
//			
//			return false
//		}
//		
//		
//		let theDictionary = root.lookForWithCondition(condition)
//		
//		if theDictionary != nil {
//			
//			theDictionary!.setObject( text, forKey:"notes")
//			theDictionary!.setObject( clickedURL.absoluteString!, forKey:"href")
//			
//			
//			FilePackage.shared().saveIndex( saveToICloud: true )
//			
//			println("Update Notes")
//			
//			return
//		}
//		
//		// 2. Add as new note
//		
//		condition = {
//			(dictionary:NSMutableDictionary)->Bool   in
//			
//			if dictionary.anchor == nil && dictionary.packageName == entry.packageURL.lastPathComponent! { return true }
//			
//			return false
//		}
//		
//		
//		let theParentItem = root.lookForWithCondition(condition)
//		
//		
//		let dict = NSMutableDictionary()
//		var originalTitle = (theParentItem?.title as NSString?)
//		
//		if originalTitle == nil { originalTitle = "Untitled" }
//		
//		if originalTitle!.length > 7 { originalTitle = originalTitle!.substringToIndex(7) + "…" }
//		
//		dict.title = bookmark.title
//		dict.originalTitle = originalTitle!
//		dict.anchor = bookmark.encodedURL
//		dict.packageName = entry.packageURL.lastPathComponent!
//		dict.setObject( shortUUIDString(), forKey:"uuid")
//		dict.setObject( text, forKey:"notes")
//		dict.setObject( clickedURL.absoluteString!, forKey:"href")
//		
//		
//		
//		// Look for the parent to add
//		condition = {
//			(dictionary:NSMutableDictionary)->Bool   in
//			
//			if dictionary.anchor == nil && dictionary.packageURL != nil && (dictionary.packageURL?.lastPathComponent == entry.packageURL.lastPathComponent ) { return true }
//			
//			return false
//		}
//		
//		
//		let parent:NSMutableDictionary? = root.lookForWithCondition( condition )
//		
//		if parent != nil { parent!.addItem(dict) }
//		else { root.addItem(dict) }
//		
//		println("Added Notes")
//		
//		FilePackage.shared().saveIndex( saveToICloud: true )
//		outlineView.reloadData()
//		
//		
//	}
//	
//	func removeHighlight( entry:FileEntry, uuid:String) {
//		
//		self.reload()
//		
//		let root = FilePackage.shared().documents()
//		
//		
//		// 1. Already Exists
//		
//		
//		var condition:((dictionary:NSMutableDictionary)->Bool) = {
//			(dictionary:NSMutableDictionary)->Bool   in
//			
//			if dictionary["uuid"] as! String? == uuid  { return true }
//			
//			return false
//		}
//		
//		
//		let theDictionary = root.lookForWithCondition(condition)
//		
//		if theDictionary != nil {
//			
//			root.removeAndDeleteForItem( theDictionary! )
//			FilePackage.shared().saveIndex( saveToICloud: true )
//			outlineView.reloadData()
//			
//		}
//		
//	}
//	
//	func updateHighlight( ) {
//		
//		FilePackage.shared().saveIndex( saveToICloud: true )
//		outlineView.reloadData()
//		
//	}
//	
//	func addHighlight( entry:FileEntry, dictionary:NSDictionary) {
//		
//		self.reload()
//		let root = FilePackage.shared().documents()
//		
//		// Look for the parent to add
//		let condition = {
//			(thisDictionary:NSMutableDictionary)->Bool   in
//			
//			if thisDictionary.anchor == nil && thisDictionary.packageURL != nil && (thisDictionary.packageURL?.lastPathComponent == entry.packageURL.lastPathComponent ) { return true }
//			
//			return false
//		}
//		
//		
//		let parent:NSMutableDictionary? = root.lookForWithCondition( condition )
//		
//		
//		var originalTitle = (parent?.title as NSString?)
//		
//		if originalTitle == nil { originalTitle = "Untitled" }
//		
//		if originalTitle!.length > 7 { originalTitle = originalTitle!.substringToIndex(7) + "…" }
//		
//		
//		let mdict = NSMutableDictionary(dictionary:dictionary)
//		if parent != nil { parent!.addItem(mdict) }
//		else { root.addItem(mdict) }
//		
//		mdict.originalTitle = originalTitle!
//		mdict.setObject( true, forKey: "hilt")
//		mdict.packageName = entry.packageURL.lastPathComponent!
//		
//		println("Added Notes")
//		
//		
//		
//		FilePackage.shared().saveIndex( saveToICloud: true )
//		outlineView.reloadData()
//		
//	}
//	
//	func deleteNotes( entry:FileEntry, bookmark:(title:String, encodedURL:String)) {
//		
//		self.reload()
//		
//		let root = FilePackage.shared().documents()
//		
//		
//		// 1. Already Exists
//		
//		
//		var condition:((dictionary:NSMutableDictionary)->Bool) = {
//			(dictionary:NSMutableDictionary)->Bool   in
//			
//			
//			if dictionary.anchor == bookmark.encodedURL && dictionary.packageName == entry.packageURL.lastPathComponent! { return true }
//			
//			return false
//		}
//		
//		
//		let theDictionary = root.lookForWithCondition(condition)
//		
//		if theDictionary != nil {
//			
//			root.removeAndDeleteForItem( theDictionary! )
//			FilePackage.shared().saveIndex( saveToICloud: true )
//			outlineView.reloadData()
//			
//		}
//		
//	}
//	
//	func notes(entry:FileEntry, bookmark:( title:String, encodedURL:String)) -> String? {
//		
//		let root:NSMutableDictionary = FilePackage.shared().documents() as NSMutableDictionary!
//		
//		var condition:((dictionary:NSMutableDictionary)->Bool) = {
//			(dictionary:NSMutableDictionary)->Bool   in
//			
//			if dictionary.anchor == bookmark.encodedURL && dictionary.packageName == entry.packageURL.lastPathComponent! { return true }
//			
//			return false
//		}
//		
//		
//		let theDictionary = root.lookForWithCondition(condition)
//		
//		return theDictionary?.objectForKey("notes") as! String?
//	}
//	
//	func allNotes(entry:FileEntry) -> [NSDictionary]? {
//		
//		let root:NSMutableDictionary = FilePackage.shared().documents() as NSMutableDictionary!
//		
//		var condition:((dictionary:NSMutableDictionary)->AnyObject?) = {
//			(dictionary:NSMutableDictionary)->AnyObject?   in
//			
//			if dictionary.anchor != nil &&
//				dictionary["notes"] != nil &&
//				dictionary.packageName == entry.packageURL.lastPathComponent! {
//					return dictionary
//			}
//			
//			return nil
//		}
//		
//		
//		let dictionaries = root.collectWithCondition(condition)
//		
//		return dictionaries as? [NSDictionary]
//	}
//	
//	func allHighlights(entry:FileEntry) -> [NSDictionary]? {
//		
//		let root:NSMutableDictionary = FilePackage.shared().documents() as NSMutableDictionary!
//		
//		var condition:((dictionary:NSMutableDictionary)->AnyObject?) = {
//			(dictionary:NSMutableDictionary)->AnyObject?   in
//			
//			if dictionary["hilt"] != nil &&
//				dictionary.packageName == entry.packageURL.lastPathComponent! {
//					return dictionary
//			}
//			
//			return nil
//		}
//		
//		
//		let dictionaries = root.collectWithCondition(condition)
//		
//		return dictionaries as? [NSDictionary]
//	}
//	
}

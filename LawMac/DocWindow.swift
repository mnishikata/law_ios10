//
//  DocWindow.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 6/08/15.
//  Copyright (c) 2015 Catalystwo. All rights reserved.
//

import Cocoa

class DocWindow: NSWindow {

	
	override init(contentRect: NSRect,
		styleMask windowStyle: Int,
		backing bufferingType: NSBackingStoreType,
		`defer` deferCreation: Bool) {
			
			super.init(contentRect: contentRect,
				styleMask: NSBorderlessWindowMask|NSClosableWindowMask|NSMiniaturizableWindowMask|NSResizableWindowMask|NSTexturedBackgroundWindowMask,
				backing : .Buffered,
				`defer` : true)
	}
	
	required init(coder:NSCoder) {
		super.init(contentRect: NSMakeRect(0,0,300,300),
			styleMask: NSBorderlessWindowMask|NSClosableWindowMask|NSMiniaturizableWindowMask|NSResizableWindowMask|NSTexturedBackgroundWindowMask,
			backing : .Buffered,
			`defer` : true)
	}
	
}

/*
    Copyright (C) 2016 Apple Inc. All Rights Reserved.
    See LICENSE.txt for this sample’s licensing information
    
    Abstract:
    This file contains the `WatchConnectivityManager` class and its delegate which encapsulate the `WatchConnectivity` behavior of the app.
*/

import WatchConnectivity

/**
The `WatchConnectivityManagerDelegate` protocol enables a `WatchConnectivityManager` object to notify another
object of changes to the activation state of the default `WCSession`. The receiver is provided with the
designator and color selected by the user to identify the watch associated with the default session.
*/
protocol WatchConnectivityManagerDelegate: class {
	func watchConnectivityManager(_ watchConnectivityManager: WatchConnectivityManager, updatedWithContext context: [String: Any])
	
	func watchConnectivityManager(_ watchConnectivityManager: WatchConnectivityManager, didReceiveMessage message: [String: Any], withReplyHandler reply: (([String : Any]) -> Void)? )
	
	@available(iOS 9.0, *)
	func watchConnectivityManager(_ watchConnectivityManager: WatchConnectivityManager, didReceiveFile file: WCSessionFile)
}

typealias FileAndMetadata = (fileURL: URL, metadata: [String : Any]? )


class WatchConnectivityManager: NSObject, WCSessionDelegate {
	// MARK: Static Properties
	
	static let shared = WatchConnectivityManager()
	
	// MARK: Properties
	
	weak var delegate: WatchConnectivityManagerDelegate?
//	var cancelOutstandingFileTransfers = false
	
	var sessionIsActive: Bool {
		if #available(iOS 9.3, *) {
			return WCSession.default().activationState == .activated
		}
		return false
	}
	
	var sendApplicationContext_: [String: Any]? = nil
	func sendApplicationContext(_ context: [String: Any]) {
		
		assert( Thread.isMainThread == true )
		
		if #available(iOS 9.3, *) {
			
			if WCSession.isSupported() == false { return }
			
			let defaultSession = WCSession.default()
			if sessionIsActive {
				do {
					
					try defaultSession.updateApplicationContext(context)
					print("* updateApplicationContext immediately")
					sendApplicationContext_ = nil
				}catch _ {
				}
			}else {
				sendApplicationContext_ = context
				
			}
		}
	}
	
	var transferFiles_ = [FileAndMetadata]()
	func transferFiles( _ fileAndMetadata: FileAndMetadata?  ) {
		
		assert( Thread.isMainThread == true )

		if #available(iOS 9.3, *) {
			print("* transferFiles \(fileAndMetadata?.fileURL.lastPathComponent)")
			let defaultSession = WCSession.default()
			if sessionIsActive {
				
				if fileAndMetadata != nil {
					transferFiles_.append(fileAndMetadata!)
				}
				
				let outstandingFiles = defaultSession.outstandingFileTransfers
				print("outstandingFiles \(outstandingFiles.count)")
				/*
				<WCSessionFileTransfer: 0x13c72d880, session file: <WCSessionFile: 0x13df117e0, identifier: 315093FA-8072-48B9-BE82-AC2CA1F707DE, file: /private/var/mobile/Containers/Data/Application/EC4D5853-A239-40EE-87BA-F6A8B4EB4F82/tmp/S34HO125.html.comcatalystwopackage, hasMetadata: YES>, transferring: NO>
				*/
				for file in transferFiles_ {
					let name = file.fileURL.lastPathComponent as String
          if outstandingFiles.filter( { (file: WCSessionFileTransfer) in
            
            let thisURL = file.file.fileURL as URL?
            if thisURL == nil { return false }
            return name == thisURL!.lastPathComponent
          
          } ).count > 0 {
						print("*** \(file.fileURL.lastPathComponent) is still transferring")
						continue
					}
					
					autoreleasepool {
						
						do {
							
              let (_,_, rawHTML) = FilePackage.shared.loadPackage(at: file.fileURL, onlyPlist: false, zoomScale: 1.0, rawHTML: true)
							let rawContentHtmlData = (rawHTML!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: true) as NSData?)?.deflate()
							
							let wrapper = try FileWrapper(url: file.fileURL, options: [])
							
							let name = FilePackage.Filename.rawContent
							wrapper.addRegularFile(withContents: rawContentHtmlData!, preferredFilename: name)
							wrapper.filename = name
							let fileToDelete = wrapper.fileWrappers![FilePackage.Filename.content]
							
							wrapper.removeFileWrapper(fileToDelete!)
							
							if let data = wrapper.serializedRepresentation {
								
								let tempPath = (NSTemporaryDirectory() as NSString).appendingPathComponent(file.fileURL.lastPathComponent)
								let url = URL(fileURLWithPath: tempPath)
								try data.write(to: url, options: [])
								
								let token: WCSessionFileTransfer = defaultSession.transferFile( url, metadata: file.metadata )
								print(token)
								
							}else {
								debugPrint("*** Failed: transferFiles")
							}
						}catch let error as NSError {
							debugPrint("*** Failed: \(error)")
							
						}
					}
				}
				
				transferFiles_.removeAll()
				
			}else if fileAndMetadata != nil {
				transferFiles_.append(fileAndMetadata!)
			}
		}
	}
	
	
	var sendMessages_: [()->Void] = []
	func sendMessage(_ message: [String : Any], replyHandler: (([String: Any]) -> Void)?, errorHandler: ((Error) -> Void)?) {
		
		assert( Thread.isMainThread == true )

		if #available(iOS 9.3, *) {
			
			if sessionIsActive {
				
				sendMessages_.append {
					print("* sendMessage_ Synchronously")
					
					let defaultSession = WCSession.default()
					defaultSession.sendMessage(message, replyHandler: replyHandler, errorHandler: errorHandler )
				}
				
				for message in sendMessages_ {
					message()
					print("* sendMessage_ immediately")
					
				}
				
				sendMessages_.removeAll()
				
			}else {
				sendMessages_.append {
					print("* sendMessage_ asynchronously")

					let defaultSession = WCSession.default()
					defaultSession.sendMessage(message, replyHandler: replyHandler, errorHandler: errorHandler )
				}
			}
		}
	}
	
	
	var transferUserInfo_ = [[String: Any]]()
	func transferUserInfo( _ userInfo: [String: Any]  ) {
		
		assert( Thread.isMainThread == true )
		
		if #available(iOS 9.3, *) {
			let defaultSession = WCSession.default()
			if sessionIsActive {
				
				let outstandingUserInfo: [WCSessionUserInfoTransfer]  = defaultSession.outstandingUserInfoTransfers
				print("outstandingFiles \(outstandingUserInfo.count)")
				/*
				<WCSessionFileTransfer: 0x13c72d880, session file: <WCSessionFile: 0x13df117e0, identifier: 315093FA-8072-48B9-BE82-AC2CA1F707DE, file: /private/var/mobile/Containers/Data/Application/EC4D5853-A239-40EE-87BA-F6A8B4EB4F82/tmp/S34HO125.html.comcatalystwopackage, hasMetadata: YES>, transferring: NO>
				*/
				for thisUserInfo in transferUserInfo_ {
					
					let filteredArray = outstandingUserInfo.filter( {  ($0.userInfo as NSDictionary) == (thisUserInfo as NSDictionary) } )
					if filteredArray.count > 0 {
						print("*** \(thisUserInfo) is still transferring")
						continue
					}
					
					let token = defaultSession.transferUserInfo(thisUserInfo)
					print(token)
					
				}
				
				transferUserInfo_.removeAll()
				
			}else  {
				transferUserInfo_.append(userInfo)
			}
		}
	}
	
	
	// MARK: Initialization
	
    private override init() {
        super.init()
    }
    
    // MARK: Convenience
	
	func configureDeviceDetailsWithApplicationContext(_ applicationContext: [String: Any]) {
		// Extract relevant values from the application context.
		
		// Inform the delegate.
		delegate?.watchConnectivityManager(self, updatedWithContext: applicationContext )
    }
    
    // MARK: WCSessionDelegate
	
	// iPhone -> Watch
	@available(iOS 9.0, *)
	func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String: Any]) {
		if #available(iOS 9.3, *) {
			print("session (in state: \(session.activationState.rawValue)) received application context \(applicationContext)")
			
			configureDeviceDetailsWithApplicationContext(applicationContext)

			
		} else {
			// Fallback on earlier versions
		}
        
		
        // NOTE: The guard is here as `watchDirectoryURL` is only available on iOS and this class is used on both platforms.
        #if os(iOS)
        print("session watch directory URL: \(session.watchDirectoryURL?.absoluteString)")
        #endif
    }
    
    // MARK: WCSessionDelegate - Asynchronous Activation
    // The next 3 methods are required in order to support asynchronous session activation; required for quick watch switching.
	
	// iPhone -> Watch
	@available(iOS 9.3, *)
	func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
		
        if let error = error {
            print("session activation failed with error: \(error.localizedDescription)")
            return
        }
		
			print("*** CONNECTED TO WATCH ***")
        print("session activated with state: \(activationState.rawValue)")
		
//		if cancelOutstandingFileTransfers {
//			let outstandingFiles = session.outstandingFileTransfers
		//			outstandingFiles.forEach{ $0.cancel() }
		//
		//		}
		//
		
		DispatchQueue.main.async {
			
			self.configureDeviceDetailsWithApplicationContext(session.receivedApplicationContext)
			
			if self.sendApplicationContext_ != nil {
				do {
					print("* sendApplicationContext_ asynchronously")
					
					try session.updateApplicationContext(self.sendApplicationContext_!)
					self.sendApplicationContext_ = nil
				}catch _ {
				}
			}
			
			for message in self.sendMessages_ {
				message()
				print("* sendMessage_ immediately")
			}
			
			self.transferFiles(nil)
			
			// NOTE: The guard is here as `watchDirectoryURL` is only available on iOS and this class is used on both platforms.
			#if os(iOS)
				print("session watch directory URL: \(session.watchDirectoryURL?.absoluteString)")
			#endif
			
		}
	}
	
	// Watch -> iPhone -> Reply
	@available(iOS 9.0, *)
	func session( _ session: WCSession,
	               didReceiveMessage message: [String: Any],
	                                 replyHandler: @escaping ([String: Any]) -> Void) {
		
		delegate?.watchConnectivityManager(self, didReceiveMessage: message, withReplyHandler: replyHandler  )

	}
	@available(iOS 9.0, *)
	func session( _ session: WCSession,
	              didReceiveMessage message: [String: Any]) {
		
		delegate?.watchConnectivityManager(self, didReceiveMessage: message, withReplyHandler: nil  )
		
	}
	
	 #if os(iOS)
	@available(iOS 9.0, *)
	func sessionDidBecomeInactive(_ session: WCSession) {
        /*
            The `sessionDidBecomeInactive(_:)` callback indicates sending has been disabled. If your iOS app 
            sends content to its Watch extension it will need to stop trying at this point. This sample 
            doesn’t send content to its Watch Extension so no action is required for this transition.
         */
		
		print("session did become inactive")
		
	}
	
	@available(iOS 9.0, *)
	func sessionDidDeactivate(_ session: WCSession) {
		print("session did deactivate")
		
		/*
		The `sessionDidDeactivate(_:)` callback indicates `WCSession` is finished delivering content to
		the iOS app. iOS apps that process content delivered from their Watch Extension should finish
		processing that content and call `activateSession()`. This sample immediately calls
		`activateSession()` as the data provided by the Watch Extension is handled immediately.
		*/
		
		WCSession.default().activate()
		
	}
	#endif
	
	@available(iOS 9.0, *)
	func session( _ session: WCSession,  didReceive file: WCSessionFile) {
		delegate?.watchConnectivityManager(self, didReceiveFile: file  )
		print("** WCSession didReceiveFile")

	}
	
	@available(iOS 9.0, *)
	func session( _ session: WCSession,
	               didFinish fileTransfer: WCSessionFileTransfer,
	                                      error: Error?) {
		print("** WCSession fileTransfer error \(error)")
		
	}
}

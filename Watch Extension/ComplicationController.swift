//
//  ComplicationController.swift
//  TestWatch WatchKit Extension
//
//  Created by Masatoshi Nishikata on 11/08/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import ClockKit


class ComplicationController: NSObject, CLKComplicationDataSource {
  
  // MARK: - Timeline Configuration
  
  func getSupportedTimeTravelDirections(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimeTravelDirections) -> Void) {
    handler([])
  }
  
  func getTimelineStartDate(for complication: CLKComplication, withHandler handler: @escaping (Date?) -> Void) {
    handler(Date())
  }
  
  func getTimelineEndDate(for complication: CLKComplication, withHandler handler: @escaping (Date?) -> Void) {
    handler(Date())
  }
  
  func getPrivacyBehavior(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationPrivacyBehavior) -> Void) {
    handler(.showOnLockScreen)
  }
  
  // MARK: - Timeline Population
  
  func getCurrentTimelineEntry(for complication: CLKComplication, withHandler handler: @escaping ((CLKComplicationTimelineEntry?) -> Void)) {
    // Call the handler with the current timeline entry
    
    if complication.family == .circularSmall {
      
      let modularTemplate = CLKComplicationTemplateCircularSmallSimpleImage()
      let image = UIImage(named:"Complication/Circular")!
      modularTemplate.imageProvider = CLKImageProvider(onePieceImage: image )
      
      let timelineEntry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: modularTemplate)
      
      handler(timelineEntry)
      
    }else if complication.family == .modularSmall {
      let modularTemplate = CLKComplicationTemplateModularSmallSimpleImage()
      let image = UIImage(named:"Complication/Modular")!
      modularTemplate.imageProvider = CLKImageProvider(onePieceImage: image )
      let timelineEntry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: modularTemplate)
      
      handler(timelineEntry)
      
    }else if complication.family == .utilitarianSmallFlat  {
      
      let modularTemplate = CLKComplicationTemplateUtilitarianSmallFlat()
      modularTemplate.textProvider = CLKSimpleTextProvider(text: WLoc("Laws"))
      let timelineEntry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: modularTemplate)
      handler(timelineEntry)
      
    } else if complication.family == .extraLarge {
      let modularTemplate = CLKComplicationTemplateExtraLargeSimpleText()
      modularTemplate.textProvider = CLKSimpleTextProvider(text: WLoc("Laws"))
      
      let timelineEntry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: modularTemplate)
      handler(timelineEntry)
      
    }else if complication.family == .utilitarianSmall  {
      
      let modularTemplate = CLKComplicationTemplateUtilitarianSmallSquare()
      let image = UIImage(named:"Complication/Utilitarian")!
      modularTemplate.imageProvider = CLKImageProvider(onePieceImage: image )
      
      let timelineEntry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: modularTemplate)
      
      handler(timelineEntry)
    }else		{
      handler(nil)
      
    }
    
    
  }
  
  func getTimelineEntries(for complication: CLKComplication, before date: Date, limit: Int, withHandler handler: @escaping (([CLKComplicationTimelineEntry]?) -> Void)) {
    // Call the handler with the timeline entries prior to the given date
    handler(nil)
  }
  
  func getTimelineEntries(for complication: CLKComplication, after date: Date, limit: Int, withHandler handler: @escaping (([CLKComplicationTimelineEntry]?) -> Void)) {
    // Call the handler with the timeline entries after to the given date
    handler(nil)
  }
  
  // MARK: - Placeholder Templates
  
  func getLocalizableSampleTemplate(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTemplate?) -> Void) {
    // This method will be called once per supported complication, and the results will be cached
    
    if complication.family == .circularSmall {
      
      let modularTemplate = CLKComplicationTemplateCircularSmallSimpleImage()
      let image = UIImage(named:"Complication/Circular")!
      modularTemplate.imageProvider = CLKImageProvider(onePieceImage: image )
      
      //modularTemplate.fillFraction = 0.7
      //modularTemplate.ringStyle = CLKComplicationRingStyle.closed
      
      handler(modularTemplate)
      
    }
      
    else if complication.family == .modularSmall {
      let modularTemplate = CLKComplicationTemplateModularSmallSimpleImage()
      let image = UIImage(named:"Complication/Modular")!
      modularTemplate.imageProvider = CLKImageProvider(onePieceImage: image )
      //modularTemplate.fillFraction = 0.0
      //modularTemplate.ringStyle = CLKComplicationRingStyle.closed
      
      handler(modularTemplate)
    }
    else if complication.family == .utilitarianSmallFlat {
      let modularTemplate = CLKComplicationTemplateUtilitarianSmallFlat()
      modularTemplate.textProvider = CLKSimpleTextProvider(text: WLoc("Laws"))
      
      handler(modularTemplate)
    }
    else if complication.family == .extraLarge {
      let modularTemplate = CLKComplicationTemplateExtraLargeSimpleText()
      modularTemplate.textProvider = CLKSimpleTextProvider(text: WLoc("Laws"))
      
      handler(modularTemplate)
    }
    else if complication.family == .utilitarianSmall {
      let modularTemplate = CLKComplicationTemplateUtilitarianSmallSquare()
      let image = UIImage(named:"Complication/Utilitarian")!
      modularTemplate.imageProvider = CLKImageProvider(onePieceImage: image )
      
      handler(modularTemplate)
    }
    else {
      handler(nil)
    }
  }
}

//
//  AppTest.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 26/09/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import XCTest
import CloudKit

class CloudKitTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testFetch() {
        // This is an example of a functional test case.
		// Use XCTAssert and related functions to verify your tests produce the correct results.
		
		let expectation = self.expectation(description: "testFetch")

		CloudKitRepository.shared.fetch("特許", tokenSearch: true) { (results, cursor, error) -> Void in
		
			print(results)
			
			if error != nil {
				print("desc \(error!.localizedDescription)")
			}
			
			expectation.fulfill()
		}
		
		self.waitForExpectations(timeout: 30) { (error) -> Void in
			print(error)
		}
	}
	
	func testFetchRecord() {
		
		let expectation = self.expectation(description: "testFetchRecord")
		
		CloudKitRepository.shared.fetchRecord(withPackageBundle:"H08HO109", desiredKeys: [RecordType.LawDocPackageKey.md5, RecordType.LawDocPackageKey.title]) {
			(results: [CKRecord], error: Error?)->Void in
			
			if error != nil {
				print(error)
			}
			
			print(results)
			
			/// End
			expectation.fulfill()
		}
		
		self.waitForExpectations(timeout: 30) { (error) -> Void in
			
			print(error)
		}
	}

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}

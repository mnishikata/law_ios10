//
//  LawsTests.swift
//  LawsTests
//
//  Created by Masatoshi Nishikata on 22/09/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import XCTest
import UIKit
//@testable import Kanna
//@testable import ProcessTags

var gDebugMode = true

class TextProcessing: XCTestCase {
	
	var dataToTest:Data? = nil
	var theString:NSMutableString? = nil
	
	override func setUp() {
		super.setUp()
		// Put setup code here. This method is called before the invocation of each test method in the class.
		
		/*
		let URL = Bundle(for:type(of: self)).url(forResource:"租税特別措置法", withExtension: "html")
		dataToTest = try? Data(contentsOf: URL!)
		
		theString = NSMutableString(data: dataToTest!, encoding: String.Encoding.ascii.rawValue)
		
		let theType = theString!.encodingType
		theString = NSMutableString(data: dataToTest!, encoding: theType.rawValue)

		XCTAssert( theString != nil, "failed to load" )
*/
	}
	
	
	override func tearDown() {
		// Put teardown code here. This method is called after the invocation of each test method in the c lass.
		super.tearDown()
	}
	
	func testBlockQuote() {
		
		self.measure {
			let test = "test".buildBlockQuote(href:"test", uuid:"123")
			print(test)
		}
	}
	
	func testDetectCommonPrefix() {
		
		let common = String.detectCommonPrefix("ABCDEF</div>", with:"ABC</div>" )
		XCTAssert( common == "ABC" )
	}
	
	func testProcessQuery() {
		
		self.measure {
			let string = "http://law.e-gov.go.jp/cgi-bin/idxrefer.cgi?H_FILE=%95%BD%93%F1%8E%4f%96%40%8C%DC%93%F1&REF_NAME=%89%C6%8E%96%8E%96%8C%8F%8E%E8%91%B1%96%40&ANCHOR_F=&ANCHOR_T="
			
			let pairs:[String:String] = string.getUrlQueryParameters() //0.002 0.001
			print(pairs)
		}
	}
	
	func testAppendParenthesisTag() {

		var title = ""
		var editedDate = ""
		
		XCTAssert( theString != nil, "failed to load" )
		
		title = theString!.titleOfHTML
		editedDate = theString!.lawEditionDateString
		print(title, editedDate)
		
		theString!.replaceCSS(stripCSSInstead:false, zoomScale:1.0)
		
		print("Converting Kanji")
		
		self.measure {
			
      let aCopy = NSMutableString(string: self.theString!)
			aCopy.convertKanjiNumberToArabicNumber(convertToZenkaku: true) // 30sec, 28.5
		}
		
		// Compress anchors
		print("Compress anchors")
		
		theString!.compressAnchors()
		
		XCTAssert( theString != nil, "failed to load" )
		
		theString!.appendParenthesisTags() // 17sec
		
		print("check memory")
	}
	
	/*
	func testAppendParenthesisTagSwiftString() {

		
		var title:NSString = ""
		var editedDate:NSString = ""
		
		XCTAssert( theString != nil, "failed to load" )
		
		
		title = titleOfHTML( theString! )
		editedDate =  lastEdited( theString! )
		
		
		print(title, editedDate)
		
		replaceCSS( theString!, stripCSSInstead:false, zoomScale:1.0)
		
		print("Converting Kanji")
		
		findAndConvertKanji(theString!, convertToZenkaku: true)
		
		// Compress anchors
		print("Compress anchors")
		
		
		compressAnchorsInString( theString! )
		
		XCTAssert( theString != nil, "failed to load" )
		
		self.measure {
			autoreleasepool {
				
				appendParenthesisTagSwiftString( self.theString! )
			}
		}
		
		
		print("check memory")
	}
	*/
	
	func testAppendSuji() {
		
		func appendSuji(_ digits:Int) -> String {
			
			var suji = "11"
			if suji.characters.count == 0 { return "" }
			
			let theSuji = "000" + suji
			suji = ""
			return theSuji.substring(with: Range<String.Index>( theSuji.characters.index(theSuji.endIndex, offsetBy: 0 - digits) ..<  theSuji.endIndex))
		}
		
		let str = appendSuji(3)
		print(str)
	}
	
	func testHTML4() {
		// This is an example of a functional test case.

		let URL = Bundle(for:type(of: self)).url(forResource:"民法", withExtension: "html")
		dataToTest = try? Data(contentsOf: URL!)
		
		theString = NSMutableString(data: dataToTest!, encoding: String.Encoding.ascii.rawValue)
		
		let theType = theString!.encodingType
		theString = NSMutableString(data: dataToTest!, encoding: theType.rawValue)
		
		
		// TIME
		self.measure {
			let _ = HTML(html:self.theString! as String, encoding: .utf8)
			// average: 0.064 on simulator
			// average: 0.132 on iPhone 5s
		}
		
		
		guard let doc = HTML(html:theString! as String, encoding: .utf8) else {
			return
		}
		let nodeSet:XPathObject = doc.css("a, b, div")
		
		for link in nodeSet {
			
			if link.tagName?.lowercased() == "div" {
				print("-")
				continue
			}
			
			var text = link.text!
			text = text.replacingOccurrences(of: "\n", with: "")
			text = text.replacingOccurrences(of: "\t", with: "")
			
			if let name = link["name"] {
				print("\(text) [NANE] \(name)")
			}
				
			else if let href = link["href"]  {
				print("\(text) [HREF] \(href)")
				
			}else {
				print("\(text) [\(link.tagName!)]")
			}
		}
	}
	
	/// MARK:-
	
	func testRangeOfHTML() {
		
		let string = "aB\ncdef" as NSString
		
		print(NSStringFromRange(string.rangeOfHTMLSnipped("A")!))
		print(NSStringFromRange(string.rangeOfHTMLSnipped("a")!))
		print(NSStringFromRange(string.rangeOfHTMLSnipped("f")!))
//		print(NSStringFromRange(string.rangeOfHTMLSnipped("ft")!))
		print(NSStringFromRange(string.rangeOfHTMLSnipped("ab")!))
		print(NSStringFromRange(string.rangeOfHTMLSnipped("abcd")!))
		print(NSStringFromRange(string.rangeOfHTMLSnipped("abcd\n\nef")!))

	}
	
	typealias ArticleType = (title:String, topItem:Bool, isArttitle:Bool, isNumber:Bool, anchor:String?, extraAnchor:String?, links:[String]?, range:NSRange )
	
//	func testKanna() { // TOO SLOW
//		//128.991
//		//130.660
//		//131.505
//		let URL = Bundle(for:self.dynamicType).urlForResource("民事訴訟法", withExtension: "html")
//		dataToTest = try? Data(contentsOf: URL!)
//		
//		theString = NSMutableString(data: dataToTest!, encoding: String.Encoding.ascii.rawValue)
//		
//		let theType = encodeType(theString!)
//		theString = NSMutableString(data: dataToTest!, encoding: theType.rawValue)
//		
//		let source = theString! as String
//		
//		guard let doc = HTML(html: source, encoding: theType) else { return }
//		
//		// 刑事訴訟法２０条が特殊
//		
//		var elements:[ XMLElement ]? = nil//[XMLElement]()
//		var articles = [ArticleType]()
//		var isNumber = false
//		var divRange = Range<String.Index>(source.startIndex ..< source.endIndex )
//		let links = doc.css("a, b, div, p")
//		
//		var debugCounter = 0
//		
//		for link in links {
//			
//			if link.tagName?.lowercased() == "div"  {
//				
//				if link["class"] == "item" || link["class"] == "number"{
//					
//					
//					let toHTML = link.toHTML!.replacingOccurrences(of: "&amp;", with: "&")
//					
//					let searchRange:NSRange = NSMakeRange(divRange.startIndex, source.endIndex - divRange.startIndex )
//					
//					let range = source.rangeOfHTMLSnipped(toHTML, range: searchRange)
//					debugCounter += 1
//					
//					
//					if range != nil {
//						divRange = range!
//					}else {
//						
//						//	print("\(toHTML) was not found")
//					}
//					
//					
//					// END PREV
//					if elements != nil {
//						articles.append( self.debugDumpElements(elements!, isNumber:isNumber, range:divRange) )
//					}
//					
//					// BEGIN
//					elements = [XMLElement]()
//					isNumber = (link["class"] == "number")
//					continue
//				}
//			}
//			
//			if link.tagName?.lowercased() == "p" {
//				
//				// END PREV
//				
//				if elements != nil {
//					articles.append( self.debugDumpElements(elements!, isNumber:isNumber, range:divRange) )
//				}
//				elements = nil
//				continue
//			}
//			elements?.append( link )
//		}
//		
//		print("** rangeOfHTMLSnipped counter: \(debugCounter)")
//		dumpArticles(articles)
//		
//	}

	
	func testKannaNSString() {
		// SEE HTMLElementsInSource in HTMLElementObject.swift

		let URL = Bundle(for:type(of: self)).url(forResource:"民事訴訟法", withExtension: "html")
		dataToTest = try? Data(contentsOf: URL!)
		
		theString = NSMutableString(data: dataToTest!, encoding: String.Encoding.ascii.rawValue)
		
		let theType = theString!.encodingType
		theString = NSMutableString(data: dataToTest!, encoding: theType.rawValue)
		
		let source = theString! as String
		
		guard let doc = HTML(html: source, encoding: theType) else { return }
		
		// 刑事訴訟法２０条が特殊
		
		var elements:[ XMLElement ]? = nil//[XMLElement]()
		var articles = [ArticleType]()
		var isNumber = false
		var divRange = NSMakeRange(0, theString!.length )
		let links = doc.css("a, b, div, p")
		
		for link in links {
			let lowercaseTagName = link.tagName?.lowercased()
			if lowercaseTagName == "div"  {
				let classTag = link["class"]

				if classTag == "item" || classTag == "number" || classTag == "arttitle" {
					
					let toHTML = link.toHTML!.replacingOccurrences(of: "&amp;", with: "&")
					let searchRange = NSMakeRange(divRange.location, theString!.length - divRange.location)
					let range = theString!.rangeOfHTMLSnipped(toHTML as NSString, range: searchRange)
					
					if range != nil {
						divRange = range!
					}else {
						
						//	print("\(toHTML) was not found")
					}
					
					// END PREV
					if elements != nil {
						articles.append( self.debugDumpElements(elements!, isNumber:isNumber, range: divRange) )
					}
					
					// BEGIN
					elements = [XMLElement]()
					isNumber = (link["class"] == "number")
					continue
				}
			}

			if lowercaseTagName == "p" {
				
				// END PREV
				
				if elements != nil {
					articles.append( self.debugDumpElements(elements!, isNumber:isNumber, range: divRange) )
				}
				elements = nil
				continue
			}
			elements?.append( link )
		}
		
		dumpArticles(articles)
		
	}
	
	func testConvertPkgNameToHFileKanji() {
		
		XCTAssert( "H08HO001".convertPkgNameToHFileKanji() == "平八法一", "FAILED" )
		XCTAssert( "S21KE000".convertPkgNameToHFileKanji() == "昭二一憲〇", "FAILED" )
		XCTAssert( "S20KE010".convertPkgNameToHFileKanji() == "昭二〇憲一〇", "FAILED" )
	}
	
	func testBuildAllTOC2() {
		// with autoreleasepool ... Max 206.6 MB, 214.3, 212.5
		// without ... 332.5 MB
		
		// autoreleaepool in build toc2 loop ... 188.5MB, 185.3
		
		// autoreleaepool in build toc2 loop NSArray ... 188.5MB, 2 min 26 sec
		
		// 7.1, 6.6,
		// dispatch_apply -> 7.3
		let expectation = self.expectation(description: "building toc2")
		
		var contents:[URL] = []
		do {
			contents = try FileManager.default.contentsOfDirectory(at: FilePackage.documentFolderURL,
			                                                       includingPropertiesForKeys: nil, options: [.skipsHiddenFiles])
		} catch _ {
		}
		
		processQueue_.async {
			//			self.measureBlock {
			let fp = FilePackage.shared
			for url in contents {
				autoreleasepool {
					if url.pathExtension == FilePackage.packageExtension {
						print("Building toc for \(url.lastPathComponent)")
						
            let (plist,linkDictionaries,rawHTML) = fp.loadPackage(at: url, onlyPlist:false, rawHTML:true)
						
						fp.buildHTMLElements(rawHTML!, for:url, plist:plist, linkDictionaries:linkDictionaries!, overwriteMode: true )
					}
				}
			}
			expectation.fulfill()
		}
		
		self.waitForExpectations(timeout: 600.0, handler: { (error) in })
	}
	
	func testInflate() {
		let URL = Bundle(for:type(of: self)).url(forResource:"民法", withExtension: "html")
		let dataToTest = try? Data(contentsOf: URL!)
		
		
		let mdata = NSMutableData(data: dataToTest!)
		for _ in 0 ..< 100 {
			mdata.append(dataToTest!)
		}
		
		let deflated = mdata.deflate()
		print("deflated data size: \(deflated?.count)")
		
		self.measure {
			(deflated! as NSData).inflate()
		}
		
		//DEFAULT	0.418	0.404 10900592 bytes
		//Speed 1	0.490	14042673
		//Best 9		0.398	0.409 10746112
	}
	
	func testKannaRemoveReturn() { // 0.769
		
		// 1.262
		let URL = Bundle(for:type(of: self)).url(forResource:"民法", withExtension: "html")
		dataToTest = try? Data(contentsOf: URL!)
		theString = NSMutableString(data: self.dataToTest!, encoding: String.Encoding.ascii.rawValue)
		
		let theType = theString!.encodingType
		theString = NSMutableString(data: self.dataToTest!, encoding: theType.rawValue)
		
		let source = (self.theString! as String).replacingOccurrences(of: "\n", with: "") //0.031sec
		
    _ = ElementDescriptor.HTMLElements(in: source as NSString, dump: true, buildRealmObjects: true) // average 0.716 on simulator
		
	}

	
	func convertElements(_ elements:[ (XMLElement, Range<String.Index>) ], isNumber:Bool, range:NSRange) -> ArticleType {
		var arttitle:String? = nil
		var bTag:XMLElement? = nil
		var comcatalystwolawsTag:XMLElement? = nil
		var nameTag:XMLElement? = nil
		var extraNameTag:XMLElement? = nil
		var links:[XMLElement]? = nil
		for element in elements {
			
			let tagName = element.0.tagName?.lowercased()
			if tagName == "b" && bTag == nil {
				bTag = element.0
			}
			
			if let href = element.0["href"] {
				if href.hasPrefix("comcatalystwolaws:") && comcatalystwolawsTag == nil {
					comcatalystwolawsTag = element.0
					articleRange = range
				}else {
					if href.hasPrefix("highlight:") { continue }
					
					// internal & external link
					if links == nil { links = [XMLElement]() }
					links!.append(element.0)
				}
			}
			
			if let _ = element.0["name"] {
				if nameTag == nil {
					nameTag = element.0
				}else if extraNameTag == nil {
					extraNameTag = element.0
				}
			}
			
			if "arttitle" == element.0["class"] {
				arttitle = element.0.text
			}
			
			if articleRange == nil {
				articleRange = range
			}else {
				articleRange!.length = NSMaxRange(range) - articleRange!.location
			}
		}
		
		// OUTPUT
		
		var val: ArticleType = ("", topItem: comcatalystwolawsTag != nil, isArttitle:false, isNumber:isNumber, anchor:nil, extraAnchor:nil, links:nil, range:range )
		
		if arttitle != nil {
			val.title = arttitle!
			val.isArttitle = true
		}
		
		if bTag != nil {
			var text = bTag!.text!
			
			text = text.replacingOccurrences(of: "\n", with: "")
			text = text.replacingOccurrences(of: "\t", with: "")
			text = text.replacingOccurrences(of: "○", with: "")
			
			val.title = text
		}
		
		if nameTag != nil {
			val.anchor = nameTag!["name"]
		}
		
		if extraNameTag != nil {
			val.extraAnchor = extraNameTag!["name"]
		}
		
		if links != nil {
			
			var linkPaths = [String]()
			for link in links! {
				linkPaths.append(link["href"]!)
			}
			
			val.links = linkPaths
		}
		
		return val
	}

	
	
	
	func dumpArticles( _ articles: [ArticleType] ) {
		// DUMP
		var currentTopItem:ArticleType? = nil
		
		for article in articles {
			var title:String = ""
			var prefix = ""
			if article.topItem || article.isArttitle {
				currentTopItem = article
				
				print("-----------------NSRange(\(article.range.location), \(article.range.length)) ")
			}else
			{
				print("---- NSRange(\(article.range.location), \(article.range.length)) ")
			}
			
			if article.isNumber {
				prefix = "　　"
				title = prefix + article.title
			}else {
				title = article.title
			}
			
			if article.anchor != nil {
				title += "　[ANCHOR] \(article.anchor!)"
			}
			
			print(title)
			
			if article.extraAnchor != nil && article.topItem {
				print("１　[EXTRA ANCHOR] \(article.extraAnchor!)")
			}
			
			if article.links != nil {
				for link in article.links! {
					print("\(prefix)[LINK] \(link)")
				}
			}
		}
		
		if currentTopItem != nil {
		}
	}
	
	var articleRange:NSRange? = nil
	
	func debugDumpElements(_ elements:[XMLElement], isNumber:Bool, range:NSRange) -> ArticleType {
		var bTag:XMLElement? = nil
		var arttitle:String? = nil
		var comcatalystwolawsTag:XMLElement? = nil
		var nameTag:XMLElement? = nil
		var extraNameTag:XMLElement? = nil
		var links:[XMLElement]? = nil
		for element in elements {
			
			let tagName = element.tagName?.lowercased()
			if tagName == "b" && bTag == nil {
				bTag = element
			}
			
			if let href = element["href"] {
				if href.hasPrefix("comcatalystwolaws:") && comcatalystwolawsTag == nil {
					comcatalystwolawsTag = element
					articleRange = range
				}else {
					if href.hasPrefix("highlight:") { continue }
					
					// internal & external link
					if links == nil { links = [XMLElement]() }
					links!.append(element)
				}
			}
			
			if let _ = element["name"] {
				if nameTag == nil {
					nameTag = element
				}else if extraNameTag == nil {
					extraNameTag = element
				}
			}
			
			if "arttitle" == element["class"] {
				arttitle = element.text
			}
			
			if articleRange == nil {
				articleRange = range
			}else {
				articleRange!.length = NSMaxRange(range) - articleRange!.location
			}
		}
		
		// OUTPUT
		
		var val: ArticleType = ("", topItem: comcatalystwolawsTag != nil, isArttitle:arttitle != nil, isNumber:isNumber, anchor:nil, extraAnchor:nil, links:nil, range:range )
		
		if arttitle != nil {
			val.title = arttitle!
			val.isArttitle = true
		}
		
		if bTag != nil {
			var text = bTag!.text!
			
			text = text.replacingOccurrences(of: "\n", with: "")
			text = text.replacingOccurrences(of: "\t", with: "")
			text = text.replacingOccurrences(of: "○", with: "")
			
			val.title = text
		}
		
		if nameTag != nil {
			val.anchor = nameTag!["name"]
		}
		
		if extraNameTag != nil {
			val.extraAnchor = extraNameTag!["name"]
		}
		
		if links != nil {
			
			var linkPaths = [String]()
			for link in links! {
				linkPaths.append(link["href"]!)
			}
			
			val.links = linkPaths
		}
		
		return val
	}
	
	/// MARK:-
	
	func testExample() {
		// This is an example of a functional test case.
		// Use XCTAssert and related functions to verify your tests produce the correct results.
	}
	
	func testPerformanceExample() {
		// This is an example of a performance test case.
		self.measure {
			// Put the code you want to measure the time of here.
		}
	}
	
}



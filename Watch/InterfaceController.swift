//
//  InterfaceController.swift
//  TestWatchApp WatchKit Extension
//
//  Created by Masatoshi Nishikata on 22/04/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import WatchKit
import WatchConnectivity
import Foundation

let watchQueue: DispatchQueue = DispatchQueue(label: "ProcessTagQueue" )

var rawHTML: NSString? = nil
var elements: [ElementDescriptor]? = nil
var packageURL: URL? = nil
var toc1: [LinkDictionary]? = nil
var title: String? = nil

var debugNote: String? = nil

class InterfaceController: WKInterfaceController, WatchConnectivityManagerDelegate {
	
	func sendMessage(_ message: [String : Any], replyHandler: (([String : Any]) -> Void)?, errorHandler: ((Error) -> Void)?){
		WatchConnectivityManager.shared.sendMessage(message, replyHandler: replyHandler, errorHandler: errorHandler)
	}
	
	func requestFiles(_ packageNames: [String]){
		WatchConnectivityManager.shared.sendMessage([FilePackage.WatchMessageKey.requestFilesFromWatch: packageNames], replyHandler: nil, errorHandler: nil)
	}
	
	func watchConnectivityManager(_ watchConnectivityManager: WatchConnectivityManager, updatedWithContext context: [String: Any]) {
		didReceiveUpdatesFromIPhone(context)
	}
	
	func watchConnectivityManager(_ watchConnectivityManager: WatchConnectivityManager, didReceiveMessage : [String: Any], withReplyHandler reply: (([String : Any]) -> Void)?) {
		reply?( [:] )
	}
	
	func watchConnectivityManager(_ watchConnectivityManager: WatchConnectivityManager, didReceiveFile file: WCSessionFile) {
		
		print("** Received \(file.fileURL)")
		do {
			let path = file.fileURL.path
				if let data = try? Data(contentsOf: URL(fileURLWithPath: path)) {
					if let wrapper = FileWrapper(serializedRepresentation: data) {
						if let filename = file.metadata?["filename"] as? String {
							let dest = FileRepository.shared.documentFolderURL.appendingPathComponent(filename)
							try wrapper.write(to: dest, options: [], originalContentsURL: nil)
							print("** moved to \(dest)")
							
							DispatchQueue.main.async {
								self.updateUI()
							}
						}
						//TODO: Delete temp file on iOS
						
					}else {
						debugPrint("*** Failed to un-wrap the wrapper")
					}
				}
			
		}catch let errror as NSError {
			debugPrint("*** Failed to write \(errror)")
		}
	}

	var dataSource: [[String: Any]]? = nil
	
	@IBOutlet var messageLabel: WKInterfaceLabel!
	@IBOutlet var table: WKInterfaceTable!
	
	override func awake(withContext context: Any?) {
		super.awake(withContext: context)
		
		let defaultSession = WCSession.default()
		defaultSession.delegate = WatchConnectivityManager.shared
		WatchConnectivityManager.shared.delegate = self
		defaultSession.activate()
		
		setTitle(WLoc("WatchKitTitle"))
		
		// Configure interface objects here.
		
		let ud = UserDefaults.standard
		ud.synchronize()
		dataSource = ud.object(forKey: "WatchIndexKey") as? [[String: Any]]
	}
	
	override func willActivate() {
		super.willActivate()
		let defaultSession = WCSession.default()
		defaultSession.delegate = WatchConnectivityManager.shared
		WatchConnectivityManager.shared.delegate = self
		defaultSession.activate()
		
		updateUI(true)

	}
	
	override func didDeactivate() {
		super.didDeactivate()
	}
	
	func updateUI(_ downloadIfNecessary: Bool = false) {
		guard dataSource != nil else {
			messageLabel.setText( WLoc("Almost there!  Open iOS App to finish installing this Watch App.") )
			table.setNumberOfRows(0, withRowType: "myRow")
			var dict: [String: Any] = [FilePackage.WatchMessageKey.requestIndexFromWatch: true]
			let ud = UserDefaults.standard
			if let etag = ud.object(forKey: "IndexETagForWathApp") as? String {
				dict["ETag"] = etag
			}
			
			sendMessage(dict, replyHandler: { context in self.didReceiveUpdatesFromIPhone(context) }, errorHandler: nil)
      return
    }
    
    let fm = FileManager.default
    table.setNumberOfRows(dataSource!.count, withRowType: "myRow")
    
    for i in 0 ..< dataSource!.count {
      // Set the values for the row controller
      if let row = table.rowController(at: i) as? MyRowController {
        let dictionary = dataSource![i]
        let pkgName = dictionary[FilePackage.WatchMessageKey.indexDataSourcePackageName] as? String
        if pkgName == nil { continue } //UNKONW ERR
        
        let pkgURL = FileRepository.shared.documentFolderURL.appendingPathComponent(pkgName!)
        let exists = fm.fileExists(atPath: (pkgURL.path))
        
        if let title = dictionary[FilePackage.WatchMessageKey.indexDataSourceTitle] {
          
          row.setText(title as! String)
          row.loading = !exists
        }else {
          row.setText("")
          row.loading = false
        }
        
        if downloadIfNecessary && !exists {
          requestFiles([pkgName!])
          print("--- Download \(pkgName!)")
          
        }
      }
		}
		messageLabel.setText("")
	}
	
	override func didAppear() {
		// This method is called when watch view controller is about to be visible to user
		super.didAppear()
	}
	
	func didReceiveUpdatesFromIPhone(_ context: [String: Any] ) {
		let etag = context[FilePackage.WatchMessageKey.indexEtag] as? String
		let source = context[FilePackage.WatchMessageKey.indexDataSource] as? [[String: Any]]
		if source == nil { return }
		
		DispatchQueue.main.async {
			
			let ud = UserDefaults.standard
			ud.set(source, forKey: "WatchIndexKey")
			
			if etag != nil {
				ud.set(etag, forKey: etag!)
			}
			ud.synchronize()
			
			self.dataSource = source
			self.updateUI(true)
		}
	}
	
	override func table( _ table: WKInterfaceTable,
	             didSelectRowAt rowIndex: Int) {
		print("tapped \(rowIndex)")
		guard let source = dataSource else { return }
		if source.count <= rowIndex { return }
		if rowIndex < 0 { return }
		let dictionary = source[rowIndex]
		let pkgName = dictionary[FilePackage.WatchMessageKey.indexDataSourcePackageName]
		if pkgName == nil { return }
		
		title = dictionary[FilePackage.WatchMessageKey.indexDataSourceTitle] as? String
		
		let pkgURL = FileRepository.shared.documentFolderURL.appendingPathComponent(pkgName as! String)
		guard true == FileManager.default.fileExists(atPath: (pkgURL.path)) else {
			
			let cancel = WKAlertAction(title: WLoc("OK"), style: WKAlertActionStyle.default, handler: {})
			presentAlert(withTitle: WLoc("Downloading"), message: WLoc("DownloadingMessage"), preferredStyle: WKAlertControllerStyle.alert, actions: [cancel])
			return
		}
		
		watchQueue.async {
			
			let fp = FilePackage.shared
			
			let myBundle = Bundle(for: type(of: self))
			let contents = FileManager.default.subpaths(atPath: myBundle.resourceURL!.path)
			
			print(contents)
			print( FileRepository.shared.documentFolderURL )
			
			let t0 = Date.timeIntervalSinceReferenceDate
			packageURL = FileRepository.shared.documentFolderURL.appendingPathComponent(pkgName! as! String)
			
      let (_,linkDictionary, html) = fp.loadPackage(at: packageURL, onlyPlist: false, zoomScale: 1.0, rawHTML: true)
			
			let t1 = Date.timeIntervalSinceReferenceDate

			if html != nil {
				
				rawHTML = html!
				
				let t2 = Date.timeIntervalSinceReferenceDate
 
				elements = fp.loadHTMLElements(for: packageURL!)
				toc1 = linkDictionary
				
				let t3 = Date.timeIntervalSinceReferenceDate

				debugNote = "Load \(t1-t0), Replace \(t2-t1), LoadHTMLElement \(t3-t2)"
			}
		}
		
		pushController(withName: "PickerController", context: nil)

	}
}

class MyRowController : NSObject {
	
	@IBOutlet weak var label: WKInterfaceLabel!
	@IBOutlet var imageView: WKInterfaceImage!
	
	var loading: Bool = false {
		didSet {
			if loading {
				imageView?.setImageNamed("loading-spinner-large")
				imageView?.startAnimatingWithImages(in: NSMakeRange(0, 24), duration: 2.4, repeatCount: -1)
				imageView?.setWidth(20)
				imageView?.setHidden( false )

			}else {
				imageView?.setHidden( true )
				imageView?.setWidth(0)
			}
		}
	}
	
	func setText(_ text: String) {
		label?.setText(text)
	}
}

class PickerController : WKInterfaceController {
	
	var connector: String? = nil
	var connectorStatus = ConnectorStatus(string: "")
	var inputtedString: String = "" {
		didSet {
			clearButton.setHidden( inputtedString.isEmpty )
		}
	}
	
	var hoge: Int = 0
	@IBOutlet var rightButton: WKInterfaceButton!
	@IBOutlet var label: WKInterfaceLabel!
	@IBOutlet var body: WKInterfaceLabel!
	@IBOutlet var imageView: WKInterfaceImage!
	@IBOutlet var clearButton: WKInterfaceButton!
	
	@IBAction func button0() { insertText("0") }
	@IBAction func button1() { insertText("1") }
	@IBAction func button2() { insertText("2") }
	@IBAction func button3() { insertText("3") }
	@IBAction func button4() { insertText("4") }
	@IBAction func button5() { insertText("5") }
	@IBAction func button6() { insertText("6") }
	@IBAction func button7() { insertText("7") }
	@IBAction func button8() { insertText("8") }
	@IBAction func button9() { insertText("9") }
	@IBAction func buttonRight() {
		
		if connectorStatus.keyTopString == "号" && inputtedString.hasSuffix("項") {
			inputtedString = String( inputtedString.characters.dropLast() )
		}
		
		insertText(connectorStatus.keyTopString)
	}
	@IBAction func buttonNo() {
		insertText("の")
	}
	@IBAction func buttonClear() {
		inputtedString = ""
		connectorStatus = ConnectorStatus(string: inputtedString)
		label.setText("")

		updateUI()
	}
	
	func updateUI() {
		rightButton.setTitle(connectorStatus.keyTopString)
		requestSnippet()
	}
	
	func insertText(_ string: String!) {
		
		inputtedString = inputtedString + string
		label.setText(inputtedString)
		connectorStatus.transitionWithString(string)
		
		updateUI()
	}
	
	override func awake(withContext context: Any?) {
		super.awake(withContext: context)
		body.setText("")
		label.setText(nil)
		clearButton.setHidden(true)
	}
	
	override func didAppear() {
		setTitle(title)
	}
	
	// SEND
	func requestSnippet() {
		
		if inputtedString.isEmpty {
			imageView.setImageNamed(nil)
			body.setText("")
			return
		}
		guard packageURL != nil else { return }

		imageView.setImageNamed("loading-spinner")
		imageView.startAnimatingWithImages(in: NSMakeRange(0, 24), duration: 2.4, repeatCount: -1)
		
		let theString = inputtedString
		watchQueue.async {
			
			let fp = FilePackage.shared
			let (anchor,element) = fp.anchor(for: theString, in: packageURL!, toc2: elements, toc1: toc1)
			
			if anchor == nil {
				DispatchQueue.main.async {
					self.imageView.setImageNamed("gray-unchecked")
					self.body.setText("")
				}
				return
			}
			
			func findAnchorInElement(_ element: ElementDescriptor) -> Bool {
        let range = element.lookForRange(for: anchor!)
				if range.location != NSNotFound {
					let htmlString = rawHTML!.substring(with: range)
					let data = htmlString.data(using: String.Encoding.utf8)
					do {
						let attr = try NSMutableAttributedString(data: data!, options: [NSDocumentTypeDocumentAttribute  : NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
						
						DispatchQueue.main.async {
							if theString == self.inputtedString {
//								self.body.setAttributedText(attr)
								self.body.setText(attr.string)
//								self.body.setText(debugNote)

								if attr.length > 0 {
									self.imageView.setImageNamed("green-checked")
								}else {
									self.imageView.setImageNamed("gray-unchecked")
									self.body.setText("")
								}
							}
						}
						return true
						
					}catch _ {
					}
				}else {
					DispatchQueue.main.async {
						self.imageView.setImageNamed("gray-unchecked")
						self.body.setText("")
					}
				}
				return false
			}
			
			if element == nil {
				for element in elements! {
					if findAnchorInElement(element) {
						break
					}
				}
			}else {
				let _ = findAnchorInElement(element!)
			}
		}
	}
}
